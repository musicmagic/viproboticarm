/*  File    : sfun_counter_cpp.cpp
 *  Abstract:
 *
 *      Example of an C++ S-function which stores an C++ object in
 *      the pointers vector PWork.
 *
 *  Copyright 1990-2013 The MathWorks, Inc.
 */

#include <iostream> 
#include <stdio.h>
#include <string>
#include <sys/select.h>

class  counter {
    double  x;
    int lastKeyValue;
    bool impedanceOn;
public:
    
    counter() {
        x = 0.0;
        lastKeyValue=1;
        impedanceOn = false;
    }
    double output(void) {
        x = x + 1.0;
        return x;
    }
    double getX() const
    {
        return x;
    }
    void setX(double v)
    {
        x = v;
    }

    int kbhit(void)
    {
        struct timeval tv;
        fd_set read_fd;
        
        /* Do not wait at all, not even a microsecond */
        tv.tv_sec=0;
        tv.tv_usec=0;
        
        /* Must be done first to initialize read_fd */
        FD_ZERO(&read_fd);
        
        /* Makes select() ask if input is ready:
         * 0 is the file descriptor for stdin    */
        FD_SET(0,&read_fd);
        
        /* The first parameter is the number of the
         * largest file descriptor to check + 1. */
        if(select(1, &read_fd,NULL, /*No writes*/NULL, /*No exceptions*/&tv) == -1)
            return 0;  /* An error occured */
        
        /*  read_fd now holds a bit map of files that are
         * readable. We test the entry for the standard
         * input (file 0). */
        
        if(FD_ISSET(0,&read_fd))
            /* Character pending on stdin */
            return 1;
        
        /* no characters were pending */
        return 0;
    }
    double getPress(){
        
        if (kbhit()) {
            //std::cout << ".";
            switch(getchar()){
                case 32:
                    if(impedanceOn){
                        std::cout<< "impedance off" <<std::endl;
                        lastKeyValue = 1;
                        impedanceOn =false;
                        return 1;
                    }else{
                        std::cout<< "impedance on" <<std::endl;
                        lastKeyValue = 2;
                        impedanceOn = true;
                        return 2;
                    }
                    break;
                case 48: //0
                    return 48;
                case 49://1
                    return 49;
                case 50: //2
                    return 50;
                case 51: //3
                    return 51;
                case 52: //4
                    return 52;
                case 53: //5    
                    return 53;
                case 54: //6    set acc tpggle
                    return 54;
                case 55: // 7   set acc bound
                    return 55;
                case 97: //a
                    return 97;
                case 98: //b
                    return 98;
                 case 104: //h for high threshold in EMG setting
                    return 104;   
                case 108: //l for low threshold in emg setting
                     return 108;
                case 109: //m for muscle settting
                    //std::cout<< "109" <<std::endl;
                    return 109;
                case 110:  // n for next section in current composition
                    return 110; 
                case 111:
                    return 111; // o for trigger rudiment
                case 112:
                     return 112; // bypass autonomous mode for primary
                case 115:
                     return 115; //bypass autonomous mode for secondary
                default:
                    break;
            }
        }
        //std::cout<< "press ... " << lastKeyValue <<std::endl;
        return lastKeyValue;
    }
};

#define S_FUNCTION_LEVEL 2
#define S_FUNCTION_NAME  keyboard_press

/*
 * Need to include simstruc.h for the definition of the SimStruct and
 * its associated macro definitions.
 */
#include "simstruc.h"

#define IS_PARAM_DOUBLE(pVal) (mxIsNumeric(pVal) && !mxIsLogical(pVal) &&\
!mxIsEmpty(pVal) && !mxIsSparse(pVal) && !mxIsComplex(pVal) && mxIsDouble(pVal))

/*====================*
 * S-function methods *
 *====================*/

#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS)  && defined(MATLAB_MEX_FILE)
/*
 * Check to make sure that each parameter is 1-d and positive
 */
static void mdlCheckParameters(SimStruct *S)
{
    
    const mxArray *pVal0 = ssGetSFcnParam(S,0);
    
    if ( !IS_PARAM_DOUBLE(pVal0)) {
        ssSetErrorStatus(S, "Parameter to S-function must be a double scalar");
        return;
    }
}
#endif


/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, 1);  /* Number of expected parameters */
#if defined(MATLAB_MEX_FILE)
    if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
        mdlCheckParameters(S);
        if (ssGetErrorStatus(S) != NULL) {
            return;
        }
    } else {
        return; /* Parameter mismatch will be reported by Simulink */
    }
#endif
    ssSetSFcnParamTunable(S, 0, 0);
    
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);
    
    if (!ssSetNumInputPorts(S, 0)) return;
    
    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, 1);
    
    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 1); // reserve element in the pointers vector
    ssSetNumModes(S, 0); // to store a C++ object
    ssSetNumNonsampledZCs(S, 0);
    
    ssSetSimStateCompliance(S, USE_CUSTOM_SIM_STATE);
    
    ssSetOptions(S, 0);
}



/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    This function is used to specify the sample time(s) for your
 *    S-function. You must register the same number of sample times as
 *    specified in ssSetNumSampleTimes.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, mxGetScalar(ssGetSFcnParam(S, 0)));
    ssSetOffsetTime(S, 0, 0.0);
    ssSetModelReferenceSampleTimeDefaultInheritance(S);
}

#define MDL_START  /* Change to #undef to remove function */
#if defined(MDL_START)
/* Function: mdlStart =======================================================
 * Abstract:
 *    This function is called once at start of model execution. If you
 *    have states that should be initialized once, this is the place
 *    to do it.
 */
static void mdlStart(SimStruct *S)
{
    ssGetPWork(S)[0] = (void *) new counter; // store new C++ object in the
}                                            // pointers vector
#endif /*  MDL_START */

/* Function: mdlOutputs =======================================================
 * Abstract:
 *    In this function, you compute the outputs of your S-function
 *    block.
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    counter *c = (counter *) ssGetPWork(S)[0];   // retrieve C++ object from
    real_T  *y = ssGetOutputPortRealSignal(S,0); // the pointers vector and use
    y[0] = c->getPress();                          // member functions of the
    //y[0] = c->getX();
    UNUSED_ARG(tid);                             // object
}

#ifdef MATLAB_MEX_FILE
/* For now mdlG[S]etSimState are only supported in normal simulation */

/* Define to indicate that this S-Function has the mdlG[S]etSimState mothods */
#define MDL_SIM_STATE

/* Function: mdlGetSimState =====================================================
 * Abstract:
 *
 */
static mxArray* mdlGetSimState(SimStruct* S)
{
    counter* c = (counter*) ssGetPWork(S)[0];
    mxArray* outSS = mxCreateDoubleMatrix(1,1,mxREAL);
    mxGetPr(outSS)[0] = c->getX();
    return outSS;
}
/* Function: mdlGetSimState =====================================================
 * Abstract:
 *
 */
static void mdlSetSimState(SimStruct* S, const mxArray* ma)
{
    counter* c = (counter*) ssGetPWork(S)[0];
    c->setX(mxGetPr(ma)[0]);
}

#endif


/* Function: mdlTerminate =====================================================
 * Abstract:
 *    In this function, you should perform any actions that are necessary
 *    at the termination of a simulation.  For example, if memory was
 *    allocated in mdlStart, this is the place to free it.
 */
static void mdlTerminate(SimStruct *S)
{
    counter *c = (counter *) ssGetPWork(S)[0]; // retrieve and destroy C++
    delete c;                                  // object in the termination
}                                              // function
/*======================================================*
 * See sfuntmpl.doc for the optional S-function methods *
 *======================================================*/

/*=============================*
 * Required S-function trailer *
 *=============================*/

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif

