#ifndef RTW_HEADER_struct_BCyM1TQF0epB1PvNrxpCnF_h_
#define RTW_HEADER_struct_BCyM1TQF0epB1PvNrxpCnF_h_
#include "struct_Nj98fhwEUm6O969J2tt7yD.h"
#include "struct_9qZLCMMOhVY4AxrMQejy0.h"
#include "struct_fqoitRciEBSWbMiMs1xp6G.h"
#include "struct_LTw73MkwrOKgjTdKqzeYAH.h"
#include "struct_jqZJIiVwXC2PGh1UGLYaAD.h"
#include "struct_pXLF1SG7JVIGRnIbnNXxpC.h"
#include "struct_8LDeTuq9MAK8ojrlDlByvH.h"
#include "struct_KXOvmy8ymuqsyffV7WWpcG.h"
#include "struct_Se3YeAXB2onI5ODlwrYQo.h"
#include "rtwtypes.h"

typedef struct {
  real_T slave_alias;
  real_T slave_position;
  real_T debug_address_index;
  struct_Nj98fhwEUm6O969J2tt7yD amp;
  struct_9qZLCMMOhVY4AxrMQejy0 control_current;
  struct_fqoitRciEBSWbMiMs1xp6G control_motor_angle;
  struct_LTw73MkwrOKgjTdKqzeYAH control_quadrature_2_angle;
  struct_jqZJIiVwXC2PGh1UGLYaAD control_analog;
  struct_pXLF1SG7JVIGRnIbnNXxpC motor;
  struct_8LDeTuq9MAK8ojrlDlByvH quadrature_1;
  struct_8LDeTuq9MAK8ojrlDlByvH quadrature_2;
  struct_KXOvmy8ymuqsyffV7WWpcG ssi;
  struct_KXOvmy8ymuqsyffV7WWpcG analog_1;
  struct_KXOvmy8ymuqsyffV7WWpcG analog_2;
  struct_KXOvmy8ymuqsyffV7WWpcG analog_diff;
  struct_Se3YeAXB2onI5ODlwrYQo bridge;
  real_T fingerprint[2000];
} struct_BCyM1TQF0epB1PvNrxpCnF;

#endif                                 /* RTW_HEADER_struct_BCyM1TQF0epB1PvNrxpCnF_h_ */
