#ifndef RTW_HEADER_struct_Nj98fhwEUm6O969J2tt7yD_h_
#define RTW_HEADER_struct_Nj98fhwEUm6O969J2tt7yD_h_
#include "struct_RIpY2xbTXEXKeuue36idi.h"
#include "rtwtypes.h"

typedef struct {
  real_T current_zero_a;
  real_T current_zero_b;
  real_T deadband;
  real_T led_brightness;
  real_T nominal_bus_voltage;
  real_T current_sensor_limit;
  struct_RIpY2xbTXEXKeuue36idi logger_10kHz;
  struct_RIpY2xbTXEXKeuue36idi logger_100kHz;
} struct_Nj98fhwEUm6O969J2tt7yD;

#endif                                 /* RTW_HEADER_struct_Nj98fhwEUm6O969J2tt7yD_h_ */
