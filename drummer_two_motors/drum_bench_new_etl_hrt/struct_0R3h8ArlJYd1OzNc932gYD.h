#ifndef RTW_HEADER_struct_0R3h8ArlJYd1OzNc932gYD_h_
#define RTW_HEADER_struct_0R3h8ArlJYd1OzNc932gYD_h_
#include "rtwtypes.h"

typedef struct {
  real_T master;
  real_T domain;
  real_T alias;
  real_T position;
} struct_0R3h8ArlJYd1OzNc932gYD;

#endif                                 /* RTW_HEADER_struct_0R3h8ArlJYd1OzNc932gYD_h_ */
