/*
 * drum_bench_new_types.h
 *
 * Code generation for model "drum_bench_new".
 *
 * Model version              : 1.418
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Mon Mar 10 19:24:22 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_drum_bench_new_types_h_
#define RTW_HEADER_drum_bench_new_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#include "struct_Kzi606PJBfEKmP4ngorXDD.h"
#include "struct_0R3h8ArlJYd1OzNc932gYD.h"
#ifndef _DEFINED_TYPEDEF_FOR_MarlinFullCommand_
#define _DEFINED_TYPEDEF_FOR_MarlinFullCommand_

typedef struct {
  real_T mode;
  real_T quadrature_1_angle;
  real_T quadrature_1_angle_dot;
  real_T quadrature_2_angle;
  real_T quadrature_2_angle_dot;
  real_T analog_diff;
  real_T analog_diff_dot;
  real_T motor_torque;
  real_T motor_iq;
  real_T motor_id;
  real_T motor_vq;
  real_T motor_vd;
  real_T rpc;
} MarlinFullCommand;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_MarlinBasicStatus_
#define _DEFINED_TYPEDEF_FOR_MarlinBasicStatus_

typedef struct {
  real_T motor_torque;
  real_T motor_iq;
  real_T motor_vq_avg;
  real_T analog_1;
  real_T analog_1_dot;
  real_T analog_2;
  real_T analog_2_dot;
  real_T analog_diff;
  real_T analog_diff_dot;
  real_T quadrature_1;
  real_T quadrature_1_dot;
  real_T quadrature_2;
  real_T quadrature_2_dot;
  real_T ssi;
  real_T ssi_dot;
  real_T accelerometer_x;
  real_T accelerometer_y;
  real_T accelerometer_z;
  uint32_T faults;
  real_T bus_v;
  uint8_T rpa_packet[28];
} MarlinBasicStatus;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_MarlinFullStatus_
#define _DEFINED_TYPEDEF_FOR_MarlinFullStatus_

typedef struct {
  MarlinBasicStatus marlin_basic_status;
  real_T temperature_motor_winding;
  real_T temperature_motor_housing;
} MarlinFullStatus;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_MarlinBasicFaults_
#define _DEFINED_TYPEDEF_FOR_MarlinBasicFaults_

typedef struct {
  boolean_T soft_fault;
  boolean_T hard_fault;
  boolean_T communication_fault;
  boolean_T external_panic;
  boolean_T current;
  boolean_T voltage;
  boolean_T analog;
  boolean_T quadrature;
  boolean_T ethercat;
  boolean_T temperature;
  boolean_T ssi;
} MarlinBasicFaults;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_MarlinBasicRPC_
#define _DEFINED_TYPEDEF_FOR_MarlinBasicRPC_

typedef struct {
  uint8_T rpc[28];
} MarlinBasicRPC;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_MarlinBasicCommand_
#define _DEFINED_TYPEDEF_FOR_MarlinBasicCommand_

typedef struct {
  uint32_T mode;
  uint8_T command[56];
} MarlinBasicCommand;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_MarlinFullInternalRPC_
#define _DEFINED_TYPEDEF_FOR_MarlinFullInternalRPC_

typedef struct {
  int32_T type;
  real32_T id;
  uint8_T data[20];
} MarlinFullInternalRPC;

#endif

#ifndef _DEFINED_TYPEDEF_FOR_MarlinFullInternalRPA_
#define _DEFINED_TYPEDEF_FOR_MarlinFullInternalRPA_

typedef struct {
  int32_T type;
  real32_T id;
  uint8_T data[20];
} MarlinFullInternalRPA;

#endif

/* Parameters (auto storage) */
typedef struct P_drum_bench_new_T_ P_drum_bench_new_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_drum_bench_new_T RT_MODEL_drum_bench_new_T;

#endif                                 /* RTW_HEADER_drum_bench_new_types_h_ */
