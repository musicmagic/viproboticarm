#ifndef RTW_HEADER_struct_bL7nOhahIPgI0X3SLeRjmE_h_
#define RTW_HEADER_struct_bL7nOhahIPgI0X3SLeRjmE_h_
#include "rtwtypes.h"

typedef struct {
  real_T kd;
  real_T ki;
  real_T ki_limit;
  real_T kp;
} struct_bL7nOhahIPgI0X3SLeRjmE;

#endif                                 /* RTW_HEADER_struct_bL7nOhahIPgI0X3SLeRjmE_h_ */
