# Copyright 2012 IgH Essen GmbH
#
# Abstract:
#	Real-Time Workshop template makefile for building a
#	RTAI Kernel Module of a Simulink model
#
# 	This makefile attempts to conform to the guidelines specified in the
# 	IEEE Std 1003.2-1992 (POSIX) standard. It is designed to be used
#       with GNU Make which is located in matlabroot/rtw/bin.
#
# 	Note that this template is automatically customized by the Real-Time
#	Workshop build procedure to create "<model>.mk"
#
#       The following defines can be used to modify the behavior of the
#	build:
#	  OPT_OPTS       - Optimization options. Default is -O. To enable
#                          debugging specify as OPT_OPTS=-g.
#                          Because of optimization problems in IBM_RS,
#                          default is no-optimization.
#	  CPP_OPTS       - C++ compiler options.
#	  OPTS           - User specific compile options.
#	  USER_SRCS      - Additional user sources, such as files needed by
#			   S-functions.
#	  USER_INCLUDES  - Additional include paths
#			   (i.e. USER_INCLUDES="-Iwhere-ever -Iwhere-ever2")
#
#       This template makefile is designed to be used with a system target
#       file that contains 'rtwgensettings.BuildDirSuffix' see grt.tlc

#------------------------ Macros read by make_rtw ------------------------------
#
# The following macros are read by the Real-Time Workshop build procedure:
#
#  MAKECMD         - This is the command used to invoke the make utility
#  HOST            - What platform this template makefile is targeted for
#                    (i.e. PC or UNIX)
#  BUILD           - Invoke make from the Real-Time Workshop build procedure
#                    (yes/no)?
#  SYS_TARGET_FILE - Name of system target file.

MAKECMD         = make
HOST            = UNIX
BUILD           = yes
SYS_TARGET_FILE = etherlab.tlc

#---------------------- Tokens expanded by make_rtw ----------------------------
#
# The following tokens, when wrapped with "|>" and "<|" are expanded by the
# Real-Time Workshop build procedure.
#
#  MODEL_NAME          - Name of the Simulink block diagram
#  MODEL_MODULES       - Any additional generated source modules
#  MAKEFILE_NAME       - Name of makefile created from template makefile <model>.mk
#  MATLAB_ROOT         - Path to were MATLAB is installed.
#  S_FUNCTIONS         - List of S-functions.
#  S_FUNCTIONS_LIB     - List of S-functions libraries to link.
#  SOLVER              - Solver source file name
#  NUMST               - Number of sample times
#  TID01EQ             - yes (1) or no (0): Are sampling rates of continuous task
#                        (tid=0) and 1st discrete task equal.
#  NCSTATES            - Number of continuous states
#  COMPUTER            - Computer type. See the MATLAB computer command.
#  BUILDARGS           - Options passed in at the command line.
#  MULTITASKING        - yes (1) or no (0): Is solver mode multitasking
#  EXT_MODE            - yes (1) or no (0): Build for external mode
#  TMW_EXTMODE_TESTING - yes (1) or no (0): Build ext_test.c for external mode
#                        testing.
#  EXTMODE_TRANSPORT   - Index of transport mechanism (e.g. tcpip, serial) for extmode
#  EXTMODE_STATIC      - yes (1) or no (0): Use static instead of dynamic mem alloc.
#  EXTMODE_STATIC_SIZE - Size of static memory allocation buffer.
#  GEN_MDLREF_SFCN     - (1/0): are we generating model reference wrapper s-function
#  TGT_FCN_LIB         - Target Funtion library to use
#  MODELREFS           - List of referenced models

MODEL                = drum_bench_new
MODULES              = capi_helper.c drum_bench_new_capi.c drum_bench_new_data.c model_params_and_yaml.c rtGetInf.c rtGetNaN.c rt_matrx.c rt_nonfinite.c rt_printf.c rt_zcfcn.c yaml_helper.c 
MAKEFILE             = drum_bench_new.mk
MATLAB_ROOT          = /usr/local/MATLAB/R2013b
ALT_MATLAB_ROOT      = /usr/local/MATLAB/R2013b
MATLAB_BIN           = /usr/local/MATLAB/R2013b/bin
MASTER_ANCHOR_DIR    = 
START_DIR            = /home/meka/meka/rsc-drummer/drummer_two_motors
S_FUNCTIONS          = keyboard_press.cpp networking.cpp EMG.cpp behavior_controller.cpp write_yaml_params_wrapper.c ecrt_support.c
S_FUNCTIONS_LIB      = /usr/lib/x86_64-linux-gnu/libyaml.a /usr/lib/x86_64-linux-gnu/libpthread.so
SOLVER               = 
NUMST                = 6
TID01EQ              = 1
NCSTATES             = 0
COMPUTER             = GLNXA64
BUILDARGS            =  EXT_MODE=0 EXTMODE_STATIC_ALLOC=0 EXTMODE_STATIC_ALLOC_SIZE=1000000 EXTMODE_TRANSPORT=0 MAT_FILE=0
MULTITASKING         = 0
TMW_EXTMODE_TESTING  = 0
MEXEXT               = mexa64
TGT_FCN_LIB          = C89/C90 (ANSI)
MODELREFS            = 
SHARED_SRC           = 
SHARED_SRC_DIR       = 
SHARED_BIN_DIR       = 
SHARED_LIB           = 
TARGET_LANG_EXT      = c
OPTIMIZATION_FLAGS   = 
ADDITIONAL_LDFLAGS   = 

# To enable debugging:
# set DEBUG_BUILD = 1
DEBUG_BUILD          = 0

OVERRUNMAX	     = 1
BUFFER_TIME	     = 2
STACKSIZE            = 2000
VERBOSE              = 0
ETHERLAB_DIR         = /opt/etherlab
MODULE_PAYLOAD       = untitled.mdl 
OPT_OPTS             = -O2


META                 = $(MODEL)_meta.txt
PROPERTIES           = $(MODEL)_properties.c

#--------------------------- Model and reference models -----------------------
MODELLIB                  = drum_bench_newlib.a
MODELREF_LINK_LIBS        = 
MODELREF_INC_PATH         = 
RELATIVE_PATH_TO_ANCHOR   = ..
# NONE: standalone, SIM: modelref sim, RTW: modelref rtw
MODELREF_TARGET_TYPE       = NONE

#-- In the case when directory name contains space ---
ifneq ($(MATLAB_ROOT),$(ALT_MATLAB_ROOT))
MATLAB_ROOT := $(ALT_MATLAB_ROOT)
endif

#--------------------------- Tool Specifications -------------------------------

include $(MATLAB_ROOT)/rtw/c/tools/unixtools.mk

include $(ETHERLAB_DIR)/rtw/etherlab/etherlab_tools.mk

#------------------------------ Include Path -----------------------------------

MATLAB_INCLUDES = \
	-I$(MATLAB_ROOT)/simulink/include \
	-I$(MATLAB_ROOT)/extern/include \
	-I$(MATLAB_ROOT)/rtw/c/src \
	-I$(MATLAB_ROOT)/rtw/c/src/ext_mode/common

# Additional file include paths
ADD_INCLUDES = \
	-I$(START_DIR)/drum_bench_new_etl_hrt \
	-I$(START_DIR) \
	-I$(START_DIR)/../../rsc-base/simulink/library/c \
	-I$(MATLAB_ROOT)/rtw/c/src \
	-I/usr/lib/x86_64-linux-gnu \
	-I/usr/include \
	-I/home/meka/meka/rsc-base/simulink/library/c \
	-I$(MATLAB_ROOT)/toolbox/dsp/include \
	-I$(MATLAB_ROOT)/toolbox/dsp/extern/src/export/include/src \
	-I$(MATLAB_ROOT)/toolbox/dsp/extern/src/export/include \
	-I$(MATLAB_ROOT)/toolbox/shared/dsp/vision/matlab/include \


SHARED_INCLUDES =
ifneq ($(SHARED_SRC_DIR),)
SHARED_INCLUDES = -I$(SHARED_SRC_DIR)
endif

INCLUDES = -I. -I$(RELATIVE_PATH_TO_ANCHOR) $(MATLAB_INCLUDES) \
   $(ADD_INCLUDES) $(USER_INCLUDES) $(INSTRUMENT_INCLUDES) \
   $(MODELREF_INC_PATH) $(SHARED_INCLUDES) \
   -I$(ETHERLAB_DIR)/rtw/include -I$(ETHERLAB_DIR)/include

#----------------------------- External mode -----------------------------------
# Uncomment -DVERBOSE to have information printed to stdout
# To add a new transport layer, see the comments in
#   <matlabroot>/toolbox/simulink/simulink/extmode_transports.m
#ifeq ($(EXT_MODE),1)
#  EXT_CC_OPTS = -DEXT_MODE -D$(COMPUTER) #-DVERBOSE
#  EXT_LIB     =
#  EXT_SRC = ext_svr.c updown.c ext_work.c ext_svr_tcpip_transport.c
#  ifeq ($(TMW_EXTMODE_TESTING),1)
#    EXT_SRC     += ext_test.c
#    EXT_CC_OPTS += -DTMW_EXTMODE_TESTING
#  endif
#  ifeq ($(EXTMODE_STATIC),1)
#    EXT_SRC             += mem_mgr.c
#    EXT_CC_OPTS         += -DEXTMODE_STATIC -DEXTMODE_STATIC_SIZE=$(EXTMODE_STATIC_SIZE)
#  endif
#endif

#----------------------------- Real-Time Model ---------------------------------
RTM_CC_OPTS = -DUSE_RTMODEL

#-------------------------------- C Flags --------------------------------------

# Optimization Options
ifndef OPT_OPTS
OPT_OPTS = $(DEFAULT_OPT_OPTS)
endif

# General User Options
ifeq ($(DEBUG_BUILD),0)
OPTS =
else
#   Set OPTS=-g and any additional flags for debugging
OPTS = -g
LDFLAGS += -g
endif

# Compiler options, etc:
ifneq ($(OPTIMIZATION_FLAGS),)
CC_OPTS = $(OPTS) $(EXT_CC_OPTS) $(RTM_CC_OPTS) $(OPTIMIZATION_FLAGS)
else
CC_OPTS = $(OPT_OPTS) $(OPTS) $(EXT_CC_OPTS) $(RTM_CC_OPTS)
endif

CPP_REQ_DEFINES = -DMODEL="$(MODEL)" -DRT -DNUMST="$(NUMST)" \
                  -DTID01EQ="$(TID01EQ)" -DNCSTATES="$(NCSTATES)" -DUNIX \
                  -DMULTITASKING="$(MULTITASKING)"

RTW_OPTS = -DOVERRUNMAX="$(OVERRUNMAX)" -DBUFFER_TIME="$(BUFFER_TIME)" \
           -DSTACKSIZE="$(STACKSIZE)"

# Rob commented file dependency CFLAGS out, since the . prefix doesn't work well 
#     when compiling the shared lib. Doesn't seem to break anything so far...
#DEP = -Wp,-MMD,.$@.dep
DEP =

CFLAGS   = $(DEP) $(CC_OPTS) $(CPP_REQ_DEFINES) $(RTW_OPTS) $(INCLUDES)
CPPFLAGS = $(DEP) $(CPP_OPTS) $(CC_OPTS) $(CPP_REQ_DEFINES) $(RTW_OPTS) $(INCLUDES)
#-------------------------- Additional Libraries ------------------------------

SYSLIBS = -rdynamic $(EXT_LIB) -lm -ldl -lrt -lpdserv -L$(ETHERLAB_DIR)/lib \
        -Wl,--rpath $(ETHERLAB_DIR)/lib

ifneq ($(findstring rtipc.c,$(S_FUNCTIONS)),)
    SYSLIBS += -lrtipc
    S_FUNCTIONS := $(filter-out rtipc.c,$(S_FUNCTIONS))
endif

ifneq ($(findstring ecrt_support.c,$(S_FUNCTIONS)),)
    SYSLIBS += -lethercat
endif

LIBS =
 
LIBS += $(S_FUNCTIONS_LIB) $(INSTRUMENT_LIBS)

#ifneq ($(PRECOMP_RTW_LIB),)
#LIBS := $(filter-out rtwlib.a,$(LIBS)) $(PRECOMP_RTW_LIB)
#endif

#----------------------------- Source Files ------------------------------------

USER_SRCS =

USER_OBJS       = $(addsuffix .o, $(basename $(USER_SRCS)))
LOCAL_USER_OBJS = $(notdir $(USER_OBJS))

SRCS = $(MODULES) $(S_FUNCTIONS)

ifeq ($(MODELREF_TARGET_TYPE), NONE)
    PRODUCT            = $(RELATIVE_PATH_TO_ANCHOR)/$(MODEL)
    BIN_SETTING        = $(LD) $(LDFLAGS) $(ADDITIONAL_LDFLAGS) -o $(PRODUCT)
    BUILD_PRODUCT_TYPE = "executable"
    SRCS               += $(MODEL).$(TARGET_LANG_EXT) $(EXT_SRC) $(SOLVER) \
                          hrt_main_meka.c rt_sim.c $(MODEL)_header.c
else
    # Model reference coder target
    PRODUCT            = $(MODELLIB)
    BUILD_PRODUCT_TYPE = "library"
endif

ifneq ($(findstring .cpp,$(suffix $(SRCS), $(USER_SRCS))),)
  LD = $(CPP)
endif

OBJS      = $(addsuffix .o, $(basename $(SRCS))) $(USER_OBJS)
LINK_OBJS = $(addsuffix .o, $(basename $(SRCS))) $(LOCAL_USER_OBJS)

SHARED_SRC := $(wildcard $(SHARED_SRC))
SHARED_OBJS = $(addsuffix .o, $(basename $(SHARED_SRC)))

# Add RELATIVE_PATH_TO_ANCHOR to non-absolute files
MODULE_PAYLOAD := $(filter /%,$(MODULE_PAYLOAD)) \
                  $(addprefix $(RELATIVE_PATH_TO_ANCHOR)/,\
                          $(filter-out /%,$(MODULE_PAYLOAD)))

# Filter out files that do not exist
MODULE_PAYLOAD := $(wildcard $(MODULE_PAYLOAD))

#------------- Test Compile using gcc -Wall to look for warnings ---------------
#
# DO_GCC_TEST=1 runs gcc with compiler warning flags on all the source files
# used in this build. This includes the generated code, and any user source
# files needed for the build and placed in this directory.
#
# WARN_ON_GLNX=1 runs the linux compiler with warnings flags. On hand-written
# code we use the max amount of flags availabe while on the generated code, a
# few less.
#
# See rtw/c/tools/unixtools.mk for the definition of GCC_WARN_OPTS
GCC_TEST_CMD  := echo
GCC_TEST_OUT  := > /dev/null
ifeq ($(DO_GCC_TEST), 1)
  GCC_TEST := gcc -c -o /dev/null $(GCC_WARN_OPTS_MAX) $(CPP_REQ_DEFINES) \
                                     $(INCLUDES)
  GCC_TEST_CMD := echo; echo "\#\#\# GCC_TEST $(GCC_TEST) $<"; $(GCC_TEST)
  GCC_TEST_OUT := ; echo
endif

# Pulled out by Rob because @echo is inserted at line start for all rules.
cc  = $(CC)
cpp = $(CPP)
ld  = $(LD)
ar  = $(AR)


#ifeq ($(VERBOSE),1)
#cc  = $(CC)
#cpp = $(CPP)
#ld  = $(LD)
#ar  = $(AR)
#else
#cc  = @echo "  CC\t" $@ && $(CC)
#cpp = @echo "  C++\t" $@ && $(CPP)
#ld  = @echo "  LD\t" $@ && $(LD)
#ar  = @echo "  AR\t" $@ && $(AR)
#endif

#--------------------------------- Rules ---------------------------------------
all : preload Makefile $(PRODUCT)

preload :
#	sanitize_capi.sh $(MODEL)
	mv $(MODEL)_capi.c $(MODEL)_capi.c.old
	sed 's/<\([a-z0-9_]*\)>/\1/' <$(MODEL)_capi.c.old >$(MODEL)_capi.c

ifeq ($(MODELREF_TARGET_TYPE),NONE)
$(PRODUCT) : $(OBJS) $(SHARED_LIB) $(LIBS) $(MODELREF_LINK_LIBS)
	LD_LIBRARY_PATH= $(BIN_SETTING) $(LINK_OBJS) \
	        $(MODELREF_LINK_LIBS) $(SHARED_LIB) $(LIBS) $(SYSLIBS)
	@echo "### Created $(BUILD_PRODUCT_TYPE): $@"
else
$(PRODUCT) : $(OBJS) $(SHARED_LIB)
	@rm -f $(MODELLIB)
	ar ruvs $(MODELLIB) $(LINK_OBJS)
	@echo "### Created $(MODELLIB)"
	@echo "### Created $(BUILD_PRODUCT_TYPE): $@"
endif

#-------------------------------- Depencencies -------------------------------
#DEPFILES := $(wildcard .*.dep)
ifneq ($(DEPFILES),)
include $(DEPFILES)
endif

# Generate model symbol file
module_payload.c: $(MODULE_PAYLOAD)
	@echo Generating $@
	@LD_LIBRARY_PATH= $(ETHERLAB_DIR)/rtw/bin/gen_payload.py $^ > $@

Makefile: $(MAKEFILE)
	ln -s $< $@

ifneq ($(SHARED_SRC_DIR),)
$(SHARED_BIN_DIR)/%.o : $(SHARED_SRC_DIR)/%.c
	cd $(SHARED_BIN_DIR); $(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) $(notdir $?)

$(SHARED_BIN_DIR)/%.o : $(SHARED_SRC_DIR)/%.cpp
	cd $(SHARED_BIN_DIR); $(cpp) -c $(CPPFLAGS) $(GCC_WALL_FLAG_MAX) $(notdir $?)
endif

%.o : %.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG) "$<"

%.o : %.cpp
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cpp) -c $(CPPFLAGS) $(GCC_WALL_FLAG) "$<"

%.o : $(RELATIVE_PATH_TO_ANCHOR)/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG) "$<"

%.o : $(RELATIVE_PATH_TO_ANCHOR)/%.cpp
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cpp) -c $(CPPFLAGS) "$<"

ifeq ($(TARGET_LANG_EXT),cpp)
%.o : $(MATLAB_ROOT)/rtw/c/grt/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cpp) -c $(CPPFLAGS) $(GCC_WALL_FLAG_MAX) "$<"
else
%.o : $(MATLAB_ROOT)/rtw/c/grt/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) "$<"
endif

%.o : $(ETHERLAB_DIR)/rtw/src/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) "$<"

%.o : $(MATLAB_ROOT)/rtw/c/src/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) "$<"

%.o : $(MATLAB_ROOT)/rtw/c/src/ext_mode/common/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) "$<"

%.o : $(MATLAB_ROOT)/rtw/c/src/rtiostream/rtiostreamtcpip/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) "$<"

%.o : $(MATLAB_ROOT)/rtw/c/src/ext_mode/custom/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) "$<"

%.o : /home/meka/meka/rsc-base/simulink/library/c/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) "$<"

%.o : $(MATLAB_ROOT)/rtw/c/src/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) "$<"

%.o : $(MATLAB_ROOT)/simulink/src/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) "$<"

%.o : /opt/etherlab/rtw/blocks/EtherCAT/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) "$<"



%.o : /home/meka/meka/rsc-base/simulink/library/c/%.cpp
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cpp) -c $(CPPFLAGS) "$<"
%.o : $(MATLAB_ROOT)/rtw/c/src/%.cpp
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cpp) -c $(CPPFLAGS) "$<"
%.o : $(MATLAB_ROOT)/simulink/src/%.cpp
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cpp) -c $(CPPFLAGS) "$<"
%.o : /opt/etherlab/rtw/blocks/EtherCAT/%.cpp
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cpp) -c $(CPPFLAGS) "$<"


%.o : $(MATLAB_ROOT)/simulink/src/%.c
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) "$<"

%.o : $(MATLAB_ROOT)/simulink/src/%.cpp
	@$(GCC_TEST_CMD) "$<" $(GCC_TEST_OUT)
	$(cpp) -c $(CPPFLAGS) "$<"

#------------------------------- Libraries -------------------------------------





#----------------------------- Dependencies ------------------------------------

$(LINK_OBJS) : $(MAKEFILE) rtw_proj.tmw

#$(SHARED_LIB) : $(SHARED_OBJS)
#	@echo "### Creating Shared Library $@ "
#	cd $(SHARED_BIN_DIR); $(cc) -c $(CFLAGS) $(GCC_WALL_FLAG_MAX) $(notdir $?)
#	ar ruvs $@ $(SHARED_OBJS)
#	@echo "### $@ Created  "

$(SHARED_LIB) : $(SHARED_OBJS)
	      @echo "### Creating Shared Library $@"
	      ar r $@ $(SHARED_OBJS)
	      @echo "### Created $@"

$(SHARED_OBJS) : $(SHARED_BIN_DIR)/%.o : $(SHARED_SRC_DIR)/%.c
	       $(cc) -c $(CFLAGS) -o $@ $<

#--------- Miscellaneous rules to purge, clean ---------------------------------

purge : clean
	@echo "### Deleting the generated source code for $(MODEL)"
	@\rm -f $(MODEL).c $(MODEL).h $(MODEL)_types.h $(MODEL)_data.c \
	        $(MODEL)_private.h $(MODEL).rtw $(MODULES) rtw_proj.tmw \
                $(MAKEFILE) $(META) $(PROPERTIES) Makefile

clean :
	@echo "### Deleting the objects and $(PRODUCT)"
	@\rm -f $(LINK_OBJS) $(PRODUCT) $(MODELLIB) .*.dep

# EOF: etherlab_hrt.tmf
