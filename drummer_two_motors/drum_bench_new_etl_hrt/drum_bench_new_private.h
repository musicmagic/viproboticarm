/*
 * drum_bench_new_private.h
 *
 * Code generation for model "drum_bench_new".
 *
 * Model version              : 1.418
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Mon Mar 10 19:24:22 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_drum_bench_new_private_h_
#define RTW_HEADER_drum_bench_new_private_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#ifndef UCHAR_MAX
#include <limits.h>
#endif

#if ( UCHAR_MAX != (0xFFU) ) || ( SCHAR_MAX != (0x7F) )
#error "Code was generated for compiler with different sized uchar/char. Consider adjusting Emulation Hardware word size settings on the Hardware Implementation pane to match your compiler word sizes as defined in the compiler's limits.h header file. Alternatively, you can select 'None' for Emulation Hardware and select the 'Enable portable word sizes' option for ERT based targets, which will disable the preprocessor word size checks."
#endif

#if ( USHRT_MAX != (0xFFFFU) ) || ( SHRT_MAX != (0x7FFF) )
#error "Code was generated for compiler with different sized ushort/short. Consider adjusting Emulation Hardware word size settings on the Hardware Implementation pane to match your compiler word sizes as defined in the compilers limits.h header file. Alternatively, you can select 'None' for Emulation Hardware and select the 'Enable portable word sizes' option for ERT based targets, this will disable the preprocessor word size checks."
#endif

#if ( UINT_MAX != (0xFFFFFFFFU) ) || ( INT_MAX != (0x7FFFFFFF) )
#error "Code was generated for compiler with different sized uint/int. Consider adjusting Emulation Hardware word size settings on the Hardware Implementation pane to match your compiler word sizes as defined in the compilers limits.h header file. Alternatively, you can select 'None' for Emulation Hardware and select the 'Enable portable word sizes' option for ERT based targets, this will disable the preprocessor word size checks."
#endif

#if 0

/* Skip this size verification because of preprocessor limitation */
#if ( ULONG_MAX != (0xFFFFFFFFFFFFFFFFUL) ) || ( LONG_MAX != (0x7FFFFFFFFFFFFFFFL) )
#error "Code was generated for compiler with different sized ulong/long. Consider adjusting Emulation Hardware word size settings on the Hardware Implementation pane to match your compiler word sizes as defined in the compilers limits.h header file. Alternatively, you can select 'None' for Emulation Hardware and select the 'Enable portable word sizes' option for ERT based targets, this will disable the preprocessor word size checks."
#endif
#endif

#ifndef __RTWTYPES_H__
#error This file requires rtwtypes.h to be included
#else
#ifdef TMWTYPES_PREVIOUSLY_INCLUDED
#error This file requires rtwtypes.h to be included before tmwtypes.h
#endif                                 /* TMWTYPES_PREVIOUSLY_INCLUDED */
#endif                                 /* __RTWTYPES_H__ */

extern void write_yaml_params_Outputs_wrapper();
extern real_T rt_roundd_snf(real_T u);
extern real_T rt_powd_snf(real_T u0, real_T u1);
extern void MWDSPCG_FFT_Interleave_R2BR_D(const real_T x[], creal_T y[], const
  int32_T nChans, const int32_T nRows);
extern void MWDSPCG_R2DIT_TBLS_Z(creal_T y[], const int32_T nChans, const
  int32_T nRows, const int32_T fftLen, const int32_T offset, const real_T
  tablePtr[], const int32_T twiddleStep, const boolean_T isInverse);
extern void MWDSPCG_FFT_DblLen_Z(creal_T y[], const int32_T nChans, const
  int32_T nRows, const real_T twiddleTable[], const int32_T twiddleStep);
extern void keyboard_press(SimStruct *rts);
extern void networking(SimStruct *rts);
extern void EMG(SimStruct *rts);
extern void behavior_controller(SimStruct *rts);
extern void drum_bench_new_mean(const real_T rtu_u[128], B_mean_drum_bench_new_T
  *localB);
extern void drum_bench_new_slopeChange_Init(DW_slopeChange_drum_bench_new_T
  *localDW);
extern void drum_bench_new_slopeChange(real_T rtu_u,
  B_slopeChange_drum_bench_new_T *localB, DW_slopeChange_drum_bench_new_T
  *localDW);
extern void drum_bench_new_smooth_Init(DW_smooth_drum_bench_new_T *localDW);
extern void drum_bench_new_smooth(real_T rtu_u, B_smooth_drum_bench_new_T
  *localB, DW_smooth_drum_bench_new_T *localDW);
extern void drum_bench_new_smooth1_Init(DW_smooth1_drum_bench_new_T *localDW);
extern void drum_bench_new_smooth1(real_T rtu_u, B_smooth1_drum_bench_new_T
  *localB, DW_smooth1_drum_bench_new_T *localDW);
extern void drum_bench_new_smooth3_Init(DW_smooth3_drum_bench_new_T *localDW);
extern void drum_bench_new_smooth3(real_T rtu_u, B_smooth3_drum_bench_new_T
  *localB, DW_smooth3_drum_bench_new_T *localDW);
extern void drum_bench_ne_zeroCrossing_Init(DW_zeroCrossing_drum_bench_ne_T
  *localDW);
extern void drum_bench_new_zeroCrossing(real_T rtu_u,
  B_zeroCrossing_drum_bench_new_T *localB, DW_zeroCrossing_drum_bench_ne_T
  *localDW);
extern void drum_be_FilteredDerivative_Init(DW_FilteredDerivative_drum_be_T
  *localDW);
extern void drum__FilteredDerivative_Update(B_FilteredDerivative_drum_ben_T
  *localB, DW_FilteredDerivative_drum_be_T *localDW);
extern void drum_bench_n_FilteredDerivative(real_T rtu_u,
  B_FilteredDerivative_drum_ben_T *localB, DW_FilteredDerivative_drum_be_T
  *localDW, real_T rtp_N);
extern void drum_ben_enabledsubsystem_Start(B_enabledsubsystem_drum_bench_T
  *localB);
extern void drum_bench_new_enabledsubsystem(boolean_T rtu_0, const uint8_T
  rtu_1[20], B_enabledsubsystem_drum_bench_T *localB);
extern void drum_ben_EnabledSubsystem_Start(B_EnabledSubsystem_drum_bench_T
  *localB);
extern void drum_bench_new_EnabledSubsystem(boolean_T rtu_0, const uint8_T
  rtu_1[20], B_EnabledSubsystem_drum_bench_T *localB);
extern void drum_bench_MATLABFunction1_Init(DW_MATLABFunction1_drum_bench_T
  *localDW);
extern void drum_bench_new_MATLABFunction1(const real32_T rtu_u[76], const
  MarlinFullInternalRPA *rtu_rpa, uint32_T rtu_rpc_type,
  B_MATLABFunction1_drum_bench__T *localB, DW_MATLABFunction1_drum_bench_T
  *localDW);
extern void drum_bench_new_selectrpc(real_T rtu_rpc_command, const
  MarlinFullInternalRPC rtu_u[15], B_selectrpc_drum_bench_new_T *localB);

#endif                                 /* RTW_HEADER_drum_bench_new_private_h_ */
