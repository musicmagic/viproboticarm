#ifndef RTW_HEADER_struct_gK2Jy8LweAj0bYVyYXESZG_h_
#define RTW_HEADER_struct_gK2Jy8LweAj0bYVyYXESZG_h_
#include "rtwtypes.h"

typedef struct {
  real_T eA1[4];
  real_T eA2[2];
  real_T temperature_allowable_motor_winding;
  real_T temperature_allowable_motor_housing;
} struct_gK2Jy8LweAj0bYVyYXESZG;

#endif                                 /* RTW_HEADER_struct_gK2Jy8LweAj0bYVyYXESZG_h_ */
