#ifndef RTW_HEADER_struct_fqoitRciEBSWbMiMs1xp6G_h_
#define RTW_HEADER_struct_fqoitRciEBSWbMiMs1xp6G_h_
#include "struct_QBpPZnk5niXRFhzN7Lj31C.h"
#include "struct_AAoDtaShZdSuxE8MfKAZuD.h"
#include "struct_bL7nOhahIPgI0X3SLeRjmE.h"
#include "rtwtypes.h"

typedef struct {
  struct_QBpPZnk5niXRFhzN7Lj31C pid_deadband;
  struct_AAoDtaShZdSuxE8MfKAZuD pid_interpolate;
  struct_bL7nOhahIPgI0X3SLeRjmE pid;
} struct_fqoitRciEBSWbMiMs1xp6G;

#endif                                 /* RTW_HEADER_struct_fqoitRciEBSWbMiMs1xp6G_h_ */
