#ifndef RTW_HEADER_struct_6bi3MChDTb2LN2fRyJtpm_h_
#define RTW_HEADER_struct_6bi3MChDTb2LN2fRyJtpm_h_
#include "struct_jkDUSheYYfvtbbR4tr6mvD.h"
#include "rtwtypes.h"

typedef struct {
  real_T mode;
  struct_jkDUSheYYfvtbbR4tr6mvD quadrature_1_angle;
  real_T quadrature_1_angle_dot;
  real_T quadrature_1_angle_dotdot;
  struct_jkDUSheYYfvtbbR4tr6mvD quadrature_2_angle;
  real_T quadrature_2_angle_dot;
  real_T quadrature_2_angle_dotdot;
  struct_jkDUSheYYfvtbbR4tr6mvD analog;
  real_T analog_dot;
  real_T motor_torque;
  struct_jkDUSheYYfvtbbR4tr6mvD motor_iq;
  real_T motor_id;
  struct_jkDUSheYYfvtbbR4tr6mvD motor_vq;
  real_T rpc_command;
  real_T motor_vd;
  real_T quadrature_1_angle_gain;
  real_T quadrature_1_angle_dot_gain;
  real_T analog_gain;
  real_T analog_dot_gain;
  real_T trigger_zero_bias;
} struct_6bi3MChDTb2LN2fRyJtpm;

#endif                                 /* RTW_HEADER_struct_6bi3MChDTb2LN2fRyJtpm_h_ */
