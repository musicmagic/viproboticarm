#ifndef RTW_HEADER_struct_INE22yG6rcc1ImNexAY7TC_h_
#define RTW_HEADER_struct_INE22yG6rcc1ImNexAY7TC_h_
#include "rtwtypes.h"

typedef struct {
  real_T counts_per_revolution;
} struct_INE22yG6rcc1ImNexAY7TC;

#endif                                 /* RTW_HEADER_struct_INE22yG6rcc1ImNexAY7TC_h_ */
