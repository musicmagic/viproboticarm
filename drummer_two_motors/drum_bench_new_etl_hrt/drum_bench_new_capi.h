/*
 * drum_bench_new_capi.h
 *
 * Code generation for model "drum_bench_new".
 *
 * Model version              : 1.418
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Mon Mar 10 19:24:22 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef _RTW_HEADER_drum_bench_new_capi_h_
#define _RTW_HEADER_drum_bench_new_capi_h_
#include "drum_bench_new.h"

extern void drum_bench_new_InitializeDataMapInfo(RT_MODEL_drum_bench_new_T *
  const drum_bench_new_M
  );

#endif                                 /* _RTW_HEADER_drum_bench_new_capi_h_ */

/* EOF: drum_bench_new_capi.h */
