#ifndef __c1_dsp_param_lib_h__
#define __c1_dsp_param_lib_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_MarlinFullInternalRPA_tag
#define struct_MarlinFullInternalRPA_tag

struct MarlinFullInternalRPA_tag
{
  int32_T type;
  real32_T id;
  uint8_T data[20];
};

#endif                                 /*struct_MarlinFullInternalRPA_tag*/

#ifndef typedef_c1_MarlinFullInternalRPA
#define typedef_c1_MarlinFullInternalRPA

typedef struct MarlinFullInternalRPA_tag c1_MarlinFullInternalRPA;

#endif                                 /*typedef_c1_MarlinFullInternalRPA*/

#ifndef typedef_SFc1_dsp_param_libInstanceStruct
#define typedef_SFc1_dsp_param_libInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c1_sfEvent;
  boolean_T c1_isStable;
  boolean_T c1_doneDoubleBufferReInit;
  uint8_T c1_is_active_c1_dsp_param_lib;
  real_T c1_i;
  boolean_T c1_i_not_empty;
  real32_T c1_id_sent;
  boolean_T c1_id_sent_not_empty;
  uint32_T c1_method;
  boolean_T c1_method_not_empty;
  uint32_T c1_state;
  boolean_T c1_state_not_empty;
  uint32_T c1_b_state[2];
  boolean_T c1_b_state_not_empty;
  uint32_T c1_c_state[625];
  boolean_T c1_c_state_not_empty;
} SFc1_dsp_param_libInstanceStruct;

#endif                                 /*typedef_SFc1_dsp_param_libInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c1_dsp_param_lib_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c1_dsp_param_lib_get_check_sum(mxArray *plhs[]);
extern void c1_dsp_param_lib_method_dispatcher(SimStruct *S, int_T method, void *
  data);

#endif
