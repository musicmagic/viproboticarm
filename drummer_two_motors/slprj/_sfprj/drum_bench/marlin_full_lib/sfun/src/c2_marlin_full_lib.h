#ifndef __c2_marlin_full_lib_h__
#define __c2_marlin_full_lib_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_MarlinFullInternalRPC_tag
#define struct_MarlinFullInternalRPC_tag

struct MarlinFullInternalRPC_tag
{
  int32_T type;
  real32_T id;
  uint8_T data[20];
};

#endif                                 /*struct_MarlinFullInternalRPC_tag*/

#ifndef typedef_c2_MarlinFullInternalRPC
#define typedef_c2_MarlinFullInternalRPC

typedef struct MarlinFullInternalRPC_tag c2_MarlinFullInternalRPC;

#endif                                 /*typedef_c2_MarlinFullInternalRPC*/

#ifndef typedef_SFc2_marlin_full_libInstanceStruct
#define typedef_SFc2_marlin_full_libInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c2_sfEvent;
  boolean_T c2_isStable;
  boolean_T c2_doneDoubleBufferReInit;
  uint8_T c2_is_active_c2_marlin_full_lib;
} SFc2_marlin_full_libInstanceStruct;

#endif                                 /*typedef_SFc2_marlin_full_libInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c2_marlin_full_lib_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c2_marlin_full_lib_get_check_sum(mxArray *plhs[]);
extern void c2_marlin_full_lib_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif
