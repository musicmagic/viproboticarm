#ifndef __c12_drum_bench_h__
#define __c12_drum_bench_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc12_drum_benchInstanceStruct
#define typedef_SFc12_drum_benchInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c12_sfEvent;
  boolean_T c12_isStable;
  boolean_T c12_doneDoubleBufferReInit;
  uint8_T c12_is_active_c12_drum_bench;
  real_T c12_i;
  boolean_T c12_i_not_empty;
  real_T c12_n;
  boolean_T c12_n_not_empty;
  real_T c12_buf[500];
  boolean_T c12_buf_not_empty;
} SFc12_drum_benchInstanceStruct;

#endif                                 /*typedef_SFc12_drum_benchInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c12_drum_bench_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c12_drum_bench_get_check_sum(mxArray *plhs[]);
extern void c12_drum_bench_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
