#ifndef __c10_drum_bench_h__
#define __c10_drum_bench_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc10_drum_benchInstanceStruct
#define typedef_SFc10_drum_benchInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c10_sfEvent;
  boolean_T c10_isStable;
  boolean_T c10_doneDoubleBufferReInit;
  uint8_T c10_is_active_c10_drum_bench;
  real_T c10_i;
  boolean_T c10_i_not_empty;
  real_T c10_n;
  boolean_T c10_n_not_empty;
  real_T c10_buf[1500];
  boolean_T c10_buf_not_empty;
} SFc10_drum_benchInstanceStruct;

#endif                                 /*typedef_SFc10_drum_benchInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c10_drum_bench_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c10_drum_bench_get_check_sum(mxArray *plhs[]);
extern void c10_drum_bench_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
