#ifndef __c14_drum_bench_h__
#define __c14_drum_bench_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc14_drum_benchInstanceStruct
#define typedef_SFc14_drum_benchInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c14_sfEvent;
  boolean_T c14_isStable;
  boolean_T c14_doneDoubleBufferReInit;
  uint8_T c14_is_active_c14_drum_bench;
  real_T c14_lastSign;
  boolean_T c14_lastSign_not_empty;
  real_T c14_output;
  boolean_T c14_output_not_empty;
} SFc14_drum_benchInstanceStruct;

#endif                                 /*typedef_SFc14_drum_benchInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c14_drum_bench_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c14_drum_bench_get_check_sum(mxArray *plhs[]);
extern void c14_drum_bench_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
