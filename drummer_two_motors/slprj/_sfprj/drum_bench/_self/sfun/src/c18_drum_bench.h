#ifndef __c18_drum_bench_h__
#define __c18_drum_bench_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc18_drum_benchInstanceStruct
#define typedef_SFc18_drum_benchInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c18_sfEvent;
  boolean_T c18_isStable;
  boolean_T c18_doneDoubleBufferReInit;
  uint8_T c18_is_active_c18_drum_bench;
} SFc18_drum_benchInstanceStruct;

#endif                                 /*typedef_SFc18_drum_benchInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c18_drum_bench_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c18_drum_bench_get_check_sum(mxArray *plhs[]);
extern void c18_drum_bench_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
