#ifndef __c5_drum_bench_h__
#define __c5_drum_bench_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc5_drum_benchInstanceStruct
#define typedef_SFc5_drum_benchInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c5_sfEvent;
  boolean_T c5_isStable;
  boolean_T c5_doneDoubleBufferReInit;
  uint8_T c5_is_active_c5_drum_bench;
  real_T c5_i;
  boolean_T c5_i_not_empty;
  real_T c5_n;
  boolean_T c5_n_not_empty;
  real_T c5_buf[500];
  boolean_T c5_buf_not_empty;
} SFc5_drum_benchInstanceStruct;

#endif                                 /*typedef_SFc5_drum_benchInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c5_drum_bench_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c5_drum_bench_get_check_sum(mxArray *plhs[]);
extern void c5_drum_bench_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
