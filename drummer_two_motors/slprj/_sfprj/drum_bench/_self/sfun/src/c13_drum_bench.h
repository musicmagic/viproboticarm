#ifndef __c13_drum_bench_h__
#define __c13_drum_bench_h__

/* Include files */
#include "sfc_sf.h"
#include "sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc13_drum_benchInstanceStruct
#define typedef_SFc13_drum_benchInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c13_sfEvent;
  boolean_T c13_isStable;
  boolean_T c13_doneDoubleBufferReInit;
  uint8_T c13_is_active_c13_drum_bench;
  real_T c13_i;
  boolean_T c13_i_not_empty;
  real_T c13_n;
  boolean_T c13_n_not_empty;
  real_T c13_buf[7];
  boolean_T c13_buf_not_empty;
} SFc13_drum_benchInstanceStruct;

#endif                                 /*typedef_SFc13_drum_benchInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c13_drum_bench_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c13_drum_bench_get_check_sum(mxArray *plhs[]);
extern void c13_drum_bench_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
