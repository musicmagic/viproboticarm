/* Include files */

#include <stddef.h>
#include "blas.h"
#include "marlin_full_lib_sfun.h"
#include "c2_marlin_full_lib.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "marlin_full_lib_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c(sfGlobalDebugInstanceStruct,S);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static const char * c2_debug_family_names[8] = { "active_id", "nargin",
  "nargout", "rpc_command", "u", "type", "id", "data" };

/* Function Declarations */
static void initialize_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct
  *chartInstance);
static void initialize_params_c2_marlin_full_lib
  (SFc2_marlin_full_libInstanceStruct *chartInstance);
static void enable_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct
  *chartInstance);
static void disable_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct
  *chartInstance);
static void c2_update_debugger_state_c2_marlin_full_lib
  (SFc2_marlin_full_libInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c2_marlin_full_lib
  (SFc2_marlin_full_libInstanceStruct *chartInstance);
static void set_sim_state_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct *
  chartInstance, const mxArray *c2_st);
static void finalize_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct
  *chartInstance);
static void sf_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct
  *chartInstance);
static void initSimStructsc2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct *
  chartInstance);
static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber);
static const mxArray *c2_sf_marshallOut(void *chartInstanceVoid, void *c2_inData);
static void c2_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_data, const char_T *c2_identifier, uint8_T
  c2_y[20]);
static void c2_b_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId,
  uint8_T c2_y[20]);
static void c2_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_b_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static real32_T c2_c_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_id, const char_T *c2_identifier);
static real32_T c2_d_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_c_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static int32_T c2_e_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_type, const char_T *c2_identifier);
static int32_T c2_f_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_d_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static const mxArray *c2_e_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static real_T c2_g_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static void c2_info_helper(const mxArray **c2_info);
static const mxArray *c2_emlrt_marshallOut(char * c2_u);
static const mxArray *c2_b_emlrt_marshallOut(uint32_T c2_u);
static const mxArray *c2_u_bus_io(void *chartInstanceVoid, void *c2_pData);
static uint8_T c2_h_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_b_is_active_c2_marlin_full_lib, const char_T
  *c2_identifier);
static uint8_T c2_i_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void init_dsm_address_info(SFc2_marlin_full_libInstanceStruct
  *chartInstance);

/* Function Definitions */
static void initialize_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct
  *chartInstance)
{
  chartInstance->c2_sfEvent = CALL_EVENT;
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
  chartInstance->c2_is_active_c2_marlin_full_lib = 0U;
}

static void initialize_params_c2_marlin_full_lib
  (SFc2_marlin_full_libInstanceStruct *chartInstance)
{
}

static void enable_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct
  *chartInstance)
{
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
}

static void disable_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct
  *chartInstance)
{
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
}

static void c2_update_debugger_state_c2_marlin_full_lib
  (SFc2_marlin_full_libInstanceStruct *chartInstance)
{
}

static const mxArray *get_sim_state_c2_marlin_full_lib
  (SFc2_marlin_full_libInstanceStruct *chartInstance)
{
  const mxArray *c2_st;
  const mxArray *c2_y = NULL;
  int32_T c2_i0;
  uint8_T c2_u[20];
  const mxArray *c2_b_y = NULL;
  real32_T c2_hoistedGlobal;
  real32_T c2_b_u;
  const mxArray *c2_c_y = NULL;
  int32_T c2_b_hoistedGlobal;
  int32_T c2_c_u;
  const mxArray *c2_d_y = NULL;
  uint8_T c2_c_hoistedGlobal;
  uint8_T c2_d_u;
  const mxArray *c2_e_y = NULL;
  real32_T *c2_id;
  int32_T *c2_type;
  uint8_T (*c2_data)[20];
  c2_data = (uint8_T (*)[20])ssGetOutputPortSignal(chartInstance->S, 3);
  c2_id = (real32_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c2_type = (int32_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c2_st = NULL;
  c2_st = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_createcellarray(4), FALSE);
  for (c2_i0 = 0; c2_i0 < 20; c2_i0++) {
    c2_u[c2_i0] = (*c2_data)[c2_i0];
  }

  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", c2_u, 3, 0U, 1U, 0U, 1, 20), FALSE);
  sf_mex_setcell(c2_y, 0, c2_b_y);
  c2_hoistedGlobal = *c2_id;
  c2_b_u = c2_hoistedGlobal;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_b_u, 1, 0U, 0U, 0U, 0), FALSE);
  sf_mex_setcell(c2_y, 1, c2_c_y);
  c2_b_hoistedGlobal = *c2_type;
  c2_c_u = c2_b_hoistedGlobal;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_c_u, 6, 0U, 0U, 0U, 0), FALSE);
  sf_mex_setcell(c2_y, 2, c2_d_y);
  c2_c_hoistedGlobal = chartInstance->c2_is_active_c2_marlin_full_lib;
  c2_d_u = c2_c_hoistedGlobal;
  c2_e_y = NULL;
  sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_d_u, 3, 0U, 0U, 0U, 0), FALSE);
  sf_mex_setcell(c2_y, 3, c2_e_y);
  sf_mex_assign(&c2_st, c2_y, FALSE);
  return c2_st;
}

static void set_sim_state_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct *
  chartInstance, const mxArray *c2_st)
{
  const mxArray *c2_u;
  uint8_T c2_uv0[20];
  int32_T c2_i1;
  real32_T *c2_id;
  int32_T *c2_type;
  uint8_T (*c2_data)[20];
  c2_data = (uint8_T (*)[20])ssGetOutputPortSignal(chartInstance->S, 3);
  c2_id = (real32_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c2_type = (int32_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  chartInstance->c2_doneDoubleBufferReInit = TRUE;
  c2_u = sf_mex_dup(c2_st);
  c2_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c2_u, 0)), "data",
                      c2_uv0);
  for (c2_i1 = 0; c2_i1 < 20; c2_i1++) {
    (*c2_data)[c2_i1] = c2_uv0[c2_i1];
  }

  *c2_id = c2_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c2_u,
    1)), "id");
  *c2_type = c2_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c2_u,
    2)), "type");
  chartInstance->c2_is_active_c2_marlin_full_lib = c2_h_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c2_u, 3)),
     "is_active_c2_marlin_full_lib");
  sf_mex_destroy(&c2_u);
  c2_update_debugger_state_c2_marlin_full_lib(chartInstance);
  sf_mex_destroy(&c2_st);
}

static void finalize_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct
  *chartInstance)
{
}

static void sf_c2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct
  *chartInstance)
{
  int32_T c2_i2;
  real_T c2_hoistedGlobal;
  real_T c2_rpc_command;
  int32_T c2_i3;
  c2_MarlinFullInternalRPC c2_u[15];
  int32_T c2_i4;
  uint32_T c2_debug_family_var_map[8];
  real_T c2_active_id;
  real_T c2_nargin = 2.0;
  real_T c2_nargout = 3.0;
  int32_T c2_type;
  real32_T c2_id;
  uint8_T c2_data[20];
  int32_T c2_i5;
  int32_T c2_i6;
  int32_T c2_i7;
  real_T *c2_b_rpc_command;
  int32_T *c2_b_type;
  real32_T *c2_b_id;
  uint8_T (*c2_b_data)[20];
  c2_MarlinFullInternalRPC (*c2_b_u)[15];
  boolean_T guard1 = FALSE;
  boolean_T guard2 = FALSE;
  c2_b_data = (uint8_T (*)[20])ssGetOutputPortSignal(chartInstance->S, 3);
  c2_b_id = (real32_T *)ssGetOutputPortSignal(chartInstance->S, 2);
  c2_b_type = (int32_T *)ssGetOutputPortSignal(chartInstance->S, 1);
  c2_b_u = (c2_MarlinFullInternalRPC (*)[15])ssGetInputPortSignal
    (chartInstance->S, 1);
  c2_b_rpc_command = (real_T *)ssGetInputPortSignal(chartInstance->S, 0);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = (real_T)ssGetT(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 0U, chartInstance->c2_sfEvent);
  _SFD_DATA_RANGE_CHECK(*c2_b_rpc_command, 0U);
  _SFD_DATA_RANGE_CHECK((real_T)*c2_b_type, 2U);
  _SFD_DATA_RANGE_CHECK((real_T)*c2_b_id, 3U);
  for (c2_i2 = 0; c2_i2 < 20; c2_i2++) {
    _SFD_DATA_RANGE_CHECK((real_T)(*c2_b_data)[c2_i2], 4U);
  }

  chartInstance->c2_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 0U, chartInstance->c2_sfEvent);
  c2_hoistedGlobal = *c2_b_rpc_command;
  c2_rpc_command = c2_hoistedGlobal;
  for (c2_i3 = 0; c2_i3 < 15; c2_i3++) {
    c2_u[c2_i3].type = *(int32_T *)((char_T *)(c2_MarlinFullInternalRPC *)
      ((char_T *)c2_b_u + 32 * c2_i3) + 0);
    c2_u[c2_i3].id = *(real32_T *)((char_T *)(c2_MarlinFullInternalRPC *)
      ((char_T *)c2_b_u + 32 * c2_i3) + 4);
    for (c2_i4 = 0; c2_i4 < 20; c2_i4++) {
      c2_u[c2_i3].data[c2_i4] = ((uint8_T *)((char_T *)(c2_MarlinFullInternalRPC
        *)((char_T *)c2_b_u + 32 * c2_i3) + 8))[c2_i4];
    }
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 8U, 8U, c2_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_active_id, 0U, c2_e_sf_marshallOut,
    c2_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 1U, c2_e_sf_marshallOut,
    c2_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 2U, c2_e_sf_marshallOut,
    c2_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c2_rpc_command, 3U, c2_e_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(c2_u, 4U, c2_d_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_type, 5U, c2_c_sf_marshallOut,
    c2_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_id, 6U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_data, 7U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 15);
  c2_active_id = c2_rpc_command;
  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 17);
  guard1 = FALSE;
  guard2 = FALSE;
  if (CV_EML_COND(0, 1, 0, c2_active_id != 0.0) != 0.0) {
    if (CV_EML_COND(0, 1, 1, c2_active_id >= 1.0)) {
      if (CV_EML_COND(0, 1, 2, c2_active_id < 15.0)) {
        CV_EML_MCDC(0, 1, 0, TRUE);
        CV_EML_IF(0, 1, 0, TRUE);
        _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 18);
        c2_type = c2_u[(int32_T)(real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("u",
          (int32_T)_SFD_INTEGER_CHECK("active_id", c2_active_id), 1, 15, 1, 0) -
          1].type;
        _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 19);
        c2_id = c2_u[(int32_T)(real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("u", (int32_T)
          _SFD_INTEGER_CHECK("active_id", c2_active_id), 1, 15, 1, 0) - 1].id;
        _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 20);
        for (c2_i5 = 0; c2_i5 < 20; c2_i5++) {
          c2_data[c2_i5] = c2_u[(int32_T)(real_T)_SFD_EML_ARRAY_BOUNDS_CHECK("u",
            (int32_T)_SFD_INTEGER_CHECK("active_id", c2_active_id), 1, 15, 1, 0)
            - 1].data[c2_i5];
        }
      } else {
        guard1 = TRUE;
      }
    } else {
      guard2 = TRUE;
    }
  } else {
    guard2 = TRUE;
  }

  if (guard2 == TRUE) {
    guard1 = TRUE;
  }

  if (guard1 == TRUE) {
    CV_EML_MCDC(0, 1, 0, FALSE);
    CV_EML_IF(0, 1, 0, FALSE);
    _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 22);
    c2_type = 0;
    _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 23);
    c2_id = 0.0F;
    _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 24);
    for (c2_i6 = 0; c2_i6 < 20; c2_i6++) {
      c2_data[c2_i6] = 0U;
    }
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, -24);
  _SFD_SYMBOL_SCOPE_POP();
  *c2_b_type = c2_type;
  *c2_b_id = c2_id;
  for (c2_i7 = 0; c2_i7 < 20; c2_i7++) {
    (*c2_b_data)[c2_i7] = c2_data[c2_i7];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_marlin_full_libMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void initSimStructsc2_marlin_full_lib(SFc2_marlin_full_libInstanceStruct *
  chartInstance)
{
}

static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber)
{
}

static const mxArray *c2_sf_marshallOut(void *chartInstanceVoid, void *c2_inData)
{
  const mxArray *c2_mxArrayOutData = NULL;
  int32_T c2_i8;
  uint8_T c2_b_inData[20];
  int32_T c2_i9;
  uint8_T c2_u[20];
  const mxArray *c2_y = NULL;
  SFc2_marlin_full_libInstanceStruct *chartInstance;
  chartInstance = (SFc2_marlin_full_libInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  for (c2_i8 = 0; c2_i8 < 20; c2_i8++) {
    c2_b_inData[c2_i8] = (*(uint8_T (*)[20])c2_inData)[c2_i8];
  }

  for (c2_i9 = 0; c2_i9 < 20; c2_i9++) {
    c2_u[c2_i9] = c2_b_inData[c2_i9];
  }

  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_u, 3, 0U, 1U, 0U, 1, 20), FALSE);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, FALSE);
  return c2_mxArrayOutData;
}

static void c2_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_data, const char_T *c2_identifier, uint8_T
  c2_y[20])
{
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_data), &c2_thisId, c2_y);
  sf_mex_destroy(&c2_data);
}

static void c2_b_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId,
  uint8_T c2_y[20])
{
  uint8_T c2_uv1[20];
  int32_T c2_i10;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), c2_uv1, 1, 3, 0U, 1, 0U, 1, 20);
  for (c2_i10 = 0; c2_i10 < 20; c2_i10++) {
    c2_y[c2_i10] = c2_uv1[c2_i10];
  }

  sf_mex_destroy(&c2_u);
}

static void c2_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_data;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  uint8_T c2_y[20];
  int32_T c2_i11;
  SFc2_marlin_full_libInstanceStruct *chartInstance;
  chartInstance = (SFc2_marlin_full_libInstanceStruct *)chartInstanceVoid;
  c2_data = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_data), &c2_thisId, c2_y);
  sf_mex_destroy(&c2_data);
  for (c2_i11 = 0; c2_i11 < 20; c2_i11++) {
    (*(uint8_T (*)[20])c2_outData)[c2_i11] = c2_y[c2_i11];
  }

  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_b_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData = NULL;
  real32_T c2_u;
  const mxArray *c2_y = NULL;
  SFc2_marlin_full_libInstanceStruct *chartInstance;
  chartInstance = (SFc2_marlin_full_libInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_u = *(real32_T *)c2_inData;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 1, 0U, 0U, 0U, 0), FALSE);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, FALSE);
  return c2_mxArrayOutData;
}

static real32_T c2_c_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_id, const char_T *c2_identifier)
{
  real32_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_id), &c2_thisId);
  sf_mex_destroy(&c2_id);
  return c2_y;
}

static real32_T c2_d_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real32_T c2_y;
  real32_T c2_f0;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_f0, 1, 1, 0U, 0, 0U, 0);
  c2_y = c2_f0;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_id;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real32_T c2_y;
  SFc2_marlin_full_libInstanceStruct *chartInstance;
  chartInstance = (SFc2_marlin_full_libInstanceStruct *)chartInstanceVoid;
  c2_id = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_id), &c2_thisId);
  sf_mex_destroy(&c2_id);
  *(real32_T *)c2_outData = c2_y;
  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_c_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData = NULL;
  int32_T c2_u;
  const mxArray *c2_y = NULL;
  SFc2_marlin_full_libInstanceStruct *chartInstance;
  chartInstance = (SFc2_marlin_full_libInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_u = *(int32_T *)c2_inData;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 6, 0U, 0U, 0U, 0), FALSE);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, FALSE);
  return c2_mxArrayOutData;
}

static int32_T c2_e_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_type, const char_T *c2_identifier)
{
  int32_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_type), &c2_thisId);
  sf_mex_destroy(&c2_type);
  return c2_y;
}

static int32_T c2_f_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  int32_T c2_y;
  int32_T c2_i12;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_i12, 1, 6, 0U, 0, 0U, 0);
  c2_y = c2_i12;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_type;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  int32_T c2_y;
  SFc2_marlin_full_libInstanceStruct *chartInstance;
  chartInstance = (SFc2_marlin_full_libInstanceStruct *)chartInstanceVoid;
  c2_type = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_type), &c2_thisId);
  sf_mex_destroy(&c2_type);
  *(int32_T *)c2_outData = c2_y;
  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_d_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_i13;
  c2_MarlinFullInternalRPC c2_b_inData[15];
  int32_T c2_i14;
  c2_MarlinFullInternalRPC c2_u[15];
  const mxArray *c2_y = NULL;
  static int32_T c2_iv0[1] = { 15 };

  int32_T c2_iv1[1];
  int32_T c2_i15;
  c2_MarlinFullInternalRPC *c2_r0;
  int32_T c2_b_u;
  const mxArray *c2_b_y = NULL;
  real32_T c2_c_u;
  const mxArray *c2_c_y = NULL;
  int32_T c2_i16;
  uint8_T c2_uv2[20];
  int32_T c2_i17;
  uint8_T c2_d_u[20];
  const mxArray *c2_d_y = NULL;
  SFc2_marlin_full_libInstanceStruct *chartInstance;
  chartInstance = (SFc2_marlin_full_libInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  for (c2_i13 = 0; c2_i13 < 15; c2_i13++) {
    c2_b_inData[c2_i13] = (*(c2_MarlinFullInternalRPC (*)[15])c2_inData)[c2_i13];
  }

  for (c2_i14 = 0; c2_i14 < 15; c2_i14++) {
    c2_u[c2_i14] = c2_b_inData[c2_i14];
  }

  c2_y = NULL;
  c2_iv1[0] = c2_iv0[0];
  sf_mex_assign(&c2_y, sf_mex_createstructarray("structure", 1, c2_iv1), FALSE);
  for (c2_i15 = 0; c2_i15 < 15; c2_i15++) {
    c2_r0 = &c2_u[c2_i15];
    c2_b_u = c2_r0->type;
    c2_b_y = NULL;
    sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_u, 6, 0U, 0U, 0U, 0), FALSE);
    sf_mex_addfield(c2_y, c2_b_y, "type", "type", c2_i15);
    c2_c_u = c2_r0->id;
    c2_c_y = NULL;
    sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_u, 1, 0U, 0U, 0U, 0), FALSE);
    sf_mex_addfield(c2_y, c2_c_y, "id", "id", c2_i15);
    for (c2_i16 = 0; c2_i16 < 20; c2_i16++) {
      c2_uv2[c2_i16] = c2_r0->data[c2_i16];
    }

    for (c2_i17 = 0; c2_i17 < 20; c2_i17++) {
      c2_d_u[c2_i17] = c2_uv2[c2_i17];
    }

    c2_d_y = NULL;
    sf_mex_assign(&c2_d_y, sf_mex_create("y", c2_d_u, 3, 0U, 1U, 0U, 1, 20),
                  FALSE);
    sf_mex_addfield(c2_y, c2_d_y, "data", "data", c2_i15);
  }

  sf_mex_assign(&c2_mxArrayOutData, c2_y, FALSE);
  return c2_mxArrayOutData;
}

static const mxArray *c2_e_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData = NULL;
  real_T c2_u;
  const mxArray *c2_y = NULL;
  SFc2_marlin_full_libInstanceStruct *chartInstance;
  chartInstance = (SFc2_marlin_full_libInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_u = *(real_T *)c2_inData;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 0, 0U, 0U, 0U, 0), FALSE);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, FALSE);
  return c2_mxArrayOutData;
}

static real_T c2_g_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d0;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d0, 1, 0, 0U, 0, 0U, 0);
  c2_y = c2_d0;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_nargout;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real_T c2_y;
  SFc2_marlin_full_libInstanceStruct *chartInstance;
  chartInstance = (SFc2_marlin_full_libInstanceStruct *)chartInstanceVoid;
  c2_nargout = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_nargout), &c2_thisId);
  sf_mex_destroy(&c2_nargout);
  *(real_T *)c2_outData = c2_y;
  sf_mex_destroy(&c2_mxArrayInData);
}

const mxArray *sf_c2_marlin_full_lib_get_eml_resolved_functions_info(void)
{
  const mxArray *c2_nameCaptureInfo = NULL;
  c2_nameCaptureInfo = NULL;
  sf_mex_assign(&c2_nameCaptureInfo, sf_mex_createstruct("structure", 2, 1, 1),
                FALSE);
  c2_info_helper(&c2_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c2_nameCaptureInfo);
  return c2_nameCaptureInfo;
}

static void c2_info_helper(const mxArray **c2_info)
{
  const mxArray *c2_rhs0 = NULL;
  const mxArray *c2_lhs0 = NULL;
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("length"), "name", "name", 0);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("struct"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/length.m"), "resolved",
                  "resolved", 0);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(1303178606U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c2_rhs0, sf_mex_createcellarray(0), FALSE);
  sf_mex_assign(&c2_lhs0, sf_mex_createcellarray(0), FALSE);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_rhs0), "rhs", "rhs", 0);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_lhs0), "lhs", "lhs", 0);
  sf_mex_destroy(&c2_rhs0);
  sf_mex_destroy(&c2_lhs0);
}

static const mxArray *c2_emlrt_marshallOut(char * c2_u)
{
  const mxArray *c2_y = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c2_u)), FALSE);
  return c2_y;
}

static const mxArray *c2_b_emlrt_marshallOut(uint32_T c2_u)
{
  const mxArray *c2_y = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 7, 0U, 0U, 0U, 0), FALSE);
  return c2_y;
}

static const mxArray *c2_u_bus_io(void *chartInstanceVoid, void *c2_pData)
{
  const mxArray *c2_mxVal = NULL;
  int32_T c2_i18;
  c2_MarlinFullInternalRPC c2_tmp[15];
  int32_T c2_i19;
  SFc2_marlin_full_libInstanceStruct *chartInstance;
  chartInstance = (SFc2_marlin_full_libInstanceStruct *)chartInstanceVoid;
  c2_mxVal = NULL;
  for (c2_i18 = 0; c2_i18 < 15; c2_i18++) {
    c2_tmp[c2_i18].type = *(int32_T *)((char_T *)(c2_MarlinFullInternalRPC *)
      ((char_T *)c2_pData + 32 * c2_i18) + 0);
    c2_tmp[c2_i18].id = *(real32_T *)((char_T *)(c2_MarlinFullInternalRPC *)
      ((char_T *)c2_pData + 32 * c2_i18) + 4);
    for (c2_i19 = 0; c2_i19 < 20; c2_i19++) {
      c2_tmp[c2_i18].data[c2_i19] = ((uint8_T *)((char_T *)
        (c2_MarlinFullInternalRPC *)((char_T *)c2_pData + 32 * c2_i18) + 8))
        [c2_i19];
    }
  }

  sf_mex_assign(&c2_mxVal, c2_d_sf_marshallOut(chartInstance, c2_tmp), FALSE);
  return c2_mxVal;
}

static uint8_T c2_h_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_b_is_active_c2_marlin_full_lib, const char_T
  *c2_identifier)
{
  uint8_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_i_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_b_is_active_c2_marlin_full_lib), &c2_thisId);
  sf_mex_destroy(&c2_b_is_active_c2_marlin_full_lib);
  return c2_y;
}

static uint8_T c2_i_emlrt_marshallIn(SFc2_marlin_full_libInstanceStruct
  *chartInstance, const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  uint8_T c2_y;
  uint8_T c2_u0;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_u0, 1, 3, 0U, 0, 0U, 0);
  c2_y = c2_u0;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void init_dsm_address_info(SFc2_marlin_full_libInstanceStruct
  *chartInstance)
{
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c2_marlin_full_lib_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(439896810U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(3939658861U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1520103595U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1042018512U);
}

mxArray *sf_c2_marlin_full_lib_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1,1,5,
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("vxG35xLlqLWmGgDuzKeSpD");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(15);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(13));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,3,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(8));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(9));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(20);
      pr[1] = (double)(1);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(3));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c2_marlin_full_lib_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c2_marlin_full_lib_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

static const mxArray *sf_get_sim_state_info_c2_marlin_full_lib(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x4'type','srcId','name','auxInfo'{{M[1],M[9],T\"data\",},{M[1],M[8],T\"id\",},{M[1],M[5],T\"type\",},{M[8],M[0],T\"is_active_c2_marlin_full_lib\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 4, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c2_marlin_full_lib_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc2_marlin_full_libInstanceStruct *chartInstance;
    chartInstance = (SFc2_marlin_full_libInstanceStruct *) ((ChartInfoStruct *)
      (ssGetUserData(S)))->chartInstance;
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _marlin_full_libMachineNumber_,
           2,
           1,
           1,
           5,
           0,
           0,
           0,
           0,
           0,
           &(chartInstance->chartNumber),
           &(chartInstance->instanceNumber),
           ssGetPath(S),
           (void *)S);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          init_script_number_translation(_marlin_full_libMachineNumber_,
            chartInstance->chartNumber);
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_marlin_full_libMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _marlin_full_libMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"rpc_command");
          _SFD_SET_DATA_PROPS(1,1,1,0,"u");
          _SFD_SET_DATA_PROPS(2,2,0,1,"type");
          _SFD_SET_DATA_PROPS(3,2,0,1,"id");
          _SFD_SET_DATA_PROPS(4,2,0,1,"data");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,1,0,0,0,0,0,3,1);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",2,-1,470);
        _SFD_CV_INIT_EML_IF(0,1,0,246,303,390,470);

        {
          static int condStart[] = { 250, 263, 281 };

          static int condEnd[] = { 259, 277, 302 };

          static int pfixExpr[] = { 0, 1, -3, 2, -3 };

          _SFD_CV_INIT_EML_MCDC(0,1,0,250,302,3,0,&(condStart[0]),&(condEnd[0]),
                                5,&(pfixExpr[0]));
        }

        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_e_sf_marshallOut,(MexInFcnForType)NULL);

        {
          unsigned int dimVector[1];
          dimVector[0]= 15;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_STRUCT,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_u_bus_io,(MexInFcnForType)NULL);
        }

        _SFD_SET_DATA_COMPILED_PROPS(2,SF_INT32,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_c_sf_marshallOut,(MexInFcnForType)c2_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_SINGLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)c2_b_sf_marshallIn);

        {
          unsigned int dimVector[1];
          dimVector[0]= 20;
          _SFD_SET_DATA_COMPILED_PROPS(4,SF_UINT8,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)
            c2_sf_marshallIn);
        }

        {
          real_T *c2_rpc_command;
          int32_T *c2_type;
          real32_T *c2_id;
          c2_MarlinFullInternalRPC (*c2_u)[15];
          uint8_T (*c2_data)[20];
          c2_data = (uint8_T (*)[20])ssGetOutputPortSignal(chartInstance->S, 3);
          c2_id = (real32_T *)ssGetOutputPortSignal(chartInstance->S, 2);
          c2_type = (int32_T *)ssGetOutputPortSignal(chartInstance->S, 1);
          c2_u = (c2_MarlinFullInternalRPC (*)[15])ssGetInputPortSignal
            (chartInstance->S, 1);
          c2_rpc_command = (real_T *)ssGetInputPortSignal(chartInstance->S, 0);
          _SFD_SET_DATA_VALUE_PTR(0U, c2_rpc_command);
          _SFD_SET_DATA_VALUE_PTR(1U, *c2_u);
          _SFD_SET_DATA_VALUE_PTR(2U, c2_type);
          _SFD_SET_DATA_VALUE_PTR(3U, c2_id);
          _SFD_SET_DATA_VALUE_PTR(4U, *c2_data);
        }
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _marlin_full_libMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "4yPK3fHvOfegXPpzLINBr";
}

static void sf_opaque_initialize_c2_marlin_full_lib(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc2_marlin_full_libInstanceStruct*)
    chartInstanceVar)->S,0);
  initialize_params_c2_marlin_full_lib((SFc2_marlin_full_libInstanceStruct*)
    chartInstanceVar);
  initialize_c2_marlin_full_lib((SFc2_marlin_full_libInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_enable_c2_marlin_full_lib(void *chartInstanceVar)
{
  enable_c2_marlin_full_lib((SFc2_marlin_full_libInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_disable_c2_marlin_full_lib(void *chartInstanceVar)
{
  disable_c2_marlin_full_lib((SFc2_marlin_full_libInstanceStruct*)
    chartInstanceVar);
}

static void sf_opaque_gateway_c2_marlin_full_lib(void *chartInstanceVar)
{
  sf_c2_marlin_full_lib((SFc2_marlin_full_libInstanceStruct*) chartInstanceVar);
}

extern const mxArray* sf_internal_get_sim_state_c2_marlin_full_lib(SimStruct* S)
{
  ChartInfoStruct *chartInfo = (ChartInfoStruct*) ssGetUserData(S);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_raw2high");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = (mxArray*) get_sim_state_c2_marlin_full_lib
    ((SFc2_marlin_full_libInstanceStruct*)chartInfo->chartInstance);/* raw sim ctx */
  prhs[3] = (mxArray*) sf_get_sim_state_info_c2_marlin_full_lib();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_raw2high'.\n");
  }

  return plhs[0];
}

extern void sf_internal_set_sim_state_c2_marlin_full_lib(SimStruct* S, const
  mxArray *st)
{
  ChartInfoStruct *chartInfo = (ChartInfoStruct*) ssGetUserData(S);
  mxArray *plhs[1] = { NULL };

  mxArray *prhs[4];
  int mxError = 0;
  prhs[0] = mxCreateString("chart_simctx_high2raw");
  prhs[1] = mxCreateDoubleScalar(ssGetSFuncBlockHandle(S));
  prhs[2] = mxDuplicateArray(st);      /* high level simctx */
  prhs[3] = (mxArray*) sf_get_sim_state_info_c2_marlin_full_lib();/* state var info */
  mxError = sf_mex_call_matlab(1, plhs, 4, prhs, "sfprivate");
  mxDestroyArray(prhs[0]);
  mxDestroyArray(prhs[1]);
  mxDestroyArray(prhs[2]);
  mxDestroyArray(prhs[3]);
  if (mxError || plhs[0] == NULL) {
    sf_mex_error_message("Stateflow Internal Error: \nError calling 'chart_simctx_high2raw'.\n");
  }

  set_sim_state_c2_marlin_full_lib((SFc2_marlin_full_libInstanceStruct*)
    chartInfo->chartInstance, mxDuplicateArray(plhs[0]));
  mxDestroyArray(plhs[0]);
}

static const mxArray* sf_opaque_get_sim_state_c2_marlin_full_lib(SimStruct* S)
{
  return sf_internal_get_sim_state_c2_marlin_full_lib(S);
}

static void sf_opaque_set_sim_state_c2_marlin_full_lib(SimStruct* S, const
  mxArray *st)
{
  sf_internal_set_sim_state_c2_marlin_full_lib(S, st);
}

static void sf_opaque_terminate_c2_marlin_full_lib(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc2_marlin_full_libInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_marlin_full_lib_optimization_info();
    }

    finalize_c2_marlin_full_lib((SFc2_marlin_full_libInstanceStruct*)
      chartInstanceVar);
    utFree((void *)chartInstanceVar);
    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc2_marlin_full_lib((SFc2_marlin_full_libInstanceStruct*)
    chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c2_marlin_full_lib(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c2_marlin_full_lib((SFc2_marlin_full_libInstanceStruct*)
      (((ChartInfoStruct *)ssGetUserData(S))->chartInstance));
  }
}

static void mdlSetWorkWidths_c2_marlin_full_lib(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_marlin_full_lib_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(S,sf_get_instance_specialization(),infoStruct,
      2);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(S,sf_get_instance_specialization(),
                infoStruct,2,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop(S,
      sf_get_instance_specialization(),infoStruct,2,
      "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(S,sf_get_instance_specialization(),infoStruct,2);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,2,2);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,2,3);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=3; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 2; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,2);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3505983695U));
  ssSetChecksum1(S,(579720555U));
  ssSetChecksum2(S,(2701845996U));
  ssSetChecksum3(S,(2491577277U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c2_marlin_full_lib(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c2_marlin_full_lib(SimStruct *S)
{
  SFc2_marlin_full_libInstanceStruct *chartInstance;
  chartInstance = (SFc2_marlin_full_libInstanceStruct *)utMalloc(sizeof
    (SFc2_marlin_full_libInstanceStruct));
  memset(chartInstance, 0, sizeof(SFc2_marlin_full_libInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c2_marlin_full_lib;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c2_marlin_full_lib;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c2_marlin_full_lib;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c2_marlin_full_lib;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c2_marlin_full_lib;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c2_marlin_full_lib;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c2_marlin_full_lib;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c2_marlin_full_lib;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c2_marlin_full_lib;
  chartInstance->chartInfo.mdlStart = mdlStart_c2_marlin_full_lib;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c2_marlin_full_lib;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->S = S;
  ssSetUserData(S,(void *)(&(chartInstance->chartInfo)));/* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
  chart_debug_initialization(S,1);
}

void c2_marlin_full_lib_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c2_marlin_full_lib(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c2_marlin_full_lib(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c2_marlin_full_lib(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c2_marlin_full_lib_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
