#ifndef RTW_HEADER_struct_KoXNgBBld1DARpq3izFtbF_h_
#define RTW_HEADER_struct_KoXNgBBld1DARpq3izFtbF_h_
#include "rtwtypes.h"

typedef struct {
  real_T eA1;
  real_T eA2;
} struct_KoXNgBBld1DARpq3izFtbF;

#endif                                 /* RTW_HEADER_struct_KoXNgBBld1DARpq3izFtbF_h_ */
