#ifndef RTW_HEADER_struct_CXnTNOFtLHj3Uh94en2j8C_h_
#define RTW_HEADER_struct_CXnTNOFtLHj3Uh94en2j8C_h_
#include "struct_6bi3MChDTb2LN2fRyJtpm.h"
#include "struct_om0sqrv6CVBccOjddaiW7E.h"
#include "rtwtypes.h"

typedef struct {
  struct_6bi3MChDTb2LN2fRyJtpm input0;
  struct_6bi3MChDTb2LN2fRyJtpm input1;
  struct_om0sqrv6CVBccOjddaiW7E input_extra;
} struct_CXnTNOFtLHj3Uh94en2j8C;

#endif                                 /* RTW_HEADER_struct_CXnTNOFtLHj3Uh94en2j8C_h_ */
