/*
 *  rtmodel.h:
 *
 * Code generation for model "drum_bench_working".
 *
 * Model version              : 1.99
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Tue Jul  8 09:25:28 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_rtmodel_h_
#define RTW_HEADER_rtmodel_h_

/*
 *  Includes the appropriate headers when we are using rtModel
 */
#include "drum_bench_working.h"
#define GRTINTERFACE                   1
#endif                                 /* RTW_HEADER_rtmodel_h_ */
