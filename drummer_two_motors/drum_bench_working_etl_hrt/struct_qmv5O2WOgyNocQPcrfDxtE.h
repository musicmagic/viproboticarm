#ifndef RTW_HEADER_struct_qmv5O2WOgyNocQPcrfDxtE_h_
#define RTW_HEADER_struct_qmv5O2WOgyNocQPcrfDxtE_h_
#include "rtwtypes.h"

typedef struct {
  real_T bias;
  real_T ka;
  real_T kd;
  real_T var_imp_cutoff;
} struct_qmv5O2WOgyNocQPcrfDxtE;

#endif                                 /* RTW_HEADER_struct_qmv5O2WOgyNocQPcrfDxtE_h_ */
