/*
 * drum_bench_working_capi.c
 *
 * Code generation for model "drum_bench_working".
 *
 * Model version              : 1.99
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Tue Jul  8 09:25:28 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "rtw_capi.h"
#ifdef HOST_CAPI_BUILD
#include "drum_bench_working_capi_host.h"
#define sizeof(s)                      ((size_t)(0xFFFF))
#undef rt_offsetof
#define rt_offsetof(s,el)              ((uint16_T)(0xFFFF))
#define TARGET_CONST
#define TARGET_STRING(s)               (s)
#else                                  /* HOST_CAPI_BUILD */
#include "drum_bench_working.h"
#include "drum_bench_working_private.h"
#ifdef LIGHT_WEIGHT_CAPI
#define TARGET_CONST
#define TARGET_STRING(s)               (NULL)
#else
#define TARGET_CONST                   const
#define TARGET_STRING(s)               (s)
#endif
#endif                                 /* HOST_CAPI_BUILD */

/* Block output signal information */
static const rtwCAPI_Signals rtBlockSignals[] = {
  /* addrMapIndex, sysNum, blockPath,
   * signalName, portNumber, dataTypeIndex, dimIndex, fxpIndex, sTimeIndex
   */
  { 0, 0, TARGET_STRING("drum_bench_working/Bus Selector"),
    TARGET_STRING("mode"), 0, 0, 0, 0, 0 },

  { 1, 0, TARGET_STRING("drum_bench_working/Bus Selector"),
    TARGET_STRING("motor_torque"), 1, 0, 0, 0, 0 },

  { 2, 0, TARGET_STRING("drum_bench_working/Bus Selector"),
    TARGET_STRING("motor_iq"), 2, 0, 0, 0, 0 },

  { 3, 0, TARGET_STRING("drum_bench_working/Bus Selector1"),
    TARGET_STRING("mode"), 0, 0, 0, 0, 0 },

  { 4, 0, TARGET_STRING("drum_bench_working/Bus Selector1"),
    TARGET_STRING("motor_torque"), 1, 0, 0, 0, 0 },

  { 5, 0, TARGET_STRING("drum_bench_working/Bus Selector1"),
    TARGET_STRING("motor_iq"), 2, 0, 0, 0, 0 },

  { 6, 0, TARGET_STRING("drum_bench_working/mode0"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 7, 0, TARGET_STRING("drum_bench_working/mode1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 8, 0, TARGET_STRING("drum_bench_working/motor_iq0"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 9, 0, TARGET_STRING("drum_bench_working/motor_iq1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 10, 0, TARGET_STRING("drum_bench_working/motor_torque0"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 11, 0, TARGET_STRING("drum_bench_working/motor_torque1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 12, 0, TARGET_STRING("drum_bench_working/mode switch"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 13, 0, TARGET_STRING("drum_bench_working/mode switch1"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 14, 0, TARGET_STRING("drum_bench_working/Domain State"),
    TARGET_STRING(""), 0, 2, 0, 0, 1 },

  { 15, 0, TARGET_STRING("drum_bench_working/Domain State"),
    TARGET_STRING(""), 1, 3, 0, 0, 1 },

  { 16, 0, TARGET_STRING("drum_bench_working/Master State"),
    TARGET_STRING(""), 0, 2, 0, 0, 1 },

  { 17, 0, TARGET_STRING("drum_bench_working/Master State"),
    TARGET_STRING(""), 1, 3, 0, 0, 1 },

  { 18, 0, TARGET_STRING("drum_bench_working/Master State"),
    TARGET_STRING(""), 2, 4, 0, 0, 1 },

  { 19, 1, TARGET_STRING(
    "drum_bench_working/impedence_controller/Filtered Derivative"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 20, 0, TARGET_STRING(
    "drum_bench_working/impedence_controller/Bus Assignment"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 21, 0, TARGET_STRING("drum_bench_working/impedence_controller/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 22, 0, TARGET_STRING("drum_bench_working/impedence_controller/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 23, 0, TARGET_STRING("drum_bench_working/impedence_controller/Bus Selector3"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 0 },

  { 24, 0, TARGET_STRING("drum_bench_working/impedence_controller/motor_torque"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 25, 0, TARGET_STRING("drum_bench_working/impedence_controller/kd"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 26, 0, TARGET_STRING("drum_bench_working/impedence_controller/kp"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 27, 0, TARGET_STRING("drum_bench_working/impedence_controller/Saturation"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 28, 0, TARGET_STRING("drum_bench_working/impedence_controller/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 29, 0, TARGET_STRING("drum_bench_working/impedence_controller/Sum1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 30, 0, TARGET_STRING("drum_bench_working/impedence_controller/Sum2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 31, 2, TARGET_STRING(
    "drum_bench_working/impedence_controller1/Filtered Derivative"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 32, 0, TARGET_STRING(
    "drum_bench_working/impedence_controller1/Bus Assignment"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 33, 0, TARGET_STRING(
    "drum_bench_working/impedence_controller1/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 34, 0, TARGET_STRING(
    "drum_bench_working/impedence_controller1/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 35, 0, TARGET_STRING(
    "drum_bench_working/impedence_controller1/Bus Selector3"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 0 },

  { 36, 0, TARGET_STRING("drum_bench_working/impedence_controller1/motor_torque"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 37, 0, TARGET_STRING("drum_bench_working/impedence_controller1/kd"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 38, 0, TARGET_STRING("drum_bench_working/impedence_controller1/kp"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 39, 0, TARGET_STRING("drum_bench_working/impedence_controller1/Saturation"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 40, 0, TARGET_STRING("drum_bench_working/impedence_controller1/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 41, 0, TARGET_STRING("drum_bench_working/impedence_controller1/Sum1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 42, 0, TARGET_STRING("drum_bench_working/impedence_controller1/Sum2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 43, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/Bus Creator"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 44, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/Bus Creator"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 45, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/Bus Selector"),
    TARGET_STRING("marlin_basic_status"), 0, 5, 0, 0, 0 },

  { 46, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/Bus Selector"),
    TARGET_STRING("temperature_motor_winding"), 1, 0, 0, 0, 0 },

  { 47, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/Bus Selector"),
    TARGET_STRING("temperature_motor_housing"), 2, 0, 0, 0, 0 },

  { 48, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/temperature_motor_housing"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 49, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/temperature_motor_winding"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 50, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/Bus Selector"),
    TARGET_STRING("marlin_basic_status"), 0, 5, 0, 0, 0 },

  { 51, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/Bus Selector"),
    TARGET_STRING("temperature_motor_winding"), 1, 0, 0, 0, 0 },

  { 52, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/Bus Selector"),
    TARGET_STRING("temperature_motor_housing"), 2, 0, 0, 0, 0 },

  { 53, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/temperature_motor_housing"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 54, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/temperature_motor_winding"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 55, 0, TARGET_STRING("drum_bench_working/marlin_1.1_full1/Bus Creator"),
    TARGET_STRING(""), 0, 6, 0, 0, 0 },

  { 56, 0, TARGET_STRING("drum_bench_working/marlin_1.1_full1/Bus Selector"),
    TARGET_STRING("rpc"), 0, 0, 0, 0, 0 },

  { 57, 0, TARGET_STRING("drum_bench_working/marlin_1.1_full2/Bus Creator"),
    TARGET_STRING(""), 0, 6, 0, 0, 0 },

  { 58, 0, TARGET_STRING("drum_bench_working/marlin_1.1_full2/Bus Selector"),
    TARGET_STRING("rpc"), 0, 0, 0, 0, 0 },

  { 59, 11, TARGET_STRING(
    "drum_bench_working/var_impedence_controller/Filtered Derivative"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 60, 0, TARGET_STRING("drum_bench_working/var_impedence_controller/bias"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 61, 0, TARGET_STRING(
    "drum_bench_working/var_impedence_controller/Bus Assignment"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 62, 0, TARGET_STRING(
    "drum_bench_working/var_impedence_controller/Bus Selector1"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 0 },

  { 63, 0, TARGET_STRING(
    "drum_bench_working/var_impedence_controller/Bus Selector1"),
    TARGET_STRING("analog_1"), 1, 0, 0, 0, 0 },

  { 64, 0, TARGET_STRING(
    "drum_bench_working/var_impedence_controller/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 65, 0, TARGET_STRING(
    "drum_bench_working/var_impedence_controller/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 66, 0, TARGET_STRING("drum_bench_working/var_impedence_controller/ka"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 67, 0, TARGET_STRING("drum_bench_working/var_impedence_controller/kd"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 68, 0, TARGET_STRING("drum_bench_working/var_impedence_controller/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 69, 0, TARGET_STRING("drum_bench_working/var_impedence_controller/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 70, 0, TARGET_STRING("drum_bench_working/var_impedence_controller/Sum1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 71, 0, TARGET_STRING("drum_bench_working/var_impedence_controller/Sum2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 72, 12, TARGET_STRING(
    "drum_bench_working/var_impedence_controller1/Filtered Derivative"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 73, 0, TARGET_STRING("drum_bench_working/var_impedence_controller1/bias"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 74, 0, TARGET_STRING(
    "drum_bench_working/var_impedence_controller1/Bus Assignment"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 75, 0, TARGET_STRING(
    "drum_bench_working/var_impedence_controller1/Bus Selector1"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 0 },

  { 76, 0, TARGET_STRING(
    "drum_bench_working/var_impedence_controller1/Bus Selector1"),
    TARGET_STRING("analog_1"), 1, 0, 0, 0, 0 },

  { 77, 0, TARGET_STRING(
    "drum_bench_working/var_impedence_controller1/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 78, 0, TARGET_STRING(
    "drum_bench_working/var_impedence_controller1/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 79, 0, TARGET_STRING("drum_bench_working/var_impedence_controller1/ka"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 80, 0, TARGET_STRING("drum_bench_working/var_impedence_controller1/kd"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 81, 0, TARGET_STRING("drum_bench_working/var_impedence_controller1/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 82, 0, TARGET_STRING("drum_bench_working/var_impedence_controller1/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 83, 0, TARGET_STRING("drum_bench_working/var_impedence_controller1/Sum1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 84, 0, TARGET_STRING("drum_bench_working/var_impedence_controller1/Sum2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 85, 1, TARGET_STRING(
    "drum_bench_working/impedence_controller/Filtered Derivative/Filter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 86, 1, TARGET_STRING(
    "drum_bench_working/impedence_controller/Filtered Derivative/Filter Coefficient"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 87, 1, TARGET_STRING(
    "drum_bench_working/impedence_controller/Filtered Derivative/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 88, 2, TARGET_STRING(
    "drum_bench_working/impedence_controller1/Filtered Derivative/Filter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 89, 2, TARGET_STRING(
    "drum_bench_working/impedence_controller1/Filtered Derivative/Filter Coefficient"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 90, 2, TARGET_STRING(
    "drum_bench_working/impedence_controller1/Filtered Derivative/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 91, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 92, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 93, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 94, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 95, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 96, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 97, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 98, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 99, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 100, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 101, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 102, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 103, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 104, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 105, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 106, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 107, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 108, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 109, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 110, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 111, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 112, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 113, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 114, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 115, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 116, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 117, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 118, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 119, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 120, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 121, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 122, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 123, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 124, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 125, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 126, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 127, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 128, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 129, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 130, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 131, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 132, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 133, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 134, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 135, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 136, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 137, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 138, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 139, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 140, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 141, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 142, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 143, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 144, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 145, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 146, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 147, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 148, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 149, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 150, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 151, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 152, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 153, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 154, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 155, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 156, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 157, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 158, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 159, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 160, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 161, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 0 },

  { 162, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_iq"), 1, 0, 0, 0, 0 },

  { 163, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_vq_avg"), 2, 0, 0, 0, 0 },

  { 164, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1"), 3, 0, 0, 0, 0 },

  { 165, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1_dot"), 4, 0, 0, 0, 0 },

  { 166, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2"), 5, 0, 0, 0, 0 },

  { 167, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2_dot"), 6, 0, 0, 0, 0 },

  { 168, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff"), 7, 0, 0, 0, 0 },

  { 169, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff_dot"), 8, 0, 0, 0, 0 },

  { 170, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1"), 9, 0, 0, 0, 0 },

  { 171, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1_dot"), 10, 0, 0, 0, 0 },

  { 172, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2"), 11, 0, 0, 0, 0 },

  { 173, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2_dot"), 12, 0, 0, 0, 0 },

  { 174, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi"), 13, 0, 0, 0, 0 },

  { 175, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi_dot"), 14, 0, 0, 0, 0 },

  { 176, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_x"), 15, 0, 0, 0, 0 },

  { 177, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_y"), 16, 0, 0, 0, 0 },

  { 178, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_z"), 17, 0, 0, 0, 0 },

  { 179, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("faults"), 18, 2, 0, 0, 0 },

  { 180, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("bus_v"), 19, 0, 0, 0, 0 },

  { 181, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("rpa_packet"), 20, 3, 1, 0, 0 },

  { 182, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 183, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_y"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 184, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_z"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 185, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/analog_1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 186, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/analog_1_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 187, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/analog_2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 188, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/analog_2_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 189, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/analog_diff"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 190, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/analog_diff_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 191, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/bus_v"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 192, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/motor_iq"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 193, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/motor_torque"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 194, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/motor_vq_avg"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 195, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 196, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 197, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/quadrature_2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 198, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/quadrature_2_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 199, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/ssi"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 200, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/ssi_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 201, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/rpa_float"),
    TARGET_STRING(""), 0, 7, 2, 0, 0 },

  { 202, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 0 },

  { 203, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_iq"), 1, 0, 0, 0, 0 },

  { 204, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_vq_avg"), 2, 0, 0, 0, 0 },

  { 205, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1"), 3, 0, 0, 0, 0 },

  { 206, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1_dot"), 4, 0, 0, 0, 0 },

  { 207, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2"), 5, 0, 0, 0, 0 },

  { 208, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2_dot"), 6, 0, 0, 0, 0 },

  { 209, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff"), 7, 0, 0, 0, 0 },

  { 210, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff_dot"), 8, 0, 0, 0, 0 },

  { 211, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1"), 9, 0, 0, 0, 0 },

  { 212, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1_dot"), 10, 0, 0, 0, 0 },

  { 213, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2"), 11, 0, 0, 0, 0 },

  { 214, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2_dot"), 12, 0, 0, 0, 0 },

  { 215, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi"), 13, 0, 0, 0, 0 },

  { 216, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi_dot"), 14, 0, 0, 0, 0 },

  { 217, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_x"), 15, 0, 0, 0, 0 },

  { 218, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_y"), 16, 0, 0, 0, 0 },

  { 219, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_z"), 17, 0, 0, 0, 0 },

  { 220, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("faults"), 18, 2, 0, 0, 0 },

  { 221, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("bus_v"), 19, 0, 0, 0, 0 },

  { 222, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("rpa_packet"), 20, 3, 1, 0, 0 },

  { 223, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_x"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 224, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_y"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 225, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_z"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 226, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/analog_1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 227, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/analog_1_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 228, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/analog_2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 229, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/analog_2_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 230, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/analog_diff"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 231, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/analog_diff_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 232, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/bus_v"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 233, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/motor_iq"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 234, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/motor_torque"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 235, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/motor_vq_avg"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 236, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 237, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 238, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 239, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_2_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 240, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/ssi"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 241, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/ssi_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 242, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/rpa_float"),
    TARGET_STRING(""), 0, 7, 2, 0, 0 },

  { 243, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector"),
    TARGET_STRING("mode"), 0, 0, 0, 0, 0 },

  { 244, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector1"),
    TARGET_STRING("motor_iq"), 0, 0, 0, 0, 0 },

  { 245, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector1"),
    TARGET_STRING("motor_id"), 1, 0, 0, 0, 0 },

  { 246, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector2"),
    TARGET_STRING("motor_vq"), 0, 0, 0, 0, 0 },

  { 247, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector2"),
    TARGET_STRING("motor_vd"), 1, 0, 0, 0, 0 },

  { 248, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector3"),
    TARGET_STRING("analog_diff"), 0, 0, 0, 0, 0 },

  { 249, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector3"),
    TARGET_STRING("analog_diff_dot"), 1, 0, 0, 0, 0 },

  { 250, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector4"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 251, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector4"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 252, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector5"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 0 },

  { 253, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Multiport Switch"),
    TARGET_STRING("mode"), 0, 2, 0, 0, 0 },

  { 254, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/Multiport Switch"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 255, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/motor_temperature_model/eA1"),
    TARGET_STRING(""), 0, 0, 4, 0, 0 },

  { 256, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/motor_temperature_model/eA2"),
    TARGET_STRING(""), 0, 0, 4, 0, 0 },

  { 257, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/motor_temperature_model/resistance"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 258, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/motor_temperature_model/Math Function"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 259, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/motor_temperature_model/Sum"),
    TARGET_STRING(""), 0, 0, 4, 0, 0 },

  { 260, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/motor_temperature_model/Delay"),
    TARGET_STRING(""), 0, 0, 4, 0, 0 },

  { 261, 6, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/select rpc"),
    TARGET_STRING("type"), 0, 8, 0, 0, 0 },

  { 262, 6, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/select rpc"),
    TARGET_STRING("id"), 1, 7, 0, 0, 0 },

  { 263, 6, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/select rpc"),
    TARGET_STRING("data"), 2, 3, 5, 0, 0 },

  { 264, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/Vector Concatenate"),
    TARGET_STRING(""), 0, 9, 6, 0, 0 },

  { 265, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/Byte Pack"),
    TARGET_STRING("rpc"), 0, 3, 1, 0, 0 },

  { 266, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/Byte Unpack"),
    TARGET_STRING("type"), 0, 8, 0, 0, 0 },

  { 267, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/Byte Unpack"),
    TARGET_STRING("id"), 1, 7, 0, 0, 0 },

  { 268, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/Byte Unpack"),
    TARGET_STRING("data"), 2, 3, 5, 0, 0 },

  { 269, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector"),
    TARGET_STRING("mode"), 0, 0, 0, 0, 0 },

  { 270, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector1"),
    TARGET_STRING("motor_iq"), 0, 0, 0, 0, 0 },

  { 271, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector1"),
    TARGET_STRING("motor_id"), 1, 0, 0, 0, 0 },

  { 272, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector2"),
    TARGET_STRING("motor_vq"), 0, 0, 0, 0, 0 },

  { 273, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector2"),
    TARGET_STRING("motor_vd"), 1, 0, 0, 0, 0 },

  { 274, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector3"),
    TARGET_STRING("analog_diff"), 0, 0, 0, 0, 0 },

  { 275, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector3"),
    TARGET_STRING("analog_diff_dot"), 1, 0, 0, 0, 0 },

  { 276, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector4"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 277, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector4"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 278, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector5"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 0 },

  { 279, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Multiport Switch"),
    TARGET_STRING("mode"), 0, 2, 0, 0, 0 },

  { 280, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/Multiport Switch"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 281, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/motor_temperature_model/eA1"),
    TARGET_STRING(""), 0, 0, 4, 0, 0 },

  { 282, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/motor_temperature_model/eA2"),
    TARGET_STRING(""), 0, 0, 4, 0, 0 },

  { 283, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/motor_temperature_model/resistance"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 284, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/motor_temperature_model/Math Function"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 285, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/motor_temperature_model/Sum"),
    TARGET_STRING(""), 0, 0, 4, 0, 0 },

  { 286, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/motor_temperature_model/Delay"),
    TARGET_STRING(""), 0, 0, 4, 0, 0 },

  { 287, 10, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/select rpc"),
    TARGET_STRING("type"), 0, 8, 0, 0, 0 },

  { 288, 10, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/select rpc"),
    TARGET_STRING("id"), 1, 7, 0, 0, 0 },

  { 289, 10, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/select rpc"),
    TARGET_STRING("data"), 2, 3, 5, 0, 0 },

  { 290, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/Vector Concatenate"),
    TARGET_STRING(""), 0, 9, 6, 0, 0 },

  { 291, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/Byte Pack"),
    TARGET_STRING("rpc"), 0, 3, 1, 0, 0 },

  { 292, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/Byte Unpack"),
    TARGET_STRING("type"), 0, 8, 0, 0, 0 },

  { 293, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/Byte Unpack"),
    TARGET_STRING("id"), 1, 7, 0, 0, 0 },

  { 294, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/Byte Unpack"),
    TARGET_STRING("data"), 2, 3, 5, 0, 0 },

  { 295, 11, TARGET_STRING(
    "drum_bench_working/var_impedence_controller/Filtered Derivative/Filter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 296, 11, TARGET_STRING(
    "drum_bench_working/var_impedence_controller/Filtered Derivative/Filter Coefficient"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 297, 11, TARGET_STRING(
    "drum_bench_working/var_impedence_controller/Filtered Derivative/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 298, 12, TARGET_STRING(
    "drum_bench_working/var_impedence_controller1/Filtered Derivative/Filter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 299, 12, TARGET_STRING(
    "drum_bench_working/var_impedence_controller1/Filtered Derivative/Filter Coefficient"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 300, 12, TARGET_STRING(
    "drum_bench_working/var_impedence_controller1/Filtered Derivative/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 301, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 302, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 303, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 304, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 305, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 306, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 307, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/analog_diff/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 308, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 309, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 310, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 311, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 312, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 313, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 314, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_iq/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 315, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 316, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 317, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 318, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 319, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 320, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 321, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/motor_vq/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 322, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 323, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 324, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 325, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 326, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 327, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 328, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 329, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 330, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 331, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 332, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 333, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 334, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 335, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 336, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 337, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 338, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 339, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 340, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 341, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 342, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 343, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 344, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 345, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 346, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 347, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 348, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 349, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 350, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 351, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 352, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 353, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 354, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 355, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 356, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 357, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 358, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 359, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 360, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 361, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 362, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 363, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 364, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 365, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 366, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 367, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 368, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 369, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 370, 0, TARGET_STRING(
    "drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 371, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/analog_"),
    TARGET_STRING("analog"), 0, 4, 0, 0, 0 },

  { 372, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/communication_fault_"),
    TARGET_STRING("communication_fault"), 0, 4, 0, 0, 0 },

  { 373, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/current_"),
    TARGET_STRING("current"), 0, 4, 0, 0, 0 },

  { 374, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/ethercat_"),
    TARGET_STRING("ethercat"), 0, 4, 0, 0, 0 },

  { 375, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/external_panic_"),
    TARGET_STRING("external_panic"), 0, 4, 0, 0, 0 },

  { 376, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/hard_fault_"),
    TARGET_STRING("hard_fault"), 0, 4, 0, 0, 0 },

  { 377, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/quadrature_"),
    TARGET_STRING("quadrature"), 0, 4, 0, 0, 0 },

  { 378, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/raw_integer"),
    TARGET_STRING("raw_integer"), 0, 2, 0, 0, 0 },

  { 379, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/soft_fault_"),
    TARGET_STRING("soft_fault"), 0, 4, 0, 0, 0 },

  { 380, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/ssi_"),
    TARGET_STRING("ssi"), 0, 4, 0, 0, 0 },

  { 381, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/temperature_"),
    TARGET_STRING("temperature"), 0, 4, 0, 0, 0 },

  { 382, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/voltage_"),
    TARGET_STRING("voltage"), 0, 4, 0, 0, 0 },

  { 383, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/analog_"),
    TARGET_STRING("analog"), 0, 4, 0, 0, 0 },

  { 384, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/communication_fault_"),
    TARGET_STRING("communication_fault"), 0, 4, 0, 0, 0 },

  { 385, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/current_"),
    TARGET_STRING("current"), 0, 4, 0, 0, 0 },

  { 386, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/ethercat_"),
    TARGET_STRING("ethercat"), 0, 4, 0, 0, 0 },

  { 387, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/external_panic_"),
    TARGET_STRING("external_panic"), 0, 4, 0, 0, 0 },

  { 388, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/hard_fault_"),
    TARGET_STRING("hard_fault"), 0, 4, 0, 0, 0 },

  { 389, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/quadrature_"),
    TARGET_STRING("quadrature"), 0, 4, 0, 0, 0 },

  { 390, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/raw_integer"),
    TARGET_STRING("raw_integer"), 0, 2, 0, 0, 0 },

  { 391, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/soft_fault_"),
    TARGET_STRING("soft_fault"), 0, 4, 0, 0, 0 },

  { 392, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/ssi_"),
    TARGET_STRING("ssi"), 0, 4, 0, 0, 0 },

  { 393, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/temperature_"),
    TARGET_STRING("temperature"), 0, 4, 0, 0, 0 },

  { 394, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/voltage_"),
    TARGET_STRING("voltage"), 0, 4, 0, 0, 0 },

  { 395, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/accelerometer_x_"),
    TARGET_STRING("accelerometer_x"), 0, 0, 0, 0, 0 },

  { 396, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/accelerometer_y_"),
    TARGET_STRING("accelerometer_y"), 0, 0, 0, 0, 0 },

  { 397, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/accelerometer_z_"),
    TARGET_STRING("accelerometer_z"), 0, 0, 0, 0, 0 },

  { 398, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_1_"),
    TARGET_STRING("analog_1"), 0, 0, 0, 0, 0 },

  { 399, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_1_dot_"),
    TARGET_STRING("analog_1_dot"), 0, 0, 0, 0, 0 },

  { 400, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_2_"),
    TARGET_STRING("analog_2"), 0, 0, 0, 0, 0 },

  { 401, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_2_dot_"),
    TARGET_STRING("analog_2_dot"), 0, 0, 0, 0, 0 },

  { 402, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_diff_"),
    TARGET_STRING("analog_diff"), 0, 0, 0, 0, 0 },

  { 403, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_diff_dot_"),
    TARGET_STRING("analog_diff_dot"), 0, 0, 0, 0, 0 },

  { 404, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/bus_v_"),
    TARGET_STRING("bus_v"), 0, 0, 0, 0, 0 },

  { 405, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/debug_"),
    TARGET_STRING("debug"), 0, 0, 0, 0, 0 },

  { 406, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/faults_"),
    TARGET_STRING("faults"), 0, 2, 0, 0, 0 },

  { 407, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/motor_iq_"),
    TARGET_STRING("motor_iq"), 0, 0, 0, 0, 0 },

  { 408, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/motor_torque_"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 0 },

  { 409, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/motor_vq_avg_"),
    TARGET_STRING("motor_vq_avg"), 0, 0, 0, 0, 0 },

  { 410, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/quadrature_1_"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 0 },

  { 411, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/quadrature_1_dot_"),
    TARGET_STRING("quadrature_1_dot"), 0, 0, 0, 0, 0 },

  { 412, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/quadrature_2_"),
    TARGET_STRING("quadrature_2"), 0, 0, 0, 0, 0 },

  { 413, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/quadrature_2_dot_"),
    TARGET_STRING("quadrature_2_dot"), 0, 0, 0, 0, 0 },

  { 414, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/rpa_packet_"),
    TARGET_STRING("rpa_packet"), 0, 3, 1, 0, 0 },

  { 415, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/ssi_"),
    TARGET_STRING("ssi"), 0, 0, 0, 0, 0 },

  { 416, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/ssi_dot_"),
    TARGET_STRING("ssi_dot"), 0, 0, 0, 0, 0 },

  { 417, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 418, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 1, 7, 0, 0, 0 },

  { 419, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 2, 7, 0, 0, 0 },

  { 420, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 3, 7, 0, 0, 0 },

  { 421, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 4, 7, 0, 0, 0 },

  { 422, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 5, 7, 0, 0, 0 },

  { 423, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 6, 7, 0, 0, 0 },

  { 424, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 7, 7, 0, 0, 0 },

  { 425, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 8, 7, 0, 0, 0 },

  { 426, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 9, 7, 0, 0, 0 },

  { 427, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 10, 7, 0, 0, 0 },

  { 428, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 11, 7, 0, 0, 0 },

  { 429, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 12, 7, 0, 0, 0 },

  { 430, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 13, 7, 0, 0, 0 },

  { 431, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 14, 7, 0, 0, 0 },

  { 432, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 15, 7, 0, 0, 0 },

  { 433, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 16, 7, 0, 0, 0 },

  { 434, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 17, 7, 0, 0, 0 },

  { 435, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 18, 2, 0, 0, 0 },

  { 436, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 19, 7, 0, 0, 0 },

  { 437, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 20, 7, 0, 0, 0 },

  { 438, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 21, 3, 1, 0, 0 },

  { 439, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/analog_diff/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 440, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/analog_diff/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 441, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/brake/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 442, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/brake/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 443, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/current/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 444, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/current/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 445, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/motor_torque/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 446, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/motor_torque/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 447, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/open/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 448, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/open/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 449, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/phase lock/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 450, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/phase lock/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 451, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/quadrature_1/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 452, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/quadrature_1/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 453, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/voltage/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 454, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/voltage/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 455, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/10 khz logger read/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 456, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/10 khz logger read/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 457, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/10 khz logger start/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 458, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/10 khz logger start/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 459, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/100 khz logger read/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 460, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/100 khz logger read/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 461, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/100 khz logger read2/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 462, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/100 khz logger read2/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 463, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/100 khz logger start/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 464, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/100 khz logger start/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 465, 3, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/enabled subsystem"),
    TARGET_STRING(""), 0, 3, 5, 0, 0 },

  { 466, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 467, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 8, 0, 0 },

  { 468, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/analog_limit"),
    TARGET_STRING("analog_limit"), 0, 0, 0, 0, 0 },

  { 469, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/ethercat"),
    TARGET_STRING("ethercat"), 0, 0, 0, 0, 0 },

  { 470, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/external_panic"),
    TARGET_STRING("external_panic"), 0, 0, 0, 0, 0 },

  { 471, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/motor encoder"),
    TARGET_STRING("motor_encoder"), 0, 0, 0, 0, 0 },

  { 472, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/over_current"),
    TARGET_STRING("over_current"), 0, 0, 0, 0, 0 },

  { 473, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/over_voltage"),
    TARGET_STRING("over_voltage"), 0, 0, 0, 0, 0 },

  { 474, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/quadrature_1_high"),
    TARGET_STRING("quadrature_1_high"), 0, 0, 0, 0, 0 },

  { 475, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/quadrature_1_low"),
    TARGET_STRING("quadrature_1_low"), 0, 0, 0, 0, 0 },

  { 476, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/quadrature_2_limit"),
    TARGET_STRING("quadrature_2_limit"), 0, 0, 0, 0, 0 },

  { 477, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/under_voltage"),
    TARGET_STRING("under_voltage"), 0, 0, 0, 0, 0 },

  { 478, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 479, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/Byte Unpack"),
    TARGET_STRING(""), 0, 10, 8, 0, 0 },

  { 480, 4, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/Enabled Subsystem"),
    TARGET_STRING(""), 0, 3, 5, 0, 0 },

  { 481, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 482, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/load a"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 483, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/load a1"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 484, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/load a2"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 485, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/load a3"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 486, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/load b"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 487, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 488, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/Byte Unpack"),
    TARGET_STRING(""), 0, 7, 9, 0, 0 },

  { 489, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 490, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 4, 0, 0 },

  { 491, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 0 },

  { 492, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug1/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 493, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug1/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 4, 0, 0 },

  { 494, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug1/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 0 },

  { 495, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug2/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 496, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug2/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 4, 0, 0 },

  { 497, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug2/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 0 },

  { 498, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/current_index"),
    TARGET_STRING(""), 0, 8, 0, 0, 0 },

  { 499, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/store param/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 500, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/store param/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 501, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/testrpc/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 502, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/zero both encoder angles/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 503, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/zero both encoder angles/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 504, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/zero output torque/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 505, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/zero output torque/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 506, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/accelerometer_x_"),
    TARGET_STRING("accelerometer_x"), 0, 0, 0, 0, 0 },

  { 507, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/accelerometer_y_"),
    TARGET_STRING("accelerometer_y"), 0, 0, 0, 0, 0 },

  { 508, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/accelerometer_z_"),
    TARGET_STRING("accelerometer_z"), 0, 0, 0, 0, 0 },

  { 509, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_1_"),
    TARGET_STRING("analog_1"), 0, 0, 0, 0, 0 },

  { 510, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_1_dot_"),
    TARGET_STRING("analog_1_dot"), 0, 0, 0, 0, 0 },

  { 511, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_2_"),
    TARGET_STRING("analog_2"), 0, 0, 0, 0, 0 },

  { 512, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_2_dot_"),
    TARGET_STRING("analog_2_dot"), 0, 0, 0, 0, 0 },

  { 513, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_diff_"),
    TARGET_STRING("analog_diff"), 0, 0, 0, 0, 0 },

  { 514, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_diff_dot_"),
    TARGET_STRING("analog_diff_dot"), 0, 0, 0, 0, 0 },

  { 515, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/bus_v_"),
    TARGET_STRING("bus_v"), 0, 0, 0, 0, 0 },

  { 516, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/debug_"),
    TARGET_STRING("debug"), 0, 0, 0, 0, 0 },

  { 517, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/faults_"),
    TARGET_STRING("faults"), 0, 2, 0, 0, 0 },

  { 518, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/motor_iq_"),
    TARGET_STRING("motor_iq"), 0, 0, 0, 0, 0 },

  { 519, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/motor_torque_"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 0 },

  { 520, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/motor_vq_avg_"),
    TARGET_STRING("motor_vq_avg"), 0, 0, 0, 0, 0 },

  { 521, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/quadrature_1_"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 0 },

  { 522, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/quadrature_1_dot_"),
    TARGET_STRING("quadrature_1_dot"), 0, 0, 0, 0, 0 },

  { 523, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/quadrature_2_"),
    TARGET_STRING("quadrature_2"), 0, 0, 0, 0, 0 },

  { 524, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/quadrature_2_dot_"),
    TARGET_STRING("quadrature_2_dot"), 0, 0, 0, 0, 0 },

  { 525, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/rpa_packet_"),
    TARGET_STRING("rpa_packet"), 0, 3, 1, 0, 0 },

  { 526, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/ssi_"),
    TARGET_STRING("ssi"), 0, 0, 0, 0, 0 },

  { 527, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/ssi_dot_"),
    TARGET_STRING("ssi_dot"), 0, 0, 0, 0, 0 },

  { 528, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 529, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 1, 7, 0, 0, 0 },

  { 530, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 2, 7, 0, 0, 0 },

  { 531, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 3, 7, 0, 0, 0 },

  { 532, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 4, 7, 0, 0, 0 },

  { 533, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 5, 7, 0, 0, 0 },

  { 534, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 6, 7, 0, 0, 0 },

  { 535, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 7, 7, 0, 0, 0 },

  { 536, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 8, 7, 0, 0, 0 },

  { 537, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 9, 7, 0, 0, 0 },

  { 538, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 10, 7, 0, 0, 0 },

  { 539, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 11, 7, 0, 0, 0 },

  { 540, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 12, 7, 0, 0, 0 },

  { 541, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 13, 7, 0, 0, 0 },

  { 542, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 14, 7, 0, 0, 0 },

  { 543, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 15, 7, 0, 0, 0 },

  { 544, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 16, 7, 0, 0, 0 },

  { 545, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 17, 7, 0, 0, 0 },

  { 546, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 18, 2, 0, 0, 0 },

  { 547, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 19, 7, 0, 0, 0 },

  { 548, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 20, 7, 0, 0, 0 },

  { 549, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 21, 3, 1, 0, 0 },

  { 550, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/analog_diff/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 551, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/analog_diff/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 552, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/brake/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 553, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/brake/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 554, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/current/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 555, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/current/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 556, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/motor_torque/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 557, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/motor_torque/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 558, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/open/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 559, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/open/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 560, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/phase lock/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 561, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/phase lock/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 562, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/quadrature_1/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 563, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/quadrature_1/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 564, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/voltage/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 7, 0, 0 },

  { 565, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/voltage/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 3, 0, 0 },

  { 566, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/10 khz logger read/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 567, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/10 khz logger read/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 568, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/10 khz logger start/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 569, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/10 khz logger start/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 570, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/100 khz logger read/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 571, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/100 khz logger read/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 572, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/100 khz logger read2/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 573, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/100 khz logger read2/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 574, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/100 khz logger start/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 575, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/100 khz logger start/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 576, 7, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/enabled subsystem"),
    TARGET_STRING(""), 0, 3, 5, 0, 0 },

  { 577, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 578, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 8, 0, 0 },

  { 579, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/analog_limit"),
    TARGET_STRING("analog_limit"), 0, 0, 0, 0, 0 },

  { 580, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/ethercat"),
    TARGET_STRING("ethercat"), 0, 0, 0, 0, 0 },

  { 581, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/external_panic"),
    TARGET_STRING("external_panic"), 0, 0, 0, 0, 0 },

  { 582, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/motor encoder"),
    TARGET_STRING("motor_encoder"), 0, 0, 0, 0, 0 },

  { 583, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/over_current"),
    TARGET_STRING("over_current"), 0, 0, 0, 0, 0 },

  { 584, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/over_voltage"),
    TARGET_STRING("over_voltage"), 0, 0, 0, 0, 0 },

  { 585, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/quadrature_1_high"),
    TARGET_STRING("quadrature_1_high"), 0, 0, 0, 0, 0 },

  { 586, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/quadrature_1_low"),
    TARGET_STRING("quadrature_1_low"), 0, 0, 0, 0, 0 },

  { 587, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/quadrature_2_limit"),
    TARGET_STRING("quadrature_2_limit"), 0, 0, 0, 0, 0 },

  { 588, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/under_voltage"),
    TARGET_STRING("under_voltage"), 0, 0, 0, 0, 0 },

  { 589, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 590, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/Byte Unpack"),
    TARGET_STRING(""), 0, 10, 8, 0, 0 },

  { 591, 8, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/Enabled Subsystem"),
    TARGET_STRING(""), 0, 3, 5, 0, 0 },

  { 592, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 593, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/load a"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 594, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/load a1"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 595, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/load a2"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 596, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/load a3"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 597, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/load b"),
    TARGET_STRING(""), 0, 7, 0, 0, 0 },

  { 598, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 599, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/Byte Unpack"),
    TARGET_STRING(""), 0, 7, 9, 0, 0 },

  { 600, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 601, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 4, 0, 0 },

  { 602, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 0 },

  { 603, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug1/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 604, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug1/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 4, 0, 0 },

  { 605, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug1/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 0 },

  { 606, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug2/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 607, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug2/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 4, 0, 0 },

  { 608, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug2/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 0 },

  { 609, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/current_index"),
    TARGET_STRING(""), 0, 8, 0, 0, 0 },

  { 610, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/store param/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 611, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/store param/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 612, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/testrpc/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 613, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/zero both encoder angles/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 614, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/zero both encoder angles/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 615, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/zero output torque/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 616, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/zero output torque/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 2 },

  { 617, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 618, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 619, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 2, 0 },

  { 620, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 621, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 3, 0 },

  { 622, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 623, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 4, 0 },

  { 624, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 625, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 5, 0 },

  { 626, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 627, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 6, 0 },

  { 628, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 629, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 7, 0 },

  { 630, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 631, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 8, 0 },

  { 632, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 633, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 9, 0 },

  { 634, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 635, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 10, 0 },

  { 636, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 637, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 11, 0 },

  { 638, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 639, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 640, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 641, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 2, 0 },

  { 642, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 643, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 3, 0 },

  { 644, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 645, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 4, 0 },

  { 646, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 647, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 5, 0 },

  { 648, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 649, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 6, 0 },

  { 650, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 651, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 7, 0 },

  { 652, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 653, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 8, 0 },

  { 654, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 655, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 9, 0 },

  { 656, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 657, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 10, 0 },

  { 658, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 659, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 11, 0 },

  { 660, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 0 },

  { 661, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running/Output"),
    TARGET_STRING(""), 0, 2, 0, 0, 0 },

  { 662, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/Compare To Constant/Compare"),
    TARGET_STRING(""), 0, 4, 0, 0, 0 },

  { 663, 3, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/enabled subsystem/In1"),
    TARGET_STRING(""), 0, 3, 5, 0, 0 },

  { 664, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/Compare To Constant/Compare"),
    TARGET_STRING(""), 0, 4, 0, 0, 0 },

  { 665, 4, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/Enabled Subsystem/In1"),
    TARGET_STRING(""), 0, 3, 5, 0, 0 },

  { 666, 5, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING("type"), 0, 8, 0, 0, 0 },

  { 667, 5, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING("id"), 1, 7, 0, 0, 0 },

  { 668, 5, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING(""), 2, 8, 0, 0, 0 },

  { 669, 5, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING(""), 3, 7, 0, 0, 0 },

  { 670, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 671, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 0 },

  { 672, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 10, 0, 0 },

  { 673, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/Data Type Conversion"),
    TARGET_STRING(""), 0, 7, 10, 0, 0 },

  { 674, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running/Output"),
    TARGET_STRING(""), 0, 2, 0, 0, 0 },

  { 675, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/Compare To Constant/Compare"),
    TARGET_STRING(""), 0, 4, 0, 0, 0 },

  { 676, 7, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/enabled subsystem/In1"),
    TARGET_STRING(""), 0, 3, 5, 0, 0 },

  { 677, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/Compare To Constant/Compare"),
    TARGET_STRING(""), 0, 4, 0, 0, 0 },

  { 678, 8, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/Enabled Subsystem/In1"),
    TARGET_STRING(""), 0, 3, 5, 0, 0 },

  { 679, 9, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING("type"), 0, 8, 0, 0, 0 },

  { 680, 9, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING("id"), 1, 7, 0, 0, 0 },

  { 681, 9, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING(""), 2, 8, 0, 0, 0 },

  { 682, 9, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING(""), 3, 7, 0, 0, 0 },

  { 683, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 684, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 5, 0, 0 },

  { 685, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 10, 0, 0 },

  { 686, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/Data Type Conversion"),
    TARGET_STRING(""), 0, 7, 10, 0, 0 },

  { 687, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running/Increment Real World/FixPt Sum1"),
    TARGET_STRING(""), 0, 2, 0, 0, 0 },

  { 688, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running/Wrap To Zero/FixPt Switch"),
    TARGET_STRING(""), 0, 2, 0, 0, 0 },

  { 689, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 11, 0, 0 },

  { 690, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 691, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 692, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 693, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 694, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 695, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 696, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 697, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 698, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 699, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/bridge/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 0 },

  { 700, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/bridge/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 701, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/bridge/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 702, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 14, 0, 0 },

  { 703, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 704, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 14, 0, 0 },

  { 705, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 706, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 707, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 708, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 709, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 710, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 711, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 0 },

  { 712, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 11, 0, 0 },

  { 713, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 714, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 715, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 716, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 717, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 718, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 719, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 720, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id7"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 721, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 0 },

  { 722, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 723, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 0 },

  { 724, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 725, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 726, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running/Increment Real World/FixPt Sum1"),
    TARGET_STRING(""), 0, 2, 0, 0, 0 },

  { 727, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running/Wrap To Zero/FixPt Switch"),
    TARGET_STRING(""), 0, 2, 0, 0, 0 },

  { 728, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 11, 0, 0 },

  { 729, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 730, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 731, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 732, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 733, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 734, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 735, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 736, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 737, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 738, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/bridge/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 0 },

  { 739, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/bridge/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 740, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/bridge/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 741, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 14, 0, 0 },

  { 742, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 743, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 14, 0, 0 },

  { 744, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 745, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 746, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 747, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 748, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 749, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 750, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 0 },

  { 751, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 11, 0, 0 },

  { 752, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 753, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 754, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 755, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 756, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 757, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 758, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 759, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id7"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 760, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 0 },

  { 761, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 762, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 0 },

  { 763, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 764, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 765, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 766, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 767, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 768, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 769, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 770, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 771, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 772, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 773, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 774, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 775, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 776, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 777, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 778, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 779, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 780, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 781, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 782, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 783, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 784, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 785, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 786, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 787, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 788, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 789, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 790, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 0 },

  { 791, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 792, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 793, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 794, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 795, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 0 },

  { 796, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 797, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 798, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 799, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 800, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 801, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 802, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 803, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 804, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 0 },

  { 805, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 806, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 807, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 808, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 809, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 810, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 811, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 812, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 813, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 814, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 815, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 816, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 817, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 818, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 819, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 820, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 821, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 822, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 823, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 824, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 825, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 826, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 827, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 828, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 829, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 830, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 831, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 832, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 833, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 834, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 835, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 836, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 837, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 838, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 839, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 840, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 841, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 842, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 843, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 844, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 845, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 846, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 847, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 848, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 849, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 850, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 851, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 852, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 853, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 854, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 855, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 0 },

  { 856, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 857, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 858, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 859, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 860, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 0 },

  { 861, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 862, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 863, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 864, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 865, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 866, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 867, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 868, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 869, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 0 },

  { 870, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 871, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 872, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 873, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 874, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 875, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 876, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 877, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 878, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 879, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 880, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 881, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 882, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 883, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 884, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 885, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 886, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 887, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 888, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 0 },

  { 889, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 890, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 891, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 892, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 893, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 894, 0, TARGET_STRING(
    "drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  {
    0, 0, (NULL), (NULL), 0, 0, 0, 0, 0
  }
};

/* Individual block tuning is not valid when inline parameters is *
 * selected. An empty map is produced to provide a consistent     *
 * interface independent  of inlining parameters.                 *
 */
static const rtwCAPI_BlockParameters rtBlockParameters[] = {
  /* addrMapIndex, blockPath,
   * paramName, dataTypeIndex, dimIndex, fixPtIdx
   */
  {
    0, (NULL), (NULL), 0, 0, 0
  }
};

/* Tunable variable parameters */
static const rtwCAPI_ModelParameters rtModelParameters[] = {
  /* addrMapIndex, varName, dataTypeIndex, dimIndex, fixPtIndex */
  { 895, TARGET_STRING("config"), 37, 0, 0 },

  { 0, (NULL), 0, 0, 0 }
};

#ifndef HOST_CAPI_BUILD

/* Declare Data Addresses statically */
static void* rtDataAddrMap[] = {
  &drum_bench_working_B.mode,          /* 0: Signal */
  &drum_bench_working_B.motor_torque_h,/* 1: Signal */
  &drum_bench_working_B.motor_iq_b,    /* 2: Signal */
  &drum_bench_working_B.mode_l,        /* 3: Signal */
  &drum_bench_working_B.motor_torque_px,/* 4: Signal */
  &drum_bench_working_B.motor_iq_n,    /* 5: Signal */
  &drum_bench_working_B.mode0,         /* 6: Signal */
  &drum_bench_working_B.mode1,         /* 7: Signal */
  &drum_bench_working_B.motor_iq0,     /* 8: Signal */
  &drum_bench_working_B.motor_iq1,     /* 9: Signal */
  &drum_bench_working_B.motor_torque0, /* 10: Signal */
  &drum_bench_working_B.motor_torque1, /* 11: Signal */
  &drum_bench_working_B.modeswitch,    /* 12: Signal */
  &drum_bench_working_B.modeswitch1,   /* 13: Signal */
  &drum_bench_working_B.DomainState_o1,/* 14: Signal */
  &drum_bench_working_B.DomainState_o2,/* 15: Signal */
  &drum_bench_working_B.MasterState_o1,/* 16: Signal */
  &drum_bench_working_B.MasterState_o2,/* 17: Signal */
  &drum_bench_working_B.MasterState_o3,/* 18: Signal */
  &drum_bench_working_B.FilteredDerivative.FilterCoefficient,/* 19: Signal */
  &drum_bench_working_B.BusAssignment, /* 20: Signal */
  &drum_bench_working_B.quadrature_1_angle,/* 21: Signal */
  &drum_bench_working_B.quadrature_1_angle_dot,/* 22: Signal */
  &drum_bench_working_B.quadrature_1_n,/* 23: Signal */
  &drum_bench_working_B.motor_torque_p,/* 24: Signal */
  &drum_bench_working_B.kd,            /* 25: Signal */
  &drum_bench_working_B.kp,            /* 26: Signal */
  &drum_bench_working_B.Saturation,    /* 27: Signal */
  &drum_bench_working_B.Sum_m,         /* 28: Signal */
  &drum_bench_working_B.Sum1,          /* 29: Signal */
  &drum_bench_working_B.Sum2,          /* 30: Signal */
  &drum_bench_working_B.FilteredDerivative_n.FilterCoefficient,/* 31: Signal */
  &drum_bench_working_B.BusAssignment_n4,/* 32: Signal */
  &drum_bench_working_B.quadrature_1_angle_j,/* 33: Signal */
  &drum_bench_working_B.quadrature_1_angle_dot_of,/* 34: Signal */
  &drum_bench_working_B.quadrature_1_nt,/* 35: Signal */
  &drum_bench_working_B.motor_torque_b,/* 36: Signal */
  &drum_bench_working_B.kd_j,          /* 37: Signal */
  &drum_bench_working_B.kp_h,          /* 38: Signal */
  &drum_bench_working_B.Saturation_l,  /* 39: Signal */
  &drum_bench_working_B.Sum_f,         /* 40: Signal */
  &drum_bench_working_B.Sum1_h,        /* 41: Signal */
  &drum_bench_working_B.Sum2_e,        /* 42: Signal */
  &drum_bench_working_B.BusCreator_n,  /* 43: Signal */
  &drum_bench_working_B.BusCreator_f,  /* 44: Signal */
  &drum_bench_working_B.marlin_basic_status,/* 45: Signal */
  &drum_bench_working_B.temperature_motor_winding,/* 46: Signal */
  &drum_bench_working_B.temperature_motor_housing,/* 47: Signal */
  &drum_bench_working_B.temperature_motor_housing_j,/* 48: Signal */
  &drum_bench_working_B.temperature_motor_winding_h,/* 49: Signal */
  &drum_bench_working_B.marlin_basic_status_l,/* 50: Signal */
  &drum_bench_working_B.temperature_motor_winding_k,/* 51: Signal */
  &drum_bench_working_B.temperature_motor_housing_jp,/* 52: Signal */
  &drum_bench_working_B.temperature_motor_housing_m,/* 53: Signal */
  &drum_bench_working_B.temperature_motor_winding_i,/* 54: Signal */
  &drum_bench_working_B.BusCreator,    /* 55: Signal */
  &drum_bench_working_B.rpc,           /* 56: Signal */
  &drum_bench_working_B.BusCreator_i,  /* 57: Signal */
  &drum_bench_working_B.rpc_l,         /* 58: Signal */
  &drum_bench_working_B.FilteredDerivative_c.FilterCoefficient,/* 59: Signal */
  &drum_bench_working_B.bias,          /* 60: Signal */
  &drum_bench_working_B.BusAssignment_n,/* 61: Signal */
  &drum_bench_working_B.quadrature_1_f,/* 62: Signal */
  &drum_bench_working_B.analog_1_i,    /* 63: Signal */
  &drum_bench_working_B.quadrature_1_angle_k,/* 64: Signal */
  &drum_bench_working_B.quadrature_1_angle_dot_o,/* 65: Signal */
  &drum_bench_working_B.ka,            /* 66: Signal */
  &drum_bench_working_B.kd_n,          /* 67: Signal */
  &drum_bench_working_B.Product,       /* 68: Signal */
  &drum_bench_working_B.Sum_e,         /* 69: Signal */
  &drum_bench_working_B.Sum1_o,        /* 70: Signal */
  &drum_bench_working_B.Sum2_b,        /* 71: Signal */
  &drum_bench_working_B.FilteredDerivative_p.FilterCoefficient,/* 72: Signal */
  &drum_bench_working_B.bias_c,        /* 73: Signal */
  &drum_bench_working_B.BusAssignment_nx,/* 74: Signal */
  &drum_bench_working_B.quadrature_1_o,/* 75: Signal */
  &drum_bench_working_B.analog_1_h,    /* 76: Signal */
  &drum_bench_working_B.quadrature_1_angle_o,/* 77: Signal */
  &drum_bench_working_B.quadrature_1_angle_dot_e,/* 78: Signal */
  &drum_bench_working_B.ka_l,          /* 79: Signal */
  &drum_bench_working_B.kd_l,          /* 80: Signal */
  &drum_bench_working_B.Product_i,     /* 81: Signal */
  &drum_bench_working_B.Sum_co,        /* 82: Signal */
  &drum_bench_working_B.Sum1_e,        /* 83: Signal */
  &drum_bench_working_B.Sum2_o,        /* 84: Signal */
  &drum_bench_working_B.FilteredDerivative.Filter,/* 85: Signal */
  &drum_bench_working_B.FilteredDerivative.FilterCoefficient,/* 86: Signal */
  &drum_bench_working_B.FilteredDerivative.Sum,/* 87: Signal */
  &drum_bench_working_B.FilteredDerivative_n.Filter,/* 88: Signal */
  &drum_bench_working_B.FilteredDerivative_n.FilterCoefficient,/* 89: Signal */
  &drum_bench_working_B.FilteredDerivative_n.Sum,/* 90: Signal */
  &drum_bench_working_B.DataTypeConversion_c,/* 91: Signal */
  &drum_bench_working_B.Gain_a,        /* 92: Signal */
  &drum_bench_working_B.MultiportSwitch,/* 93: Signal */
  &drum_bench_working_B.RateLimiter_b, /* 94: Signal */
  &drum_bench_working_B.Sign,          /* 95: Signal */
  &drum_bench_working_B.SineWave,      /* 96: Signal */
  &drum_bench_working_B.Sum_cn,        /* 97: Signal */
  &drum_bench_working_B.DataTypeConversion_n,/* 98: Signal */
  &drum_bench_working_B.Gain_bc,       /* 99: Signal */
  &drum_bench_working_B.MultiportSwitch_e,/* 100: Signal */
  &drum_bench_working_B.RateLimiter_k, /* 101: Signal */
  &drum_bench_working_B.Sign_a,        /* 102: Signal */
  &drum_bench_working_B.SineWave_h,    /* 103: Signal */
  &drum_bench_working_B.Sum_d,         /* 104: Signal */
  &drum_bench_working_B.DataTypeConversion_d,/* 105: Signal */
  &drum_bench_working_B.Gain_l,        /* 106: Signal */
  &drum_bench_working_B.MultiportSwitch_l,/* 107: Signal */
  &drum_bench_working_B.RateLimiter_e, /* 108: Signal */
  &drum_bench_working_B.Sign_m,        /* 109: Signal */
  &drum_bench_working_B.SineWave_f,    /* 110: Signal */
  &drum_bench_working_B.Sum_l,         /* 111: Signal */
  &drum_bench_working_B.DataTypeConversion,/* 112: Signal */
  &drum_bench_working_B.Gain_f,        /* 113: Signal */
  &drum_bench_working_B.MultiportSwitch_n,/* 114: Signal */
  &drum_bench_working_B.RateLimiter,   /* 115: Signal */
  &drum_bench_working_B.Sign_f,        /* 116: Signal */
  &drum_bench_working_B.SineWave_l,    /* 117: Signal */
  &drum_bench_working_B.Sum_c,         /* 118: Signal */
  &drum_bench_working_B.DataTypeConversion_a,/* 119: Signal */
  &drum_bench_working_B.Gain_g,        /* 120: Signal */
  &drum_bench_working_B.MultiportSwitch_b,/* 121: Signal */
  &drum_bench_working_B.RateLimiter_o, /* 122: Signal */
  &drum_bench_working_B.Sign_i,        /* 123: Signal */
  &drum_bench_working_B.SineWave_n,    /* 124: Signal */
  &drum_bench_working_B.Sum_p,         /* 125: Signal */
  &drum_bench_working_B.DataTypeConversion_l,/* 126: Signal */
  &drum_bench_working_B.Gain_ncb,      /* 127: Signal */
  &drum_bench_working_B.MultiportSwitch_h,/* 128: Signal */
  &drum_bench_working_B.RateLimiter_b5,/* 129: Signal */
  &drum_bench_working_B.Sign_ml,       /* 130: Signal */
  &drum_bench_working_B.SineWave_ff,   /* 131: Signal */
  &drum_bench_working_B.Sum_g,         /* 132: Signal */
  &drum_bench_working_B.DataTypeConversion_j,/* 133: Signal */
  &drum_bench_working_B.Gain_gv,       /* 134: Signal */
  &drum_bench_working_B.MultiportSwitch_o,/* 135: Signal */
  &drum_bench_working_B.RateLimiter_eb,/* 136: Signal */
  &drum_bench_working_B.Sign_h,        /* 137: Signal */
  &drum_bench_working_B.SineWave_hf,   /* 138: Signal */
  &drum_bench_working_B.Sum_bf,        /* 139: Signal */
  &drum_bench_working_B.DataTypeConversion_e,/* 140: Signal */
  &drum_bench_working_B.Gain_i,        /* 141: Signal */
  &drum_bench_working_B.MultiportSwitch_a,/* 142: Signal */
  &drum_bench_working_B.RateLimiter_ol,/* 143: Signal */
  &drum_bench_working_B.Sign_k,        /* 144: Signal */
  &drum_bench_working_B.SineWave_g,    /* 145: Signal */
  &drum_bench_working_B.Sum_o,         /* 146: Signal */
  &drum_bench_working_B.DataTypeConversion_m,/* 147: Signal */
  &drum_bench_working_B.Gain_m,        /* 148: Signal */
  &drum_bench_working_B.MultiportSwitch_i,/* 149: Signal */
  &drum_bench_working_B.RateLimiter_d, /* 150: Signal */
  &drum_bench_working_B.Sign_l,        /* 151: Signal */
  &drum_bench_working_B.SineWave_m,    /* 152: Signal */
  &drum_bench_working_B.Sum_cq,        /* 153: Signal */
  &drum_bench_working_B.DataTypeConversion_c2,/* 154: Signal */
  &drum_bench_working_B.Gain_nh,       /* 155: Signal */
  &drum_bench_working_B.MultiportSwitch_k,/* 156: Signal */
  &drum_bench_working_B.RateLimiter_f, /* 157: Signal */
  &drum_bench_working_B.Sign_ip,       /* 158: Signal */
  &drum_bench_working_B.SineWave_ld,   /* 159: Signal */
  &drum_bench_working_B.Sum_a,         /* 160: Signal */
  &drum_bench_working_B.motor_torque_m,/* 161: Signal */
  &drum_bench_working_B.motor_iq_a,    /* 162: Signal */
  &drum_bench_working_B.motor_vq_avg_f,/* 163: Signal */
  &drum_bench_working_B.analog_1_g,    /* 164: Signal */
  &drum_bench_working_B.analog_1_dot_h,/* 165: Signal */
  &drum_bench_working_B.analog_2_k,    /* 166: Signal */
  &drum_bench_working_B.analog_2_dot_m,/* 167: Signal */
  &drum_bench_working_B.analog_diff_h, /* 168: Signal */
  &drum_bench_working_B.analog_diff_dot_k,/* 169: Signal */
  &drum_bench_working_B.quadrature_1_h,/* 170: Signal */
  &drum_bench_working_B.quadrature_1_dot_l,/* 171: Signal */
  &drum_bench_working_B.quadrature_2_o,/* 172: Signal */
  &drum_bench_working_B.quadrature_2_dot_l,/* 173: Signal */
  &drum_bench_working_B.ssi_c,         /* 174: Signal */
  &drum_bench_working_B.ssi_dot_f,     /* 175: Signal */
  &drum_bench_working_B.accelerometer_x_n,/* 176: Signal */
  &drum_bench_working_B.accelerometer_y_a,/* 177: Signal */
  &drum_bench_working_B.accelerometer_z_l,/* 178: Signal */
  &drum_bench_working_B.faults_p,      /* 179: Signal */
  &drum_bench_working_B.bus_v_k,       /* 180: Signal */
  &drum_bench_working_B.rpa_packet_k[0],/* 181: Signal */
  &drum_bench_working_B.accelerometer_x_e,/* 182: Signal */
  &drum_bench_working_B.accelerometer_y_b,/* 183: Signal */
  &drum_bench_working_B.accelerometer_z_d,/* 184: Signal */
  &drum_bench_working_B.analog_1_o,    /* 185: Signal */
  &drum_bench_working_B.analog_1_dot_d,/* 186: Signal */
  &drum_bench_working_B.analog_2_p,    /* 187: Signal */
  &drum_bench_working_B.analog_2_dot_l,/* 188: Signal */
  &drum_bench_working_B.analog_diff_b, /* 189: Signal */
  &drum_bench_working_B.analog_diff_dot_f,/* 190: Signal */
  &drum_bench_working_B.bus_v_d,       /* 191: Signal */
  &drum_bench_working_B.motor_iq_g,    /* 192: Signal */
  &drum_bench_working_B.motor_torque_f,/* 193: Signal */
  &drum_bench_working_B.motor_vq_avg_b,/* 194: Signal */
  &drum_bench_working_B.quadrature_1_j,/* 195: Signal */
  &drum_bench_working_B.quadrature_1_dot_c,/* 196: Signal */
  &drum_bench_working_B.quadrature_2_l,/* 197: Signal */
  &drum_bench_working_B.quadrature_2_dot_o,/* 198: Signal */
  &drum_bench_working_B.ssi_f,         /* 199: Signal */
  &drum_bench_working_B.ssi_dot_k,     /* 200: Signal */
  &drum_bench_working_B.rpa_float[0],  /* 201: Signal */
  &drum_bench_working_B.motor_torque_pd,/* 202: Signal */
  &drum_bench_working_B.motor_iq_f,    /* 203: Signal */
  &drum_bench_working_B.motor_vq_avg_n,/* 204: Signal */
  &drum_bench_working_B.analog_1_m,    /* 205: Signal */
  &drum_bench_working_B.analog_1_dot_f,/* 206: Signal */
  &drum_bench_working_B.analog_2_b,    /* 207: Signal */
  &drum_bench_working_B.analog_2_dot_e,/* 208: Signal */
  &drum_bench_working_B.analog_diff_hz,/* 209: Signal */
  &drum_bench_working_B.analog_diff_dot_h,/* 210: Signal */
  &drum_bench_working_B.quadrature_1_a,/* 211: Signal */
  &drum_bench_working_B.quadrature_1_dot_j,/* 212: Signal */
  &drum_bench_working_B.quadrature_2_lz,/* 213: Signal */
  &drum_bench_working_B.quadrature_2_dot_o4,/* 214: Signal */
  &drum_bench_working_B.ssi_n,         /* 215: Signal */
  &drum_bench_working_B.ssi_dot_l,     /* 216: Signal */
  &drum_bench_working_B.accelerometer_x_f,/* 217: Signal */
  &drum_bench_working_B.accelerometer_y_jg,/* 218: Signal */
  &drum_bench_working_B.accelerometer_z_g,/* 219: Signal */
  &drum_bench_working_B.faults_o,      /* 220: Signal */
  &drum_bench_working_B.bus_v_p,       /* 221: Signal */
  &drum_bench_working_B.rpa_packet_g[0],/* 222: Signal */
  &drum_bench_working_B.accelerometer_x_d,/* 223: Signal */
  &drum_bench_working_B.accelerometer_y_e,/* 224: Signal */
  &drum_bench_working_B.accelerometer_z_b,/* 225: Signal */
  &drum_bench_working_B.analog_1_b,    /* 226: Signal */
  &drum_bench_working_B.analog_1_dot_b,/* 227: Signal */
  &drum_bench_working_B.analog_2_a,    /* 228: Signal */
  &drum_bench_working_B.analog_2_dot_k,/* 229: Signal */
  &drum_bench_working_B.analog_diff_n, /* 230: Signal */
  &drum_bench_working_B.analog_diff_dot_i,/* 231: Signal */
  &drum_bench_working_B.bus_v_pu,      /* 232: Signal */
  &drum_bench_working_B.motor_iq_gt,   /* 233: Signal */
  &drum_bench_working_B.motor_torque_pz,/* 234: Signal */
  &drum_bench_working_B.motor_vq_avg_i,/* 235: Signal */
  &drum_bench_working_B.quadrature_1_ib,/* 236: Signal */
  &drum_bench_working_B.quadrature_1_dot_f,/* 237: Signal */
  &drum_bench_working_B.quadrature_2_g,/* 238: Signal */
  &drum_bench_working_B.quadrature_2_dot_e,/* 239: Signal */
  &drum_bench_working_B.ssi_h,         /* 240: Signal */
  &drum_bench_working_B.ssi_dot_o,     /* 241: Signal */
  &drum_bench_working_B.rpa_float_f[0],/* 242: Signal */
  &drum_bench_working_B.mode_k,        /* 243: Signal */
  &drum_bench_working_B.motor_iq_k,    /* 244: Signal */
  &drum_bench_working_B.motor_id,      /* 245: Signal */
  &drum_bench_working_B.motor_vq,      /* 246: Signal */
  &drum_bench_working_B.motor_vd,      /* 247: Signal */
  &drum_bench_working_B.analog_diff_g, /* 248: Signal */
  &drum_bench_working_B.analog_diff_dot_o,/* 249: Signal */
  &drum_bench_working_B.quadrature_1_angle_b,/* 250: Signal */
  &drum_bench_working_B.quadrature_1_angle_dot_j,/* 251: Signal */
  &drum_bench_working_B.motor_torque_mv,/* 252: Signal */
  &drum_bench_working_B.mode_a,        /* 253: Signal */
  &drum_bench_working_B.command_e[0],  /* 254: Signal */
  &drum_bench_working_B.eA1[0],        /* 255: Signal */
  &drum_bench_working_B.eA2[0],        /* 256: Signal */
  &drum_bench_working_B.resistance,    /* 257: Signal */
  &drum_bench_working_B.MathFunction,  /* 258: Signal */
  &drum_bench_working_B.Sum[0],        /* 259: Signal */
  &drum_bench_working_B.Delay[0],      /* 260: Signal */
  &drum_bench_working_B.sf_selectrpc.type,/* 261: Signal */
  &drum_bench_working_B.sf_selectrpc.id,/* 262: Signal */
  &drum_bench_working_B.sf_selectrpc.data[0],/* 263: Signal */
  &drum_bench_working_B.VectorConcatenate[0],/* 264: Signal */
  &drum_bench_working_B.rpc_i[0],      /* 265: Signal */
  &drum_bench_working_B.type,          /* 266: Signal */
  &drum_bench_working_B.id,            /* 267: Signal */
  &drum_bench_working_B.data[0],       /* 268: Signal */
  &drum_bench_working_B.mode_g,        /* 269: Signal */
  &drum_bench_working_B.motor_iq_m,    /* 270: Signal */
  &drum_bench_working_B.motor_id_k,    /* 271: Signal */
  &drum_bench_working_B.motor_vq_d,    /* 272: Signal */
  &drum_bench_working_B.motor_vd_m,    /* 273: Signal */
  &drum_bench_working_B.analog_diff_i, /* 274: Signal */
  &drum_bench_working_B.analog_diff_dot_ky,/* 275: Signal */
  &drum_bench_working_B.quadrature_1_angle_c,/* 276: Signal */
  &drum_bench_working_B.quadrature_1_angle_dot_d,/* 277: Signal */
  &drum_bench_working_B.motor_torque_hy,/* 278: Signal */
  &drum_bench_working_B.mode_e,        /* 279: Signal */
  &drum_bench_working_B.command_c[0],  /* 280: Signal */
  &drum_bench_working_B.eA1_c[0],      /* 281: Signal */
  &drum_bench_working_B.eA2_f[0],      /* 282: Signal */
  &drum_bench_working_B.resistance_e,  /* 283: Signal */
  &drum_bench_working_B.MathFunction_p,/* 284: Signal */
  &drum_bench_working_B.Sum_do[0],     /* 285: Signal */
  &drum_bench_working_B.Delay_l[0],    /* 286: Signal */
  &drum_bench_working_B.sf_selectrpc_j.type,/* 287: Signal */
  &drum_bench_working_B.sf_selectrpc_j.id,/* 288: Signal */
  &drum_bench_working_B.sf_selectrpc_j.data[0],/* 289: Signal */
  &drum_bench_working_B.VectorConcatenate_a[0],/* 290: Signal */
  &drum_bench_working_B.rpc_n[0],      /* 291: Signal */
  &drum_bench_working_B.type_b,        /* 292: Signal */
  &drum_bench_working_B.id_p,          /* 293: Signal */
  &drum_bench_working_B.data_a[0],     /* 294: Signal */
  &drum_bench_working_B.FilteredDerivative_c.Filter,/* 295: Signal */
  &drum_bench_working_B.FilteredDerivative_c.FilterCoefficient,/* 296: Signal */
  &drum_bench_working_B.FilteredDerivative_c.Sum,/* 297: Signal */
  &drum_bench_working_B.FilteredDerivative_p.Filter,/* 298: Signal */
  &drum_bench_working_B.FilteredDerivative_p.FilterCoefficient,/* 299: Signal */
  &drum_bench_working_B.FilteredDerivative_p.Sum,/* 300: Signal */
  &drum_bench_working_B.Clock1,        /* 301: Signal */
  &drum_bench_working_B.Gain,          /* 302: Signal */
  &drum_bench_working_B.Product_j,     /* 303: Signal */
  &drum_bench_working_B.Product1,      /* 304: Signal */
  &drum_bench_working_B.Product2,      /* 305: Signal */
  &drum_bench_working_B.Sum_j,         /* 306: Signal */
  &drum_bench_working_B.Output,        /* 307: Signal */
  &drum_bench_working_B.Clock1_n,      /* 308: Signal */
  &drum_bench_working_B.Gain_b,        /* 309: Signal */
  &drum_bench_working_B.Product_p,     /* 310: Signal */
  &drum_bench_working_B.Product1_o,    /* 311: Signal */
  &drum_bench_working_B.Product2_p,    /* 312: Signal */
  &drum_bench_working_B.Sum_k,         /* 313: Signal */
  &drum_bench_working_B.Output_p,      /* 314: Signal */
  &drum_bench_working_B.Clock1_e,      /* 315: Signal */
  &drum_bench_working_B.Gain_n,        /* 316: Signal */
  &drum_bench_working_B.Product_b,     /* 317: Signal */
  &drum_bench_working_B.Product1_c,    /* 318: Signal */
  &drum_bench_working_B.Product2_n,    /* 319: Signal */
  &drum_bench_working_B.Sum_kn,        /* 320: Signal */
  &drum_bench_working_B.Output_n,      /* 321: Signal */
  &drum_bench_working_B.Clock1_g,      /* 322: Signal */
  &drum_bench_working_B.Gain_nc,       /* 323: Signal */
  &drum_bench_working_B.Product_d,     /* 324: Signal */
  &drum_bench_working_B.Product1_i,    /* 325: Signal */
  &drum_bench_working_B.Product2_i,    /* 326: Signal */
  &drum_bench_working_B.Sum_i,         /* 327: Signal */
  &drum_bench_working_B.Output_h,      /* 328: Signal */
  &drum_bench_working_B.Clock1_j,      /* 329: Signal */
  &drum_bench_working_B.Gain_nz,       /* 330: Signal */
  &drum_bench_working_B.Product_h,     /* 331: Signal */
  &drum_bench_working_B.Product1_f,    /* 332: Signal */
  &drum_bench_working_B.Product2_l,    /* 333: Signal */
  &drum_bench_working_B.Sum_b,         /* 334: Signal */
  &drum_bench_working_B.Output_c,      /* 335: Signal */
  &drum_bench_working_B.Clock1_gp,     /* 336: Signal */
  &drum_bench_working_B.Gain_a3,       /* 337: Signal */
  &drum_bench_working_B.Product_bh,    /* 338: Signal */
  &drum_bench_working_B.Product1_e,    /* 339: Signal */
  &drum_bench_working_B.Product2_d,    /* 340: Signal */
  &drum_bench_working_B.Sum_i2,        /* 341: Signal */
  &drum_bench_working_B.Output_i,      /* 342: Signal */
  &drum_bench_working_B.Clock1_f,      /* 343: Signal */
  &drum_bench_working_B.Gain_d,        /* 344: Signal */
  &drum_bench_working_B.Product_ie,    /* 345: Signal */
  &drum_bench_working_B.Product1_p,    /* 346: Signal */
  &drum_bench_working_B.Product2_np,   /* 347: Signal */
  &drum_bench_working_B.Sum_c3,        /* 348: Signal */
  &drum_bench_working_B.Output_f,      /* 349: Signal */
  &drum_bench_working_B.Clock1_l,      /* 350: Signal */
  &drum_bench_working_B.Gain_h,        /* 351: Signal */
  &drum_bench_working_B.Product_k,     /* 352: Signal */
  &drum_bench_working_B.Product1_pt,   /* 353: Signal */
  &drum_bench_working_B.Product2_j,    /* 354: Signal */
  &drum_bench_working_B.Sum_dl,        /* 355: Signal */
  &drum_bench_working_B.Output_e,      /* 356: Signal */
  &drum_bench_working_B.Clock1_d,      /* 357: Signal */
  &drum_bench_working_B.Gain_c,        /* 358: Signal */
  &drum_bench_working_B.Product_kg,    /* 359: Signal */
  &drum_bench_working_B.Product1_j,    /* 360: Signal */
  &drum_bench_working_B.Product2_g,    /* 361: Signal */
  &drum_bench_working_B.Sum_en,        /* 362: Signal */
  &drum_bench_working_B.Output_m,      /* 363: Signal */
  &drum_bench_working_B.Clock1_h,      /* 364: Signal */
  &drum_bench_working_B.Gain_p,        /* 365: Signal */
  &drum_bench_working_B.Product_if,    /* 366: Signal */
  &drum_bench_working_B.Product1_j2,   /* 367: Signal */
  &drum_bench_working_B.Product2_gp,   /* 368: Signal */
  &drum_bench_working_B.Sum_ke,        /* 369: Signal */
  &drum_bench_working_B.Output_l,      /* 370: Signal */
  &drum_bench_working_B.analog,        /* 371: Signal */
  &drum_bench_working_B.communication_fault,/* 372: Signal */
  &drum_bench_working_B.current,       /* 373: Signal */
  &drum_bench_working_B.ethercat_m,    /* 374: Signal */
  &drum_bench_working_B.external_panic_e,/* 375: Signal */
  &drum_bench_working_B.hard_fault,    /* 376: Signal */
  &drum_bench_working_B.quadrature,    /* 377: Signal */
  &drum_bench_working_B.raw_integer,   /* 378: Signal */
  &drum_bench_working_B.soft_fault,    /* 379: Signal */
  &drum_bench_working_B.ssi_i,         /* 380: Signal */
  &drum_bench_working_B.temperature,   /* 381: Signal */
  &drum_bench_working_B.voltage,       /* 382: Signal */
  &drum_bench_working_B.analog_i,      /* 383: Signal */
  &drum_bench_working_B.communication_fault_o,/* 384: Signal */
  &drum_bench_working_B.current_h,     /* 385: Signal */
  &drum_bench_working_B.ethercat_e,    /* 386: Signal */
  &drum_bench_working_B.external_panic_o,/* 387: Signal */
  &drum_bench_working_B.hard_fault_e,  /* 388: Signal */
  &drum_bench_working_B.quadrature_l,  /* 389: Signal */
  &drum_bench_working_B.raw_integer_l, /* 390: Signal */
  &drum_bench_working_B.soft_fault_a,  /* 391: Signal */
  &drum_bench_working_B.ssi_m,         /* 392: Signal */
  &drum_bench_working_B.temperature_b, /* 393: Signal */
  &drum_bench_working_B.voltage_f,     /* 394: Signal */
  &drum_bench_working_B.accelerometer_x,/* 395: Signal */
  &drum_bench_working_B.accelerometer_y,/* 396: Signal */
  &drum_bench_working_B.accelerometer_z,/* 397: Signal */
  &drum_bench_working_B.analog_1,      /* 398: Signal */
  &drum_bench_working_B.analog_1_dot,  /* 399: Signal */
  &drum_bench_working_B.analog_2,      /* 400: Signal */
  &drum_bench_working_B.analog_2_dot,  /* 401: Signal */
  &drum_bench_working_B.analog_diff,   /* 402: Signal */
  &drum_bench_working_B.analog_diff_dot,/* 403: Signal */
  &drum_bench_working_B.bus_v,         /* 404: Signal */
  &drum_bench_working_B.debug,         /* 405: Signal */
  &drum_bench_working_B.faults,        /* 406: Signal */
  &drum_bench_working_B.motor_iq,      /* 407: Signal */
  &drum_bench_working_B.motor_torque,  /* 408: Signal */
  &drum_bench_working_B.motor_vq_avg,  /* 409: Signal */
  &drum_bench_working_B.quadrature_1,  /* 410: Signal */
  &drum_bench_working_B.quadrature_1_dot,/* 411: Signal */
  &drum_bench_working_B.quadrature_2,  /* 412: Signal */
  &drum_bench_working_B.quadrature_2_dot,/* 413: Signal */
  &drum_bench_working_B.rpa_packet[0], /* 414: Signal */
  &drum_bench_working_B.ssi,           /* 415: Signal */
  &drum_bench_working_B.ssi_dot,       /* 416: Signal */
  &drum_bench_working_B.marlin_ec_o1,  /* 417: Signal */
  &drum_bench_working_B.marlin_ec_o2,  /* 418: Signal */
  &drum_bench_working_B.marlin_ec_o3,  /* 419: Signal */
  &drum_bench_working_B.marlin_ec_o4,  /* 420: Signal */
  &drum_bench_working_B.marlin_ec_o5,  /* 421: Signal */
  &drum_bench_working_B.marlin_ec_o6,  /* 422: Signal */
  &drum_bench_working_B.marlin_ec_o7,  /* 423: Signal */
  &drum_bench_working_B.marlin_ec_o8,  /* 424: Signal */
  &drum_bench_working_B.marlin_ec_o9,  /* 425: Signal */
  &drum_bench_working_B.marlin_ec_o10, /* 426: Signal */
  &drum_bench_working_B.marlin_ec_o11, /* 427: Signal */
  &drum_bench_working_B.marlin_ec_o12, /* 428: Signal */
  &drum_bench_working_B.marlin_ec_o13, /* 429: Signal */
  &drum_bench_working_B.marlin_ec_o14, /* 430: Signal */
  &drum_bench_working_B.marlin_ec_o15, /* 431: Signal */
  &drum_bench_working_B.marlin_ec_o16, /* 432: Signal */
  &drum_bench_working_B.marlin_ec_o17, /* 433: Signal */
  &drum_bench_working_B.marlin_ec_o18, /* 434: Signal */
  &drum_bench_working_B.marlin_ec_o19, /* 435: Signal */
  &drum_bench_working_B.marlin_ec_o20, /* 436: Signal */
  &drum_bench_working_B.marlin_ec_o21, /* 437: Signal */
  &drum_bench_working_B.marlin_ec_o22[0],/* 438: Signal */
  &drum_bench_working_B.DataTypeConversion1_f[0],/* 439: Signal */
  &drum_bench_working_B.command_g[0],  /* 440: Signal */
  &drum_bench_working_B.DataTypeConversion1_o[0],/* 441: Signal */
  &drum_bench_working_B.command_j[0],  /* 442: Signal */
  &drum_bench_working_B.DataTypeConversion1_b[0],/* 443: Signal */
  &drum_bench_working_B.command_l[0],  /* 444: Signal */
  &drum_bench_working_B.DataTypeConversion1_h[0],/* 445: Signal */
  &drum_bench_working_B.command_a[0],  /* 446: Signal */
  &drum_bench_working_B.DataTypeConversion1[0],/* 447: Signal */
  &drum_bench_working_B.command[0],    /* 448: Signal */
  &drum_bench_working_B.DataTypeConversion1_oj[0],/* 449: Signal */
  &drum_bench_working_B.command_k[0],  /* 450: Signal */
  &drum_bench_working_B.DataTypeConversion1_fm[0],/* 451: Signal */
  &drum_bench_working_B.command_gi[0], /* 452: Signal */
  &drum_bench_working_B.DataTypeConversion1_e[0],/* 453: Signal */
  &drum_bench_working_B.command_f[0],  /* 454: Signal */
  &drum_bench_working_B.BusCreator_nf, /* 455: Signal */
  (void *) &drum_bench_working_ConstB.data[0],/* 456: Signal */
  &drum_bench_working_B.BusCreator_k,  /* 457: Signal */
  (void *) &drum_bench_working_ConstB.data_p[0],/* 458: Signal */
  &drum_bench_working_B.BusCreator_iq, /* 459: Signal */
  (void *) &drum_bench_working_ConstB.data_f[0],/* 460: Signal */
  &drum_bench_working_B.BusCreator_o,  /* 461: Signal */
  (void *) &drum_bench_working_ConstB.data_c[0],/* 462: Signal */
  &drum_bench_working_B.BusCreator_l,  /* 463: Signal */
  (void *) &drum_bench_working_ConstB.data_h[0],/* 464: Signal */
  &drum_bench_working_B.enabledsubsystem.In1[0],/* 465: Signal */
  &drum_bench_working_B.BusCreator_d,  /* 466: Signal */
  &drum_bench_working_B.DataTypeConversion_cm[0],/* 467: Signal */
  &drum_bench_working_B.analog_limit,  /* 468: Signal */
  &drum_bench_working_B.ethercat,      /* 469: Signal */
  &drum_bench_working_B.external_panic,/* 470: Signal */
  &drum_bench_working_B.motor_encoder, /* 471: Signal */
  &drum_bench_working_B.over_current,  /* 472: Signal */
  &drum_bench_working_B.over_voltage,  /* 473: Signal */
  &drum_bench_working_B.quadrature_1_high,/* 474: Signal */
  &drum_bench_working_B.quadrature_1_low,/* 475: Signal */
  &drum_bench_working_B.quadrature_2_limit,/* 476: Signal */
  &drum_bench_working_B.under_voltage, /* 477: Signal */
  (void *) &drum_bench_working_ConstB.data_d[0],/* 478: Signal */
  &drum_bench_working_B.ByteUnpack_e[0],/* 479: Signal */
  &drum_bench_working_B.EnabledSubsystem.In1[0],/* 480: Signal */
  &drum_bench_working_B.BusCreator_ld, /* 481: Signal */
  &drum_bench_working_B.loada,         /* 482: Signal */
  &drum_bench_working_B.loada1,        /* 483: Signal */
  &drum_bench_working_B.loada2,        /* 484: Signal */
  &drum_bench_working_B.loada3,        /* 485: Signal */
  &drum_bench_working_B.loadb,         /* 486: Signal */
  (void *) &drum_bench_working_ConstB.data_cz[0],/* 487: Signal */
  &drum_bench_working_B.ByteUnpack[0], /* 488: Signal */
  &drum_bench_working_B.BusCreator_c,  /* 489: Signal */
  &drum_bench_working_B.DataTypeConversion_g[0],/* 490: Signal */
  &drum_bench_working_B.data_e[0],     /* 491: Signal */
  &drum_bench_working_B.BusCreator_hl, /* 492: Signal */
  &drum_bench_working_B.DataTypeConversion_h[0],/* 493: Signal */
  &drum_bench_working_B.data_b[0],     /* 494: Signal */
  &drum_bench_working_B.BusCreator_nh, /* 495: Signal */
  &drum_bench_working_B.DataTypeConversion_ee[0],/* 496: Signal */
  &drum_bench_working_B.data_n[0],     /* 497: Signal */
  &drum_bench_working_B.current_index, /* 498: Signal */
  &drum_bench_working_B.BusCreator_h,  /* 499: Signal */
  (void *) &drum_bench_working_ConstB.data_e[0],/* 500: Signal */
  &drum_bench_working_B.BusCreator_g,  /* 501: Signal */
  &drum_bench_working_B.BusCreator_bc, /* 502: Signal */
  (void *) &drum_bench_working_ConstB.data_o[0],/* 503: Signal */
  &drum_bench_working_B.BusCreator_b,  /* 504: Signal */
  (void *) &drum_bench_working_ConstB.data_cu[0],/* 505: Signal */
  &drum_bench_working_B.accelerometer_x_ej,/* 506: Signal */
  &drum_bench_working_B.accelerometer_y_j,/* 507: Signal */
  &drum_bench_working_B.accelerometer_z_i,/* 508: Signal */
  &drum_bench_working_B.analog_1_on,   /* 509: Signal */
  &drum_bench_working_B.analog_1_dot_k,/* 510: Signal */
  &drum_bench_working_B.analog_2_h,    /* 511: Signal */
  &drum_bench_working_B.analog_2_dot_h,/* 512: Signal */
  &drum_bench_working_B.analog_diff_o, /* 513: Signal */
  &drum_bench_working_B.analog_diff_dot_m,/* 514: Signal */
  &drum_bench_working_B.bus_v_l,       /* 515: Signal */
  &drum_bench_working_B.debug_d,       /* 516: Signal */
  &drum_bench_working_B.faults_k,      /* 517: Signal */
  &drum_bench_working_B.motor_iq_bj,   /* 518: Signal */
  &drum_bench_working_B.motor_torque_l,/* 519: Signal */
  &drum_bench_working_B.motor_vq_avg_e,/* 520: Signal */
  &drum_bench_working_B.quadrature_1_i,/* 521: Signal */
  &drum_bench_working_B.quadrature_1_dot_e,/* 522: Signal */
  &drum_bench_working_B.quadrature_2_h,/* 523: Signal */
  &drum_bench_working_B.quadrature_2_dot_b,/* 524: Signal */
  &drum_bench_working_B.rpa_packet_b[0],/* 525: Signal */
  &drum_bench_working_B.ssi_b,         /* 526: Signal */
  &drum_bench_working_B.ssi_dot_b,     /* 527: Signal */
  &drum_bench_working_B.marlin_ec_o1_k,/* 528: Signal */
  &drum_bench_working_B.marlin_ec_o2_f,/* 529: Signal */
  &drum_bench_working_B.marlin_ec_o3_h,/* 530: Signal */
  &drum_bench_working_B.marlin_ec_o4_a,/* 531: Signal */
  &drum_bench_working_B.marlin_ec_o5_l,/* 532: Signal */
  &drum_bench_working_B.marlin_ec_o6_j,/* 533: Signal */
  &drum_bench_working_B.marlin_ec_o7_c,/* 534: Signal */
  &drum_bench_working_B.marlin_ec_o8_j,/* 535: Signal */
  &drum_bench_working_B.marlin_ec_o9_j,/* 536: Signal */
  &drum_bench_working_B.marlin_ec_o10_n,/* 537: Signal */
  &drum_bench_working_B.marlin_ec_o11_d,/* 538: Signal */
  &drum_bench_working_B.marlin_ec_o12_h,/* 539: Signal */
  &drum_bench_working_B.marlin_ec_o13_b,/* 540: Signal */
  &drum_bench_working_B.marlin_ec_o14_e,/* 541: Signal */
  &drum_bench_working_B.marlin_ec_o15_h,/* 542: Signal */
  &drum_bench_working_B.marlin_ec_o16_c,/* 543: Signal */
  &drum_bench_working_B.marlin_ec_o17_a,/* 544: Signal */
  &drum_bench_working_B.marlin_ec_o18_g,/* 545: Signal */
  &drum_bench_working_B.marlin_ec_o19_n,/* 546: Signal */
  &drum_bench_working_B.marlin_ec_o20_k,/* 547: Signal */
  &drum_bench_working_B.marlin_ec_o21_m,/* 548: Signal */
  &drum_bench_working_B.marlin_ec_o22_a[0],/* 549: Signal */
  &drum_bench_working_B.DataTypeConversion1_fj[0],/* 550: Signal */
  &drum_bench_working_B.command_hl[0], /* 551: Signal */
  &drum_bench_working_B.DataTypeConversion1_bm[0],/* 552: Signal */
  &drum_bench_working_B.command_n[0],  /* 553: Signal */
  &drum_bench_working_B.DataTypeConversion1_a[0],/* 554: Signal */
  &drum_bench_working_B.command_i[0],  /* 555: Signal */
  &drum_bench_working_B.DataTypeConversion1_c[0],/* 556: Signal */
  &drum_bench_working_B.command_jo[0], /* 557: Signal */
  &drum_bench_working_B.DataTypeConversion1_d[0],/* 558: Signal */
  &drum_bench_working_B.command_li[0], /* 559: Signal */
  &drum_bench_working_B.DataTypeConversion1_ev[0],/* 560: Signal */
  &drum_bench_working_B.command_o[0],  /* 561: Signal */
  &drum_bench_working_B.DataTypeConversion1_aj[0],/* 562: Signal */
  &drum_bench_working_B.command_gh[0], /* 563: Signal */
  &drum_bench_working_B.DataTypeConversion1_ey[0],/* 564: Signal */
  &drum_bench_working_B.command_h[0],  /* 565: Signal */
  &drum_bench_working_B.BusCreator_br, /* 566: Signal */
  (void *) &drum_bench_working_ConstB.data_ef[0],/* 567: Signal */
  &drum_bench_working_B.BusCreator_ih, /* 568: Signal */
  (void *) &drum_bench_working_ConstB.data_c4[0],/* 569: Signal */
  &drum_bench_working_B.BusCreator_g1, /* 570: Signal */
  (void *) &drum_bench_working_ConstB.data_g[0],/* 571: Signal */
  &drum_bench_working_B.BusCreator_i2, /* 572: Signal */
  (void *) &drum_bench_working_ConstB.data_b[0],/* 573: Signal */
  &drum_bench_working_B.BusCreator_lg, /* 574: Signal */
  (void *) &drum_bench_working_ConstB.data_fw[0],/* 575: Signal */
  &drum_bench_working_B.enabledsubsystem_l.In1[0],/* 576: Signal */
  &drum_bench_working_B.BusCreator_d1, /* 577: Signal */
  &drum_bench_working_B.DataTypeConversion_i[0],/* 578: Signal */
  &drum_bench_working_B.analog_limit_k,/* 579: Signal */
  &drum_bench_working_B.ethercat_l,    /* 580: Signal */
  &drum_bench_working_B.external_panic_l,/* 581: Signal */
  &drum_bench_working_B.motor_encoder_h,/* 582: Signal */
  &drum_bench_working_B.over_current_p,/* 583: Signal */
  &drum_bench_working_B.over_voltage_o,/* 584: Signal */
  &drum_bench_working_B.quadrature_1_high_p,/* 585: Signal */
  &drum_bench_working_B.quadrature_1_low_c,/* 586: Signal */
  &drum_bench_working_B.quadrature_2_limit_a,/* 587: Signal */
  &drum_bench_working_B.under_voltage_l,/* 588: Signal */
  (void *) &drum_bench_working_ConstB.data_og[0],/* 589: Signal */
  &drum_bench_working_B.ByteUnpack_eb[0],/* 590: Signal */
  &drum_bench_working_B.EnabledSubsystem_k.In1[0],/* 591: Signal */
  &drum_bench_working_B.BusCreator_ow, /* 592: Signal */
  &drum_bench_working_B.loada_c,       /* 593: Signal */
  &drum_bench_working_B.loada1_h,      /* 594: Signal */
  &drum_bench_working_B.loada2_p,      /* 595: Signal */
  &drum_bench_working_B.loada3_o,      /* 596: Signal */
  &drum_bench_working_B.loadb_a,       /* 597: Signal */
  (void *) &drum_bench_working_ConstB.data_e0[0],/* 598: Signal */
  &drum_bench_working_B.ByteUnpack_h[0],/* 599: Signal */
  &drum_bench_working_B.BusCreator_dj, /* 600: Signal */
  &drum_bench_working_B.DataTypeConversion_k[0],/* 601: Signal */
  &drum_bench_working_B.data_o[0],     /* 602: Signal */
  &drum_bench_working_B.BusCreator_om, /* 603: Signal */
  &drum_bench_working_B.DataTypeConversion_k0[0],/* 604: Signal */
  &drum_bench_working_B.data_bl[0],    /* 605: Signal */
  &drum_bench_working_B.BusCreator_co, /* 606: Signal */
  &drum_bench_working_B.DataTypeConversion_iv[0],/* 607: Signal */
  &drum_bench_working_B.data_mh[0],    /* 608: Signal */
  &drum_bench_working_B.current_index_c,/* 609: Signal */
  &drum_bench_working_B.BusCreator_dy, /* 610: Signal */
  (void *) &drum_bench_working_ConstB.data_ec[0],/* 611: Signal */
  &drum_bench_working_B.BusCreator_gs, /* 612: Signal */
  &drum_bench_working_B.BusCreator_bw, /* 613: Signal */
  (void *) &drum_bench_working_ConstB.data_ci[0],/* 614: Signal */
  &drum_bench_working_B.BusCreator_fc, /* 615: Signal */
  (void *) &drum_bench_working_ConstB.data_bu[0],/* 616: Signal */
  &drum_bench_working_B.ExtractDesiredBits_n,/* 617: Signal */
  &drum_bench_working_B.ModifyScalingOnly,/* 618: Signal */
  &drum_bench_working_B.ExtractDesiredBits_pr,/* 619: Signal */
  &drum_bench_working_B.ModifyScalingOnly_c,/* 620: Signal */
  &drum_bench_working_B.ExtractDesiredBits_dw,/* 621: Signal */
  &drum_bench_working_B.ModifyScalingOnly_j,/* 622: Signal */
  &drum_bench_working_B.ExtractDesiredBits_k,/* 623: Signal */
  &drum_bench_working_B.ModifyScalingOnly_b,/* 624: Signal */
  &drum_bench_working_B.ExtractDesiredBits_l,/* 625: Signal */
  &drum_bench_working_B.ModifyScalingOnly_jt,/* 626: Signal */
  &drum_bench_working_B.ExtractDesiredBits_d,/* 627: Signal */
  &drum_bench_working_B.ModifyScalingOnly_h,/* 628: Signal */
  &drum_bench_working_B.ExtractDesiredBits_h,/* 629: Signal */
  &drum_bench_working_B.ModifyScalingOnly_m,/* 630: Signal */
  &drum_bench_working_B.ExtractDesiredBits_ng,/* 631: Signal */
  &drum_bench_working_B.ModifyScalingOnly_i,/* 632: Signal */
  &drum_bench_working_B.ExtractDesiredBits_fo,/* 633: Signal */
  &drum_bench_working_B.ModifyScalingOnly_mj,/* 634: Signal */
  &drum_bench_working_B.ExtractDesiredBits_cg,/* 635: Signal */
  &drum_bench_working_B.ModifyScalingOnly_jz,/* 636: Signal */
  &drum_bench_working_B.ExtractDesiredBits,/* 637: Signal */
  &drum_bench_working_B.ModifyScalingOnly_d,/* 638: Signal */
  &drum_bench_working_B.ExtractDesiredBits_p,/* 639: Signal */
  &drum_bench_working_B.ModifyScalingOnly_je,/* 640: Signal */
  &drum_bench_working_B.ExtractDesiredBits_f,/* 641: Signal */
  &drum_bench_working_B.ModifyScalingOnly_o,/* 642: Signal */
  &drum_bench_working_B.ExtractDesiredBits_i1,/* 643: Signal */
  &drum_bench_working_B.ModifyScalingOnly_p,/* 644: Signal */
  &drum_bench_working_B.ExtractDesiredBits_f2,/* 645: Signal */
  &drum_bench_working_B.ModifyScalingOnly_oc,/* 646: Signal */
  &drum_bench_working_B.ExtractDesiredBits_j,/* 647: Signal */
  &drum_bench_working_B.ModifyScalingOnly_l,/* 648: Signal */
  &drum_bench_working_B.ExtractDesiredBits_lr,/* 649: Signal */
  &drum_bench_working_B.ModifyScalingOnly_e,/* 650: Signal */
  &drum_bench_working_B.ExtractDesiredBits_o,/* 651: Signal */
  &drum_bench_working_B.ModifyScalingOnly_f,/* 652: Signal */
  &drum_bench_working_B.ExtractDesiredBits_g,/* 653: Signal */
  &drum_bench_working_B.ModifyScalingOnly_cr,/* 654: Signal */
  &drum_bench_working_B.ExtractDesiredBits_kb,/* 655: Signal */
  &drum_bench_working_B.ModifyScalingOnly_n,/* 656: Signal */
  &drum_bench_working_B.ExtractDesiredBits_i,/* 657: Signal */
  &drum_bench_working_B.ModifyScalingOnly_k,/* 658: Signal */
  &drum_bench_working_B.ExtractDesiredBits_c,/* 659: Signal */
  &drum_bench_working_B.ModifyScalingOnly_pi,/* 660: Signal */
  &drum_bench_working_B.Output_o,      /* 661: Signal */
  &drum_bench_working_B.Compare,       /* 662: Signal */
  &drum_bench_working_B.enabledsubsystem.In1[0],/* 663: Signal */
  &drum_bench_working_B.Compare_a,     /* 664: Signal */
  &drum_bench_working_B.EnabledSubsystem.In1[0],/* 665: Signal */
  &drum_bench_working_B.sf_MATLABFunction1.type,/* 666: Signal */
  &drum_bench_working_B.sf_MATLABFunction1.id,/* 667: Signal */
  &drum_bench_working_B.sf_MATLABFunction1.index,/* 668: Signal */
  &drum_bench_working_B.sf_MATLABFunction1.value,/* 669: Signal */
  &drum_bench_working_B.BusCreator_a,  /* 670: Signal */
  &drum_bench_working_B.data_m[0],     /* 671: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 672: Signal */
  &drum_bench_working_B.DataTypeConversion_f[0],/* 673: Signal */
  &drum_bench_working_B.Output_i4,     /* 674: Signal */
  &drum_bench_working_B.Compare_b,     /* 675: Signal */
  &drum_bench_working_B.enabledsubsystem_l.In1[0],/* 676: Signal */
  &drum_bench_working_B.Compare_aw,    /* 677: Signal */
  &drum_bench_working_B.EnabledSubsystem_k.In1[0],/* 678: Signal */
  &drum_bench_working_B.sf_MATLABFunction1_a.type,/* 679: Signal */
  &drum_bench_working_B.sf_MATLABFunction1_a.id,/* 680: Signal */
  &drum_bench_working_B.sf_MATLABFunction1_a.index,/* 681: Signal */
  &drum_bench_working_B.sf_MATLABFunction1_a.value,/* 682: Signal */
  &drum_bench_working_B.BusCreator_j,  /* 683: Signal */
  &drum_bench_working_B.data_e5[0],    /* 684: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 685: Signal */
  &drum_bench_working_B.DataTypeConversion_aa[0],/* 686: Signal */
  &drum_bench_working_B.FixPtSum1,     /* 687: Signal */
  &drum_bench_working_B.FixPtSwitch,   /* 688: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 689: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 690: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 691: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 692: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 693: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 694: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 695: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 696: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 697: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 698: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 699: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 700: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 701: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 702: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 703: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 704: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 705: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 706: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 707: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 708: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 709: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 710: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 711: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 712: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 713: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 714: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 715: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 716: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 717: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 718: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 719: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 720: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 721: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 722: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 723: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 724: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 725: Signal */
  &drum_bench_working_B.FixPtSum1_e,   /* 726: Signal */
  &drum_bench_working_B.FixPtSwitch_b, /* 727: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 728: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 729: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 730: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 731: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 732: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 733: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 734: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 735: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 736: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 737: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 738: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 739: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 740: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 741: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 742: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 743: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 744: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 745: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 746: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 747: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 748: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 749: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 750: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 751: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 752: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 753: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 754: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 755: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 756: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 757: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 758: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 759: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 760: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 761: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 762: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 763: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 764: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 765: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 766: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 767: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 768: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 769: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 770: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 771: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 772: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 773: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 774: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 775: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 776: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 777: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 778: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 779: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 780: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 781: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 782: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 783: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 784: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 785: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 786: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 787: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 788: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 789: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 790: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 791: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 792: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 793: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 794: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 795: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 796: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 797: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 798: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 799: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 800: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 801: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 802: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 803: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 804: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 805: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 806: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 807: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 808: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 809: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 810: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 811: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 812: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 813: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 814: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 815: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 816: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 817: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 818: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 819: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 820: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 821: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 822: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 823: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 824: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 825: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 826: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 827: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 828: Signal */
  &drum_bench_working_B.VectorConcatenate_d[0],/* 829: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 830: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 831: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 832: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 833: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 834: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 835: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 836: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 837: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 838: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 839: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 840: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 841: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 842: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 843: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 844: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 845: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 846: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 847: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 848: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 849: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 850: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 851: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 852: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 853: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 854: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 855: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 856: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 857: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 858: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 859: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 860: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 861: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 862: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 863: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 864: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 865: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 866: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 867: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 868: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 869: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 870: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 871: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 872: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 873: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 874: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 875: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 876: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 877: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 878: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 879: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 880: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 881: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 882: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 883: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 884: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 885: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 886: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 887: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 888: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 889: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 890: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 891: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 892: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 893: Signal */
  &drum_bench_working_B.VectorConcatenate_e[0],/* 894: Signal */
  &drum_bench_working_P.config,        /* 895: Model Parameter */
};

/* Declare Data Run-Time Dimension Buffer Addresses statically */
static int32_T* rtVarDimsAddrMap[] = {
  (NULL)
};

#endif

/* Data Type Map - use dataTypeMapIndex to access this structure */
static TARGET_CONST rtwCAPI_DataTypeMap rtDataTypeMap[] = {
  /* cName, mwName, numElements, elemMapIndex, dataSize, slDataId, *
   * isComplex, isPointer */
  { "double", "real_T", 0, 0, sizeof(real_T), SS_DOUBLE, 0, 0 },

  { "struct", "MarlinFullCommand", 13, 1, sizeof(MarlinFullCommand), SS_STRUCT,
    0, 0 },

  { "unsigned int", "uint32_T", 0, 0, sizeof(uint32_T), SS_UINT32, 0, 0 },

  { "unsigned char", "uint8_T", 0, 0, sizeof(uint8_T), SS_UINT8, 0, 0 },

  { "unsigned char", "boolean_T", 0, 0, sizeof(boolean_T), SS_BOOLEAN, 0, 0 },

  { "struct", "MarlinBasicStatus", 21, 14, sizeof(MarlinBasicStatus), SS_STRUCT,
    0, 0 },

  { "struct", "MarlinFullStatus", 3, 35, sizeof(MarlinFullStatus), SS_STRUCT, 0,
    0 },

  { "float", "real32_T", 0, 0, sizeof(real32_T), SS_SINGLE, 0, 0 },

  { "int", "int32_T", 0, 0, sizeof(int32_T), SS_INT32, 0, 0 },

  { "struct", "MarlinFullInternalRPC", 3, 38, sizeof(MarlinFullInternalRPC),
    SS_STRUCT, 0, 0 },

  { "unsigned short", "uint16_T", 0, 0, sizeof(uint16_T), SS_UINT16, 0, 0 },

  { "struct", "struct_RIpY2xbTXEXKeuue36idi", 1, 41, sizeof
    (struct_RIpY2xbTXEXKeuue36idi), SS_STRUCT, 0, 0 },

  { "struct", "struct_Nj98fhwEUm6O969J2tt7yD", 8, 42, sizeof
    (struct_Nj98fhwEUm6O969J2tt7yD), SS_STRUCT, 0, 0 },

  { "struct", "struct_9qZLCMMOhVY4AxrMQejy0", 5, 50, sizeof
    (struct_9qZLCMMOhVY4AxrMQejy0), SS_STRUCT, 0, 0 },

  { "struct", "struct_QBpPZnk5niXRFhzN7Lj31C", 1, 55, sizeof
    (struct_QBpPZnk5niXRFhzN7Lj31C), SS_STRUCT, 0, 0 },

  { "struct", "struct_AAoDtaShZdSuxE8MfKAZuD", 1, 56, sizeof
    (struct_AAoDtaShZdSuxE8MfKAZuD), SS_STRUCT, 0, 0 },

  { "struct", "struct_bL7nOhahIPgI0X3SLeRjmE", 4, 57, sizeof
    (struct_bL7nOhahIPgI0X3SLeRjmE), SS_STRUCT, 0, 0 },

  { "struct", "struct_fqoitRciEBSWbMiMs1xp6G", 3, 61, sizeof
    (struct_fqoitRciEBSWbMiMs1xp6G), SS_STRUCT, 0, 0 },

  { "struct", "struct_LTw73MkwrOKgjTdKqzeYAH", 1, 64, sizeof
    (struct_LTw73MkwrOKgjTdKqzeYAH), SS_STRUCT, 0, 0 },

  { "struct", "struct_jqZJIiVwXC2PGh1UGLYaAD", 2, 65, sizeof
    (struct_jqZJIiVwXC2PGh1UGLYaAD), SS_STRUCT, 0, 0 },

  { "struct", "struct_INE22yG6rcc1ImNexAY7TC", 1, 67, sizeof
    (struct_INE22yG6rcc1ImNexAY7TC), SS_STRUCT, 0, 0 },

  { "struct", "struct_gK2Jy8LweAj0bYVyYXESZG", 4, 68, sizeof
    (struct_gK2Jy8LweAj0bYVyYXESZG), SS_STRUCT, 0, 0 },

  { "struct", "struct_pXLF1SG7JVIGRnIbnNXxpC", 10, 72, sizeof
    (struct_pXLF1SG7JVIGRnIbnNXxpC), SS_STRUCT, 0, 0 },

  { "struct", "struct_fxT5Nq3tGzQ2GEaQv1zUtC", 6, 82, sizeof
    (struct_fxT5Nq3tGzQ2GEaQv1zUtC), SS_STRUCT, 0, 0 },

  { "struct", "struct_8LDeTuq9MAK8ojrlDlByvH", 2, 88, sizeof
    (struct_8LDeTuq9MAK8ojrlDlByvH), SS_STRUCT, 0, 0 },

  { "struct", "struct_KXOvmy8ymuqsyffV7WWpcG", 1, 90, sizeof
    (struct_KXOvmy8ymuqsyffV7WWpcG), SS_STRUCT, 0, 0 },

  { "struct", "struct_KoXNgBBld1DARpq3izFtbF", 2, 91, sizeof
    (struct_KoXNgBBld1DARpq3izFtbF), SS_STRUCT, 0, 0 },

  { "struct", "struct_Se3YeAXB2onI5ODlwrYQo", 1, 93, sizeof
    (struct_Se3YeAXB2onI5ODlwrYQo), SS_STRUCT, 0, 0 },

  { "struct", "struct_BCyM1TQF0epB1PvNrxpCnF", 17, 94, sizeof
    (struct_BCyM1TQF0epB1PvNrxpCnF), SS_STRUCT, 0, 0 },

  { "struct", "struct_0tMNkeucayrJub3Th2yZ6E", 2, 111, sizeof
    (struct_0tMNkeucayrJub3Th2yZ6E), SS_STRUCT, 0, 0 },

  { "struct", "struct_jkDUSheYYfvtbbR4tr6mvD", 5, 113, sizeof
    (struct_jkDUSheYYfvtbbR4tr6mvD), SS_STRUCT, 0, 0 },

  { "struct", "struct_6bi3MChDTb2LN2fRyJtpm", 20, 118, sizeof
    (struct_6bi3MChDTb2LN2fRyJtpm), SS_STRUCT, 0, 0 },

  { "struct", "struct_om0sqrv6CVBccOjddaiW7E", 1, 138, sizeof
    (struct_om0sqrv6CVBccOjddaiW7E), SS_STRUCT, 0, 0 },

  { "struct", "struct_CXnTNOFtLHj3Uh94en2j8C", 3, 139, sizeof
    (struct_CXnTNOFtLHj3Uh94en2j8C), SS_STRUCT, 0, 0 },

  { "struct", "struct_Ff8qvljR6PDUJi6EFt21oB", 3, 142, sizeof
    (struct_Ff8qvljR6PDUJi6EFt21oB), SS_STRUCT, 0, 0 },

  { "struct", "struct_qmv5O2WOgyNocQPcrfDxtE", 4, 145, sizeof
    (struct_qmv5O2WOgyNocQPcrfDxtE), SS_STRUCT, 0, 0 },

  { "struct", "struct_FlGEfMk29qJHlFXdbDtRXC", 3, 149, sizeof
    (struct_FlGEfMk29qJHlFXdbDtRXC), SS_STRUCT, 0, 0 },

  { "struct", "struct_Kzi606PJBfEKmP4ngorXDD", 4, 152, sizeof
    (struct_Kzi606PJBfEKmP4ngorXDD), SS_STRUCT, 0, 0 }
};

#ifdef HOST_CAPI_BUILD
#undef sizeof
#endif

/* Structure Element Map - use elemMapIndex to access this structure */
static TARGET_CONST rtwCAPI_ElementMap rtElementMap[] = {
  /* elementName, elementOffset, dataTypeIndex, dimIndex, fxpIndex */
  { (NULL), 0, 0, 0, 0 },

  { "mode", rt_offsetof(MarlinFullCommand, mode), 0, 0, 0 },

  { "quadrature_1_angle", rt_offsetof(MarlinFullCommand, quadrature_1_angle), 0,
    0, 0 },

  { "quadrature_1_angle_dot", rt_offsetof(MarlinFullCommand,
    quadrature_1_angle_dot), 0, 0, 0 },

  { "quadrature_2_angle", rt_offsetof(MarlinFullCommand, quadrature_2_angle), 0,
    0, 0 },

  { "quadrature_2_angle_dot", rt_offsetof(MarlinFullCommand,
    quadrature_2_angle_dot), 0, 0, 0 },

  { "analog_diff", rt_offsetof(MarlinFullCommand, analog_diff), 0, 0, 0 },

  { "analog_diff_dot", rt_offsetof(MarlinFullCommand, analog_diff_dot), 0, 0, 0
  },

  { "motor_torque", rt_offsetof(MarlinFullCommand, motor_torque), 0, 0, 0 },

  { "motor_iq", rt_offsetof(MarlinFullCommand, motor_iq), 0, 0, 0 },

  { "motor_id", rt_offsetof(MarlinFullCommand, motor_id), 0, 0, 0 },

  { "motor_vq", rt_offsetof(MarlinFullCommand, motor_vq), 0, 0, 0 },

  { "motor_vd", rt_offsetof(MarlinFullCommand, motor_vd), 0, 0, 0 },

  { "rpc", rt_offsetof(MarlinFullCommand, rpc), 0, 0, 0 },

  { "motor_torque", rt_offsetof(MarlinBasicStatus, motor_torque), 0, 0, 0 },

  { "motor_iq", rt_offsetof(MarlinBasicStatus, motor_iq), 0, 0, 0 },

  { "motor_vq_avg", rt_offsetof(MarlinBasicStatus, motor_vq_avg), 0, 0, 0 },

  { "analog_1", rt_offsetof(MarlinBasicStatus, analog_1), 0, 0, 0 },

  { "analog_1_dot", rt_offsetof(MarlinBasicStatus, analog_1_dot), 0, 0, 0 },

  { "analog_2", rt_offsetof(MarlinBasicStatus, analog_2), 0, 0, 0 },

  { "analog_2_dot", rt_offsetof(MarlinBasicStatus, analog_2_dot), 0, 0, 0 },

  { "analog_diff", rt_offsetof(MarlinBasicStatus, analog_diff), 0, 0, 0 },

  { "analog_diff_dot", rt_offsetof(MarlinBasicStatus, analog_diff_dot), 0, 0, 0
  },

  { "quadrature_1", rt_offsetof(MarlinBasicStatus, quadrature_1), 0, 0, 0 },

  { "quadrature_1_dot", rt_offsetof(MarlinBasicStatus, quadrature_1_dot), 0, 0,
    0 },

  { "quadrature_2", rt_offsetof(MarlinBasicStatus, quadrature_2), 0, 0, 0 },

  { "quadrature_2_dot", rt_offsetof(MarlinBasicStatus, quadrature_2_dot), 0, 0,
    0 },

  { "ssi", rt_offsetof(MarlinBasicStatus, ssi), 0, 0, 0 },

  { "ssi_dot", rt_offsetof(MarlinBasicStatus, ssi_dot), 0, 0, 0 },

  { "accelerometer_x", rt_offsetof(MarlinBasicStatus, accelerometer_x), 0, 0, 0
  },

  { "accelerometer_y", rt_offsetof(MarlinBasicStatus, accelerometer_y), 0, 0, 0
  },

  { "accelerometer_z", rt_offsetof(MarlinBasicStatus, accelerometer_z), 0, 0, 0
  },

  { "faults", rt_offsetof(MarlinBasicStatus, faults), 2, 0, 0 },

  { "bus_v", rt_offsetof(MarlinBasicStatus, bus_v), 0, 0, 0 },

  { "rpa_packet", rt_offsetof(MarlinBasicStatus, rpa_packet), 3, 1, 0 },

  { "marlin_basic_status", rt_offsetof(MarlinFullStatus, marlin_basic_status), 5,
    0, 0 },

  { "temperature_motor_winding", rt_offsetof(MarlinFullStatus,
    temperature_motor_winding), 0, 0, 0 },

  { "temperature_motor_housing", rt_offsetof(MarlinFullStatus,
    temperature_motor_housing), 0, 0, 0 },

  { "type", rt_offsetof(MarlinFullInternalRPC, type), 8, 0, 0 },

  { "id", rt_offsetof(MarlinFullInternalRPC, id), 7, 0, 0 },

  { "data", rt_offsetof(MarlinFullInternalRPC, data), 3, 5, 0 },

  { "trigger_level", rt_offsetof(struct_RIpY2xbTXEXKeuue36idi, trigger_level), 0,
    17, 0 },

  { "current_zero_a", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, current_zero_a),
    0, 17, 0 },

  { "current_zero_b", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, current_zero_b),
    0, 17, 0 },

  { "deadband", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, deadband), 0, 17, 0 },

  { "led_brightness", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, led_brightness),
    0, 17, 0 },

  { "nominal_bus_voltage", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD,
    nominal_bus_voltage), 0, 17, 0 },

  { "current_sensor_limit", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD,
    current_sensor_limit), 0, 17, 0 },

  { "logger_10kHz", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, logger_10kHz), 11,
    17, 0 },

  { "logger_100kHz", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, logger_100kHz),
    11, 17, 0 },

  { "current_filter_bandwidth", rt_offsetof(struct_9qZLCMMOhVY4AxrMQejy0,
    current_filter_bandwidth), 0, 17, 0 },

  { "fingerprint_gain", rt_offsetof(struct_9qZLCMMOhVY4AxrMQejy0,
    fingerprint_gain), 0, 17, 0 },

  { "ki", rt_offsetof(struct_9qZLCMMOhVY4AxrMQejy0, ki), 0, 17, 0 },

  { "ki_limit", rt_offsetof(struct_9qZLCMMOhVY4AxrMQejy0, ki_limit), 0, 17, 0 },

  { "kp", rt_offsetof(struct_9qZLCMMOhVY4AxrMQejy0, kp), 0, 17, 0 },

  { "deadband", rt_offsetof(struct_QBpPZnk5niXRFhzN7Lj31C, deadband), 0, 17, 0 },

  { "enabled", rt_offsetof(struct_AAoDtaShZdSuxE8MfKAZuD, enabled), 0, 17, 0 },

  { "kd", rt_offsetof(struct_bL7nOhahIPgI0X3SLeRjmE, kd), 0, 17, 0 },

  { "ki", rt_offsetof(struct_bL7nOhahIPgI0X3SLeRjmE, ki), 0, 17, 0 },

  { "ki_limit", rt_offsetof(struct_bL7nOhahIPgI0X3SLeRjmE, ki_limit), 0, 17, 0 },

  { "kp", rt_offsetof(struct_bL7nOhahIPgI0X3SLeRjmE, kp), 0, 17, 0 },

  { "pid_deadband", rt_offsetof(struct_fqoitRciEBSWbMiMs1xp6G, pid_deadband), 14,
    17, 0 },

  { "pid_interpolate", rt_offsetof(struct_fqoitRciEBSWbMiMs1xp6G,
    pid_interpolate), 15, 17, 0 },

  { "pid", rt_offsetof(struct_fqoitRciEBSWbMiMs1xp6G, pid), 16, 17, 0 },

  { "pid", rt_offsetof(struct_LTw73MkwrOKgjTdKqzeYAH, pid), 16, 17, 0 },

  { "pid", rt_offsetof(struct_jqZJIiVwXC2PGh1UGLYaAD, pid), 16, 17, 0 },

  { "source", rt_offsetof(struct_jqZJIiVwXC2PGh1UGLYaAD, source), 0, 17, 0 },

  { "counts_per_revolution", rt_offsetof(struct_INE22yG6rcc1ImNexAY7TC,
    counts_per_revolution), 0, 17, 0 },

  { "eA1", rt_offsetof(struct_gK2Jy8LweAj0bYVyYXESZG, eA1), 0, 18, 0 },

  { "eA2", rt_offsetof(struct_gK2Jy8LweAj0bYVyYXESZG, eA2), 0, 19, 0 },

  { "temperature_allowable_motor_winding", rt_offsetof
    (struct_gK2Jy8LweAj0bYVyYXESZG, temperature_allowable_motor_winding), 0, 17,
    0 },

  { "temperature_allowable_motor_housing", rt_offsetof
    (struct_gK2Jy8LweAj0bYVyYXESZG, temperature_allowable_motor_housing), 0, 17,
    0 },

  { "current_limit", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, current_limit),
    0, 17, 0 },

  { "voltage_limit", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, voltage_limit),
    0, 17, 0 },

  { "km", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, km), 0, 17, 0 },

  { "resistance", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, resistance), 0, 17,
    0 },

  { "num_poles", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, num_poles), 0, 17, 0
  },

  { "electrical_offset", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC,
    electrical_offset), 0, 17, 0 },

  { "v_per_a", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, v_per_a), 0, 17, 0 },

  { "encoder", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, encoder), 20, 17, 0 },

  { "thermal_model", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, thermal_model),
    21, 17, 0 },

  { "phase_lock_current", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC,
    phase_lock_current), 0, 17, 0 },

  { "bias", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC, bias), 0, 17, 0 },

  { "gain", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC, gain), 0, 17, 0 },

  { "limit_high", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC, limit_high), 0, 17,
    0 },

  { "limit_low", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC, limit_low), 0, 17, 0
  },

  { "filter_bandwidth", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC,
    filter_bandwidth), 0, 17, 0 },

  { "dot_filter_bandwidth", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC,
    dot_filter_bandwidth), 0, 17, 0 },

  { "sensor", rt_offsetof(struct_8LDeTuq9MAK8ojrlDlByvH, sensor), 23, 17, 0 },

  { "enable_index", rt_offsetof(struct_8LDeTuq9MAK8ojrlDlByvH, enable_index), 0,
    17, 0 },

  { "sensor", rt_offsetof(struct_KXOvmy8ymuqsyffV7WWpcG, sensor), 23, 17, 0 },

  { "eA1", rt_offsetof(struct_KoXNgBBld1DARpq3izFtbF, eA1), 0, 17, 0 },

  { "eA2", rt_offsetof(struct_KoXNgBBld1DARpq3izFtbF, eA2), 0, 17, 0 },

  { "thermal_model", rt_offsetof(struct_Se3YeAXB2onI5ODlwrYQo, thermal_model),
    26, 17, 0 },

  { "slave_alias", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, slave_alias), 0,
    17, 0 },

  { "slave_position", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, slave_position),
    0, 17, 0 },

  { "debug_address_index", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF,
    debug_address_index), 0, 17, 0 },

  { "amp", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, amp), 12, 17, 0 },

  { "control_current", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF,
    control_current), 13, 17, 0 },

  { "control_motor_angle", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF,
    control_motor_angle), 17, 17, 0 },

  { "control_quadrature_2_angle", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF,
    control_quadrature_2_angle), 18, 17, 0 },

  { "control_analog", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, control_analog),
    19, 17, 0 },

  { "motor", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, motor), 22, 17, 0 },

  { "quadrature_1", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, quadrature_1), 24,
    17, 0 },

  { "quadrature_2", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, quadrature_2), 24,
    17, 0 },

  { "ssi", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, ssi), 25, 17, 0 },

  { "analog_1", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, analog_1), 25, 17, 0
  },

  { "analog_2", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, analog_2), 25, 17, 0
  },

  { "analog_diff", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, analog_diff), 25,
    17, 0 },

  { "bridge", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, bridge), 27, 17, 0 },

  { "fingerprint", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, fingerprint), 0,
    20, 0 },

  { "M0", rt_offsetof(struct_0tMNkeucayrJub3Th2yZ6E, M0), 28, 17, 0 },

  { "M1", rt_offsetof(struct_0tMNkeucayrJub3Th2yZ6E, M1), 28, 17, 0 },

  { "amplitude", rt_offsetof(struct_jkDUSheYYfvtbbR4tr6mvD, amplitude), 0, 17, 0
  },

  { "bias", rt_offsetof(struct_jkDUSheYYfvtbbR4tr6mvD, bias), 0, 17, 0 },

  { "frequency", rt_offsetof(struct_jkDUSheYYfvtbbR4tr6mvD, frequency), 0, 17, 0
  },

  { "select_input", rt_offsetof(struct_jkDUSheYYfvtbbR4tr6mvD, select_input), 0,
    17, 0 },

  { "slew_rate", rt_offsetof(struct_jkDUSheYYfvtbbR4tr6mvD, slew_rate), 0, 17, 0
  },

  { "mode", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, mode), 0, 17, 0 },

  { "quadrature_1_angle", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_1_angle), 30, 17, 0 },

  { "quadrature_1_angle_dot", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_1_angle_dot), 0, 17, 0 },

  { "quadrature_1_angle_dotdot", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_1_angle_dotdot), 0, 17, 0 },

  { "quadrature_2_angle", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_2_angle), 30, 17, 0 },

  { "quadrature_2_angle_dot", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_2_angle_dot), 0, 17, 0 },

  { "quadrature_2_angle_dotdot", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_2_angle_dotdot), 0, 17, 0 },

  { "analog", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, analog), 30, 17, 0 },

  { "analog_dot", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, analog_dot), 0, 17,
    0 },

  { "motor_torque", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, motor_torque), 0,
    17, 0 },

  { "motor_iq", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, motor_iq), 30, 17, 0 },

  { "motor_id", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, motor_id), 0, 17, 0 },

  { "motor_vq", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, motor_vq), 30, 17, 0 },

  { "rpc_command", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, rpc_command), 0, 17,
    0 },

  { "motor_vd", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, motor_vd), 0, 17, 0 },

  { "quadrature_1_angle_gain", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_1_angle_gain), 0, 17, 0 },

  { "quadrature_1_angle_dot_gain", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_1_angle_dot_gain), 0, 17, 0 },

  { "analog_gain", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, analog_gain), 0, 17,
    0 },

  { "analog_dot_gain", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, analog_dot_gain),
    0, 17, 0 },

  { "trigger_zero_bias", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    trigger_zero_bias), 0, 17, 0 },

  { "trigger_yaml_write", rt_offsetof(struct_om0sqrv6CVBccOjddaiW7E,
    trigger_yaml_write), 0, 17, 0 },

  { "input0", rt_offsetof(struct_CXnTNOFtLHj3Uh94en2j8C, input0), 31, 17, 0 },

  { "input1", rt_offsetof(struct_CXnTNOFtLHj3Uh94en2j8C, input1), 31, 17, 0 },

  { "input_extra", rt_offsetof(struct_CXnTNOFtLHj3Uh94en2j8C, input_extra), 32,
    17, 0 },

  { "kp", rt_offsetof(struct_Ff8qvljR6PDUJi6EFt21oB, kp), 0, 17, 0 },

  { "kd", rt_offsetof(struct_Ff8qvljR6PDUJi6EFt21oB, kd), 0, 17, 0 },

  { "imp_cutoff", rt_offsetof(struct_Ff8qvljR6PDUJi6EFt21oB, imp_cutoff), 0, 17,
    0 },

  { "bias", rt_offsetof(struct_qmv5O2WOgyNocQPcrfDxtE, bias), 0, 17, 0 },

  { "ka", rt_offsetof(struct_qmv5O2WOgyNocQPcrfDxtE, ka), 0, 17, 0 },

  { "kd", rt_offsetof(struct_qmv5O2WOgyNocQPcrfDxtE, kd), 0, 17, 0 },

  { "var_imp_cutoff", rt_offsetof(struct_qmv5O2WOgyNocQPcrfDxtE, var_imp_cutoff),
    0, 17, 0 },

  { "mode_select", rt_offsetof(struct_FlGEfMk29qJHlFXdbDtRXC, mode_select), 0,
    17, 0 },

  { "impedence_control", rt_offsetof(struct_FlGEfMk29qJHlFXdbDtRXC,
    impedence_control), 34, 17, 0 },

  { "var_impedence_control", rt_offsetof(struct_FlGEfMk29qJHlFXdbDtRXC,
    var_impedence_control), 35, 17, 0 },

  { "marlins", rt_offsetof(struct_Kzi606PJBfEKmP4ngorXDD, marlins), 29, 17, 0 },

  { "input", rt_offsetof(struct_Kzi606PJBfEKmP4ngorXDD, input), 33, 17, 0 },

  { "control0", rt_offsetof(struct_Kzi606PJBfEKmP4ngorXDD, control0), 36, 17, 0
  },

  { "control1", rt_offsetof(struct_Kzi606PJBfEKmP4ngorXDD, control1), 36, 17, 0
  }
};

/* Dimension Map - use dimensionMapIndex to access elements of ths structure*/
static const rtwCAPI_DimensionMap rtDimensionMap[] = {
  /* dataOrientation, dimArrayIndex, numDims, vardimsIndex */
  { rtwCAPI_SCALAR, 0, 2, 0 },

  { rtwCAPI_VECTOR, 2, 2, 0 },

  { rtwCAPI_MATRIX_COL_MAJOR, 4, 2, 0 },

  { rtwCAPI_VECTOR, 6, 2, 0 },

  { rtwCAPI_VECTOR, 8, 2, 0 },

  { rtwCAPI_VECTOR, 10, 2, 0 },

  { rtwCAPI_VECTOR, 12, 2, 0 },

  { rtwCAPI_VECTOR, 14, 2, 0 },

  { rtwCAPI_VECTOR, 16, 2, 0 },

  { rtwCAPI_VECTOR, 18, 2, 0 },

  { rtwCAPI_VECTOR, 20, 2, 0 },

  { rtwCAPI_VECTOR, 22, 2, 0 },

  { rtwCAPI_VECTOR, 24, 2, 0 },

  { rtwCAPI_VECTOR, 26, 2, 0 },

  { rtwCAPI_VECTOR, 28, 2, 0 },

  { rtwCAPI_VECTOR, 30, 2, 0 },

  { rtwCAPI_VECTOR, 32, 2, 0 },

  { rtwCAPI_MATRIX_COL_MAJOR, 0, 2, 0 },

  { rtwCAPI_MATRIX_COL_MAJOR, 34, 2, 0 },

  { rtwCAPI_MATRIX_COL_MAJOR, 8, 2, 0 },

  { rtwCAPI_MATRIX_COL_MAJOR, 36, 2, 0 }
};

/* Dimension Array- use dimArrayIndex to access elements of this array */
static const uint_T rtDimensionArray[] = {
  1,                                   /* 0 */
  1,                                   /* 1 */
  28,                                  /* 2 */
  1,                                   /* 3 */
  7,                                   /* 4 */
  1,                                   /* 5 */
  56,                                  /* 6 */
  1,                                   /* 7 */
  2,                                   /* 8 */
  1,                                   /* 9 */
  20,                                  /* 10 */
  1,                                   /* 11 */
  15,                                  /* 12 */
  1,                                   /* 13 */
  14,                                  /* 14 */
  1,                                   /* 15 */
  10,                                  /* 16 */
  1,                                   /* 17 */
  5,                                   /* 18 */
  1,                                   /* 19 */
  76,                                  /* 20 */
  1,                                   /* 21 */
  1,                                   /* 22 */
  8,                                   /* 23 */
  1,                                   /* 24 */
  6,                                   /* 25 */
  1,                                   /* 26 */
  2,                                   /* 27 */
  1,                                   /* 28 */
  5,                                   /* 29 */
  1,                                   /* 30 */
  4,                                   /* 31 */
  1,                                   /* 32 */
  7,                                   /* 33 */
  2,                                   /* 34 */
  2,                                   /* 35 */
  2000,                                /* 36 */
  1                                    /* 37 */
};

/* C-API stores floating point values in an array. The elements of this  *
 * are unique. This ensures that values which are shared across the model*
 * are stored in the most efficient way. These values are referenced by  *
 *           - rtwCAPI_FixPtMap.fracSlopePtr,                            *
 *           - rtwCAPI_FixPtMap.biasPtr,                                 *
 *           - rtwCAPI_SampleTimeMap.samplePeriodPtr,                    *
 *           - rtwCAPI_SampleTimeMap.sampleOffsetPtr                     */
static const real_T rtcapiStoredFloats[] = {
  0.0003, 0.0, 1.0
};

/* Fixed Point Map */
static const rtwCAPI_FixPtMap rtFixPtMap[] = {
  /* fracSlopePtr, biasPtr, scaleType, wordLength, exponent, isSigned */
  { (NULL), (NULL), rtwCAPI_FIX_RESERVED, 0, 0, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 0, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 1, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 10, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 2, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 3, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 4, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 5, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 6, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 7, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 8, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 9, 0 }
};

/* Sample Time Map - use sTimeIndex to access elements of ths structure */
static const rtwCAPI_SampleTimeMap rtSampleTimeMap[] = {
  /* samplePeriodPtr, sampleOffsetPtr, tid, samplingMode */
  { (const void *) &rtcapiStoredFloats[0], (const void *) &rtcapiStoredFloats[1],
    1, 0 },

  { (const void *) &rtcapiStoredFloats[1], (const void *) &rtcapiStoredFloats[1],
    0, 0 },

  { (NULL), (NULL), -2, 0 }
};

static rtwCAPI_ModelMappingStaticInfo mmiStatic = {
  /* Signals:{signals, numSignals},
   * Params: {blockParameters, numBlockParameters,
   *          modelParameters, numModelParameters},
   * States: {states, numStates},
   * Root Inputs: {rootInputs, numRootInputs}
   * Root Outputs: {rootOutputs, numRootOutputs}
   * Maps:   {dataTypeMap, dimensionMap, fixPtMap,
   *          elementMap, sampleTimeMap, dimensionArray},
   * TargetType: targetType
   */
  { rtBlockSignals, 895,
    (NULL), 0,
    (NULL), 0 },

  { rtBlockParameters, 0,
    rtModelParameters, 1 },

  { (NULL), 0 },

  { rtDataTypeMap, rtDimensionMap, rtFixPtMap,
    rtElementMap, rtSampleTimeMap, rtDimensionArray },
  "float", (NULL), 0,
};

/* Function to get C API Model Mapping Static Info */
const rtwCAPI_ModelMappingStaticInfo*
  drum_bench_working_GetCAPIStaticMap()
{
  return &mmiStatic;
}

/* Cache pointers into DataMapInfo substructure of RTModel */
#ifndef HOST_CAPI_BUILD

void drum_bench_working_InitializeDataMapInfo(RT_MODEL_drum_bench_working_T *
  const drum_bench_working_M)
{
  /* Set C-API version */
  rtwCAPI_SetVersion(drum_bench_working_M->DataMapInfo.mmi, 1);

  /* Cache static C-API data into the Real-time Model Data structure */
  rtwCAPI_SetStaticMap(drum_bench_working_M->DataMapInfo.mmi, &mmiStatic);

  /* Cache static C-API logging data into the Real-time Model Data structure */
  rtwCAPI_SetLoggingStaticMap(drum_bench_working_M->DataMapInfo.mmi, (NULL));

  /* Cache C-API Data Addresses into the Real-Time Model Data structure */
  rtwCAPI_SetDataAddressMap(drum_bench_working_M->DataMapInfo.mmi, rtDataAddrMap);

  /* Cache C-API Data Run-Time Dimension Buffer Addresses into the Real-Time Model Data structure */
  rtwCAPI_SetVarDimsAddressMap(drum_bench_working_M->DataMapInfo.mmi,
    rtVarDimsAddrMap);

  /* Cache the instance C-API logging pointer */
  rtwCAPI_SetInstanceLoggingInfo(drum_bench_working_M->DataMapInfo.mmi, (NULL));

  /* Set reference to submodels */
  rtwCAPI_SetChildMMIArray(drum_bench_working_M->DataMapInfo.mmi, (NULL));
  rtwCAPI_SetChildMMIArrayLen(drum_bench_working_M->DataMapInfo.mmi, 0);
}

#else                                  /* HOST_CAPI_BUILD */
#ifdef __cplusplus

extern "C" {

#endif

  void drum_bench_working_host_InitializeDataMapInfo
    (drum_bench_working_host_DataMapInfo_T *dataMap, const char *path)
  {
    /* Set C-API version */
    rtwCAPI_SetVersion(dataMap->mmi, 1);

    /* Cache static C-API data into the Real-time Model Data structure */
    rtwCAPI_SetStaticMap(dataMap->mmi, &mmiStatic);

    /* host data address map is NULL */
    rtwCAPI_SetDataAddressMap(dataMap->mmi, NULL);

    /* host vardims address map is NULL */
    rtwCAPI_SetVarDimsAddressMap(dataMap->mmi, NULL);

    /* Set Instance specific path */
    rtwCAPI_SetPath(dataMap->mmi, path);
    rtwCAPI_SetFullPath(dataMap->mmi, NULL);

    /* Set reference to submodels */
    rtwCAPI_SetChildMMIArray(dataMap->mmi, (NULL));
    rtwCAPI_SetChildMMIArrayLen(dataMap->mmi, 0);
  }

#ifdef __cplusplus

}
#endif
#endif                                 /* HOST_CAPI_BUILD */

/* EOF: drum_bench_working_capi.c */
