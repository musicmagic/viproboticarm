/*
 * drum_bench_working_private.h
 *
 * Code generation for model "drum_bench_working".
 *
 * Model version              : 1.99
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Tue Jul  8 09:25:28 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_drum_bench_working_private_h_
#define RTW_HEADER_drum_bench_working_private_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#ifndef UCHAR_MAX
#include <limits.h>
#endif

#if ( UCHAR_MAX != (0xFFU) ) || ( SCHAR_MAX != (0x7F) )
#error "Code was generated for compiler with different sized uchar/char. Consider adjusting Emulation Hardware word size settings on the Hardware Implementation pane to match your compiler word sizes as defined in the compiler's limits.h header file. Alternatively, you can select 'None' for Emulation Hardware and select the 'Enable portable word sizes' option for ERT based targets, which will disable the preprocessor word size checks."
#endif

#if ( USHRT_MAX != (0xFFFFU) ) || ( SHRT_MAX != (0x7FFF) )
#error "Code was generated for compiler with different sized ushort/short. Consider adjusting Emulation Hardware word size settings on the Hardware Implementation pane to match your compiler word sizes as defined in the compilers limits.h header file. Alternatively, you can select 'None' for Emulation Hardware and select the 'Enable portable word sizes' option for ERT based targets, this will disable the preprocessor word size checks."
#endif

#if ( UINT_MAX != (0xFFFFFFFFU) ) || ( INT_MAX != (0x7FFFFFFF) )
#error "Code was generated for compiler with different sized uint/int. Consider adjusting Emulation Hardware word size settings on the Hardware Implementation pane to match your compiler word sizes as defined in the compilers limits.h header file. Alternatively, you can select 'None' for Emulation Hardware and select the 'Enable portable word sizes' option for ERT based targets, this will disable the preprocessor word size checks."
#endif

#if 0

/* Skip this size verification because of preprocessor limitation */
#if ( ULONG_MAX != (0xFFFFFFFFFFFFFFFFUL) ) || ( LONG_MAX != (0x7FFFFFFFFFFFFFFFL) )
#error "Code was generated for compiler with different sized ulong/long. Consider adjusting Emulation Hardware word size settings on the Hardware Implementation pane to match your compiler word sizes as defined in the compilers limits.h header file. Alternatively, you can select 'None' for Emulation Hardware and select the 'Enable portable word sizes' option for ERT based targets, this will disable the preprocessor word size checks."
#endif
#endif

#ifndef __RTWTYPES_H__
#error This file requires rtwtypes.h to be included
#else
#ifdef TMWTYPES_PREVIOUSLY_INCLUDED
#error This file requires rtwtypes.h to be included before tmwtypes.h
#endif                                 /* TMWTYPES_PREVIOUSLY_INCLUDED */
#endif                                 /* __RTWTYPES_H__ */

extern void write_yaml_params_Outputs_wrapper();
extern real_T rt_roundd_snf(real_T u);
extern void drum_be_FilteredDerivative_Init(DW_FilteredDerivative_drum_be_T
  *localDW);
extern void drum__FilteredDerivative_Update(B_FilteredDerivative_drum_ben_T
  *localB, DW_FilteredDerivative_drum_be_T *localDW);
extern void drum_bench_w_FilteredDerivative(real_T rtu_u,
  B_FilteredDerivative_drum_ben_T *localB, DW_FilteredDerivative_drum_be_T
  *localDW, real_T rtp_N);
extern void drum_ben_enabledsubsystem_Start(B_enabledsubsystem_drum_bench_T
  *localB);
extern void drum_bench_wor_enabledsubsystem(boolean_T rtu_0, const uint8_T
  rtu_1[20], B_enabledsubsystem_drum_bench_T *localB);
extern void drum_ben_EnabledSubsystem_Start(B_EnabledSubsystem_drum_bench_T
  *localB);
extern void drum_bench_wor_EnabledSubsystem(boolean_T rtu_0, const uint8_T
  rtu_1[20], B_EnabledSubsystem_drum_bench_T *localB);
extern void drum_bench_MATLABFunction1_Init(DW_MATLABFunction1_drum_bench_T
  *localDW);
extern void drum_bench_work_MATLABFunction1(const real32_T rtu_u[76], const
  MarlinFullInternalRPA *rtu_rpa, uint32_T rtu_rpc_type,
  B_MATLABFunction1_drum_bench__T *localB, DW_MATLABFunction1_drum_bench_T
  *localDW);
extern void drum_bench_working_selectrpc(real_T rtu_rpc_command, const
  MarlinFullInternalRPC rtu_u[15], B_selectrpc_drum_bench_workin_T *localB);

#endif                                 /* RTW_HEADER_drum_bench_working_private_h_ */
