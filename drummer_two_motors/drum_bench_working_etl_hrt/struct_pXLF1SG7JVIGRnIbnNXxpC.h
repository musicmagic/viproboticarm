#ifndef RTW_HEADER_struct_pXLF1SG7JVIGRnIbnNXxpC_h_
#define RTW_HEADER_struct_pXLF1SG7JVIGRnIbnNXxpC_h_
#include "struct_INE22yG6rcc1ImNexAY7TC.h"
#include "struct_gK2Jy8LweAj0bYVyYXESZG.h"
#include "rtwtypes.h"

typedef struct {
  real_T current_limit;
  real_T voltage_limit;
  real_T km;
  real_T resistance;
  real_T num_poles;
  real_T electrical_offset;
  real_T v_per_a;
  struct_INE22yG6rcc1ImNexAY7TC encoder;
  struct_gK2Jy8LweAj0bYVyYXESZG thermal_model;
  real_T phase_lock_current;
} struct_pXLF1SG7JVIGRnIbnNXxpC;

#endif                                 /* RTW_HEADER_struct_pXLF1SG7JVIGRnIbnNXxpC_h_ */
