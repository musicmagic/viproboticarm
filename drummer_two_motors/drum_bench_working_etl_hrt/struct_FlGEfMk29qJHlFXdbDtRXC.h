#ifndef RTW_HEADER_struct_FlGEfMk29qJHlFXdbDtRXC_h_
#define RTW_HEADER_struct_FlGEfMk29qJHlFXdbDtRXC_h_
#include "struct_Ff8qvljR6PDUJi6EFt21oB.h"
#include "struct_qmv5O2WOgyNocQPcrfDxtE.h"
#include "rtwtypes.h"

typedef struct {
  real_T mode_select;
  struct_Ff8qvljR6PDUJi6EFt21oB impedence_control;
  struct_qmv5O2WOgyNocQPcrfDxtE var_impedence_control;
} struct_FlGEfMk29qJHlFXdbDtRXC;

#endif                                 /* RTW_HEADER_struct_FlGEfMk29qJHlFXdbDtRXC_h_ */
