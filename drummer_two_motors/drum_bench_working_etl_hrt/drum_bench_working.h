/*
 * drum_bench_working.h
 *
 * Code generation for model "drum_bench_working".
 *
 * Model version              : 1.99
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Tue Jul  8 09:25:28 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_drum_bench_working_h_
#define RTW_HEADER_drum_bench_working_h_
#include "rtw_modelmap.h"
#ifndef drum_bench_working_COMMON_INCLUDES_
# define drum_bench_working_COMMON_INCLUDES_
#include <math.h>
#include <stddef.h>
#include <string.h>
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "ecrt_support.h"
#include "stdio.h"
#include "rtGetInf.h"
#include "rtGetNaN.h"
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rt_zcfcn.h"
#endif                                 /* drum_bench_working_COMMON_INCLUDES_ */

#include "drum_bench_working_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetBlkStateChangeFlag
# define rtmGetBlkStateChangeFlag(rtm) ((rtm)->ModelData.blkStateChange)
#endif

#ifndef rtmSetBlkStateChangeFlag
# define rtmSetBlkStateChangeFlag(rtm, val) ((rtm)->ModelData.blkStateChange = (val))
#endif

#ifndef rtmGetBlockIO
# define rtmGetBlockIO(rtm)            ((rtm)->ModelData.blockIO)
#endif

#ifndef rtmSetBlockIO
# define rtmSetBlockIO(rtm, val)       ((rtm)->ModelData.blockIO = (val))
#endif

#ifndef rtmGetChecksums
# define rtmGetChecksums(rtm)          ((rtm)->Sizes.checksums)
#endif

#ifndef rtmSetChecksums
# define rtmSetChecksums(rtm, val)     ((rtm)->Sizes.checksums = (val))
#endif

#ifndef rtmGetConstBlockIO
# define rtmGetConstBlockIO(rtm)       ((rtm)->ModelData.constBlockIO)
#endif

#ifndef rtmSetConstBlockIO
# define rtmSetConstBlockIO(rtm, val)  ((rtm)->ModelData.constBlockIO = (val))
#endif

#ifndef rtmGetContStateDisabled
# define rtmGetContStateDisabled(rtm)  ((rtm)->ModelData.contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
# define rtmSetContStateDisabled(rtm, val) ((rtm)->ModelData.contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
# define rtmGetContStates(rtm)         ((rtm)->ModelData.contStates)
#endif

#ifndef rtmSetContStates
# define rtmSetContStates(rtm, val)    ((rtm)->ModelData.contStates = (val))
#endif

#ifndef rtmGetDataMapInfo
# define rtmGetDataMapInfo(rtm)        ((rtm)->DataMapInfo)
#endif

#ifndef rtmSetDataMapInfo
# define rtmSetDataMapInfo(rtm, val)   ((rtm)->DataMapInfo = (val))
#endif

#ifndef rtmGetDefaultParam
# define rtmGetDefaultParam(rtm)       ((rtm)->ModelData.defaultParam)
#endif

#ifndef rtmSetDefaultParam
# define rtmSetDefaultParam(rtm, val)  ((rtm)->ModelData.defaultParam = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
# define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->ModelData.derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
# define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->ModelData.derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetDirectFeedThrough
# define rtmGetDirectFeedThrough(rtm)  ((rtm)->Sizes.sysDirFeedThru)
#endif

#ifndef rtmSetDirectFeedThrough
# define rtmSetDirectFeedThrough(rtm, val) ((rtm)->Sizes.sysDirFeedThru = (val))
#endif

#ifndef rtmGetErrorStatusFlag
# define rtmGetErrorStatusFlag(rtm)    ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatusFlag
# define rtmSetErrorStatusFlag(rtm, val) ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmSetFinalTime
# define rtmSetFinalTime(rtm, val)     ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetFirstInitCondFlag
# define rtmGetFirstInitCondFlag(rtm)  ()
#endif

#ifndef rtmSetFirstInitCondFlag
# define rtmSetFirstInitCondFlag(rtm, val) ()
#endif

#ifndef rtmGetIntgData
# define rtmGetIntgData(rtm)           ()
#endif

#ifndef rtmSetIntgData
# define rtmSetIntgData(rtm, val)      ()
#endif

#ifndef rtmGetMdlRefGlobalTID
# define rtmGetMdlRefGlobalTID(rtm)    ()
#endif

#ifndef rtmSetMdlRefGlobalTID
# define rtmSetMdlRefGlobalTID(rtm, val) ()
#endif

#ifndef rtmGetMdlRefTriggerTID
# define rtmGetMdlRefTriggerTID(rtm)   ()
#endif

#ifndef rtmSetMdlRefTriggerTID
# define rtmSetMdlRefTriggerTID(rtm, val) ()
#endif

#ifndef rtmGetModelMappingInfo
# define rtmGetModelMappingInfo(rtm)   ((rtm)->SpecialInfo.mappingInfo)
#endif

#ifndef rtmSetModelMappingInfo
# define rtmSetModelMappingInfo(rtm, val) ((rtm)->SpecialInfo.mappingInfo = (val))
#endif

#ifndef rtmGetModelName
# define rtmGetModelName(rtm)          ((rtm)->modelName)
#endif

#ifndef rtmSetModelName
# define rtmSetModelName(rtm, val)     ((rtm)->modelName = (val))
#endif

#ifndef rtmGetNonInlinedSFcns
# define rtmGetNonInlinedSFcns(rtm)    ()
#endif

#ifndef rtmSetNonInlinedSFcns
# define rtmSetNonInlinedSFcns(rtm, val) ()
#endif

#ifndef rtmGetNumBlockIO
# define rtmGetNumBlockIO(rtm)         ((rtm)->Sizes.numBlockIO)
#endif

#ifndef rtmSetNumBlockIO
# define rtmSetNumBlockIO(rtm, val)    ((rtm)->Sizes.numBlockIO = (val))
#endif

#ifndef rtmGetNumBlockParams
# define rtmGetNumBlockParams(rtm)     ((rtm)->Sizes.numBlockPrms)
#endif

#ifndef rtmSetNumBlockParams
# define rtmSetNumBlockParams(rtm, val) ((rtm)->Sizes.numBlockPrms = (val))
#endif

#ifndef rtmGetNumBlocks
# define rtmGetNumBlocks(rtm)          ((rtm)->Sizes.numBlocks)
#endif

#ifndef rtmSetNumBlocks
# define rtmSetNumBlocks(rtm, val)     ((rtm)->Sizes.numBlocks = (val))
#endif

#ifndef rtmGetNumContStates
# define rtmGetNumContStates(rtm)      ((rtm)->Sizes.numContStates)
#endif

#ifndef rtmSetNumContStates
# define rtmSetNumContStates(rtm, val) ((rtm)->Sizes.numContStates = (val))
#endif

#ifndef rtmGetNumDWork
# define rtmGetNumDWork(rtm)           ((rtm)->Sizes.numDwork)
#endif

#ifndef rtmSetNumDWork
# define rtmSetNumDWork(rtm, val)      ((rtm)->Sizes.numDwork = (val))
#endif

#ifndef rtmGetNumInputPorts
# define rtmGetNumInputPorts(rtm)      ((rtm)->Sizes.numIports)
#endif

#ifndef rtmSetNumInputPorts
# define rtmSetNumInputPorts(rtm, val) ((rtm)->Sizes.numIports = (val))
#endif

#ifndef rtmGetNumNonSampledZCs
# define rtmGetNumNonSampledZCs(rtm)   ((rtm)->Sizes.numNonSampZCs)
#endif

#ifndef rtmSetNumNonSampledZCs
# define rtmSetNumNonSampledZCs(rtm, val) ((rtm)->Sizes.numNonSampZCs = (val))
#endif

#ifndef rtmGetNumOutputPorts
# define rtmGetNumOutputPorts(rtm)     ((rtm)->Sizes.numOports)
#endif

#ifndef rtmSetNumOutputPorts
# define rtmSetNumOutputPorts(rtm, val) ((rtm)->Sizes.numOports = (val))
#endif

#ifndef rtmGetNumSFcnParams
# define rtmGetNumSFcnParams(rtm)      ((rtm)->Sizes.numSFcnPrms)
#endif

#ifndef rtmSetNumSFcnParams
# define rtmSetNumSFcnParams(rtm, val) ((rtm)->Sizes.numSFcnPrms = (val))
#endif

#ifndef rtmGetNumSFunctions
# define rtmGetNumSFunctions(rtm)      ((rtm)->Sizes.numSFcns)
#endif

#ifndef rtmSetNumSFunctions
# define rtmSetNumSFunctions(rtm, val) ((rtm)->Sizes.numSFcns = (val))
#endif

#ifndef rtmGetNumSampleTimes
# define rtmGetNumSampleTimes(rtm)     ((rtm)->Sizes.numSampTimes)
#endif

#ifndef rtmSetNumSampleTimes
# define rtmSetNumSampleTimes(rtm, val) ((rtm)->Sizes.numSampTimes = (val))
#endif

#ifndef rtmGetNumU
# define rtmGetNumU(rtm)               ((rtm)->Sizes.numU)
#endif

#ifndef rtmSetNumU
# define rtmSetNumU(rtm, val)          ((rtm)->Sizes.numU = (val))
#endif

#ifndef rtmGetNumY
# define rtmGetNumY(rtm)               ((rtm)->Sizes.numY)
#endif

#ifndef rtmSetNumY
# define rtmSetNumY(rtm, val)          ((rtm)->Sizes.numY = (val))
#endif

#ifndef rtmGetOdeF
# define rtmGetOdeF(rtm)               ()
#endif

#ifndef rtmSetOdeF
# define rtmSetOdeF(rtm, val)          ()
#endif

#ifndef rtmGetOdeY
# define rtmGetOdeY(rtm)               ()
#endif

#ifndef rtmSetOdeY
# define rtmSetOdeY(rtm, val)          ()
#endif

#ifndef rtmGetOffsetTimeArray
# define rtmGetOffsetTimeArray(rtm)    ((rtm)->Timing.offsetTimesArray)
#endif

#ifndef rtmSetOffsetTimeArray
# define rtmSetOffsetTimeArray(rtm, val) ((rtm)->Timing.offsetTimesArray = (val))
#endif

#ifndef rtmGetOffsetTimePtr
# define rtmGetOffsetTimePtr(rtm)      ((rtm)->Timing.offsetTimes)
#endif

#ifndef rtmSetOffsetTimePtr
# define rtmSetOffsetTimePtr(rtm, val) ((rtm)->Timing.offsetTimes = (val))
#endif

#ifndef rtmGetOptions
# define rtmGetOptions(rtm)            ((rtm)->Sizes.options)
#endif

#ifndef rtmSetOptions
# define rtmSetOptions(rtm, val)       ((rtm)->Sizes.options = (val))
#endif

#ifndef rtmGetParamIsMalloced
# define rtmGetParamIsMalloced(rtm)    ()
#endif

#ifndef rtmSetParamIsMalloced
# define rtmSetParamIsMalloced(rtm, val) ()
#endif

#ifndef rtmGetPath
# define rtmGetPath(rtm)               ((rtm)->path)
#endif

#ifndef rtmSetPath
# define rtmSetPath(rtm, val)          ((rtm)->path = (val))
#endif

#ifndef rtmGetPerTaskSampleHits
# define rtmGetPerTaskSampleHits(rtm)  ()
#endif

#ifndef rtmSetPerTaskSampleHits
# define rtmSetPerTaskSampleHits(rtm, val) ()
#endif

#ifndef rtmGetPerTaskSampleHitsArray
# define rtmGetPerTaskSampleHitsArray(rtm) ((rtm)->Timing.perTaskSampleHitsArray)
#endif

#ifndef rtmSetPerTaskSampleHitsArray
# define rtmSetPerTaskSampleHitsArray(rtm, val) ((rtm)->Timing.perTaskSampleHitsArray = (val))
#endif

#ifndef rtmGetPerTaskSampleHitsPtr
# define rtmGetPerTaskSampleHitsPtr(rtm) ((rtm)->Timing.perTaskSampleHits)
#endif

#ifndef rtmSetPerTaskSampleHitsPtr
# define rtmSetPerTaskSampleHitsPtr(rtm, val) ((rtm)->Timing.perTaskSampleHits = (val))
#endif

#ifndef rtmGetPrevZCSigState
# define rtmGetPrevZCSigState(rtm)     ((rtm)->ModelData.prevZCSigState)
#endif

#ifndef rtmSetPrevZCSigState
# define rtmSetPrevZCSigState(rtm, val) ((rtm)->ModelData.prevZCSigState = (val))
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmSetRTWExtModeInfo
# define rtmSetRTWExtModeInfo(rtm, val) ((rtm)->extModeInfo = (val))
#endif

#ifndef rtmGetRTWGeneratedSFcn
# define rtmGetRTWGeneratedSFcn(rtm)   ((rtm)->Sizes.rtwGenSfcn)
#endif

#ifndef rtmSetRTWGeneratedSFcn
# define rtmSetRTWGeneratedSFcn(rtm, val) ((rtm)->Sizes.rtwGenSfcn = (val))
#endif

#ifndef rtmGetRTWLogInfo
# define rtmGetRTWLogInfo(rtm)         ()
#endif

#ifndef rtmSetRTWLogInfo
# define rtmSetRTWLogInfo(rtm, val)    ()
#endif

#ifndef rtmGetRTWRTModelMethodsInfo
# define rtmGetRTWRTModelMethodsInfo(rtm) ()
#endif

#ifndef rtmSetRTWRTModelMethodsInfo
# define rtmSetRTWRTModelMethodsInfo(rtm, val) ()
#endif

#ifndef rtmGetRTWSfcnInfo
# define rtmGetRTWSfcnInfo(rtm)        ((rtm)->sfcnInfo)
#endif

#ifndef rtmSetRTWSfcnInfo
# define rtmSetRTWSfcnInfo(rtm, val)   ((rtm)->sfcnInfo = (val))
#endif

#ifndef rtmGetRTWSolverInfo
# define rtmGetRTWSolverInfo(rtm)      ((rtm)->solverInfo)
#endif

#ifndef rtmSetRTWSolverInfo
# define rtmSetRTWSolverInfo(rtm, val) ((rtm)->solverInfo = (val))
#endif

#ifndef rtmGetRTWSolverInfoPtr
# define rtmGetRTWSolverInfoPtr(rtm)   ((rtm)->solverInfoPtr)
#endif

#ifndef rtmSetRTWSolverInfoPtr
# define rtmSetRTWSolverInfoPtr(rtm, val) ((rtm)->solverInfoPtr = (val))
#endif

#ifndef rtmGetReservedForXPC
# define rtmGetReservedForXPC(rtm)     ((rtm)->SpecialInfo.xpcData)
#endif

#ifndef rtmSetReservedForXPC
# define rtmSetReservedForXPC(rtm, val) ((rtm)->SpecialInfo.xpcData = (val))
#endif

#ifndef rtmGetRootDWork
# define rtmGetRootDWork(rtm)          ((rtm)->ModelData.dwork)
#endif

#ifndef rtmSetRootDWork
# define rtmSetRootDWork(rtm, val)     ((rtm)->ModelData.dwork = (val))
#endif

#ifndef rtmGetSFunctions
# define rtmGetSFunctions(rtm)         ((rtm)->childSfunctions)
#endif

#ifndef rtmSetSFunctions
# define rtmSetSFunctions(rtm, val)    ((rtm)->childSfunctions = (val))
#endif

#ifndef rtmGetSampleHitArray
# define rtmGetSampleHitArray(rtm)     ((rtm)->Timing.sampleHitArray)
#endif

#ifndef rtmSetSampleHitArray
# define rtmSetSampleHitArray(rtm, val) ((rtm)->Timing.sampleHitArray = (val))
#endif

#ifndef rtmGetSampleHitPtr
# define rtmGetSampleHitPtr(rtm)       ((rtm)->Timing.sampleHits)
#endif

#ifndef rtmSetSampleHitPtr
# define rtmSetSampleHitPtr(rtm, val)  ((rtm)->Timing.sampleHits = (val))
#endif

#ifndef rtmGetSampleTimeArray
# define rtmGetSampleTimeArray(rtm)    ((rtm)->Timing.sampleTimesArray)
#endif

#ifndef rtmSetSampleTimeArray
# define rtmSetSampleTimeArray(rtm, val) ((rtm)->Timing.sampleTimesArray = (val))
#endif

#ifndef rtmGetSampleTimePtr
# define rtmGetSampleTimePtr(rtm)      ((rtm)->Timing.sampleTimes)
#endif

#ifndef rtmSetSampleTimePtr
# define rtmSetSampleTimePtr(rtm, val) ((rtm)->Timing.sampleTimes = (val))
#endif

#ifndef rtmGetSampleTimeTaskIDArray
# define rtmGetSampleTimeTaskIDArray(rtm) ((rtm)->Timing.sampleTimeTaskIDArray)
#endif

#ifndef rtmSetSampleTimeTaskIDArray
# define rtmSetSampleTimeTaskIDArray(rtm, val) ((rtm)->Timing.sampleTimeTaskIDArray = (val))
#endif

#ifndef rtmGetSampleTimeTaskIDPtr
# define rtmGetSampleTimeTaskIDPtr(rtm) ((rtm)->Timing.sampleTimeTaskIDPtr)
#endif

#ifndef rtmSetSampleTimeTaskIDPtr
# define rtmSetSampleTimeTaskIDPtr(rtm, val) ((rtm)->Timing.sampleTimeTaskIDPtr = (val))
#endif

#ifndef rtmGetSimMode
# define rtmGetSimMode(rtm)            ((rtm)->simMode)
#endif

#ifndef rtmSetSimMode
# define rtmSetSimMode(rtm, val)       ((rtm)->simMode = (val))
#endif

#ifndef rtmGetSimTimeStep
# define rtmGetSimTimeStep(rtm)        ((rtm)->Timing.simTimeStep)
#endif

#ifndef rtmSetSimTimeStep
# define rtmSetSimTimeStep(rtm, val)   ((rtm)->Timing.simTimeStep = (val))
#endif

#ifndef rtmGetStartTime
# define rtmGetStartTime(rtm)          ((rtm)->Timing.tStart)
#endif

#ifndef rtmSetStartTime
# define rtmSetStartTime(rtm, val)     ((rtm)->Timing.tStart = (val))
#endif

#ifndef rtmGetStepSize
# define rtmGetStepSize(rtm)           ((rtm)->Timing.stepSize)
#endif

#ifndef rtmSetStepSize
# define rtmSetStepSize(rtm, val)      ((rtm)->Timing.stepSize = (val))
#endif

#ifndef rtmGetStopRequestedFlag
# define rtmGetStopRequestedFlag(rtm)  ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequestedFlag
# define rtmSetStopRequestedFlag(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetTaskCounters
# define rtmGetTaskCounters(rtm)       ()
#endif

#ifndef rtmSetTaskCounters
# define rtmSetTaskCounters(rtm, val)  ()
#endif

#ifndef rtmGetTaskTimeArray
# define rtmGetTaskTimeArray(rtm)      ((rtm)->Timing.tArray)
#endif

#ifndef rtmSetTaskTimeArray
# define rtmSetTaskTimeArray(rtm, val) ((rtm)->Timing.tArray = (val))
#endif

#ifndef rtmGetTimePtr
# define rtmGetTimePtr(rtm)            ((rtm)->Timing.t)
#endif

#ifndef rtmSetTimePtr
# define rtmSetTimePtr(rtm, val)       ((rtm)->Timing.t = (val))
#endif

#ifndef rtmGetTimingData
# define rtmGetTimingData(rtm)         ((rtm)->Timing.timingData)
#endif

#ifndef rtmSetTimingData
# define rtmSetTimingData(rtm, val)    ((rtm)->Timing.timingData = (val))
#endif

#ifndef rtmGetU
# define rtmGetU(rtm)                  ((rtm)->ModelData.inputs)
#endif

#ifndef rtmSetU
# define rtmSetU(rtm, val)             ((rtm)->ModelData.inputs = (val))
#endif

#ifndef rtmGetVarNextHitTimesListPtr
# define rtmGetVarNextHitTimesListPtr(rtm) ((rtm)->Timing.varNextHitTimesList)
#endif

#ifndef rtmSetVarNextHitTimesListPtr
# define rtmSetVarNextHitTimesListPtr(rtm, val) ((rtm)->Timing.varNextHitTimesList = (val))
#endif

#ifndef rtmGetY
# define rtmGetY(rtm)                  ((rtm)->ModelData.outputs)
#endif

#ifndef rtmSetY
# define rtmSetY(rtm, val)             ((rtm)->ModelData.outputs = (val))
#endif

#ifndef rtmGetZCCacheNeedsReset
# define rtmGetZCCacheNeedsReset(rtm)  ((rtm)->ModelData.zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
# define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->ModelData.zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetZCSignalValues
# define rtmGetZCSignalValues(rtm)     ((rtm)->ModelData.zcSignalValues)
#endif

#ifndef rtmSetZCSignalValues
# define rtmSetZCSignalValues(rtm, val) ((rtm)->ModelData.zcSignalValues = (val))
#endif

#ifndef rtmGet_TimeOfLastOutput
# define rtmGet_TimeOfLastOutput(rtm)  ((rtm)->Timing.timeOfLastOutput)
#endif

#ifndef rtmSet_TimeOfLastOutput
# define rtmSet_TimeOfLastOutput(rtm, val) ((rtm)->Timing.timeOfLastOutput = (val))
#endif

#ifndef rtmGetdX
# define rtmGetdX(rtm)                 ((rtm)->ModelData.derivs)
#endif

#ifndef rtmSetdX
# define rtmSetdX(rtm, val)            ((rtm)->ModelData.derivs = (val))
#endif

#ifndef rtmGetChecksumVal
# define rtmGetChecksumVal(rtm, idx)   ((rtm)->Sizes.checksums[idx])
#endif

#ifndef rtmSetChecksumVal
# define rtmSetChecksumVal(rtm, idx, val) ((rtm)->Sizes.checksums[idx] = (val))
#endif

#ifndef rtmGetDWork
# define rtmGetDWork(rtm, idx)         ((rtm)->ModelData.dwork[idx])
#endif

#ifndef rtmSetDWork
# define rtmSetDWork(rtm, idx, val)    ((rtm)->ModelData.dwork[idx] = (val))
#endif

#ifndef rtmGetOffsetTime
# define rtmGetOffsetTime(rtm, idx)    ((rtm)->Timing.offsetTimes[idx])
#endif

#ifndef rtmSetOffsetTime
# define rtmSetOffsetTime(rtm, idx, val) ((rtm)->Timing.offsetTimes[idx] = (val))
#endif

#ifndef rtmGetSFunction
# define rtmGetSFunction(rtm, idx)     ((rtm)->childSfunctions[idx])
#endif

#ifndef rtmSetSFunction
# define rtmSetSFunction(rtm, idx, val) ((rtm)->childSfunctions[idx] = (val))
#endif

#ifndef rtmGetSampleTime
# define rtmGetSampleTime(rtm, idx)    ((rtm)->Timing.sampleTimes[idx])
#endif

#ifndef rtmSetSampleTime
# define rtmSetSampleTime(rtm, idx, val) ((rtm)->Timing.sampleTimes[idx] = (val))
#endif

#ifndef rtmGetSampleTimeTaskID
# define rtmGetSampleTimeTaskID(rtm, idx) ((rtm)->Timing.sampleTimeTaskIDPtr[idx])
#endif

#ifndef rtmSetSampleTimeTaskID
# define rtmSetSampleTimeTaskID(rtm, idx, val) ((rtm)->Timing.sampleTimeTaskIDPtr[idx] = (val))
#endif

#ifndef rtmGetVarNextHitTimeList
# define rtmGetVarNextHitTimeList(rtm, idx) ((rtm)->Timing.varNextHitTimesList[idx])
#endif

#ifndef rtmSetVarNextHitTimeList
# define rtmSetVarNextHitTimeList(rtm, idx, val) ((rtm)->Timing.varNextHitTimesList[idx] = (val))
#endif

#ifndef rtmIsContinuousTask
# define rtmIsContinuousTask(rtm, tid) ((tid) == 0)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmIsMajorTimeStep
# define rtmIsMajorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MAJOR_TIME_STEP)
#endif

#ifndef rtmIsMinorTimeStep
# define rtmIsMinorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MINOR_TIME_STEP)
#endif

#ifndef rtmIsSampleHit
# define rtmIsSampleHit(rtm, sti, tid) ((rtmIsMajorTimeStep((rtm)) && (rtm)->Timing.sampleHits[(rtm)->Timing.sampleTimeTaskIDPtr[sti]]))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmSetT
# define rtmSetT(rtm, val)                                       /* Do Nothing */
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmSetTFinal
# define rtmSetTFinal(rtm, val)        ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               ((rtm)->Timing.t)
#endif

#ifndef rtmSetTPtr
# define rtmSetTPtr(rtm, val)          ((rtm)->Timing.t = (val))
#endif

#ifndef rtmGetTStart
# define rtmGetTStart(rtm)             ((rtm)->Timing.tStart)
#endif

#ifndef rtmSetTStart
# define rtmSetTStart(rtm, val)        ((rtm)->Timing.tStart = (val))
#endif

#ifndef rtmGetTaskTime
# define rtmGetTaskTime(rtm, sti)      (rtmGetTPtr((rtm))[(rtm)->Timing.sampleTimeTaskIDPtr[sti]])
#endif

#ifndef rtmSetTaskTime
# define rtmSetTaskTime(rtm, sti, val) (rtmGetTPtr((rtm))[sti] = (val))
#endif

#ifndef rtmGetTimeOfLastOutput
# define rtmGetTimeOfLastOutput(rtm)   ((rtm)->Timing.timeOfLastOutput)
#endif

#ifdef rtmGetRTWSolverInfo
#undef rtmGetRTWSolverInfo
#endif

#define rtmGetRTWSolverInfo(rtm)       &((rtm)->solverInfo)

/* Definition for use in the target main file */
#define drum_bench_working_rtModel     RT_MODEL_drum_bench_working_T

/* Block signals for system '<S1>/Filtered Derivative' */
typedef struct {
  real_T Filter;                       /* '<S12>/Filter' */
  real_T Sum;                          /* '<S12>/Sum' */
  real_T FilterCoefficient;            /* '<S12>/Filter Coefficient' */
} B_FilteredDerivative_drum_ben_T;

/* Block states (auto storage) for system '<S1>/Filtered Derivative' */
typedef struct {
  real_T Filter_DSTATE;                /* '<S12>/Filter' */
} DW_FilteredDerivative_drum_be_T;

/* Block signals for system '<S81>/enabled subsystem' */
typedef struct {
  uint8_T In1[20];                     /* '<S93>/In1' */
} B_enabledsubsystem_drum_bench_T;

/* Block signals for system '<S82>/Enabled Subsystem' */
typedef struct {
  uint8_T In1[20];                     /* '<S95>/In1' */
} B_EnabledSubsystem_drum_bench_T;

/* Block signals for system '<S96>/MATLAB Function1' */
typedef struct {
  real32_T id;                         /* '<S96>/MATLAB Function1' */
  real32_T value;                      /* '<S96>/MATLAB Function1' */
  int32_T type;                        /* '<S96>/MATLAB Function1' */
  int32_T index;                       /* '<S96>/MATLAB Function1' */
} B_MATLABFunction1_drum_bench__T;

/* Block states (auto storage) for system '<S96>/MATLAB Function1' */
typedef struct {
  real_T i;                            /* '<S96>/MATLAB Function1' */
  real32_T id_sent;                    /* '<S96>/MATLAB Function1' */
  uint32_T method;                     /* '<S96>/MATLAB Function1' */
  uint32_T state;                      /* '<S96>/MATLAB Function1' */
  uint32_T state_g[2];                 /* '<S96>/MATLAB Function1' */
  uint32_T state_gf[625];              /* '<S96>/MATLAB Function1' */
  boolean_T state_not_empty;           /* '<S96>/MATLAB Function1' */
} DW_MATLABFunction1_drum_bench_T;

/* Block signals for system '<S63>/select rpc' */
typedef struct {
  real32_T id;                         /* '<S63>/select rpc' */
  int32_T type;                        /* '<S63>/select rpc' */
  uint8_T data[20];                    /* '<S63>/select rpc' */
} B_selectrpc_drum_bench_workin_T;

/* Block signals (auto storage) */
typedef struct {
  MarlinFullStatus BusCreator;         /* '<S7>/Bus Creator' */
  MarlinFullStatus BusCreator_i;       /* '<S8>/Bus Creator' */
  MarlinBasicStatus BusConversion_InsertedFor_BusCr;
  MarlinBasicStatus marlin_basic_status;/* '<S5>/Bus Selector' */
  MarlinBasicStatus BusConversion_InsertedFor_Bus_a;
  MarlinBasicStatus marlin_basic_status_l;/* '<S6>/Bus Selector' */
  MarlinFullCommand BusCreator_n;      /* '<S3>/Bus Creator' */
  MarlinFullCommand BusAssignment;     /* '<S1>/Bus Assignment' */
  MarlinFullCommand BusAssignment_n;   /* '<S9>/Bus Assignment' */
  MarlinFullCommand modeswitch;        /* '<Root>/mode switch' */
  MarlinFullCommand BusCreator_f;      /* '<S4>/Bus Creator' */
  MarlinFullCommand BusAssignment_n4;  /* '<S2>/Bus Assignment' */
  MarlinFullCommand BusAssignment_nx;  /* '<S10>/Bus Assignment' */
  MarlinFullCommand modeswitch1;       /* '<Root>/mode switch1' */
  MarlinFullInternalRPC BusCreator_nf; /* '<S76>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_k;  /* '<S77>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_iq; /* '<S78>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_o;  /* '<S79>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_l;  /* '<S80>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_a;  /* '<S96>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_h;  /* '<S88>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_d;  /* '<S81>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_b;  /* '<S91>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_bc; /* '<S90>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_ld; /* '<S82>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_c;  /* '<S83>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_hl; /* '<S84>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_nh; /* '<S85>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_g;  /* '<S89>/Bus Creator' */
  MarlinFullInternalRPC VectorConcatenate[15];/* '<S63>/Vector Concatenate' */
  MarlinFullInternalRPC BusCreator_br; /* '<S141>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_ih; /* '<S142>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_g1; /* '<S143>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_i2; /* '<S144>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_lg; /* '<S145>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_j;  /* '<S161>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_dy; /* '<S153>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_d1; /* '<S146>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_fc; /* '<S156>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_bw; /* '<S155>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_ow; /* '<S147>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_dj; /* '<S148>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_om; /* '<S149>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_co; /* '<S150>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_gs; /* '<S154>/Bus Creator' */
  MarlinFullInternalRPC VectorConcatenate_a[15];/* '<S128>/Vector Concatenate' */
  MarlinFullInternalRPA BusConversion_InsertedFor_MATLA;
  MarlinFullInternalRPA BusConversion_InsertedFor_MAT_c;
  real_T motor_torque;                 /* '<S64>/motor_torque_' */
  real_T motor_iq;                     /* '<S64>/motor_iq_' */
  real_T motor_vq_avg;                 /* '<S64>/motor_vq_avg_' */
  real_T analog_1;                     /* '<S64>/analog_1_' */
  real_T analog_1_dot;                 /* '<S64>/analog_1_dot_' */
  real_T analog_2;                     /* '<S64>/analog_2_' */
  real_T analog_2_dot;                 /* '<S64>/analog_2_dot_' */
  real_T analog_diff;                  /* '<S64>/analog_diff_' */
  real_T analog_diff_dot;              /* '<S64>/analog_diff_dot_' */
  real_T quadrature_1;                 /* '<S64>/quadrature_1_' */
  real_T quadrature_1_dot;             /* '<S64>/quadrature_1_dot_' */
  real_T quadrature_2;                 /* '<S64>/quadrature_2_' */
  real_T quadrature_2_dot;             /* '<S64>/quadrature_2_dot_' */
  real_T ssi;                          /* '<S64>/ssi_' */
  real_T ssi_dot;                      /* '<S64>/ssi_dot_' */
  real_T accelerometer_x;              /* '<S64>/accelerometer_x_' */
  real_T accelerometer_y;              /* '<S64>/accelerometer_y_' */
  real_T accelerometer_z;              /* '<S64>/accelerometer_z_' */
  real_T bus_v;                        /* '<S64>/bus_v_' */
  real_T Delay[2];                     /* '<S62>/Delay' */
  real_T eA1[2];                       /* '<S62>/eA1' */
  real_T MathFunction;                 /* '<S62>/Math Function' */
  real_T resistance;                   /* '<S62>/resistance' */
  real_T eA2[2];                       /* '<S62>/eA2' */
  real_T Sum[2];                       /* '<S62>/Sum' */
  real_T temperature_motor_winding;    /* '<S5>/Bus Selector' */
  real_T temperature_motor_housing;    /* '<S5>/Bus Selector' */
  real_T motor_torque_m;               /* '<S34>/Bus Selector' */
  real_T motor_iq_a;                   /* '<S34>/Bus Selector' */
  real_T motor_vq_avg_f;               /* '<S34>/Bus Selector' */
  real_T analog_1_g;                   /* '<S34>/Bus Selector' */
  real_T analog_1_dot_h;               /* '<S34>/Bus Selector' */
  real_T analog_2_k;                   /* '<S34>/Bus Selector' */
  real_T analog_2_dot_m;               /* '<S34>/Bus Selector' */
  real_T analog_diff_h;                /* '<S34>/Bus Selector' */
  real_T analog_diff_dot_k;            /* '<S34>/Bus Selector' */
  real_T quadrature_1_h;               /* '<S34>/Bus Selector' */
  real_T quadrature_1_dot_l;           /* '<S34>/Bus Selector' */
  real_T quadrature_2_o;               /* '<S34>/Bus Selector' */
  real_T quadrature_2_dot_l;           /* '<S34>/Bus Selector' */
  real_T ssi_c;                        /* '<S34>/Bus Selector' */
  real_T ssi_dot_f;                    /* '<S34>/Bus Selector' */
  real_T accelerometer_x_n;            /* '<S34>/Bus Selector' */
  real_T accelerometer_y_a;            /* '<S34>/Bus Selector' */
  real_T accelerometer_z_l;            /* '<S34>/Bus Selector' */
  real_T bus_v_k;                      /* '<S34>/Bus Selector' */
  real_T accelerometer_x_e;            /* '<S34>/accelerometer_x' */
  real_T accelerometer_y_b;            /* '<S34>/accelerometer_y' */
  real_T accelerometer_z_d;            /* '<S34>/accelerometer_z' */
  real_T analog_1_o;                   /* '<S34>/analog_1' */
  real_T analog_1_dot_d;               /* '<S34>/analog_1_dot' */
  real_T analog_2_p;                   /* '<S34>/analog_2' */
  real_T analog_2_dot_l;               /* '<S34>/analog_2_dot' */
  real_T analog_diff_b;                /* '<S34>/analog_diff' */
  real_T analog_diff_dot_f;            /* '<S34>/analog_diff_dot' */
  real_T bus_v_d;                      /* '<S34>/bus_v' */
  real_T motor_iq_g;                   /* '<S34>/motor_iq' */
  real_T motor_torque_f;               /* '<S34>/motor_torque' */
  real_T motor_vq_avg_b;               /* '<S34>/motor_vq_avg' */
  real_T quadrature_1_j;               /* '<S34>/quadrature_1' */
  real_T quadrature_1_dot_c;           /* '<S34>/quadrature_1_dot' */
  real_T quadrature_2_l;               /* '<S34>/quadrature_2' */
  real_T quadrature_2_dot_o;           /* '<S34>/quadrature_2_dot' */
  real_T ssi_f;                        /* '<S34>/ssi' */
  real_T ssi_dot_k;                    /* '<S34>/ssi_dot' */
  real_T temperature_motor_housing_j;  /* '<S5>/temperature_motor_housing' */
  real_T temperature_motor_winding_h;  /* '<S5>/temperature_motor_winding' */
  real_T Sum_c;                        /* '<S17>/Sum' */
  real_T RateLimiter;                  /* '<S17>/Rate Limiter' */
  real_T DataTypeConversion;           /* '<S17>/Data Type Conversion' */
  real_T Sum_p;                        /* '<S18>/Sum' */
  real_T RateLimiter_o;                /* '<S18>/Rate Limiter' */
  real_T DataTypeConversion_a;         /* '<S18>/Data Type Conversion' */
  real_T Sum_cn;                       /* '<S14>/Sum' */
  real_T RateLimiter_b;                /* '<S14>/Rate Limiter' */
  real_T DataTypeConversion_c;         /* '<S14>/Data Type Conversion' */
  real_T Sum_d;                        /* '<S15>/Sum' */
  real_T RateLimiter_k;                /* '<S15>/Rate Limiter' */
  real_T DataTypeConversion_n;         /* '<S15>/Data Type Conversion' */
  real_T Sum_l;                        /* '<S16>/Sum' */
  real_T RateLimiter_e;                /* '<S16>/Rate Limiter' */
  real_T DataTypeConversion_d;         /* '<S16>/Data Type Conversion' */
  real_T quadrature_1_angle;           /* '<S1>/Bus Selector2' */
  real_T quadrature_1_angle_dot;       /* '<S1>/Bus Selector2' */
  real_T quadrature_1_n;               /* '<S1>/Bus Selector3' */
  real_T Sum_m;                        /* '<S1>/Sum' */
  real_T kp;                           /* '<S1>/kp' */
  real_T Saturation;                   /* '<S1>/Saturation' */
  real_T Sum1;                         /* '<S1>/Sum1' */
  real_T kd;                           /* '<S1>/kd' */
  real_T Sum2;                         /* '<S1>/Sum2' */
  real_T motor_torque_p;               /* '<S1>/motor_torque' */
  real_T quadrature_1_angle_k;         /* '<S9>/Bus Selector2' */
  real_T quadrature_1_angle_dot_o;     /* '<S9>/Bus Selector2' */
  real_T quadrature_1_f;               /* '<S9>/Bus Selector1' */
  real_T analog_1_i;                   /* '<S9>/Bus Selector1' */
  real_T Sum_e;                        /* '<S9>/Sum' */
  real_T bias;                         /* '<S9>/bias' */
  real_T ka;                           /* '<S9>/ka' */
  real_T Product;                      /* '<S9>/Product' */
  real_T Sum1_o;                       /* '<S9>/Sum1' */
  real_T kd_n;                         /* '<S9>/kd' */
  real_T Sum2_b;                       /* '<S9>/Sum2' */
  real_T mode;                         /* '<Root>/Bus Selector' */
  real_T motor_torque_h;               /* '<Root>/Bus Selector' */
  real_T motor_iq_b;                   /* '<Root>/Bus Selector' */
  real_T Clock1;                       /* '<S19>/Clock1' */
  real_T Product_j;                    /* '<S19>/Product' */
  real_T Gain;                         /* '<S19>/Gain' */
  real_T Product1;                     /* '<S19>/Product1' */
  real_T Sum_j;                        /* '<S19>/Sum' */
  real_T Product2;                     /* '<S19>/Product2' */
  real_T Output;                       /* '<S19>/Output' */
  real_T SineWave;                     /* '<S14>/Sine Wave' */
  real_T Sign;                         /* '<S14>/Sign' */
  real_T MultiportSwitch;              /* '<S14>/Multiport Switch' */
  real_T Gain_a;                       /* '<S14>/Gain' */
  real_T Clock1_n;                     /* '<S20>/Clock1' */
  real_T Product_p;                    /* '<S20>/Product' */
  real_T Gain_b;                       /* '<S20>/Gain' */
  real_T Product1_o;                   /* '<S20>/Product1' */
  real_T Sum_k;                        /* '<S20>/Sum' */
  real_T Product2_p;                   /* '<S20>/Product2' */
  real_T Output_p;                     /* '<S20>/Output' */
  real_T SineWave_h;                   /* '<S15>/Sine Wave' */
  real_T Sign_a;                       /* '<S15>/Sign' */
  real_T MultiportSwitch_e;            /* '<S15>/Multiport Switch' */
  real_T Gain_bc;                      /* '<S15>/Gain' */
  real_T Clock1_e;                     /* '<S21>/Clock1' */
  real_T Product_b;                    /* '<S21>/Product' */
  real_T Gain_n;                       /* '<S21>/Gain' */
  real_T Product1_c;                   /* '<S21>/Product1' */
  real_T Sum_kn;                       /* '<S21>/Sum' */
  real_T Product2_n;                   /* '<S21>/Product2' */
  real_T Output_n;                     /* '<S21>/Output' */
  real_T SineWave_f;                   /* '<S16>/Sine Wave' */
  real_T Sign_m;                       /* '<S16>/Sign' */
  real_T MultiportSwitch_l;            /* '<S16>/Multiport Switch' */
  real_T Gain_l;                       /* '<S16>/Gain' */
  real_T Clock1_g;                     /* '<S22>/Clock1' */
  real_T Product_d;                    /* '<S22>/Product' */
  real_T Gain_nc;                      /* '<S22>/Gain' */
  real_T Product1_i;                   /* '<S22>/Product1' */
  real_T Sum_i;                        /* '<S22>/Sum' */
  real_T Product2_i;                   /* '<S22>/Product2' */
  real_T Output_h;                     /* '<S22>/Output' */
  real_T SineWave_l;                   /* '<S17>/Sine Wave' */
  real_T Sign_f;                       /* '<S17>/Sign' */
  real_T MultiportSwitch_n;            /* '<S17>/Multiport Switch' */
  real_T Gain_f;                       /* '<S17>/Gain' */
  real_T Clock1_j;                     /* '<S23>/Clock1' */
  real_T Product_h;                    /* '<S23>/Product' */
  real_T Gain_nz;                      /* '<S23>/Gain' */
  real_T Product1_f;                   /* '<S23>/Product1' */
  real_T Sum_b;                        /* '<S23>/Sum' */
  real_T Product2_l;                   /* '<S23>/Product2' */
  real_T Output_c;                     /* '<S23>/Output' */
  real_T SineWave_n;                   /* '<S18>/Sine Wave' */
  real_T Sign_i;                       /* '<S18>/Sign' */
  real_T MultiportSwitch_b;            /* '<S18>/Multiport Switch' */
  real_T Gain_g;                       /* '<S18>/Gain' */
  real_T rpc;                          /* '<S7>/Bus Selector' */
  real_T debug;                        /* '<S64>/debug_' */
  real_T mode_k;                       /* '<S61>/Bus Selector' */
  real_T motor_iq_k;                   /* '<S61>/Bus Selector1' */
  real_T motor_id;                     /* '<S61>/Bus Selector1' */
  real_T motor_vq;                     /* '<S61>/Bus Selector2' */
  real_T motor_vd;                     /* '<S61>/Bus Selector2' */
  real_T analog_diff_g;                /* '<S61>/Bus Selector3' */
  real_T analog_diff_dot_o;            /* '<S61>/Bus Selector3' */
  real_T quadrature_1_angle_b;         /* '<S61>/Bus Selector4' */
  real_T quadrature_1_angle_dot_j;     /* '<S61>/Bus Selector4' */
  real_T motor_torque_mv;              /* '<S61>/Bus Selector5' */
  real_T VectorConcatenate_d[76];      /* '<S97>/Vector Concatenate' */
  real_T DataTypeConversion_cm[10];    /* '<S81>/Data Type Conversion' */
  real_T analog_limit;                 /* '<S81>/analog_limit' */
  real_T ethercat;                     /* '<S81>/ethercat' */
  real_T external_panic;               /* '<S81>/external_panic' */
  real_T motor_encoder;                /* '<S81>/motor encoder' */
  real_T over_current;                 /* '<S81>/over_current' */
  real_T over_voltage;                 /* '<S81>/over_voltage' */
  real_T quadrature_1_high;            /* '<S81>/quadrature_1_high' */
  real_T quadrature_1_low;             /* '<S81>/quadrature_1_low' */
  real_T quadrature_2_limit;           /* '<S81>/quadrature_2_limit' */
  real_T under_voltage;                /* '<S81>/under_voltage' */
  real_T mode0;                        /* '<Root>/mode0' */
  real_T motor_iq0;                    /* '<Root>/motor_iq0' */
  real_T motor_torque0;                /* '<Root>/motor_torque0' */
  real_T motor_torque_l;               /* '<S129>/motor_torque_' */
  real_T motor_iq_bj;                  /* '<S129>/motor_iq_' */
  real_T motor_vq_avg_e;               /* '<S129>/motor_vq_avg_' */
  real_T analog_1_on;                  /* '<S129>/analog_1_' */
  real_T analog_1_dot_k;               /* '<S129>/analog_1_dot_' */
  real_T analog_2_h;                   /* '<S129>/analog_2_' */
  real_T analog_2_dot_h;               /* '<S129>/analog_2_dot_' */
  real_T analog_diff_o;                /* '<S129>/analog_diff_' */
  real_T analog_diff_dot_m;            /* '<S129>/analog_diff_dot_' */
  real_T quadrature_1_i;               /* '<S129>/quadrature_1_' */
  real_T quadrature_1_dot_e;           /* '<S129>/quadrature_1_dot_' */
  real_T quadrature_2_h;               /* '<S129>/quadrature_2_' */
  real_T quadrature_2_dot_b;           /* '<S129>/quadrature_2_dot_' */
  real_T ssi_b;                        /* '<S129>/ssi_' */
  real_T ssi_dot_b;                    /* '<S129>/ssi_dot_' */
  real_T accelerometer_x_ej;           /* '<S129>/accelerometer_x_' */
  real_T accelerometer_y_j;            /* '<S129>/accelerometer_y_' */
  real_T accelerometer_z_i;            /* '<S129>/accelerometer_z_' */
  real_T bus_v_l;                      /* '<S129>/bus_v_' */
  real_T Delay_l[2];                   /* '<S127>/Delay' */
  real_T eA1_c[2];                     /* '<S127>/eA1' */
  real_T MathFunction_p;               /* '<S127>/Math Function' */
  real_T resistance_e;                 /* '<S127>/resistance' */
  real_T eA2_f[2];                     /* '<S127>/eA2' */
  real_T Sum_do[2];                    /* '<S127>/Sum' */
  real_T temperature_motor_winding_k;  /* '<S6>/Bus Selector' */
  real_T temperature_motor_housing_jp; /* '<S6>/Bus Selector' */
  real_T motor_torque_pd;              /* '<S47>/Bus Selector' */
  real_T motor_iq_f;                   /* '<S47>/Bus Selector' */
  real_T motor_vq_avg_n;               /* '<S47>/Bus Selector' */
  real_T analog_1_m;                   /* '<S47>/Bus Selector' */
  real_T analog_1_dot_f;               /* '<S47>/Bus Selector' */
  real_T analog_2_b;                   /* '<S47>/Bus Selector' */
  real_T analog_2_dot_e;               /* '<S47>/Bus Selector' */
  real_T analog_diff_hz;               /* '<S47>/Bus Selector' */
  real_T analog_diff_dot_h;            /* '<S47>/Bus Selector' */
  real_T quadrature_1_a;               /* '<S47>/Bus Selector' */
  real_T quadrature_1_dot_j;           /* '<S47>/Bus Selector' */
  real_T quadrature_2_lz;              /* '<S47>/Bus Selector' */
  real_T quadrature_2_dot_o4;          /* '<S47>/Bus Selector' */
  real_T ssi_n;                        /* '<S47>/Bus Selector' */
  real_T ssi_dot_l;                    /* '<S47>/Bus Selector' */
  real_T accelerometer_x_f;            /* '<S47>/Bus Selector' */
  real_T accelerometer_y_jg;           /* '<S47>/Bus Selector' */
  real_T accelerometer_z_g;            /* '<S47>/Bus Selector' */
  real_T bus_v_p;                      /* '<S47>/Bus Selector' */
  real_T accelerometer_x_d;            /* '<S47>/accelerometer_x' */
  real_T accelerometer_y_e;            /* '<S47>/accelerometer_y' */
  real_T accelerometer_z_b;            /* '<S47>/accelerometer_z' */
  real_T analog_1_b;                   /* '<S47>/analog_1' */
  real_T analog_1_dot_b;               /* '<S47>/analog_1_dot' */
  real_T analog_2_a;                   /* '<S47>/analog_2' */
  real_T analog_2_dot_k;               /* '<S47>/analog_2_dot' */
  real_T analog_diff_n;                /* '<S47>/analog_diff' */
  real_T analog_diff_dot_i;            /* '<S47>/analog_diff_dot' */
  real_T bus_v_pu;                     /* '<S47>/bus_v' */
  real_T motor_iq_gt;                  /* '<S47>/motor_iq' */
  real_T motor_torque_pz;              /* '<S47>/motor_torque' */
  real_T motor_vq_avg_i;               /* '<S47>/motor_vq_avg' */
  real_T quadrature_1_ib;              /* '<S47>/quadrature_1' */
  real_T quadrature_1_dot_f;           /* '<S47>/quadrature_1_dot' */
  real_T quadrature_2_g;               /* '<S47>/quadrature_2' */
  real_T quadrature_2_dot_e;           /* '<S47>/quadrature_2_dot' */
  real_T ssi_h;                        /* '<S47>/ssi' */
  real_T ssi_dot_o;                    /* '<S47>/ssi_dot' */
  real_T temperature_motor_housing_m;  /* '<S6>/temperature_motor_housing' */
  real_T temperature_motor_winding_i;  /* '<S6>/temperature_motor_winding' */
  real_T Sum_cq;                       /* '<S27>/Sum' */
  real_T RateLimiter_d;                /* '<S27>/Rate Limiter' */
  real_T DataTypeConversion_m;         /* '<S27>/Data Type Conversion' */
  real_T Sum_a;                        /* '<S28>/Sum' */
  real_T RateLimiter_f;                /* '<S28>/Rate Limiter' */
  real_T DataTypeConversion_c2;        /* '<S28>/Data Type Conversion' */
  real_T Sum_g;                        /* '<S24>/Sum' */
  real_T RateLimiter_b5;               /* '<S24>/Rate Limiter' */
  real_T DataTypeConversion_l;         /* '<S24>/Data Type Conversion' */
  real_T Sum_bf;                       /* '<S25>/Sum' */
  real_T RateLimiter_eb;               /* '<S25>/Rate Limiter' */
  real_T DataTypeConversion_j;         /* '<S25>/Data Type Conversion' */
  real_T Sum_o;                        /* '<S26>/Sum' */
  real_T RateLimiter_ol;               /* '<S26>/Rate Limiter' */
  real_T DataTypeConversion_e;         /* '<S26>/Data Type Conversion' */
  real_T quadrature_1_angle_j;         /* '<S2>/Bus Selector2' */
  real_T quadrature_1_angle_dot_of;    /* '<S2>/Bus Selector2' */
  real_T quadrature_1_nt;              /* '<S2>/Bus Selector3' */
  real_T Sum_f;                        /* '<S2>/Sum' */
  real_T kp_h;                         /* '<S2>/kp' */
  real_T Saturation_l;                 /* '<S2>/Saturation' */
  real_T Sum1_h;                       /* '<S2>/Sum1' */
  real_T kd_j;                         /* '<S2>/kd' */
  real_T Sum2_e;                       /* '<S2>/Sum2' */
  real_T motor_torque_b;               /* '<S2>/motor_torque' */
  real_T quadrature_1_angle_o;         /* '<S10>/Bus Selector2' */
  real_T quadrature_1_angle_dot_e;     /* '<S10>/Bus Selector2' */
  real_T quadrature_1_o;               /* '<S10>/Bus Selector1' */
  real_T analog_1_h;                   /* '<S10>/Bus Selector1' */
  real_T Sum_co;                       /* '<S10>/Sum' */
  real_T bias_c;                       /* '<S10>/bias' */
  real_T ka_l;                         /* '<S10>/ka' */
  real_T Product_i;                    /* '<S10>/Product' */
  real_T Sum1_e;                       /* '<S10>/Sum1' */
  real_T kd_l;                         /* '<S10>/kd' */
  real_T Sum2_o;                       /* '<S10>/Sum2' */
  real_T mode_l;                       /* '<Root>/Bus Selector1' */
  real_T motor_torque_px;              /* '<Root>/Bus Selector1' */
  real_T motor_iq_n;                   /* '<Root>/Bus Selector1' */
  real_T Clock1_gp;                    /* '<S29>/Clock1' */
  real_T Product_bh;                   /* '<S29>/Product' */
  real_T Gain_a3;                      /* '<S29>/Gain' */
  real_T Product1_e;                   /* '<S29>/Product1' */
  real_T Sum_i2;                       /* '<S29>/Sum' */
  real_T Product2_d;                   /* '<S29>/Product2' */
  real_T Output_i;                     /* '<S29>/Output' */
  real_T SineWave_ff;                  /* '<S24>/Sine Wave' */
  real_T Sign_ml;                      /* '<S24>/Sign' */
  real_T MultiportSwitch_h;            /* '<S24>/Multiport Switch' */
  real_T Gain_ncb;                     /* '<S24>/Gain' */
  real_T Clock1_f;                     /* '<S30>/Clock1' */
  real_T Product_ie;                   /* '<S30>/Product' */
  real_T Gain_d;                       /* '<S30>/Gain' */
  real_T Product1_p;                   /* '<S30>/Product1' */
  real_T Sum_c3;                       /* '<S30>/Sum' */
  real_T Product2_np;                  /* '<S30>/Product2' */
  real_T Output_f;                     /* '<S30>/Output' */
  real_T SineWave_hf;                  /* '<S25>/Sine Wave' */
  real_T Sign_h;                       /* '<S25>/Sign' */
  real_T MultiportSwitch_o;            /* '<S25>/Multiport Switch' */
  real_T Gain_gv;                      /* '<S25>/Gain' */
  real_T Clock1_l;                     /* '<S31>/Clock1' */
  real_T Product_k;                    /* '<S31>/Product' */
  real_T Gain_h;                       /* '<S31>/Gain' */
  real_T Product1_pt;                  /* '<S31>/Product1' */
  real_T Sum_dl;                       /* '<S31>/Sum' */
  real_T Product2_j;                   /* '<S31>/Product2' */
  real_T Output_e;                     /* '<S31>/Output' */
  real_T SineWave_g;                   /* '<S26>/Sine Wave' */
  real_T Sign_k;                       /* '<S26>/Sign' */
  real_T MultiportSwitch_a;            /* '<S26>/Multiport Switch' */
  real_T Gain_i;                       /* '<S26>/Gain' */
  real_T Clock1_d;                     /* '<S32>/Clock1' */
  real_T Product_kg;                   /* '<S32>/Product' */
  real_T Gain_c;                       /* '<S32>/Gain' */
  real_T Product1_j;                   /* '<S32>/Product1' */
  real_T Sum_en;                       /* '<S32>/Sum' */
  real_T Product2_g;                   /* '<S32>/Product2' */
  real_T Output_m;                     /* '<S32>/Output' */
  real_T SineWave_m;                   /* '<S27>/Sine Wave' */
  real_T Sign_l;                       /* '<S27>/Sign' */
  real_T MultiportSwitch_i;            /* '<S27>/Multiport Switch' */
  real_T Gain_m;                       /* '<S27>/Gain' */
  real_T Clock1_h;                     /* '<S33>/Clock1' */
  real_T Product_if;                   /* '<S33>/Product' */
  real_T Gain_p;                       /* '<S33>/Gain' */
  real_T Product1_j2;                  /* '<S33>/Product1' */
  real_T Sum_ke;                       /* '<S33>/Sum' */
  real_T Product2_gp;                  /* '<S33>/Product2' */
  real_T Output_l;                     /* '<S33>/Output' */
  real_T SineWave_ld;                  /* '<S28>/Sine Wave' */
  real_T Sign_ip;                      /* '<S28>/Sign' */
  real_T MultiportSwitch_k;            /* '<S28>/Multiport Switch' */
  real_T Gain_nh;                      /* '<S28>/Gain' */
  real_T rpc_l;                        /* '<S8>/Bus Selector' */
  real_T debug_d;                      /* '<S129>/debug_' */
  real_T mode_g;                       /* '<S126>/Bus Selector' */
  real_T motor_iq_m;                   /* '<S126>/Bus Selector1' */
  real_T motor_id_k;                   /* '<S126>/Bus Selector1' */
  real_T motor_vq_d;                   /* '<S126>/Bus Selector2' */
  real_T motor_vd_m;                   /* '<S126>/Bus Selector2' */
  real_T analog_diff_i;                /* '<S126>/Bus Selector3' */
  real_T analog_diff_dot_ky;           /* '<S126>/Bus Selector3' */
  real_T quadrature_1_angle_c;         /* '<S126>/Bus Selector4' */
  real_T quadrature_1_angle_dot_d;     /* '<S126>/Bus Selector4' */
  real_T motor_torque_hy;              /* '<S126>/Bus Selector5' */
  real_T VectorConcatenate_e[76];      /* '<S162>/Vector Concatenate' */
  real_T DataTypeConversion_i[10];     /* '<S146>/Data Type Conversion' */
  real_T analog_limit_k;               /* '<S146>/analog_limit' */
  real_T ethercat_l;                   /* '<S146>/ethercat' */
  real_T external_panic_l;             /* '<S146>/external_panic' */
  real_T motor_encoder_h;              /* '<S146>/motor encoder' */
  real_T over_current_p;               /* '<S146>/over_current' */
  real_T over_voltage_o;               /* '<S146>/over_voltage' */
  real_T quadrature_1_high_p;          /* '<S146>/quadrature_1_high' */
  real_T quadrature_1_low_c;           /* '<S146>/quadrature_1_low' */
  real_T quadrature_2_limit_a;         /* '<S146>/quadrature_2_limit' */
  real_T under_voltage_l;              /* '<S146>/under_voltage' */
  real_T mode1;                        /* '<Root>/mode1' */
  real_T motor_iq1;                    /* '<Root>/motor_iq1' */
  real_T motor_torque1;                /* '<Root>/motor_torque1' */
  real32_T marlin_ec_o1;               /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o2;               /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o3;               /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o4;               /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o5;               /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o6;               /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o7;               /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o8;               /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o9;               /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o10;              /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o11;              /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o12;              /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o13;              /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o14;              /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o15;              /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o16;              /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o17;              /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o18;              /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o20;              /* '<S64>/marlin_ec' */
  real32_T marlin_ec_o21;              /* '<S64>/marlin_ec' */
  real32_T rpa_float[7];               /* '<S34>/rpa_float' */
  real32_T DataTypeConversion1[14];    /* '<S72>/Data Type Conversion1' */
  real32_T DataTypeConversion1_o[14];  /* '<S69>/Data Type Conversion1' */
  real32_T DataTypeConversion1_oj[14]; /* '<S73>/Data Type Conversion1' */
  real32_T DataTypeConversion1_b[14];  /* '<S70>/Data Type Conversion1' */
  real32_T DataTypeConversion1_e[14];  /* '<S75>/Data Type Conversion1' */
  real32_T DataTypeConversion1_f[14];  /* '<S68>/Data Type Conversion1' */
  real32_T DataTypeConversion1_fm[14]; /* '<S74>/Data Type Conversion1' */
  real32_T DataTypeConversion1_h[14];  /* '<S71>/Data Type Conversion1' */
  real32_T DataTypeConversion_f[76];   /* '<S97>/Data Type Conversion' */
  real32_T id;                         /* '<S63>/Byte Unpack' */
  real32_T ByteUnpack[5];              /* '<S82>/Byte Unpack' */
  real32_T loada;                      /* '<S82>/load a' */
  real32_T loada1;                     /* '<S82>/load a1' */
  real32_T loada2;                     /* '<S82>/load a2' */
  real32_T loada3;                     /* '<S82>/load a3' */
  real32_T loadb;                      /* '<S82>/load b' */
  real32_T marlin_ec_o1_k;             /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o2_f;             /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o3_h;             /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o4_a;             /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o5_l;             /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o6_j;             /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o7_c;             /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o8_j;             /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o9_j;             /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o10_n;            /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o11_d;            /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o12_h;            /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o13_b;            /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o14_e;            /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o15_h;            /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o16_c;            /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o17_a;            /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o18_g;            /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o20_k;            /* '<S129>/marlin_ec' */
  real32_T marlin_ec_o21_m;            /* '<S129>/marlin_ec' */
  real32_T rpa_float_f[7];             /* '<S47>/rpa_float' */
  real32_T DataTypeConversion1_d[14];  /* '<S137>/Data Type Conversion1' */
  real32_T DataTypeConversion1_bm[14]; /* '<S134>/Data Type Conversion1' */
  real32_T DataTypeConversion1_ev[14]; /* '<S138>/Data Type Conversion1' */
  real32_T DataTypeConversion1_a[14];  /* '<S135>/Data Type Conversion1' */
  real32_T DataTypeConversion1_ey[14]; /* '<S140>/Data Type Conversion1' */
  real32_T DataTypeConversion1_fj[14]; /* '<S133>/Data Type Conversion1' */
  real32_T DataTypeConversion1_aj[14]; /* '<S139>/Data Type Conversion1' */
  real32_T DataTypeConversion1_c[14];  /* '<S136>/Data Type Conversion1' */
  real32_T DataTypeConversion_aa[76];  /* '<S162>/Data Type Conversion' */
  real32_T id_p;                       /* '<S128>/Byte Unpack' */
  real32_T ByteUnpack_h[5];            /* '<S147>/Byte Unpack' */
  real32_T loada_c;                    /* '<S147>/load a' */
  real32_T loada1_h;                   /* '<S147>/load a1' */
  real32_T loada2_p;                   /* '<S147>/load a2' */
  real32_T loada3_o;                   /* '<S147>/load a3' */
  real32_T loadb_a;                    /* '<S147>/load b' */
  int32_T type;                        /* '<S63>/Byte Unpack' */
  int32_T current_index;               /* '<S86>/current_index' */
  int32_T type_b;                      /* '<S128>/Byte Unpack' */
  int32_T current_index_c;             /* '<S151>/current_index' */
  uint32_T marlin_ec_o19;              /* '<S64>/marlin_ec' */
  uint32_T faults;                     /* '<S64>/faults_' */
  uint32_T faults_p;                   /* '<S34>/Bus Selector' */
  uint32_T raw_integer;                /* '<S35>/raw_integer' */
  uint32_T Output_o;                   /* '<S65>/Output' */
  uint32_T FixPtSum1;                  /* '<S66>/FixPt Sum1' */
  uint32_T FixPtSwitch;                /* '<S67>/FixPt Switch' */
  uint32_T mode_a;                     /* '<S61>/Multiport Switch' */
  uint32_T DataTypeConversion_g[2];    /* '<S83>/Data Type Conversion' */
  uint32_T DataTypeConversion_h[2];    /* '<S84>/Data Type Conversion' */
  uint32_T DataTypeConversion_ee[2];   /* '<S85>/Data Type Conversion' */
  uint32_T marlin_ec_o19_n;            /* '<S129>/marlin_ec' */
  uint32_T faults_k;                   /* '<S129>/faults_' */
  uint32_T faults_o;                   /* '<S47>/Bus Selector' */
  uint32_T raw_integer_l;              /* '<S48>/raw_integer' */
  uint32_T Output_i4;                  /* '<S130>/Output' */
  uint32_T FixPtSum1_e;                /* '<S131>/FixPt Sum1' */
  uint32_T FixPtSwitch_b;              /* '<S132>/FixPt Switch' */
  uint32_T mode_e;                     /* '<S126>/Multiport Switch' */
  uint32_T DataTypeConversion_k[2];    /* '<S148>/Data Type Conversion' */
  uint32_T DataTypeConversion_k0[2];   /* '<S149>/Data Type Conversion' */
  uint32_T DataTypeConversion_iv[2];   /* '<S150>/Data Type Conversion' */
  uint32_T DomainState_o1;             /* '<Root>/Domain State' */
  uint32_T MasterState_o1;             /* '<Root>/Master State' */
  uint16_T ByteUnpack_e[10];           /* '<S81>/Byte Unpack' */
  uint16_T ByteUnpack_eb[10];          /* '<S146>/Byte Unpack' */
  uint8_T marlin_ec_o22[28];           /* '<S64>/marlin_ec' */
  uint8_T rpa_packet[28];              /* '<S64>/rpa_packet_' */
  uint8_T rpa_packet_k[28];            /* '<S34>/Bus Selector' */
  uint8_T command[56];                 /* '<S72>/Byte Pack3' */
  uint8_T command_j[56];               /* '<S69>/Byte Pack3' */
  uint8_T command_k[56];               /* '<S73>/Byte Pack3' */
  uint8_T command_l[56];               /* '<S70>/Byte Pack2' */
  uint8_T command_f[56];               /* '<S75>/Byte Pack2' */
  uint8_T command_g[56];               /* '<S68>/Byte Pack2' */
  uint8_T command_gi[56];              /* '<S74>/Byte Pack2' */
  uint8_T command_a[56];               /* '<S71>/Byte Pack2' */
  uint8_T command_e[56];               /* '<S61>/Multiport Switch' */
  uint8_T data[20];                    /* '<S63>/Byte Unpack' */
  uint8_T data_m[20];                  /* '<S96>/Byte Pack' */
  uint8_T data_e[20];                  /* '<S83>/Byte Pack' */
  uint8_T data_b[20];                  /* '<S84>/Byte Pack' */
  uint8_T data_n[20];                  /* '<S85>/Byte Pack' */
  uint8_T rpc_i[28];                   /* '<S63>/Byte Pack' */
  uint8_T marlin_ec_o22_a[28];         /* '<S129>/marlin_ec' */
  uint8_T rpa_packet_b[28];            /* '<S129>/rpa_packet_' */
  uint8_T rpa_packet_g[28];            /* '<S47>/Bus Selector' */
  uint8_T command_li[56];              /* '<S137>/Byte Pack3' */
  uint8_T command_n[56];               /* '<S134>/Byte Pack3' */
  uint8_T command_o[56];               /* '<S138>/Byte Pack3' */
  uint8_T command_i[56];               /* '<S135>/Byte Pack2' */
  uint8_T command_h[56];               /* '<S140>/Byte Pack2' */
  uint8_T command_hl[56];              /* '<S133>/Byte Pack2' */
  uint8_T command_gh[56];              /* '<S139>/Byte Pack2' */
  uint8_T command_jo[56];              /* '<S136>/Byte Pack2' */
  uint8_T command_c[56];               /* '<S126>/Multiport Switch' */
  uint8_T data_a[20];                  /* '<S128>/Byte Unpack' */
  uint8_T data_e5[20];                 /* '<S161>/Byte Pack' */
  uint8_T data_o[20];                  /* '<S148>/Byte Pack' */
  uint8_T data_bl[20];                 /* '<S149>/Byte Pack' */
  uint8_T data_mh[20];                 /* '<S150>/Byte Pack' */
  uint8_T rpc_n[28];                   /* '<S128>/Byte Pack' */
  uint8_T DomainState_o2;              /* '<Root>/Domain State' */
  uint8_T MasterState_o2;              /* '<Root>/Master State' */
  boolean_T analog;                    /* '<S35>/analog_' */
  boolean_T communication_fault;       /* '<S35>/communication_fault_' */
  boolean_T current;                   /* '<S35>/current_' */
  boolean_T ethercat_m;                /* '<S35>/ethercat_' */
  boolean_T external_panic_e;          /* '<S35>/external_panic_' */
  boolean_T hard_fault;                /* '<S35>/hard_fault_' */
  boolean_T quadrature;                /* '<S35>/quadrature_' */
  boolean_T soft_fault;                /* '<S35>/soft_fault_' */
  boolean_T ssi_i;                     /* '<S35>/ssi_' */
  boolean_T temperature;               /* '<S35>/temperature_' */
  boolean_T voltage;                   /* '<S35>/voltage_' */
  boolean_T Compare;                   /* '<S92>/Compare' */
  boolean_T Compare_a;                 /* '<S94>/Compare' */
  boolean_T analog_i;                  /* '<S48>/analog_' */
  boolean_T communication_fault_o;     /* '<S48>/communication_fault_' */
  boolean_T current_h;                 /* '<S48>/current_' */
  boolean_T ethercat_e;                /* '<S48>/ethercat_' */
  boolean_T external_panic_o;          /* '<S48>/external_panic_' */
  boolean_T hard_fault_e;              /* '<S48>/hard_fault_' */
  boolean_T quadrature_l;              /* '<S48>/quadrature_' */
  boolean_T soft_fault_a;              /* '<S48>/soft_fault_' */
  boolean_T ssi_m;                     /* '<S48>/ssi_' */
  boolean_T temperature_b;             /* '<S48>/temperature_' */
  boolean_T voltage_f;                 /* '<S48>/voltage_' */
  boolean_T Compare_b;                 /* '<S157>/Compare' */
  boolean_T Compare_aw;                /* '<S159>/Compare' */
  boolean_T MasterState_o3;            /* '<Root>/Master State' */
  uint8_T ExtractDesiredBits;          /* '<S46>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_c;        /* '<S59>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_n;        /* '<S36>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly;           /* '<S36>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_c;         /* '<S37>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_j;         /* '<S38>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_b;         /* '<S39>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_jt;        /* '<S40>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_h;         /* '<S41>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_m;         /* '<S42>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_i;         /* '<S43>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_mj;        /* '<S44>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_jz;        /* '<S45>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_d;         /* '<S46>/Modify Scaling Only' */
  uint8_T ExtractDesiredBits_p;        /* '<S49>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_je;        /* '<S49>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_o;         /* '<S50>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_p;         /* '<S51>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_oc;        /* '<S52>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_l;         /* '<S53>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_e;         /* '<S54>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_f;         /* '<S55>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_cr;        /* '<S56>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_n;         /* '<S57>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_k;         /* '<S58>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_pi;        /* '<S59>/Modify Scaling Only' */
  uint8_T ExtractDesiredBits_pr;       /* '<S37>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_f;        /* '<S50>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_k;        /* '<S39>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_f2;       /* '<S52>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_l;        /* '<S40>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_j;        /* '<S53>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_d;        /* '<S41>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_lr;       /* '<S54>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_h;        /* '<S42>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_o;        /* '<S55>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_ng;       /* '<S43>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_g;        /* '<S56>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_fo;       /* '<S44>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_kb;       /* '<S57>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_cg;       /* '<S45>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_i;        /* '<S58>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_dw;       /* '<S38>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_i1;       /* '<S51>/Extract Desired Bits' */
  B_FilteredDerivative_drum_ben_T FilteredDerivative_p;/* '<S10>/Filtered Derivative' */
  B_FilteredDerivative_drum_ben_T FilteredDerivative_c;/* '<S9>/Filtered Derivative' */
  B_selectrpc_drum_bench_workin_T sf_selectrpc_j;/* '<S128>/select rpc' */
  B_MATLABFunction1_drum_bench__T sf_MATLABFunction1_a;/* '<S161>/MATLAB Function1' */
  B_EnabledSubsystem_drum_bench_T EnabledSubsystem_k;/* '<S147>/Enabled Subsystem' */
  B_enabledsubsystem_drum_bench_T enabledsubsystem_l;/* '<S146>/enabled subsystem' */
  B_selectrpc_drum_bench_workin_T sf_selectrpc;/* '<S63>/select rpc' */
  B_MATLABFunction1_drum_bench__T sf_MATLABFunction1;/* '<S96>/MATLAB Function1' */
  B_EnabledSubsystem_drum_bench_T EnabledSubsystem;/* '<S82>/Enabled Subsystem' */
  B_enabledsubsystem_drum_bench_T enabledsubsystem;/* '<S81>/enabled subsystem' */
  B_FilteredDerivative_drum_ben_T FilteredDerivative_n;/* '<S2>/Filtered Derivative' */
  B_FilteredDerivative_drum_ben_T FilteredDerivative;/* '<S1>/Filtered Derivative' */
} B_drum_bench_working_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T Delay_DSTATE[2];              /* '<S62>/Delay' */
  real_T Delay_DSTATE_n[2];            /* '<S127>/Delay' */
  real_T PrevY;                        /* '<S17>/Rate Limiter' */
  real_T PrevY_a;                      /* '<S18>/Rate Limiter' */
  real_T PrevY_h;                      /* '<S14>/Rate Limiter' */
  real_T PrevY_d;                      /* '<S15>/Rate Limiter' */
  real_T PrevY_dx;                     /* '<S16>/Rate Limiter' */
  real_T PrevY_m;                      /* '<S27>/Rate Limiter' */
  real_T PrevY_b;                      /* '<S28>/Rate Limiter' */
  real_T PrevY_f;                      /* '<S24>/Rate Limiter' */
  real_T PrevY_d5;                     /* '<S25>/Rate Limiter' */
  real_T PrevY_k;                      /* '<S26>/Rate Limiter' */
  struct {
    void *DomainPtr;
  } DomainState_PWORK;                 /* '<Root>/Domain State' */

  struct {
    void *MasterPtr;
  } MasterState_PWORK;                 /* '<Root>/Master State' */

  uint32_T Output_DSTATE;              /* '<S65>/Output' */
  uint32_T Output_DSTATE_h;            /* '<S130>/Output' */
  DW_FilteredDerivative_drum_be_T FilteredDerivative_p;/* '<S10>/Filtered Derivative' */
  DW_FilteredDerivative_drum_be_T FilteredDerivative_c;/* '<S9>/Filtered Derivative' */
  DW_MATLABFunction1_drum_bench_T sf_MATLABFunction1_a;/* '<S161>/MATLAB Function1' */
  DW_MATLABFunction1_drum_bench_T sf_MATLABFunction1;/* '<S96>/MATLAB Function1' */
  DW_FilteredDerivative_drum_be_T FilteredDerivative_n;/* '<S2>/Filtered Derivative' */
  DW_FilteredDerivative_drum_be_T FilteredDerivative;/* '<S1>/Filtered Derivative' */
} DW_drum_bench_working_T;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState TriggeredSubsystem_Trig_ZCE;/* '<S11>/Triggered Subsystem' */
} PrevZCX_drum_bench_working_T;

/* Invariant block signals (auto storage) */
typedef struct {
  const uint8_T data[20];              /* '<S76>/Byte Pack' */
  const uint8_T data_p[20];            /* '<S77>/Byte Pack' */
  const uint8_T data_f[20];            /* '<S78>/Byte Pack' */
  const uint8_T data_c[20];            /* '<S79>/Byte Pack' */
  const uint8_T data_h[20];            /* '<S80>/Byte Pack' */
  const uint8_T data_e[20];            /* '<S88>/Byte Pack' */
  const uint8_T data_d[20];            /* '<S81>/Byte Pack' */
  const uint8_T data_cu[20];           /* '<S91>/Byte Pack' */
  const uint8_T data_o[20];            /* '<S90>/Byte Pack' */
  const uint8_T data_cz[20];           /* '<S82>/Byte Pack' */
  const uint8_T data_ef[20];           /* '<S141>/Byte Pack' */
  const uint8_T data_c4[20];           /* '<S142>/Byte Pack' */
  const uint8_T data_g[20];            /* '<S143>/Byte Pack' */
  const uint8_T data_b[20];            /* '<S144>/Byte Pack' */
  const uint8_T data_fw[20];           /* '<S145>/Byte Pack' */
  const uint8_T data_ec[20];           /* '<S153>/Byte Pack' */
  const uint8_T data_og[20];           /* '<S146>/Byte Pack' */
  const uint8_T data_bu[20];           /* '<S156>/Byte Pack' */
  const uint8_T data_ci[20];           /* '<S155>/Byte Pack' */
  const uint8_T data_e0[20];           /* '<S147>/Byte Pack' */
} ConstB_drum_bench_working_T;

/* Constant parameters (auto storage) */
typedef struct {
  /* Pooled Parameter (Expression: zeros(14,1))
   * Referenced by:
   *   '<S69>/filler'
   *   '<S72>/filler'
   *   '<S134>/filler'
   *   '<S137>/filler'
   */
  real_T pooled16[14];

  /* Pooled Parameter (Expression: zeros(13,1))
   * Referenced by:
   *   '<S71>/filler'
   *   '<S73>/filler'
   *   '<S136>/filler'
   *   '<S138>/filler'
   */
  real_T pooled17[13];

  /* Pooled Parameter (Expression: zeros(12,1))
   * Referenced by:
   *   '<S68>/filler'
   *   '<S70>/filler'
   *   '<S74>/filler'
   *   '<S75>/filler'
   *   '<S133>/filler'
   *   '<S135>/filler'
   *   '<S139>/filler'
   *   '<S140>/filler'
   */
  real_T pooled18[12];

  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<S72>/mode'
   *   '<S76>/type_param'
   *   '<S78>/type_param'
   *   '<S79>/type_param'
   *   '<S81>/type_param'
   *   '<S91>/type_param'
   *   '<S137>/mode'
   *   '<S141>/type_param'
   *   '<S143>/type_param'
   *   '<S144>/type_param'
   *   '<S146>/type_param'
   *   '<S156>/type_param'
   *   '<S65>/Output'
   *   '<S96>/Constant1'
   *   '<S130>/Output'
   *   '<S161>/Constant1'
   *   '<S67>/Constant'
   *   '<S132>/Constant'
   */
  uint32_T pooled30;

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S96>/Constant'
   *   '<S161>/Constant'
   */
  uint32_T pooled39[2];

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S83>/type_param1'
   *   '<S84>/type_param1'
   *   '<S85>/type_param1'
   *   '<S148>/type_param1'
   *   '<S149>/type_param1'
   *   '<S150>/type_param1'
   */
  uint32_T pooled40[3];

  /* Pooled Parameter (Expression: uint8(zeros(20,1)))
   * Referenced by:
   *   '<S93>/Out1'
   *   '<S95>/Out1'
   *   '<S158>/Out1'
   *   '<S160>/Out1'
   */
  uint8_T pooled41[20];

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S89>/Constant'
   *   '<S154>/Constant'
   */
  uint8_T pooled42[20];
} ConstP_drum_bench_working_T;

/* Backward compatible GRT Identifiers */
#define rtB                            drum_bench_working_B
#define BlockIO                        B_drum_bench_working_T
#define rtP                            drum_bench_working_P
#define Parameters                     P_drum_bench_working_T
#define rtDWork                        drum_bench_working_DW
#define D_Work                         DW_drum_bench_working_T
#define tConstBlockIOType              ConstB_drum_bench_working_T
#define rtC                            drum_bench_working_ConstB
#define ConstParam                     ConstP_drum_bench_working_T
#define rtcP                           drum_bench_working_ConstP
#define rtPrevZCSigState               drum_bench_working_PrevZCX
#define PrevZCSigStates                PrevZCX_drum_bench_working_T

/* Parameters (auto storage) */
struct P_drum_bench_working_T_ {
  struct_Kzi606PJBfEKmP4ngorXDD config;/* Variable: config
                                        * Referenced by:
                                        *   '<Root>/mode select'
                                        *   '<Root>/mode select1'
                                        *   '<S1>/Filtered Derivative'
                                        *   '<S1>/kd'
                                        *   '<S1>/kp'
                                        *   '<S2>/Filtered Derivative'
                                        *   '<S2>/kd'
                                        *   '<S2>/kp'
                                        *   '<S3>/analog_diff_dot'
                                        *   '<S3>/mode'
                                        *   '<S3>/motor_id'
                                        *   '<S3>/motor_torque'
                                        *   '<S3>/motor_vd'
                                        *   '<S3>/quadrature_1_angle_dot'
                                        *   '<S3>/quadrature_2_angle_dot'
                                        *   '<S3>/rpc_command'
                                        *   '<S4>/analog_diff_dot'
                                        *   '<S4>/mode'
                                        *   '<S4>/motor_id'
                                        *   '<S4>/motor_torque'
                                        *   '<S4>/motor_vd'
                                        *   '<S4>/quadrature_1_angle_dot'
                                        *   '<S4>/quadrature_2_angle_dot'
                                        *   '<S4>/rpc_command'
                                        *   '<S9>/Filtered Derivative'
                                        *   '<S9>/bias'
                                        *   '<S9>/ka'
                                        *   '<S9>/kd'
                                        *   '<S10>/Filtered Derivative'
                                        *   '<S10>/bias'
                                        *   '<S10>/ka'
                                        *   '<S10>/kd'
                                        *   '<S11>/Constant'
                                        *   '<S14>/Constant'
                                        *   '<S14>/select input'
                                        *   '<S14>/Gain'
                                        *   '<S14>/Sine Wave'
                                        *   '<S15>/Constant'
                                        *   '<S15>/select input'
                                        *   '<S15>/Gain'
                                        *   '<S15>/Sine Wave'
                                        *   '<S16>/Constant'
                                        *   '<S16>/select input'
                                        *   '<S16>/Gain'
                                        *   '<S16>/Sine Wave'
                                        *   '<S17>/Constant'
                                        *   '<S17>/select input'
                                        *   '<S17>/Gain'
                                        *   '<S17>/Sine Wave'
                                        *   '<S18>/Constant'
                                        *   '<S18>/select input'
                                        *   '<S18>/Gain'
                                        *   '<S18>/Sine Wave'
                                        *   '<S24>/Constant'
                                        *   '<S24>/select input'
                                        *   '<S24>/Gain'
                                        *   '<S24>/Sine Wave'
                                        *   '<S25>/Constant'
                                        *   '<S25>/select input'
                                        *   '<S25>/Gain'
                                        *   '<S25>/Sine Wave'
                                        *   '<S26>/Constant'
                                        *   '<S26>/select input'
                                        *   '<S26>/Gain'
                                        *   '<S26>/Sine Wave'
                                        *   '<S27>/Constant'
                                        *   '<S27>/select input'
                                        *   '<S27>/Gain'
                                        *   '<S27>/Sine Wave'
                                        *   '<S28>/Constant'
                                        *   '<S28>/select input'
                                        *   '<S28>/Gain'
                                        *   '<S28>/Sine Wave'
                                        *   '<S61>/lock_current'
                                        *   '<S62>/eA1'
                                        *   '<S62>/eA2'
                                        *   '<S62>/resistance'
                                        *   '<S126>/lock_current'
                                        *   '<S127>/eA1'
                                        *   '<S127>/eA2'
                                        *   '<S127>/resistance'
                                        *   '<S19>/deltaFreq'
                                        *   '<S20>/deltaFreq'
                                        *   '<S21>/deltaFreq'
                                        *   '<S22>/deltaFreq'
                                        *   '<S23>/deltaFreq'
                                        *   '<S29>/deltaFreq'
                                        *   '<S30>/deltaFreq'
                                        *   '<S31>/deltaFreq'
                                        *   '<S32>/deltaFreq'
                                        *   '<S33>/deltaFreq'
                                        *   '<S64>/marlin_ec'
                                        *   '<S83>/value1'
                                        *   '<S84>/value1'
                                        *   '<S85>/value1'
                                        *   '<S129>/marlin_ec'
                                        *   '<S148>/value1'
                                        *   '<S149>/value1'
                                        *   '<S150>/value1'
                                        *   '<S99>/id'
                                        *   '<S99>/id1'
                                        *   '<S99>/id2'
                                        *   '<S99>/id3'
                                        *   '<S99>/id4'
                                        *   '<S99>/id5'
                                        *   '<S103>/id'
                                        *   '<S103>/id1'
                                        *   '<S104>/id'
                                        *   '<S105>/id'
                                        *   '<S105>/id1'
                                        *   '<S105>/id2'
                                        *   '<S105>/id3'
                                        *   '<S105>/id4'
                                        *   '<S108>/id'
                                        *   '<S108>/id1'
                                        *   '<S108>/id2'
                                        *   '<S108>/id3'
                                        *   '<S108>/id4'
                                        *   '<S108>/id5'
                                        *   '<S108>/id6'
                                        *   '<S108>/id7'
                                        *   '<S109>/id'
                                        *   '<S110>/id'
                                        *   '<S164>/id'
                                        *   '<S164>/id1'
                                        *   '<S164>/id2'
                                        *   '<S164>/id3'
                                        *   '<S164>/id4'
                                        *   '<S164>/id5'
                                        *   '<S168>/id'
                                        *   '<S168>/id1'
                                        *   '<S169>/id'
                                        *   '<S170>/id'
                                        *   '<S170>/id1'
                                        *   '<S170>/id2'
                                        *   '<S170>/id3'
                                        *   '<S170>/id4'
                                        *   '<S173>/id'
                                        *   '<S173>/id1'
                                        *   '<S173>/id2'
                                        *   '<S173>/id3'
                                        *   '<S173>/id4'
                                        *   '<S173>/id5'
                                        *   '<S173>/id6'
                                        *   '<S173>/id7'
                                        *   '<S174>/id'
                                        *   '<S175>/id'
                                        *   '<S112>/id'
                                        *   '<S113>/id'
                                        *   '<S114>/id'
                                        *   '<S114>/id1'
                                        *   '<S114>/id3'
                                        *   '<S114>/id4'
                                        *   '<S114>/id5'
                                        *   '<S114>/id6'
                                        *   '<S115>/id'
                                        *   '<S115>/id1'
                                        *   '<S115>/id3'
                                        *   '<S115>/id4'
                                        *   '<S115>/id5'
                                        *   '<S115>/id6'
                                        *   '<S116>/id'
                                        *   '<S116>/id1'
                                        *   '<S116>/id3'
                                        *   '<S116>/id4'
                                        *   '<S116>/id5'
                                        *   '<S116>/id6'
                                        *   '<S117>/id'
                                        *   '<S117>/id1'
                                        *   '<S117>/id2'
                                        *   '<S117>/id3'
                                        *   '<S118>/id'
                                        *   '<S118>/id1'
                                        *   '<S118>/id2'
                                        *   '<S118>/id3'
                                        *   '<S119>/id'
                                        *   '<S120>/id'
                                        *   '<S121>/id'
                                        *   '<S121>/id1'
                                        *   '<S121>/id2'
                                        *   '<S121>/id3'
                                        *   '<S122>/id'
                                        *   '<S122>/id1'
                                        *   '<S122>/id3'
                                        *   '<S122>/id4'
                                        *   '<S122>/id5'
                                        *   '<S122>/id6'
                                        *   '<S123>/id'
                                        *   '<S123>/id1'
                                        *   '<S123>/id3'
                                        *   '<S123>/id4'
                                        *   '<S123>/id5'
                                        *   '<S123>/id6'
                                        *   '<S124>/id'
                                        *   '<S124>/id1'
                                        *   '<S124>/id3'
                                        *   '<S124>/id4'
                                        *   '<S124>/id5'
                                        *   '<S124>/id6'
                                        *   '<S177>/id'
                                        *   '<S178>/id'
                                        *   '<S179>/id'
                                        *   '<S179>/id1'
                                        *   '<S179>/id3'
                                        *   '<S179>/id4'
                                        *   '<S179>/id5'
                                        *   '<S179>/id6'
                                        *   '<S180>/id'
                                        *   '<S180>/id1'
                                        *   '<S180>/id3'
                                        *   '<S180>/id4'
                                        *   '<S180>/id5'
                                        *   '<S180>/id6'
                                        *   '<S181>/id'
                                        *   '<S181>/id1'
                                        *   '<S181>/id3'
                                        *   '<S181>/id4'
                                        *   '<S181>/id5'
                                        *   '<S181>/id6'
                                        *   '<S182>/id'
                                        *   '<S182>/id1'
                                        *   '<S182>/id2'
                                        *   '<S182>/id3'
                                        *   '<S183>/id'
                                        *   '<S183>/id1'
                                        *   '<S183>/id2'
                                        *   '<S183>/id3'
                                        *   '<S184>/id'
                                        *   '<S185>/id'
                                        *   '<S186>/id'
                                        *   '<S186>/id1'
                                        *   '<S186>/id2'
                                        *   '<S186>/id3'
                                        *   '<S187>/id'
                                        *   '<S187>/id1'
                                        *   '<S187>/id3'
                                        *   '<S187>/id4'
                                        *   '<S187>/id5'
                                        *   '<S187>/id6'
                                        *   '<S188>/id'
                                        *   '<S188>/id1'
                                        *   '<S188>/id3'
                                        *   '<S188>/id4'
                                        *   '<S188>/id5'
                                        *   '<S188>/id6'
                                        *   '<S189>/id'
                                        *   '<S189>/id1'
                                        *   '<S189>/id3'
                                        *   '<S189>/id4'
                                        *   '<S189>/id5'
                                        *   '<S189>/id6'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_drum_bench_working_T {
  const char_T *path;
  const char_T *modelName;
  struct SimStruct_tag * *childSfunctions;
  const char_T *errorStatus;
  SS_SimMode simMode;
  RTWExtModeInfo *extModeInfo;
  RTWSolverInfo solverInfo;
  RTWSolverInfo *solverInfoPtr;
  void *sfcnInfo;

  /*
   * DataMapInfo:
   * The following substructure contains information regarding
   * structures generated in the model's C API.
   */
  struct {
    rtwCAPI_ModelMappingInfo mmi;
  } DataMapInfo;

  /*
   * ModelData:
   * The following substructure contains information regarding
   * the data used in the model.
   */
  struct {
    void *blockIO;
    const void *constBlockIO;
    void *defaultParam;
    ZCSigState *prevZCSigState;
    real_T *contStates;
    real_T *derivs;
    void *zcSignalValues;
    void *inputs;
    void *outputs;
    boolean_T *contStateDisabled;
    boolean_T zCCacheNeedsReset;
    boolean_T derivCacheNeedsReset;
    boolean_T blkStateChange;
    void *dwork;
  } ModelData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
    uint32_T options;
    int_T numContStates;
    int_T numU;
    int_T numY;
    int_T numSampTimes;
    int_T numBlocks;
    int_T numBlockIO;
    int_T numBlockPrms;
    int_T numDwork;
    int_T numSFcnPrms;
    int_T numSFcns;
    int_T numIports;
    int_T numOports;
    int_T numNonSampZCs;
    int_T sysDirFeedThru;
    int_T rtwGenSfcn;
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
    void *xpcData;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T stepSize;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    time_T stepSize1;
    time_T tStart;
    time_T tFinal;
    time_T timeOfLastOutput;
    void *timingData;
    real_T *varNextHitTimesList;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *sampleTimes;
    time_T *offsetTimes;
    int_T *sampleTimeTaskIDPtr;
    int_T *sampleHits;
    int_T *perTaskSampleHits;
    time_T *t;
    time_T sampleTimesArray[2];
    time_T offsetTimesArray[2];
    int_T sampleTimeTaskIDArray[2];
    int_T sampleHitArray[2];
    int_T perTaskSampleHitsArray[4];
    time_T tArray[2];
  } Timing;
};

/* Block parameters (auto storage) */
extern P_drum_bench_working_T drum_bench_working_P;

/* Block signals (auto storage) */
extern B_drum_bench_working_T drum_bench_working_B;

/* Block states (auto storage) */
extern DW_drum_bench_working_T drum_bench_working_DW;
extern const ConstB_drum_bench_working_T drum_bench_working_ConstB;/* constant block i/o */

/* Constant parameters (auto storage) */
extern const ConstP_drum_bench_working_T drum_bench_working_ConstP;

/* External data declarations for dependent source files */

/* Zero-crossing (trigger) state */
extern PrevZCX_drum_bench_working_T drum_bench_working_PrevZCX;

/* Function to get C API Model Mapping Static Info */
extern const rtwCAPI_ModelMappingStaticInfo*
  drum_bench_working_GetCAPIStaticMap(void);

/* Real-time Model object */
extern RT_MODEL_drum_bench_working_T *const drum_bench_working_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'drum_bench_working'
 * '<S1>'   : 'drum_bench_working/impedence_controller'
 * '<S2>'   : 'drum_bench_working/impedence_controller1'
 * '<S3>'   : 'drum_bench_working/marlin 1.1 full command generator'
 * '<S4>'   : 'drum_bench_working/marlin 1.1 full command generator1'
 * '<S5>'   : 'drum_bench_working/marlin_1.1_full status'
 * '<S6>'   : 'drum_bench_working/marlin_1.1_full status1'
 * '<S7>'   : 'drum_bench_working/marlin_1.1_full1'
 * '<S8>'   : 'drum_bench_working/marlin_1.1_full2'
 * '<S9>'   : 'drum_bench_working/var_impedence_controller'
 * '<S10>'  : 'drum_bench_working/var_impedence_controller1'
 * '<S11>'  : 'drum_bench_working/write_yaml_params'
 * '<S12>'  : 'drum_bench_working/impedence_controller/Filtered Derivative'
 * '<S13>'  : 'drum_bench_working/impedence_controller1/Filtered Derivative'
 * '<S14>'  : 'drum_bench_working/marlin 1.1 full command generator/analog_diff'
 * '<S15>'  : 'drum_bench_working/marlin 1.1 full command generator/motor_iq'
 * '<S16>'  : 'drum_bench_working/marlin 1.1 full command generator/motor_vq'
 * '<S17>'  : 'drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle'
 * '<S18>'  : 'drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle'
 * '<S19>'  : 'drum_bench_working/marlin 1.1 full command generator/analog_diff/Chirp Signal'
 * '<S20>'  : 'drum_bench_working/marlin 1.1 full command generator/motor_iq/Chirp Signal'
 * '<S21>'  : 'drum_bench_working/marlin 1.1 full command generator/motor_vq/Chirp Signal'
 * '<S22>'  : 'drum_bench_working/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal'
 * '<S23>'  : 'drum_bench_working/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal'
 * '<S24>'  : 'drum_bench_working/marlin 1.1 full command generator1/analog_diff'
 * '<S25>'  : 'drum_bench_working/marlin 1.1 full command generator1/motor_iq'
 * '<S26>'  : 'drum_bench_working/marlin 1.1 full command generator1/motor_vq'
 * '<S27>'  : 'drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle'
 * '<S28>'  : 'drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle'
 * '<S29>'  : 'drum_bench_working/marlin 1.1 full command generator1/analog_diff/Chirp Signal'
 * '<S30>'  : 'drum_bench_working/marlin 1.1 full command generator1/motor_iq/Chirp Signal'
 * '<S31>'  : 'drum_bench_working/marlin 1.1 full command generator1/motor_vq/Chirp Signal'
 * '<S32>'  : 'drum_bench_working/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal'
 * '<S33>'  : 'drum_bench_working/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal'
 * '<S34>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status'
 * '<S35>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults'
 * '<S36>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits'
 * '<S37>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1'
 * '<S38>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10'
 * '<S39>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2'
 * '<S40>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3'
 * '<S41>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4'
 * '<S42>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5'
 * '<S43>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6'
 * '<S44>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7'
 * '<S45>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8'
 * '<S46>'  : 'drum_bench_working/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9'
 * '<S47>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status'
 * '<S48>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults'
 * '<S49>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits'
 * '<S50>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1'
 * '<S51>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10'
 * '<S52>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2'
 * '<S53>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3'
 * '<S54>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4'
 * '<S55>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5'
 * '<S56>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6'
 * '<S57>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7'
 * '<S58>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8'
 * '<S59>'  : 'drum_bench_working/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9'
 * '<S60>'  : 'drum_bench_working/marlin_1.1_full1/marlin_1.1_basic'
 * '<S61>'  : 'drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector'
 * '<S62>'  : 'drum_bench_working/marlin_1.1_full1/motor_temperature_model'
 * '<S63>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator'
 * '<S64>'  : 'drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec'
 * '<S65>'  : 'drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running'
 * '<S66>'  : 'drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running/Increment Real World'
 * '<S67>'  : 'drum_bench_working/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running/Wrap To Zero'
 * '<S68>'  : 'drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/analog_diff'
 * '<S69>'  : 'drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/brake'
 * '<S70>'  : 'drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/current'
 * '<S71>'  : 'drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/motor_torque'
 * '<S72>'  : 'drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/open'
 * '<S73>'  : 'drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/phase lock'
 * '<S74>'  : 'drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/quadrature_1'
 * '<S75>'  : 'drum_bench_working/marlin_1.1_full1/marlin_full_control_mode_selector/voltage'
 * '<S76>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/10 khz logger read'
 * '<S77>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/10 khz logger start'
 * '<S78>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/100 khz logger read'
 * '<S79>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/100 khz logger read2'
 * '<S80>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/100 khz logger start'
 * '<S81>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/check safety'
 * '<S82>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell'
 * '<S83>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug'
 * '<S84>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug1'
 * '<S85>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/get status debug2'
 * '<S86>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1'
 * '<S87>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/select rpc'
 * '<S88>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/store param'
 * '<S89>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/testrpc'
 * '<S90>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/zero both encoder angles'
 * '<S91>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/zero output torque'
 * '<S92>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/Compare To Constant'
 * '<S93>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/check safety/enabled subsystem'
 * '<S94>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/Compare To Constant'
 * '<S95>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/get raw load cell/Enabled Subsystem'
 * '<S96>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/dsp param iterator'
 * '<S97>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1'
 * '<S98>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1'
 * '<S99>'  : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp'
 * '<S100>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1'
 * '<S101>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2'
 * '<S102>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff'
 * '<S103>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/bridge'
 * '<S104>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog'
 * '<S105>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current'
 * '<S106>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle'
 * '<S107>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle'
 * '<S108>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor'
 * '<S109>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1'
 * '<S110>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2'
 * '<S111>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi'
 * '<S112>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz'
 * '<S113>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz'
 * '<S114>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor'
 * '<S115>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor'
 * '<S116>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor'
 * '<S117>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid'
 * '<S118>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid'
 * '<S119>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband'
 * '<S120>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate'
 * '<S121>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid'
 * '<S122>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor'
 * '<S123>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor'
 * '<S124>' : 'drum_bench_working/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor'
 * '<S125>' : 'drum_bench_working/marlin_1.1_full2/marlin_1.1_basic'
 * '<S126>' : 'drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector'
 * '<S127>' : 'drum_bench_working/marlin_1.1_full2/motor_temperature_model'
 * '<S128>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator'
 * '<S129>' : 'drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec'
 * '<S130>' : 'drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running'
 * '<S131>' : 'drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running/Increment Real World'
 * '<S132>' : 'drum_bench_working/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running/Wrap To Zero'
 * '<S133>' : 'drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/analog_diff'
 * '<S134>' : 'drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/brake'
 * '<S135>' : 'drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/current'
 * '<S136>' : 'drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/motor_torque'
 * '<S137>' : 'drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/open'
 * '<S138>' : 'drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/phase lock'
 * '<S139>' : 'drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/quadrature_1'
 * '<S140>' : 'drum_bench_working/marlin_1.1_full2/marlin_full_control_mode_selector/voltage'
 * '<S141>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/10 khz logger read'
 * '<S142>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/10 khz logger start'
 * '<S143>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/100 khz logger read'
 * '<S144>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/100 khz logger read2'
 * '<S145>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/100 khz logger start'
 * '<S146>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/check safety'
 * '<S147>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell'
 * '<S148>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug'
 * '<S149>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug1'
 * '<S150>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/get status debug2'
 * '<S151>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1'
 * '<S152>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/select rpc'
 * '<S153>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/store param'
 * '<S154>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/testrpc'
 * '<S155>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/zero both encoder angles'
 * '<S156>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/zero output torque'
 * '<S157>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/Compare To Constant'
 * '<S158>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/check safety/enabled subsystem'
 * '<S159>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/Compare To Constant'
 * '<S160>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/get raw load cell/Enabled Subsystem'
 * '<S161>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/dsp param iterator'
 * '<S162>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1'
 * '<S163>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1'
 * '<S164>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp'
 * '<S165>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1'
 * '<S166>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2'
 * '<S167>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff'
 * '<S168>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/bridge'
 * '<S169>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog'
 * '<S170>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current'
 * '<S171>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle'
 * '<S172>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle'
 * '<S173>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor'
 * '<S174>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1'
 * '<S175>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2'
 * '<S176>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi'
 * '<S177>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz'
 * '<S178>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz'
 * '<S179>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor'
 * '<S180>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor'
 * '<S181>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor'
 * '<S182>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid'
 * '<S183>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid'
 * '<S184>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband'
 * '<S185>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate'
 * '<S186>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid'
 * '<S187>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor'
 * '<S188>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor'
 * '<S189>' : 'drum_bench_working/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor'
 * '<S190>' : 'drum_bench_working/var_impedence_controller/Filtered Derivative'
 * '<S191>' : 'drum_bench_working/var_impedence_controller1/Filtered Derivative'
 * '<S192>' : 'drum_bench_working/write_yaml_params/Triggered Subsystem'
 */
#endif                                 /* RTW_HEADER_drum_bench_working_h_ */
