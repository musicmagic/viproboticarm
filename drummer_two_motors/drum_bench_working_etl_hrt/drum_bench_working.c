/*
 * drum_bench_working.c
 *
 * Code generation for model "drum_bench_working".
 *
 * Model version              : 1.99
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Tue Jul  8 09:25:28 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "drum_bench_working_capi.h"
#include "drum_bench_working.h"
#include "drum_bench_working_private.h"

/* S-Function Block: <S64>/marlin_ec
 * Mapped Pdos for block M3EC
 */
static struct pdo_map pdo_map_1[] = {
  { 0x7000, 1, 1032, 0, 0, &drum_bench_working_B.mode_a },/* In1[0] */
  { 0x7000, 2, 1008, 0, 0, &drum_bench_working_B.command_e[0] },/* In2[0] */
  { 0x7000, 2, 1008, 0, 1, &drum_bench_working_B.command_e[1] },/* In2[1] */
  { 0x7000, 2, 1008, 0, 2, &drum_bench_working_B.command_e[2] },/* In2[2] */
  { 0x7000, 2, 1008, 0, 3, &drum_bench_working_B.command_e[3] },/* In2[3] */
  { 0x7000, 2, 1008, 0, 4, &drum_bench_working_B.command_e[4] },/* In2[4] */
  { 0x7000, 2, 1008, 0, 5, &drum_bench_working_B.command_e[5] },/* In2[5] */
  { 0x7000, 2, 1008, 0, 6, &drum_bench_working_B.command_e[6] },/* In2[6] */
  { 0x7000, 2, 1008, 0, 7, &drum_bench_working_B.command_e[7] },/* In2[7] */
  { 0x7000, 2, 1008, 0, 8, &drum_bench_working_B.command_e[8] },/* In2[8] */
  { 0x7000, 2, 1008, 0, 9, &drum_bench_working_B.command_e[9] },/* In2[9] */
  { 0x7000, 2, 1008, 0, 10, &drum_bench_working_B.command_e[10] },/* In2[10] */
  { 0x7000, 2, 1008, 0, 11, &drum_bench_working_B.command_e[11] },/* In2[11] */
  { 0x7000, 2, 1008, 0, 12, &drum_bench_working_B.command_e[12] },/* In2[12] */
  { 0x7000, 2, 1008, 0, 13, &drum_bench_working_B.command_e[13] },/* In2[13] */
  { 0x7000, 2, 1008, 0, 14, &drum_bench_working_B.command_e[14] },/* In2[14] */
  { 0x7000, 2, 1008, 0, 15, &drum_bench_working_B.command_e[15] },/* In2[15] */
  { 0x7000, 2, 1008, 0, 16, &drum_bench_working_B.command_e[16] },/* In2[16] */
  { 0x7000, 2, 1008, 0, 17, &drum_bench_working_B.command_e[17] },/* In2[17] */
  { 0x7000, 2, 1008, 0, 18, &drum_bench_working_B.command_e[18] },/* In2[18] */
  { 0x7000, 2, 1008, 0, 19, &drum_bench_working_B.command_e[19] },/* In2[19] */
  { 0x7000, 2, 1008, 0, 20, &drum_bench_working_B.command_e[20] },/* In2[20] */
  { 0x7000, 2, 1008, 0, 21, &drum_bench_working_B.command_e[21] },/* In2[21] */
  { 0x7000, 2, 1008, 0, 22, &drum_bench_working_B.command_e[22] },/* In2[22] */
  { 0x7000, 2, 1008, 0, 23, &drum_bench_working_B.command_e[23] },/* In2[23] */
  { 0x7000, 2, 1008, 0, 24, &drum_bench_working_B.command_e[24] },/* In2[24] */
  { 0x7000, 2, 1008, 0, 25, &drum_bench_working_B.command_e[25] },/* In2[25] */
  { 0x7000, 2, 1008, 0, 26, &drum_bench_working_B.command_e[26] },/* In2[26] */
  { 0x7000, 2, 1008, 0, 27, &drum_bench_working_B.command_e[27] },/* In2[27] */
  { 0x7000, 3, 1008, 0, 0, &drum_bench_working_B.command_e[28] },/* In3[0] */
  { 0x7000, 3, 1008, 0, 1, &drum_bench_working_B.command_e[29] },/* In3[1] */
  { 0x7000, 3, 1008, 0, 2, &drum_bench_working_B.command_e[30] },/* In3[2] */
  { 0x7000, 3, 1008, 0, 3, &drum_bench_working_B.command_e[31] },/* In3[3] */
  { 0x7000, 3, 1008, 0, 4, &drum_bench_working_B.command_e[32] },/* In3[4] */
  { 0x7000, 3, 1008, 0, 5, &drum_bench_working_B.command_e[33] },/* In3[5] */
  { 0x7000, 3, 1008, 0, 6, &drum_bench_working_B.command_e[34] },/* In3[6] */
  { 0x7000, 3, 1008, 0, 7, &drum_bench_working_B.command_e[35] },/* In3[7] */
  { 0x7000, 3, 1008, 0, 8, &drum_bench_working_B.command_e[36] },/* In3[8] */
  { 0x7000, 3, 1008, 0, 9, &drum_bench_working_B.command_e[37] },/* In3[9] */
  { 0x7000, 3, 1008, 0, 10, &drum_bench_working_B.command_e[38] },/* In3[10] */
  { 0x7000, 3, 1008, 0, 11, &drum_bench_working_B.command_e[39] },/* In3[11] */
  { 0x7000, 3, 1008, 0, 12, &drum_bench_working_B.command_e[40] },/* In3[12] */
  { 0x7000, 3, 1008, 0, 13, &drum_bench_working_B.command_e[41] },/* In3[13] */
  { 0x7000, 3, 1008, 0, 14, &drum_bench_working_B.command_e[42] },/* In3[14] */
  { 0x7000, 3, 1008, 0, 15, &drum_bench_working_B.command_e[43] },/* In3[15] */
  { 0x7000, 3, 1008, 0, 16, &drum_bench_working_B.command_e[44] },/* In3[16] */
  { 0x7000, 3, 1008, 0, 17, &drum_bench_working_B.command_e[45] },/* In3[17] */
  { 0x7000, 3, 1008, 0, 18, &drum_bench_working_B.command_e[46] },/* In3[18] */
  { 0x7000, 3, 1008, 0, 19, &drum_bench_working_B.command_e[47] },/* In3[19] */
  { 0x7000, 3, 1008, 0, 20, &drum_bench_working_B.command_e[48] },/* In3[20] */
  { 0x7000, 3, 1008, 0, 21, &drum_bench_working_B.command_e[49] },/* In3[21] */
  { 0x7000, 3, 1008, 0, 22, &drum_bench_working_B.command_e[50] },/* In3[22] */
  { 0x7000, 3, 1008, 0, 23, &drum_bench_working_B.command_e[51] },/* In3[23] */
  { 0x7000, 3, 1008, 0, 24, &drum_bench_working_B.command_e[52] },/* In3[24] */
  { 0x7000, 3, 1008, 0, 25, &drum_bench_working_B.command_e[53] },/* In3[25] */
  { 0x7000, 3, 1008, 0, 26, &drum_bench_working_B.command_e[54] },/* In3[26] */
  { 0x7000, 3, 1008, 0, 27, &drum_bench_working_B.command_e[55] },/* In3[27] */
  { 0x7000, 4, 1032, 0, 0, &drum_bench_working_B.Output_o },/* In4[0] */
  { 0x7000, 5, 1008, 0, 0, &drum_bench_working_B.rpc_i[0] },/* In5[0] */
  { 0x7000, 5, 1008, 0, 1, &drum_bench_working_B.rpc_i[1] },/* In5[1] */
  { 0x7000, 5, 1008, 0, 2, &drum_bench_working_B.rpc_i[2] },/* In5[2] */
  { 0x7000, 5, 1008, 0, 3, &drum_bench_working_B.rpc_i[3] },/* In5[3] */
  { 0x7000, 5, 1008, 0, 4, &drum_bench_working_B.rpc_i[4] },/* In5[4] */
  { 0x7000, 5, 1008, 0, 5, &drum_bench_working_B.rpc_i[5] },/* In5[5] */
  { 0x7000, 5, 1008, 0, 6, &drum_bench_working_B.rpc_i[6] },/* In5[6] */
  { 0x7000, 5, 1008, 0, 7, &drum_bench_working_B.rpc_i[7] },/* In5[7] */
  { 0x7000, 5, 1008, 0, 8, &drum_bench_working_B.rpc_i[8] },/* In5[8] */
  { 0x7000, 5, 1008, 0, 9, &drum_bench_working_B.rpc_i[9] },/* In5[9] */
  { 0x7000, 5, 1008, 0, 10, &drum_bench_working_B.rpc_i[10] },/* In5[10] */
  { 0x7000, 5, 1008, 0, 11, &drum_bench_working_B.rpc_i[11] },/* In5[11] */
  { 0x7000, 5, 1008, 0, 12, &drum_bench_working_B.rpc_i[12] },/* In5[12] */
  { 0x7000, 5, 1008, 0, 13, &drum_bench_working_B.rpc_i[13] },/* In5[13] */
  { 0x7000, 5, 1008, 0, 14, &drum_bench_working_B.rpc_i[14] },/* In5[14] */
  { 0x7000, 5, 1008, 0, 15, &drum_bench_working_B.rpc_i[15] },/* In5[15] */
  { 0x7000, 5, 1008, 0, 16, &drum_bench_working_B.rpc_i[16] },/* In5[16] */
  { 0x7000, 5, 1008, 0, 17, &drum_bench_working_B.rpc_i[17] },/* In5[17] */
  { 0x7000, 5, 1008, 0, 18, &drum_bench_working_B.rpc_i[18] },/* In5[18] */
  { 0x7000, 5, 1008, 0, 19, &drum_bench_working_B.rpc_i[19] },/* In5[19] */
  { 0x7000, 5, 1008, 0, 20, &drum_bench_working_B.rpc_i[20] },/* In5[20] */
  { 0x7000, 5, 1008, 0, 21, &drum_bench_working_B.rpc_i[21] },/* In5[21] */
  { 0x7000, 5, 1008, 0, 22, &drum_bench_working_B.rpc_i[22] },/* In5[22] */
  { 0x7000, 5, 1008, 0, 23, &drum_bench_working_B.rpc_i[23] },/* In5[23] */
  { 0x7000, 5, 1008, 0, 24, &drum_bench_working_B.rpc_i[24] },/* In5[24] */
  { 0x7000, 5, 1008, 0, 25, &drum_bench_working_B.rpc_i[25] },/* In5[25] */
  { 0x7000, 5, 1008, 0, 26, &drum_bench_working_B.rpc_i[26] },/* In5[26] */
  { 0x7000, 5, 1008, 0, 27, &drum_bench_working_B.rpc_i[27] },/* In5[27] */
  { 0x6000, 1, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o1 },/* Out1[0] */
  { 0x6000, 2, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o2 },/* Out2[0] */
  { 0x6000, 3, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o3 },/* Out3[0] */
  { 0x6000, 4, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o4 },/* Out4[0] */
  { 0x6000, 5, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o5 },/* Out5[0] */
  { 0x6000, 6, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o6 },/* Out6[0] */
  { 0x6000, 7, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o7 },/* Out7[0] */
  { 0x6000, 8, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o8 },/* Out8[0] */
  { 0x6000, 9, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o9 },/* Out9[0] */
  { 0x6000, 10, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o10 },/* Out10[0] */
  { 0x6000, 11, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o11 },/* Out11[0] */
  { 0x6000, 12, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o12 },/* Out12[0] */
  { 0x6000, 13, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o13 },/* Out13[0] */
  { 0x6000, 14, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o14 },/* Out14[0] */
  { 0x6000, 15, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o15 },/* Out15[0] */
  { 0x6000, 16, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o16 },/* Out16[0] */
  { 0x6000, 17, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o17 },/* Out17[0] */
  { 0x6000, 18, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o18 },/* Out18[0] */
  { 0x6000, 19, 1032, 0, 0, &drum_bench_working_B.marlin_ec_o19 },/* Out19[0] */
  { 0x6000, 20, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o20 },/* Out20[0] */
  { 0x6000, 21, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o21 },/* Out21[0] */
  { 0x6000, 22, 1008, 0, 0, &drum_bench_working_B.marlin_ec_o22[0] },/* Out22[0] */
  { 0x6000, 22, 1008, 0, 1, &drum_bench_working_B.marlin_ec_o22[1] },/* Out22[1] */
  { 0x6000, 22, 1008, 0, 2, &drum_bench_working_B.marlin_ec_o22[2] },/* Out22[2] */
  { 0x6000, 22, 1008, 0, 3, &drum_bench_working_B.marlin_ec_o22[3] },/* Out22[3] */
  { 0x6000, 22, 1008, 0, 4, &drum_bench_working_B.marlin_ec_o22[4] },/* Out22[4] */
  { 0x6000, 22, 1008, 0, 5, &drum_bench_working_B.marlin_ec_o22[5] },/* Out22[5] */
  { 0x6000, 22, 1008, 0, 6, &drum_bench_working_B.marlin_ec_o22[6] },/* Out22[6] */
  { 0x6000, 22, 1008, 0, 7, &drum_bench_working_B.marlin_ec_o22[7] },/* Out22[7] */
  { 0x6000, 22, 1008, 0, 8, &drum_bench_working_B.marlin_ec_o22[8] },/* Out22[8] */
  { 0x6000, 22, 1008, 0, 9, &drum_bench_working_B.marlin_ec_o22[9] },/* Out22[9] */
  { 0x6000, 22, 1008, 0, 10, &drum_bench_working_B.marlin_ec_o22[10] },/* Out22[10] */
  { 0x6000, 22, 1008, 0, 11, &drum_bench_working_B.marlin_ec_o22[11] },/* Out22[11] */
  { 0x6000, 22, 1008, 0, 12, &drum_bench_working_B.marlin_ec_o22[12] },/* Out22[12] */
  { 0x6000, 22, 1008, 0, 13, &drum_bench_working_B.marlin_ec_o22[13] },/* Out22[13] */
  { 0x6000, 22, 1008, 0, 14, &drum_bench_working_B.marlin_ec_o22[14] },/* Out22[14] */
  { 0x6000, 22, 1008, 0, 15, &drum_bench_working_B.marlin_ec_o22[15] },/* Out22[15] */
  { 0x6000, 22, 1008, 0, 16, &drum_bench_working_B.marlin_ec_o22[16] },/* Out22[16] */
  { 0x6000, 22, 1008, 0, 17, &drum_bench_working_B.marlin_ec_o22[17] },/* Out22[17] */
  { 0x6000, 22, 1008, 0, 18, &drum_bench_working_B.marlin_ec_o22[18] },/* Out22[18] */
  { 0x6000, 22, 1008, 0, 19, &drum_bench_working_B.marlin_ec_o22[19] },/* Out22[19] */
  { 0x6000, 22, 1008, 0, 20, &drum_bench_working_B.marlin_ec_o22[20] },/* Out22[20] */
  { 0x6000, 22, 1008, 0, 21, &drum_bench_working_B.marlin_ec_o22[21] },/* Out22[21] */
  { 0x6000, 22, 1008, 0, 22, &drum_bench_working_B.marlin_ec_o22[22] },/* Out22[22] */
  { 0x6000, 22, 1008, 0, 23, &drum_bench_working_B.marlin_ec_o22[23] },/* Out22[23] */
  { 0x6000, 22, 1008, 0, 24, &drum_bench_working_B.marlin_ec_o22[24] },/* Out22[24] */
  { 0x6000, 22, 1008, 0, 25, &drum_bench_working_B.marlin_ec_o22[25] },/* Out22[25] */
  { 0x6000, 22, 1008, 0, 26, &drum_bench_working_B.marlin_ec_o22[26] },/* Out22[26] */
  { 0x6000, 22, 1008, 0, 27, &drum_bench_working_B.marlin_ec_o22[27] },/* Out22[27] */
};

/* PDO configuration for M3EC
 */
static ec_pdo_entry_info_t pdo_entry_info_518_00000000_0[] = {
  { 0x7000, 1, 32 },                   /* 0 */
  { 0x7000, 2, 224 },                  /* 1 */
  { 0x7000, 3, 224 },                  /* 2 */
  { 0x7000, 4, 32 },                   /* 3 */
  { 0x7000, 5, 224 },                  /* 4 */
  { 0x6000, 1, 32 },                   /* 5 */
  { 0x6000, 2, 32 },                   /* 6 */
  { 0x6000, 3, 32 },                   /* 7 */
  { 0x6000, 4, 32 },                   /* 8 */
  { 0x6000, 5, 32 },                   /* 9 */
  { 0x6000, 6, 32 },                   /* 10 */
  { 0x6000, 7, 32 },                   /* 11 */
  { 0x6000, 8, 32 },                   /* 12 */
  { 0x6000, 9, 32 },                   /* 13 */
  { 0x6000, 10, 32 },                  /* 14 */
  { 0x6000, 11, 32 },                  /* 15 */
  { 0x6000, 12, 32 },                  /* 16 */
  { 0x6000, 13, 32 },                  /* 17 */
  { 0x6000, 14, 32 },                  /* 18 */
  { 0x6000, 15, 32 },                  /* 19 */
  { 0x6000, 16, 32 },                  /* 20 */
  { 0x6000, 17, 32 },                  /* 21 */
  { 0x6000, 18, 32 },                  /* 22 */
  { 0x6000, 19, 32 },                  /* 23 */
  { 0x6000, 20, 32 },                  /* 24 */
  { 0x6000, 21, 32 },                  /* 25 */
  { 0x6000, 22, 224 },                 /* 26 */
};

static ec_pdo_info_t pdo_info_518_00000000_0[] = {
  { 0x1600, 5, &pdo_entry_info_518_00000000_0[0] },/* */
  { 0x1A00, 22, &pdo_entry_info_518_00000000_0[5U] },/* */
};

static ec_sync_info_t sync_manager_518_00000000_0[] = {
  { 0U, EC_DIR_OUTPUT, 1U, &pdo_info_518_00000000_0[0] },/* */
  { 1U, EC_DIR_INPUT, 1U, &pdo_info_518_00000000_0[1U] },/* */
  { (uint8_T)EC_END, },
};

/* S-Function Block: <S64>/marlin_ec
 * Registering EtherCAT block M3EC with Driver
 */
static struct ec_slave ec_slave_1 = {
  NULL,                                /* Linked list */
  0,                                   /* TID */
  0U, 0U, 0U, 0U,                      /* MasterId, DomainId, SlaveAlias, SlavePosition */
  1304U, 0x00000000,                   /* VendorId, ProductCode */
  0, NULL,                             /* SdoConfigCount, SdoVar */
  0, NULL,                             /* SoeConfigCount, SoeVar */
  sync_manager_518_00000000_0,         /* SyncManager Configuration */

  { 0, },                              /* Distributed Clock Configuration */
  86, 49, pdo_map_1                    /* RxPdo and TxPdo Pdo count and Configuration */
};

/* S-Function Block: <S129>/marlin_ec
 * Mapped Pdos for block M3EC
 */
static struct pdo_map pdo_map_2[] = {
  { 0x7000, 1, 1032, 0, 0, &drum_bench_working_B.mode_e },/* In1[0] */
  { 0x7000, 2, 1008, 0, 0, &drum_bench_working_B.command_c[0] },/* In2[0] */
  { 0x7000, 2, 1008, 0, 1, &drum_bench_working_B.command_c[1] },/* In2[1] */
  { 0x7000, 2, 1008, 0, 2, &drum_bench_working_B.command_c[2] },/* In2[2] */
  { 0x7000, 2, 1008, 0, 3, &drum_bench_working_B.command_c[3] },/* In2[3] */
  { 0x7000, 2, 1008, 0, 4, &drum_bench_working_B.command_c[4] },/* In2[4] */
  { 0x7000, 2, 1008, 0, 5, &drum_bench_working_B.command_c[5] },/* In2[5] */
  { 0x7000, 2, 1008, 0, 6, &drum_bench_working_B.command_c[6] },/* In2[6] */
  { 0x7000, 2, 1008, 0, 7, &drum_bench_working_B.command_c[7] },/* In2[7] */
  { 0x7000, 2, 1008, 0, 8, &drum_bench_working_B.command_c[8] },/* In2[8] */
  { 0x7000, 2, 1008, 0, 9, &drum_bench_working_B.command_c[9] },/* In2[9] */
  { 0x7000, 2, 1008, 0, 10, &drum_bench_working_B.command_c[10] },/* In2[10] */
  { 0x7000, 2, 1008, 0, 11, &drum_bench_working_B.command_c[11] },/* In2[11] */
  { 0x7000, 2, 1008, 0, 12, &drum_bench_working_B.command_c[12] },/* In2[12] */
  { 0x7000, 2, 1008, 0, 13, &drum_bench_working_B.command_c[13] },/* In2[13] */
  { 0x7000, 2, 1008, 0, 14, &drum_bench_working_B.command_c[14] },/* In2[14] */
  { 0x7000, 2, 1008, 0, 15, &drum_bench_working_B.command_c[15] },/* In2[15] */
  { 0x7000, 2, 1008, 0, 16, &drum_bench_working_B.command_c[16] },/* In2[16] */
  { 0x7000, 2, 1008, 0, 17, &drum_bench_working_B.command_c[17] },/* In2[17] */
  { 0x7000, 2, 1008, 0, 18, &drum_bench_working_B.command_c[18] },/* In2[18] */
  { 0x7000, 2, 1008, 0, 19, &drum_bench_working_B.command_c[19] },/* In2[19] */
  { 0x7000, 2, 1008, 0, 20, &drum_bench_working_B.command_c[20] },/* In2[20] */
  { 0x7000, 2, 1008, 0, 21, &drum_bench_working_B.command_c[21] },/* In2[21] */
  { 0x7000, 2, 1008, 0, 22, &drum_bench_working_B.command_c[22] },/* In2[22] */
  { 0x7000, 2, 1008, 0, 23, &drum_bench_working_B.command_c[23] },/* In2[23] */
  { 0x7000, 2, 1008, 0, 24, &drum_bench_working_B.command_c[24] },/* In2[24] */
  { 0x7000, 2, 1008, 0, 25, &drum_bench_working_B.command_c[25] },/* In2[25] */
  { 0x7000, 2, 1008, 0, 26, &drum_bench_working_B.command_c[26] },/* In2[26] */
  { 0x7000, 2, 1008, 0, 27, &drum_bench_working_B.command_c[27] },/* In2[27] */
  { 0x7000, 3, 1008, 0, 0, &drum_bench_working_B.command_c[28] },/* In3[0] */
  { 0x7000, 3, 1008, 0, 1, &drum_bench_working_B.command_c[29] },/* In3[1] */
  { 0x7000, 3, 1008, 0, 2, &drum_bench_working_B.command_c[30] },/* In3[2] */
  { 0x7000, 3, 1008, 0, 3, &drum_bench_working_B.command_c[31] },/* In3[3] */
  { 0x7000, 3, 1008, 0, 4, &drum_bench_working_B.command_c[32] },/* In3[4] */
  { 0x7000, 3, 1008, 0, 5, &drum_bench_working_B.command_c[33] },/* In3[5] */
  { 0x7000, 3, 1008, 0, 6, &drum_bench_working_B.command_c[34] },/* In3[6] */
  { 0x7000, 3, 1008, 0, 7, &drum_bench_working_B.command_c[35] },/* In3[7] */
  { 0x7000, 3, 1008, 0, 8, &drum_bench_working_B.command_c[36] },/* In3[8] */
  { 0x7000, 3, 1008, 0, 9, &drum_bench_working_B.command_c[37] },/* In3[9] */
  { 0x7000, 3, 1008, 0, 10, &drum_bench_working_B.command_c[38] },/* In3[10] */
  { 0x7000, 3, 1008, 0, 11, &drum_bench_working_B.command_c[39] },/* In3[11] */
  { 0x7000, 3, 1008, 0, 12, &drum_bench_working_B.command_c[40] },/* In3[12] */
  { 0x7000, 3, 1008, 0, 13, &drum_bench_working_B.command_c[41] },/* In3[13] */
  { 0x7000, 3, 1008, 0, 14, &drum_bench_working_B.command_c[42] },/* In3[14] */
  { 0x7000, 3, 1008, 0, 15, &drum_bench_working_B.command_c[43] },/* In3[15] */
  { 0x7000, 3, 1008, 0, 16, &drum_bench_working_B.command_c[44] },/* In3[16] */
  { 0x7000, 3, 1008, 0, 17, &drum_bench_working_B.command_c[45] },/* In3[17] */
  { 0x7000, 3, 1008, 0, 18, &drum_bench_working_B.command_c[46] },/* In3[18] */
  { 0x7000, 3, 1008, 0, 19, &drum_bench_working_B.command_c[47] },/* In3[19] */
  { 0x7000, 3, 1008, 0, 20, &drum_bench_working_B.command_c[48] },/* In3[20] */
  { 0x7000, 3, 1008, 0, 21, &drum_bench_working_B.command_c[49] },/* In3[21] */
  { 0x7000, 3, 1008, 0, 22, &drum_bench_working_B.command_c[50] },/* In3[22] */
  { 0x7000, 3, 1008, 0, 23, &drum_bench_working_B.command_c[51] },/* In3[23] */
  { 0x7000, 3, 1008, 0, 24, &drum_bench_working_B.command_c[52] },/* In3[24] */
  { 0x7000, 3, 1008, 0, 25, &drum_bench_working_B.command_c[53] },/* In3[25] */
  { 0x7000, 3, 1008, 0, 26, &drum_bench_working_B.command_c[54] },/* In3[26] */
  { 0x7000, 3, 1008, 0, 27, &drum_bench_working_B.command_c[55] },/* In3[27] */
  { 0x7000, 4, 1032, 0, 0, &drum_bench_working_B.Output_i4 },/* In4[0] */
  { 0x7000, 5, 1008, 0, 0, &drum_bench_working_B.rpc_n[0] },/* In5[0] */
  { 0x7000, 5, 1008, 0, 1, &drum_bench_working_B.rpc_n[1] },/* In5[1] */
  { 0x7000, 5, 1008, 0, 2, &drum_bench_working_B.rpc_n[2] },/* In5[2] */
  { 0x7000, 5, 1008, 0, 3, &drum_bench_working_B.rpc_n[3] },/* In5[3] */
  { 0x7000, 5, 1008, 0, 4, &drum_bench_working_B.rpc_n[4] },/* In5[4] */
  { 0x7000, 5, 1008, 0, 5, &drum_bench_working_B.rpc_n[5] },/* In5[5] */
  { 0x7000, 5, 1008, 0, 6, &drum_bench_working_B.rpc_n[6] },/* In5[6] */
  { 0x7000, 5, 1008, 0, 7, &drum_bench_working_B.rpc_n[7] },/* In5[7] */
  { 0x7000, 5, 1008, 0, 8, &drum_bench_working_B.rpc_n[8] },/* In5[8] */
  { 0x7000, 5, 1008, 0, 9, &drum_bench_working_B.rpc_n[9] },/* In5[9] */
  { 0x7000, 5, 1008, 0, 10, &drum_bench_working_B.rpc_n[10] },/* In5[10] */
  { 0x7000, 5, 1008, 0, 11, &drum_bench_working_B.rpc_n[11] },/* In5[11] */
  { 0x7000, 5, 1008, 0, 12, &drum_bench_working_B.rpc_n[12] },/* In5[12] */
  { 0x7000, 5, 1008, 0, 13, &drum_bench_working_B.rpc_n[13] },/* In5[13] */
  { 0x7000, 5, 1008, 0, 14, &drum_bench_working_B.rpc_n[14] },/* In5[14] */
  { 0x7000, 5, 1008, 0, 15, &drum_bench_working_B.rpc_n[15] },/* In5[15] */
  { 0x7000, 5, 1008, 0, 16, &drum_bench_working_B.rpc_n[16] },/* In5[16] */
  { 0x7000, 5, 1008, 0, 17, &drum_bench_working_B.rpc_n[17] },/* In5[17] */
  { 0x7000, 5, 1008, 0, 18, &drum_bench_working_B.rpc_n[18] },/* In5[18] */
  { 0x7000, 5, 1008, 0, 19, &drum_bench_working_B.rpc_n[19] },/* In5[19] */
  { 0x7000, 5, 1008, 0, 20, &drum_bench_working_B.rpc_n[20] },/* In5[20] */
  { 0x7000, 5, 1008, 0, 21, &drum_bench_working_B.rpc_n[21] },/* In5[21] */
  { 0x7000, 5, 1008, 0, 22, &drum_bench_working_B.rpc_n[22] },/* In5[22] */
  { 0x7000, 5, 1008, 0, 23, &drum_bench_working_B.rpc_n[23] },/* In5[23] */
  { 0x7000, 5, 1008, 0, 24, &drum_bench_working_B.rpc_n[24] },/* In5[24] */
  { 0x7000, 5, 1008, 0, 25, &drum_bench_working_B.rpc_n[25] },/* In5[25] */
  { 0x7000, 5, 1008, 0, 26, &drum_bench_working_B.rpc_n[26] },/* In5[26] */
  { 0x7000, 5, 1008, 0, 27, &drum_bench_working_B.rpc_n[27] },/* In5[27] */
  { 0x6000, 1, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o1_k },/* Out1[0] */
  { 0x6000, 2, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o2_f },/* Out2[0] */
  { 0x6000, 3, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o3_h },/* Out3[0] */
  { 0x6000, 4, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o4_a },/* Out4[0] */
  { 0x6000, 5, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o5_l },/* Out5[0] */
  { 0x6000, 6, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o6_j },/* Out6[0] */
  { 0x6000, 7, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o7_c },/* Out7[0] */
  { 0x6000, 8, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o8_j },/* Out8[0] */
  { 0x6000, 9, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o9_j },/* Out9[0] */
  { 0x6000, 10, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o10_n },/* Out10[0] */
  { 0x6000, 11, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o11_d },/* Out11[0] */
  { 0x6000, 12, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o12_h },/* Out12[0] */
  { 0x6000, 13, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o13_b },/* Out13[0] */
  { 0x6000, 14, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o14_e },/* Out14[0] */
  { 0x6000, 15, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o15_h },/* Out15[0] */
  { 0x6000, 16, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o16_c },/* Out16[0] */
  { 0x6000, 17, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o17_a },/* Out17[0] */
  { 0x6000, 18, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o18_g },/* Out18[0] */
  { 0x6000, 19, 1032, 0, 0, &drum_bench_working_B.marlin_ec_o19_n },/* Out19[0] */
  { 0x6000, 20, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o20_k },/* Out20[0] */
  { 0x6000, 21, 3032, 0, 0, &drum_bench_working_B.marlin_ec_o21_m },/* Out21[0] */
  { 0x6000, 22, 1008, 0, 0, &drum_bench_working_B.marlin_ec_o22_a[0] },/* Out22[0] */
  { 0x6000, 22, 1008, 0, 1, &drum_bench_working_B.marlin_ec_o22_a[1] },/* Out22[1] */
  { 0x6000, 22, 1008, 0, 2, &drum_bench_working_B.marlin_ec_o22_a[2] },/* Out22[2] */
  { 0x6000, 22, 1008, 0, 3, &drum_bench_working_B.marlin_ec_o22_a[3] },/* Out22[3] */
  { 0x6000, 22, 1008, 0, 4, &drum_bench_working_B.marlin_ec_o22_a[4] },/* Out22[4] */
  { 0x6000, 22, 1008, 0, 5, &drum_bench_working_B.marlin_ec_o22_a[5] },/* Out22[5] */
  { 0x6000, 22, 1008, 0, 6, &drum_bench_working_B.marlin_ec_o22_a[6] },/* Out22[6] */
  { 0x6000, 22, 1008, 0, 7, &drum_bench_working_B.marlin_ec_o22_a[7] },/* Out22[7] */
  { 0x6000, 22, 1008, 0, 8, &drum_bench_working_B.marlin_ec_o22_a[8] },/* Out22[8] */
  { 0x6000, 22, 1008, 0, 9, &drum_bench_working_B.marlin_ec_o22_a[9] },/* Out22[9] */
  { 0x6000, 22, 1008, 0, 10, &drum_bench_working_B.marlin_ec_o22_a[10] },/* Out22[10] */
  { 0x6000, 22, 1008, 0, 11, &drum_bench_working_B.marlin_ec_o22_a[11] },/* Out22[11] */
  { 0x6000, 22, 1008, 0, 12, &drum_bench_working_B.marlin_ec_o22_a[12] },/* Out22[12] */
  { 0x6000, 22, 1008, 0, 13, &drum_bench_working_B.marlin_ec_o22_a[13] },/* Out22[13] */
  { 0x6000, 22, 1008, 0, 14, &drum_bench_working_B.marlin_ec_o22_a[14] },/* Out22[14] */
  { 0x6000, 22, 1008, 0, 15, &drum_bench_working_B.marlin_ec_o22_a[15] },/* Out22[15] */
  { 0x6000, 22, 1008, 0, 16, &drum_bench_working_B.marlin_ec_o22_a[16] },/* Out22[16] */
  { 0x6000, 22, 1008, 0, 17, &drum_bench_working_B.marlin_ec_o22_a[17] },/* Out22[17] */
  { 0x6000, 22, 1008, 0, 18, &drum_bench_working_B.marlin_ec_o22_a[18] },/* Out22[18] */
  { 0x6000, 22, 1008, 0, 19, &drum_bench_working_B.marlin_ec_o22_a[19] },/* Out22[19] */
  { 0x6000, 22, 1008, 0, 20, &drum_bench_working_B.marlin_ec_o22_a[20] },/* Out22[20] */
  { 0x6000, 22, 1008, 0, 21, &drum_bench_working_B.marlin_ec_o22_a[21] },/* Out22[21] */
  { 0x6000, 22, 1008, 0, 22, &drum_bench_working_B.marlin_ec_o22_a[22] },/* Out22[22] */
  { 0x6000, 22, 1008, 0, 23, &drum_bench_working_B.marlin_ec_o22_a[23] },/* Out22[23] */
  { 0x6000, 22, 1008, 0, 24, &drum_bench_working_B.marlin_ec_o22_a[24] },/* Out22[24] */
  { 0x6000, 22, 1008, 0, 25, &drum_bench_working_B.marlin_ec_o22_a[25] },/* Out22[25] */
  { 0x6000, 22, 1008, 0, 26, &drum_bench_working_B.marlin_ec_o22_a[26] },/* Out22[26] */
  { 0x6000, 22, 1008, 0, 27, &drum_bench_working_B.marlin_ec_o22_a[27] },/* Out22[27] */
};

/* S-Function Block: <S129>/marlin_ec
 * Registering EtherCAT block M3EC with Driver
 */
static struct ec_slave ec_slave_2 = {
  NULL,                                /* Linked list */
  0,                                   /* TID */
  0U, 0U, 0U, 0U,                      /* MasterId, DomainId, SlaveAlias, SlavePosition */
  1304U, 0x00000000,                   /* VendorId, ProductCode */
  0, NULL,                             /* SdoConfigCount, SdoVar */
  0, NULL,                             /* SoeConfigCount, SoeVar */
  sync_manager_518_00000000_0,         /* SyncManager Configuration */

  { 0, },                              /* Distributed Clock Configuration */
  86, 49, pdo_map_2                    /* RxPdo and TxPdo Pdo count and Configuration */
};

/* Block signals (auto storage) */
B_drum_bench_working_T drum_bench_working_B;

/* Block states (auto storage) */
DW_drum_bench_working_T drum_bench_working_DW;

/* Previous zero-crossings (trigger) states */
PrevZCX_drum_bench_working_T drum_bench_working_PrevZCX;

/* Real-time model */
RT_MODEL_drum_bench_working_T drum_bench_working_M_;
RT_MODEL_drum_bench_working_T *const drum_bench_working_M =
  &drum_bench_working_M_;

/* Forward declaration for local functions */
static void drum_bench_twister_state_vector(uint32_T mt[625], real_T seed);
static real_T drum_bench_w_eml_rand_mt19937ar(uint32_T state[625]);
static real_T drum_bench_working_rand(DW_MATLABFunction1_drum_bench_T *localDW);

/* All EtherLAB error messages go in here */
char etl_errbuf[256];

/*
 * Initial conditions for atomic system:
 *    '<S1>/Filtered Derivative'
 *    '<S2>/Filtered Derivative'
 *    '<S9>/Filtered Derivative'
 *    '<S10>/Filtered Derivative'
 */
void drum_be_FilteredDerivative_Init(DW_FilteredDerivative_drum_be_T *localDW)
{
  /* InitializeConditions for DiscreteIntegrator: '<S12>/Filter' */
  localDW->Filter_DSTATE = 0.0;
}

/*
 * Outputs for atomic system:
 *    '<S1>/Filtered Derivative'
 *    '<S2>/Filtered Derivative'
 *    '<S9>/Filtered Derivative'
 *    '<S10>/Filtered Derivative'
 */
void drum_bench_w_FilteredDerivative(real_T rtu_u,
  B_FilteredDerivative_drum_ben_T *localB, DW_FilteredDerivative_drum_be_T
  *localDW, real_T rtp_N)
{
  /* DiscreteIntegrator: '<S12>/Filter' */
  localB->Filter = localDW->Filter_DSTATE;

  /* Sum: '<S12>/Sum' */
  localB->Sum = rtu_u - localB->Filter;

  /* Gain: '<S12>/Filter Coefficient' */
  localB->FilterCoefficient = rtp_N * localB->Sum;
}

/*
 * Update for atomic system:
 *    '<S1>/Filtered Derivative'
 *    '<S2>/Filtered Derivative'
 *    '<S9>/Filtered Derivative'
 *    '<S10>/Filtered Derivative'
 */
void drum__FilteredDerivative_Update(B_FilteredDerivative_drum_ben_T *localB,
  DW_FilteredDerivative_drum_be_T *localDW)
{
  /* Update for DiscreteIntegrator: '<S12>/Filter' */
  localDW->Filter_DSTATE = 0.0003 * localB->FilterCoefficient +
    localDW->Filter_DSTATE;
}

/*
 * Start for enable system:
 *    '<S81>/enabled subsystem'
 *    '<S146>/enabled subsystem'
 */
void drum_ben_enabledsubsystem_Start(B_enabledsubsystem_drum_bench_T *localB)
{
  int32_T i;

  /* VirtualOutportStart for Outport: '<S93>/Out1' */
  for (i = 0; i < 20; i++) {
    localB->In1[i] = drum_bench_working_ConstP.pooled41[i];
  }

  /* End of VirtualOutportStart for Outport: '<S93>/Out1' */
}

/*
 * Output and update for enable system:
 *    '<S81>/enabled subsystem'
 *    '<S146>/enabled subsystem'
 */
void drum_bench_wor_enabledsubsystem(boolean_T rtu_0, const uint8_T rtu_1[20],
  B_enabledsubsystem_drum_bench_T *localB)
{
  int32_T i;

  /* Outputs for Enabled SubSystem: '<S81>/enabled subsystem' incorporates:
   *  EnablePort: '<S93>/Enable'
   */
  if (rtu_0) {
    /* Inport: '<S93>/In1' */
    for (i = 0; i < 20; i++) {
      localB->In1[i] = rtu_1[i];
    }

    /* End of Inport: '<S93>/In1' */
  }

  /* End of Outputs for SubSystem: '<S81>/enabled subsystem' */
}

/*
 * Start for enable system:
 *    '<S82>/Enabled Subsystem'
 *    '<S147>/Enabled Subsystem'
 */
void drum_ben_EnabledSubsystem_Start(B_EnabledSubsystem_drum_bench_T *localB)
{
  int32_T i;

  /* VirtualOutportStart for Outport: '<S95>/Out1' */
  for (i = 0; i < 20; i++) {
    localB->In1[i] = drum_bench_working_ConstP.pooled41[i];
  }

  /* End of VirtualOutportStart for Outport: '<S95>/Out1' */
}

/*
 * Output and update for enable system:
 *    '<S82>/Enabled Subsystem'
 *    '<S147>/Enabled Subsystem'
 */
void drum_bench_wor_EnabledSubsystem(boolean_T rtu_0, const uint8_T rtu_1[20],
  B_EnabledSubsystem_drum_bench_T *localB)
{
  int32_T i;

  /* Outputs for Enabled SubSystem: '<S82>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S95>/Enable'
   */
  if (rtu_0) {
    /* Inport: '<S95>/In1' */
    for (i = 0; i < 20; i++) {
      localB->In1[i] = rtu_1[i];
    }

    /* End of Inport: '<S95>/In1' */
  }

  /* End of Outputs for SubSystem: '<S82>/Enabled Subsystem' */
}

/* Function for MATLAB Function: '<S96>/MATLAB Function1' */
static void drum_bench_twister_state_vector(uint32_T mt[625], real_T seed)
{
  uint32_T r;
  int32_T mti;
  real_T tmp;
  tmp = seed;
  if (tmp < 4.294967296E+9) {
    if (tmp >= 0.0) {
      r = (uint32_T)tmp;
    } else {
      r = 0U;
    }
  } else {
    r = MAX_uint32_T;
  }

  mt[0] = r;
  for (mti = 0; mti < 623; mti++) {
    r = ((r >> 30U ^ r) * 1812433253U + mti) + 1U;
    mt[mti + 1] = r;
  }

  mt[624] = 624U;
}

/* Function for MATLAB Function: '<S96>/MATLAB Function1' */
static real_T drum_bench_w_eml_rand_mt19937ar(uint32_T state[625])
{
  real_T r;
  real_T b_r;
  uint32_T u[2];
  uint32_T mti;
  uint32_T y;
  int32_T j;
  int32_T k;
  boolean_T b_isvalid;
  int32_T exitg;
  boolean_T exitg_0;

  /* ========================= COPYRIGHT NOTICE ============================ */
  /*  This is a uniform (0,1) pseudorandom number generator based on:        */
  /*                                                                         */
  /*  A C-program for MT19937, with initialization improved 2002/1/26.       */
  /*  Coded by Takuji Nishimura and Makoto Matsumoto.                        */
  /*                                                                         */
  /*  Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,      */
  /*  All rights reserved.                                                   */
  /*                                                                         */
  /*  Redistribution and use in source and binary forms, with or without     */
  /*  modification, are permitted provided that the following conditions     */
  /*  are met:                                                               */
  /*                                                                         */
  /*    1. Redistributions of source code must retain the above copyright    */
  /*       notice, this list of conditions and the following disclaimer.     */
  /*                                                                         */
  /*    2. Redistributions in binary form must reproduce the above copyright */
  /*       notice, this list of conditions and the following disclaimer      */
  /*       in the documentation and/or other materials provided with the     */
  /*       distribution.                                                     */
  /*                                                                         */
  /*    3. The names of its contributors may not be used to endorse or       */
  /*       promote products derived from this software without specific      */
  /*       prior written permission.                                         */
  /*                                                                         */
  /*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
  /*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
  /*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
  /*  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT  */
  /*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
  /*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
  /*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
  /*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
  /*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
  /*  (INCLUDING  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE */
  /*  OF THIS  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  */
  /*                                                                         */
  /* =============================   END   ================================= */
  do {
    exitg = 0;
    u[0] = 0U;
    u[1] = 0U;
    for (j = 0; j < 2; j++) {
      mti = state[624] + 1U;
      if (mti >= 625U) {
        for (k = 0; k < 227; k++) {
          y = (state[1 + k] & 2147483647U) | (state[k] & 2147483648U);
          if ((int32_T)(y & 1U) == 0) {
            mti = y >> 1U;
          } else {
            mti = y >> 1U ^ 2567483615U;
          }

          state[k] = state[397 + k] ^ mti;
        }

        for (k = 0; k < 396; k++) {
          y = (state[k + 227] & 2147483648U) | (state[228 + k] & 2147483647U);
          if ((int32_T)(y & 1U) == 0) {
            mti = y >> 1U;
          } else {
            mti = y >> 1U ^ 2567483615U;
          }

          state[k + 227] = state[k] ^ mti;
        }

        y = (state[623] & 2147483648U) | (state[0] & 2147483647U);
        if ((int32_T)(y & 1U) == 0) {
          mti = y >> 1U;
        } else {
          mti = y >> 1U ^ 2567483615U;
        }

        state[623] = state[396] ^ mti;
        mti = 1U;
      }

      y = state[(int32_T)mti - 1];
      state[624] = mti;
      y ^= y >> 11U;
      y ^= y << 7U & 2636928640U;
      y ^= y << 15U & 4022730752U;
      y ^= y >> 18U;
      u[j] = y;
    }

    b_r = ((real_T)(u[0] >> 5U) * 6.7108864E+7 + (real_T)(u[1] >> 6U)) *
      1.1102230246251565E-16;
    if (b_r == 0.0) {
      if ((state[624] >= 1U) && (state[624] < 625U)) {
        b_isvalid = TRUE;
      } else {
        b_isvalid = FALSE;
      }

      if (b_isvalid) {
        b_isvalid = FALSE;
        k = 1;
        exitg_0 = FALSE;
        while ((!exitg_0) && (k < 625)) {
          if (state[k - 1] == 0U) {
            k++;
          } else {
            b_isvalid = TRUE;
            exitg_0 = TRUE;
          }
        }
      }

      if (!b_isvalid) {
        drum_bench_twister_state_vector(state, 5489.0);
      }
    } else {
      exitg = 1;
    }
  } while (exitg == 0);

  r = b_r;
  return r;
}

/* Function for MATLAB Function: '<S96>/MATLAB Function1' */
static real_T drum_bench_working_rand(DW_MATLABFunction1_drum_bench_T *localDW)
{
  real_T r;
  real_T b;
  int32_T hi;
  uint32_T test;
  uint32_T test_0;
  if (localDW->method == 4U) {
    hi = (int32_T)(localDW->state / 127773U);
    test = (localDW->state - hi * 127773U) * 16807U;
    test_0 = 2836U * hi;
    if (test < test_0) {
      test = (test - test_0) + 2147483647U;
    } else {
      test -= test_0;
    }

    localDW->state = test;
    r = (real_T)test * 4.6566128752457969E-10;
  } else if (localDW->method == 5U) {
    test = 69069U * localDW->state_g[0] + 1234567U;
    test_0 = localDW->state_g[1] << 13 ^ localDW->state_g[1];
    test_0 ^= test_0 >> 17;
    test_0 ^= test_0 << 5;
    localDW->state_g[0] = test;
    localDW->state_g[1] = test_0;
    r = (real_T)(test + test_0) * 2.328306436538696E-10;
  } else {
    if (!localDW->state_not_empty) {
      memset(&localDW->state_gf[0], 0, 625U * sizeof(uint32_T));
      drum_bench_twister_state_vector(localDW->state_gf, 5489.0);
      localDW->state_not_empty = TRUE;
    }

    b = drum_bench_w_eml_rand_mt19937ar(localDW->state_gf);
    r = b;
  }

  return r;
}

real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/*
 * Initial conditions for atomic system:
 *    '<S96>/MATLAB Function1'
 *    '<S161>/MATLAB Function1'
 */
void drum_bench_MATLABFunction1_Init(DW_MATLABFunction1_drum_bench_T *localDW)
{
  localDW->state_not_empty = FALSE;
  localDW->i = 0.0;
  localDW->id_sent = 0.0F;
  localDW->state = 1144108930U;
  localDW->state_g[0] = 362436069U;
  localDW->state_g[1] = 521288629U;
  localDW->method = 7U;
}

/*
 * Output and update for atomic system:
 *    '<S96>/MATLAB Function1'
 *    '<S161>/MATLAB Function1'
 */
void drum_bench_work_MATLABFunction1(const real32_T rtu_u[76], const
  MarlinFullInternalRPA *rtu_rpa, uint32_T rtu_rpc_type,
  B_MATLABFunction1_drum_bench__T *localB, DW_MATLABFunction1_drum_bench_T
  *localDW)
{
  uint32_T rpc_type;
  int32_T tmp;
  real_T tmp_0;
  rpc_type = rtu_rpc_type;

  /* MATLAB Function 'dsp param iterator/MATLAB Function1': '<S98>:1' */
  if ((((real_T)rtu_rpa->type == rtu_rpc_type) && (rtu_rpa->id ==
        localDW->id_sent)) || (localDW->id_sent == 0.0F)) {
    /* '<S98>:1:10' */
    /* '<S98>:1:11' */
    localDW->i++;
    if (localDW->i > 76.0) {
      /* '<S98>:1:12' */
      /* '<S98>:1:13' */
      localDW->i = 1.0;
    }

    /* '<S98>:1:15' */
    localDW->id_sent = (real32_T)drum_bench_working_rand(localDW);
  }

  /* '<S98>:1:18' */
  /* '<S98>:1:19' */
  /* '<S98>:1:20' */
  /* '<S98>:1:21' */
  if (rpc_type > 2147483647U) {
    rpc_type = 2147483647U;
  }

  localB->type = (int32_T)rpc_type;
  localB->id = localDW->id_sent;
  tmp_0 = rt_roundd_snf(localDW->i);
  if (tmp_0 < 2.147483648E+9) {
    if (tmp_0 >= -2.147483648E+9) {
      tmp = (int32_T)tmp_0;
    } else {
      tmp = MIN_int32_T;
    }
  } else {
    tmp = MAX_int32_T;
  }

  localB->index = tmp;
  localB->value = rtu_u[(int32_T)localDW->i - 1];
}

/*
 * Output and update for atomic system:
 *    '<S63>/select rpc'
 *    '<S128>/select rpc'
 */
void drum_bench_working_selectrpc(real_T rtu_rpc_command, const
  MarlinFullInternalRPC rtu_u[15], B_selectrpc_drum_bench_workin_T *localB)
{
  int32_T i;

  /* MATLAB Function 'marlin_1.1_full/rpc_generator/select rpc': '<S87>:1' */
  /* active_id = find(u.request,1,'first'); */
  /*  active_id = 0; */
  /*  for i = 1:length(u) */
  /*      if (u(i).request) */
  /*          active_id = i; */
  /*          break; */
  /*      end */
  /*  end */
  if ((rtu_rpc_command != 0.0) && (rtu_rpc_command >= 1.0) && (rtu_rpc_command <
       15.0)) {
    /* '<S87>:1:17' */
    /* '<S87>:1:18' */
    localB->type = rtu_u[(int32_T)rtu_rpc_command - 1].type;

    /* '<S87>:1:19' */
    localB->id = rtu_u[(int32_T)rtu_rpc_command - 1].id;

    /* '<S87>:1:20' */
    for (i = 0; i < 20; i++) {
      localB->data[i] = rtu_u[(int32_T)rtu_rpc_command - 1].data[i];
    }
  } else {
    /* '<S87>:1:22' */
    localB->type = 0;

    /* '<S87>:1:23' */
    localB->id = 0.0F;

    /* '<S87>:1:24' */
    for (i = 0; i < 20; i++) {
      localB->data[i] = 0U;
    }
  }
}

/* Model output function */
static void drum_bench_working_output(void)
{
  real_T u;
  ZCEventType zcEvent;
  int32_T i;
  real_T u_0;
  real_T u_1;

  /* user code (Output function Body) */

  /* EtherCAT Process for Sample Time [0.0003] */
  if (1 && rtmIsMajorTimeStep(drum_bench_working_M)) {
    ecs_receive(0);

#ifdef ASYNC_ECAT

    ecs_send(0);

#endif

  }

  /* S-Function Block: <S64>/marlin_ec */
  /* Output Port 1 written directly by slave */
  /* Output Port 2 written directly by slave */
  /* Output Port 3 written directly by slave */
  /* Output Port 4 written directly by slave */
  /* Output Port 5 written directly by slave */
  /* Output Port 6 written directly by slave */
  /* Output Port 7 written directly by slave */
  /* Output Port 8 written directly by slave */
  /* Output Port 9 written directly by slave */
  /* Output Port 10 written directly by slave */
  /* Output Port 11 written directly by slave */
  /* Output Port 12 written directly by slave */
  /* Output Port 13 written directly by slave */
  /* Output Port 14 written directly by slave */
  /* Output Port 15 written directly by slave */
  /* Output Port 16 written directly by slave */
  /* Output Port 17 written directly by slave */
  /* Output Port 18 written directly by slave */
  /* Output Port 19 written directly by slave */
  /* Output Port 20 written directly by slave */
  /* Output Port 21 written directly by slave */
  /* Output Port 22 written directly by slave */

  /* DataTypeConversion: '<S64>/motor_torque_' */
  drum_bench_working_B.motor_torque = drum_bench_working_B.marlin_ec_o1;

  /* DataTypeConversion: '<S64>/motor_iq_' */
  drum_bench_working_B.motor_iq = drum_bench_working_B.marlin_ec_o2;

  /* DataTypeConversion: '<S64>/motor_vq_avg_' */
  drum_bench_working_B.motor_vq_avg = drum_bench_working_B.marlin_ec_o3;

  /* DataTypeConversion: '<S64>/analog_1_' */
  drum_bench_working_B.analog_1 = drum_bench_working_B.marlin_ec_o4;

  /* DataTypeConversion: '<S64>/analog_1_dot_' */
  drum_bench_working_B.analog_1_dot = drum_bench_working_B.marlin_ec_o5;

  /* DataTypeConversion: '<S64>/analog_2_' */
  drum_bench_working_B.analog_2 = drum_bench_working_B.marlin_ec_o6;

  /* DataTypeConversion: '<S64>/analog_2_dot_' */
  drum_bench_working_B.analog_2_dot = drum_bench_working_B.marlin_ec_o7;

  /* DataTypeConversion: '<S64>/analog_diff_' */
  drum_bench_working_B.analog_diff = drum_bench_working_B.marlin_ec_o8;

  /* DataTypeConversion: '<S64>/analog_diff_dot_' */
  drum_bench_working_B.analog_diff_dot = drum_bench_working_B.marlin_ec_o9;

  /* DataTypeConversion: '<S64>/quadrature_1_' */
  drum_bench_working_B.quadrature_1 = drum_bench_working_B.marlin_ec_o10;

  /* DataTypeConversion: '<S64>/quadrature_1_dot_' */
  drum_bench_working_B.quadrature_1_dot = drum_bench_working_B.marlin_ec_o11;

  /* DataTypeConversion: '<S64>/quadrature_2_' */
  drum_bench_working_B.quadrature_2 = drum_bench_working_B.marlin_ec_o12;

  /* DataTypeConversion: '<S64>/quadrature_2_dot_' */
  drum_bench_working_B.quadrature_2_dot = drum_bench_working_B.marlin_ec_o13;

  /* DataTypeConversion: '<S64>/ssi_' */
  drum_bench_working_B.ssi = drum_bench_working_B.marlin_ec_o14;

  /* DataTypeConversion: '<S64>/ssi_dot_' */
  drum_bench_working_B.ssi_dot = drum_bench_working_B.marlin_ec_o15;

  /* DataTypeConversion: '<S64>/accelerometer_x_' */
  drum_bench_working_B.accelerometer_x = drum_bench_working_B.marlin_ec_o16;

  /* DataTypeConversion: '<S64>/accelerometer_y_' */
  drum_bench_working_B.accelerometer_y = drum_bench_working_B.marlin_ec_o17;

  /* DataTypeConversion: '<S64>/accelerometer_z_' */
  drum_bench_working_B.accelerometer_z = drum_bench_working_B.marlin_ec_o18;

  /* DataTypeConversion: '<S64>/faults_' */
  drum_bench_working_B.faults = drum_bench_working_B.marlin_ec_o19;

  /* DataTypeConversion: '<S64>/bus_v_' */
  drum_bench_working_B.bus_v = drum_bench_working_B.marlin_ec_o20;

  /* BusCreator: '<S7>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.motor_torque =
    drum_bench_working_B.motor_torque;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.motor_iq =
    drum_bench_working_B.motor_iq;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.motor_vq_avg =
    drum_bench_working_B.motor_vq_avg;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.analog_1 =
    drum_bench_working_B.analog_1;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.analog_1_dot =
    drum_bench_working_B.analog_1_dot;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.analog_2 =
    drum_bench_working_B.analog_2;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.analog_2_dot =
    drum_bench_working_B.analog_2_dot;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.analog_diff =
    drum_bench_working_B.analog_diff;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.analog_diff_dot =
    drum_bench_working_B.analog_diff_dot;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.quadrature_1 =
    drum_bench_working_B.quadrature_1;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.quadrature_1_dot =
    drum_bench_working_B.quadrature_1_dot;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.quadrature_2 =
    drum_bench_working_B.quadrature_2;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.quadrature_2_dot =
    drum_bench_working_B.quadrature_2_dot;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.ssi =
    drum_bench_working_B.ssi;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.ssi_dot =
    drum_bench_working_B.ssi_dot;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.accelerometer_x =
    drum_bench_working_B.accelerometer_x;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.accelerometer_y =
    drum_bench_working_B.accelerometer_y;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.accelerometer_z =
    drum_bench_working_B.accelerometer_z;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.faults =
    drum_bench_working_B.faults;
  drum_bench_working_B.BusConversion_InsertedFor_BusCr.bus_v =
    drum_bench_working_B.bus_v;
  for (i = 0; i < 28; i++) {
    /* DataTypeConversion: '<S64>/rpa_packet_' */
    drum_bench_working_B.rpa_packet[i] = drum_bench_working_B.marlin_ec_o22[i];
    drum_bench_working_B.BusConversion_InsertedFor_BusCr.rpa_packet[i] =
      drum_bench_working_B.rpa_packet[i];
  }

  /* End of BusCreator: '<S7>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */

  /* Delay: '<S62>/Delay' */
  drum_bench_working_B.Delay[0] = drum_bench_working_DW.Delay_DSTATE[0];
  drum_bench_working_B.Delay[1] = drum_bench_working_DW.Delay_DSTATE[1];

  /* Gain: '<S62>/eA1' */
  drum_bench_working_B.eA1[0] = 0.0;
  drum_bench_working_B.eA1[0] +=
    drum_bench_working_P.config.marlins.M0.motor.thermal_model.eA1[0] *
    drum_bench_working_B.Delay[0];
  drum_bench_working_B.eA1[0] +=
    drum_bench_working_P.config.marlins.M0.motor.thermal_model.eA1[2] *
    drum_bench_working_B.Delay[1];
  drum_bench_working_B.eA1[1] = 0.0;
  drum_bench_working_B.eA1[1] +=
    drum_bench_working_P.config.marlins.M0.motor.thermal_model.eA1[1] *
    drum_bench_working_B.Delay[0];
  drum_bench_working_B.eA1[1] +=
    drum_bench_working_P.config.marlins.M0.motor.thermal_model.eA1[3] *
    drum_bench_working_B.Delay[1];

  /* Math: '<S62>/Math Function'
   *
   * About '<S62>/Math Function':
   *  Operator: magnitude^2
   */
  u = drum_bench_working_B.motor_iq;
  drum_bench_working_B.MathFunction = u * u;

  /* Gain: '<S62>/resistance' */
  drum_bench_working_B.resistance =
    drum_bench_working_P.config.marlins.M0.motor.resistance *
    drum_bench_working_B.MathFunction;

  /* Gain: '<S62>/eA2' */
  drum_bench_working_B.eA2[0] =
    drum_bench_working_P.config.marlins.M0.motor.thermal_model.eA2[0] *
    drum_bench_working_B.resistance;
  drum_bench_working_B.eA2[1] =
    drum_bench_working_P.config.marlins.M0.motor.thermal_model.eA2[1] *
    drum_bench_working_B.resistance;

  /* Sum: '<S62>/Sum' */
  drum_bench_working_B.Sum[0] = drum_bench_working_B.eA1[0] +
    drum_bench_working_B.eA2[0];
  drum_bench_working_B.Sum[1] = drum_bench_working_B.eA1[1] +
    drum_bench_working_B.eA2[1];

  /* BusCreator: '<S7>/Bus Creator' */
  drum_bench_working_B.BusCreator.marlin_basic_status =
    drum_bench_working_B.BusConversion_InsertedFor_BusCr;
  drum_bench_working_B.BusCreator.temperature_motor_winding =
    drum_bench_working_B.Sum[0];
  drum_bench_working_B.BusCreator.temperature_motor_housing =
    drum_bench_working_B.Sum[1];

  /* BusSelector: '<S5>/Bus Selector' */
  drum_bench_working_B.marlin_basic_status =
    drum_bench_working_B.BusCreator.marlin_basic_status;
  drum_bench_working_B.temperature_motor_winding =
    drum_bench_working_B.BusCreator.temperature_motor_winding;
  drum_bench_working_B.temperature_motor_housing =
    drum_bench_working_B.BusCreator.temperature_motor_housing;

  /* BusSelector: '<S34>/Bus Selector' */
  drum_bench_working_B.motor_torque_m =
    drum_bench_working_B.marlin_basic_status.motor_torque;
  drum_bench_working_B.motor_iq_a =
    drum_bench_working_B.marlin_basic_status.motor_iq;
  drum_bench_working_B.motor_vq_avg_f =
    drum_bench_working_B.marlin_basic_status.motor_vq_avg;
  drum_bench_working_B.analog_1_g =
    drum_bench_working_B.marlin_basic_status.analog_1;
  drum_bench_working_B.analog_1_dot_h =
    drum_bench_working_B.marlin_basic_status.analog_1_dot;
  drum_bench_working_B.analog_2_k =
    drum_bench_working_B.marlin_basic_status.analog_2;
  drum_bench_working_B.analog_2_dot_m =
    drum_bench_working_B.marlin_basic_status.analog_2_dot;
  drum_bench_working_B.analog_diff_h =
    drum_bench_working_B.marlin_basic_status.analog_diff;
  drum_bench_working_B.analog_diff_dot_k =
    drum_bench_working_B.marlin_basic_status.analog_diff_dot;
  drum_bench_working_B.quadrature_1_h =
    drum_bench_working_B.marlin_basic_status.quadrature_1;
  drum_bench_working_B.quadrature_1_dot_l =
    drum_bench_working_B.marlin_basic_status.quadrature_1_dot;
  drum_bench_working_B.quadrature_2_o =
    drum_bench_working_B.marlin_basic_status.quadrature_2;
  drum_bench_working_B.quadrature_2_dot_l =
    drum_bench_working_B.marlin_basic_status.quadrature_2_dot;
  drum_bench_working_B.ssi_c = drum_bench_working_B.marlin_basic_status.ssi;
  drum_bench_working_B.ssi_dot_f =
    drum_bench_working_B.marlin_basic_status.ssi_dot;
  drum_bench_working_B.accelerometer_x_n =
    drum_bench_working_B.marlin_basic_status.accelerometer_x;
  drum_bench_working_B.accelerometer_y_a =
    drum_bench_working_B.marlin_basic_status.accelerometer_y;
  drum_bench_working_B.accelerometer_z_l =
    drum_bench_working_B.marlin_basic_status.accelerometer_z;
  drum_bench_working_B.faults_p =
    drum_bench_working_B.marlin_basic_status.faults;
  drum_bench_working_B.bus_v_k = drum_bench_working_B.marlin_basic_status.bus_v;
  for (i = 0; i < 28; i++) {
    drum_bench_working_B.rpa_packet_k[i] =
      drum_bench_working_B.marlin_basic_status.rpa_packet[i];
  }

  /* End of BusSelector: '<S34>/Bus Selector' */

  /* DataTypeConversion: '<S34>/accelerometer_x' */
  drum_bench_working_B.accelerometer_x_e =
    drum_bench_working_B.accelerometer_x_n;

  /* DataTypeConversion: '<S34>/accelerometer_y' */
  drum_bench_working_B.accelerometer_y_b =
    drum_bench_working_B.accelerometer_y_a;

  /* DataTypeConversion: '<S34>/accelerometer_z' */
  drum_bench_working_B.accelerometer_z_d =
    drum_bench_working_B.accelerometer_z_l;

  /* DataTypeConversion: '<S34>/analog_1' */
  drum_bench_working_B.analog_1_o = drum_bench_working_B.analog_1_g;

  /* DataTypeConversion: '<S34>/analog_1_dot' */
  drum_bench_working_B.analog_1_dot_d = drum_bench_working_B.analog_1_dot_h;

  /* DataTypeConversion: '<S34>/analog_2' */
  drum_bench_working_B.analog_2_p = drum_bench_working_B.analog_2_k;

  /* DataTypeConversion: '<S34>/analog_2_dot' */
  drum_bench_working_B.analog_2_dot_l = drum_bench_working_B.analog_2_dot_m;

  /* DataTypeConversion: '<S34>/analog_diff' */
  drum_bench_working_B.analog_diff_b = drum_bench_working_B.analog_diff_h;

  /* DataTypeConversion: '<S34>/analog_diff_dot' */
  drum_bench_working_B.analog_diff_dot_f =
    drum_bench_working_B.analog_diff_dot_k;

  /* DataTypeConversion: '<S34>/bus_v' */
  drum_bench_working_B.bus_v_d = drum_bench_working_B.bus_v_k;

  /* DataTypeConversion: '<S35>/raw_integer' */
  drum_bench_working_B.raw_integer = drum_bench_working_B.faults_p;

  /* DataTypeConversion: '<S36>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_n = (uint8_T)((uint8_T)
    drum_bench_working_B.raw_integer & 1);

  /* DataTypeConversion: '<S36>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly =
    drum_bench_working_B.ExtractDesiredBits_n;

  /* DataTypeConversion: '<S37>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_pr = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer >> 1) & 1);

  /* DataTypeConversion: '<S37>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_c =
    drum_bench_working_B.ExtractDesiredBits_pr;

  /* DataTypeConversion: '<S38>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_dw = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer >> 10) & 1);

  /* DataTypeConversion: '<S38>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_j =
    drum_bench_working_B.ExtractDesiredBits_dw;

  /* DataTypeConversion: '<S39>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_k = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer >> 2) & 1);

  /* DataTypeConversion: '<S39>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_b =
    drum_bench_working_B.ExtractDesiredBits_k;

  /* DataTypeConversion: '<S40>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_l = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer >> 3) & 1);

  /* DataTypeConversion: '<S40>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_jt =
    drum_bench_working_B.ExtractDesiredBits_l;

  /* DataTypeConversion: '<S41>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_d = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer >> 4) & 1);

  /* DataTypeConversion: '<S41>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_h =
    drum_bench_working_B.ExtractDesiredBits_d;

  /* DataTypeConversion: '<S42>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_h = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer >> 5) & 1);

  /* DataTypeConversion: '<S42>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_m =
    drum_bench_working_B.ExtractDesiredBits_h;

  /* DataTypeConversion: '<S43>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_ng = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer >> 6) & 1);

  /* DataTypeConversion: '<S43>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_i =
    drum_bench_working_B.ExtractDesiredBits_ng;

  /* DataTypeConversion: '<S44>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_fo = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer >> 7) & 1);

  /* DataTypeConversion: '<S44>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_mj =
    drum_bench_working_B.ExtractDesiredBits_fo;

  /* DataTypeConversion: '<S45>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_cg = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer >> 8) & 1);

  /* DataTypeConversion: '<S45>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_jz =
    drum_bench_working_B.ExtractDesiredBits_cg;

  /* DataTypeConversion: '<S46>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer >> 9) & 1);

  /* DataTypeConversion: '<S46>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_d =
    drum_bench_working_B.ExtractDesiredBits;

  /* DataTypeConversion: '<S35>/analog_' */
  drum_bench_working_B.analog = (drum_bench_working_B.ModifyScalingOnly_i != 0);

  /* DataTypeConversion: '<S35>/communication_fault_' */
  drum_bench_working_B.communication_fault =
    (drum_bench_working_B.ModifyScalingOnly_b != 0);

  /* DataTypeConversion: '<S35>/current_' */
  drum_bench_working_B.current = (drum_bench_working_B.ModifyScalingOnly_h != 0);

  /* DataTypeConversion: '<S35>/ethercat_' */
  drum_bench_working_B.ethercat_m = (drum_bench_working_B.ModifyScalingOnly_jz
    != 0);

  /* DataTypeConversion: '<S35>/external_panic_' */
  drum_bench_working_B.external_panic_e =
    (drum_bench_working_B.ModifyScalingOnly_jt != 0);

  /* DataTypeConversion: '<S35>/hard_fault_' */
  drum_bench_working_B.hard_fault = (drum_bench_working_B.ModifyScalingOnly_c !=
    0);

  /* DataTypeConversion: '<S35>/quadrature_' */
  drum_bench_working_B.quadrature = (drum_bench_working_B.ModifyScalingOnly_mj
    != 0);

  /* DataTypeConversion: '<S35>/soft_fault_' */
  drum_bench_working_B.soft_fault = (drum_bench_working_B.ModifyScalingOnly != 0);

  /* DataTypeConversion: '<S35>/ssi_' */
  drum_bench_working_B.ssi_i = (drum_bench_working_B.ModifyScalingOnly_j != 0);

  /* DataTypeConversion: '<S35>/temperature_' */
  drum_bench_working_B.temperature = (drum_bench_working_B.ModifyScalingOnly_d
    != 0);

  /* DataTypeConversion: '<S35>/voltage_' */
  drum_bench_working_B.voltage = (drum_bench_working_B.ModifyScalingOnly_m != 0);

  /* DataTypeConversion: '<S34>/motor_iq' */
  drum_bench_working_B.motor_iq_g = drum_bench_working_B.motor_iq_a;

  /* DataTypeConversion: '<S34>/motor_torque' */
  drum_bench_working_B.motor_torque_f = drum_bench_working_B.motor_torque_m;

  /* DataTypeConversion: '<S34>/motor_vq_avg' */
  drum_bench_working_B.motor_vq_avg_b = drum_bench_working_B.motor_vq_avg_f;

  /* DataTypeConversion: '<S34>/quadrature_1' */
  drum_bench_working_B.quadrature_1_j = drum_bench_working_B.quadrature_1_h;

  /* DataTypeConversion: '<S34>/quadrature_1_dot' */
  drum_bench_working_B.quadrature_1_dot_c =
    drum_bench_working_B.quadrature_1_dot_l;

  /* DataTypeConversion: '<S34>/quadrature_2' */
  drum_bench_working_B.quadrature_2_l = drum_bench_working_B.quadrature_2_o;

  /* DataTypeConversion: '<S34>/quadrature_2_dot' */
  drum_bench_working_B.quadrature_2_dot_o =
    drum_bench_working_B.quadrature_2_dot_l;

  /* Unpack: <S34>/rpa_float */
  (void) memcpy(&drum_bench_working_B.rpa_float[0],
                &drum_bench_working_B.rpa_packet_k[0],
                28);

  /* DataTypeConversion: '<S34>/ssi' */
  drum_bench_working_B.ssi_f = drum_bench_working_B.ssi_c;

  /* DataTypeConversion: '<S34>/ssi_dot' */
  drum_bench_working_B.ssi_dot_k = drum_bench_working_B.ssi_dot_f;

  /* DataTypeConversion: '<S5>/temperature_motor_housing' */
  drum_bench_working_B.temperature_motor_housing_j =
    drum_bench_working_B.temperature_motor_housing;

  /* DataTypeConversion: '<S5>/temperature_motor_winding' */
  drum_bench_working_B.temperature_motor_winding_h =
    drum_bench_working_B.temperature_motor_winding;

  /* Sum: '<S17>/Sum' incorporates:
   *  Constant: '<S17>/Constant'
   */
  drum_bench_working_B.Sum_c = 0.0 +
    drum_bench_working_P.config.input.input0.quadrature_1_angle.bias;

  /* RateLimiter: '<S17>/Rate Limiter' */
  u = drum_bench_working_B.Sum_c - drum_bench_working_DW.PrevY;
  if (u > 29.999999999999996) {
    drum_bench_working_B.RateLimiter = drum_bench_working_DW.PrevY +
      29.999999999999996;
  } else if (u < (-29.999999999999996)) {
    drum_bench_working_B.RateLimiter = drum_bench_working_DW.PrevY +
      (-29.999999999999996);
  } else {
    drum_bench_working_B.RateLimiter = drum_bench_working_B.Sum_c;
  }

  drum_bench_working_DW.PrevY = drum_bench_working_B.RateLimiter;

  /* End of RateLimiter: '<S17>/Rate Limiter' */

  /* DataTypeConversion: '<S17>/Data Type Conversion' */
  drum_bench_working_B.DataTypeConversion = drum_bench_working_B.RateLimiter;

  /* Sum: '<S18>/Sum' incorporates:
   *  Constant: '<S18>/Constant'
   */
  drum_bench_working_B.Sum_p = 0.0 +
    drum_bench_working_P.config.input.input0.quadrature_2_angle.bias;

  /* RateLimiter: '<S18>/Rate Limiter' */
  u = drum_bench_working_B.Sum_p - drum_bench_working_DW.PrevY_a;
  if (u > 0.0003) {
    drum_bench_working_B.RateLimiter_o = drum_bench_working_DW.PrevY_a + 0.0003;
  } else if (u < (-0.0003)) {
    drum_bench_working_B.RateLimiter_o = drum_bench_working_DW.PrevY_a +
      (-0.0003);
  } else {
    drum_bench_working_B.RateLimiter_o = drum_bench_working_B.Sum_p;
  }

  drum_bench_working_DW.PrevY_a = drum_bench_working_B.RateLimiter_o;

  /* End of RateLimiter: '<S18>/Rate Limiter' */

  /* DataTypeConversion: '<S18>/Data Type Conversion' */
  drum_bench_working_B.DataTypeConversion_a = drum_bench_working_B.RateLimiter_o;

  /* Sum: '<S14>/Sum' incorporates:
   *  Constant: '<S14>/Constant'
   */
  drum_bench_working_B.Sum_cn = 0.0 +
    drum_bench_working_P.config.input.input0.analog.bias;

  /* RateLimiter: '<S14>/Rate Limiter' */
  u = drum_bench_working_B.Sum_cn - drum_bench_working_DW.PrevY_h;
  if (u > 0.0003) {
    drum_bench_working_B.RateLimiter_b = drum_bench_working_DW.PrevY_h + 0.0003;
  } else if (u < (-0.0003)) {
    drum_bench_working_B.RateLimiter_b = drum_bench_working_DW.PrevY_h +
      (-0.0003);
  } else {
    drum_bench_working_B.RateLimiter_b = drum_bench_working_B.Sum_cn;
  }

  drum_bench_working_DW.PrevY_h = drum_bench_working_B.RateLimiter_b;

  /* End of RateLimiter: '<S14>/Rate Limiter' */

  /* DataTypeConversion: '<S14>/Data Type Conversion' */
  drum_bench_working_B.DataTypeConversion_c = drum_bench_working_B.RateLimiter_b;

  /* Sum: '<S15>/Sum' incorporates:
   *  Constant: '<S15>/Constant'
   */
  drum_bench_working_B.Sum_d = 0.0 +
    drum_bench_working_P.config.input.input0.motor_iq.bias;

  /* RateLimiter: '<S15>/Rate Limiter' */
  u = drum_bench_working_B.Sum_d - drum_bench_working_DW.PrevY_d;
  if (u > 3.0E+196) {
    drum_bench_working_B.RateLimiter_k = drum_bench_working_DW.PrevY_d +
      3.0E+196;
  } else if (u < (-3.0E+196)) {
    drum_bench_working_B.RateLimiter_k = drum_bench_working_DW.PrevY_d +
      (-3.0E+196);
  } else {
    drum_bench_working_B.RateLimiter_k = drum_bench_working_B.Sum_d;
  }

  drum_bench_working_DW.PrevY_d = drum_bench_working_B.RateLimiter_k;

  /* End of RateLimiter: '<S15>/Rate Limiter' */

  /* DataTypeConversion: '<S15>/Data Type Conversion' */
  drum_bench_working_B.DataTypeConversion_n = drum_bench_working_B.RateLimiter_k;

  /* Sum: '<S16>/Sum' incorporates:
   *  Constant: '<S16>/Constant'
   */
  drum_bench_working_B.Sum_l = 0.0 +
    drum_bench_working_P.config.input.input0.motor_vq.bias;

  /* RateLimiter: '<S16>/Rate Limiter' */
  u = drum_bench_working_B.Sum_l - drum_bench_working_DW.PrevY_dx;
  if (u > 0.0003) {
    drum_bench_working_B.RateLimiter_e = drum_bench_working_DW.PrevY_dx + 0.0003;
  } else if (u < (-0.0003)) {
    drum_bench_working_B.RateLimiter_e = drum_bench_working_DW.PrevY_dx +
      (-0.0003);
  } else {
    drum_bench_working_B.RateLimiter_e = drum_bench_working_B.Sum_l;
  }

  drum_bench_working_DW.PrevY_dx = drum_bench_working_B.RateLimiter_e;

  /* End of RateLimiter: '<S16>/Rate Limiter' */

  /* DataTypeConversion: '<S16>/Data Type Conversion' */
  drum_bench_working_B.DataTypeConversion_d = drum_bench_working_B.RateLimiter_e;

  /* BusCreator: '<S3>/Bus Creator' incorporates:
   *  Constant: '<S3>/analog_diff_dot'
   *  Constant: '<S3>/mode'
   *  Constant: '<S3>/motor_id'
   *  Constant: '<S3>/motor_torque'
   *  Constant: '<S3>/motor_vd'
   *  Constant: '<S3>/quadrature_1_angle_dot'
   *  Constant: '<S3>/quadrature_2_angle_dot'
   *  Constant: '<S3>/rpc_command'
   */
  drum_bench_working_B.BusCreator_n.mode =
    drum_bench_working_P.config.input.input0.mode;
  drum_bench_working_B.BusCreator_n.quadrature_1_angle =
    drum_bench_working_B.DataTypeConversion;
  drum_bench_working_B.BusCreator_n.quadrature_1_angle_dot =
    drum_bench_working_P.config.input.input0.quadrature_1_angle_dot;
  drum_bench_working_B.BusCreator_n.quadrature_2_angle =
    drum_bench_working_B.DataTypeConversion_a;
  drum_bench_working_B.BusCreator_n.quadrature_2_angle_dot =
    drum_bench_working_P.config.input.input0.quadrature_2_angle_dot;
  drum_bench_working_B.BusCreator_n.analog_diff =
    drum_bench_working_B.DataTypeConversion_c;
  drum_bench_working_B.BusCreator_n.analog_diff_dot =
    drum_bench_working_P.config.input.input0.analog_dot;
  drum_bench_working_B.BusCreator_n.motor_torque =
    drum_bench_working_P.config.input.input0.motor_torque;
  drum_bench_working_B.BusCreator_n.motor_iq =
    drum_bench_working_B.DataTypeConversion_n;
  drum_bench_working_B.BusCreator_n.motor_id =
    drum_bench_working_P.config.input.input0.motor_id;
  drum_bench_working_B.BusCreator_n.motor_vq =
    drum_bench_working_B.DataTypeConversion_d;
  drum_bench_working_B.BusCreator_n.motor_vd =
    drum_bench_working_P.config.input.input0.motor_vd;
  drum_bench_working_B.BusCreator_n.rpc =
    drum_bench_working_P.config.input.input0.rpc_command;

  /* BusSelector: '<S1>/Bus Selector2' */
  drum_bench_working_B.quadrature_1_angle =
    drum_bench_working_B.BusCreator_n.quadrature_1_angle;
  drum_bench_working_B.quadrature_1_angle_dot =
    drum_bench_working_B.BusCreator_n.quadrature_1_angle_dot;

  /* BusSelector: '<S1>/Bus Selector3' */
  drum_bench_working_B.quadrature_1_n =
    drum_bench_working_B.BusCreator.marlin_basic_status.quadrature_1;

  /* Sum: '<S1>/Sum' */
  drum_bench_working_B.Sum_m = drum_bench_working_B.quadrature_1_angle -
    drum_bench_working_B.quadrature_1_n;

  /* Gain: '<S1>/kp' */
  drum_bench_working_B.kp =
    drum_bench_working_P.config.control0.impedence_control.kp *
    drum_bench_working_B.Sum_m;

  /* Saturate: '<S1>/Saturation' */
  u = drum_bench_working_B.kp;
  u_0 = (-12.0);
  u_1 = 12.0;
  if (u >= u_1) {
    drum_bench_working_B.Saturation = u_1;
  } else if (u <= u_0) {
    drum_bench_working_B.Saturation = u_0;
  } else {
    drum_bench_working_B.Saturation = u;
  }

  /* End of Saturate: '<S1>/Saturation' */

  /* Outputs for Atomic SubSystem: '<S1>/Filtered Derivative' */
  drum_bench_w_FilteredDerivative(drum_bench_working_B.quadrature_1_n,
    &drum_bench_working_B.FilteredDerivative,
    &drum_bench_working_DW.FilteredDerivative,
    drum_bench_working_P.config.control0.impedence_control.imp_cutoff);

  /* End of Outputs for SubSystem: '<S1>/Filtered Derivative' */

  /* Sum: '<S1>/Sum1' */
  drum_bench_working_B.Sum1 = drum_bench_working_B.quadrature_1_angle_dot -
    drum_bench_working_B.FilteredDerivative.FilterCoefficient;

  /* Gain: '<S1>/kd' */
  drum_bench_working_B.kd =
    drum_bench_working_P.config.control0.impedence_control.kd *
    drum_bench_working_B.Sum1;

  /* Sum: '<S1>/Sum2' */
  drum_bench_working_B.Sum2 = drum_bench_working_B.Saturation +
    drum_bench_working_B.kd;

  /* DataTypeConversion: '<S1>/motor_torque' */
  drum_bench_working_B.motor_torque_p = drum_bench_working_B.Sum2;

  /* BusAssignment: '<S1>/Bus Assignment' incorporates:
   *  Constant: '<S1>/current mode'
   */
  drum_bench_working_B.BusAssignment = drum_bench_working_B.BusCreator_n;
  drum_bench_working_B.BusAssignment.motor_iq =
    drum_bench_working_B.motor_torque_p;
  drum_bench_working_B.BusAssignment.mode = 3.0;

  /* BusSelector: '<S9>/Bus Selector2' */
  drum_bench_working_B.quadrature_1_angle_k =
    drum_bench_working_B.BusCreator_n.quadrature_1_angle;
  drum_bench_working_B.quadrature_1_angle_dot_o =
    drum_bench_working_B.BusCreator_n.quadrature_1_angle_dot;

  /* BusSelector: '<S9>/Bus Selector1' */
  drum_bench_working_B.quadrature_1_f =
    drum_bench_working_B.BusCreator.marlin_basic_status.quadrature_1;
  drum_bench_working_B.analog_1_i =
    drum_bench_working_B.BusCreator.marlin_basic_status.analog_1;

  /* Sum: '<S9>/Sum' */
  drum_bench_working_B.Sum_e = drum_bench_working_B.quadrature_1_angle_k -
    drum_bench_working_B.quadrature_1_f;

  /* Bias: '<S9>/bias' */
  drum_bench_working_B.bias = drum_bench_working_B.analog_1_i +
    drum_bench_working_P.config.control0.var_impedence_control.bias;

  /* Gain: '<S9>/ka' */
  drum_bench_working_B.ka =
    drum_bench_working_P.config.control0.var_impedence_control.ka *
    drum_bench_working_B.bias;

  /* Product: '<S9>/Product' */
  drum_bench_working_B.Product = drum_bench_working_B.Sum_e *
    drum_bench_working_B.ka;

  /* Outputs for Atomic SubSystem: '<S9>/Filtered Derivative' */
  drum_bench_w_FilteredDerivative(drum_bench_working_B.quadrature_1_f,
    &drum_bench_working_B.FilteredDerivative_c,
    &drum_bench_working_DW.FilteredDerivative_c,
    drum_bench_working_P.config.control0.var_impedence_control.var_imp_cutoff);

  /* End of Outputs for SubSystem: '<S9>/Filtered Derivative' */

  /* Sum: '<S9>/Sum1' */
  drum_bench_working_B.Sum1_o = drum_bench_working_B.quadrature_1_angle_dot_o -
    drum_bench_working_B.FilteredDerivative_c.FilterCoefficient;

  /* Gain: '<S9>/kd' */
  drum_bench_working_B.kd_n =
    drum_bench_working_P.config.control0.var_impedence_control.kd *
    drum_bench_working_B.Sum1_o;

  /* Sum: '<S9>/Sum2' */
  drum_bench_working_B.Sum2_b = drum_bench_working_B.Product +
    drum_bench_working_B.kd_n;

  /* BusAssignment: '<S9>/Bus Assignment' incorporates:
   *  Constant: '<S9>/current mode'
   */
  drum_bench_working_B.BusAssignment_n = drum_bench_working_B.BusCreator_n;
  drum_bench_working_B.BusAssignment_n.motor_iq = drum_bench_working_B.Sum2_b;
  drum_bench_working_B.BusAssignment_n.mode = 3.0;

  /* MultiPortSwitch: '<Root>/mode switch' incorporates:
   *  Constant: '<Root>/mode select'
   */
  switch ((int32_T)drum_bench_working_P.config.control0.mode_select) {
   case 1:
    drum_bench_working_B.modeswitch = drum_bench_working_B.BusCreator_n;
    break;

   case 2:
    drum_bench_working_B.modeswitch = drum_bench_working_B.BusAssignment;
    break;

   case 3:
    drum_bench_working_B.modeswitch = drum_bench_working_B.BusAssignment_n;
    break;

   default:
    drum_bench_working_B.modeswitch = drum_bench_working_B.BusCreator_n;
    break;
  }

  /* End of MultiPortSwitch: '<Root>/mode switch' */

  /* BusSelector: '<Root>/Bus Selector' */
  drum_bench_working_B.mode = drum_bench_working_B.modeswitch.mode;
  drum_bench_working_B.motor_torque_h =
    drum_bench_working_B.modeswitch.motor_torque;
  drum_bench_working_B.motor_iq_b = drum_bench_working_B.modeswitch.motor_iq;

  /* Clock: '<S19>/Clock1' */
  drum_bench_working_B.Clock1 = drum_bench_working_M->Timing.t[0];

  /* Product: '<S19>/Product' incorporates:
   *  Constant: '<S19>/deltaFreq'
   *  Constant: '<S19>/targetTime'
   */
  u = drum_bench_working_P.config.input.input0.analog.frequency - 0.1;
  u *= 6.2831853071795862;
  drum_bench_working_B.Product_j = u / 100.0;

  /* Gain: '<S19>/Gain' */
  drum_bench_working_B.Gain = 0.5 * drum_bench_working_B.Product_j;

  /* Product: '<S19>/Product1' */
  drum_bench_working_B.Product1 = drum_bench_working_B.Clock1 *
    drum_bench_working_B.Gain;

  /* Sum: '<S19>/Sum' incorporates:
   *  Constant: '<S19>/initialFreq'
   */
  drum_bench_working_B.Sum_j = drum_bench_working_B.Product1 +
    0.62831853071795862;

  /* Product: '<S19>/Product2' */
  drum_bench_working_B.Product2 = drum_bench_working_B.Clock1 *
    drum_bench_working_B.Sum_j;

  /* Trigonometry: '<S19>/Output' */
  drum_bench_working_B.Output = sin(drum_bench_working_B.Product2);

  /* Sin: '<S14>/Sine Wave' */
  drum_bench_working_B.SineWave = sin
    (drum_bench_working_P.config.input.input0.analog.frequency *
     drum_bench_working_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S14>/Sign' */
  u = drum_bench_working_B.SineWave;
  if (u < 0.0) {
    drum_bench_working_B.Sign = -1.0;
  } else if (u > 0.0) {
    drum_bench_working_B.Sign = 1.0;
  } else if (u == 0.0) {
    drum_bench_working_B.Sign = 0.0;
  } else {
    drum_bench_working_B.Sign = u;
  }

  /* End of Signum: '<S14>/Sign' */

  /* MultiPortSwitch: '<S14>/Multiport Switch' incorporates:
   *  Constant: '<S14>/Constant1'
   *  Constant: '<S14>/select input'
   */
  switch ((int32_T)drum_bench_working_P.config.input.input0.analog.select_input)
  {
   case 0:
    drum_bench_working_B.MultiportSwitch = 0.0;
    break;

   case 1:
    drum_bench_working_B.MultiportSwitch = drum_bench_working_B.SineWave;
    break;

   case 2:
    drum_bench_working_B.MultiportSwitch = drum_bench_working_B.Sign;
    break;

   case 3:
    drum_bench_working_B.MultiportSwitch = drum_bench_working_B.Output;
    break;

   default:
    drum_bench_working_B.MultiportSwitch = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S14>/Multiport Switch' */

  /* Gain: '<S14>/Gain' */
  drum_bench_working_B.Gain_a =
    drum_bench_working_P.config.input.input0.analog.amplitude * 0.0;

  /* Clock: '<S20>/Clock1' */
  drum_bench_working_B.Clock1_n = drum_bench_working_M->Timing.t[0];

  /* Product: '<S20>/Product' incorporates:
   *  Constant: '<S20>/deltaFreq'
   *  Constant: '<S20>/targetTime'
   */
  u = drum_bench_working_P.config.input.input0.motor_iq.frequency - 0.1;
  u *= 6.2831853071795862;
  drum_bench_working_B.Product_p = u / 100.0;

  /* Gain: '<S20>/Gain' */
  drum_bench_working_B.Gain_b = 0.5 * drum_bench_working_B.Product_p;

  /* Product: '<S20>/Product1' */
  drum_bench_working_B.Product1_o = drum_bench_working_B.Clock1_n *
    drum_bench_working_B.Gain_b;

  /* Sum: '<S20>/Sum' incorporates:
   *  Constant: '<S20>/initialFreq'
   */
  drum_bench_working_B.Sum_k = drum_bench_working_B.Product1_o +
    0.62831853071795862;

  /* Product: '<S20>/Product2' */
  drum_bench_working_B.Product2_p = drum_bench_working_B.Clock1_n *
    drum_bench_working_B.Sum_k;

  /* Trigonometry: '<S20>/Output' */
  drum_bench_working_B.Output_p = sin(drum_bench_working_B.Product2_p);

  /* Sin: '<S15>/Sine Wave' */
  drum_bench_working_B.SineWave_h = sin
    (drum_bench_working_P.config.input.input0.motor_iq.frequency *
     drum_bench_working_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S15>/Sign' */
  u = drum_bench_working_B.SineWave_h;
  if (u < 0.0) {
    drum_bench_working_B.Sign_a = -1.0;
  } else if (u > 0.0) {
    drum_bench_working_B.Sign_a = 1.0;
  } else if (u == 0.0) {
    drum_bench_working_B.Sign_a = 0.0;
  } else {
    drum_bench_working_B.Sign_a = u;
  }

  /* End of Signum: '<S15>/Sign' */

  /* MultiPortSwitch: '<S15>/Multiport Switch' incorporates:
   *  Constant: '<S15>/Constant1'
   *  Constant: '<S15>/select input'
   */
  switch ((int32_T)
          drum_bench_working_P.config.input.input0.motor_iq.select_input) {
   case 0:
    drum_bench_working_B.MultiportSwitch_e = 0.0;
    break;

   case 1:
    drum_bench_working_B.MultiportSwitch_e = drum_bench_working_B.SineWave_h;
    break;

   case 2:
    drum_bench_working_B.MultiportSwitch_e = drum_bench_working_B.Sign_a;
    break;

   case 3:
    drum_bench_working_B.MultiportSwitch_e = drum_bench_working_B.Output_p;
    break;

   default:
    drum_bench_working_B.MultiportSwitch_e = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S15>/Multiport Switch' */

  /* Gain: '<S15>/Gain' */
  drum_bench_working_B.Gain_bc =
    drum_bench_working_P.config.input.input0.motor_iq.amplitude * 0.0;

  /* Clock: '<S21>/Clock1' */
  drum_bench_working_B.Clock1_e = drum_bench_working_M->Timing.t[0];

  /* Product: '<S21>/Product' incorporates:
   *  Constant: '<S21>/deltaFreq'
   *  Constant: '<S21>/targetTime'
   */
  u = drum_bench_working_P.config.input.input0.motor_vq.frequency - 0.1;
  u *= 6.2831853071795862;
  drum_bench_working_B.Product_b = u / 100.0;

  /* Gain: '<S21>/Gain' */
  drum_bench_working_B.Gain_n = 0.5 * drum_bench_working_B.Product_b;

  /* Product: '<S21>/Product1' */
  drum_bench_working_B.Product1_c = drum_bench_working_B.Clock1_e *
    drum_bench_working_B.Gain_n;

  /* Sum: '<S21>/Sum' incorporates:
   *  Constant: '<S21>/initialFreq'
   */
  drum_bench_working_B.Sum_kn = drum_bench_working_B.Product1_c +
    0.62831853071795862;

  /* Product: '<S21>/Product2' */
  drum_bench_working_B.Product2_n = drum_bench_working_B.Clock1_e *
    drum_bench_working_B.Sum_kn;

  /* Trigonometry: '<S21>/Output' */
  drum_bench_working_B.Output_n = sin(drum_bench_working_B.Product2_n);

  /* Sin: '<S16>/Sine Wave' */
  drum_bench_working_B.SineWave_f = sin
    (drum_bench_working_P.config.input.input0.motor_vq.frequency *
     drum_bench_working_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S16>/Sign' */
  u = drum_bench_working_B.SineWave_f;
  if (u < 0.0) {
    drum_bench_working_B.Sign_m = -1.0;
  } else if (u > 0.0) {
    drum_bench_working_B.Sign_m = 1.0;
  } else if (u == 0.0) {
    drum_bench_working_B.Sign_m = 0.0;
  } else {
    drum_bench_working_B.Sign_m = u;
  }

  /* End of Signum: '<S16>/Sign' */

  /* MultiPortSwitch: '<S16>/Multiport Switch' incorporates:
   *  Constant: '<S16>/Constant1'
   *  Constant: '<S16>/select input'
   */
  switch ((int32_T)
          drum_bench_working_P.config.input.input0.motor_vq.select_input) {
   case 0:
    drum_bench_working_B.MultiportSwitch_l = 0.0;
    break;

   case 1:
    drum_bench_working_B.MultiportSwitch_l = drum_bench_working_B.SineWave_f;
    break;

   case 2:
    drum_bench_working_B.MultiportSwitch_l = drum_bench_working_B.Sign_m;
    break;

   case 3:
    drum_bench_working_B.MultiportSwitch_l = drum_bench_working_B.Output_n;
    break;

   default:
    drum_bench_working_B.MultiportSwitch_l = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S16>/Multiport Switch' */

  /* Gain: '<S16>/Gain' */
  drum_bench_working_B.Gain_l =
    drum_bench_working_P.config.input.input0.motor_vq.amplitude * 0.0;

  /* Clock: '<S22>/Clock1' */
  drum_bench_working_B.Clock1_g = drum_bench_working_M->Timing.t[0];

  /* Product: '<S22>/Product' incorporates:
   *  Constant: '<S22>/deltaFreq'
   *  Constant: '<S22>/targetTime'
   */
  u = drum_bench_working_P.config.input.input0.quadrature_1_angle.frequency -
    0.1;
  u *= 6.2831853071795862;
  drum_bench_working_B.Product_d = u / 100.0;

  /* Gain: '<S22>/Gain' */
  drum_bench_working_B.Gain_nc = 0.5 * drum_bench_working_B.Product_d;

  /* Product: '<S22>/Product1' */
  drum_bench_working_B.Product1_i = drum_bench_working_B.Clock1_g *
    drum_bench_working_B.Gain_nc;

  /* Sum: '<S22>/Sum' incorporates:
   *  Constant: '<S22>/initialFreq'
   */
  drum_bench_working_B.Sum_i = drum_bench_working_B.Product1_i +
    0.62831853071795862;

  /* Product: '<S22>/Product2' */
  drum_bench_working_B.Product2_i = drum_bench_working_B.Clock1_g *
    drum_bench_working_B.Sum_i;

  /* Trigonometry: '<S22>/Output' */
  drum_bench_working_B.Output_h = sin(drum_bench_working_B.Product2_i);

  /* Sin: '<S17>/Sine Wave' */
  drum_bench_working_B.SineWave_l = sin
    (drum_bench_working_P.config.input.input0.quadrature_1_angle.frequency *
     drum_bench_working_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S17>/Sign' */
  u = drum_bench_working_B.SineWave_l;
  if (u < 0.0) {
    drum_bench_working_B.Sign_f = -1.0;
  } else if (u > 0.0) {
    drum_bench_working_B.Sign_f = 1.0;
  } else if (u == 0.0) {
    drum_bench_working_B.Sign_f = 0.0;
  } else {
    drum_bench_working_B.Sign_f = u;
  }

  /* End of Signum: '<S17>/Sign' */

  /* MultiPortSwitch: '<S17>/Multiport Switch' incorporates:
   *  Constant: '<S17>/Constant1'
   *  Constant: '<S17>/select input'
   */
  switch ((int32_T)
          drum_bench_working_P.config.input.input0.quadrature_1_angle.select_input)
  {
   case 0:
    drum_bench_working_B.MultiportSwitch_n = 0.0;
    break;

   case 1:
    drum_bench_working_B.MultiportSwitch_n = drum_bench_working_B.SineWave_l;
    break;

   case 2:
    drum_bench_working_B.MultiportSwitch_n = drum_bench_working_B.Sign_f;
    break;

   case 3:
    drum_bench_working_B.MultiportSwitch_n = drum_bench_working_B.Output_h;
    break;

   default:
    drum_bench_working_B.MultiportSwitch_n = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S17>/Multiport Switch' */

  /* Gain: '<S17>/Gain' */
  drum_bench_working_B.Gain_f =
    drum_bench_working_P.config.input.input0.quadrature_1_angle.amplitude * 0.0;

  /* Clock: '<S23>/Clock1' */
  drum_bench_working_B.Clock1_j = drum_bench_working_M->Timing.t[0];

  /* Product: '<S23>/Product' incorporates:
   *  Constant: '<S23>/deltaFreq'
   *  Constant: '<S23>/targetTime'
   */
  u = drum_bench_working_P.config.input.input0.quadrature_2_angle.frequency -
    0.1;
  u *= 6.2831853071795862;
  drum_bench_working_B.Product_h = u / 100.0;

  /* Gain: '<S23>/Gain' */
  drum_bench_working_B.Gain_nz = 0.5 * drum_bench_working_B.Product_h;

  /* Product: '<S23>/Product1' */
  drum_bench_working_B.Product1_f = drum_bench_working_B.Clock1_j *
    drum_bench_working_B.Gain_nz;

  /* Sum: '<S23>/Sum' incorporates:
   *  Constant: '<S23>/initialFreq'
   */
  drum_bench_working_B.Sum_b = drum_bench_working_B.Product1_f +
    0.62831853071795862;

  /* Product: '<S23>/Product2' */
  drum_bench_working_B.Product2_l = drum_bench_working_B.Clock1_j *
    drum_bench_working_B.Sum_b;

  /* Trigonometry: '<S23>/Output' */
  drum_bench_working_B.Output_c = sin(drum_bench_working_B.Product2_l);

  /* Sin: '<S18>/Sine Wave' */
  drum_bench_working_B.SineWave_n = sin
    (drum_bench_working_P.config.input.input0.quadrature_2_angle.frequency *
     drum_bench_working_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S18>/Sign' */
  u = drum_bench_working_B.SineWave_n;
  if (u < 0.0) {
    drum_bench_working_B.Sign_i = -1.0;
  } else if (u > 0.0) {
    drum_bench_working_B.Sign_i = 1.0;
  } else if (u == 0.0) {
    drum_bench_working_B.Sign_i = 0.0;
  } else {
    drum_bench_working_B.Sign_i = u;
  }

  /* End of Signum: '<S18>/Sign' */

  /* MultiPortSwitch: '<S18>/Multiport Switch' incorporates:
   *  Constant: '<S18>/Constant1'
   *  Constant: '<S18>/select input'
   */
  switch ((int32_T)
          drum_bench_working_P.config.input.input0.quadrature_2_angle.select_input)
  {
   case 0:
    drum_bench_working_B.MultiportSwitch_b = 0.0;
    break;

   case 1:
    drum_bench_working_B.MultiportSwitch_b = drum_bench_working_B.SineWave_n;
    break;

   case 2:
    drum_bench_working_B.MultiportSwitch_b = drum_bench_working_B.Sign_i;
    break;

   case 3:
    drum_bench_working_B.MultiportSwitch_b = drum_bench_working_B.Output_c;
    break;

   default:
    drum_bench_working_B.MultiportSwitch_b = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S18>/Multiport Switch' */

  /* Gain: '<S18>/Gain' */
  drum_bench_working_B.Gain_g =
    drum_bench_working_P.config.input.input0.quadrature_2_angle.amplitude * 0.0;

  /* BusSelector: '<S7>/Bus Selector' */
  drum_bench_working_B.rpc = drum_bench_working_B.modeswitch.rpc;

  /* UnitDelay: '<S65>/Output' */
  drum_bench_working_B.Output_o = drum_bench_working_DW.Output_DSTATE;

  /* Sum: '<S66>/FixPt Sum1' incorporates:
   *  Constant: '<S66>/FixPt Constant'
   */
  drum_bench_working_B.FixPtSum1 = drum_bench_working_B.Output_o + 1U;

  /* Switch: '<S67>/FixPt Switch' incorporates:
   *  Constant: '<S67>/Constant'
   */
  if (drum_bench_working_B.FixPtSum1 > 4294967295U) {
    drum_bench_working_B.FixPtSwitch = 0U;
  } else {
    drum_bench_working_B.FixPtSwitch = drum_bench_working_B.FixPtSum1;
  }

  /* End of Switch: '<S67>/FixPt Switch' */

  /* DataTypeConversion: '<S64>/debug_' */
  drum_bench_working_B.debug = drum_bench_working_B.marlin_ec_o21;

  /* BusSelector: '<S61>/Bus Selector' */
  drum_bench_working_B.mode_k = drum_bench_working_B.modeswitch.mode;

  /* BusSelector: '<S61>/Bus Selector1' */
  drum_bench_working_B.motor_iq_k = drum_bench_working_B.modeswitch.motor_iq;
  drum_bench_working_B.motor_id = drum_bench_working_B.modeswitch.motor_id;

  /* BusSelector: '<S61>/Bus Selector2' */
  drum_bench_working_B.motor_vq = drum_bench_working_B.modeswitch.motor_vq;
  drum_bench_working_B.motor_vd = drum_bench_working_B.modeswitch.motor_vd;

  /* BusSelector: '<S61>/Bus Selector3' */
  drum_bench_working_B.analog_diff_g =
    drum_bench_working_B.modeswitch.analog_diff;
  drum_bench_working_B.analog_diff_dot_o =
    drum_bench_working_B.modeswitch.analog_diff_dot;

  /* BusSelector: '<S61>/Bus Selector4' */
  drum_bench_working_B.quadrature_1_angle_b =
    drum_bench_working_B.modeswitch.quadrature_1_angle;
  drum_bench_working_B.quadrature_1_angle_dot_j =
    drum_bench_working_B.modeswitch.quadrature_1_angle_dot;

  /* BusSelector: '<S61>/Bus Selector5' */
  drum_bench_working_B.motor_torque_mv =
    drum_bench_working_B.modeswitch.motor_torque;

  /* MultiPortSwitch: '<S61>/Multiport Switch' incorporates:
   *  Constant: '<S68>/mode'
   *  Constant: '<S69>/mode'
   *  Constant: '<S70>/mode'
   *  Constant: '<S71>/mode'
   *  Constant: '<S72>/mode'
   *  Constant: '<S73>/mode'
   *  Constant: '<S74>/mode'
   *  Constant: '<S75>/mode'
   */
  switch ((int32_T)drum_bench_working_B.mode_k) {
   case 0:
    drum_bench_working_B.mode_a = 0U;
    break;

   case 1:
    drum_bench_working_B.mode_a = 1U;
    break;

   case 2:
    drum_bench_working_B.mode_a = 2U;
    break;

   case 3:
    drum_bench_working_B.mode_a = 3U;
    break;

   case 4:
    drum_bench_working_B.mode_a = 4U;
    break;

   case 5:
    drum_bench_working_B.mode_a = 5U;
    break;

   case 6:
    drum_bench_working_B.mode_a = 6U;
    break;

   case 7:
    drum_bench_working_B.mode_a = 8U;
    break;

   default:
    drum_bench_working_B.mode_a = 0U;
    break;
  }

  for (i = 0; i < 14; i++) {
    /* DataTypeConversion: '<S72>/Data Type Conversion1' incorporates:
     *  Constant: '<S72>/filler'
     */
    drum_bench_working_B.DataTypeConversion1[i] = (real32_T)
      drum_bench_working_ConstP.pooled16[i];

    /* DataTypeConversion: '<S69>/Data Type Conversion1' incorporates:
     *  Constant: '<S69>/filler'
     */
    drum_bench_working_B.DataTypeConversion1_o[i] = (real32_T)
      drum_bench_working_ConstP.pooled16[i];
  }

  /* End of MultiPortSwitch: '<S61>/Multiport Switch' */

  /* Pack: <S72>/Byte Pack3 */
  (void) memcpy(&drum_bench_working_B.command[0],
                &drum_bench_working_B.DataTypeConversion1[0],
                56);

  /* Pack: <S69>/Byte Pack3 */
  (void) memcpy(&drum_bench_working_B.command_j[0],
                &drum_bench_working_B.DataTypeConversion1_o[0],
                56);

  /* DataTypeConversion: '<S73>/Data Type Conversion1' incorporates:
   *  Constant: '<S61>/lock_current'
   *  Constant: '<S73>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_oj[0] = (real32_T)
    drum_bench_working_P.config.marlins.M0.motor.phase_lock_current;
  for (i = 0; i < 13; i++) {
    drum_bench_working_B.DataTypeConversion1_oj[i + 1] = (real32_T)
      drum_bench_working_ConstP.pooled17[i];
  }

  /* End of DataTypeConversion: '<S73>/Data Type Conversion1' */

  /* Pack: <S73>/Byte Pack3 */
  (void) memcpy(&drum_bench_working_B.command_k[0],
                &drum_bench_working_B.DataTypeConversion1_oj[0],
                56);

  /* DataTypeConversion: '<S70>/Data Type Conversion1' incorporates:
   *  Constant: '<S70>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_b[0] = (real32_T)
    drum_bench_working_B.motor_iq_k;
  drum_bench_working_B.DataTypeConversion1_b[1] = (real32_T)
    drum_bench_working_B.motor_id;
  for (i = 0; i < 12; i++) {
    drum_bench_working_B.DataTypeConversion1_b[i + 2] = (real32_T)
      drum_bench_working_ConstP.pooled18[i];
  }

  /* End of DataTypeConversion: '<S70>/Data Type Conversion1' */

  /* Pack: <S70>/Byte Pack2 */
  (void) memcpy(&drum_bench_working_B.command_l[0],
                &drum_bench_working_B.DataTypeConversion1_b[0],
                56);

  /* DataTypeConversion: '<S75>/Data Type Conversion1' incorporates:
   *  Constant: '<S75>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_e[0] = (real32_T)
    drum_bench_working_B.motor_vq;
  drum_bench_working_B.DataTypeConversion1_e[1] = (real32_T)
    drum_bench_working_B.motor_vd;
  for (i = 0; i < 12; i++) {
    drum_bench_working_B.DataTypeConversion1_e[i + 2] = (real32_T)
      drum_bench_working_ConstP.pooled18[i];
  }

  /* End of DataTypeConversion: '<S75>/Data Type Conversion1' */

  /* Pack: <S75>/Byte Pack2 */
  (void) memcpy(&drum_bench_working_B.command_f[0],
                &drum_bench_working_B.DataTypeConversion1_e[0],
                56);

  /* DataTypeConversion: '<S68>/Data Type Conversion1' incorporates:
   *  Constant: '<S68>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_f[0] = (real32_T)
    drum_bench_working_B.analog_diff_g;
  drum_bench_working_B.DataTypeConversion1_f[1] = (real32_T)
    drum_bench_working_B.analog_diff_dot_o;
  for (i = 0; i < 12; i++) {
    drum_bench_working_B.DataTypeConversion1_f[i + 2] = (real32_T)
      drum_bench_working_ConstP.pooled18[i];
  }

  /* End of DataTypeConversion: '<S68>/Data Type Conversion1' */

  /* Pack: <S68>/Byte Pack2 */
  (void) memcpy(&drum_bench_working_B.command_g[0],
                &drum_bench_working_B.DataTypeConversion1_f[0],
                56);

  /* DataTypeConversion: '<S74>/Data Type Conversion1' incorporates:
   *  Constant: '<S74>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_fm[0] = (real32_T)
    drum_bench_working_B.quadrature_1_angle_b;
  drum_bench_working_B.DataTypeConversion1_fm[1] = (real32_T)
    drum_bench_working_B.quadrature_1_angle_dot_j;
  for (i = 0; i < 12; i++) {
    drum_bench_working_B.DataTypeConversion1_fm[i + 2] = (real32_T)
      drum_bench_working_ConstP.pooled18[i];
  }

  /* End of DataTypeConversion: '<S74>/Data Type Conversion1' */

  /* Pack: <S74>/Byte Pack2 */
  (void) memcpy(&drum_bench_working_B.command_gi[0],
                &drum_bench_working_B.DataTypeConversion1_fm[0],
                56);

  /* DataTypeConversion: '<S71>/Data Type Conversion1' incorporates:
   *  Constant: '<S71>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_h[0] = (real32_T)
    drum_bench_working_B.motor_torque_mv;
  for (i = 0; i < 13; i++) {
    drum_bench_working_B.DataTypeConversion1_h[i + 1] = (real32_T)
      drum_bench_working_ConstP.pooled17[i];
  }

  /* End of DataTypeConversion: '<S71>/Data Type Conversion1' */

  /* Pack: <S71>/Byte Pack2 */
  (void) memcpy(&drum_bench_working_B.command_a[0],
                &drum_bench_working_B.DataTypeConversion1_h[0],
                56);

  /* MultiPortSwitch: '<S61>/Multiport Switch' */
  switch ((int32_T)drum_bench_working_B.mode_k) {
   case 0:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_e[i] = drum_bench_working_B.command[i];
    }
    break;

   case 1:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_e[i] = drum_bench_working_B.command_j[i];
    }
    break;

   case 2:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_e[i] = drum_bench_working_B.command_k[i];
    }
    break;

   case 3:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_e[i] = drum_bench_working_B.command_l[i];
    }
    break;

   case 4:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_e[i] = drum_bench_working_B.command_f[i];
    }
    break;

   case 5:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_e[i] = drum_bench_working_B.command_g[i];
    }
    break;

   case 6:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_e[i] = drum_bench_working_B.command_gi[i];
    }
    break;

   case 7:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_e[i] = drum_bench_working_B.command_a[i];
    }
    break;

   default:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_e[i] = drum_bench_working_B.command[i];
    }
    break;
  }

  /* End of MultiPortSwitch: '<S61>/Multiport Switch' */

  /* BusCreator: '<S76>/Bus Creator' incorporates:
   *  Constant: '<S76>/id'
   *  Constant: '<S76>/type'
   */
  drum_bench_working_B.BusCreator_nf.type = 6;
  drum_bench_working_B.BusCreator_nf.id = 0.0F;

  /* BusCreator: '<S77>/Bus Creator' incorporates:
   *  Constant: '<S77>/id'
   *  Constant: '<S77>/type'
   */
  drum_bench_working_B.BusCreator_k.type = 7;
  drum_bench_working_B.BusCreator_k.id = 1.0F;

  /* BusCreator: '<S78>/Bus Creator' incorporates:
   *  Constant: '<S78>/id'
   *  Constant: '<S78>/type'
   */
  drum_bench_working_B.BusCreator_iq.type = 7;
  drum_bench_working_B.BusCreator_iq.id = 0.0F;

  /* BusCreator: '<S79>/Bus Creator' incorporates:
   *  Constant: '<S79>/id'
   *  Constant: '<S79>/type'
   */
  drum_bench_working_B.BusCreator_o.type = 7;
  drum_bench_working_B.BusCreator_o.id = 0.0F;

  /* BusCreator: '<S80>/Bus Creator' incorporates:
   *  Constant: '<S80>/id'
   *  Constant: '<S80>/type'
   */
  drum_bench_working_B.BusCreator_l.type = 7;
  drum_bench_working_B.BusCreator_l.id = 0.0F;
  for (i = 0; i < 20; i++) {
    /* BusCreator: '<S76>/Bus Creator' */
    drum_bench_working_B.BusCreator_nf.data[i] =
      drum_bench_working_ConstB.data[i];

    /* BusCreator: '<S77>/Bus Creator' */
    drum_bench_working_B.BusCreator_k.data[i] =
      drum_bench_working_ConstB.data_p[i];

    /* BusCreator: '<S78>/Bus Creator' */
    drum_bench_working_B.BusCreator_iq.data[i] =
      drum_bench_working_ConstB.data_f[i];

    /* BusCreator: '<S79>/Bus Creator' */
    drum_bench_working_B.BusCreator_o.data[i] =
      drum_bench_working_ConstB.data_c[i];
    drum_bench_working_B.BusCreator_l.data[i] =
      drum_bench_working_ConstB.data_h[i];
  }

  /* End of BusCreator: '<S80>/Bus Creator' */

  /* Constant: '<S99>/id' */
  drum_bench_working_B.VectorConcatenate_d[0] =
    drum_bench_working_P.config.marlins.M0.amp.current_zero_a;

  /* Constant: '<S99>/id1' */
  drum_bench_working_B.VectorConcatenate_d[1] =
    drum_bench_working_P.config.marlins.M0.amp.current_zero_b;

  /* Constant: '<S99>/id2' */
  drum_bench_working_B.VectorConcatenate_d[2] =
    drum_bench_working_P.config.marlins.M0.amp.deadband;

  /* Constant: '<S99>/id3' */
  drum_bench_working_B.VectorConcatenate_d[3] =
    drum_bench_working_P.config.marlins.M0.amp.led_brightness;

  /* Constant: '<S99>/id4' */
  drum_bench_working_B.VectorConcatenate_d[4] =
    drum_bench_working_P.config.marlins.M0.amp.nominal_bus_voltage;

  /* Constant: '<S99>/id5' */
  drum_bench_working_B.VectorConcatenate_d[5] =
    drum_bench_working_P.config.marlins.M0.amp.current_sensor_limit;

  /* Constant: '<S113>/id' */
  drum_bench_working_B.VectorConcatenate_d[6] =
    drum_bench_working_P.config.marlins.M0.amp.logger_10kHz.trigger_level;

  /* Constant: '<S112>/id' */
  drum_bench_working_B.VectorConcatenate_d[7] =
    drum_bench_working_P.config.marlins.M0.amp.logger_100kHz.trigger_level;

  /* Constant: '<S105>/id' */
  drum_bench_working_B.VectorConcatenate_d[8] =
    drum_bench_working_P.config.marlins.M0.control_current.current_filter_bandwidth;

  /* Constant: '<S105>/id1' */
  drum_bench_working_B.VectorConcatenate_d[9] =
    drum_bench_working_P.config.marlins.M0.control_current.fingerprint_gain;

  /* Constant: '<S105>/id2' */
  drum_bench_working_B.VectorConcatenate_d[10] =
    drum_bench_working_P.config.marlins.M0.control_current.ki;

  /* Constant: '<S105>/id3' */
  drum_bench_working_B.VectorConcatenate_d[11] =
    drum_bench_working_P.config.marlins.M0.control_current.ki_limit;

  /* Constant: '<S105>/id4' */
  drum_bench_working_B.VectorConcatenate_d[12] =
    drum_bench_working_P.config.marlins.M0.control_current.kp;

  /* Constant: '<S119>/id' */
  drum_bench_working_B.VectorConcatenate_d[13] =
    drum_bench_working_P.config.marlins.M0.control_motor_angle.pid_deadband.deadband;

  /* Constant: '<S120>/id' */
  drum_bench_working_B.VectorConcatenate_d[14] =
    drum_bench_working_P.config.marlins.M0.control_motor_angle.pid_interpolate.enabled;

  /* Constant: '<S118>/id' */
  drum_bench_working_B.VectorConcatenate_d[15] =
    drum_bench_working_P.config.marlins.M0.control_motor_angle.pid.kd;

  /* Constant: '<S118>/id1' */
  drum_bench_working_B.VectorConcatenate_d[16] =
    drum_bench_working_P.config.marlins.M0.control_motor_angle.pid.ki;

  /* Constant: '<S118>/id2' */
  drum_bench_working_B.VectorConcatenate_d[17] =
    drum_bench_working_P.config.marlins.M0.control_motor_angle.pid.ki_limit;

  /* Constant: '<S118>/id3' */
  drum_bench_working_B.VectorConcatenate_d[18] =
    drum_bench_working_P.config.marlins.M0.control_motor_angle.pid.kp;

  /* Constant: '<S121>/id' */
  drum_bench_working_B.VectorConcatenate_d[19] =
    drum_bench_working_P.config.marlins.M0.control_quadrature_2_angle.pid.kd;

  /* Constant: '<S121>/id1' */
  drum_bench_working_B.VectorConcatenate_d[20] =
    drum_bench_working_P.config.marlins.M0.control_quadrature_2_angle.pid.ki;

  /* Constant: '<S121>/id2' */
  drum_bench_working_B.VectorConcatenate_d[21] =
    drum_bench_working_P.config.marlins.M0.control_quadrature_2_angle.pid.ki_limit;

  /* Constant: '<S121>/id3' */
  drum_bench_working_B.VectorConcatenate_d[22] =
    drum_bench_working_P.config.marlins.M0.control_quadrature_2_angle.pid.kp;

  /* Constant: '<S117>/id' */
  drum_bench_working_B.VectorConcatenate_d[23] =
    drum_bench_working_P.config.marlins.M0.control_analog.pid.kd;

  /* Constant: '<S117>/id1' */
  drum_bench_working_B.VectorConcatenate_d[24] =
    drum_bench_working_P.config.marlins.M0.control_analog.pid.ki;

  /* Constant: '<S117>/id2' */
  drum_bench_working_B.VectorConcatenate_d[25] =
    drum_bench_working_P.config.marlins.M0.control_analog.pid.ki_limit;

  /* Constant: '<S117>/id3' */
  drum_bench_working_B.VectorConcatenate_d[26] =
    drum_bench_working_P.config.marlins.M0.control_analog.pid.kp;

  /* Constant: '<S104>/id' */
  drum_bench_working_B.VectorConcatenate_d[27] =
    drum_bench_working_P.config.marlins.M0.control_analog.source;

  /* Constant: '<S108>/id' */
  drum_bench_working_B.VectorConcatenate_d[28] =
    drum_bench_working_P.config.marlins.M0.motor.current_limit;

  /* Constant: '<S108>/id1' */
  drum_bench_working_B.VectorConcatenate_d[29] =
    drum_bench_working_P.config.marlins.M0.motor.voltage_limit;

  /* Constant: '<S108>/id2' */
  drum_bench_working_B.VectorConcatenate_d[30] =
    drum_bench_working_P.config.marlins.M0.motor.encoder.counts_per_revolution;

  /* Constant: '<S108>/id3' */
  drum_bench_working_B.VectorConcatenate_d[31] =
    drum_bench_working_P.config.marlins.M0.motor.km;

  /* Constant: '<S108>/id4' */
  drum_bench_working_B.VectorConcatenate_d[32] =
    drum_bench_working_P.config.marlins.M0.motor.num_poles;

  /* Constant: '<S108>/id5' */
  drum_bench_working_B.VectorConcatenate_d[33] =
    drum_bench_working_P.config.marlins.M0.motor.resistance;

  /* Constant: '<S108>/id6' */
  drum_bench_working_B.VectorConcatenate_d[34] =
    drum_bench_working_P.config.marlins.M0.motor.electrical_offset;

  /* Constant: '<S108>/id7' */
  drum_bench_working_B.VectorConcatenate_d[35] =
    drum_bench_working_P.config.marlins.M0.motor.v_per_a;

  /* Constant: '<S103>/id' */
  drum_bench_working_B.VectorConcatenate_d[36] =
    drum_bench_working_P.config.marlins.M0.bridge.thermal_model.eA1;

  /* Constant: '<S103>/id1' */
  drum_bench_working_B.VectorConcatenate_d[37] =
    drum_bench_working_P.config.marlins.M0.bridge.thermal_model.eA2;

  /* Constant: '<S122>/id' */
  drum_bench_working_B.VectorConcatenate_d[38] =
    drum_bench_working_P.config.marlins.M0.quadrature_1.sensor.bias;

  /* Constant: '<S122>/id1' */
  drum_bench_working_B.VectorConcatenate_d[39] =
    drum_bench_working_P.config.marlins.M0.quadrature_1.sensor.gain;

  /* Constant: '<S122>/id6' */
  drum_bench_working_B.VectorConcatenate_d[40] =
    drum_bench_working_P.config.marlins.M0.quadrature_1.sensor.limit_high;

  /* Constant: '<S122>/id3' */
  drum_bench_working_B.VectorConcatenate_d[41] =
    drum_bench_working_P.config.marlins.M0.quadrature_1.sensor.limit_low;

  /* Constant: '<S122>/id4' */
  drum_bench_working_B.VectorConcatenate_d[42] =
    drum_bench_working_P.config.marlins.M0.quadrature_1.sensor.filter_bandwidth;

  /* Constant: '<S122>/id5' */
  drum_bench_working_B.VectorConcatenate_d[43] =
    drum_bench_working_P.config.marlins.M0.quadrature_1.sensor.dot_filter_bandwidth;

  /* Constant: '<S109>/id' */
  drum_bench_working_B.VectorConcatenate_d[44] =
    drum_bench_working_P.config.marlins.M0.quadrature_1.enable_index;

  /* Constant: '<S123>/id' */
  drum_bench_working_B.VectorConcatenate_d[45] =
    drum_bench_working_P.config.marlins.M0.quadrature_2.sensor.bias;

  /* Constant: '<S123>/id1' */
  drum_bench_working_B.VectorConcatenate_d[46] =
    drum_bench_working_P.config.marlins.M0.quadrature_2.sensor.gain;

  /* Constant: '<S123>/id6' */
  drum_bench_working_B.VectorConcatenate_d[47] =
    drum_bench_working_P.config.marlins.M0.quadrature_2.sensor.limit_high;

  /* Constant: '<S123>/id3' */
  drum_bench_working_B.VectorConcatenate_d[48] =
    drum_bench_working_P.config.marlins.M0.quadrature_2.sensor.limit_low;

  /* Constant: '<S123>/id4' */
  drum_bench_working_B.VectorConcatenate_d[49] =
    drum_bench_working_P.config.marlins.M0.quadrature_2.sensor.filter_bandwidth;

  /* Constant: '<S123>/id5' */
  drum_bench_working_B.VectorConcatenate_d[50] =
    drum_bench_working_P.config.marlins.M0.quadrature_2.sensor.dot_filter_bandwidth;

  /* Constant: '<S110>/id' */
  drum_bench_working_B.VectorConcatenate_d[51] =
    drum_bench_working_P.config.marlins.M0.quadrature_2.enable_index;

  /* Constant: '<S114>/id' */
  drum_bench_working_B.VectorConcatenate_d[52] =
    drum_bench_working_P.config.marlins.M0.analog_1.sensor.bias;

  /* Constant: '<S114>/id1' */
  drum_bench_working_B.VectorConcatenate_d[53] =
    drum_bench_working_P.config.marlins.M0.analog_1.sensor.gain;

  /* Constant: '<S114>/id6' */
  drum_bench_working_B.VectorConcatenate_d[54] =
    drum_bench_working_P.config.marlins.M0.analog_1.sensor.limit_high;

  /* Constant: '<S114>/id3' */
  drum_bench_working_B.VectorConcatenate_d[55] =
    drum_bench_working_P.config.marlins.M0.analog_1.sensor.limit_low;

  /* Constant: '<S114>/id4' */
  drum_bench_working_B.VectorConcatenate_d[56] =
    drum_bench_working_P.config.marlins.M0.analog_1.sensor.filter_bandwidth;

  /* Constant: '<S114>/id5' */
  drum_bench_working_B.VectorConcatenate_d[57] =
    drum_bench_working_P.config.marlins.M0.analog_1.sensor.dot_filter_bandwidth;

  /* Constant: '<S115>/id' */
  drum_bench_working_B.VectorConcatenate_d[58] =
    drum_bench_working_P.config.marlins.M0.analog_2.sensor.bias;

  /* Constant: '<S115>/id1' */
  drum_bench_working_B.VectorConcatenate_d[59] =
    drum_bench_working_P.config.marlins.M0.analog_2.sensor.gain;

  /* Constant: '<S115>/id6' */
  drum_bench_working_B.VectorConcatenate_d[60] =
    drum_bench_working_P.config.marlins.M0.analog_2.sensor.limit_high;

  /* Constant: '<S115>/id3' */
  drum_bench_working_B.VectorConcatenate_d[61] =
    drum_bench_working_P.config.marlins.M0.analog_2.sensor.limit_low;

  /* Constant: '<S115>/id4' */
  drum_bench_working_B.VectorConcatenate_d[62] =
    drum_bench_working_P.config.marlins.M0.analog_2.sensor.filter_bandwidth;

  /* Constant: '<S115>/id5' */
  drum_bench_working_B.VectorConcatenate_d[63] =
    drum_bench_working_P.config.marlins.M0.analog_2.sensor.dot_filter_bandwidth;

  /* Constant: '<S116>/id' */
  drum_bench_working_B.VectorConcatenate_d[64] =
    drum_bench_working_P.config.marlins.M0.analog_diff.sensor.bias;

  /* Constant: '<S116>/id1' */
  drum_bench_working_B.VectorConcatenate_d[65] =
    drum_bench_working_P.config.marlins.M0.analog_diff.sensor.gain;

  /* Constant: '<S116>/id6' */
  drum_bench_working_B.VectorConcatenate_d[66] =
    drum_bench_working_P.config.marlins.M0.analog_diff.sensor.limit_high;

  /* Constant: '<S116>/id3' */
  drum_bench_working_B.VectorConcatenate_d[67] =
    drum_bench_working_P.config.marlins.M0.analog_diff.sensor.limit_low;

  /* Constant: '<S116>/id4' */
  drum_bench_working_B.VectorConcatenate_d[68] =
    drum_bench_working_P.config.marlins.M0.analog_diff.sensor.filter_bandwidth;

  /* Constant: '<S116>/id5' */
  drum_bench_working_B.VectorConcatenate_d[69] =
    drum_bench_working_P.config.marlins.M0.analog_diff.sensor.dot_filter_bandwidth;

  /* Constant: '<S124>/id' */
  drum_bench_working_B.VectorConcatenate_d[70] =
    drum_bench_working_P.config.marlins.M0.ssi.sensor.bias;

  /* Constant: '<S124>/id1' */
  drum_bench_working_B.VectorConcatenate_d[71] =
    drum_bench_working_P.config.marlins.M0.ssi.sensor.gain;

  /* Constant: '<S124>/id6' */
  drum_bench_working_B.VectorConcatenate_d[72] =
    drum_bench_working_P.config.marlins.M0.ssi.sensor.limit_high;

  /* Constant: '<S124>/id3' */
  drum_bench_working_B.VectorConcatenate_d[73] =
    drum_bench_working_P.config.marlins.M0.ssi.sensor.limit_low;

  /* Constant: '<S124>/id4' */
  drum_bench_working_B.VectorConcatenate_d[74] =
    drum_bench_working_P.config.marlins.M0.ssi.sensor.filter_bandwidth;

  /* Constant: '<S124>/id5' */
  drum_bench_working_B.VectorConcatenate_d[75] =
    drum_bench_working_P.config.marlins.M0.ssi.sensor.dot_filter_bandwidth;

  /* DataTypeConversion: '<S97>/Data Type Conversion' */
  for (i = 0; i < 76; i++) {
    drum_bench_working_B.DataTypeConversion_f[i] = (real32_T)
      drum_bench_working_B.VectorConcatenate_d[i];
  }

  /* End of DataTypeConversion: '<S97>/Data Type Conversion' */

  /* Unpack: <S63>/Byte Unpack */
  (void) memcpy(&drum_bench_working_B.type, &drum_bench_working_B.rpa_packet[0],
                4);
  (void) memcpy(&drum_bench_working_B.id, &drum_bench_working_B.rpa_packet[4],
                4);
  (void) memcpy(&drum_bench_working_B.data[0], &drum_bench_working_B.rpa_packet
                [8],
                20);

  /* BusCreator: '<S96>/BusConversion_InsertedFor_MATLAB Function1_at_inport_1' */
  drum_bench_working_B.BusConversion_InsertedFor_MATLA.type =
    drum_bench_working_B.type;
  drum_bench_working_B.BusConversion_InsertedFor_MATLA.id =
    drum_bench_working_B.id;
  for (i = 0; i < 20; i++) {
    drum_bench_working_B.BusConversion_InsertedFor_MATLA.data[i] =
      drum_bench_working_B.data[i];
  }

  /* End of BusCreator: '<S96>/BusConversion_InsertedFor_MATLAB Function1_at_inport_1' */

  /* MATLAB Function: '<S96>/MATLAB Function1' incorporates:
   *  Constant: '<S96>/Constant2'
   */
  drum_bench_work_MATLABFunction1(drum_bench_working_B.DataTypeConversion_f,
    &drum_bench_working_B.BusConversion_InsertedFor_MATLA, 1U,
    &drum_bench_working_B.sf_MATLABFunction1,
    &drum_bench_working_DW.sf_MATLABFunction1);

  /* Pack: <S96>/Byte Pack */
  (void) memcpy(&drum_bench_working_B.data_m[0],
                &drum_bench_working_ConstP.pooled30,
                4);
  (void) memcpy(&drum_bench_working_B.data_m[4],
                &drum_bench_working_B.sf_MATLABFunction1.index,
                4);
  (void) memcpy(&drum_bench_working_B.data_m[8],
                &drum_bench_working_B.sf_MATLABFunction1.value,
                4);
  (void) memcpy(&drum_bench_working_B.data_m[12],
                drum_bench_working_ConstP.pooled39,
                8);

  /* BusCreator: '<S96>/Bus Creator' */
  drum_bench_working_B.BusCreator_a.type =
    drum_bench_working_B.sf_MATLABFunction1.type;
  drum_bench_working_B.BusCreator_a.id =
    drum_bench_working_B.sf_MATLABFunction1.id;

  /* BusCreator: '<S88>/Bus Creator' incorporates:
   *  Constant: '<S88>/id'
   *  Constant: '<S88>/type'
   */
  drum_bench_working_B.BusCreator_h.type = 1;
  drum_bench_working_B.BusCreator_h.id = 0.0F;

  /* BusCreator: '<S81>/Bus Creator' incorporates:
   *  Constant: '<S81>/id'
   *  Constant: '<S81>/type'
   */
  drum_bench_working_B.BusCreator_d.type = 2;
  drum_bench_working_B.BusCreator_d.id = 0.0F;

  /* BusCreator: '<S91>/Bus Creator' incorporates:
   *  Constant: '<S91>/id'
   *  Constant: '<S91>/type'
   */
  drum_bench_working_B.BusCreator_b.type = 3;
  drum_bench_working_B.BusCreator_b.id = 0.0F;

  /* BusCreator: '<S90>/Bus Creator' incorporates:
   *  Constant: '<S90>/id'
   *  Constant: '<S90>/type'
   */
  drum_bench_working_B.BusCreator_bc.type = 3;
  drum_bench_working_B.BusCreator_bc.id = 0.0F;

  /* BusCreator: '<S82>/Bus Creator' incorporates:
   *  Constant: '<S82>/id'
   *  Constant: '<S82>/type'
   */
  drum_bench_working_B.BusCreator_ld.type = 3;
  drum_bench_working_B.BusCreator_ld.id = 1.0F;

  /* DataTypeConversion: '<S83>/Data Type Conversion' incorporates:
   *  Constant: '<S83>/value'
   *  Constant: '<S83>/value1'
   */
  u = floor(1.0);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_g[0] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;
  u = floor(drum_bench_working_P.config.marlins.M0.debug_address_index);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_g[1] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;

  /* End of DataTypeConversion: '<S83>/Data Type Conversion' */

  /* Pack: <S83>/Byte Pack */
  (void) memcpy(&drum_bench_working_B.data_e[0],
                &drum_bench_working_B.DataTypeConversion_g[0],
                8);
  (void) memcpy(&drum_bench_working_B.data_e[8],
                drum_bench_working_ConstP.pooled40,
                12);

  /* BusCreator: '<S83>/Bus Creator' incorporates:
   *  Constant: '<S83>/id'
   *  Constant: '<S83>/type'
   */
  drum_bench_working_B.BusCreator_c.type = 3;
  drum_bench_working_B.BusCreator_c.id = 1.1F;

  /* DataTypeConversion: '<S84>/Data Type Conversion' incorporates:
   *  Constant: '<S84>/value'
   *  Constant: '<S84>/value1'
   */
  u = floor(1.0);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_h[0] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;
  u = floor(drum_bench_working_P.config.marlins.M0.debug_address_index);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_h[1] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;

  /* End of DataTypeConversion: '<S84>/Data Type Conversion' */

  /* Pack: <S84>/Byte Pack */
  (void) memcpy(&drum_bench_working_B.data_b[0],
                &drum_bench_working_B.DataTypeConversion_h[0],
                8);
  (void) memcpy(&drum_bench_working_B.data_b[8],
                drum_bench_working_ConstP.pooled40,
                12);

  /* BusCreator: '<S84>/Bus Creator' incorporates:
   *  Constant: '<S84>/id'
   *  Constant: '<S84>/type'
   */
  drum_bench_working_B.BusCreator_hl.type = 3;
  drum_bench_working_B.BusCreator_hl.id = 1.1F;

  /* DataTypeConversion: '<S85>/Data Type Conversion' incorporates:
   *  Constant: '<S85>/value'
   *  Constant: '<S85>/value1'
   */
  u = floor(1.0);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_ee[0] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;
  u = floor(drum_bench_working_P.config.marlins.M0.debug_address_index);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_ee[1] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;

  /* End of DataTypeConversion: '<S85>/Data Type Conversion' */

  /* Pack: <S85>/Byte Pack */
  (void) memcpy(&drum_bench_working_B.data_n[0],
                &drum_bench_working_B.DataTypeConversion_ee[0],
                8);
  (void) memcpy(&drum_bench_working_B.data_n[8],
                drum_bench_working_ConstP.pooled40,
                12);

  /* BusCreator: '<S85>/Bus Creator' incorporates:
   *  Constant: '<S85>/id'
   *  Constant: '<S85>/type'
   */
  drum_bench_working_B.BusCreator_nh.type = 3;
  drum_bench_working_B.BusCreator_nh.id = 1.1F;

  /* BusCreator: '<S89>/Bus Creator' incorporates:
   *  Constant: '<S89>/Constant'
   *  Constant: '<S89>/Constant1'
   *  Constant: '<S89>/Constant2'
   */
  drum_bench_working_B.BusCreator_g.type = 10;
  drum_bench_working_B.BusCreator_g.id = 20.0F;
  for (i = 0; i < 20; i++) {
    /* BusCreator: '<S96>/Bus Creator' */
    drum_bench_working_B.BusCreator_a.data[i] = drum_bench_working_B.data_m[i];

    /* BusCreator: '<S88>/Bus Creator' */
    drum_bench_working_B.BusCreator_h.data[i] =
      drum_bench_working_ConstB.data_e[i];

    /* BusCreator: '<S81>/Bus Creator' */
    drum_bench_working_B.BusCreator_d.data[i] =
      drum_bench_working_ConstB.data_d[i];

    /* BusCreator: '<S91>/Bus Creator' */
    drum_bench_working_B.BusCreator_b.data[i] =
      drum_bench_working_ConstB.data_cu[i];

    /* BusCreator: '<S90>/Bus Creator' */
    drum_bench_working_B.BusCreator_bc.data[i] =
      drum_bench_working_ConstB.data_o[i];

    /* BusCreator: '<S82>/Bus Creator' */
    drum_bench_working_B.BusCreator_ld.data[i] =
      drum_bench_working_ConstB.data_cz[i];

    /* BusCreator: '<S83>/Bus Creator' */
    drum_bench_working_B.BusCreator_c.data[i] = drum_bench_working_B.data_e[i];

    /* BusCreator: '<S84>/Bus Creator' */
    drum_bench_working_B.BusCreator_hl.data[i] = drum_bench_working_B.data_b[i];

    /* BusCreator: '<S85>/Bus Creator' */
    drum_bench_working_B.BusCreator_nh.data[i] = drum_bench_working_B.data_n[i];
    drum_bench_working_B.BusCreator_g.data[i] =
      drum_bench_working_ConstP.pooled42[i];
  }

  /* End of BusCreator: '<S89>/Bus Creator' */

  /* Concatenate: '<S63>/Vector Concatenate' */
  drum_bench_working_B.VectorConcatenate[0] = drum_bench_working_B.BusCreator_a;
  drum_bench_working_B.VectorConcatenate[1] = drum_bench_working_B.BusCreator_h;
  drum_bench_working_B.VectorConcatenate[2] = drum_bench_working_B.BusCreator_d;
  drum_bench_working_B.VectorConcatenate[3] = drum_bench_working_B.BusCreator_b;
  drum_bench_working_B.VectorConcatenate[4] = drum_bench_working_B.BusCreator_bc;
  drum_bench_working_B.VectorConcatenate[5] = drum_bench_working_B.BusCreator_ld;
  drum_bench_working_B.VectorConcatenate[6] = drum_bench_working_B.BusCreator_nf;
  drum_bench_working_B.VectorConcatenate[7] = drum_bench_working_B.BusCreator_k;
  drum_bench_working_B.VectorConcatenate[8] = drum_bench_working_B.BusCreator_iq;
  drum_bench_working_B.VectorConcatenate[9] = drum_bench_working_B.BusCreator_l;
  drum_bench_working_B.VectorConcatenate[10] = drum_bench_working_B.BusCreator_o;
  drum_bench_working_B.VectorConcatenate[11] = drum_bench_working_B.BusCreator_c;
  drum_bench_working_B.VectorConcatenate[12] =
    drum_bench_working_B.BusCreator_hl;
  drum_bench_working_B.VectorConcatenate[13] =
    drum_bench_working_B.BusCreator_nh;
  drum_bench_working_B.VectorConcatenate[14] = drum_bench_working_B.BusCreator_g;

  /* MATLAB Function: '<S63>/select rpc' */
  drum_bench_working_selectrpc(drum_bench_working_B.rpc,
    drum_bench_working_B.VectorConcatenate, &drum_bench_working_B.sf_selectrpc);

  /* Pack: <S63>/Byte Pack */
  (void) memcpy(&drum_bench_working_B.rpc_i[0],
                &drum_bench_working_B.sf_selectrpc.type,
                4);
  (void) memcpy(&drum_bench_working_B.rpc_i[4],
                &drum_bench_working_B.sf_selectrpc.id,
                4);
  (void) memcpy(&drum_bench_working_B.rpc_i[8],
                &drum_bench_working_B.sf_selectrpc.data[0],
                20);

  /* RelationalOperator: '<S92>/Compare' incorporates:
   *  Constant: '<S92>/Constant'
   */
  drum_bench_working_B.Compare = (drum_bench_working_B.type == 2);

  /* Outputs for Enabled SubSystem: '<S81>/enabled subsystem' */
  drum_bench_wor_enabledsubsystem(drum_bench_working_B.Compare,
    drum_bench_working_B.data, &drum_bench_working_B.enabledsubsystem);

  /* End of Outputs for SubSystem: '<S81>/enabled subsystem' */

  /* Unpack: <S81>/Byte Unpack */
  (void) memcpy(&drum_bench_working_B.ByteUnpack_e[0],
                &drum_bench_working_B.enabledsubsystem.In1[0],
                20);

  /* DataTypeConversion: '<S81>/Data Type Conversion' */
  for (i = 0; i < 10; i++) {
    drum_bench_working_B.DataTypeConversion_cm[i] =
      drum_bench_working_B.ByteUnpack_e[i];
  }

  /* End of DataTypeConversion: '<S81>/Data Type Conversion' */

  /* DataTypeConversion: '<S81>/analog_limit' */
  drum_bench_working_B.analog_limit = drum_bench_working_B.ByteUnpack_e[3];

  /* DataTypeConversion: '<S81>/ethercat' */
  drum_bench_working_B.ethercat = drum_bench_working_B.ByteUnpack_e[8];

  /* DataTypeConversion: '<S81>/external_panic' */
  drum_bench_working_B.external_panic = drum_bench_working_B.ByteUnpack_e[9];

  /* DataTypeConversion: '<S81>/motor encoder' */
  drum_bench_working_B.motor_encoder = drum_bench_working_B.ByteUnpack_e[4];

  /* DataTypeConversion: '<S81>/over_current' */
  drum_bench_working_B.over_current = drum_bench_working_B.ByteUnpack_e[0];

  /* DataTypeConversion: '<S81>/over_voltage' */
  drum_bench_working_B.over_voltage = drum_bench_working_B.ByteUnpack_e[2];

  /* DataTypeConversion: '<S81>/quadrature_1_high' */
  drum_bench_working_B.quadrature_1_high = drum_bench_working_B.ByteUnpack_e[6];

  /* DataTypeConversion: '<S81>/quadrature_1_low' */
  drum_bench_working_B.quadrature_1_low = drum_bench_working_B.ByteUnpack_e[5];

  /* DataTypeConversion: '<S81>/quadrature_2_limit' */
  drum_bench_working_B.quadrature_2_limit = drum_bench_working_B.ByteUnpack_e[7];

  /* DataTypeConversion: '<S81>/under_voltage' */
  drum_bench_working_B.under_voltage = drum_bench_working_B.ByteUnpack_e[1];

  /* RelationalOperator: '<S94>/Compare' incorporates:
   *  Constant: '<S94>/Constant'
   */
  drum_bench_working_B.Compare_a = (drum_bench_working_B.type == 3);

  /* Outputs for Enabled SubSystem: '<S82>/Enabled Subsystem' */
  drum_bench_wor_EnabledSubsystem(drum_bench_working_B.Compare_a,
    drum_bench_working_B.data, &drum_bench_working_B.EnabledSubsystem);

  /* End of Outputs for SubSystem: '<S82>/Enabled Subsystem' */

  /* Unpack: <S82>/Byte Unpack */
  (void) memcpy(&drum_bench_working_B.ByteUnpack[0],
                &drum_bench_working_B.EnabledSubsystem.In1[0],
                20);

  /* DataTypeConversion: '<S82>/load a' */
  drum_bench_working_B.loada = drum_bench_working_B.ByteUnpack[0];

  /* DataTypeConversion: '<S82>/load a1' */
  drum_bench_working_B.loada1 = drum_bench_working_B.ByteUnpack[2];

  /* DataTypeConversion: '<S82>/load a2' */
  drum_bench_working_B.loada2 = drum_bench_working_B.ByteUnpack[3];

  /* DataTypeConversion: '<S82>/load a3' */
  drum_bench_working_B.loada3 = drum_bench_working_B.ByteUnpack[4];

  /* DataTypeConversion: '<S82>/load b' */
  drum_bench_working_B.loadb = drum_bench_working_B.ByteUnpack[1];

  /* DataTypeConversion: '<S86>/current_index' */
  drum_bench_working_B.current_index =
    drum_bench_working_B.sf_MATLABFunction1.index;

  /* DataTypeConversion: '<Root>/mode0' */
  drum_bench_working_B.mode0 = drum_bench_working_B.mode;

  /* DataTypeConversion: '<Root>/motor_iq0' */
  drum_bench_working_B.motor_iq0 = drum_bench_working_B.motor_iq_b;

  /* DataTypeConversion: '<Root>/motor_torque0' */
  drum_bench_working_B.motor_torque0 = drum_bench_working_B.motor_torque_h;

  /* S-Function Block: <S129>/marlin_ec */
  /* Output Port 1 written directly by slave */
  /* Output Port 2 written directly by slave */
  /* Output Port 3 written directly by slave */
  /* Output Port 4 written directly by slave */
  /* Output Port 5 written directly by slave */
  /* Output Port 6 written directly by slave */
  /* Output Port 7 written directly by slave */
  /* Output Port 8 written directly by slave */
  /* Output Port 9 written directly by slave */
  /* Output Port 10 written directly by slave */
  /* Output Port 11 written directly by slave */
  /* Output Port 12 written directly by slave */
  /* Output Port 13 written directly by slave */
  /* Output Port 14 written directly by slave */
  /* Output Port 15 written directly by slave */
  /* Output Port 16 written directly by slave */
  /* Output Port 17 written directly by slave */
  /* Output Port 18 written directly by slave */
  /* Output Port 19 written directly by slave */
  /* Output Port 20 written directly by slave */
  /* Output Port 21 written directly by slave */
  /* Output Port 22 written directly by slave */

  /* DataTypeConversion: '<S129>/motor_torque_' */
  drum_bench_working_B.motor_torque_l = drum_bench_working_B.marlin_ec_o1_k;

  /* DataTypeConversion: '<S129>/motor_iq_' */
  drum_bench_working_B.motor_iq_bj = drum_bench_working_B.marlin_ec_o2_f;

  /* DataTypeConversion: '<S129>/motor_vq_avg_' */
  drum_bench_working_B.motor_vq_avg_e = drum_bench_working_B.marlin_ec_o3_h;

  /* DataTypeConversion: '<S129>/analog_1_' */
  drum_bench_working_B.analog_1_on = drum_bench_working_B.marlin_ec_o4_a;

  /* DataTypeConversion: '<S129>/analog_1_dot_' */
  drum_bench_working_B.analog_1_dot_k = drum_bench_working_B.marlin_ec_o5_l;

  /* DataTypeConversion: '<S129>/analog_2_' */
  drum_bench_working_B.analog_2_h = drum_bench_working_B.marlin_ec_o6_j;

  /* DataTypeConversion: '<S129>/analog_2_dot_' */
  drum_bench_working_B.analog_2_dot_h = drum_bench_working_B.marlin_ec_o7_c;

  /* DataTypeConversion: '<S129>/analog_diff_' */
  drum_bench_working_B.analog_diff_o = drum_bench_working_B.marlin_ec_o8_j;

  /* DataTypeConversion: '<S129>/analog_diff_dot_' */
  drum_bench_working_B.analog_diff_dot_m = drum_bench_working_B.marlin_ec_o9_j;

  /* DataTypeConversion: '<S129>/quadrature_1_' */
  drum_bench_working_B.quadrature_1_i = drum_bench_working_B.marlin_ec_o10_n;

  /* DataTypeConversion: '<S129>/quadrature_1_dot_' */
  drum_bench_working_B.quadrature_1_dot_e = drum_bench_working_B.marlin_ec_o11_d;

  /* DataTypeConversion: '<S129>/quadrature_2_' */
  drum_bench_working_B.quadrature_2_h = drum_bench_working_B.marlin_ec_o12_h;

  /* DataTypeConversion: '<S129>/quadrature_2_dot_' */
  drum_bench_working_B.quadrature_2_dot_b = drum_bench_working_B.marlin_ec_o13_b;

  /* DataTypeConversion: '<S129>/ssi_' */
  drum_bench_working_B.ssi_b = drum_bench_working_B.marlin_ec_o14_e;

  /* DataTypeConversion: '<S129>/ssi_dot_' */
  drum_bench_working_B.ssi_dot_b = drum_bench_working_B.marlin_ec_o15_h;

  /* DataTypeConversion: '<S129>/accelerometer_x_' */
  drum_bench_working_B.accelerometer_x_ej = drum_bench_working_B.marlin_ec_o16_c;

  /* DataTypeConversion: '<S129>/accelerometer_y_' */
  drum_bench_working_B.accelerometer_y_j = drum_bench_working_B.marlin_ec_o17_a;

  /* DataTypeConversion: '<S129>/accelerometer_z_' */
  drum_bench_working_B.accelerometer_z_i = drum_bench_working_B.marlin_ec_o18_g;

  /* DataTypeConversion: '<S129>/faults_' */
  drum_bench_working_B.faults_k = drum_bench_working_B.marlin_ec_o19_n;

  /* DataTypeConversion: '<S129>/bus_v_' */
  drum_bench_working_B.bus_v_l = drum_bench_working_B.marlin_ec_o20_k;

  /* BusCreator: '<S8>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.motor_torque =
    drum_bench_working_B.motor_torque_l;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.motor_iq =
    drum_bench_working_B.motor_iq_bj;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.motor_vq_avg =
    drum_bench_working_B.motor_vq_avg_e;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.analog_1 =
    drum_bench_working_B.analog_1_on;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.analog_1_dot =
    drum_bench_working_B.analog_1_dot_k;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.analog_2 =
    drum_bench_working_B.analog_2_h;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.analog_2_dot =
    drum_bench_working_B.analog_2_dot_h;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.analog_diff =
    drum_bench_working_B.analog_diff_o;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.analog_diff_dot =
    drum_bench_working_B.analog_diff_dot_m;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.quadrature_1 =
    drum_bench_working_B.quadrature_1_i;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.quadrature_1_dot =
    drum_bench_working_B.quadrature_1_dot_e;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.quadrature_2 =
    drum_bench_working_B.quadrature_2_h;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.quadrature_2_dot =
    drum_bench_working_B.quadrature_2_dot_b;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.ssi =
    drum_bench_working_B.ssi_b;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.ssi_dot =
    drum_bench_working_B.ssi_dot_b;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.accelerometer_x =
    drum_bench_working_B.accelerometer_x_ej;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.accelerometer_y =
    drum_bench_working_B.accelerometer_y_j;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.accelerometer_z =
    drum_bench_working_B.accelerometer_z_i;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.faults =
    drum_bench_working_B.faults_k;
  drum_bench_working_B.BusConversion_InsertedFor_Bus_a.bus_v =
    drum_bench_working_B.bus_v_l;
  for (i = 0; i < 28; i++) {
    /* DataTypeConversion: '<S129>/rpa_packet_' */
    drum_bench_working_B.rpa_packet_b[i] =
      drum_bench_working_B.marlin_ec_o22_a[i];
    drum_bench_working_B.BusConversion_InsertedFor_Bus_a.rpa_packet[i] =
      drum_bench_working_B.rpa_packet_b[i];
  }

  /* End of BusCreator: '<S8>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */

  /* Delay: '<S127>/Delay' */
  drum_bench_working_B.Delay_l[0] = drum_bench_working_DW.Delay_DSTATE_n[0];
  drum_bench_working_B.Delay_l[1] = drum_bench_working_DW.Delay_DSTATE_n[1];

  /* Gain: '<S127>/eA1' */
  drum_bench_working_B.eA1_c[0] = 0.0;
  drum_bench_working_B.eA1_c[0] +=
    drum_bench_working_P.config.marlins.M1.motor.thermal_model.eA1[0] *
    drum_bench_working_B.Delay_l[0];
  drum_bench_working_B.eA1_c[0] +=
    drum_bench_working_P.config.marlins.M1.motor.thermal_model.eA1[2] *
    drum_bench_working_B.Delay_l[1];
  drum_bench_working_B.eA1_c[1] = 0.0;
  drum_bench_working_B.eA1_c[1] +=
    drum_bench_working_P.config.marlins.M1.motor.thermal_model.eA1[1] *
    drum_bench_working_B.Delay_l[0];
  drum_bench_working_B.eA1_c[1] +=
    drum_bench_working_P.config.marlins.M1.motor.thermal_model.eA1[3] *
    drum_bench_working_B.Delay_l[1];

  /* Math: '<S127>/Math Function'
   *
   * About '<S127>/Math Function':
   *  Operator: magnitude^2
   */
  u = drum_bench_working_B.motor_iq_bj;
  drum_bench_working_B.MathFunction_p = u * u;

  /* Gain: '<S127>/resistance' */
  drum_bench_working_B.resistance_e =
    drum_bench_working_P.config.marlins.M1.motor.resistance *
    drum_bench_working_B.MathFunction_p;

  /* Gain: '<S127>/eA2' */
  drum_bench_working_B.eA2_f[0] =
    drum_bench_working_P.config.marlins.M1.motor.thermal_model.eA2[0] *
    drum_bench_working_B.resistance_e;
  drum_bench_working_B.eA2_f[1] =
    drum_bench_working_P.config.marlins.M1.motor.thermal_model.eA2[1] *
    drum_bench_working_B.resistance_e;

  /* Sum: '<S127>/Sum' */
  drum_bench_working_B.Sum_do[0] = drum_bench_working_B.eA1_c[0] +
    drum_bench_working_B.eA2_f[0];
  drum_bench_working_B.Sum_do[1] = drum_bench_working_B.eA1_c[1] +
    drum_bench_working_B.eA2_f[1];

  /* BusCreator: '<S8>/Bus Creator' */
  drum_bench_working_B.BusCreator_i.marlin_basic_status =
    drum_bench_working_B.BusConversion_InsertedFor_Bus_a;
  drum_bench_working_B.BusCreator_i.temperature_motor_winding =
    drum_bench_working_B.Sum_do[0];
  drum_bench_working_B.BusCreator_i.temperature_motor_housing =
    drum_bench_working_B.Sum_do[1];

  /* BusSelector: '<S6>/Bus Selector' */
  drum_bench_working_B.marlin_basic_status_l =
    drum_bench_working_B.BusCreator_i.marlin_basic_status;
  drum_bench_working_B.temperature_motor_winding_k =
    drum_bench_working_B.BusCreator_i.temperature_motor_winding;
  drum_bench_working_B.temperature_motor_housing_jp =
    drum_bench_working_B.BusCreator_i.temperature_motor_housing;

  /* BusSelector: '<S47>/Bus Selector' */
  drum_bench_working_B.motor_torque_pd =
    drum_bench_working_B.marlin_basic_status_l.motor_torque;
  drum_bench_working_B.motor_iq_f =
    drum_bench_working_B.marlin_basic_status_l.motor_iq;
  drum_bench_working_B.motor_vq_avg_n =
    drum_bench_working_B.marlin_basic_status_l.motor_vq_avg;
  drum_bench_working_B.analog_1_m =
    drum_bench_working_B.marlin_basic_status_l.analog_1;
  drum_bench_working_B.analog_1_dot_f =
    drum_bench_working_B.marlin_basic_status_l.analog_1_dot;
  drum_bench_working_B.analog_2_b =
    drum_bench_working_B.marlin_basic_status_l.analog_2;
  drum_bench_working_B.analog_2_dot_e =
    drum_bench_working_B.marlin_basic_status_l.analog_2_dot;
  drum_bench_working_B.analog_diff_hz =
    drum_bench_working_B.marlin_basic_status_l.analog_diff;
  drum_bench_working_B.analog_diff_dot_h =
    drum_bench_working_B.marlin_basic_status_l.analog_diff_dot;
  drum_bench_working_B.quadrature_1_a =
    drum_bench_working_B.marlin_basic_status_l.quadrature_1;
  drum_bench_working_B.quadrature_1_dot_j =
    drum_bench_working_B.marlin_basic_status_l.quadrature_1_dot;
  drum_bench_working_B.quadrature_2_lz =
    drum_bench_working_B.marlin_basic_status_l.quadrature_2;
  drum_bench_working_B.quadrature_2_dot_o4 =
    drum_bench_working_B.marlin_basic_status_l.quadrature_2_dot;
  drum_bench_working_B.ssi_n = drum_bench_working_B.marlin_basic_status_l.ssi;
  drum_bench_working_B.ssi_dot_l =
    drum_bench_working_B.marlin_basic_status_l.ssi_dot;
  drum_bench_working_B.accelerometer_x_f =
    drum_bench_working_B.marlin_basic_status_l.accelerometer_x;
  drum_bench_working_B.accelerometer_y_jg =
    drum_bench_working_B.marlin_basic_status_l.accelerometer_y;
  drum_bench_working_B.accelerometer_z_g =
    drum_bench_working_B.marlin_basic_status_l.accelerometer_z;
  drum_bench_working_B.faults_o =
    drum_bench_working_B.marlin_basic_status_l.faults;
  drum_bench_working_B.bus_v_p =
    drum_bench_working_B.marlin_basic_status_l.bus_v;
  for (i = 0; i < 28; i++) {
    drum_bench_working_B.rpa_packet_g[i] =
      drum_bench_working_B.marlin_basic_status_l.rpa_packet[i];
  }

  /* End of BusSelector: '<S47>/Bus Selector' */

  /* DataTypeConversion: '<S47>/accelerometer_x' */
  drum_bench_working_B.accelerometer_x_d =
    drum_bench_working_B.accelerometer_x_f;

  /* DataTypeConversion: '<S47>/accelerometer_y' */
  drum_bench_working_B.accelerometer_y_e =
    drum_bench_working_B.accelerometer_y_jg;

  /* DataTypeConversion: '<S47>/accelerometer_z' */
  drum_bench_working_B.accelerometer_z_b =
    drum_bench_working_B.accelerometer_z_g;

  /* DataTypeConversion: '<S47>/analog_1' */
  drum_bench_working_B.analog_1_b = drum_bench_working_B.analog_1_m;

  /* DataTypeConversion: '<S47>/analog_1_dot' */
  drum_bench_working_B.analog_1_dot_b = drum_bench_working_B.analog_1_dot_f;

  /* DataTypeConversion: '<S47>/analog_2' */
  drum_bench_working_B.analog_2_a = drum_bench_working_B.analog_2_b;

  /* DataTypeConversion: '<S47>/analog_2_dot' */
  drum_bench_working_B.analog_2_dot_k = drum_bench_working_B.analog_2_dot_e;

  /* DataTypeConversion: '<S47>/analog_diff' */
  drum_bench_working_B.analog_diff_n = drum_bench_working_B.analog_diff_hz;

  /* DataTypeConversion: '<S47>/analog_diff_dot' */
  drum_bench_working_B.analog_diff_dot_i =
    drum_bench_working_B.analog_diff_dot_h;

  /* DataTypeConversion: '<S47>/bus_v' */
  drum_bench_working_B.bus_v_pu = drum_bench_working_B.bus_v_p;

  /* DataTypeConversion: '<S48>/raw_integer' */
  drum_bench_working_B.raw_integer_l = drum_bench_working_B.faults_o;

  /* DataTypeConversion: '<S49>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_p = (uint8_T)((uint8_T)
    drum_bench_working_B.raw_integer_l & 1);

  /* DataTypeConversion: '<S49>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_je =
    drum_bench_working_B.ExtractDesiredBits_p;

  /* DataTypeConversion: '<S50>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_f = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer_l >> 1) & 1);

  /* DataTypeConversion: '<S50>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_o =
    drum_bench_working_B.ExtractDesiredBits_f;

  /* DataTypeConversion: '<S51>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_i1 = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer_l >> 10) & 1);

  /* DataTypeConversion: '<S51>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_p =
    drum_bench_working_B.ExtractDesiredBits_i1;

  /* DataTypeConversion: '<S52>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_f2 = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer_l >> 2) & 1);

  /* DataTypeConversion: '<S52>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_oc =
    drum_bench_working_B.ExtractDesiredBits_f2;

  /* DataTypeConversion: '<S53>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_j = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer_l >> 3) & 1);

  /* DataTypeConversion: '<S53>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_l =
    drum_bench_working_B.ExtractDesiredBits_j;

  /* DataTypeConversion: '<S54>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_lr = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer_l >> 4) & 1);

  /* DataTypeConversion: '<S54>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_e =
    drum_bench_working_B.ExtractDesiredBits_lr;

  /* DataTypeConversion: '<S55>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_o = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer_l >> 5) & 1);

  /* DataTypeConversion: '<S55>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_f =
    drum_bench_working_B.ExtractDesiredBits_o;

  /* DataTypeConversion: '<S56>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_g = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer_l >> 6) & 1);

  /* DataTypeConversion: '<S56>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_cr =
    drum_bench_working_B.ExtractDesiredBits_g;

  /* DataTypeConversion: '<S57>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_kb = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer_l >> 7) & 1);

  /* DataTypeConversion: '<S57>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_n =
    drum_bench_working_B.ExtractDesiredBits_kb;

  /* DataTypeConversion: '<S58>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_i = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer_l >> 8) & 1);

  /* DataTypeConversion: '<S58>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_k =
    drum_bench_working_B.ExtractDesiredBits_i;

  /* DataTypeConversion: '<S59>/Extract Desired Bits' */
  drum_bench_working_B.ExtractDesiredBits_c = (uint8_T)((uint8_T)
    (drum_bench_working_B.raw_integer_l >> 9) & 1);

  /* DataTypeConversion: '<S59>/Modify Scaling Only' */
  drum_bench_working_B.ModifyScalingOnly_pi =
    drum_bench_working_B.ExtractDesiredBits_c;

  /* DataTypeConversion: '<S48>/analog_' */
  drum_bench_working_B.analog_i = (drum_bench_working_B.ModifyScalingOnly_cr !=
    0);

  /* DataTypeConversion: '<S48>/communication_fault_' */
  drum_bench_working_B.communication_fault_o =
    (drum_bench_working_B.ModifyScalingOnly_oc != 0);

  /* DataTypeConversion: '<S48>/current_' */
  drum_bench_working_B.current_h = (drum_bench_working_B.ModifyScalingOnly_e !=
    0);

  /* DataTypeConversion: '<S48>/ethercat_' */
  drum_bench_working_B.ethercat_e = (drum_bench_working_B.ModifyScalingOnly_k !=
    0);

  /* DataTypeConversion: '<S48>/external_panic_' */
  drum_bench_working_B.external_panic_o =
    (drum_bench_working_B.ModifyScalingOnly_l != 0);

  /* DataTypeConversion: '<S48>/hard_fault_' */
  drum_bench_working_B.hard_fault_e = (drum_bench_working_B.ModifyScalingOnly_o
    != 0);

  /* DataTypeConversion: '<S48>/quadrature_' */
  drum_bench_working_B.quadrature_l = (drum_bench_working_B.ModifyScalingOnly_n
    != 0);

  /* DataTypeConversion: '<S48>/soft_fault_' */
  drum_bench_working_B.soft_fault_a = (drum_bench_working_B.ModifyScalingOnly_je
    != 0);

  /* DataTypeConversion: '<S48>/ssi_' */
  drum_bench_working_B.ssi_m = (drum_bench_working_B.ModifyScalingOnly_p != 0);

  /* DataTypeConversion: '<S48>/temperature_' */
  drum_bench_working_B.temperature_b =
    (drum_bench_working_B.ModifyScalingOnly_pi != 0);

  /* DataTypeConversion: '<S48>/voltage_' */
  drum_bench_working_B.voltage_f = (drum_bench_working_B.ModifyScalingOnly_f !=
    0);

  /* DataTypeConversion: '<S47>/motor_iq' */
  drum_bench_working_B.motor_iq_gt = drum_bench_working_B.motor_iq_f;

  /* DataTypeConversion: '<S47>/motor_torque' */
  drum_bench_working_B.motor_torque_pz = drum_bench_working_B.motor_torque_pd;

  /* DataTypeConversion: '<S47>/motor_vq_avg' */
  drum_bench_working_B.motor_vq_avg_i = drum_bench_working_B.motor_vq_avg_n;

  /* DataTypeConversion: '<S47>/quadrature_1' */
  drum_bench_working_B.quadrature_1_ib = drum_bench_working_B.quadrature_1_a;

  /* DataTypeConversion: '<S47>/quadrature_1_dot' */
  drum_bench_working_B.quadrature_1_dot_f =
    drum_bench_working_B.quadrature_1_dot_j;

  /* DataTypeConversion: '<S47>/quadrature_2' */
  drum_bench_working_B.quadrature_2_g = drum_bench_working_B.quadrature_2_lz;

  /* DataTypeConversion: '<S47>/quadrature_2_dot' */
  drum_bench_working_B.quadrature_2_dot_e =
    drum_bench_working_B.quadrature_2_dot_o4;

  /* Unpack: <S47>/rpa_float */
  (void) memcpy(&drum_bench_working_B.rpa_float_f[0],
                &drum_bench_working_B.rpa_packet_g[0],
                28);

  /* DataTypeConversion: '<S47>/ssi' */
  drum_bench_working_B.ssi_h = drum_bench_working_B.ssi_n;

  /* DataTypeConversion: '<S47>/ssi_dot' */
  drum_bench_working_B.ssi_dot_o = drum_bench_working_B.ssi_dot_l;

  /* DataTypeConversion: '<S6>/temperature_motor_housing' */
  drum_bench_working_B.temperature_motor_housing_m =
    drum_bench_working_B.temperature_motor_housing_jp;

  /* DataTypeConversion: '<S6>/temperature_motor_winding' */
  drum_bench_working_B.temperature_motor_winding_i =
    drum_bench_working_B.temperature_motor_winding_k;

  /* Sum: '<S27>/Sum' incorporates:
   *  Constant: '<S27>/Constant'
   */
  drum_bench_working_B.Sum_cq = 0.0 +
    drum_bench_working_P.config.input.input1.quadrature_1_angle.bias;

  /* RateLimiter: '<S27>/Rate Limiter' */
  u = drum_bench_working_B.Sum_cq - drum_bench_working_DW.PrevY_m;
  if (u > 29.999999999999996) {
    drum_bench_working_B.RateLimiter_d = drum_bench_working_DW.PrevY_m +
      29.999999999999996;
  } else if (u < (-29.999999999999996)) {
    drum_bench_working_B.RateLimiter_d = drum_bench_working_DW.PrevY_m +
      (-29.999999999999996);
  } else {
    drum_bench_working_B.RateLimiter_d = drum_bench_working_B.Sum_cq;
  }

  drum_bench_working_DW.PrevY_m = drum_bench_working_B.RateLimiter_d;

  /* End of RateLimiter: '<S27>/Rate Limiter' */

  /* DataTypeConversion: '<S27>/Data Type Conversion' */
  drum_bench_working_B.DataTypeConversion_m = drum_bench_working_B.RateLimiter_d;

  /* Sum: '<S28>/Sum' incorporates:
   *  Constant: '<S28>/Constant'
   */
  drum_bench_working_B.Sum_a = 0.0 +
    drum_bench_working_P.config.input.input1.quadrature_2_angle.bias;

  /* RateLimiter: '<S28>/Rate Limiter' */
  u = drum_bench_working_B.Sum_a - drum_bench_working_DW.PrevY_b;
  if (u > 0.0003) {
    drum_bench_working_B.RateLimiter_f = drum_bench_working_DW.PrevY_b + 0.0003;
  } else if (u < (-0.0003)) {
    drum_bench_working_B.RateLimiter_f = drum_bench_working_DW.PrevY_b +
      (-0.0003);
  } else {
    drum_bench_working_B.RateLimiter_f = drum_bench_working_B.Sum_a;
  }

  drum_bench_working_DW.PrevY_b = drum_bench_working_B.RateLimiter_f;

  /* End of RateLimiter: '<S28>/Rate Limiter' */

  /* DataTypeConversion: '<S28>/Data Type Conversion' */
  drum_bench_working_B.DataTypeConversion_c2 =
    drum_bench_working_B.RateLimiter_f;

  /* Sum: '<S24>/Sum' incorporates:
   *  Constant: '<S24>/Constant'
   */
  drum_bench_working_B.Sum_g = 0.0 +
    drum_bench_working_P.config.input.input1.analog.bias;

  /* RateLimiter: '<S24>/Rate Limiter' */
  u = drum_bench_working_B.Sum_g - drum_bench_working_DW.PrevY_f;
  if (u > 0.0003) {
    drum_bench_working_B.RateLimiter_b5 = drum_bench_working_DW.PrevY_f + 0.0003;
  } else if (u < (-0.0003)) {
    drum_bench_working_B.RateLimiter_b5 = drum_bench_working_DW.PrevY_f +
      (-0.0003);
  } else {
    drum_bench_working_B.RateLimiter_b5 = drum_bench_working_B.Sum_g;
  }

  drum_bench_working_DW.PrevY_f = drum_bench_working_B.RateLimiter_b5;

  /* End of RateLimiter: '<S24>/Rate Limiter' */

  /* DataTypeConversion: '<S24>/Data Type Conversion' */
  drum_bench_working_B.DataTypeConversion_l =
    drum_bench_working_B.RateLimiter_b5;

  /* Sum: '<S25>/Sum' incorporates:
   *  Constant: '<S25>/Constant'
   */
  drum_bench_working_B.Sum_bf = 0.0 +
    drum_bench_working_P.config.input.input1.motor_iq.bias;

  /* RateLimiter: '<S25>/Rate Limiter' */
  u = drum_bench_working_B.Sum_bf - drum_bench_working_DW.PrevY_d5;
  if (u > 3.0E+196) {
    drum_bench_working_B.RateLimiter_eb = drum_bench_working_DW.PrevY_d5 +
      3.0E+196;
  } else if (u < (-3.0E+196)) {
    drum_bench_working_B.RateLimiter_eb = drum_bench_working_DW.PrevY_d5 +
      (-3.0E+196);
  } else {
    drum_bench_working_B.RateLimiter_eb = drum_bench_working_B.Sum_bf;
  }

  drum_bench_working_DW.PrevY_d5 = drum_bench_working_B.RateLimiter_eb;

  /* End of RateLimiter: '<S25>/Rate Limiter' */

  /* DataTypeConversion: '<S25>/Data Type Conversion' */
  drum_bench_working_B.DataTypeConversion_j =
    drum_bench_working_B.RateLimiter_eb;

  /* Sum: '<S26>/Sum' incorporates:
   *  Constant: '<S26>/Constant'
   */
  drum_bench_working_B.Sum_o = 0.0 +
    drum_bench_working_P.config.input.input1.motor_vq.bias;

  /* RateLimiter: '<S26>/Rate Limiter' */
  u = drum_bench_working_B.Sum_o - drum_bench_working_DW.PrevY_k;
  if (u > 0.0003) {
    drum_bench_working_B.RateLimiter_ol = drum_bench_working_DW.PrevY_k + 0.0003;
  } else if (u < (-0.0003)) {
    drum_bench_working_B.RateLimiter_ol = drum_bench_working_DW.PrevY_k +
      (-0.0003);
  } else {
    drum_bench_working_B.RateLimiter_ol = drum_bench_working_B.Sum_o;
  }

  drum_bench_working_DW.PrevY_k = drum_bench_working_B.RateLimiter_ol;

  /* End of RateLimiter: '<S26>/Rate Limiter' */

  /* DataTypeConversion: '<S26>/Data Type Conversion' */
  drum_bench_working_B.DataTypeConversion_e =
    drum_bench_working_B.RateLimiter_ol;

  /* BusCreator: '<S4>/Bus Creator' incorporates:
   *  Constant: '<S4>/analog_diff_dot'
   *  Constant: '<S4>/mode'
   *  Constant: '<S4>/motor_id'
   *  Constant: '<S4>/motor_torque'
   *  Constant: '<S4>/motor_vd'
   *  Constant: '<S4>/quadrature_1_angle_dot'
   *  Constant: '<S4>/quadrature_2_angle_dot'
   *  Constant: '<S4>/rpc_command'
   */
  drum_bench_working_B.BusCreator_f.mode =
    drum_bench_working_P.config.input.input1.mode;
  drum_bench_working_B.BusCreator_f.quadrature_1_angle =
    drum_bench_working_B.DataTypeConversion_m;
  drum_bench_working_B.BusCreator_f.quadrature_1_angle_dot =
    drum_bench_working_P.config.input.input1.quadrature_1_angle_dot;
  drum_bench_working_B.BusCreator_f.quadrature_2_angle =
    drum_bench_working_B.DataTypeConversion_c2;
  drum_bench_working_B.BusCreator_f.quadrature_2_angle_dot =
    drum_bench_working_P.config.input.input1.quadrature_2_angle_dot;
  drum_bench_working_B.BusCreator_f.analog_diff =
    drum_bench_working_B.DataTypeConversion_l;
  drum_bench_working_B.BusCreator_f.analog_diff_dot =
    drum_bench_working_P.config.input.input1.analog_dot;
  drum_bench_working_B.BusCreator_f.motor_torque =
    drum_bench_working_P.config.input.input1.motor_torque;
  drum_bench_working_B.BusCreator_f.motor_iq =
    drum_bench_working_B.DataTypeConversion_j;
  drum_bench_working_B.BusCreator_f.motor_id =
    drum_bench_working_P.config.input.input1.motor_id;
  drum_bench_working_B.BusCreator_f.motor_vq =
    drum_bench_working_B.DataTypeConversion_e;
  drum_bench_working_B.BusCreator_f.motor_vd =
    drum_bench_working_P.config.input.input1.motor_vd;
  drum_bench_working_B.BusCreator_f.rpc =
    drum_bench_working_P.config.input.input1.rpc_command;

  /* BusSelector: '<S2>/Bus Selector2' */
  drum_bench_working_B.quadrature_1_angle_j =
    drum_bench_working_B.BusCreator_f.quadrature_1_angle;
  drum_bench_working_B.quadrature_1_angle_dot_of =
    drum_bench_working_B.BusCreator_f.quadrature_1_angle_dot;

  /* BusSelector: '<S2>/Bus Selector3' */
  drum_bench_working_B.quadrature_1_nt =
    drum_bench_working_B.BusCreator_i.marlin_basic_status.quadrature_1;

  /* Sum: '<S2>/Sum' */
  drum_bench_working_B.Sum_f = drum_bench_working_B.quadrature_1_angle_j -
    drum_bench_working_B.quadrature_1_nt;

  /* Gain: '<S2>/kp' */
  drum_bench_working_B.kp_h =
    drum_bench_working_P.config.control1.impedence_control.kp *
    drum_bench_working_B.Sum_f;

  /* Saturate: '<S2>/Saturation' */
  u = drum_bench_working_B.kp_h;
  u_0 = (-12.0);
  u_1 = 12.0;
  if (u >= u_1) {
    drum_bench_working_B.Saturation_l = u_1;
  } else if (u <= u_0) {
    drum_bench_working_B.Saturation_l = u_0;
  } else {
    drum_bench_working_B.Saturation_l = u;
  }

  /* End of Saturate: '<S2>/Saturation' */

  /* Outputs for Atomic SubSystem: '<S2>/Filtered Derivative' */
  drum_bench_w_FilteredDerivative(drum_bench_working_B.quadrature_1_nt,
    &drum_bench_working_B.FilteredDerivative_n,
    &drum_bench_working_DW.FilteredDerivative_n,
    drum_bench_working_P.config.control1.impedence_control.imp_cutoff);

  /* End of Outputs for SubSystem: '<S2>/Filtered Derivative' */

  /* Sum: '<S2>/Sum1' */
  drum_bench_working_B.Sum1_h = drum_bench_working_B.quadrature_1_angle_dot_of -
    drum_bench_working_B.FilteredDerivative_n.FilterCoefficient;

  /* Gain: '<S2>/kd' */
  drum_bench_working_B.kd_j =
    drum_bench_working_P.config.control1.impedence_control.kd *
    drum_bench_working_B.Sum1_h;

  /* Sum: '<S2>/Sum2' */
  drum_bench_working_B.Sum2_e = drum_bench_working_B.Saturation_l +
    drum_bench_working_B.kd_j;

  /* DataTypeConversion: '<S2>/motor_torque' */
  drum_bench_working_B.motor_torque_b = drum_bench_working_B.Sum2_e;

  /* BusAssignment: '<S2>/Bus Assignment' incorporates:
   *  Constant: '<S2>/current mode'
   */
  drum_bench_working_B.BusAssignment_n4 = drum_bench_working_B.BusCreator_f;
  drum_bench_working_B.BusAssignment_n4.motor_iq =
    drum_bench_working_B.motor_torque_b;
  drum_bench_working_B.BusAssignment_n4.mode = 3.0;

  /* BusSelector: '<S10>/Bus Selector2' */
  drum_bench_working_B.quadrature_1_angle_o =
    drum_bench_working_B.BusCreator_f.quadrature_1_angle;
  drum_bench_working_B.quadrature_1_angle_dot_e =
    drum_bench_working_B.BusCreator_f.quadrature_1_angle_dot;

  /* BusSelector: '<S10>/Bus Selector1' */
  drum_bench_working_B.quadrature_1_o =
    drum_bench_working_B.BusCreator_i.marlin_basic_status.quadrature_1;
  drum_bench_working_B.analog_1_h =
    drum_bench_working_B.BusCreator_i.marlin_basic_status.analog_1;

  /* Sum: '<S10>/Sum' */
  drum_bench_working_B.Sum_co = drum_bench_working_B.quadrature_1_angle_o -
    drum_bench_working_B.quadrature_1_o;

  /* Bias: '<S10>/bias' */
  drum_bench_working_B.bias_c = drum_bench_working_B.analog_1_h +
    drum_bench_working_P.config.control1.var_impedence_control.bias;

  /* Gain: '<S10>/ka' */
  drum_bench_working_B.ka_l =
    drum_bench_working_P.config.control1.var_impedence_control.ka *
    drum_bench_working_B.bias_c;

  /* Product: '<S10>/Product' */
  drum_bench_working_B.Product_i = drum_bench_working_B.Sum_co *
    drum_bench_working_B.ka_l;

  /* Outputs for Atomic SubSystem: '<S10>/Filtered Derivative' */
  drum_bench_w_FilteredDerivative(drum_bench_working_B.quadrature_1_o,
    &drum_bench_working_B.FilteredDerivative_p,
    &drum_bench_working_DW.FilteredDerivative_p,
    drum_bench_working_P.config.control1.var_impedence_control.var_imp_cutoff);

  /* End of Outputs for SubSystem: '<S10>/Filtered Derivative' */

  /* Sum: '<S10>/Sum1' */
  drum_bench_working_B.Sum1_e = drum_bench_working_B.quadrature_1_angle_dot_e -
    drum_bench_working_B.FilteredDerivative_p.FilterCoefficient;

  /* Gain: '<S10>/kd' */
  drum_bench_working_B.kd_l =
    drum_bench_working_P.config.control1.var_impedence_control.kd *
    drum_bench_working_B.Sum1_e;

  /* Sum: '<S10>/Sum2' */
  drum_bench_working_B.Sum2_o = drum_bench_working_B.Product_i +
    drum_bench_working_B.kd_l;

  /* BusAssignment: '<S10>/Bus Assignment' incorporates:
   *  Constant: '<S10>/current mode'
   */
  drum_bench_working_B.BusAssignment_nx = drum_bench_working_B.BusCreator_f;
  drum_bench_working_B.BusAssignment_nx.motor_iq = drum_bench_working_B.Sum2_o;
  drum_bench_working_B.BusAssignment_nx.mode = 3.0;

  /* MultiPortSwitch: '<Root>/mode switch1' incorporates:
   *  Constant: '<Root>/mode select1'
   */
  switch ((int32_T)drum_bench_working_P.config.control1.mode_select) {
   case 1:
    drum_bench_working_B.modeswitch1 = drum_bench_working_B.BusCreator_f;
    break;

   case 2:
    drum_bench_working_B.modeswitch1 = drum_bench_working_B.BusAssignment_n4;
    break;

   case 3:
    drum_bench_working_B.modeswitch1 = drum_bench_working_B.BusAssignment_nx;
    break;

   default:
    drum_bench_working_B.modeswitch1 = drum_bench_working_B.BusCreator_f;
    break;
  }

  /* End of MultiPortSwitch: '<Root>/mode switch1' */

  /* BusSelector: '<Root>/Bus Selector1' */
  drum_bench_working_B.mode_l = drum_bench_working_B.modeswitch1.mode;
  drum_bench_working_B.motor_torque_px =
    drum_bench_working_B.modeswitch1.motor_torque;
  drum_bench_working_B.motor_iq_n = drum_bench_working_B.modeswitch1.motor_iq;

  /* Clock: '<S29>/Clock1' */
  drum_bench_working_B.Clock1_gp = drum_bench_working_M->Timing.t[0];

  /* Product: '<S29>/Product' incorporates:
   *  Constant: '<S29>/deltaFreq'
   *  Constant: '<S29>/targetTime'
   */
  u = drum_bench_working_P.config.input.input1.analog.frequency - 0.1;
  u *= 6.2831853071795862;
  drum_bench_working_B.Product_bh = u / 100.0;

  /* Gain: '<S29>/Gain' */
  drum_bench_working_B.Gain_a3 = 0.5 * drum_bench_working_B.Product_bh;

  /* Product: '<S29>/Product1' */
  drum_bench_working_B.Product1_e = drum_bench_working_B.Clock1_gp *
    drum_bench_working_B.Gain_a3;

  /* Sum: '<S29>/Sum' incorporates:
   *  Constant: '<S29>/initialFreq'
   */
  drum_bench_working_B.Sum_i2 = drum_bench_working_B.Product1_e +
    0.62831853071795862;

  /* Product: '<S29>/Product2' */
  drum_bench_working_B.Product2_d = drum_bench_working_B.Clock1_gp *
    drum_bench_working_B.Sum_i2;

  /* Trigonometry: '<S29>/Output' */
  drum_bench_working_B.Output_i = sin(drum_bench_working_B.Product2_d);

  /* Sin: '<S24>/Sine Wave' */
  drum_bench_working_B.SineWave_ff = sin
    (drum_bench_working_P.config.input.input1.analog.frequency *
     drum_bench_working_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S24>/Sign' */
  u = drum_bench_working_B.SineWave_ff;
  if (u < 0.0) {
    drum_bench_working_B.Sign_ml = -1.0;
  } else if (u > 0.0) {
    drum_bench_working_B.Sign_ml = 1.0;
  } else if (u == 0.0) {
    drum_bench_working_B.Sign_ml = 0.0;
  } else {
    drum_bench_working_B.Sign_ml = u;
  }

  /* End of Signum: '<S24>/Sign' */

  /* MultiPortSwitch: '<S24>/Multiport Switch' incorporates:
   *  Constant: '<S24>/Constant1'
   *  Constant: '<S24>/select input'
   */
  switch ((int32_T)drum_bench_working_P.config.input.input1.analog.select_input)
  {
   case 0:
    drum_bench_working_B.MultiportSwitch_h = 0.0;
    break;

   case 1:
    drum_bench_working_B.MultiportSwitch_h = drum_bench_working_B.SineWave_ff;
    break;

   case 2:
    drum_bench_working_B.MultiportSwitch_h = drum_bench_working_B.Sign_ml;
    break;

   case 3:
    drum_bench_working_B.MultiportSwitch_h = drum_bench_working_B.Output_i;
    break;

   default:
    drum_bench_working_B.MultiportSwitch_h = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S24>/Multiport Switch' */

  /* Gain: '<S24>/Gain' */
  drum_bench_working_B.Gain_ncb =
    drum_bench_working_P.config.input.input1.analog.amplitude * 0.0;

  /* Clock: '<S30>/Clock1' */
  drum_bench_working_B.Clock1_f = drum_bench_working_M->Timing.t[0];

  /* Product: '<S30>/Product' incorporates:
   *  Constant: '<S30>/deltaFreq'
   *  Constant: '<S30>/targetTime'
   */
  u = drum_bench_working_P.config.input.input1.motor_iq.frequency - 0.1;
  u *= 6.2831853071795862;
  drum_bench_working_B.Product_ie = u / 100.0;

  /* Gain: '<S30>/Gain' */
  drum_bench_working_B.Gain_d = 0.5 * drum_bench_working_B.Product_ie;

  /* Product: '<S30>/Product1' */
  drum_bench_working_B.Product1_p = drum_bench_working_B.Clock1_f *
    drum_bench_working_B.Gain_d;

  /* Sum: '<S30>/Sum' incorporates:
   *  Constant: '<S30>/initialFreq'
   */
  drum_bench_working_B.Sum_c3 = drum_bench_working_B.Product1_p +
    0.62831853071795862;

  /* Product: '<S30>/Product2' */
  drum_bench_working_B.Product2_np = drum_bench_working_B.Clock1_f *
    drum_bench_working_B.Sum_c3;

  /* Trigonometry: '<S30>/Output' */
  drum_bench_working_B.Output_f = sin(drum_bench_working_B.Product2_np);

  /* Sin: '<S25>/Sine Wave' */
  drum_bench_working_B.SineWave_hf = sin
    (drum_bench_working_P.config.input.input1.motor_iq.frequency *
     drum_bench_working_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S25>/Sign' */
  u = drum_bench_working_B.SineWave_hf;
  if (u < 0.0) {
    drum_bench_working_B.Sign_h = -1.0;
  } else if (u > 0.0) {
    drum_bench_working_B.Sign_h = 1.0;
  } else if (u == 0.0) {
    drum_bench_working_B.Sign_h = 0.0;
  } else {
    drum_bench_working_B.Sign_h = u;
  }

  /* End of Signum: '<S25>/Sign' */

  /* MultiPortSwitch: '<S25>/Multiport Switch' incorporates:
   *  Constant: '<S25>/Constant1'
   *  Constant: '<S25>/select input'
   */
  switch ((int32_T)
          drum_bench_working_P.config.input.input1.motor_iq.select_input) {
   case 0:
    drum_bench_working_B.MultiportSwitch_o = 0.0;
    break;

   case 1:
    drum_bench_working_B.MultiportSwitch_o = drum_bench_working_B.SineWave_hf;
    break;

   case 2:
    drum_bench_working_B.MultiportSwitch_o = drum_bench_working_B.Sign_h;
    break;

   case 3:
    drum_bench_working_B.MultiportSwitch_o = drum_bench_working_B.Output_f;
    break;

   default:
    drum_bench_working_B.MultiportSwitch_o = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S25>/Multiport Switch' */

  /* Gain: '<S25>/Gain' */
  drum_bench_working_B.Gain_gv =
    drum_bench_working_P.config.input.input1.motor_iq.amplitude * 0.0;

  /* Clock: '<S31>/Clock1' */
  drum_bench_working_B.Clock1_l = drum_bench_working_M->Timing.t[0];

  /* Product: '<S31>/Product' incorporates:
   *  Constant: '<S31>/deltaFreq'
   *  Constant: '<S31>/targetTime'
   */
  u = drum_bench_working_P.config.input.input1.motor_vq.frequency - 0.1;
  u *= 6.2831853071795862;
  drum_bench_working_B.Product_k = u / 100.0;

  /* Gain: '<S31>/Gain' */
  drum_bench_working_B.Gain_h = 0.5 * drum_bench_working_B.Product_k;

  /* Product: '<S31>/Product1' */
  drum_bench_working_B.Product1_pt = drum_bench_working_B.Clock1_l *
    drum_bench_working_B.Gain_h;

  /* Sum: '<S31>/Sum' incorporates:
   *  Constant: '<S31>/initialFreq'
   */
  drum_bench_working_B.Sum_dl = drum_bench_working_B.Product1_pt +
    0.62831853071795862;

  /* Product: '<S31>/Product2' */
  drum_bench_working_B.Product2_j = drum_bench_working_B.Clock1_l *
    drum_bench_working_B.Sum_dl;

  /* Trigonometry: '<S31>/Output' */
  drum_bench_working_B.Output_e = sin(drum_bench_working_B.Product2_j);

  /* Sin: '<S26>/Sine Wave' */
  drum_bench_working_B.SineWave_g = sin
    (drum_bench_working_P.config.input.input1.motor_vq.frequency *
     drum_bench_working_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S26>/Sign' */
  u = drum_bench_working_B.SineWave_g;
  if (u < 0.0) {
    drum_bench_working_B.Sign_k = -1.0;
  } else if (u > 0.0) {
    drum_bench_working_B.Sign_k = 1.0;
  } else if (u == 0.0) {
    drum_bench_working_B.Sign_k = 0.0;
  } else {
    drum_bench_working_B.Sign_k = u;
  }

  /* End of Signum: '<S26>/Sign' */

  /* MultiPortSwitch: '<S26>/Multiport Switch' incorporates:
   *  Constant: '<S26>/Constant1'
   *  Constant: '<S26>/select input'
   */
  switch ((int32_T)
          drum_bench_working_P.config.input.input1.motor_vq.select_input) {
   case 0:
    drum_bench_working_B.MultiportSwitch_a = 0.0;
    break;

   case 1:
    drum_bench_working_B.MultiportSwitch_a = drum_bench_working_B.SineWave_g;
    break;

   case 2:
    drum_bench_working_B.MultiportSwitch_a = drum_bench_working_B.Sign_k;
    break;

   case 3:
    drum_bench_working_B.MultiportSwitch_a = drum_bench_working_B.Output_e;
    break;

   default:
    drum_bench_working_B.MultiportSwitch_a = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S26>/Multiport Switch' */

  /* Gain: '<S26>/Gain' */
  drum_bench_working_B.Gain_i =
    drum_bench_working_P.config.input.input1.motor_vq.amplitude * 0.0;

  /* Clock: '<S32>/Clock1' */
  drum_bench_working_B.Clock1_d = drum_bench_working_M->Timing.t[0];

  /* Product: '<S32>/Product' incorporates:
   *  Constant: '<S32>/deltaFreq'
   *  Constant: '<S32>/targetTime'
   */
  u = drum_bench_working_P.config.input.input1.quadrature_1_angle.frequency -
    0.1;
  u *= 6.2831853071795862;
  drum_bench_working_B.Product_kg = u / 100.0;

  /* Gain: '<S32>/Gain' */
  drum_bench_working_B.Gain_c = 0.5 * drum_bench_working_B.Product_kg;

  /* Product: '<S32>/Product1' */
  drum_bench_working_B.Product1_j = drum_bench_working_B.Clock1_d *
    drum_bench_working_B.Gain_c;

  /* Sum: '<S32>/Sum' incorporates:
   *  Constant: '<S32>/initialFreq'
   */
  drum_bench_working_B.Sum_en = drum_bench_working_B.Product1_j +
    0.62831853071795862;

  /* Product: '<S32>/Product2' */
  drum_bench_working_B.Product2_g = drum_bench_working_B.Clock1_d *
    drum_bench_working_B.Sum_en;

  /* Trigonometry: '<S32>/Output' */
  drum_bench_working_B.Output_m = sin(drum_bench_working_B.Product2_g);

  /* Sin: '<S27>/Sine Wave' */
  drum_bench_working_B.SineWave_m = sin
    (drum_bench_working_P.config.input.input1.quadrature_1_angle.frequency *
     drum_bench_working_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S27>/Sign' */
  u = drum_bench_working_B.SineWave_m;
  if (u < 0.0) {
    drum_bench_working_B.Sign_l = -1.0;
  } else if (u > 0.0) {
    drum_bench_working_B.Sign_l = 1.0;
  } else if (u == 0.0) {
    drum_bench_working_B.Sign_l = 0.0;
  } else {
    drum_bench_working_B.Sign_l = u;
  }

  /* End of Signum: '<S27>/Sign' */

  /* MultiPortSwitch: '<S27>/Multiport Switch' incorporates:
   *  Constant: '<S27>/Constant1'
   *  Constant: '<S27>/select input'
   */
  switch ((int32_T)
          drum_bench_working_P.config.input.input1.quadrature_1_angle.select_input)
  {
   case 0:
    drum_bench_working_B.MultiportSwitch_i = 0.0;
    break;

   case 1:
    drum_bench_working_B.MultiportSwitch_i = drum_bench_working_B.SineWave_m;
    break;

   case 2:
    drum_bench_working_B.MultiportSwitch_i = drum_bench_working_B.Sign_l;
    break;

   case 3:
    drum_bench_working_B.MultiportSwitch_i = drum_bench_working_B.Output_m;
    break;

   default:
    drum_bench_working_B.MultiportSwitch_i = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S27>/Multiport Switch' */

  /* Gain: '<S27>/Gain' */
  drum_bench_working_B.Gain_m =
    drum_bench_working_P.config.input.input1.quadrature_1_angle.amplitude * 0.0;

  /* Clock: '<S33>/Clock1' */
  drum_bench_working_B.Clock1_h = drum_bench_working_M->Timing.t[0];

  /* Product: '<S33>/Product' incorporates:
   *  Constant: '<S33>/deltaFreq'
   *  Constant: '<S33>/targetTime'
   */
  u = drum_bench_working_P.config.input.input1.quadrature_2_angle.frequency -
    0.1;
  u *= 6.2831853071795862;
  drum_bench_working_B.Product_if = u / 100.0;

  /* Gain: '<S33>/Gain' */
  drum_bench_working_B.Gain_p = 0.5 * drum_bench_working_B.Product_if;

  /* Product: '<S33>/Product1' */
  drum_bench_working_B.Product1_j2 = drum_bench_working_B.Clock1_h *
    drum_bench_working_B.Gain_p;

  /* Sum: '<S33>/Sum' incorporates:
   *  Constant: '<S33>/initialFreq'
   */
  drum_bench_working_B.Sum_ke = drum_bench_working_B.Product1_j2 +
    0.62831853071795862;

  /* Product: '<S33>/Product2' */
  drum_bench_working_B.Product2_gp = drum_bench_working_B.Clock1_h *
    drum_bench_working_B.Sum_ke;

  /* Trigonometry: '<S33>/Output' */
  drum_bench_working_B.Output_l = sin(drum_bench_working_B.Product2_gp);

  /* Sin: '<S28>/Sine Wave' */
  drum_bench_working_B.SineWave_ld = sin
    (drum_bench_working_P.config.input.input1.quadrature_2_angle.frequency *
     drum_bench_working_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S28>/Sign' */
  u = drum_bench_working_B.SineWave_ld;
  if (u < 0.0) {
    drum_bench_working_B.Sign_ip = -1.0;
  } else if (u > 0.0) {
    drum_bench_working_B.Sign_ip = 1.0;
  } else if (u == 0.0) {
    drum_bench_working_B.Sign_ip = 0.0;
  } else {
    drum_bench_working_B.Sign_ip = u;
  }

  /* End of Signum: '<S28>/Sign' */

  /* MultiPortSwitch: '<S28>/Multiport Switch' incorporates:
   *  Constant: '<S28>/Constant1'
   *  Constant: '<S28>/select input'
   */
  switch ((int32_T)
          drum_bench_working_P.config.input.input1.quadrature_2_angle.select_input)
  {
   case 0:
    drum_bench_working_B.MultiportSwitch_k = 0.0;
    break;

   case 1:
    drum_bench_working_B.MultiportSwitch_k = drum_bench_working_B.SineWave_ld;
    break;

   case 2:
    drum_bench_working_B.MultiportSwitch_k = drum_bench_working_B.Sign_ip;
    break;

   case 3:
    drum_bench_working_B.MultiportSwitch_k = drum_bench_working_B.Output_l;
    break;

   default:
    drum_bench_working_B.MultiportSwitch_k = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S28>/Multiport Switch' */

  /* Gain: '<S28>/Gain' */
  drum_bench_working_B.Gain_nh =
    drum_bench_working_P.config.input.input1.quadrature_2_angle.amplitude * 0.0;

  /* BusSelector: '<S8>/Bus Selector' */
  drum_bench_working_B.rpc_l = drum_bench_working_B.modeswitch1.rpc;

  /* UnitDelay: '<S130>/Output' */
  drum_bench_working_B.Output_i4 = drum_bench_working_DW.Output_DSTATE_h;

  /* Sum: '<S131>/FixPt Sum1' incorporates:
   *  Constant: '<S131>/FixPt Constant'
   */
  drum_bench_working_B.FixPtSum1_e = drum_bench_working_B.Output_i4 + 1U;

  /* Switch: '<S132>/FixPt Switch' incorporates:
   *  Constant: '<S132>/Constant'
   */
  if (drum_bench_working_B.FixPtSum1_e > 4294967295U) {
    drum_bench_working_B.FixPtSwitch_b = 0U;
  } else {
    drum_bench_working_B.FixPtSwitch_b = drum_bench_working_B.FixPtSum1_e;
  }

  /* End of Switch: '<S132>/FixPt Switch' */

  /* DataTypeConversion: '<S129>/debug_' */
  drum_bench_working_B.debug_d = drum_bench_working_B.marlin_ec_o21_m;

  /* BusSelector: '<S126>/Bus Selector' */
  drum_bench_working_B.mode_g = drum_bench_working_B.modeswitch1.mode;

  /* BusSelector: '<S126>/Bus Selector1' */
  drum_bench_working_B.motor_iq_m = drum_bench_working_B.modeswitch1.motor_iq;
  drum_bench_working_B.motor_id_k = drum_bench_working_B.modeswitch1.motor_id;

  /* BusSelector: '<S126>/Bus Selector2' */
  drum_bench_working_B.motor_vq_d = drum_bench_working_B.modeswitch1.motor_vq;
  drum_bench_working_B.motor_vd_m = drum_bench_working_B.modeswitch1.motor_vd;

  /* BusSelector: '<S126>/Bus Selector3' */
  drum_bench_working_B.analog_diff_i =
    drum_bench_working_B.modeswitch1.analog_diff;
  drum_bench_working_B.analog_diff_dot_ky =
    drum_bench_working_B.modeswitch1.analog_diff_dot;

  /* BusSelector: '<S126>/Bus Selector4' */
  drum_bench_working_B.quadrature_1_angle_c =
    drum_bench_working_B.modeswitch1.quadrature_1_angle;
  drum_bench_working_B.quadrature_1_angle_dot_d =
    drum_bench_working_B.modeswitch1.quadrature_1_angle_dot;

  /* BusSelector: '<S126>/Bus Selector5' */
  drum_bench_working_B.motor_torque_hy =
    drum_bench_working_B.modeswitch1.motor_torque;

  /* MultiPortSwitch: '<S126>/Multiport Switch' incorporates:
   *  Constant: '<S133>/mode'
   *  Constant: '<S134>/mode'
   *  Constant: '<S135>/mode'
   *  Constant: '<S136>/mode'
   *  Constant: '<S137>/mode'
   *  Constant: '<S138>/mode'
   *  Constant: '<S139>/mode'
   *  Constant: '<S140>/mode'
   */
  switch ((int32_T)drum_bench_working_B.mode_g) {
   case 0:
    drum_bench_working_B.mode_e = 0U;
    break;

   case 1:
    drum_bench_working_B.mode_e = 1U;
    break;

   case 2:
    drum_bench_working_B.mode_e = 2U;
    break;

   case 3:
    drum_bench_working_B.mode_e = 3U;
    break;

   case 4:
    drum_bench_working_B.mode_e = 4U;
    break;

   case 5:
    drum_bench_working_B.mode_e = 5U;
    break;

   case 6:
    drum_bench_working_B.mode_e = 6U;
    break;

   case 7:
    drum_bench_working_B.mode_e = 8U;
    break;

   default:
    drum_bench_working_B.mode_e = 0U;
    break;
  }

  for (i = 0; i < 14; i++) {
    /* DataTypeConversion: '<S137>/Data Type Conversion1' incorporates:
     *  Constant: '<S137>/filler'
     */
    drum_bench_working_B.DataTypeConversion1_d[i] = (real32_T)
      drum_bench_working_ConstP.pooled16[i];

    /* DataTypeConversion: '<S134>/Data Type Conversion1' incorporates:
     *  Constant: '<S134>/filler'
     */
    drum_bench_working_B.DataTypeConversion1_bm[i] = (real32_T)
      drum_bench_working_ConstP.pooled16[i];
  }

  /* End of MultiPortSwitch: '<S126>/Multiport Switch' */

  /* Pack: <S137>/Byte Pack3 */
  (void) memcpy(&drum_bench_working_B.command_li[0],
                &drum_bench_working_B.DataTypeConversion1_d[0],
                56);

  /* Pack: <S134>/Byte Pack3 */
  (void) memcpy(&drum_bench_working_B.command_n[0],
                &drum_bench_working_B.DataTypeConversion1_bm[0],
                56);

  /* DataTypeConversion: '<S138>/Data Type Conversion1' incorporates:
   *  Constant: '<S126>/lock_current'
   *  Constant: '<S138>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_ev[0] = (real32_T)
    drum_bench_working_P.config.marlins.M1.motor.phase_lock_current;
  for (i = 0; i < 13; i++) {
    drum_bench_working_B.DataTypeConversion1_ev[i + 1] = (real32_T)
      drum_bench_working_ConstP.pooled17[i];
  }

  /* End of DataTypeConversion: '<S138>/Data Type Conversion1' */

  /* Pack: <S138>/Byte Pack3 */
  (void) memcpy(&drum_bench_working_B.command_o[0],
                &drum_bench_working_B.DataTypeConversion1_ev[0],
                56);

  /* DataTypeConversion: '<S135>/Data Type Conversion1' incorporates:
   *  Constant: '<S135>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_a[0] = (real32_T)
    drum_bench_working_B.motor_iq_m;
  drum_bench_working_B.DataTypeConversion1_a[1] = (real32_T)
    drum_bench_working_B.motor_id_k;
  for (i = 0; i < 12; i++) {
    drum_bench_working_B.DataTypeConversion1_a[i + 2] = (real32_T)
      drum_bench_working_ConstP.pooled18[i];
  }

  /* End of DataTypeConversion: '<S135>/Data Type Conversion1' */

  /* Pack: <S135>/Byte Pack2 */
  (void) memcpy(&drum_bench_working_B.command_i[0],
                &drum_bench_working_B.DataTypeConversion1_a[0],
                56);

  /* DataTypeConversion: '<S140>/Data Type Conversion1' incorporates:
   *  Constant: '<S140>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_ey[0] = (real32_T)
    drum_bench_working_B.motor_vq_d;
  drum_bench_working_B.DataTypeConversion1_ey[1] = (real32_T)
    drum_bench_working_B.motor_vd_m;
  for (i = 0; i < 12; i++) {
    drum_bench_working_B.DataTypeConversion1_ey[i + 2] = (real32_T)
      drum_bench_working_ConstP.pooled18[i];
  }

  /* End of DataTypeConversion: '<S140>/Data Type Conversion1' */

  /* Pack: <S140>/Byte Pack2 */
  (void) memcpy(&drum_bench_working_B.command_h[0],
                &drum_bench_working_B.DataTypeConversion1_ey[0],
                56);

  /* DataTypeConversion: '<S133>/Data Type Conversion1' incorporates:
   *  Constant: '<S133>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_fj[0] = (real32_T)
    drum_bench_working_B.analog_diff_i;
  drum_bench_working_B.DataTypeConversion1_fj[1] = (real32_T)
    drum_bench_working_B.analog_diff_dot_ky;
  for (i = 0; i < 12; i++) {
    drum_bench_working_B.DataTypeConversion1_fj[i + 2] = (real32_T)
      drum_bench_working_ConstP.pooled18[i];
  }

  /* End of DataTypeConversion: '<S133>/Data Type Conversion1' */

  /* Pack: <S133>/Byte Pack2 */
  (void) memcpy(&drum_bench_working_B.command_hl[0],
                &drum_bench_working_B.DataTypeConversion1_fj[0],
                56);

  /* DataTypeConversion: '<S139>/Data Type Conversion1' incorporates:
   *  Constant: '<S139>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_aj[0] = (real32_T)
    drum_bench_working_B.quadrature_1_angle_c;
  drum_bench_working_B.DataTypeConversion1_aj[1] = (real32_T)
    drum_bench_working_B.quadrature_1_angle_dot_d;
  for (i = 0; i < 12; i++) {
    drum_bench_working_B.DataTypeConversion1_aj[i + 2] = (real32_T)
      drum_bench_working_ConstP.pooled18[i];
  }

  /* End of DataTypeConversion: '<S139>/Data Type Conversion1' */

  /* Pack: <S139>/Byte Pack2 */
  (void) memcpy(&drum_bench_working_B.command_gh[0],
                &drum_bench_working_B.DataTypeConversion1_aj[0],
                56);

  /* DataTypeConversion: '<S136>/Data Type Conversion1' incorporates:
   *  Constant: '<S136>/filler'
   */
  drum_bench_working_B.DataTypeConversion1_c[0] = (real32_T)
    drum_bench_working_B.motor_torque_hy;
  for (i = 0; i < 13; i++) {
    drum_bench_working_B.DataTypeConversion1_c[i + 1] = (real32_T)
      drum_bench_working_ConstP.pooled17[i];
  }

  /* End of DataTypeConversion: '<S136>/Data Type Conversion1' */

  /* Pack: <S136>/Byte Pack2 */
  (void) memcpy(&drum_bench_working_B.command_jo[0],
                &drum_bench_working_B.DataTypeConversion1_c[0],
                56);

  /* MultiPortSwitch: '<S126>/Multiport Switch' */
  switch ((int32_T)drum_bench_working_B.mode_g) {
   case 0:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_c[i] = drum_bench_working_B.command_li[i];
    }
    break;

   case 1:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_c[i] = drum_bench_working_B.command_n[i];
    }
    break;

   case 2:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_c[i] = drum_bench_working_B.command_o[i];
    }
    break;

   case 3:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_c[i] = drum_bench_working_B.command_i[i];
    }
    break;

   case 4:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_c[i] = drum_bench_working_B.command_h[i];
    }
    break;

   case 5:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_c[i] = drum_bench_working_B.command_hl[i];
    }
    break;

   case 6:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_c[i] = drum_bench_working_B.command_gh[i];
    }
    break;

   case 7:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_c[i] = drum_bench_working_B.command_jo[i];
    }
    break;

   default:
    for (i = 0; i < 56; i++) {
      drum_bench_working_B.command_c[i] = drum_bench_working_B.command_li[i];
    }
    break;
  }

  /* End of MultiPortSwitch: '<S126>/Multiport Switch' */

  /* BusCreator: '<S141>/Bus Creator' incorporates:
   *  Constant: '<S141>/id'
   *  Constant: '<S141>/type'
   */
  drum_bench_working_B.BusCreator_br.type = 6;
  drum_bench_working_B.BusCreator_br.id = 0.0F;

  /* BusCreator: '<S142>/Bus Creator' incorporates:
   *  Constant: '<S142>/id'
   *  Constant: '<S142>/type'
   */
  drum_bench_working_B.BusCreator_ih.type = 7;
  drum_bench_working_B.BusCreator_ih.id = 1.0F;

  /* BusCreator: '<S143>/Bus Creator' incorporates:
   *  Constant: '<S143>/id'
   *  Constant: '<S143>/type'
   */
  drum_bench_working_B.BusCreator_g1.type = 7;
  drum_bench_working_B.BusCreator_g1.id = 0.0F;

  /* BusCreator: '<S144>/Bus Creator' incorporates:
   *  Constant: '<S144>/id'
   *  Constant: '<S144>/type'
   */
  drum_bench_working_B.BusCreator_i2.type = 7;
  drum_bench_working_B.BusCreator_i2.id = 0.0F;

  /* BusCreator: '<S145>/Bus Creator' incorporates:
   *  Constant: '<S145>/id'
   *  Constant: '<S145>/type'
   */
  drum_bench_working_B.BusCreator_lg.type = 7;
  drum_bench_working_B.BusCreator_lg.id = 0.0F;
  for (i = 0; i < 20; i++) {
    /* BusCreator: '<S141>/Bus Creator' */
    drum_bench_working_B.BusCreator_br.data[i] =
      drum_bench_working_ConstB.data_ef[i];

    /* BusCreator: '<S142>/Bus Creator' */
    drum_bench_working_B.BusCreator_ih.data[i] =
      drum_bench_working_ConstB.data_c4[i];

    /* BusCreator: '<S143>/Bus Creator' */
    drum_bench_working_B.BusCreator_g1.data[i] =
      drum_bench_working_ConstB.data_g[i];

    /* BusCreator: '<S144>/Bus Creator' */
    drum_bench_working_B.BusCreator_i2.data[i] =
      drum_bench_working_ConstB.data_b[i];
    drum_bench_working_B.BusCreator_lg.data[i] =
      drum_bench_working_ConstB.data_fw[i];
  }

  /* End of BusCreator: '<S145>/Bus Creator' */

  /* Constant: '<S164>/id' */
  drum_bench_working_B.VectorConcatenate_e[0] =
    drum_bench_working_P.config.marlins.M1.amp.current_zero_a;

  /* Constant: '<S164>/id1' */
  drum_bench_working_B.VectorConcatenate_e[1] =
    drum_bench_working_P.config.marlins.M1.amp.current_zero_b;

  /* Constant: '<S164>/id2' */
  drum_bench_working_B.VectorConcatenate_e[2] =
    drum_bench_working_P.config.marlins.M1.amp.deadband;

  /* Constant: '<S164>/id3' */
  drum_bench_working_B.VectorConcatenate_e[3] =
    drum_bench_working_P.config.marlins.M1.amp.led_brightness;

  /* Constant: '<S164>/id4' */
  drum_bench_working_B.VectorConcatenate_e[4] =
    drum_bench_working_P.config.marlins.M1.amp.nominal_bus_voltage;

  /* Constant: '<S164>/id5' */
  drum_bench_working_B.VectorConcatenate_e[5] =
    drum_bench_working_P.config.marlins.M1.amp.current_sensor_limit;

  /* Constant: '<S178>/id' */
  drum_bench_working_B.VectorConcatenate_e[6] =
    drum_bench_working_P.config.marlins.M1.amp.logger_10kHz.trigger_level;

  /* Constant: '<S177>/id' */
  drum_bench_working_B.VectorConcatenate_e[7] =
    drum_bench_working_P.config.marlins.M1.amp.logger_100kHz.trigger_level;

  /* Constant: '<S170>/id' */
  drum_bench_working_B.VectorConcatenate_e[8] =
    drum_bench_working_P.config.marlins.M1.control_current.current_filter_bandwidth;

  /* Constant: '<S170>/id1' */
  drum_bench_working_B.VectorConcatenate_e[9] =
    drum_bench_working_P.config.marlins.M1.control_current.fingerprint_gain;

  /* Constant: '<S170>/id2' */
  drum_bench_working_B.VectorConcatenate_e[10] =
    drum_bench_working_P.config.marlins.M1.control_current.ki;

  /* Constant: '<S170>/id3' */
  drum_bench_working_B.VectorConcatenate_e[11] =
    drum_bench_working_P.config.marlins.M1.control_current.ki_limit;

  /* Constant: '<S170>/id4' */
  drum_bench_working_B.VectorConcatenate_e[12] =
    drum_bench_working_P.config.marlins.M1.control_current.kp;

  /* Constant: '<S184>/id' */
  drum_bench_working_B.VectorConcatenate_e[13] =
    drum_bench_working_P.config.marlins.M1.control_motor_angle.pid_deadband.deadband;

  /* Constant: '<S185>/id' */
  drum_bench_working_B.VectorConcatenate_e[14] =
    drum_bench_working_P.config.marlins.M1.control_motor_angle.pid_interpolate.enabled;

  /* Constant: '<S183>/id' */
  drum_bench_working_B.VectorConcatenate_e[15] =
    drum_bench_working_P.config.marlins.M1.control_motor_angle.pid.kd;

  /* Constant: '<S183>/id1' */
  drum_bench_working_B.VectorConcatenate_e[16] =
    drum_bench_working_P.config.marlins.M1.control_motor_angle.pid.ki;

  /* Constant: '<S183>/id2' */
  drum_bench_working_B.VectorConcatenate_e[17] =
    drum_bench_working_P.config.marlins.M1.control_motor_angle.pid.ki_limit;

  /* Constant: '<S183>/id3' */
  drum_bench_working_B.VectorConcatenate_e[18] =
    drum_bench_working_P.config.marlins.M1.control_motor_angle.pid.kp;

  /* Constant: '<S186>/id' */
  drum_bench_working_B.VectorConcatenate_e[19] =
    drum_bench_working_P.config.marlins.M1.control_quadrature_2_angle.pid.kd;

  /* Constant: '<S186>/id1' */
  drum_bench_working_B.VectorConcatenate_e[20] =
    drum_bench_working_P.config.marlins.M1.control_quadrature_2_angle.pid.ki;

  /* Constant: '<S186>/id2' */
  drum_bench_working_B.VectorConcatenate_e[21] =
    drum_bench_working_P.config.marlins.M1.control_quadrature_2_angle.pid.ki_limit;

  /* Constant: '<S186>/id3' */
  drum_bench_working_B.VectorConcatenate_e[22] =
    drum_bench_working_P.config.marlins.M1.control_quadrature_2_angle.pid.kp;

  /* Constant: '<S182>/id' */
  drum_bench_working_B.VectorConcatenate_e[23] =
    drum_bench_working_P.config.marlins.M1.control_analog.pid.kd;

  /* Constant: '<S182>/id1' */
  drum_bench_working_B.VectorConcatenate_e[24] =
    drum_bench_working_P.config.marlins.M1.control_analog.pid.ki;

  /* Constant: '<S182>/id2' */
  drum_bench_working_B.VectorConcatenate_e[25] =
    drum_bench_working_P.config.marlins.M1.control_analog.pid.ki_limit;

  /* Constant: '<S182>/id3' */
  drum_bench_working_B.VectorConcatenate_e[26] =
    drum_bench_working_P.config.marlins.M1.control_analog.pid.kp;

  /* Constant: '<S169>/id' */
  drum_bench_working_B.VectorConcatenate_e[27] =
    drum_bench_working_P.config.marlins.M1.control_analog.source;

  /* Constant: '<S173>/id' */
  drum_bench_working_B.VectorConcatenate_e[28] =
    drum_bench_working_P.config.marlins.M1.motor.current_limit;

  /* Constant: '<S173>/id1' */
  drum_bench_working_B.VectorConcatenate_e[29] =
    drum_bench_working_P.config.marlins.M1.motor.voltage_limit;

  /* Constant: '<S173>/id2' */
  drum_bench_working_B.VectorConcatenate_e[30] =
    drum_bench_working_P.config.marlins.M1.motor.encoder.counts_per_revolution;

  /* Constant: '<S173>/id3' */
  drum_bench_working_B.VectorConcatenate_e[31] =
    drum_bench_working_P.config.marlins.M1.motor.km;

  /* Constant: '<S173>/id4' */
  drum_bench_working_B.VectorConcatenate_e[32] =
    drum_bench_working_P.config.marlins.M1.motor.num_poles;

  /* Constant: '<S173>/id5' */
  drum_bench_working_B.VectorConcatenate_e[33] =
    drum_bench_working_P.config.marlins.M1.motor.resistance;

  /* Constant: '<S173>/id6' */
  drum_bench_working_B.VectorConcatenate_e[34] =
    drum_bench_working_P.config.marlins.M1.motor.electrical_offset;

  /* Constant: '<S173>/id7' */
  drum_bench_working_B.VectorConcatenate_e[35] =
    drum_bench_working_P.config.marlins.M1.motor.v_per_a;

  /* Constant: '<S168>/id' */
  drum_bench_working_B.VectorConcatenate_e[36] =
    drum_bench_working_P.config.marlins.M1.bridge.thermal_model.eA1;

  /* Constant: '<S168>/id1' */
  drum_bench_working_B.VectorConcatenate_e[37] =
    drum_bench_working_P.config.marlins.M1.bridge.thermal_model.eA2;

  /* Constant: '<S187>/id' */
  drum_bench_working_B.VectorConcatenate_e[38] =
    drum_bench_working_P.config.marlins.M1.quadrature_1.sensor.bias;

  /* Constant: '<S187>/id1' */
  drum_bench_working_B.VectorConcatenate_e[39] =
    drum_bench_working_P.config.marlins.M1.quadrature_1.sensor.gain;

  /* Constant: '<S187>/id6' */
  drum_bench_working_B.VectorConcatenate_e[40] =
    drum_bench_working_P.config.marlins.M1.quadrature_1.sensor.limit_high;

  /* Constant: '<S187>/id3' */
  drum_bench_working_B.VectorConcatenate_e[41] =
    drum_bench_working_P.config.marlins.M1.quadrature_1.sensor.limit_low;

  /* Constant: '<S187>/id4' */
  drum_bench_working_B.VectorConcatenate_e[42] =
    drum_bench_working_P.config.marlins.M1.quadrature_1.sensor.filter_bandwidth;

  /* Constant: '<S187>/id5' */
  drum_bench_working_B.VectorConcatenate_e[43] =
    drum_bench_working_P.config.marlins.M1.quadrature_1.sensor.dot_filter_bandwidth;

  /* Constant: '<S174>/id' */
  drum_bench_working_B.VectorConcatenate_e[44] =
    drum_bench_working_P.config.marlins.M1.quadrature_1.enable_index;

  /* Constant: '<S188>/id' */
  drum_bench_working_B.VectorConcatenate_e[45] =
    drum_bench_working_P.config.marlins.M1.quadrature_2.sensor.bias;

  /* Constant: '<S188>/id1' */
  drum_bench_working_B.VectorConcatenate_e[46] =
    drum_bench_working_P.config.marlins.M1.quadrature_2.sensor.gain;

  /* Constant: '<S188>/id6' */
  drum_bench_working_B.VectorConcatenate_e[47] =
    drum_bench_working_P.config.marlins.M1.quadrature_2.sensor.limit_high;

  /* Constant: '<S188>/id3' */
  drum_bench_working_B.VectorConcatenate_e[48] =
    drum_bench_working_P.config.marlins.M1.quadrature_2.sensor.limit_low;

  /* Constant: '<S188>/id4' */
  drum_bench_working_B.VectorConcatenate_e[49] =
    drum_bench_working_P.config.marlins.M1.quadrature_2.sensor.filter_bandwidth;

  /* Constant: '<S188>/id5' */
  drum_bench_working_B.VectorConcatenate_e[50] =
    drum_bench_working_P.config.marlins.M1.quadrature_2.sensor.dot_filter_bandwidth;

  /* Constant: '<S175>/id' */
  drum_bench_working_B.VectorConcatenate_e[51] =
    drum_bench_working_P.config.marlins.M1.quadrature_2.enable_index;

  /* Constant: '<S179>/id' */
  drum_bench_working_B.VectorConcatenate_e[52] =
    drum_bench_working_P.config.marlins.M1.analog_1.sensor.bias;

  /* Constant: '<S179>/id1' */
  drum_bench_working_B.VectorConcatenate_e[53] =
    drum_bench_working_P.config.marlins.M1.analog_1.sensor.gain;

  /* Constant: '<S179>/id6' */
  drum_bench_working_B.VectorConcatenate_e[54] =
    drum_bench_working_P.config.marlins.M1.analog_1.sensor.limit_high;

  /* Constant: '<S179>/id3' */
  drum_bench_working_B.VectorConcatenate_e[55] =
    drum_bench_working_P.config.marlins.M1.analog_1.sensor.limit_low;

  /* Constant: '<S179>/id4' */
  drum_bench_working_B.VectorConcatenate_e[56] =
    drum_bench_working_P.config.marlins.M1.analog_1.sensor.filter_bandwidth;

  /* Constant: '<S179>/id5' */
  drum_bench_working_B.VectorConcatenate_e[57] =
    drum_bench_working_P.config.marlins.M1.analog_1.sensor.dot_filter_bandwidth;

  /* Constant: '<S180>/id' */
  drum_bench_working_B.VectorConcatenate_e[58] =
    drum_bench_working_P.config.marlins.M1.analog_2.sensor.bias;

  /* Constant: '<S180>/id1' */
  drum_bench_working_B.VectorConcatenate_e[59] =
    drum_bench_working_P.config.marlins.M1.analog_2.sensor.gain;

  /* Constant: '<S180>/id6' */
  drum_bench_working_B.VectorConcatenate_e[60] =
    drum_bench_working_P.config.marlins.M1.analog_2.sensor.limit_high;

  /* Constant: '<S180>/id3' */
  drum_bench_working_B.VectorConcatenate_e[61] =
    drum_bench_working_P.config.marlins.M1.analog_2.sensor.limit_low;

  /* Constant: '<S180>/id4' */
  drum_bench_working_B.VectorConcatenate_e[62] =
    drum_bench_working_P.config.marlins.M1.analog_2.sensor.filter_bandwidth;

  /* Constant: '<S180>/id5' */
  drum_bench_working_B.VectorConcatenate_e[63] =
    drum_bench_working_P.config.marlins.M1.analog_2.sensor.dot_filter_bandwidth;

  /* Constant: '<S181>/id' */
  drum_bench_working_B.VectorConcatenate_e[64] =
    drum_bench_working_P.config.marlins.M1.analog_diff.sensor.bias;

  /* Constant: '<S181>/id1' */
  drum_bench_working_B.VectorConcatenate_e[65] =
    drum_bench_working_P.config.marlins.M1.analog_diff.sensor.gain;

  /* Constant: '<S181>/id6' */
  drum_bench_working_B.VectorConcatenate_e[66] =
    drum_bench_working_P.config.marlins.M1.analog_diff.sensor.limit_high;

  /* Constant: '<S181>/id3' */
  drum_bench_working_B.VectorConcatenate_e[67] =
    drum_bench_working_P.config.marlins.M1.analog_diff.sensor.limit_low;

  /* Constant: '<S181>/id4' */
  drum_bench_working_B.VectorConcatenate_e[68] =
    drum_bench_working_P.config.marlins.M1.analog_diff.sensor.filter_bandwidth;

  /* Constant: '<S181>/id5' */
  drum_bench_working_B.VectorConcatenate_e[69] =
    drum_bench_working_P.config.marlins.M1.analog_diff.sensor.dot_filter_bandwidth;

  /* Constant: '<S189>/id' */
  drum_bench_working_B.VectorConcatenate_e[70] =
    drum_bench_working_P.config.marlins.M1.ssi.sensor.bias;

  /* Constant: '<S189>/id1' */
  drum_bench_working_B.VectorConcatenate_e[71] =
    drum_bench_working_P.config.marlins.M1.ssi.sensor.gain;

  /* Constant: '<S189>/id6' */
  drum_bench_working_B.VectorConcatenate_e[72] =
    drum_bench_working_P.config.marlins.M1.ssi.sensor.limit_high;

  /* Constant: '<S189>/id3' */
  drum_bench_working_B.VectorConcatenate_e[73] =
    drum_bench_working_P.config.marlins.M1.ssi.sensor.limit_low;

  /* Constant: '<S189>/id4' */
  drum_bench_working_B.VectorConcatenate_e[74] =
    drum_bench_working_P.config.marlins.M1.ssi.sensor.filter_bandwidth;

  /* Constant: '<S189>/id5' */
  drum_bench_working_B.VectorConcatenate_e[75] =
    drum_bench_working_P.config.marlins.M1.ssi.sensor.dot_filter_bandwidth;

  /* DataTypeConversion: '<S162>/Data Type Conversion' */
  for (i = 0; i < 76; i++) {
    drum_bench_working_B.DataTypeConversion_aa[i] = (real32_T)
      drum_bench_working_B.VectorConcatenate_e[i];
  }

  /* End of DataTypeConversion: '<S162>/Data Type Conversion' */

  /* Unpack: <S128>/Byte Unpack */
  (void) memcpy(&drum_bench_working_B.type_b,
                &drum_bench_working_B.rpa_packet_b[0],
                4);
  (void) memcpy(&drum_bench_working_B.id_p, &drum_bench_working_B.rpa_packet_b[4],
                4);
  (void) memcpy(&drum_bench_working_B.data_a[0],
                &drum_bench_working_B.rpa_packet_b[8],
                20);

  /* BusCreator: '<S161>/BusConversion_InsertedFor_MATLAB Function1_at_inport_1' */
  drum_bench_working_B.BusConversion_InsertedFor_MAT_c.type =
    drum_bench_working_B.type_b;
  drum_bench_working_B.BusConversion_InsertedFor_MAT_c.id =
    drum_bench_working_B.id_p;
  for (i = 0; i < 20; i++) {
    drum_bench_working_B.BusConversion_InsertedFor_MAT_c.data[i] =
      drum_bench_working_B.data_a[i];
  }

  /* End of BusCreator: '<S161>/BusConversion_InsertedFor_MATLAB Function1_at_inport_1' */

  /* MATLAB Function: '<S161>/MATLAB Function1' incorporates:
   *  Constant: '<S161>/Constant2'
   */
  drum_bench_work_MATLABFunction1(drum_bench_working_B.DataTypeConversion_aa,
    &drum_bench_working_B.BusConversion_InsertedFor_MAT_c, 1U,
    &drum_bench_working_B.sf_MATLABFunction1_a,
    &drum_bench_working_DW.sf_MATLABFunction1_a);

  /* Pack: <S161>/Byte Pack */
  (void) memcpy(&drum_bench_working_B.data_e5[0],
                &drum_bench_working_ConstP.pooled30,
                4);
  (void) memcpy(&drum_bench_working_B.data_e5[4],
                &drum_bench_working_B.sf_MATLABFunction1_a.index,
                4);
  (void) memcpy(&drum_bench_working_B.data_e5[8],
                &drum_bench_working_B.sf_MATLABFunction1_a.value,
                4);
  (void) memcpy(&drum_bench_working_B.data_e5[12],
                drum_bench_working_ConstP.pooled39,
                8);

  /* BusCreator: '<S161>/Bus Creator' */
  drum_bench_working_B.BusCreator_j.type =
    drum_bench_working_B.sf_MATLABFunction1_a.type;
  drum_bench_working_B.BusCreator_j.id =
    drum_bench_working_B.sf_MATLABFunction1_a.id;

  /* BusCreator: '<S153>/Bus Creator' incorporates:
   *  Constant: '<S153>/id'
   *  Constant: '<S153>/type'
   */
  drum_bench_working_B.BusCreator_dy.type = 1;
  drum_bench_working_B.BusCreator_dy.id = 0.0F;

  /* BusCreator: '<S146>/Bus Creator' incorporates:
   *  Constant: '<S146>/id'
   *  Constant: '<S146>/type'
   */
  drum_bench_working_B.BusCreator_d1.type = 2;
  drum_bench_working_B.BusCreator_d1.id = 0.0F;

  /* BusCreator: '<S156>/Bus Creator' incorporates:
   *  Constant: '<S156>/id'
   *  Constant: '<S156>/type'
   */
  drum_bench_working_B.BusCreator_fc.type = 3;
  drum_bench_working_B.BusCreator_fc.id = 0.0F;

  /* BusCreator: '<S155>/Bus Creator' incorporates:
   *  Constant: '<S155>/id'
   *  Constant: '<S155>/type'
   */
  drum_bench_working_B.BusCreator_bw.type = 3;
  drum_bench_working_B.BusCreator_bw.id = 0.0F;

  /* BusCreator: '<S147>/Bus Creator' incorporates:
   *  Constant: '<S147>/id'
   *  Constant: '<S147>/type'
   */
  drum_bench_working_B.BusCreator_ow.type = 3;
  drum_bench_working_B.BusCreator_ow.id = 1.0F;

  /* DataTypeConversion: '<S148>/Data Type Conversion' incorporates:
   *  Constant: '<S148>/value'
   *  Constant: '<S148>/value1'
   */
  u = floor(1.0);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_k[0] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;
  u = floor(drum_bench_working_P.config.marlins.M1.debug_address_index);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_k[1] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;

  /* End of DataTypeConversion: '<S148>/Data Type Conversion' */

  /* Pack: <S148>/Byte Pack */
  (void) memcpy(&drum_bench_working_B.data_o[0],
                &drum_bench_working_B.DataTypeConversion_k[0],
                8);
  (void) memcpy(&drum_bench_working_B.data_o[8],
                drum_bench_working_ConstP.pooled40,
                12);

  /* BusCreator: '<S148>/Bus Creator' incorporates:
   *  Constant: '<S148>/id'
   *  Constant: '<S148>/type'
   */
  drum_bench_working_B.BusCreator_dj.type = 3;
  drum_bench_working_B.BusCreator_dj.id = 1.1F;

  /* DataTypeConversion: '<S149>/Data Type Conversion' incorporates:
   *  Constant: '<S149>/value'
   *  Constant: '<S149>/value1'
   */
  u = floor(1.0);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_k0[0] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;
  u = floor(drum_bench_working_P.config.marlins.M1.debug_address_index);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_k0[1] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;

  /* End of DataTypeConversion: '<S149>/Data Type Conversion' */

  /* Pack: <S149>/Byte Pack */
  (void) memcpy(&drum_bench_working_B.data_bl[0],
                &drum_bench_working_B.DataTypeConversion_k0[0],
                8);
  (void) memcpy(&drum_bench_working_B.data_bl[8],
                drum_bench_working_ConstP.pooled40,
                12);

  /* BusCreator: '<S149>/Bus Creator' incorporates:
   *  Constant: '<S149>/id'
   *  Constant: '<S149>/type'
   */
  drum_bench_working_B.BusCreator_om.type = 3;
  drum_bench_working_B.BusCreator_om.id = 1.1F;

  /* DataTypeConversion: '<S150>/Data Type Conversion' incorporates:
   *  Constant: '<S150>/value'
   *  Constant: '<S150>/value1'
   */
  u = floor(1.0);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_iv[0] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;
  u = floor(drum_bench_working_P.config.marlins.M1.debug_address_index);
  if (rtIsNaN(u) || rtIsInf(u)) {
    u = 0.0;
  } else {
    u = fmod(u, 4.294967296E+9);
  }

  drum_bench_working_B.DataTypeConversion_iv[1] = u < 0.0 ? (uint32_T)-(int32_T)
    (uint32_T)-u : (uint32_T)u;

  /* End of DataTypeConversion: '<S150>/Data Type Conversion' */

  /* Pack: <S150>/Byte Pack */
  (void) memcpy(&drum_bench_working_B.data_mh[0],
                &drum_bench_working_B.DataTypeConversion_iv[0],
                8);
  (void) memcpy(&drum_bench_working_B.data_mh[8],
                drum_bench_working_ConstP.pooled40,
                12);

  /* BusCreator: '<S150>/Bus Creator' incorporates:
   *  Constant: '<S150>/id'
   *  Constant: '<S150>/type'
   */
  drum_bench_working_B.BusCreator_co.type = 3;
  drum_bench_working_B.BusCreator_co.id = 1.1F;

  /* BusCreator: '<S154>/Bus Creator' incorporates:
   *  Constant: '<S154>/Constant'
   *  Constant: '<S154>/Constant1'
   *  Constant: '<S154>/Constant2'
   */
  drum_bench_working_B.BusCreator_gs.type = 10;
  drum_bench_working_B.BusCreator_gs.id = 20.0F;
  for (i = 0; i < 20; i++) {
    /* BusCreator: '<S161>/Bus Creator' */
    drum_bench_working_B.BusCreator_j.data[i] = drum_bench_working_B.data_e5[i];

    /* BusCreator: '<S153>/Bus Creator' */
    drum_bench_working_B.BusCreator_dy.data[i] =
      drum_bench_working_ConstB.data_ec[i];

    /* BusCreator: '<S146>/Bus Creator' */
    drum_bench_working_B.BusCreator_d1.data[i] =
      drum_bench_working_ConstB.data_og[i];

    /* BusCreator: '<S156>/Bus Creator' */
    drum_bench_working_B.BusCreator_fc.data[i] =
      drum_bench_working_ConstB.data_bu[i];

    /* BusCreator: '<S155>/Bus Creator' */
    drum_bench_working_B.BusCreator_bw.data[i] =
      drum_bench_working_ConstB.data_ci[i];

    /* BusCreator: '<S147>/Bus Creator' */
    drum_bench_working_B.BusCreator_ow.data[i] =
      drum_bench_working_ConstB.data_e0[i];

    /* BusCreator: '<S148>/Bus Creator' */
    drum_bench_working_B.BusCreator_dj.data[i] = drum_bench_working_B.data_o[i];

    /* BusCreator: '<S149>/Bus Creator' */
    drum_bench_working_B.BusCreator_om.data[i] = drum_bench_working_B.data_bl[i];

    /* BusCreator: '<S150>/Bus Creator' */
    drum_bench_working_B.BusCreator_co.data[i] = drum_bench_working_B.data_mh[i];
    drum_bench_working_B.BusCreator_gs.data[i] =
      drum_bench_working_ConstP.pooled42[i];
  }

  /* End of BusCreator: '<S154>/Bus Creator' */

  /* Concatenate: '<S128>/Vector Concatenate' */
  drum_bench_working_B.VectorConcatenate_a[0] =
    drum_bench_working_B.BusCreator_j;
  drum_bench_working_B.VectorConcatenate_a[1] =
    drum_bench_working_B.BusCreator_dy;
  drum_bench_working_B.VectorConcatenate_a[2] =
    drum_bench_working_B.BusCreator_d1;
  drum_bench_working_B.VectorConcatenate_a[3] =
    drum_bench_working_B.BusCreator_fc;
  drum_bench_working_B.VectorConcatenate_a[4] =
    drum_bench_working_B.BusCreator_bw;
  drum_bench_working_B.VectorConcatenate_a[5] =
    drum_bench_working_B.BusCreator_ow;
  drum_bench_working_B.VectorConcatenate_a[6] =
    drum_bench_working_B.BusCreator_br;
  drum_bench_working_B.VectorConcatenate_a[7] =
    drum_bench_working_B.BusCreator_ih;
  drum_bench_working_B.VectorConcatenate_a[8] =
    drum_bench_working_B.BusCreator_g1;
  drum_bench_working_B.VectorConcatenate_a[9] =
    drum_bench_working_B.BusCreator_lg;
  drum_bench_working_B.VectorConcatenate_a[10] =
    drum_bench_working_B.BusCreator_i2;
  drum_bench_working_B.VectorConcatenate_a[11] =
    drum_bench_working_B.BusCreator_dj;
  drum_bench_working_B.VectorConcatenate_a[12] =
    drum_bench_working_B.BusCreator_om;
  drum_bench_working_B.VectorConcatenate_a[13] =
    drum_bench_working_B.BusCreator_co;
  drum_bench_working_B.VectorConcatenate_a[14] =
    drum_bench_working_B.BusCreator_gs;

  /* MATLAB Function: '<S128>/select rpc' */
  drum_bench_working_selectrpc(drum_bench_working_B.rpc_l,
    drum_bench_working_B.VectorConcatenate_a,
    &drum_bench_working_B.sf_selectrpc_j);

  /* Pack: <S128>/Byte Pack */
  (void) memcpy(&drum_bench_working_B.rpc_n[0],
                &drum_bench_working_B.sf_selectrpc_j.type,
                4);
  (void) memcpy(&drum_bench_working_B.rpc_n[4],
                &drum_bench_working_B.sf_selectrpc_j.id,
                4);
  (void) memcpy(&drum_bench_working_B.rpc_n[8],
                &drum_bench_working_B.sf_selectrpc_j.data[0],
                20);

  /* RelationalOperator: '<S157>/Compare' incorporates:
   *  Constant: '<S157>/Constant'
   */
  drum_bench_working_B.Compare_b = (drum_bench_working_B.type_b == 2);

  /* Outputs for Enabled SubSystem: '<S146>/enabled subsystem' */
  drum_bench_wor_enabledsubsystem(drum_bench_working_B.Compare_b,
    drum_bench_working_B.data_a, &drum_bench_working_B.enabledsubsystem_l);

  /* End of Outputs for SubSystem: '<S146>/enabled subsystem' */

  /* Unpack: <S146>/Byte Unpack */
  (void) memcpy(&drum_bench_working_B.ByteUnpack_eb[0],
                &drum_bench_working_B.enabledsubsystem_l.In1[0],
                20);

  /* DataTypeConversion: '<S146>/Data Type Conversion' */
  for (i = 0; i < 10; i++) {
    drum_bench_working_B.DataTypeConversion_i[i] =
      drum_bench_working_B.ByteUnpack_eb[i];
  }

  /* End of DataTypeConversion: '<S146>/Data Type Conversion' */

  /* DataTypeConversion: '<S146>/analog_limit' */
  drum_bench_working_B.analog_limit_k = drum_bench_working_B.ByteUnpack_eb[3];

  /* DataTypeConversion: '<S146>/ethercat' */
  drum_bench_working_B.ethercat_l = drum_bench_working_B.ByteUnpack_eb[8];

  /* DataTypeConversion: '<S146>/external_panic' */
  drum_bench_working_B.external_panic_l = drum_bench_working_B.ByteUnpack_eb[9];

  /* DataTypeConversion: '<S146>/motor encoder' */
  drum_bench_working_B.motor_encoder_h = drum_bench_working_B.ByteUnpack_eb[4];

  /* DataTypeConversion: '<S146>/over_current' */
  drum_bench_working_B.over_current_p = drum_bench_working_B.ByteUnpack_eb[0];

  /* DataTypeConversion: '<S146>/over_voltage' */
  drum_bench_working_B.over_voltage_o = drum_bench_working_B.ByteUnpack_eb[2];

  /* DataTypeConversion: '<S146>/quadrature_1_high' */
  drum_bench_working_B.quadrature_1_high_p = drum_bench_working_B.ByteUnpack_eb
    [6];

  /* DataTypeConversion: '<S146>/quadrature_1_low' */
  drum_bench_working_B.quadrature_1_low_c = drum_bench_working_B.ByteUnpack_eb[5];

  /* DataTypeConversion: '<S146>/quadrature_2_limit' */
  drum_bench_working_B.quadrature_2_limit_a =
    drum_bench_working_B.ByteUnpack_eb[7];

  /* DataTypeConversion: '<S146>/under_voltage' */
  drum_bench_working_B.under_voltage_l = drum_bench_working_B.ByteUnpack_eb[1];

  /* RelationalOperator: '<S159>/Compare' incorporates:
   *  Constant: '<S159>/Constant'
   */
  drum_bench_working_B.Compare_aw = (drum_bench_working_B.type_b == 3);

  /* Outputs for Enabled SubSystem: '<S147>/Enabled Subsystem' */
  drum_bench_wor_EnabledSubsystem(drum_bench_working_B.Compare_aw,
    drum_bench_working_B.data_a, &drum_bench_working_B.EnabledSubsystem_k);

  /* End of Outputs for SubSystem: '<S147>/Enabled Subsystem' */

  /* Unpack: <S147>/Byte Unpack */
  (void) memcpy(&drum_bench_working_B.ByteUnpack_h[0],
                &drum_bench_working_B.EnabledSubsystem_k.In1[0],
                20);

  /* DataTypeConversion: '<S147>/load a' */
  drum_bench_working_B.loada_c = drum_bench_working_B.ByteUnpack_h[0];

  /* DataTypeConversion: '<S147>/load a1' */
  drum_bench_working_B.loada1_h = drum_bench_working_B.ByteUnpack_h[2];

  /* DataTypeConversion: '<S147>/load a2' */
  drum_bench_working_B.loada2_p = drum_bench_working_B.ByteUnpack_h[3];

  /* DataTypeConversion: '<S147>/load a3' */
  drum_bench_working_B.loada3_o = drum_bench_working_B.ByteUnpack_h[4];

  /* DataTypeConversion: '<S147>/load b' */
  drum_bench_working_B.loadb_a = drum_bench_working_B.ByteUnpack_h[1];

  /* DataTypeConversion: '<S151>/current_index' */
  drum_bench_working_B.current_index_c =
    drum_bench_working_B.sf_MATLABFunction1_a.index;

  /* DataTypeConversion: '<Root>/mode1' */
  drum_bench_working_B.mode1 = drum_bench_working_B.mode_l;

  /* DataTypeConversion: '<Root>/motor_iq1' */
  drum_bench_working_B.motor_iq1 = drum_bench_working_B.motor_iq_n;

  /* DataTypeConversion: '<Root>/motor_torque1' */
  drum_bench_working_B.motor_torque1 = drum_bench_working_B.motor_torque_px;

  /* S-Function Block: <Root>/Domain State
   */
  {
    ec_domain_state_t state;
    ecrt_domain_state( (ec_domain_t*)
                      drum_bench_working_DW.DomainState_PWORK.DomainPtr, &state);
    drum_bench_working_B.DomainState_o1 = state.working_counter;
    drum_bench_working_B.DomainState_o2 = state.wc_state;
  }

  /* S-Function Block: <Root>/Master State
   */
  {
    ec_master_state_t state;
    ecrt_master_state( (ec_master_t*)
                      drum_bench_working_DW.MasterState_PWORK.MasterPtr, &state);
    drum_bench_working_B.MasterState_o1 = state.slaves_responding;
    drum_bench_working_B.MasterState_o2 = state.al_states;
    drum_bench_working_B.MasterState_o3 = state.link_up ? 1 : 0;
  }

  /* Outputs for Triggered SubSystem: '<S11>/Triggered Subsystem' incorporates:
   *  TriggerPort: '<S192>/Trigger'
   */
  /* Constant: '<S11>/Constant' */
  zcEvent = rt_ZCFcn(RISING_ZERO_CROSSING,
                     &drum_bench_working_PrevZCX.TriggeredSubsystem_Trig_ZCE,
                     (drum_bench_working_P.config.input.input_extra.trigger_yaml_write));
  if (zcEvent != NO_ZCEVENT) {
    /* S-Function (write_yaml_params): '<S192>/S-Function' */
    write_yaml_params_Outputs_wrapper( );
  }

  /* End of Outputs for SubSystem: '<S11>/Triggered Subsystem' */
}

/* Model update function */
static void drum_bench_working_update(void)
{
  /* S-Function Block: <S64>/marlin_ec
   */
  /* Input Port 1 written directly by slave */
  /* Input Port 2 written directly by slave */
  /* Input Port 3 written directly by slave */
  /* Input Port 4 written directly by slave */
  /* Input Port 5 written directly by slave */

  /* Update for Delay: '<S62>/Delay' */
  drum_bench_working_DW.Delay_DSTATE[0] = drum_bench_working_B.Sum[0];
  drum_bench_working_DW.Delay_DSTATE[1] = drum_bench_working_B.Sum[1];

  /* Update for Atomic SubSystem: '<S1>/Filtered Derivative' */
  drum__FilteredDerivative_Update(&drum_bench_working_B.FilteredDerivative,
    &drum_bench_working_DW.FilteredDerivative);

  /* End of Update for SubSystem: '<S1>/Filtered Derivative' */

  /* Update for Atomic SubSystem: '<S9>/Filtered Derivative' */
  drum__FilteredDerivative_Update(&drum_bench_working_B.FilteredDerivative_c,
    &drum_bench_working_DW.FilteredDerivative_c);

  /* End of Update for SubSystem: '<S9>/Filtered Derivative' */

  /* Update for UnitDelay: '<S65>/Output' */
  drum_bench_working_DW.Output_DSTATE = drum_bench_working_B.FixPtSwitch;

  /* S-Function Block: <S129>/marlin_ec
   */
  /* Input Port 1 written directly by slave */
  /* Input Port 2 written directly by slave */
  /* Input Port 3 written directly by slave */
  /* Input Port 4 written directly by slave */
  /* Input Port 5 written directly by slave */

  /* Update for Delay: '<S127>/Delay' */
  drum_bench_working_DW.Delay_DSTATE_n[0] = drum_bench_working_B.Sum_do[0];
  drum_bench_working_DW.Delay_DSTATE_n[1] = drum_bench_working_B.Sum_do[1];

  /* Update for Atomic SubSystem: '<S2>/Filtered Derivative' */
  drum__FilteredDerivative_Update(&drum_bench_working_B.FilteredDerivative_n,
    &drum_bench_working_DW.FilteredDerivative_n);

  /* End of Update for SubSystem: '<S2>/Filtered Derivative' */

  /* Update for Atomic SubSystem: '<S10>/Filtered Derivative' */
  drum__FilteredDerivative_Update(&drum_bench_working_B.FilteredDerivative_p,
    &drum_bench_working_DW.FilteredDerivative_p);

  /* End of Update for SubSystem: '<S10>/Filtered Derivative' */

  /* Update for UnitDelay: '<S130>/Output' */
  drum_bench_working_DW.Output_DSTATE_h = drum_bench_working_B.FixPtSwitch_b;

  /* user code (Update function Trailer) */

  /* EtherCAT Queue for Sample Time [0.0003] */
#ifndef ASYNC_ECAT

  if (1 && rtmIsMajorTimeStep(drum_bench_working_M)) {
    ecs_send(0);
  }

#endif

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++drum_bench_working_M->Timing.clockTick0)) {
    ++drum_bench_working_M->Timing.clockTickH0;
  }

  drum_bench_working_M->Timing.t[0] = drum_bench_working_M->Timing.clockTick0 *
    drum_bench_working_M->Timing.stepSize0 +
    drum_bench_working_M->Timing.clockTickH0 *
    drum_bench_working_M->Timing.stepSize0 * 4294967296.0;

  {
    /* Update absolute timer for sample time: [0.0003s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++drum_bench_working_M->Timing.clockTick1)) {
      ++drum_bench_working_M->Timing.clockTickH1;
    }

    drum_bench_working_M->Timing.t[1] = drum_bench_working_M->Timing.clockTick1 *
      drum_bench_working_M->Timing.stepSize1 +
      drum_bench_working_M->Timing.clockTickH1 *
      drum_bench_working_M->Timing.stepSize1 * 4294967296.0;
  }
}

/* Model initialize function */
void drum_bench_working_initialize(void)
{
  {
    /* user code (Start function Header) */
    const char __attribute__((unused)) *errstr_rc;
    const struct ec_slave *slave_head;
    const struct ec_slave **slave_list = &slave_head;

    /* Zero terminated list of sample times for EtherCAT */
    unsigned int ec_st[1] = {
      300000U,
    };

    /* Start for S-Function (ec_slave3_runtime): '<S64>/marlin_ec' */

    /* End Modification */

    /* S-Function Block: <S64>/marlin_ec
     * Registering EtherCAT block M3EC with Driver.
     */

    /* Modification to EtherLab allowing alias and position to get set
     * at runtime. (see Rob or Advait)
     */
    ec_slave_1.alias = *&(drum_bench_working_P.config.marlins.M0.slave_alias);
    ec_slave_1.position =
      *&(drum_bench_working_P.config.marlins.M0.slave_position);

    /* End Modification */
    *slave_list = &ec_slave_1;
    slave_list = &ec_slave_1.next;

    /* Start for Enabled SubSystem: '<S81>/enabled subsystem' */
    drum_ben_enabledsubsystem_Start(&drum_bench_working_B.enabledsubsystem);

    /* End of Start for SubSystem: '<S81>/enabled subsystem' */

    /* Start for Enabled SubSystem: '<S82>/Enabled Subsystem' */
    drum_ben_EnabledSubsystem_Start(&drum_bench_working_B.EnabledSubsystem);

    /* End of Start for SubSystem: '<S82>/Enabled Subsystem' */

    /* Start for S-Function (ec_slave3_runtime): '<S129>/marlin_ec' */

    /* End Modification */

    /* S-Function Block: <S129>/marlin_ec
     * Registering EtherCAT block M3EC with Driver.
     */

    /* Modification to EtherLab allowing alias and position to get set
     * at runtime. (see Rob or Advait)
     */
    ec_slave_2.alias = *&(drum_bench_working_P.config.marlins.M1.slave_alias);
    ec_slave_2.position =
      *&(drum_bench_working_P.config.marlins.M1.slave_position);

    /* End Modification */
    *slave_list = &ec_slave_2;
    slave_list = &ec_slave_2.next;

    /* Start for Enabled SubSystem: '<S146>/enabled subsystem' */
    drum_ben_enabledsubsystem_Start(&drum_bench_working_B.enabledsubsystem_l);

    /* End of Start for SubSystem: '<S146>/enabled subsystem' */

    /* Start for Enabled SubSystem: '<S147>/Enabled Subsystem' */
    drum_ben_EnabledSubsystem_Start(&drum_bench_working_B.EnabledSubsystem_k);

    /* End of Start for SubSystem: '<S147>/Enabled Subsystem' */

    /* S-Function Block: <Root>/Domain State
     */
    drum_bench_working_DW.DomainState_PWORK.DomainPtr = ecs_get_domain_ptr(0U,
      0U, 1, 1, 0, &errstr_rc);
    if (!drum_bench_working_DW.DomainState_PWORK.DomainPtr) {
      snprintf(etl_errbuf, sizeof(etl_errbuf),
               "Getting input domain pointer in "
               "drum_bench_working/Domain State failed: %s",
               errstr_rc);
      rtmSetErrorStatus(drum_bench_working_M, etl_errbuf);
      return;
    }

    /* S-Function Block: <Root>/Master State
     */
    errstr_rc = ecs_setup_master(0, 0U,
      &drum_bench_working_DW.MasterState_PWORK.MasterPtr);
    if (errstr_rc) {
      snprintf(etl_errbuf, sizeof(etl_errbuf),
               "Setting up master in drum_bench_working/Master State "
               "failed: %s", errstr_rc);
      rtmSetErrorStatus(drum_bench_working_M, etl_errbuf);
      return;
    }

    /* user code (Start function Trailer) */

    /* Starting EtherCAT subsystem */
    if ((errstr_rc = ecs_start(slave_head, ec_st, 1, 1))) {
      snprintf(etl_errbuf, sizeof(etl_errbuf),
               "Starting EtherCAT subsystem failed: %s", errstr_rc);
      rtmSetErrorStatus(drum_bench_working_M, etl_errbuf);
      return;
    }
  }

  drum_bench_working_PrevZCX.TriggeredSubsystem_Trig_ZCE = UNINITIALIZED_ZCSIG;

  /* InitializeConditions for Delay: '<S62>/Delay' */
  drum_bench_working_DW.Delay_DSTATE[0] = 0.0;
  drum_bench_working_DW.Delay_DSTATE[1] = 0.0;

  /* InitializeConditions for RateLimiter: '<S17>/Rate Limiter' */
  drum_bench_working_DW.PrevY = 0.0;

  /* InitializeConditions for RateLimiter: '<S18>/Rate Limiter' */
  drum_bench_working_DW.PrevY_a = 0.0;

  /* InitializeConditions for RateLimiter: '<S14>/Rate Limiter' */
  drum_bench_working_DW.PrevY_h = 0.0;

  /* InitializeConditions for RateLimiter: '<S15>/Rate Limiter' */
  drum_bench_working_DW.PrevY_d = 0.0;

  /* InitializeConditions for RateLimiter: '<S16>/Rate Limiter' */
  drum_bench_working_DW.PrevY_dx = 0.0;

  /* InitializeConditions for Atomic SubSystem: '<S1>/Filtered Derivative' */
  drum_be_FilteredDerivative_Init(&drum_bench_working_DW.FilteredDerivative);

  /* End of InitializeConditions for SubSystem: '<S1>/Filtered Derivative' */

  /* InitializeConditions for Atomic SubSystem: '<S9>/Filtered Derivative' */
  drum_be_FilteredDerivative_Init(&drum_bench_working_DW.FilteredDerivative_c);

  /* End of InitializeConditions for SubSystem: '<S9>/Filtered Derivative' */

  /* InitializeConditions for UnitDelay: '<S65>/Output' */
  drum_bench_working_DW.Output_DSTATE = 0U;

  /* InitializeConditions for MATLAB Function: '<S96>/MATLAB Function1' */
  drum_bench_MATLABFunction1_Init(&drum_bench_working_DW.sf_MATLABFunction1);

  /* InitializeConditions for Delay: '<S127>/Delay' */
  drum_bench_working_DW.Delay_DSTATE_n[0] = 0.0;
  drum_bench_working_DW.Delay_DSTATE_n[1] = 0.0;

  /* InitializeConditions for RateLimiter: '<S27>/Rate Limiter' */
  drum_bench_working_DW.PrevY_m = 0.0;

  /* InitializeConditions for RateLimiter: '<S28>/Rate Limiter' */
  drum_bench_working_DW.PrevY_b = 0.0;

  /* InitializeConditions for RateLimiter: '<S24>/Rate Limiter' */
  drum_bench_working_DW.PrevY_f = 0.0;

  /* InitializeConditions for RateLimiter: '<S25>/Rate Limiter' */
  drum_bench_working_DW.PrevY_d5 = 0.0;

  /* InitializeConditions for RateLimiter: '<S26>/Rate Limiter' */
  drum_bench_working_DW.PrevY_k = 0.0;

  /* InitializeConditions for Atomic SubSystem: '<S2>/Filtered Derivative' */
  drum_be_FilteredDerivative_Init(&drum_bench_working_DW.FilteredDerivative_n);

  /* End of InitializeConditions for SubSystem: '<S2>/Filtered Derivative' */

  /* InitializeConditions for Atomic SubSystem: '<S10>/Filtered Derivative' */
  drum_be_FilteredDerivative_Init(&drum_bench_working_DW.FilteredDerivative_p);

  /* End of InitializeConditions for SubSystem: '<S10>/Filtered Derivative' */

  /* InitializeConditions for UnitDelay: '<S130>/Output' */
  drum_bench_working_DW.Output_DSTATE_h = 0U;

  /* InitializeConditions for MATLAB Function: '<S161>/MATLAB Function1' */
  drum_bench_MATLABFunction1_Init(&drum_bench_working_DW.sf_MATLABFunction1_a);
}

/* Model terminate function */
void drum_bench_working_terminate(void)
{
  /* user code (Terminate function Body) */

  /* Shutting down EtherCAT subsystem */
  ecs_end(1);
}

/*========================================================================*
 * Start of Classic call interface                                        *
 *========================================================================*/
void MdlOutputs(int_T tid)
{
  drum_bench_working_output();
  UNUSED_PARAMETER(tid);
}

void MdlUpdate(int_T tid)
{
  drum_bench_working_update();
  UNUSED_PARAMETER(tid);
}

void MdlInitializeSizes(void)
{
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
}

void MdlStart(void)
{
  drum_bench_working_initialize();
}

void MdlTerminate(void)
{
  drum_bench_working_terminate();
}

/* Registration function */
RT_MODEL_drum_bench_working_T *drum_bench_working(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)drum_bench_working_M, 0,
                sizeof(RT_MODEL_drum_bench_working_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&drum_bench_working_M->solverInfo,
                          &drum_bench_working_M->Timing.simTimeStep);
    rtsiSetTPtr(&drum_bench_working_M->solverInfo, &rtmGetTPtr
                (drum_bench_working_M));
    rtsiSetStepSizePtr(&drum_bench_working_M->solverInfo,
                       &drum_bench_working_M->Timing.stepSize0);
    rtsiSetErrorStatusPtr(&drum_bench_working_M->solverInfo, (&rtmGetErrorStatus
                           (drum_bench_working_M)));
    rtsiSetRTModelPtr(&drum_bench_working_M->solverInfo, drum_bench_working_M);
  }

  rtsiSetSimTimeStep(&drum_bench_working_M->solverInfo, MAJOR_TIME_STEP);
  rtsiSetSolverName(&drum_bench_working_M->solverInfo,"FixedStepDiscrete");

  /* Initialize timing info */
  {
    int_T *mdlTsMap = drum_bench_working_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    drum_bench_working_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    drum_bench_working_M->Timing.sampleTimes =
      (&drum_bench_working_M->Timing.sampleTimesArray[0]);
    drum_bench_working_M->Timing.offsetTimes =
      (&drum_bench_working_M->Timing.offsetTimesArray[0]);

    /* task periods */
    drum_bench_working_M->Timing.sampleTimes[0] = (0.0);
    drum_bench_working_M->Timing.sampleTimes[1] = (0.0003);

    /* task offsets */
    drum_bench_working_M->Timing.offsetTimes[0] = (0.0);
    drum_bench_working_M->Timing.offsetTimes[1] = (0.0);
  }

  rtmSetTPtr(drum_bench_working_M, &drum_bench_working_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = drum_bench_working_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    drum_bench_working_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(drum_bench_working_M, -1);
  drum_bench_working_M->Timing.stepSize0 = 0.0003;
  drum_bench_working_M->Timing.stepSize1 = 0.0003;
  drum_bench_working_M->solverInfoPtr = (&drum_bench_working_M->solverInfo);
  drum_bench_working_M->Timing.stepSize = (0.0003);
  rtsiSetFixedStepSize(&drum_bench_working_M->solverInfo, 0.0003);
  rtsiSetSolverMode(&drum_bench_working_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  drum_bench_working_M->ModelData.blockIO = ((void *) &drum_bench_working_B);
  (void) memset(((void *) &drum_bench_working_B), 0,
                sizeof(B_drum_bench_working_T));

  /* parameters */
  drum_bench_working_M->ModelData.defaultParam = ((real_T *)
    &drum_bench_working_P);

  /* states (dwork) */
  drum_bench_working_M->ModelData.dwork = ((void *) &drum_bench_working_DW);
  (void) memset((void *)&drum_bench_working_DW, 0,
                sizeof(DW_drum_bench_working_T));

  /* Initialize DataMapInfo substructure containing ModelMap for C API */
  drum_bench_working_InitializeDataMapInfo(drum_bench_working_M);

  /* Initialize Sizes */
  drum_bench_working_M->Sizes.numContStates = (0);/* Number of continuous states */
  drum_bench_working_M->Sizes.numY = (0);/* Number of model outputs */
  drum_bench_working_M->Sizes.numU = (0);/* Number of model inputs */
  drum_bench_working_M->Sizes.sysDirFeedThru = (0);/* The model is not direct feedthrough */
  drum_bench_working_M->Sizes.numSampTimes = (2);/* Number of sample times */
  drum_bench_working_M->Sizes.numBlocks = (1068);/* Number of blocks */
  drum_bench_working_M->Sizes.numBlockIO = (667);/* Number of block outputs */
  drum_bench_working_M->Sizes.numBlockPrms = (1);/* Sum of parameter "widths" */
  return drum_bench_working_M;
}

/*========================================================================*
 * End of Classic call interface                                          *
 *========================================================================*/
