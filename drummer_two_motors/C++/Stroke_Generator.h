#ifndef ST_GEN_H
#define ST_GEN_H


#include <math.h>
#include "Periodic.h"
#include "Atoms.h"

class Stroke_Generator
{
private:
    double m_SampleRate;
    
    double m_SteadyStatePos;
    double m_StrikeStatePos;
    double m_ParkPos;
    
    double m_StrikeDelayInSec;
    int m_StrikeDelaySamples;
    int m_SamplesSinceStrike;
    
    int m_StrokeType;
    float m_StrokeAmplitude;
    
    bool m_ByPass;
    
    Periodic *m_PeriodicGen;
    Atoms *m_AtomGen;
    
public:
    
    Stroke_Generator(double sr, double parkPos, double steadyStatePos, double strikePos) 
    {
        m_SampleRate = sr;
        m_ParkPos = parkPos;
        m_SteadyStatePos = steadyStatePos;
        m_StrikeStatePos = strikePos;
        m_ByPass = false;
        m_PeriodicGen = new Periodic(sr);
        m_AtomGen = new Atoms();
        m_AtomGen->setParams(m_SampleRate);
        setDefaults();
    }
    
    ~Stroke_Generator()
    {
        delete m_PeriodicGen;
        delete m_AtomGen;
    }

    void setStrokeType(int stroke_Type) // The sequence should set this externally from a composition
    {
        // A sequencer can output -1, 0, 1, 2.... but -1 is about holding onto the last non-negative strikeInstruction
      
            if(stroke_Type != -1)
            {
                if(stroke_Type == 1)
                {
                    m_SamplesSinceStrike = 0; // reset the counter everytime a strike is instantiated. 
                }
                if(stroke_Type == 2)
                {
                    m_PeriodicGen->setFrequency(10);
                }
                if(stroke_Type == 4)
                {
                    m_AtomGen->setNumberOfStrokes(2); // if the sequencer value is 4 then play diddle, then have the number of hits for the atom will be 2. 
                    m_AtomGen->resetForNextHit();
                }
                m_StrokeType = stroke_Type;
            }
        
    }
    
    int getStrokeType()
    {
       return m_StrokeType; 
    }
    
    void setStrokeAmplitude(float amplitude) //amplitude is between 0.3 and 1, it is a scaling factor. 
    {
        m_StrokeAmplitude = amplitude;
    }
    
    float getStrokeAmplitude()
    {
        return m_StrokeAmplitude;
    }
    
    void toggleByPass()
    {
        m_ByPass = !m_ByPass;
    }
    void setByPass(bool bval) // in order to set bypass programmtically
    {
        m_ByPass = bval;
    }
    void singleStrike()
    {
       m_SamplesSinceStrike = 0;
       m_StrokeType = 1;
    }
    double updatePos()
    {
        return updateOut();
    }
    void parkStick()
    {
        m_StrokeType = 3;
    }
private:

    void setDefaults()
    {
        //std::cout <<"Defaults done " <<std::endl;
        m_StrikeDelayInSec = 0.04; 
        m_StrikeDelaySamples =  (int)(m_StrikeDelayInSec/m_SampleRate);
        m_StrokeType = 0;  
        m_StrokeAmplitude = 1.0;
    }

    double updateOut()
    {
        if(m_ByPass == false)
        {
            switch(m_StrokeType)
            {
                case 0:
                    return m_SteadyStatePos;
                    break;
                case 1:
                    m_SamplesSinceStrike++;
                    if(m_SamplesSinceStrike <= m_StrikeDelaySamples)
                    {
                        return m_StrokeAmplitude*(m_StrikeStatePos-m_SteadyStatePos) + m_SteadyStatePos;
                    }else
                    {
                        m_SamplesSinceStrike = m_StrikeDelaySamples + 1;
                        setStrokeType(0); // after the strike update the current stroketype to steady state. 
                        return m_SteadyStatePos;
                    }
                    break;
                case 2:
                    return m_PeriodicGen->output();
                    break;
                case 3 :
                    return m_ParkPos;
                    break;
                case 4:
                    if(m_AtomGen->getOutput())
                    {
                        return m_StrokeAmplitude*(m_StrikeStatePos-m_SteadyStatePos) + m_SteadyStatePos;
                    }
                    else
                    {
                        return m_SteadyStatePos;
                    }
                    break;
                default:
                    return m_SteadyStatePos; 
                    break;
            }
        }else
        {
            return m_SteadyStatePos;
        }
    }
};

#endif