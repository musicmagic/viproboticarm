#ifndef ATOMS_H
#define ATOMS_H

#include <iostream>
#include <math.h>

/*
	An atom refers to a small one shot rhythmic gesture such as a single hit , a diddle or even a triple stroke, that is evenly spaced. 
	This gesture can be referred to as a one single composite gesture and if used as an element in a sequencer the whole gesture will be treated as one single sequence element.
*/
class Atoms
{

private:

	int m_NumberOfStrokesInAtom;
	int m_InterHitIntervalInSamples;
	float m_SampleRate; // 0.001

	int m_InterHitSampleCounter;
	int m_CurrentHitNumber; // to keep track of how many hits have already been played. 
    int m_SamplesSinceStrike;
    float m_StrikeDelayInSec;

public:
	Atoms()
	{
		m_NumberOfStrokesInAtom = 1;
		m_InterHitSampleCounter = 0;
		m_CurrentHitNumber = 0;
        m_StrikeDelayInSec = 0.03;
        m_SamplesSinceStrike = 0;
	}

	~Atoms()
	{

	}

	void setParams(float sampleRate)
	{
		m_SampleRate = sampleRate;
		m_InterHitIntervalInSamples = (int)(0.1/m_SampleRate); // 0.2 seconds between any successive hits. this would the rate of quintuplets at 60bpm
        m_SamplesSinceStrike = (int)(m_StrikeDelayInSec/m_SampleRate);
    }

	void setInterHitDuration(float durInSec) // in order to make the diddle more open or close. This interval can be a subdivision of the inter sequence element duration and then we can precise subdivisions
	{
		m_InterHitIntervalInSamples = (int)(durInSec/m_SampleRate);
	}

	void setNumberOfStrokes(int numStrokes)
	{
		m_NumberOfStrokesInAtom = numStrokes;
	}

	void resetForNextHit()
	{
		m_InterHitSampleCounter = -1;
		m_CurrentHitNumber = 0;
	}
	int getOutput()
	{	
		m_InterHitSampleCounter++;
		if(m_InterHitSampleCounter >= m_InterHitIntervalInSamples) // the inter hit interval determines how fast the diddles or triple strokes are going to be. 
		{
			m_InterHitSampleCounter = 0;
            m_CurrentHitNumber++;
		}
       
		if((m_InterHitSampleCounter == 0 || m_InterHitSampleCounter <= m_SamplesSinceStrike) && m_CurrentHitNumber < m_NumberOfStrokesInAtom)
		{
			return 1; //  1 means hit
		}
		else
		{
			return 0; // 0 is steady state for the stroke generator.
		}
	}
};


#endif