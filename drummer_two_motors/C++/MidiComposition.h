/******Authored by Deepak Gopinath********************/


#ifndef MIDICOMPOSITION_H
#define MIDICOMPOSITION_H

#include <iostream>
#include "ParseOnsetText.h"

using namespace std;

class MidiComposition
{
public:
    MidiComposition(string path, float tempo = 60.0)
    {
       m_MidiParser = new ParseOnsetText(path, tempo);
       m_SampleCounter = 0;
       m_MidiParser->getOnsetSequenceInSamples(m_OnsetSequence);
       m_SequenceIndex = 0;
       m_IsSecondaryStrikeOn = false;
    }
    
    ~MidiComposition()
    {
        delete m_MidiParser;
    }
    
    int getPrimaryStrokeType()
    {
        return 0;
    }
    
    void updateSecondarySampleCounter()
    {
       m_IsSecondaryStrikeOn = false;
       m_SampleCounter++;
       if(m_SampleCounter >= (m_OnsetSequence[m_SequenceIndex+1] - m_OnsetSequence[m_SequenceIndex])) // inter onset time interval;
       {
           m_IsSecondaryStrikeOn = true;
           m_SampleCounter = 0;
           m_SequenceIndex++;
           if(m_SequenceIndex >= m_OnsetSequence.size())
           {
                m_SequenceIndex = 0;
           }
       }
    }
    
    void resetIndexAndCounter()
    {
        m_SequenceIndex = 0;
        m_SampleCounter = 0;
    }
    bool getIsSecondaryStrikeOn()
    {
       return m_IsSecondaryStrikeOn;
    }
private:
     ParseOnsetText *m_MidiParser;
     int m_SampleCounter;
     
     std::vector<int> m_OnsetSequence;
     int m_SequenceIndex; 
     bool m_IsSecondaryStrikeOn;
     
     
     
};

#endif