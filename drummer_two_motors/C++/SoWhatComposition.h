/******Authored by Deepak Gopinath********************/


#ifndef SOWHATCOMPOSITION_H
#define SOWHATCOMPOSITON_H

#include <iostream>
#include <cstdlib>
#include <ctime>

#include "Sequencer.h"

#define SEC_TEMPO 180

class SoWhatComposition
{
private:
    
    Sequencer *m_SecondarySeq;
    float m_SecondaryAmp;
    float m_SampleRate;
    float m_SecondaryTempo;
    float m_SecondaryTimerCounter;
    
    
public:
    SoWhatComposition() 
    {
       std::srand(unsigned (std::time(0)));
       m_SecondarySeq = new Sequencer();
       m_SampleRate = 0.001;
       m_SecondaryTempo = SEC_TEMPO;
       m_SecondarySeq->setParams(m_SecondaryTempo, m_SampleRate);
       m_SecondarySeq->generateCellBasedSequence(16, 1); // all notes. 
       m_SecondarySeq->setNoteFlow(Sequencer::k_EighthTriplet);
       m_SecondaryAmp = 0.0;
       m_SecondaryTimerCounter = 0;
    }
    ~SoWhatComposition()
    {
        delete m_SecondarySeq;
    }
    
    int getPrimaryStrokeType()
    {
        return 0; // always going to be steady state for So what
    }
    
    int getSecondaryStrokeType()
    {
        updateSecondaryCounter();
        if(m_SecondaryTimerCounter > (rand()%1000 + 1800))
        {
            if( (rand()%1000)/1000.0  <  0.5 )
            {
                if(m_SecondarySeq->getCurrentNoteFlow() == Sequencer::k_Sixteenth)
                {
                    m_SecondarySeq->setNoteFlow(Sequencer::k_EighthTriplet);
                }else
                {
                    m_SecondarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                }  
            }
            
            m_SecondaryTimerCounter = 0; // reset the counter
        }
        return m_SecondarySeq->getSequenceValue();
    }
    
    float getSecondaryStrokeAmp()
    {
        return m_SecondaryAmp;
    }

    void setExpressionPedalValue(float pedalVal) 
    {
       // std::cout<<"Amplitude is " << pedalVal;
        m_SecondaryAmp  = pedalVal; // 0 to 0.98. If the pedal is all the way down, it is as good as being bypassed. 
    }
    
    void updateSecondaryCounter()
    {
        m_SecondaryTimerCounter++;
        if(m_SecondaryTimerCounter > 100000)
        {
            m_SecondaryTimerCounter = 0; // to avoid overflow
        }
    }
};

#endif

