/******Authored by Deepak Gopinath********************/

#ifndef SOLOCOMPOSITION_H
#define SOLOCOMPOSITION_H

#include <iostream>
#include <math.h>
#include <cstdlib>
#include <ctime>

#include "Sequencer.h"
#include "TempoManager.h"
#include "MidiComposition.h"

#define MAX_TEMPO_DEV  43.0

#define EXPCURVE(X, Y) (exp(X*log(Y)) - (float)1.0)/(Y - (float)1.0) // X is the input and Y is the steepness of the curve. 
#define LOGCURVE(X, Y) (log(X * (Y-1)+1) / log(Y)) // Y should be greater than 1.0. Values of Y closer to 1.0 will give straight lines. 

#define EXPSTEEPNESS 50
#define LOGSTEEPNESS 40


class SoloComposition
{
private:
    Sequencer *m_PrimarySeq;
    Sequencer *m_SecondarySeq;
    
    TempoManager *m_TempoManager;
    MidiComposition *m_UnisonMidi;
    
    float m_SampleRate;
    
    float m_PrimaryTempo;
    float m_SecondaryTempo;
    
    int m_CurrentPrimaryPosition;
    int m_CurrentSecondaryPosition;
    
    int m_NumQuarterPrimary;
    int m_NumQuarterSecondary;
    
    int m_CurrentSection;
    
    bool m_PrimaryBypass;
    bool m_SecondaryBypass;
    
    float m_PrimaryAmp;
    float m_SecondaryAmp;
    
    int m_PrimaryTimerCounter;
    int m_SecondaryTimerCounter;
    
    float m_RawExpPedalValue;
    
    int m_PrimaryIncrDecrFlag;
    int m_SecondaryIncrDecrFlag;
    
    float PRI_SECTION_BASE_TEMPO;
    float SEC_SECTION_BASE_TEMPO;
    
public:
    SoloComposition()
    {
        std::srand(unsigned (std::time(0)));
        m_PrimarySeq = new Sequencer();
		m_SecondarySeq = new Sequencer();
        m_UnisonMidi = new MidiComposition("/home/meka/meka/rsc-drummer/drummer_two_motors/C++/FinaleUnison.txt", 120);
        m_TempoManager = new TempoManager();
        
        PRI_SECTION_BASE_TEMPO = 300;
        SEC_SECTION_BASE_TEMPO = 305;
        
        m_SampleRate = 0.001;
        m_PrimaryTempo = PRI_SECTION_BASE_TEMPO;
        m_SecondaryTempo = SEC_SECTION_BASE_TEMPO;
        
        m_NumQuarterPrimary = 13;
        m_NumQuarterSecondary = 13;
     
        m_PrimarySeq->setParams(m_PrimaryTempo, m_SampleRate);
        m_SecondarySeq->setParams(m_SecondaryTempo,m_SampleRate); 
         
        m_PrimarySeq->generateCellBasedSequence(m_NumQuarterPrimary, 1);
        m_SecondarySeq->generateCellBasedSequence(m_NumQuarterSecondary, 1);
        
        m_CurrentPrimaryPosition = 0; // Before the piece starts both sticks are in steady state position. 
        m_CurrentSecondaryPosition = 3; 
        
           
        m_CurrentSection = -1;
        
        m_PrimaryBypass = false;
        m_SecondaryBypass = false;
        
        m_PrimaryTimerCounter = 0;
        m_SecondaryTimerCounter = 0;
        
        
        m_PrimaryAmp = 0.5;
        m_SecondaryAmp = 0.5;
        m_RawExpPedalValue = 0.0; 
        
        m_PrimaryIncrDecrFlag = 1.0;
        m_SecondaryIncrDecrFlag = -1.0;
        
        
         
    }
    
    ~SoloComposition()
    {
        delete m_PrimarySeq;
        delete m_SecondarySeq;
        delete m_TempoManager;
        delete m_UnisonMidi;
    }
    
    void updateSection(int updateVal)
    {
        m_CurrentSection = m_CurrentSection + updateVal;
        std::cout << "Current section in solo piece " << m_CurrentSection << std::endl;
        switch(m_CurrentSection)
        {
            case 0:
                setBypasses(false, false);
                resetCounters();
                m_PrimarySeq->generateCellBasedSequence(m_NumQuarterPrimary, 1);
                m_SecondarySeq->generateCellBasedSequence(m_NumQuarterSecondary, 1);
                break;
            case 1:
                setBypasses(false, false);
                resetCounters();
                m_PrimarySeq->generateCellBasedSequence(m_NumQuarterPrimary, 1);
                m_SecondarySeq->generateCellBasedSequence(m_NumQuarterSecondary, 1);
                break;
            case 2:
                setBypasses(true, false);
                resetCounters();
                m_PrimaryAmp = 1.0; 
                m_SecondaryAmp = 1.0;
                m_PrimarySeq->generateCellBasedSequence(m_NumQuarterPrimary, 1);
                m_SecondarySeq->generateCellBasedSequence(m_NumQuarterSecondary, 1);
                m_PrimarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                m_SecondarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                PRI_SECTION_BASE_TEMPO = 300;
                SEC_SECTION_BASE_TEMPO = 305;
                m_PrimaryTempo = PRI_SECTION_BASE_TEMPO; // change tempo for the sequences. 
                m_SecondaryTempo = SEC_SECTION_BASE_TEMPO;
                m_PrimarySeq->setTempo(m_PrimaryTempo); // for this section change the tempo for both sticks, make it higher. 
                m_SecondarySeq->setTempo(m_SecondaryTempo);
                break;
            case 3:
                setBypasses(false, false); // silence section between the main sections
                resetCounters();
                break;
            case 4:
                setBypasses(true, true);
                resetCounters();
                // generate new sequence etc...
                m_PrimaryAmp = 0.7; 
                m_SecondaryAmp = 1.0;
                m_PrimaryIncrDecrFlag = 1.0;
                m_SecondaryIncrDecrFlag = -1.0;
                PRI_SECTION_BASE_TEMPO = 300;
                SEC_SECTION_BASE_TEMPO = 305;
                PRI_SECTION_BASE_TEMPO += 3.0;
                SEC_SECTION_BASE_TEMPO += 6.0;
                
                m_PrimarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                m_SecondarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                m_PrimarySeq->setTempo(m_PrimaryTempo); // for this section change the tempo for both sticks, make it higher. 
                m_SecondarySeq->setTempo(m_SecondaryTempo);
                m_SecondarySeq->generateCellBasedSequence(51, -1); // generate a broken rhythm for the secondary stick
                setBypasses(false, false);
                break;
            case 5:
                setBypasses(false, false);
                resetCounters();
                break;
            case 6:
                setBypasses(true,true);
                resetCounters();
                m_PrimaryAmp = 1.0; // start of at zero amplitude. 
                m_SecondaryAmp = 1.0;
                PRI_SECTION_BASE_TEMPO = 120; // first stick at 120 so that the transition to the next section is smoother.
                SEC_SECTION_BASE_TEMPO = 290; // second stick going nuts...
                m_PrimaryTempo = PRI_SECTION_BASE_TEMPO;
                m_SecondaryTempo = SEC_SECTION_BASE_TEMPO;
                m_PrimarySeq->setTempo(m_PrimaryTempo);
                m_SecondarySeq->setTempo(m_SecondaryTempo);
                m_PrimarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                m_SecondarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                m_PrimarySeq->generateCellBasedSequence(47, 1); // both are broken patterns
                m_SecondarySeq->generateCellBasedSequence(53, -1);
                setBypasses(false, false);
                break;
            case 7:
                setBypasses(false, true); // short section with 8 quarter notes at 120
                resetCounters();
                
                m_PrimaryAmp = 0.9;
                m_SecondaryAmp = 0.0;
                PRI_SECTION_BASE_TEMPO = 120;
                SEC_SECTION_BASE_TEMPO = 240;
                m_PrimaryTempo = PRI_SECTION_BASE_TEMPO;
                m_SecondaryTempo = SEC_SECTION_BASE_TEMPO;
                m_PrimarySeq->setTempo(m_PrimaryTempo);
                m_SecondarySeq->setTempo(m_SecondaryTempo);
                
                m_PrimarySeq->setNoteFlow(Sequencer::k_Quarter);
                m_SecondarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                
                m_PrimarySeq->generateCellBasedSequence(8, 1); // all 
                m_SecondarySeq->generateCellBasedSequence(67, -1); 
                break;
            case 8:
                setBypasses(false, true); // groove section...straight sixteenths on primary stick.,.Eric joins. nothing in second stick
                resetCounters();
                m_PrimaryAmp = 0.7;
                m_SecondaryAmp = 0.7; // but this will be bypassed;
                
                m_PrimarySeq->setNoteFlow(Sequencer::k_Sixteenth); // change from quarter to sixteenth
                break;
            case 9:
                setBypasses(false, false) ; // second stick comes in with fill in 32nds. 
                resetCounters();
                SEC_SECTION_BASE_TEMPO = 240;
                m_SecondaryTempo = SEC_SECTION_BASE_TEMPO;
                m_SecondarySeq->setTempo(m_SecondaryTempo);
                m_SecondarySeq->generateCellBasedSequence(67, -1);
                m_PrimarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                m_SecondarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                break;
            case 10:
                setBypasses(true, true); // fill in before the last section.....
                resetCounters();
                PRI_SECTION_BASE_TEMPO = 120;
                SEC_SECTION_BASE_TEMPO = 120;
                m_PrimaryTempo = PRI_SECTION_BASE_TEMPO;
                m_PrimaryAmp = 1.0;
                m_PrimarySeq->setTempo(m_PrimaryTempo);
                m_PrimarySeq->setNoteFlow(Sequencer::k_Quarter);
                m_SecondaryAmp = 1.0;
                m_PrimarySeq->generateCellBasedSequence(8, 1); 
                break;
            case 11:   // lead in 2 measures of quarter notes at 120 to the last section with unison patterns
                std::cout << "Section 11 entered" <<std::endl;
                setBypasses(false, true);
                resetCounters();
                m_PrimarySeq->resetSampleCounter();
                m_PrimarySeq->setNoteFlow(Sequencer::k_Quarter);
                m_SecondaryAmp = 1.0;
                m_PrimarySeq->generateCellBasedSequence(8, 1); 
//                 PRI_SECTION_BASE_TEMPO = 120;
//                 SEC_SECTION_BASE_TEMPO = 120;
//                 m_PrimaryTempo = PRI_SECTION_BASE_TEMPO;
//                 m_PrimaryAmp = 1.0;
//                 m_PrimarySeq->setTempo(m_PrimaryTempo);
//                 m_PrimarySeq->setNoteFlow(Sequencer::k_Quarter);
//                 m_SecondaryAmp = 1.0;
//                 m_PrimarySeq->generateCellBasedSequence(8, 1); // all 
                
                break;
            case 12:
                std::cout << "sample count " <<   m_PrimaryTimerCounter << std::endl;
                m_UnisonMidi->resetIndexAndCounter();
               // m_PrimarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                setBypasses(false, true);
                resetCounters();
                m_PrimaryAmp = 1.0;
                m_SecondaryAmp = 0.0;
                break;
            default:
                break;
        }
    }
    
    /**************************stick position stuff*********************************/
    int getPrimaryStrokeType()
    {
        
        switch(m_CurrentSection)
        {
            case -1:
                m_CurrentPrimaryPosition = 0;
                break;
            case 0:
                m_CurrentPrimaryPosition = 0; // section 0 where jason is playing by himself, freely exploring. ends with a huge thud and lifts the arm. moves to next section which will flip the secondary stick into steady state from park state
                break;
            case 1:
                m_CurrentPrimaryPosition = 0; // section 1 is the short pause section. when jason bring his down slowly. and then moves to next section
                break;
            case 2:
                primarySectionTwo();
                break;
            case 3:
                primarySectionThree();
                break;
            case 4:
                primarySectionFour();
                break;
            case 5:
                primarySectionFive();
                break;
            case 6:
                primarySectionSix();
                break;
            case 7:
                primarySectionSeven();
                break;
            case 8:
                primarySectionEight();
                break;
            case 9:
                primarySectionNine();
                break;
            case 10:
                primarySectionTen();
                break;
            case 11:
                primarySectionEleven();
                break;
            case 12:
                primarySectionTwelve();
                break;
            default:
                break;
        }
        
        return m_CurrentPrimaryPosition;
    }
    
   int getSecondaryStrokeType()
    {
        switch(m_CurrentSection)
        {
            case -1:
                m_CurrentSecondaryPosition = 3;
                break;
            case 0:
                m_CurrentSecondaryPosition = 3;
                break;
            case 1:
                m_CurrentSecondaryPosition = 0;
                break;
            case 2:
                secondarySectionTwo();
                break;
            case 3:
                secondarySectionThree();
                break;
            case 4:
                secondarySectionFour();
                break;
            case 5:
                secondarySectionFive();
                break;
            case 6:
                secondarySectionSix();
                break;
            case 7:
                secondarySectionSeven();
                break;
            case 8:
                secondarySectionEight();
                break;
            case 9:
                secondarySectionNine();
                break;
            case 10:
                secondarySectionTen();
                break;
            case 11:
                secondarySectionEleven();
                break;
            case 12:
                secondarySectionTwelve();
                break;
            default:
                break;                
        }
        
        return m_CurrentSecondaryPosition;
    }
    
   
   /******************************************************************************************************************/
   /***************************Bypass stuff*********************************/
    void togglePrimaryBypass()
    {
        m_PrimaryBypass = !m_PrimaryBypass;
    }
          
    bool getCurrentPrimaryBypass()
    {
        return m_PrimaryBypass;
    }
    
    void toggleSecondaryBypass()
    {
        m_SecondaryBypass = !m_SecondaryBypass;
    }
    
    bool getCurrentSecondaryBypass()
    {
        return m_SecondaryBypass;
    }
    
    /********************************************************************************/
    
    void setExpressionPedalValue(float pedalVal) // pedal Val is between 0.0 and 1.0
    {
        //std::cout << "Exp pedal val" << pedalVal << std::endl;
        m_RawExpPedalValue = pedalVal;
        switch(m_CurrentSection)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                m_PrimaryTempo = PRI_SECTION_BASE_TEMPO + (pedalVal - 0.5)*2*MAX_TEMPO_DEV;
                m_SecondaryTempo = SEC_SECTION_BASE_TEMPO + (pedalVal - 0.5)*2.0*1.03*MAX_TEMPO_DEV;
               // std::cout<<"Primary Tempo " << m_PrimaryTempo << std::endl;
               // std::cout<< "Secondary Tempo " << m_SecondaryTempo << std::endl;
                m_PrimarySeq->setTempo(m_PrimaryTempo);
                m_SecondarySeq->setTempo(m_SecondaryTempo);
                break;
            case 5:
                break;
            case 6:
                //map 1-expcurve to volume. loud when slower, soft when faster
                //map linear to tempo
                //map linear to random threshold, which would change the noteflow. 
                //m_PrimaryTempo = PRI_SECTION_BASE_TEMPO + (pedalVal - 0.3)*3.5*MAX_TEMPO_DEV; //bigger range for tempo range. biased towards the faster side
                m_SecondaryTempo = SEC_SECTION_BASE_TEMPO + (pedalVal - 0.2)*3.8*MAX_TEMPO_DEV;
                
                //m_PrimaryAmp = 0.1 + (1.0 - EXPCURVE(pedalVal, EXPSTEEPNESS))*(1.0 - 0.1); // 0.1 is the minimum amplitude, 1 is the max
                m_SecondaryAmp = 0.09 + (1.0 - EXPCURVE(pedalVal, EXPSTEEPNESS - 6))*(1.0 - 0.09);
                
                //m_PrimarySeq->setTempo(m_PrimaryTempo);
                m_SecondarySeq->setTempo(m_SecondaryTempo);
                
                break;
            case 7:
                break;
            case 8:
                //map expression pedal to volume 
                m_PrimaryAmp = 0.1 + (pedalVal)*(0.9);
               // m_SecondaryAmp = 0.05 + (EXPCURVE(pedalVal, EXPSTEEPNESS))*0.95;
                break;
            case 9: // second stick comes in with 32nd fill ins
                
                m_PrimaryAmp = 0.1 + (pedalVal)*(0.9);
                m_SecondaryAmp = 0.25 + (pedalVal)*0.75;
                break;
            case 10:
                break;
            case 11:
                break;
            case 12:
                m_SecondaryAmp = 0.5 + (pedalVal)*0.5; // use the expression pedal as dynamic device for unison sections. 
                break;
            default:
                break;  
        }
    }
    
    /********************************************************************************/
    void updatePrimaryCounter()
    {
        m_PrimaryTimerCounter++;
        if(m_PrimaryTimerCounter > 100000)
        {
            m_PrimaryTimerCounter = 0; // to avoid overflow
        }
    }
    
    void updateSecondaryCounter()
    {
        m_SecondaryTimerCounter++;
        if(m_SecondaryTimerCounter > 100000)
        {
            m_SecondaryTimerCounter = 0; // to avoid overflow
        }
    }
           
    /**********************************************************************************/
    
    float getPrimaryStrokeAmp()
    {
        return m_PrimaryAmp;
    }
    
    float getSecondaryStrokeAmp()
    {
        return m_SecondaryAmp;
    }
    
    /*********************************************************************************/
    // Helper Functions
    void setBypasses(bool pVal, bool sVal)
    {
        m_PrimaryBypass = pVal;
        m_SecondaryBypass = sVal;
    }
    
    void resetCounters()
    {
        m_PrimaryTimerCounter = 0;
        m_SecondaryTimerCounter = 0; 
    }
    
    /******************************************************************************/
    // Section Two functions
    
    void primarySectionTwo()
    {
        m_CurrentPrimaryPosition = m_PrimarySeq->getSequenceValue();
    }
    
    void secondarySectionTwo()
    {   
        m_CurrentSecondaryPosition = m_SecondarySeq->getSequenceValue();
    }
    
    /*****************************************************************************/
    void primarySectionThree() // short silence section
    {
        m_CurrentPrimaryPosition = 0;
    }
    
    void secondarySectionThree()
    {
        m_CurrentSecondaryPosition = 0;
    }
    /****************************************************************************/
    void primarySectionFour()
    {
       updatePrimaryCounter();
       if((m_PrimaryTimerCounter % (rand()%250 + 300)) == 0)
       {
           //change the volume every 300-550 samples, approximately 0.3-0.5 seconds;
           m_PrimaryAmp = m_PrimaryAmp + 0.08*m_PrimaryIncrDecrFlag;
           if(m_PrimaryAmp >= 1.0) // triangle ramp up and down
           {
             m_PrimaryAmp = 1.0;
             m_PrimaryIncrDecrFlag *= -1.0;
           }
           if(m_PrimaryAmp <= 0.2)
           {
             m_PrimaryAmp = 0.2;
             m_PrimaryIncrDecrFlag *= -1.0;
           }
           
          // m_PrimaryTimerCounter = 0; //reset the counter to avoid overflow. 
       }
       m_CurrentPrimaryPosition = m_PrimarySeq->getSequenceValue();
    }
    
    void secondarySectionFour()
    {
        updateSecondaryCounter();
        if((m_SecondaryTimerCounter % (rand()%250 +  340)) == 0)   // automatic amplitude envelope on the rhythms. 
        {
            m_SecondaryAmp = m_SecondaryAmp + 0.07*m_SecondaryIncrDecrFlag;
            if(m_SecondaryAmp <= 0.2)
            {
                m_SecondaryAmp = 0.2;
                m_SecondaryIncrDecrFlag *= -1.0;
            }
            if(m_SecondaryAmp >= 1.0)
            {
                m_SecondaryAmp = 1.0;
                m_SecondaryIncrDecrFlag *= -1.0;
            }
        }
        m_CurrentSecondaryPosition = m_SecondarySeq->getSequenceValue();
    }
    /************************************************************************************/
    void primarySectionFive()
    {
        m_CurrentPrimaryPosition = 0;
    }
    
    void secondarySectionFive()
    { 
        m_CurrentSecondaryPosition = 0;
    }
    /***********************************************************************************/
    
    void primarySectionSix()
    {
        updatePrimaryCounter();
//          if((m_PrimaryTimerCounter % (rand()%250 + 300)) == 0)
//          {
//             if( (rand()%1000)/1000.0  <  m_RawExpPedalValue )
//             {
//                 if(m_PrimarySeq->getCurrentNoteFlow() == Sequencer::k_Sixteenth)
//                 {
//                     m_PrimarySeq->setNoteFlow(Sequencer::k_EighthTriplet);
//                     m_PrimarySeq->reverseSequence();
//                 }else
//                 {
//                     m_PrimarySeq->setNoteFlow(Sequencer::k_Sixteenth);
//                     m_PrimarySeq->inPlaceShuffle();
//                 }  
//             }
//          }
        m_CurrentPrimaryPosition = m_PrimarySeq->getSequenceValue();
    }
    
    void secondarySectionSix()
    {
        updateSecondaryCounter();
        if((m_SecondaryTimerCounter % (rand()%250 + 300)) == 0)
         {
            if( (rand()%1000)/1000.0  <  m_RawExpPedalValue )
            {
                if(m_SecondarySeq->getCurrentNoteFlow() == Sequencer::k_Sixteenth)
                {
                    m_SecondarySeq->setNoteFlow(Sequencer::k_EighthTriplet);
                    m_SecondarySeq->inPlaceShuffle();
                }else
                {
                    m_SecondarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                    m_SecondarySeq->reverseSequence();
                }  
            }
         }
        m_CurrentSecondaryPosition = m_SecondarySeq->getSequenceValue();
    }
    /****************************************************************************************/
    
    void primarySectionSeven()
    {
        updatePrimaryCounter();
        if(m_PrimaryTimerCounter >= 4000) // 4seconds = 2 measure of quarter notes at 120 bpm
        {
            updateSection(1); // internally increment the section so that Jason can move his foot to the hihat.
        }
        m_CurrentPrimaryPosition = m_PrimarySeq->getSequenceValue();
    }
    
    void secondarySectionSeven()
    {
        m_CurrentSecondaryPosition = 0;
    }
    /*****************************************************************************************/
    
    void primarySectionEight()
    {
        m_CurrentPrimaryPosition = m_PrimarySeq->getSequenceValue();
    }
    
    void secondarySectionEight()
    {
        m_CurrentSecondaryPosition = 0;
    }
    
    /****************************************************************************************/
    
    void primarySectionNine()
    {
        updatePrimaryCounter();
       
        if((rand()%2000/2000.0) < 0.1) // 10% probability of generating a broken 16th note hihat pattern once in every 2- 2.25s
         {
             m_PrimarySeq->generateCellBasedSequence(8, -1);
         }else
         {
             m_PrimarySeq->generateCellBasedSequence(8, 1); // 90% chance that it is straight 16ths
         }
      
        m_CurrentPrimaryPosition = m_PrimarySeq->getSequenceValue();
    }
    
    void secondarySectionNine()
    {
        m_CurrentSecondaryPosition = m_SecondarySeq->getSequenceValue(); // no matter whether the primary is broken or not, secondary is always broken 
    }
    
    /*******************************************************************************************/
    
    void primarySectionTen()
    {
        m_CurrentPrimaryPosition = 0;
    }
    
    void secondarySectionTen()
    {
        m_CurrentSecondaryPosition = 0;
    }
    /*******************************************************************************************/
    
    void primarySectionEleven()
    {
        updatePrimaryCounter();
        if(m_PrimaryTimerCounter >= 4000) // 2 second = 1 measure at 120 bopm
        {
            updateSection(1); // internally increment the section so that Jason can move his foot to the hihat.
        }
        m_CurrentPrimaryPosition = m_PrimarySeq->getSequenceValue();
       // std::cout<< "Pos " << m_CurrentPrimaryPosition <<std::endl;
    }
    
    void secondarySectionEleven()
    {
        m_CurrentSecondaryPosition = 0;
    }
    
     /*******************************************************************************************/
    void primarySectionTwelve()
    {
       // std::cout << "In section 12 " << std::endl;
          m_UnisonMidi->updateSecondarySampleCounter();
          if(m_UnisonMidi->getIsSecondaryStrikeOn())
          {
             m_CurrentPrimaryPosition = 1;
         }else
         {
             m_CurrentPrimaryPosition = -1;
         }
    }
    
    void secondarySectionTwelve()
    {
//           m_UnisonMidi->updateSecondarySampleCounter();
//           if(m_UnisonMidi->getIsSecondaryStrikeOn())
//           {
//              m_CurrentSecondaryPosition = 1;
//           }else
//           {
//              m_CurrentSecondaryPosition = -1;
//           }
        m_CurrentSecondaryPosition = 0;
    }
    
     /*******************************************************************************************/
};


#endif