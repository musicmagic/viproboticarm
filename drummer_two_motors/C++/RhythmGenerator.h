 /*
  * Class for rhythm generation
  *
  * Author:     
  * Annie Zhang (kkanne901@gmail.com)
  *
  *
  */

#ifndef RHYTHMGENERATOR_H
#define RHYTHMGENERATOR_H

#include <math.h>
#include <vector>
#include <algorithm>

using namespace std;

class RhythmGenerator{
    
public:  
    RhythmGenerator(){
        init();
    }
    
    void init(){
        fullnote.push_back(1);
        fullnote.push_back(0);
        fullnote.push_back(0);
        fullnote.push_back(0);
        
        halfnote.push_back(1);
        halfnote.push_back(0);
        
        quarnote.push_back(1);
        
        restnote.push_back(0);
        restnote.push_back(0);
        restnote.push_back(0);
        restnote.push_back(0);
        
        readRhythm();
    }
    
    void readRhythm(){
        ifstream myRhythmFile;
        myRhythmFile.open("/home/meka/meka/rsc-drummer/drummer_two_motors/C++/sym");
        string str;
        if (myRhythmFile.is_open()) {
            while (myRhythmFile.good()) {
                getline(myRhythmFile, str);
                vector<char> tmp;
                if (str.size()>0){
                    for (int i=0; i<str.size(); i++)
                        if (str[i] != ' ')
                            tmp.push_back(str[i]);
                    original.push_back(tmp);
                }
            }
        }
        else
            cout<<"could not open file"<<endl;
        myRhythmFile.close();
    }
    
    void printRhythm(int idx){
        if (original.size() == 0){
            cout<<"Rhythm empty!"<<endl;
            return;
        }
        else if (idx > original.size() || idx < 0){
            cout<<"Rhythm index out of bound"<<endl;
            return;
        }
        cout<<"printing rhythm ... "<<endl;
        if (idx == 0){
            for (int i=0; i<original.size(); i++){
                for (int j=0; j<original[i].size(); j++)
                    cout << original[i][j]<<" ";
                cout<<endl;
            }
        }
        else{
            for (int j=0; j<original[idx-1].size(); j++)
                cout << original[idx-1][j]<<" ";
            cout<<endl;
        }
    }
    
     void printRhythm(vector<char> &v){
        if (v.size() == 0){
            cout<<"Rhythm empty!"<<endl;
            return;
        }
        cout<<"printing rhythm ... "<<endl;
        for (int i=0; i<v.size(); i++)
            cout << v[i] << " ";
        cout<<endl;
    }
    
    void printSequence(vector<int> &v){
        if (v.size() == 0)
            return;
        cout<<"printing sequence ... "<<endl;
        for (int i=0; i<v.size(); i++)
            cout << v[i] << " ";
        cout<<endl;
    }
    
    void getOrigin(vector<char> &rhy, vector<int> &seq, int idx){
        if (original.size() <= 0){
            cout<<"rhythm empty"<<endl;
            return;
        }
        else if (idx > original.size() || idx < 0){
            cout<<"query rhythm index out of bound"<<endl;
            return;
        }
        
        if (idx == 0){                       // get all rhythms, each repeat twice
            for (int i=0; i<original.size(); i++){
                for (int j=0; j<original[i].size(); j++)        // get rhythm
                    rhy.push_back(original[i][j]);
                for (int k=0; k<2; k++)                         // get rhythm sequence
                    for (int j=0; j<original[i].size(); j++)
                        rhythm2int(seq, original[i][j]);
            }
        }
        else {
            for (int j=0; j<original[idx-1].size(); j++)
                rhy.push_back(original[idx-1][j]);
            for (int k=0; k<2; k++)
                for (int j=0; j<original[idx-1].size(); j++)
                    rhythm2int(seq, original[idx-1][j]);
        }
        printRhythm(idx);
        printSequence(seq);
    }
    
    void rhythm2int(vector<int> &v, char c){
        switch (c) {
            case '|':
                v.insert(v.end(), fullnote.begin(), fullnote.end());
                break;
            case '+':
                v.insert(v.end(), halfnote.begin(), halfnote.end());
                break;
            case '#':
                v.insert(v.end(), quarnote.begin(), quarnote.end());
                break;
            case '0':
                v.insert(v.end(), restnote.begin(), restnote.end());
                break;
            default :
                break;
        }
    }
    
    void generateRhythm(vector<char> &origin, vector<int> &newseq, double sensitivity) {
        vector<char> newrhy;
        int len = origin.size();
        for (int i=0; i<len; i++){
            if (origin[i] == '|'){
                if ((rand()%10+1) > sensitivity*10) {
                    if ((rand()%10+1) > sensitivity*10){
                        newrhy.push_back('+');
                        newrhy.push_back('+');
                        rhythm2int(newseq, '+');
                        rhythm2int(newseq, '+');
                    }
                    else {
                        for (int k=0; k<4; k++){
                            newrhy.push_back('+');
                            rhythm2int(newseq, '#');
                        }
                    }
                }
                else{
                    newrhy.push_back('|');
                    rhythm2int(newseq, '|');
                }
            }
            else if (origin[i] == '+') {
                if ((rand()%10+1) > sensitivity*10) {
                    for (int k=0; k<2; k++){
                        newrhy.push_back('#');
                        rhythm2int(newseq, '#');
                    }
                }
                else {
                    newrhy.push_back('+');
                    rhythm2int(newseq, '+');
                }
            }
        }
        printRhythm(newrhy);
        printSequence(newseq);
    }
    
private:
    vector<int> fullnote;
    vector<int> halfnote;
    vector<int> quarnote;
    vector<int> restnote;
    vector<int> rhythmSeq;
    vector<vector<char> > original;
};

#endif