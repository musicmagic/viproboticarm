 /*
  * A step sequencer class 
  *
  * Author:     
  * Mason Bretan (masonbretan@gmail.com)
  *
  *
  */

#ifndef SEQUENCER_H
#define SEQUENCER_H

#include <math.h>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include "RhythmGenerator.h"
#include "TempoManager.h"
#include "CellBasedRhythmGen.h"

#define EXPCURVE(X, Y) (exp(X*log(Y)) - (float)1.0)/(Y - (float)1.0)

class Sequencer{
    int nSamplesPerInterval;
    std::vector<int> sequence; //sequencer vector (1's for strike, 0's for no strike);
    int sampleCount;
    int sequenceCount;
    int randomMode;
    float sr;
    int rhythmMode; // rhythmMode is being set in the constructor. Both randomMode and rhythmMode are arguments to the constructor.
    float quarterNoteIntervalTime;
    float baseTempo;
    RhythmGenerator rg;
    TempoManager tempoManager;
    CellBasedRhythmGen cellGen;
    
    float k_SixteenthNote; // these four should be consts but generates warnings with gnu+11 compiler.
    float k_EighthNote;
    float k_QuarterNote;
    float k_EighthNoteTriplet;
    //AccNovelty try;
    
    
public:
    
    
    int rhythmIdx;
    vector<int>tempoScale;
    std::vector<vector<int> >seqs;
    
    Sequencer()
    {   std::srand(unsigned (std :: time(0)));
        sequenceCount = -1;
        sampleCount = 0;
        randomMode = 0;
        rhythmMode = 0;
        sr = 0;
        rg = RhythmGenerator();
        tempoManager = TempoManager();
        cellGen = CellBasedRhythmGen();
        
        
        k_SixteenthNote = 0.25; // these four should be consts but generates warnings with gnu+11 compiler.
        k_EighthNote = 0.5;
        k_QuarterNote = 1;
        k_EighthNoteTriplet = (float)1.0/3.0;

        rhythmIdx = 0;
        if (rhythmMode == 0) // used to bypass Annie's stuff.
                generateRandomSequence(0,4); // creates 16th note pattern for 4 quarter notes as the starting point using the CellRhythmGen 
        else if(rhythmMode == 1) // rhythmmode == 1 is to generate improvise new sequence using the rhythm generator. The long sequence would be created.
                improviseMode();
        else if (rhythmMode == 2)
                tempoMode(); // the starting point is the simple rhythm stored in sym. but later in getStrike() this flag is used to change the tempo continously
        else if(rhythmMode == 3)
                exchangeTempoMode(); // This creates multiple sequences and then exhcnages the tempo between these
    }
    Sequencer(int rMode, int rhyMode){
        std::srand(unsigned (std :: time(0)));
        sequenceCount= -1;
        sampleCount=0;
        randomMode = rMode;
        rhythmMode = rhyMode;
        sr = 0;
        rg = RhythmGenerator();
        tempoManager = TempoManager();
        cellGen = CellBasedRhythmGen();
        cellGen.printCells();
        
        k_SixteenthNote = 0.25; // these four should be consts but generates warnings with gnu+11 compiler.
        k_EighthNote = 0.5;
        k_QuarterNote = 1;
        k_EighthNoteTriplet = (float)1.0/3.0;
        rhythmIdx = 0;
        if (rhythmMode == 0) // used to bypass Annie's stuff.
                generateRandomSequence(0,4); // creates 16th note pattern for 4 quarter notes as the starting point using the CellRhythmGen 
        else if(rhythmMode == 1) // rhythmmode == 1 is only used to generate the starting sequence using the rhythm generator. The long sequence would be created.
                improviseMode();
        else if (rhythmMode == 2){
                tempoMode();
                rhythmMode = 2;
        }// the starting point is the simple rhythm stored in sym. but later in getStrike() this flag is used to change the rhythm continously
        else if(rhythmMode == 3){
                exchangeTempoMode();
                rhythmMode = 3;
        }// This creates multiple sequences and then exhcnages the tempo between these
    }
    

    int getStrike()//call only once for each sample
    {       
        updateSample();
        if(sampleCount==1){
            sequenceCount++;
            if(sequenceCount >= sequence.size())
            {  // if random mode is onchange sequence right here (at the end of the sequence) so that every loop cycle it would be different
                if(randomMode)
                { //  shuffle the sequence 25% of the time.
                    if((rand()%1000/1000.0) > 0.75)
                    {
                       // std::cout << "in place shuffling " <<std::endl;
                        std::random_shuffle(sequence.begin(), sequence.end());
                        printSequence(sequence);
                    }else //change the noteflow, adjust the nSamplesPerInterval and create a new pattern for some number of quarter notes from rhythmic cells correposnding to the correct noteflow
                    {
                        //  instead of in place shuffle we can may be change the note flow to something else and then generate a new sequence that would make sense in that note flow based on cells.
    					
                        int newNoteFlow = rand()%4; // 0,1,2,3 correposnd to 16th, 8th, quarter and 8th triplet
                        int numQuarterNotes = 4*(rand()%2 + 1);
                       // std::cout << "new note flow " << newNoteFlow << " Measures is " << (int)numQuarterNotes/4 << std::endl;
                        generateRandomSequence(newNoteFlow,numQuarterNotes);// the first argument is the new note flow, the second argument is the num of quarter notes over which this pattern would play
    					
                    }
                   // dont change the tempo ALWAYS. instead 65% of the time, when the end of the seuqnce is reached change the tempo
                   //if((rand()%1000/1000.0) > 0.55)
                   //{
                     //  float idealIntervalTime = (rand()%110 + 300)/1000.0;
                      // nSamplesPerInterval = (int)(idealIntervalTime / sr);
                      
                   //}
                }
            
                sequenceCount = 0;
                
                // change tempo here...at the end of the sequence and also if the rhythmmode == 2
                if (rhythmMode == 2)
                {
                    nSamplesPerInterval = tempoManager.updateTempo(nSamplesPerInterval, sr);
                    cout << tempoManager.getTempo(nSamplesPerInterval, sr)<< endl;
                }
                else if (rhythmMode == 3)
                {
                    if (rhythmIdx == 0)
                    {
                        int t = tempoScale[0];
                        tempoScale[0] = tempoScale[1];
                        tempoScale[1] = t;
                    }
                    nSamplesPerInterval = tempoScale[rhythmIdx];
                    setSequence(seqs[rhythmIdx]);
                    rhythmIdx = (rhythmIdx+1)%2;
                    cout<< nSamplesPerInterval <<endl;
                }
            }
            
            return sequence[sequenceCount];
        }else{
            return -1;
        }
    }
    
    void setSequence(std::vector<int> seq ){
        //std::vector<int> sequence (seq, seq + sizeof(seq) / sizeof(seq[0]) );
        //int count = sizeof(seq) / sizeof(seq[0]);
        //for (int i=0; i<count; i++) sequence.push_back(seq[i]);
        
        sequence = seq;
       // printSequence();
    } 
    
    void setParams(double bTempo, double sampleRate){
        //provide ideal interval time in seconds
        sr = sampleRate; // a local copy of sample rate;
        baseTempo = bTempo;
        quarterNoteIntervalTime = ((float)60.0)/baseTempo;// baseTempo refers to the starting tempo which will be constant. The task is to change the interval between sequence elements in such a way that the resulting note flow will be proper subdivisions of the original tempo
        double idealIntervalTime = quarterNoteIntervalTime*k_SixteenthNote; // each sequence element is interpreted as a 16th note.
        nSamplesPerInterval = (int)(idealIntervalTime / sampleRate); // in samples. 
    }
    
    void tempoMode(){
        std::vector<char> rhythm;
        std::vector<int> seq;
        std::vector<int> newseq;
        std::vector<int> tmp;
        std::cout<< "original rhythm ... " <<std::endl;
        rg.getOrigin(rhythm, seq, 1); 
        setSequence(seq);
    }
    
    void improviseMode(){
        std::vector<char> rhythm;
        std::vector<int> seq;
        std::vector<int> newseq;
        std::vector<int> tmp;
        std::cout<< "original rhythm ... " <<std::endl;
        rg.getOrigin(rhythm, seq, 1);                           // get rhythm by index, 0 - all
        std::cout<< "generated new rhythm ... "<< std::endl;
        for (int l=0; l<5; l++){
            tmp.clear();
            rg.generateRhythm(rhythm, tmp, 0.5);
            for (int k=0; k<2; k++)
                newseq.insert(newseq.end(), tmp.begin(), tmp.end());
            for (int i=0; i<10; i++)                    // a period of silence
                newseq.push_back(0);
        }
        
        std::cout<<"new sequence ... "<<std::endl;
        for (int i=0; i<newseq.size(); i++)
            std::cout<< newseq[i]<<" ";
        std::cout<<std::endl;
        
        for (int i=0; i<10; i++)                    // a period of silence
             seq.push_back(0);
        for (int i=0; i<newseq.size(); i++)
            seq.push_back(newseq[i]);
        for (int i=0; i<10; i++)                    // a period of silence
             seq.push_back(0);
        setSequence(seq);
    }
    
    void exchangeTempoMode(){
        std::vector< vector<char> > rhythms;
        std::vector<char> tmprhythm;
        std::vector<int> tmpseq;
        std::cout<< "first rhythm ... " <<std::endl;
        rg.getOrigin(tmprhythm, tmpseq, 2);
        vector<int> tmp;
        for (int i=0; i<tmpseq.size(); i++)
            tmp.push_back(tmpseq[i]);
        seqs.push_back(tmp);
        
        tmprhythm.clear();
        tmpseq.clear();
        tmp.clear();
        std::cout<< "second rhythm ... " <<std::endl;
        rg.getOrigin(tmprhythm, tmpseq, 3);
        for (int i=0; i<tmpseq.size(); i++)
            tmp.push_back(tmpseq[i]);
        seqs.push_back(tmp);
        
        tempoScale.push_back(220);
        tempoScale.push_back(100);
        
        nSamplesPerInterval = tempoScale[0];
        
        setSequence(seqs[0]);
        rhythmIdx = 1;
    }
    
    double getSamplesPerInterval(void){
        return nSamplesPerInterval;
    }
    
  
        
    void generateRandomSequence(int noteFlowMode, int numQuarterNotes)
    {
    	double idealIntervalTime;
    	switch(noteFlowMode)
    	{
    		case 0:  // 16th notes // modify inter sequence element interval time depending on the noteFlow selected in seconds
    			idealIntervalTime = quarterNoteIntervalTime*k_SixteenthNote;
                break;
    		case 1:  // eighth note
    			idealIntervalTime = quarterNoteIntervalTime*k_EighthNote;
                break;
    		case 2: // quarter
    			idealIntervalTime = quarterNoteIntervalTime*k_QuarterNote;
                break;
    		case 3: // eighth note triplet
    			idealIntervalTime = quarterNoteIntervalTime*k_EighthNoteTriplet;
    		break;
    
    	}
        nSamplesPerInterval = (int)(idealIntervalTime / sr); // modified inter sequence element time in samples.
    	sequence.clear(); // clear the existing sequence and fill it with new pattern
 		cellGen.generateCellBasedRhythm(sequence, noteFlowMode, numQuarterNotes); // This should create a sequence numQuarternotes long at the initial tempo and with an underlying note flow specified by 'noteFlow'
        printSequence(sequence);
    }
    
    void printSequence(std::vector<int>&seq)
    {
        for(int i=0; i<seq.size(); i++)
        {
            std::cout << seq[i] << " ";
        }
        std::cout << std::endl;
    }
private:
    void updateSample(){
        sampleCount++;
        if(sampleCount >= nSamplesPerInterval){
            sampleCount = 1;
        }
    } 
};

#endif