#ifndef MAIN_CONTROL_H
#define MAIN_CONTROL_H

#include <cmath>
#include <iostream>
#include "Low_level_control.h"
#include "Stroke_Generator.h"

class  Main_Control {
    Low_level_control lowLevelControl;
    Stroke_Generator* primaryStrokeGenerator;
    Stroke_Generator* secondaryStrokeGenerator;
    int currentPiece;
    int currentSection;
    
    double sampleRate;
    
    
public:
    Main_Control(){
        lowLevelControl = Low_level_control();
        sampleRate = .001;
        primaryStrokeGenerator = new Stroke_Generator(sampleRate, -6.9, 0, -1.0); // 3.4 from upper hard limit
        secondaryStrokeGenerator = new Stroke_Generator(sampleRate,6.8, 0, 1.33); // 3.4 from upper hard limit
        currentPiece =0;
        currentSection=0;
    }
    
    
    ~Main_Control(){
        delete primaryStrokeGenerator;
        delete secondaryStrokeGenerator;
    }
    
    //getters for piece and section
    int getCurrentPiece(){
        return currentPiece;
    }
    int getCurrentSection(){
        return currentSection;
    }
    
    //getters for kp and kd
    double getPrimaryKp(){
        return lowLevelControl.getPrimaryKp();
    }
    double getPrimaryKd(){
        return lowLevelControl.getPrimaryKd();
    }
    double getSecondaryKp(){
        return lowLevelControl.getSecondaryKp();
    }
    double getSecondaryKd(){
        return lowLevelControl.getSecondaryKd();
    }
    
    
    
    //setters for piece and section
    void setCurrentPiece(int piece){
        if(piece != -1){
            currentPiece = piece;
        }
    }
    int setCurrentSection(int section){
        if(section != -1){
            currentSection = section;
        }
    }
    
    // setters for accelerometer data
     void updateAccData(float accX, float accY, float accZ)
     {
         int hint = lowLevelControl.setAccData(accX,accY,accZ);
         if(hint == 1)
         {
             primaryStrokeGenerator->park();
             secondaryStrokeGenerator->park();
         }
     }
     void setAccWork()
     {
         lowLevelControl.setAccToggle();
     }
    
    //setters for kp and kd
    void setPrimaryKpKd_usingPot(float potValue){
        lowLevelControl.setPrimary_usingPot(potValue);
    }
    void setPrimaryKp_usingMessage(float val){
        lowLevelControl.setPrimaryKp(val);
    }
    void setPrimaryKd_usingMessage(float val){
        lowLevelControl.setPrimaryKd(val);
    }
    
    //strike functions and arm position functions
    double updatePrimaryPos(){
        return primaryStrokeGenerator->updatePos();
    }
    double updateSecondaryPos(){
        return secondaryStrokeGenerator->updatePos();
    }
    void primaryArmStrike(){
        primaryStrokeGenerator->singleStrike();
    }
    void secondaryArmStrike(){
        secondaryStrokeGenerator->singleStrike();
    }
  
    
private:
    
    
};

#endif