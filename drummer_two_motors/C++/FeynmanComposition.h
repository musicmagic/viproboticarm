/******Authored by Deepak Gopinath********************/


#ifndef FEYNMANCOMPOSITION_H
#define FEYNMANCOMPOSITION_H


#include <iostream>
#include "ParseOnsetText.h"

using namespace std;
class FeynmanComposition
{
public:
    FeynmanComposition(string primarypath, string secondarypath, float tempo = 60.0)
    {
        m_PrimaryOnsetParser = new ParseOnsetText(primarypath, tempo);
        m_SecondaryOnsetParser = new ParseOnsetText(secondarypath, tempo);
        
        m_PrimaryOnsetParser->getOnsetSequenceInSamples(m_PrimaryOnsetSeq);
        m_SecondaryOnsetParser->getOnsetSequenceInSamples(m_SecondaryOnsetSeq);
        
        m_PrimarySampleCounter = m_SecondarySampleCounter = 0;
        m_PrimarySeqIndex = m_SecondarySeqIndex = 0;
        m_IsPrimaryStrikeOn = m_IsSecondaryStrikeOn = false;
        
    }
    
    ~FeynmanComposition()
    {
        delete m_PrimaryOnsetParser;
        delete m_SecondaryOnsetParser;
    }
    
    void updatePrimarySampleCounter()
    {
        m_IsPrimaryStrikeOn = false;
        m_PrimarySampleCounter++;
        if(m_PrimarySampleCounter >= (m_PrimaryOnsetSeq[m_PrimarySeqIndex + 1] - m_PrimaryOnsetSeq[m_PrimarySeqIndex]))
        {
            m_IsPrimaryStrikeOn = true;
            m_PrimarySampleCounter = 0;
            m_PrimarySeqIndex++;
            if(m_PrimarySeqIndex >= m_PrimaryOnsetSeq.size())
            {
                m_PrimarySeqIndex = 0;
            }
        }
    }
    void updateSecondarySampleCounter()
    {
        m_IsSecondaryStrikeOn = false;
        m_SecondarySampleCounter++;
        if(m_SecondarySampleCounter >= (m_SecondaryOnsetSeq[m_SecondarySeqIndex + 1] - m_SecondaryOnsetSeq[m_SecondarySeqIndex]))
        {
            m_IsSecondaryStrikeOn = true;
            m_SecondarySampleCounter = 0;
            m_SecondarySeqIndex++;
            if(m_SecondarySeqIndex >= m_SecondaryOnsetSeq.size())
            {
                m_SecondarySeqIndex = 0;
            }
        }
    }
    //getter for the current state of the arm
    bool getIsPrimaryStrikeOn()
    {
       return m_IsPrimaryStrikeOn;
    }
    bool getIsSecondaryStrikeOn()
    {
       return m_IsSecondaryStrikeOn;
    }
private:
    
    ParseOnsetText *m_PrimaryOnsetParser;
    ParseOnsetText *m_SecondaryOnsetParser;
    
    int m_PrimarySampleCounter;
    int m_SecondarySampleCounter;
    
    std::vector<int> m_PrimaryOnsetSeq;
    std::vector<int> m_SecondaryOnsetSeq;
    
    int m_PrimarySeqIndex;
    int m_SecondarySeqIndex;
    
    bool m_IsPrimaryStrikeOn;
    bool m_IsSecondaryStrikeOn;
    
            
};
#endif