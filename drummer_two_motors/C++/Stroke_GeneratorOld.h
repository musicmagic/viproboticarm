/*
 * This class allows the behavior_controller to generate a control signal
 *for bot periodic and non periodic strikes. The function "strike" is called at the time
 *a drum strike is desired. It also creates an instance of Sequencer
 *and has functions for updating and controlling it.
 *
 *
 * Author:
 * Mason Bretan (masonbretan@gmail.com)
 *
 *
 */

#ifndef STROKE_GENERATOR_H
#define STROKE_GENERATOR_H

#include <math.h>
#include "Sequencer.h"
#include "Periodic.h"

class Stroke_Generator{
    double strikeCount; // this needs to be a double b/c it sends out a signal (stepper function)
    double sampleRate;
    Sequencer sequencer;
    Periodic periodic;

    double steadyStateOutput; //outputs are quadrature positions (up/neutural pos)
    double strikeOutput; //down pos
    double strikeDelay; 
    int samplesSinceStrike;
    int delaySamples;
    
    int sequencerStrokeType;
    
    float parkPos;
    
    
public:
    Stroke_Generator(double sr, float _parkPos, float steadyState, float _strikeOutput){
        sampleRate = sr;
        periodic =  Periodic(sr);
        setDefaults();
        sequencer = Sequencer(1,0); // first argument is randomMode, second argument is rhythmMode.
                                    // For Annie's stuff keep first argument as 0 and the second argument (rhythmMode) to be 1 2 or 3
                                    // For Deepak's stuff, keep first argument to be 1 and the second argument to be 0.
        steadyStateOutput = steadyState;
        strikeOutput = _strikeOutput;
        parkPos = _parkPos;
    }

    double strike(void) {
        //strikeCount = strikeCount + 1.0;
        //std::cout <<strikeCount<<"\n";
        //return strikeCount;

        samplesSinceStrike=0;
        return strikeOutput;
    }
    double getStrikeCount() const
    {
        return strikeCount;
    }
    
    void setSequencerParams(double baseTempo, double sr ){
        sequencer.setParams(baseTempo,sr);
    }
 
    double playMode(int mode){
        switch (mode){
            case 1: 
                return getSequencerStrike();
            case 2: 
                sequencer.improviseMode();
                return getSequencerStrike();
            default:
                return steadyStateOutput;
        }  
    }
    double singleStrike(void){
        samplesSinceStrike=0;
        sequencerStrokeType=1;
    }
    double updatePos(){
        return updateOut();
    }
    
    double park(){
        sequencerStrokeType = 4;
    }
    
    double getSequencerStrike(){
        
        int sequencerOutput = sequencer.getStrike();
        if(sequencerOutput!= -1){
            sequencerStrokeType = sequencerOutput;
        }
        switch(sequencerOutput){
            case -1:
                return updateOut();
            case 0:
                return steadyStateOutput;
            case 1:
                return strike();
            case 2:
                periodic.setFrequency(10);
                return periodic.output();
            default:
                return steadyStateOutput;
        }
        /*
        if(sequencer.getStrike()!=0){
            return strike();
        }else{
            return updateOut();
            //return strikeCount;
        }*/
    }
    
private:
    void setDefaults(void){
        //set defaults
        strikeCount = 0.0;
        strikeDelay = .06;//send out hit position for 1/10 of a second
        delaySamples = (int)(strikeDelay / sampleRate);
        sequencerStrokeType = 0;
    }
    double updateOut(){
        if(sequencerStrokeType == 1){ //single stroke
            samplesSinceStrike++;
            if(samplesSinceStrike <= delaySamples){
                return strikeOutput;
            }else{
                samplesSinceStrike = delaySamples +1; //just so it doesn't get too high for high sample rates
                return steadyStateOutput;
            }
        }
        
        if(sequencerStrokeType == 2){ //periodic stroke
            return periodic.output();
        }
        
        
        if(sequencerStrokeType == 4){
            return parkPos;
        }
        return steadyStateOutput;
    }
};

#endif