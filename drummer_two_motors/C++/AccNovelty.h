//
//  AccNovelty.h
//  acc_novelty
//  A class that detect novelty through accelerometer
//  Created by Minwei Gu on 2/2/14.
//  AccX: 1600 - 1800 AccY: 1900 - 2100
//

#ifndef __acc_novelty__AccNovelty__
#define __acc_novelty__AccNovelty__

#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <time.h>

using namespace std;

class AccNovelty{
    
private:
    int accCount;
    int counter;
    bool isNovelty;
    int bridgeCount;
    float kdValue;
    float threshold;
    vector<int> noveltyIndex;
    map<int, vector<float> > accData;
    float avgX,avgY,avgZ;
    float prevX, prevY, prevZ;
    
public:
    AccNovelty(){
        accCount = 0;
        counter = 0;
        isNovelty = false;
        bridgeCount = 0;
        avgX = avgY=avgZ = 0;
        prevX = prevY = prevZ = 0;
        threshold = 0.0001;
        kdValue = 0.25;
    }
    ~AccNovelty(){
    }
    void parseAccData(string accFile){
    }
    float parseAccData(float accX, float accY, float accZ){
        //cout<<"acc recieved!"<<endl;
        int key = accCount;
        vector<float> value(3);
        value[0] = accX;
        value[1] = accY;
        value[2] = accZ;
        if (counter%100 == 10)
        {
            accData.insert(pair<int,vector<float> >(key,value));
            if (accData.size()==40){
                srand(time(NULL));
                if (noveltyDetection(accX,accY))     // novelty function calls every 20ms
                {
                    cout<<"novelty detected! "<<endl;
                    kdValue = (rand()%3 / 9.0)+0.001;
                    cout<<"generated a new KD: " << kdValue <<endl;
                    return kdValue;
                }
            }
            
            prevX = accX;
            prevY = accY;
            prevZ = accZ;
            accCount = (accCount+1)%40;
            
        }
        counter = (++counter)%100;
        return kdValue;
    }
    bool noveltyDetection(float accX, float accY)
    {
        if (accX < 1640 && accY < 1950 && bridgeCount>40)
        {
            cout<<accX << " "<<accY<<endl;
            bridgeCount = 0;
            isNovelty = true;
        }
        else
        {
            bridgeCount++;
            isNovelty = false;
        }
        return isNovelty;
    }
    void noveltyDetection(){
        // detect novelty
        float tempX = accData.find(accCount)->second[0];
        float tempY = accData.find(accCount)->second[1];
        if (tempX < 1630 && tempY < 1950 && bridgeCount>40)
        {
            cout<< "novelty detected! "<<tempX<<" "<<tempY<<endl;
            bridgeCount = 0;
            isNovelty = true;
        }
        else
        {
            bridgeCount++;
            isNovelty = false;
        }
        //update the average and threshold
        if (accData.find(accCount)->second.size()==3)
        {
            avgX = (accData.find(accCount)->second[0]+avgX * (accData.size()-1))/accData.size();
            avgY = (accData.find(accCount)->second[1]+avgY * (accData.size()-1))/accData.size();
            avgZ = (accData.find(accCount)->second[2]+avgZ * (accData.size()-1))/accData.size();
        }
        threshold = avgY;
    }
};// using a simple moving average filter to detect the novelty
    

#endif /* defined(__acc_novelty__AccNovelty__) */
