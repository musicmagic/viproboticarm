#ifndef MASON_FREESTYLE_H
#define MASON_FREESTYLE_H

#include <cmath>
#include <iostream>
#include "Periodic.h"



class  Mason_Freestyle {
    
    float avgX, avgY, avgZ;
    float lastY;
    int bufferSize;
    float avgBuffer[200];
    
    int modeSize;
    float modeBuffer[100];
    int count;
    float avg,difference,lastAverage;
    int countSinceLastStop;
    
public:
    Mason_Freestyle(){
        //init
        avgX = 0;
        avgY = 0;
        avgZ = 0;
        
        
    lastY = 0;
    bufferSize = 200;
    //avgBuffer[200] = {};
    
    modeSize = 100;
    //modeBuffer[100] = {};
    count = 0;
    avg=difference=lastAverage = 0;
    countSinceLastStop = 0;
        
    }
    
    double respondToAcc(float xAcc, float yAcc, float zAcc){
        //currently just using z and y\

        avg = average(zAcc);
        difference = std::abs(lastAverage - avg);
        lastAverage = avg;
        count++;
        
        if( difference < .03 ){
            if(getMode(difference) == true){
                //cout<<"diff: "<<difference<<endl;
                lastAverage = avg;
                count++;
                countSinceLastStop =0 ;
                std::cout<<"diff: "<<"1"<<std::endl;
                return 1;
            }
        }
        countSinceLastStop++;
        if(countSinceLastStop >400){
            lastAverage = avg;
            count++;
            //cout<<"diff: "<<"0"<<endl;
            return .0001;
        }else{
            lastAverage = avg;
            count++;
            //cout<<"diff: "<<"1"<<endl;
            return 1;
        }
    }
    
private:
    
    float average(float val){
        int index = count % bufferSize;
        avgBuffer[index] = val;
        
        
        //brute force mean, should optimize
        int i=0;
        float total=0.0;
        for(i = 0;i<bufferSize;i++){
            total +=avgBuffer[i];
        }
        
        return total / bufferSize;
    }
    
    bool getMode(float val){
        int index = count % modeSize;
        modeBuffer[index] = val;
        
        int i=0;
        int total=0;
        for(i = 0;i<modeSize;i++){
            if(modeBuffer[i] <.01){
                total +=1;
            }
        }
        if (total >= 70) return true; else; return false;
    }
    
};

#endif