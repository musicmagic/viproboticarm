 /*
  * A class for converting a text file with onsets into a sequence
  *
  * Author:     
    Deepak Gopinath and Iman Mukherjee
  *
  *
  */

#ifndef PARSEONSETTEXT_H
#define PARSEONSETTEXT_H

#include <vector>
#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;


class ParseOnsetText {
    
    float m_Tempo;
    std::vector<int> m_OnsetSequenceInSamples;
    float m_SampleRate;

public:
    ParseOnsetText(string path, float initialTempo = 60.0)
    {
        m_Tempo = static_cast<float>(60.0/initialTempo);
        m_SampleRate = 1000.0;
        ifstream fileHandle;
        fileHandle.open(path.c_str());
        string str;
        
        if(fileHandle.is_open())
        {
            while(fileHandle.good())
            {
                getline(fileHandle, str);
                m_OnsetSequenceInSamples.push_back(static_cast<int>(atof(&str[0])*m_Tempo*m_SampleRate));
            }
        }
        
        fileHandle.close();
        str.clear();
    }
 
    ~ParseOnsetText()
    {
    }
    
    void getOnsetSequenceInSamples(std::vector<int> &seq)
    {
        seq = m_OnsetSequenceInSamples;
    }
    
};

#endif

