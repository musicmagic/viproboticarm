#ifndef RUDIMENTSNEW_H
#define RUDIMENTSNEW_H

#include <math.h>
#include <vector>
#include "RudimentSequencer.h"
#include "TempoManager.h"

class RudimentManager
{
public:
    enum RudimentNoteFlow_t
    {
    	k_Sixteenth,
    	k_Eighth,
    	k_Quarter,
    	k_EighthTriplet
    };

    enum RudimentType
    {
    	k_SingleStroke,
    	k_DoubleStroke,
    	k_SingleParadiddle,
    	k_DoubleParadiddle,
    	k_TripleParadiddle,

    	k_Flam,
    	k_FlamTap,
    	k_FlamAccent,
                
        k_SingleDrag,
        k_DoubleDrag,
        k_TripleDrag
    };
private:
	// once approach is to break down what each hand is doing separately and thinking of the patterns as independent rhythms. 
	// rudiments are usually based of small building blocks and they are repeated as many times as wanted at a specified tempo, time signatre

	// the pattern played by each hand can be thought off as a sequence, if the sequence is played once, then the building block would be played once

    /*Building blocks
	

	Single stroke - R L =   R - 1 0, L - 0 1
	Double stroke - R R L L   =    R  -   1 1 0 0, L - 0 0 1 1, at any tempo, any note flow, this pattern if played on each stick simultaneously will give the double stroke pattern
	Single Paradiddle - R L R R  L R L L    =   R - 1 0 1 1  0 1 0 0,   L = 0 1 0 0  1 0 1 1
	Double Paradiddle -	R L R L R R  L R L R L L -  R = 1 0 1 0 1 1  0 1 0 1 0 0 , L = 0 1 0 1 0 0  1 0 1 0 1 1 
	Triple Paradiddle - RLRLRLRR LRLRLRLL - R = 10101011 01010100 L = 01010100 10101011

		// In all the previous ones the L pattern is exactly the same sequence, as right except starting with an offset
	   		Offset in number of sequenceElement 
	  	 Single Stroke = 1
	  	 Double Stroke = 2
	  	 Single paradiddle = 4
	  	 Double Para = 6
	  	 Triple Para = 8

	Flam rudiments are more tricky because they involve both sticks playing ALMOST at the same time. quantifyin the "flaminess"
	in samples is going to be challenging.

	Let us look at some basic flam rudiments. 

	Flam - lR rL ,  R = 1 1 , L = 1 1, if repeated twice, this i will R - 1 1  1 1  L - 1 1  1 1, except that the distance between 1&2 and 3&4 is smaller than 2&3 and 4&1. That is the slight sample offset, which will bring about the flam.
    FlamTap = lR R  rL L    , R = 1 1 1  0   L = 1  0 1 1
    FlamAccent lR L R rL R L,   R = 1  0  1 1   1  0  L =  1  1  0 1   0  1 
    */

    RudimentSequencer *m_PrimaryStickPattern;
    RudimentSequencer *m_SecondaryStickPattern;

    RudimentType e_RudimentType;

    float k_SixteenthNote; // These are specifying the noteFlow at a particular tempo. 
    float k_EighthNote;
    float k_QuarterNote;
    float k_EighthNoteTriplet;

    float m_CurrentNoteFlow;
    float m_BaseTempo;

    float m_SampleRate;
   
    bool isRudimentBypassed;

    long int m_PhraseSampleCounter;
    long int m_TotalPhraseLengthInSamples;


public:
	RudimentManager()
	{
		m_PrimaryStickPattern = new RudimentSequencer();
		m_SecondaryStickPattern = new RudimentSequencer();
		
		k_SixteenthNote = 0.25; // these four should be consts but generates warnings with gnu+11 compiler.
        k_EighthNote = 0.5;
        k_QuarterNote = 1;
        k_EighthNoteTriplet = (float)1.0/3.0;
        m_SampleRate = 0.001;

        m_CurrentNoteFlow = k_SixteenthNote;
        m_BaseTempo = 100;
        isRudimentBypassed = true;
        m_PhraseSampleCounter = 0;
        m_TotalPhraseLengthInSamples = 0;

        m_PrimaryStickPattern->setParams(m_BaseTempo, m_SampleRate);
        m_SecondaryStickPattern->setParams(m_BaseTempo, m_SampleRate);
        setTempoForRudiments(75.0);
        setNoteFlowForRudiments(RudimentManager::k_EighthTriplet);
        setPhraseLength(8, 4);
        setRudimentType(RudimentManager::k_FlamAccent);
       
	}
	~RudimentManager()
	{
		delete m_PrimaryStickPattern;
		delete m_SecondaryStickPattern;
	}


	//setter to set noteflow, which will passed onto the sequencer. 
	void setNoteFlowForRudiments(RudimentNoteFlow_t noteFlowType)
	{
		switch(noteFlowType)
    	{
    		case(k_Sixteenth):
    			m_CurrentNoteFlow = k_SixteenthNote;
    			break;
    		case(k_Eighth):
    			m_CurrentNoteFlow = k_EighthNote;
    			break;
    		case(k_Quarter):
    			m_CurrentNoteFlow = k_QuarterNote;
    			break;
    		case(k_EighthTriplet):
    			m_CurrentNoteFlow = k_EighthNoteTriplet;
    			break;
    		default:
    			break;
    	}
		m_PrimaryStickPattern->setNoteFlow(noteFlowType); // so that the underlying sequencer gets the update of noteflow
		m_SecondaryStickPattern->setNoteFlow(noteFlowType);
	}

	void setTempoForRudiments(float newTempo)
	{
		m_BaseTempo = newTempo;
		m_PrimaryStickPattern->setTempo(newTempo);
		m_SecondaryStickPattern->setTempo(newTempo); // the inter sample time is adjusted in such a way that it reflects the current noteflow. 
	}

	void setPhraseLength(int numberOfBeats, int beatValue) // 4 is quarter note, 8 is eighth note, 16 is sixteenth note...the bottom number in time signature.
	{
		float m_InterBeatTimeInSec = (60.0/m_BaseTempo)*(4.0/(float)beatValue);
      //  std::cout << "inter " << m_InterBeatTimeInSec <<std::endl;
		m_TotalPhraseLengthInSamples = (int)((m_InterBeatTimeInSec * numberOfBeats)/m_SampleRate);
      //  std::cout << "initial value of total " << m_TotalPhraseLengthInSamples << std::endl;

	}
	void setRudimentType(RudimentType rudimentType)
	{
		// when a rudiment is set the sequence for each stick should be modified : first thing, this specifies the stick ordering
		// tempo, note flow etc are changed independently. 
		e_RudimentType = rudimentType;
		switch(e_RudimentType)
		{
			case k_SingleStroke:
            {
                int singleStrokeR[2] = {1,0};
				std::vector<int>tempSeqR(singleStrokeR, singleStrokeR+sizeof(singleStrokeR)/sizeof(int));
                int singleStrokeL[2] = {0,1};
				std::vector<int>tempSeqL(singleStrokeL, singleStrokeL+sizeof(singleStrokeL)/sizeof(int));
                int singleStrokeOffset[2] = {0,0};
				std::vector<int>tempOffSetSeq(singleStrokeOffset, singleStrokeOffset+sizeof(singleStrokeOffset)/sizeof(int));
				m_PrimaryStickPattern->setSequence(tempSeqR);
				m_PrimaryStickPattern->setOffsetSequence(tempOffSetSeq);
				m_SecondaryStickPattern->setSequence(tempSeqL);
				m_SecondaryStickPattern->setOffsetSequence(tempOffSetSeq);
				break;
            }
			case k_DoubleStroke:
            {
                int doubleStrokeR[4] = {1,1,0,0};
				std::vector<int>tempSeqR(doubleStrokeR, doubleStrokeR+sizeof(doubleStrokeR)/sizeof(int));
                int doubleStrokeL[4] = {0,0,1,1};
				std::vector<int>tempSeqL(doubleStrokeL, doubleStrokeL+sizeof(doubleStrokeR)/sizeof(int));
                int doubleStrokeOffset[4] = {0,0,0,0};
				std::vector<int>tempOffSetSeq(doubleStrokeOffset, doubleStrokeOffset+sizeof(doubleStrokeOffset)/sizeof(int));
				m_PrimaryStickPattern->setSequence(tempSeqR);
				m_PrimaryStickPattern->setOffsetSequence(tempOffSetSeq);
				m_SecondaryStickPattern->setSequence(tempSeqL);
				m_SecondaryStickPattern->setOffsetSequence(tempOffSetSeq);
				break;
            }
			case k_SingleParadiddle:
            {
                int singleParadiddleR[8] = {1,0,1,1,0,1,0,0};
				std::vector<int>tempSeqR(singleParadiddleR, singleParadiddleR+sizeof(singleParadiddleR)/sizeof(int));
                int singleParadiddleL[8] = {0,1,0,0,1,0,1,1};
				std::vector<int>tempSeqL(singleParadiddleL, singleParadiddleL+sizeof(singleParadiddleL)/sizeof(int));
                int singleParadiddleOffset[8] = {0,0,0,0,0,0,0,0};
				std::vector<int>tempOffSetSeq(singleParadiddleOffset, singleParadiddleOffset+sizeof(singleParadiddleOffset)/sizeof(int));
				m_PrimaryStickPattern->setSequence(tempSeqR);
				m_PrimaryStickPattern->setOffsetSequence(tempOffSetSeq);
				m_SecondaryStickPattern->setSequence(tempSeqL);
				m_SecondaryStickPattern->setOffsetSequence(tempOffSetSeq);
				break;
            }
			case k_DoubleParadiddle:
            {
                int doubleParadiddleR[12] = {1,0,1,0,1,1,0,1,0,1,0,0};
				std::vector<int>tempSeqR(doubleParadiddleR, doubleParadiddleR+sizeof(doubleParadiddleR)/sizeof(int));
                int doubleParadiddleL[12] = {0,1,0,1,0,0, 1,0,1,0,1,1};
				std::vector<int>tempSeqL(doubleParadiddleL, doubleParadiddleL+sizeof(doubleParadiddleL)/sizeof(int));
                int doubleParadiddleOffset[12] = {0,0,0,0,0,0,0,0,0,0,0,0};
				std::vector<int>tempOffSetSeq(doubleParadiddleOffset, doubleParadiddleOffset+sizeof(doubleParadiddleOffset)/sizeof(int));
				m_PrimaryStickPattern->setSequence(tempSeqR);
				m_PrimaryStickPattern->setOffsetSequence(tempOffSetSeq);
				m_SecondaryStickPattern->setSequence(tempSeqL);
				m_SecondaryStickPattern->setOffsetSequence(tempOffSetSeq);
				break;
            }
			case k_TripleParadiddle:
            {
                int tripleParadiddleR[16] = {1,0,1,0,1,0,1,1,0,1,0,1,0,1,0,0};
				std::vector<int>tempSeqR(tripleParadiddleR, tripleParadiddleR+sizeof(tripleParadiddleR)/sizeof(int));
                int tripleParadiddleL[16] = {0,1,0,1,0,1,0,0,1,0,1,0,1,0,1,1};
				std::vector<int>tempSeqL(tripleParadiddleL, tripleParadiddleL+sizeof(tripleParadiddleL)/sizeof(int));
                int tripleParadiddleOffset[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
				std::vector<int>tempOffSetSeq(tripleParadiddleOffset, tripleParadiddleOffset+sizeof(tripleParadiddleOffset)/sizeof(int));
				m_PrimaryStickPattern->setSequence(tempSeqR);
				m_PrimaryStickPattern->setOffsetSequence(tempOffSetSeq);
				m_SecondaryStickPattern->setSequence(tempSeqL);
				m_SecondaryStickPattern->setOffsetSequence(tempOffSetSeq);
				break;
            }
			case k_Flam:
            {
                int flamR[2] = {1,1};
                std::vector<int>tempSeqR(flamR, flamR+sizeof(flamR)/sizeof(int));
                int flamL[2] = {1,1};
                std::vector<int>tempSeqL(flamL, flamL+sizeof(flamL)/sizeof(int));
                int flamROffset[2] = {0,-1};
                std::vector<int>tempOffsetSeqR(flamROffset, flamROffset+sizeof(flamROffset)/sizeof(int));
                int flamLOffset[2] = {-1,0};
                std::vector<int>tempOffsetSeqL(flamLOffset, flamLOffset+sizeof(flamLOffset)/sizeof(int));
                m_PrimaryStickPattern->setSequence(tempSeqR);
				m_PrimaryStickPattern->setOffsetSequence(tempOffsetSeqR);
				m_SecondaryStickPattern->setSequence(tempSeqL);
				m_SecondaryStickPattern->setOffsetSequence(tempOffsetSeqL);
				break;
            }
            case k_FlamTap:
            {   
                int flamTapR[4] = {1,1,1,0};
                std::vector<int>tempSeqR(flamTapR, flamTapR+sizeof(flamTapR)/sizeof(int));
                int flamTapL[4] = {1,0,1,1};
                std::vector<int>tempSeqL(flamTapL, flamTapL+sizeof(flamTapL)/sizeof(int));
                int flamTapROffset[4] = {0, 0, -1, 0};
                std::vector<int>tempOffsetSeqR(flamTapROffset, flamTapROffset+sizeof(flamTapROffset)/sizeof(int));
                int flamTapLOffset[4] = {-1, 0, 0, 0};
                std::vector<int>tempOffsetSeqL(flamTapLOffset, flamTapLOffset+sizeof(flamTapLOffset)/sizeof(int));
                m_PrimaryStickPattern->setSequence(tempSeqR);
				m_PrimaryStickPattern->setOffsetSequence(tempOffsetSeqR);
				m_SecondaryStickPattern->setSequence(tempSeqL);
				m_SecondaryStickPattern->setOffsetSequence(tempOffsetSeqL); 
				break;
            }
			case k_FlamAccent:
            {
                int flamAccentR[6] = {1,0,1,1,1,0};
                std::vector<int>tempSeqR(flamAccentR, flamAccentR+sizeof(flamAccentR)/sizeof(int));
                int flamAccentL[6] = {1,1,0,1,0,1};
                std::vector<int>tempSeqL(flamAccentL, flamAccentL+sizeof(flamAccentL)/sizeof(int));
                int flamAccentROffset[6] = {0,0,0,-1,0,0};
                std::vector<int>tempOffsetSeqR(flamAccentROffset, flamAccentROffset+sizeof(flamAccentROffset)/sizeof(int));
                int flamAccentLOffset[6] = {-1,0,0,0,0,0};
                std::vector<int>tempOffsetSeqL(flamAccentLOffset, flamAccentLOffset+sizeof(flamAccentLOffset)/sizeof(int));
                m_PrimaryStickPattern->setSequence(tempSeqR);
				m_PrimaryStickPattern->setOffsetSequence(tempOffsetSeqR);
				m_SecondaryStickPattern->setSequence(tempSeqL);
				m_SecondaryStickPattern->setOffsetSequence(tempOffsetSeqL); 
                break;
            }
            case k_SingleDrag:
            {
                int singleDragR[2] = {1,4};
                std::vector<int>tempSeqR(singleDragR, singleDragR+sizeof(singleDragR)/sizeof(int));
                int singleDragL[2] = {4,1};
                std::vector<int>tempSeqL(singleDragL, singleDragL+sizeof(singleDragL)/sizeof(int));
                int singleDragROffset[2] = {0,-1};
                std::vector<int>tempOffsetSeqR(singleDragROffset, singleDragROffset+sizeof(singleDragROffset)/sizeof(int));
                int singleDragLOffset[2] = {-1,0};
                std::vector<int>tempOffsetSeqL(singleDragLOffset, singleDragLOffset+sizeof(singleDragLOffset)/sizeof(int));
                m_PrimaryStickPattern->setSequence(tempSeqR);
				m_PrimaryStickPattern->setOffsetSequence(tempOffsetSeqR);
				m_SecondaryStickPattern->setSequence(tempSeqL);
				m_SecondaryStickPattern->setOffsetSequence(tempOffsetSeqL);
                break;
            }
            case k_DoubleDrag:
            {
                int doubleDragR[4] = {1,0,4,1};
                std::vector<int>tempSeqR(doubleDragR, doubleDragR+sizeof(doubleDragR)/sizeof(int));
                int doubleDragL[4] = {4,1,1,0};
                std::vector<int>tempSeqL(doubleDragL, doubleDragL+sizeof(doubleDragL)/sizeof(int));
                int doubleDragROffset[4] = {0,0,-1,0};
                std::vector<int>tempOffsetSeqR(doubleDragROffset, doubleDragROffset+sizeof(doubleDragROffset)/sizeof(int));
                int doubleDragLOffset[4] = {-1,0,0,0};
                std::vector<int>tempOffsetSeqL(doubleDragLOffset, doubleDragLOffset+sizeof(doubleDragLOffset)/sizeof(int));
                m_PrimaryStickPattern->setSequence(tempSeqR);
				m_PrimaryStickPattern->setOffsetSequence(tempOffsetSeqR);
				m_SecondaryStickPattern->setSequence(tempSeqL);
				m_SecondaryStickPattern->setOffsetSequence(tempOffsetSeqL);
                break;
            }
            case k_TripleDrag:
            {
                int tripleDragR[6] = {1,1,0,4,4,1};
                std::vector<int>tempSeqR(tripleDragR, tripleDragR+sizeof(tripleDragR)/sizeof(int));
                int tripleDragL[6] = {4,4,1,1,1,0};
                std::vector<int>tempSeqL(tripleDragL, tripleDragL+sizeof(tripleDragL)/sizeof(int));
                int tripleDragROffset[6] = {0,0,0,-1,-1,0};
                std::vector<int>tempOffsetSeqR(tripleDragROffset, tripleDragROffset+sizeof(tripleDragROffset)/sizeof(int));
                int tripleDragLOffset[6] = {-1,-1,0,0,0,0};
                std::vector<int>tempOffsetSeqL(tripleDragLOffset, tripleDragLOffset+sizeof(tripleDragLOffset)/sizeof(int));
                m_PrimaryStickPattern->setSequence(tempSeqR);
				m_PrimaryStickPattern->setOffsetSequence(tempOffsetSeqR);
				m_SecondaryStickPattern->setSequence(tempSeqL);
				m_SecondaryStickPattern->setOffsetSequence(tempOffsetSeqL);
            }
			default:
				break;

		}

	}

	int getPrimaryStrokeType()
	{
		m_PhraseSampleCounter++;
		if(m_PhraseSampleCounter >= m_TotalPhraseLengthInSamples)
		{
          //  std::cout << "m_PhraseSampleCounter = " << m_PhraseSampleCounter << std::endl;
			isRudimentBypassed = true;
			m_PhraseSampleCounter = m_TotalPhraseLengthInSamples;
		}

		if(!isRudimentBypassed)
		{
           // std::cout << " rudiment happening" <<std::endl;
			return m_PrimaryStickPattern->getSequenceValue();
		}
		else
		{   
           // std::cout << " rudiment not" <<std::endl;
			return 0; //stay in steady state
		}
	}

	int getSecondaryStrokeType()
	{	
		if(!isRudimentBypassed) // the phrase counter in primary stroke will take care of it. 
		{
			return m_SecondaryStickPattern->getSequenceValue();
		}
		else
		{
			return 0; // stay in steady state, if bypassed. 
		}
	}

	void triggerRudimentPhrase()
	{
		isRudimentBypassed = false;
		m_PhraseSampleCounter = 0;
        m_PrimaryStickPattern->resetIndices();
        m_SecondaryStickPattern->resetIndices();
	}
	
	
};


#endif