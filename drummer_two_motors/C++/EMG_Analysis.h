#ifndef EMG_ANALYSIS_H
#define EMG_ANALYSIS_H

#include <cmath>
#include <iostream>
#include <vector>

class  EMG_Analysis {
    float avgBuffer[30];
    bool settingThresholds;
    std::vector<float> primary; //kp,kd
    float highThresh;
    float lowThresh;
    
public:
    EMG_Analysis(){
        settingThresholds=false;
        highThresh = 124;
        lowThresh = 107;
    }
    
    void readEMG( std::vector<float>& regressedVals, float emgSignal){
        
        if(settingThresholds == true){
            std::cout<< "emg = " << emgSignal <<std::endl;
        }
        emgPrimaryRegress(regressedVals,emgSignal);
    }
    
    void printThresholds(){
        std::cout<< "emg high = " << highThresh <<std::endl;
        std::cout<< "emg low = " << lowThresh <<std::endl;
    }
    
    void setLow(float emgSignal){
        if(settingThresholds == true){
            lowThresh = emgSignal;
        }
    }
    void setHigh(float emgSignal){
        if(settingThresholds == true){
            highThresh = emgSignal;
        }
    }
             
    
    void emgPrimaryRegress(std::vector<float>& regressedVals, float emgVal){
  
        float tempKp;
        tempKp = (emgVal - lowThresh)/(highThresh-lowThresh) * (1.5 - .001) + .001; //kp
        
        if(tempKp > 1.5){
            tempKp = 1.5;
        }
        if(tempKp<.01){
            tempKp = .01;
        }
       // std::cout<< "emg high = " << emgVal <<std::endl;
        regressedVals[0] = tempKp;
        regressedVals[1] = 0.000001;//kd
        primary = regressedVals;
    }
    
    void setThresholds(){
        if(settingThresholds ==false){
            settingThresholds = true;
        }else{
            settingThresholds = false;
        }
    }
    
private:
    
    
};

#endif