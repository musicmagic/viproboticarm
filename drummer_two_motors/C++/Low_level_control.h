#ifndef LOW_LEVEL_CONTROL_H
#define LOW_LEVEL_CONTROL_H

#include <cmath>
#include <iostream>
#include <vector>
#include "Pot_Analysis.h"
#include "EMG_Analysis.h"
#include "Accel_Analysis.h"

//class for doing level signal analysis on sensor data and setting low level parameters (kp and kd)
// this can inherit other classes for doing specific things like emg analysis, accel analysis, etc


class  Low_level_control {
    float primaryKp,primaryKd,secondaryKp,secondaryKd, onsetAmplitude;
    Pot_Analysis potAnalysis;
    Accel_Analysis accelAnalysis;
    EMG_Analysis emgAnalysis;

public:
    Low_level_control(){
        primaryKp = .18;
        primaryKd = 0.01;
        secondaryKp = .18;
        secondaryKd = .001;
        
        onsetAmplitude = 0;
        
        potAnalysis = Pot_Analysis();
        accelAnalysis = Accel_Analysis();
        emgAnalysis = EMG_Analysis();
    }
    
    ~Low_level_control(){
        
    }
    //getters
    double getPrimaryKp(){
        return primaryKp;
    }
    double getPrimaryKd(){
        return primaryKd;
    }
    double getSecondaryKp(){
        return secondaryKp;
    }
    double getSecondaryKd(){
        return secondaryKd;
    }
      
    double getOnsetAmplitude()
    {
        return onsetAmplitude;
    }
    
    //setters based on different params
    void setPrimary_usingPot(float potValue){
        std::vector<float> kpkd (2);
        potAnalysis.potPrimaryRegress(kpkd, potValue);
        primaryKp = kpkd[0];
        primaryKd = kpkd[1];
    }
    void setPrimaryKp(float kpVal){
        // this is used for setting kp from udp. the udp messages are in the range 1-99. to be scaled properly. 
        //kpVal is in the range 1-99.
        float max = 0.5;
        float min = 0.11;
        primaryKp = (((float)kpVal - 1.0)/(99.0 - 1.0)) * (max - min) + min;
        std::cout << "PrimaryKp is " << primaryKp << std::endl;
//         if(primaryKp > 0.7)    
//         {
//             primaryKd = 0.005; //kd
//         }else
//         {
//             primaryKd = 0.0001;
//         }
    }
    void setPrimaryKd(float kdVal){
        float max = 0.01;
        float min = 0.00005;
        primaryKd = (((float)kdVal - 1.0)/(99.0 - 1.0)) * (max - min) + min;
        //primaryKd = kdVal;
        std::cout << "PrimaryKd is " << primaryKd << std::endl;
    }
    
    void incrementPrimaryKp()
    {
        primaryKp = primaryKp + 0.01;
        if(primaryKp >= 1.3)
            primaryKp = 1.3;
        
        
        if(primaryKp > 0.7)    
        {
            primaryKd = 0.005; //kd
        }else
        {
            primaryKd = 0.0001;
        }
    }
    
    void decrementPrimaryKp()
    {
        primaryKp = primaryKp - 0.01;
        if(primaryKp <= 0.00005)
            primaryKp = 0.00005;
        
        
        if(primaryKp > 0.7)    
        {
            primaryKd = 0.005; //kd
        }else
        {
            primaryKd = 0.0001;
        }
    }

    void setOnsetAmplitude(float onsetAmp)
    {
        onsetAmplitude = onsetAmp;
    }
    int setAccData(float accX, float accY, float accZ)
    {
        accelAnalysis.updateAcc(accX,accY,accZ);
        if(accelAnalysis.isNovelty())
            return 1;                   // 1 is the novelty signal
        if(accelAnalysis.isHovering())
            return 2;                   // 2 is the hovering signal
        else
            return 3;
        
    }
    
    void setAccBound()
    {
        accelAnalysis.setBound();
        //accelAnalysis.setRecordToggle();
        //cout<<"record toggle"<<endl;
    }
    void setAccToggle()
    {
        accelAnalysis.setWorkToggle();
    }
    
    
    
    /************************************************************************************/
    // EMG Stuff
    void readEMG(float potValue){
        std::vector<float> kpkd (2);
        emgAnalysis.readEMG(kpkd, potValue);
        primaryKp = kpkd[0];
        primaryKd = kpkd[1];
    }
    void setEMGThresholds(){
        emgAnalysis.setThresholds();
    }
    void setEmgHigh(float sig){
        emgAnalysis.setHigh(sig);
    }
    void setEmgLow(float sig){
        emgAnalysis.setLow(sig);
    }
    
    /********************************************************************************************/
    
    
private:
    
    
};

#endif