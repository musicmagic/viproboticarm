#ifndef MAIN_CONTROL_H
#define MAIN_CONTROL_H

#include <cmath>
#include <iostream>
#include "Low_level_control.h"
#include "Stroke_Generator.h"
#include "CompositionOne.h"
#include "MidiComposition.h"
#include "FeynmanComposition.h"
#include "SoloComposition.h"
#include "SoWhatComposition.h"
#include "EmgTestComposition.h"
#include "RudimentManager.h"



/********************************************************************************

0 - So What
1 - Rudiment Test
3 - FeynmanRhythms
4 - Solo piece
5 - EMgTestComposition
 * 
6 - to bypass all the compositions so that the strike message from udp works...
***************************************************************************/


class  Main_Control {
    Low_level_control lowLevelControl;
    Stroke_Generator* primaryStrokeGenerator;
    Stroke_Generator* secondaryStrokeGenerator;
    int currentPiece;
    int currentSection;
    int m_CurrentRudiment;
    
    double sampleRate;

    
    CompositionOne *m_CompOne;
    MidiComposition *m_MidiComp;
    FeynmanComposition *m_FeynComp;
    SoloComposition *m_SoloComp;
    SoWhatComposition *m_SoWhatComp;
    EmgTestComposition *m_EmgTestComposition;
    RudimentManager *m_RudimentManager;
    
    
public:
    Main_Control(){
        lowLevelControl = Low_level_control();
        sampleRate = .001;
        primaryStrokeGenerator = new Stroke_Generator(sampleRate, -7.2, 1.0, -1.0); // -2.3 from upper hard limit,
        secondaryStrokeGenerator = new Stroke_Generator(sampleRate,6.9, -1.0, 0.7); // 3.0 from upper hard limit
        secondaryStrokeGenerator->setStrokeType(3); // set the secondary stick to park position
        currentPiece = -1;
        currentSection = 0;
        m_CurrentRudiment = -1;
      
        m_CompOne = new CompositionOne();
        m_MidiComp = new MidiComposition("/home/meka/meka/rsc-drummer/drummer_two_motors/C++/FinaleUnison.txt", 130); // second argument (optional) is the tempo at which the midi file will be played
        m_FeynComp = new FeynmanComposition("/home/meka/meka/rsc-drummer/drummer_two_motors/C++/onset_trial2.txt", "/home/meka/meka/rsc-drummer/drummer_two_motors/C++/onset_trial1.txt");
        m_SoloComp = new SoloComposition();
        m_SoWhatComp = new SoWhatComposition();
        m_RudimentManager = new RudimentManager();
        m_EmgTestComposition = new EmgTestComposition();
    }
    
    
    ~Main_Control(){
        delete primaryStrokeGenerator;
        delete secondaryStrokeGenerator;
        delete m_CompOne;
        delete m_MidiComp;
        delete m_FeynComp;
        delete m_SoloComp;
        delete m_SoWhatComp;
        delete m_RudimentManager;
        delete m_EmgTestComposition;
    }
    
    
    
    //getters for kp and kd
    double getPrimaryKp(){
        return lowLevelControl.getPrimaryKp();
    }
    double getPrimaryKd(){
        return lowLevelControl.getPrimaryKd();
    }
    double getSecondaryKp(){
        return lowLevelControl.getSecondaryKp();
    }
    double getSecondaryKd(){
        return lowLevelControl.getSecondaryKd();
    }
    
    //getter for onsetAmplitude

    double getOnsetAmplitude()
    {
        return lowLevelControl.getOnsetAmplitude();
    }

    
    //setters and getters for piece and section
    int getCurrentPiece(){
        return currentPiece;
    }
    int getCurrentSection(){
        return currentSection;
    }
    void setCurrentPiece(int piece){
        if(piece != -1){
            
         currentPiece = piece;
         std::cout << "MyPiece number is " << currentPiece <<std::endl;
        }
    }
    void setCurrentSection(int section){
        if(section != -1){
            currentSection = section;
        }
    }
    /**********************************************************/
    //Accelerometer stuff
    // setters for accelerometer data
    void updateAccData(float accX, float accY, float accZ)
    {
        int i = lowLevelControl.setAccData(accX,accY,accZ);
        if(i == 1)
        {
            if(secondaryStrokeGenerator->getStrokeType()!=3)
                 secondaryStrokeGenerator -> parkStick();
            else
                 secondaryStrokeGenerator -> setStrokeType(1);
        }
        else if (i == 2)
        {
            //currentPiece = 0;
            //setNoteFlow(4);
        }
        else if (i == 3)
        {
            //currentPiece = 1;
            //setNoteFlow(3);
        }
    }
    
    void setAccBound()
    {
        lowLevelControl.setAccBound();
        /*if(secondaryStrokeGenerator->getStrokeType()!=3)
                 secondaryStrokeGenerator -> parkStick();
            else
                 secondaryStrokeGenerator -> setStrokeType(1);*/
    }
     
//     void setAccHigh(){
//         lowLevelControl.setAccHigh();
//     }
//     void setAccLow(){
//         lowLevelControl.setAccLow();
//     }
    void setAccWork()
     {
         lowLevelControl.setAccToggle();
     }
    
    
    /******************************Bypass stuff **************************/
    
    void togglePrimaryByPass()
    {
        //primaryStrokeGenerator->toggleByPass();
        switch(currentPiece)
        {
            case 0:
                break;
            case 1:
                break;              
            case 2:
                break;
            case 3:
                break;
            case 4:
                m_SoloComp->togglePrimaryBypass();
                break;
                
            default:
                break;
                
        }
    }
    
    void toggleSecondaryByPass()
    {
        switch(currentPiece)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                m_SoloComp->toggleSecondaryBypass();
                break;
                
            default:
                break;
        }
    }
    
    void setPrimaryByPass(bool bval)
    {
        primaryStrokeGenerator->setByPass(bval);
    }
    
    void setSecondaryByPass(bool bval)
    {
        secondaryStrokeGenerator->setByPass(bval);
    }
    
    /************************************************************************
     //getters for primary stick stroke mode. to be used in behavior controller to determine whether the manual kp kd or the autonomous kp kd should be used
     *
     
     */
        
    int getPrimaryStickStrokeMode()
    {
        return primaryStrokeGenerator->getStrokeType(); // 0 is steady state, 3 is park state. 1 and 2 are autonomous modes
    }
  
    /************************************************************************/
    //setters for onset stuff

    void setOnsetAmplitude(double amp)
    {
        lowLevelControl.setOnsetAmplitude(amp);
    } 
    
    //setters for kp and kd
    void setPrimaryKpKd_usingPot(float potValue){
        lowLevelControl.setPrimary_usingPot(potValue);
    }
    void setPrimaryKp_usingMessage(float val){
        lowLevelControl.setPrimaryKp(val);
    }
    void setPrimaryKd_usingMessage(float val){
        lowLevelControl.setPrimaryKd(val);
    }
    
    void incrementDecrementPrimaryKp(int flagNumber)
    {
        if(flagNumber < 50)
        {
            lowLevelControl.incrementPrimaryKp();
        }
        else
        {
            lowLevelControl.decrementPrimaryKp();
        }
    }
    /*************************************************************************************/
    
    void setExpressionPedalValue(float pedalVal)
    {
        
        switch(currentPiece)
        {
            case 0:
                m_SoWhatComp->setExpressionPedalValue(pedalVal);
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                m_SoloComp->setExpressionPedalValue(pedalVal);
                break;
            default:
                break;
        }
    }
    
    void setEmgEnvelopeValue(float emgVal)
    {
        switch(currentPiece)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                m_EmgTestComposition->setEmgEnvelopeValue(emgVal);
                break;
            default:
                break;
        }
    }
    
    /****************************************************************************/
    /*************************StickAmplitude setters***********************************/
    void setPrimaryStickAmplitude(float amplitude) // these functions will be called by either the compositions of from the behavior controller to set the amplitude of the stick stroke
    {
        //std::cout << "Primary stick amplitude is " << amplitude <<std::endl;
        primaryStrokeGenerator->setStrokeAmplitude(amplitude);
    }
   
    void setSecondaryStickAmplitude(float amplitude)
    {
        //std::cout << "Secondary stick amplitude is " << amplitude << std::endl;
        secondaryStrokeGenerator->setStrokeAmplitude(amplitude);
    }
    /********************************************EMG Stuff************************************/
    
    void setEMGThresholds(){
        lowLevelControl.setEMGThresholds();
    }
    void setEmgHigh(float sig){
        lowLevelControl.setEmgHigh(sig);
    }
    void setEmgLow(float sig){
        lowLevelControl.setEmgLow(sig);
    }
    void readEMG(float sig){
        lowLevelControl.readEMG(sig);
    }
    /*****************************************************************************************/
    
     //strike functions and arm position functions
    double updatePrimaryPos()
    {    
        switch(currentPiece)
        {
            case 0:
                primaryStrokeGenerator->setStrokeType(m_SoWhatComp->getPrimaryStrokeType());
                break;
            case 1:
                //std::cout << "R" <<std::endl;
               // primaryStrokeGenerator->setStrokeType(m_CompOne->getPrimaryStrokeType());
               // std::cout<<m_RudimentManager->getPrimaryStrokeType() << std::endl;
                primaryStrokeGenerator->setStrokeType(m_RudimentManager->getPrimaryStrokeType());
               // primaryStrokeGenerator->setStrokeType(0);
                break;
            case 2:
                //primaryStrokeGenerator->setStrokeType(m_MidiComp->getPrimaryStrokeType()); // because for this only the second stick is being used
                break;
            case 3:
                m_FeynComp->updatePrimarySampleCounter();
                if(m_FeynComp->getIsPrimaryStrikeOn())
                {
                    primaryArmStrike();
                }
                break;
            case 4:
                primaryStrokeGenerator->setStrokeType(m_SoloComp->getPrimaryStrokeType());
                primaryStrokeGenerator->setStrokeAmplitude(m_SoloComp->getPrimaryStrokeAmp());
                primaryStrokeGenerator->setByPass(m_SoloComp->getCurrentPrimaryBypass());
                break;
            case 5: // use 5 for bypassing all the compositions so that the strike message over udp will not be overwritten
                primaryStrokeGenerator->setStrokeType(m_EmgTestComposition->getPrimaryStrokeType());
                break;
             
            default:
                break;

        }
        //std::cout << "P arm out " <<std::endl;
        return primaryStrokeGenerator->updatePos();
    }
    double updateSecondaryPos(){
       
        switch(currentPiece)
        {
            case 0:
                 secondaryStrokeGenerator->setStrokeType(m_SoWhatComp->getSecondaryStrokeType());
                 secondaryStrokeGenerator->setStrokeAmplitude(m_SoWhatComp->getSecondaryStrokeAmp());
                 break;
            case 1:
                secondaryStrokeGenerator->setStrokeType(m_RudimentManager->getSecondaryStrokeType());
                // secondaryStrokeGenerator->setStrokeType(m_CompOne->getSecondaryStrokeType());
                // secondaryStrokeGenerator->setByPass(m_CompOne->getCurrentByPass());
                 break;
            case 2: // for midi sequenced or onset detection based composition this is the paradigm that works the best. 
//                  m_MidiComp->updateSecondarySampleCounter();
//                  if(m_MidiComp->getIsSecondaryStrikeOn())
//                  {
//                     secondaryArmStrike();
//                  }
                 break;
            case 3:
                secondaryStrokeGenerator->setStrokeAmplitude(1.0);
                m_FeynComp->updateSecondarySampleCounter();
                if(m_FeynComp->getIsSecondaryStrikeOn())
                {
                    secondaryArmStrike();
                }
                break;
            case 4:
                secondaryStrokeGenerator->setStrokeType(m_SoloComp->getSecondaryStrokeType());
                secondaryStrokeGenerator->setStrokeAmplitude(m_SoloComp->getSecondaryStrokeAmp());
                secondaryStrokeGenerator->setByPass(m_SoloComp->getCurrentSecondaryBypass());
                break;
            case 5:
                secondaryStrokeGenerator->setStrokeType(m_EmgTestComposition->getSecondaryStrokeType());
                break;
            default:
                break;

        }
        return secondaryStrokeGenerator->updatePos();
    }
    /********************************************Single strikes from UDP*********************************/
     void primaryArmStrike()
     {
        primaryStrokeGenerator->singleStrike();
     }
    void secondaryArmStrike()
    {
        secondaryStrokeGenerator->singleStrike();
    }
    
    void triggerRudiment()
    {
        m_CurrentRudiment++;
        switch(m_CurrentRudiment)
        {
            case RudimentManager::k_SingleStroke:
                m_RudimentManager->setTempoForRudiments(160.0);
                m_RudimentManager->setNoteFlowForRudiments(RudimentManager::k_Sixteenth);
                m_RudimentManager->setPhraseLength(8, 4); // 2 measures of 4/4
                m_RudimentManager->setRudimentType(RudimentManager::k_SingleStroke);
                break;
            case RudimentManager::k_DoubleStroke:
                m_RudimentManager->setTempoForRudiments(130.0);
                m_RudimentManager->setNoteFlowForRudiments(RudimentManager::k_Sixteenth);
                m_RudimentManager->setPhraseLength(8, 4); // 2 measures of 4/4
                m_RudimentManager->setRudimentType(RudimentManager::k_DoubleStroke);
                break;
            case RudimentManager::k_SingleParadiddle:
                m_RudimentManager->setTempoForRudiments(100.0);
                m_RudimentManager->setNoteFlowForRudiments(RudimentManager::k_Sixteenth);
                m_RudimentManager->setPhraseLength(8, 4); // 2 measures of 4/4
                m_RudimentManager->setRudimentType(RudimentManager::k_SingleParadiddle);
                break;
            case RudimentManager::k_DoubleParadiddle:
                m_RudimentManager->setTempoForRudiments(120.0);
                m_RudimentManager->setNoteFlowForRudiments(RudimentManager::k_Sixteenth);
                m_RudimentManager->setPhraseLength(12, 4); // 2 measures of 4/4
                m_RudimentManager->setRudimentType(RudimentManager::k_DoubleParadiddle);
                break;
            case RudimentManager::k_TripleParadiddle:
                m_RudimentManager->setTempoForRudiments(160.0);
                m_RudimentManager->setNoteFlowForRudiments(RudimentManager::k_Sixteenth);
                m_RudimentManager->setPhraseLength(12, 4); // 3 measures of 4/4
                m_RudimentManager->setRudimentType(RudimentManager::k_TripleParadiddle);
                break;
            case RudimentManager::k_Flam:
                m_RudimentManager->setTempoForRudiments(90.0);
                m_RudimentManager->setNoteFlowForRudiments(RudimentManager::k_Quarter);
                m_RudimentManager->setPhraseLength(12, 4); // 3 measures of 4/4
                m_RudimentManager->setRudimentType(RudimentManager::k_Flam);
                break;
            case RudimentManager::k_FlamTap:
                m_RudimentManager->setTempoForRudiments(60.0);
                m_RudimentManager->setNoteFlowForRudiments(RudimentManager::k_Eighth);
                m_RudimentManager->setPhraseLength(8, 4); // 2 measures of 4/4
                m_RudimentManager->setRudimentType(RudimentManager::k_FlamTap);
                break;
            case RudimentManager::k_FlamAccent:
                m_RudimentManager->setTempoForRudiments(80);
                m_RudimentManager->setNoteFlowForRudiments(RudimentManager::k_EighthTriplet);
                m_RudimentManager->setPhraseLength(8, 4); // 2 measures of 4/4
                m_RudimentManager->setRudimentType(RudimentManager::k_FlamAccent);
                break;
            case RudimentManager::k_SingleDrag:
                m_RudimentManager->setTempoForRudiments(90);
                m_RudimentManager->setNoteFlowForRudiments(RudimentManager::k_Quarter);
                m_RudimentManager->setPhraseLength(8, 4);
                m_RudimentManager->setRudimentType(RudimentManager::k_SingleDrag);
                break;
            case RudimentManager::k_DoubleDrag:
                m_RudimentManager->setTempoForRudiments(95);
                m_RudimentManager->setNoteFlowForRudiments(RudimentManager::k_Eighth);
                m_RudimentManager->setPhraseLength(8, 4);
                m_RudimentManager->setRudimentType(RudimentManager::k_DoubleDrag);
                break;
            case RudimentManager::k_TripleDrag:
                 m_RudimentManager->setTempoForRudiments(80);
                m_RudimentManager->setNoteFlowForRudiments(RudimentManager::k_EighthTriplet);
                m_RudimentManager->setPhraseLength(8, 4);
                m_RudimentManager->setRudimentType(RudimentManager::k_TripleDrag);
            default:
                break;
        }
        m_RudimentManager->triggerRudimentPhrase();
    }
    
    void updateSection(int updateVal)
    {
        std::cout << " Called updateSection from the pedal board ..... " <<std::endl;
        switch(currentPiece)
        {
            case 0:
                break;
            case 1:
               // m_CompOne->updateSection();
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                m_SoloComp->updateSection(updateVal);
                break;
            case 5:
                m_EmgTestComposition->updateSection(updateVal);
                break;
                
            default:
                break;
        }
    }
    
    
    
private:
    
    
};

#endif