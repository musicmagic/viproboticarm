#ifndef LOW_LEVEL_CONTROL_H
#define LOW_LEVEL_CONTROL_H

#include <cmath>
#include <iostream>
#include <vector>
#include "Pot_Analysis.h"
//#include "Emg_Analsis.h"
#include "Accel_Analysis.h"

//class for doing level signal analysis on sensor data and setting low level parameters (kp and kd)
// this can inherit other classes for doing specific things like emg analysis, accel analysis, etc


class  Low_level_control {
    float primaryKp,primaryKd,secondaryKp,secondaryKd;
    Pot_Analysis potAnalysis;
    Accel_Analysis accelAnalysis;

public:
    Low_level_control(){
        primaryKp = .18;
        primaryKd = 0.0;
        secondaryKp = .25;
        secondaryKd = .01;
        
        potAnalysis = Pot_Analysis();
        accelAnalysis = Accel_Analysis();
    }
    
    ~Low_level_control(){
       
    }
    //getters
    double getPrimaryKp(){
        return primaryKp;
    }
    double getPrimaryKd(){
        return primaryKd;
    }
    double getSecondaryKp(){
        return secondaryKp;
    }
    double getSecondaryKd(){
        return secondaryKd;
    }
      
    
    //setters based on different params
    void setPrimary_usingPot(float potValue){
        std::vector<float> kpkd (2);
        potAnalysis.potPrimaryRegress(kpkd, potValue);
        primaryKp = kpkd[0];
        primaryKd = kpkd[1];
    }
    
    void setPrimaryKp(float kpVal){
        primaryKp = kpVal;
    }
    void setPrimaryKd(float kdVal){
        primaryKd = kdVal;
    }
    
    int setAccData(float accX, float accY, float accZ)
    {
        accelAnalysis.updateAcc(accX,accY,accZ);
        if(accelAnalysis.isNovelty())
            return 1;                   // 1 is the novelty signal
        if(accelAnalysis.isHovering())
            return 2;                   // 2 is the hovering signal
        else return 0;
        //std::cout<<"fcc"<<std::endl;
    }
    void setAccToggle()
    {
        accelAnalysis.setWorkToggle();
    }
    
    
private:
    
    
};

#endif