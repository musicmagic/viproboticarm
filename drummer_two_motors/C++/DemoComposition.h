#ifndef DEMOCOMPOSITION_H
#define DEMOCOMPOSITION_H

#include <iostream>


#include "Sequencer.h"
#include "TempoManager.h"

#define START_TEMPO 180

class DemoComposition
{
private:
    Sequencer *m_SecondarySeq;
    
    float m_SampleRate;
    TempoManager *m_TempoManager;
    
    int m_NumQuarterSecondary;
    int m_CurrentSection;
    
    bool m_ByPass;
   
    
public:
    DemoComposition()
    {
        m_SecondarySeq = new Sequencer();
        m_TempoManager = new TempoManager();
        m_SampleRate = 0.001;
        m_CurrentSection = -1;
        m_NumQuarterSecondary = 4;

        
        m_SecondarySeq->setParams(START_TEMPO, m_SampleRate);
        m_SecondarySeq->generateCellBasedSequence(m_NumQuarterSecondary, 1); //all 1's pattern
        updateSection();
    }
    ~DemoComposition()
    {
        delete m_SecondarySeq;
        delete m_TempoManager;
    }
    
    
    
    int getPrimaryStrokeType()
    {
        return 0;
    }
    
    int getSecondaryStrokeType()
    {
        int secondaryPosition;
        switch(m_CurrentSection)
        {
            case 0:
                secondaryPosition = 3;
                break;
            case 1:
                secondaryPosition = m_SecondarySeq->getSequenceValue();
                break;
            case 2:
                secondaryPosition = 3;
                break;
            default:
                return 0;
                break;
        }
        
        return secondaryPosition;
    }
    
    void toggleByPass()
    {
        m_ByPass = !m_ByPass;
    }
          
    bool getCurrentByPass()
    {
        return m_ByPass;
    }
    
    void setNoteFlowForSequencer(int noteflow)
    {
        switch(noteflow)
        {
            case 4:
                std::cout << "Changed note flow to triplet " << std::endl;
                m_SecondarySeq->setNoteFlow(Sequencer::k_EighthTriplet);
                break;
            case 5:
                std::cout << "Changed note flow to sixteenth " << std::endl;
                m_SecondarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                break;
            default:
                break;         
        }
    }
    
    void updateSection()
    {
        m_CurrentSection++;
          std::cout << "Current section is " << m_CurrentSection <<std::endl;
        switch(m_CurrentSection)
        {
            case 0:
              
                m_ByPass = false;
                break;
            case 1:
                m_ByPass = true;
                break;
            case 2:
                m_ByPass = false;
                break;
            default:
                break;
                
        }
 
    }
    
    
};


#endif