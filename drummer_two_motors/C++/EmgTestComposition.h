/******Authored by Deepak Gopinath********************/

#ifndef EMGTESTCOMPOSITION_H
#define EMGTESTCOMPOSITION_H

// The EMG envelope would be mapped to the tempo of the sequence to start off with. 


#include <iostream>
#include <math.h>

#include "Sequencer.h"

#define MAX_TEMPO_DEVIATION 160

class EmgTestComposition
{
    private:
    // no secondary arm in this test composition. 
    Sequencer *m_PrimarySeq;
    
    float m_SampleRate;
    float m_PrimaryTempo;
    int m_CurrentPrimaryPosition;
    float m_PrimaryAmp;
    int m_NumQuarterPrimary;
    int m_CurrentSection;
    
    float PRI_BASE_TEMPO;
    
    float m_EmgEnvelopeValue; // this variable will be updated whenever a new value arrives from the signal. its upto the composition to determine what this value should be mapped to. 
    
public:
    
    EmgTestComposition()
    {
        m_PrimarySeq = new Sequencer();
        
        PRI_BASE_TEMPO = 75;
        m_SampleRate = 0.001;
        m_PrimaryTempo = PRI_BASE_TEMPO;
        m_NumQuarterPrimary = 8; // Number of quarter notes in the sequence
        m_CurrentPrimaryPosition = 3;
        m_CurrentSection = -1;
        
        m_PrimarySeq->setParams(m_PrimaryTempo, m_SampleRate);
        m_PrimarySeq->generateCellBasedSequence(m_NumQuarterPrimary, 1);
    }
    
    ~EmgTestComposition()
    {
       delete m_PrimarySeq;
    }
    
    
    void updateSection(int updateVal)
    {
        m_CurrentSection = m_CurrentSection +  updateVal;
        switch(m_CurrentSection)
        {
            case 0:
                break;
            case 1:
               
                break;
            default:
                break;
        }
    }
    
    void setEmgEnvelopeValue(float emgEnvVal) // normalized between 0.0 and 1.0
    {
        m_EmgEnvelopeValue = emgEnvVal;
        switch(m_CurrentSection)
        {
            case 0:
                break;
            case 1:
                m_PrimaryTempo = PRI_BASE_TEMPO + m_EmgEnvelopeValue*MAX_TEMPO_DEVIATION;
                m_PrimarySeq->setTempo(m_PrimaryTempo);
                break;
            default:
                break;
                
        }
    }
    
    int getPrimaryStrokeType()
    {
        switch(m_CurrentSection)
        {
            case 0:
                m_CurrentPrimaryPosition = 0;
                break;
            case 1:
                m_CurrentPrimaryPosition = m_PrimarySeq->getSequenceValue();
                break;
            default:
                break;
        }
        return m_CurrentPrimaryPosition;
    }
    int getSecondaryStrokeType()
    {
        switch(m_CurrentSection)
        {
            case 0:
               // return 3;
                break;
            case 1:
              //  return 3:
                break;
            default:
                break;
        }
        return 3;
    }

};


#endif
