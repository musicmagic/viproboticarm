 /*
  * A class for generating a periodic signal
  * Can be useful for fast roll type functions
  *
  * Author:     
  * Mason Bretan (masonbretan@gmail.com)
  *
  *
  */

#ifndef PERIODIC_H
#define PERIODIC_H
#define PI 3.14159265

#include <math.h>

class  Periodic {
    int mode; //sine, square
    double pos;
    double interval;
    double sampleRate;
    double amplitude;
    double frequency;
    double phase;
    double rampRate; //frequency change per sample
    double rampTargetPos;
    double targetFreq;
public:
    Periodic() {
        sampleRate = .001;
        mode = 0;
        pos = 0.0;
        amplitude = 1;
        phase = 0;
        rampRate = 1;
    }
    Periodic(double sr) {
        sampleRate = sr;
        mode = 0;
        pos = 0.0;
        amplitude = 1;
        phase = 0;
        std::cout <<interval<<"\n";
        rampRate = 1;
    }
    double output(void) {
        pos+=sampleRate;
        double out;
        switch(mode){
            case 0:
                out = getSine(pos);
                break;
            case 1:
                out = getSquare(pos);
                break;
            default:
                out = getSine(pos);
                break;
        }
        //std::cout <<frequency<<"\n";
        return out;
    }
    double outputRamp(void){
        //call initRamp before use
        if(pos <= rampTargetPos){
            setFrequency(frequency + rampRate/2);
        }else{
            setFrequency(targetFreq);
        }
        //setFrequency(startFreq+(frequency + rampRate)/2);
        //delta = frequency
        //setFrequency(startFreq +(freqDifference)*d
        return output();
    }
    void initRamp(double start, double stop,double rampDuration){ //rampDuration in seconds
        rampRate = (stop - start)/(rampDuration/sampleRate);
        setFrequency(start);
        rampTargetPos = pos+sampleRate*(rampDuration/sampleRate);
        targetFreq = stop;
    }
    void setFrequency(double freq){
        //change in distance per period in seconds
        if(freq > 13){
            freq = 13;
        }
        frequency = freq;
        
    }
    void setSquareMode(){
        mode = 1;
    }
    void setSineMode()
    {
        mode =0;
    }
    void setRampMode(){
        mode = 2;
    }
    
    double singleHit(double freq) {
        setFrequency(freq);
        int totalcount = 1.0/sampleRate;
        for (int i=0; i<totalcount; i++)
            return output();
    }
    
private :
    double getSine(double t){
        return -(0.6 + (fmin(amplitude,(amplitude/frequency)*4)*cos(2*PI*frequency*t+phase))); //decreasing amplitude for higer frequencies and center of oscillation is closer to the surface
    }
    double getSquare(double t){
        if(getSine(t) >0){
            return 1;
        }else{
            return -1;
        }
    }
};

#endif