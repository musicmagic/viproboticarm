/* A step sequencer class with multiple modes 
  * Author:     
  * Mason Bretan (masonbretan@gmail.com)

  * Annie Zhang
  * Deepak Gopinath
  * Minwei Gu
  * Iman Mukherjee
 */


#include <math.h>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include "RhythmGenerator.h"
#include "CellBasedRhythmGen.h"

#define EXPCURVE(X, Y) (exp(X*log(Y)) - (float)1.0)/(Y - (float)1.0)

class Sequencer
{
	
	int nSamplesPerInterval; // This is what indirectly determines the rate at which the notes are played. 
	std::vector<int> sequence; // The sequence containing 1's 0's and 2's played by the arm. This can be generated using RhythmGenerator, or from a simple rand() process. Can be of variable length
	int sampleCount; // This is used to keep track of how many samples have been passed and this info is used to play the next element in the sequence
	int sequenceCount; // The array index for the sequence array.
	int randomMode; // This flag is used for inplace shuffling of the existing sequence and also possible replacement of the sequence using a simple rand generator. 
	int rhythmGenMode ; // This flag is used for determining whether the Rhythm Generator is used for setting the starting sequence or a hard coded sequence
	double baseTempo;
    float sr;
	float sampleRate;
    float baseIdealIntervalTime;
	RhythmGenerator rg;
	CellBasedRhythmGen cellGen;
	//AccNovelty try
    const float k_SixteenthNote = 0.25;
    const float k_EighthNote = 0.5;
    const float k_QuarterNote = 1;
    const float k_EighthNoteTriplet = (float)1.0/3.0;

public:

	bool tempoChange  = true; // Set this flag to be true or false depending on whether the tempo needs to be changed. Annie's implementation changes the tempo in both direction gradually by 10%

	//Constructor
	Sequencer()
	{
		std::srand(unsigned (std::time(0)));
		sequenceCount = -1; //initialize array index to -1 so that the first time it increases it reads the first element.
		sampleCount = 0; //counter is set to 0;
		randomMode = 0; // No random mode
		rhythmGenMode = 0;
		sr = 0; // The param will be set by the Stroke Generator whicb in turn will be set by mdlStart in behavior_controller.cpp
		rg = RhythmGenerator();
		baseTempo = 60;
		// as soon as the sequencer is build we need to create some kind of starting sequence. If we want to use 
		if(rhythmGenMode)
        {
        	createImproviseSequence();
        }
        else
        {
        	createHardCodedSequence();
        }

		 
	}	
	Sequencer(int rMode, int rGenMode){
        std::srand(unsigned (std :: time(0)));
        sequenceCount= -1;
        sampleCount = 0;
        rhythmGenMode = rGenMode;
        randomMode = rMode;
        sr = 0;
        rg = RhythmGenerator();
        baseTempo = 60;
        if(rhythmGenMode)
        {
        	createImproviseSequence();
        }
        else
        {
        	createHardCodedSequence();
        }
    }

    ~Sequencer()
    {
        
    }
    int getStrike()
    {
    	updateSample();
    	if(sampleCount == 1) // get a new value from the sequence array in intervals of nSamplesPerInterval
    	{
    		if(sequenceCount >= sequence.size())
    		{
    			if(randomMode) // if the random mode is on, permute the sequence right at the very end of one cycle of reading through the array.
    			{
    				if(rand()%1000/1000.0 > 0.55)
    				{
    					std::random_shuffle(sequence.begin(), sequence.end()); // In place shuffling of the sequencer
    				}else
    				{
    					//  instead of in place shuffle we can may be change the note flow to something else and then generate a new sequence that would make sense in that note flow based on cells.
    					generateRandomSequence(rand()%4, 4*(rand()%4));
    					
    				}
    				// dont change the tempo ALWAYS. instead 65% of the time, when the end of the seuqnce is reached change the tempo
                   if((rand()%1000/1000.0) > 0.55)
                   {
                       float idealIntervalTime = (rand()%110 + 300)/1000.0;
                       nSamplesPerInterval = (int)(idealIntervalTime / sr);
                      // std::cout << "New intersequence time in samples " << nSamplesPerInterval <<std::endl;
                   }
    			}
    			sequenceCount = 0; ///reset the arrayIndex to zero
    		}
    		if (tempoChange)
    		{
                if (nSamplesPerInterval*sr >= 0.8)
                    incTempo();
                else
                    decTempo();
            }
            return sequence[sequenceCount];
    	}
    	else
    	{
    		return -1; // if it is in between the sequence read values, send -1
    	}
    }

    void setParams( double bTempo, double sampleRate){
        //provide ideal interval time in seconds
        sr = sampleRate; // a local copy of sample rate;
        baseTempo = bTempo;
        baseIdealIntervalTime = ((float)60.0/baseTempo);
        double idealIntervalTime = baseIdealIntervalTime*k_SixteenthNote; // so that the default interpretation is that of 16th notes
        nSamplesPerInterval = (int)(idealIntervalTime / sampleRate);

    }
    void incTempo(){
        nSamplesPerInterval = (int)(nSamplesPerInterval*0.9);
    }
    
    void decTempo(){
        nSamplesPerInterval = (int)(nSamplesPerInterval*1.1);
    }
    

    void setSequence(std::vector<int> seq)
    {
    	sequence = seq; // copy the argument to the sequencer array.
    }
    void createImproviseSequence() // same as improviseMode() as before. 
    {
    	std::vector<char> rhythm;
        std::vector<int> seq;
        std::vector<int> newseq;
        std::vector<int> tmp;
        std::cout<< "original rhythm ... " <<std::endl;
        rg.getOrigin(rhythm, seq, 1);                           // get rhythm by index, 0 - all
        std::cout<< "generated new rhythm ... "<< std::endl;
        for (int l=0; l<5; l++){
            tmp.clear();
            rg.generateRhythm(rhythm, tmp, 0.5);
            for (int k=0; k<2; k++)
                newseq.insert(newseq.end(), tmp.begin(), tmp.end());
        }
        
        std::cout<<"new sequence ... "<<std::endl;
        for (int i=0; i<newseq.size(); i++)
            std::cout<< newseq[i]<<" ";
        std::cout<<std::endl;
        
        for (int i=0; i<10; i++)                    // a period of silence
             seq.push_back(0);
        for (int i=0; i<newseq.size(); i++)
            seq.push_back(newseq[i]);
        for (int i=0; i<10; i++)                    // a period of silence
             seq.push_back(0);
        setSequence(seq); // copy the generated rhythm to sequencer array
    }

    void createHardCodedSequence()
    {
    	int temp[24] = {1,0,1,1,2,2,1,1,0,0,1,0,1,0,2,1,1,1,0,0,0,1,1,2}; // For the time being let this be the starting point for a hard coded sequence
    	std::vector<int> seq (temp, temp+sizeof(temp)/sizeof(int));
    	setSequence(seq);
    }

    void generateRandomSequence(int noteFlowMode, int numQuarterNotes)
    {
    	double idealIntervalTime;
        int numElements;
    	switch(noteFlowMode)
    	{
    		case 0:  // 16th notes
    			numElements = 16;
    			idealIntervalTime = baseIdealIntervalTime*k_SixteenthNote;
    		break;
    		case 1:  // eighth note
    			numElements = 8;
    			idealIntervalTime = baseIdealIntervalTime*k_EighthNote;
    		break;
    		case 2: // quarter
    			numElements = 4;
    			idealIntervalTime = baseIdealIntervalTime*k_QuarterNote;
    		break;
    		case 3: // eighth note triplet
    			numElements = 12;
    			idealIntervalTime = baseIdealIntervalTime*k_EighthNoteTriplet;
    		break;
    
    	}
    	sequence.clear();
 		cellGen.generateCellBasedRhythm(sequence, noteFlowMode, numQuarterNotes); // This should create
        
        // for(int i=0; i<numElements; i++) // for the time being 
        // {
        // 	double randVal = rand()%1000/1000.0;
        // 	if(randVal < 0.2)
        //     	sequence.push_back(0); // uniformly distributed sequence containing 0,1,2. may be somekind of markov models might give more realistics rhythms??!!!
        // 	else if(randVal < 0.7)
        // 		sequence.push_back(1);
        // 	else if(randVal <= 1.0)
        // 		sequence.push_back(2);
        // }
    }

private:
    void updateSample()
    {
    	sampleCount++;
    	if(sampleCount >= nSamplesPerInterval)
    	{
    		sampleCount = 1;
    	}
    }
};