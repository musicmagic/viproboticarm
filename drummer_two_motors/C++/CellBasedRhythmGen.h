/* Class for generating rhythms based on rhythmic cells (last a quarter note) for different kinds of subdivisions. 
Jazz, afro cuban 6/8 etc depends on 8th note triplet based rhythms and almost all pattern can be made by combining elementary rhythmc cells together
Similar for rock, funk, samba etc., the basic note flow is 16th note based. 

Authored by Deepak Gopinath - deepakgopinathmusic@gmail.com
 *
 TODO - Add smarter transition probabilities (markov model kind of stuff) for moving from one cell to another. Control accents somehow,
*/

#ifndef CELLBASEDRHYTHMGEN_H
#define CELLBASEDRHYTHMGEN_H

#include <vector>
#include <algorithm>
#include <math.h>
#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

class CellBasedRhythmGen
{
public:
    
	CellBasedRhythmGen()
	{
        std::srand(unsigned (std::time(0)));
	    initializeCells();
	}

	~CellBasedRhythmGen()
	{

	}

	void initializeCells()
	{
        ifstream mySixteenthCell;
        mySixteenthCell.open("/home/meka/meka/rsc-drummer/drummer_two_motors/C++/sixteenthCells.txt");
        string str;
        if (mySixteenthCell.is_open())
        {
            while (mySixteenthCell.good())
            {
                getline(mySixteenthCell, str);
                vector<int> tmp;
                if (str.size()>0)
                {
                    for (int i=0; i<str.size(); i++)
                    {
                        if (str[i] != ' ')
                        {
                            tmp.push_back(atoi(&str[i]));
                        }
                    }
                    sixteenthCells.push_back(tmp);
                }
            }
        }
        mySixteenthCell.close();
        str.clear();
        
        ifstream myEighthTripletCell;
        myEighthTripletCell.open("/home/meka/meka/rsc-drummer/drummer_two_motors/C++/eighthTripletCells.txt");
        
        if(myEighthTripletCell.is_open())
        {
            while(myEighthTripletCell.good())
            {
                getline(myEighthTripletCell, str);
                vector <int> tmp;
                if(str.size() > 0)
                {
                    for (int i=0; i<str.size(); i++)
                    {
                        if(str[i] != ' ')
                        {
                            tmp.push_back(atoi(&str[i]));
                        }
                    }
                    eighthTripletCells.push_back(tmp);
                }
            }
        }
        myEighthTripletCell.close();
        str.clear();
        
        ifstream myEighthCell;
        myEighthCell.open("/home/meka/meka/rsc-drummer/drummer_two_motors/C++/eighthCells.txt");
        
        if(myEighthCell.is_open())
        {
            while(myEighthCell.good())
            {
                getline(myEighthCell, str);
                vector <int> tmp;
                if(str.size() > 0)
                {
                    for (int i=0; i<str.size(); i++)
                    {
                        if(str[i] != ' ')
                        {
                            tmp.push_back(atoi(&str[i]));
                        }
                    }
                    eighthCells.push_back(tmp);
                }
            }
        }
        myEighthCell.close();
        str.clear();
        
        ifstream myQuarterCell;
        myQuarterCell.open("/home/meka/meka/rsc-drummer/drummer_two_motors/C++/quarterCells.txt");
        if(myQuarterCell.is_open())
        {
            while(myQuarterCell.good())
            {
                getline(myQuarterCell, str);
                vector <int> tmp;
                if(str.size() > 0)
                {
                    for(int i=0; i<str.size(); i++)
                    {
                        if(str[i] != ' ')
                        {
                            tmp.push_back(atoi(&str[i]));
                        }
                        quarterCells.push_back(tmp);
                    }
                }
            }
        }
        myQuarterCell.close();
        str.clear();
    }
    
    void printCells()
    {
        for(int i=0; i<sixteenthCells.size(); i++)
        {
            for(int j=0; j<sixteenthCells[i].size(); j++)
            {
                    cout<<sixteenthCells[i][j] <<" ";
            }
            cout << endl;
        }
        cout <<" ********************* " <<endl;
        for(int i=0; i<eighthTripletCells.size(); i++)
        {
            for(int j=0; j<eighthTripletCells[i].size(); j++)
            {
                cout<<eighthTripletCells[i][j] <<" ";
            }
            cout << endl;
        }
        
        cout <<" ********************* " <<endl;
        for(int i=0; i<eighthCells.size(); i++)
        {
            for(int j=0; j<eighthCells[i].size(); j++)
            {
                cout<<eighthCells[i][j] <<" ";
            }
            cout << endl;
        }
        cout <<" ********************* " <<endl;
        for(int i=0; i<quarterCells.size(); i++)
        {
            for(int j=0; j<quarterCells[i].size(); j++)
            {
                cout<<quarterCells[i][j] <<" ";
            }
            cout << endl;
        }
         cout <<" ********************* " <<endl;
    }

	void generateCellBasedRhythm(std::vector<int>&rhythmSeq, int flowMode, int numQuarterNotes, int isAllOrNothingAtAll)
	{
        int index;
        if(isAllOrNothingAtAll == 0)
        {
            index = 0;
        }
        
        switch (flowMode) {
            case 0: // sixteenth notes
                if(isAllOrNothingAtAll == 1)
                    index = 15; 
                for(int i = 0; i < numQuarterNotes; i++)
                {
                    if(isAllOrNothingAtAll != 0 && isAllOrNothingAtAll != 1)
                    {
                        index = rand()%15 + 1; // avoid all zero's combination for the time being. now it picks uniformly from all the other possibillities which is not the most musical decision. Markov Models might come handy which would then specify transitions between one cell to another.
                    }
                    rhythmSeq.insert(rhythmSeq.end(), sixteenthCells[index].begin(), sixteenthCells[index].end());
                }
                break;
            case 1: // eighth note
                if(isAllOrNothingAtAll == 1)
                    index = 3; 
                for(int i = 0; i < numQuarterNotes; i++)
                {
                    if(isAllOrNothingAtAll != 0 && isAllOrNothingAtAll != 1)
                    {
                        index =  + 1;
                    }
                    rhythmSeq.insert(rhythmSeq.end(), eighthCells[index].begin(), eighthCells[index].end());
                }
                break;
            case 2: // quarter note
                if(isAllOrNothingAtAll == 1)
                    index = 1; 
                for(int i=0; i<numQuarterNotes; i++)
                {
                    if(isAllOrNothingAtAll != 0 && isAllOrNothingAtAll != 1)
                    {
                        index = rand()%2; // can allow 0 and 1 even for the time being. 
                    }
                    rhythmSeq.insert(rhythmSeq.end(), quarterCells[index].begin(), quarterCells[index].end());
                }
                break;
            case 3: // eighth note triplet.
                if(isAllOrNothingAtAll == 1)
                    index = 7; 
                for(int i = 0; i < numQuarterNotes; i++)
                {
                    if(isAllOrNothingAtAll != 0 && isAllOrNothingAtAll != 1)
                    {
                        index = rand()%7 + 1;
                    }
                    rhythmSeq.insert(rhythmSeq.end(), eighthTripletCells[index].begin(), eighthTripletCells[index].end());
                }
                break;
            default:
                break;
        }
	}

private:
	vector<vector<int> >sixteenthCells;
	vector<vector<int> >eighthCells;
	vector<vector<int> >eighthTripletCells;
	vector<vector<int> >quarterCells;
	vector<vector<int> >sixteenthTripletCells;
};

#endif