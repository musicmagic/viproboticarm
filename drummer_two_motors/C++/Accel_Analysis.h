//
//  Accel_Analysis.h
//  Accel_analysis
//
//  Created by Minwei Gu on 2/27/14.
//  Copyright (c) 2014 RoboticArm. All rights reserved.
//

#ifndef __Accel_analysis__Accel_Analysis__
#define __Accel_analysis__Accel_Analysis__

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class Accel_Analysis{
    
    // the novelty and hovering value should be set outside this class
    // but for now we just set some numerical value in code

public:
    Accel_Analysis(){
        isWorking = false;
        avgX = 0;
        avgY = 0;
        avgZ = 0;
        count = 0;
        bufferSize = 200; 
        // novelty init
        noveltyCount = 0;
        threshold = 300;
        xBound = 30000;
        yBound = -3500;
        zBound = 15000;
        recordToggle = false;

        // init the avg buffer
        for (int i = 0; i < 3; i++)  //avg X,Y,Z
        {
           vector<float> temp;
           for (int j = 0; j<bufferSize; j++)
              temp.push_back(0);
              avgBuffer.push_back(temp);
        } 
        // hovering init
        refX = 0;
        lastAverage = 0;
        diff = 0;
        modeSize = 100;
        hoverCount = 0; 
        for (int i = 0; i<modeSize; i++)
            modeBuffer.push_back(0);        //should be a small value?
        hover = false;
    }
    ~Accel_Analysis(){
        
        
    }
    
    
    void setWorkToggle()
    {
        if(isWorking){
            cout<<"set acc off"<<endl;
            isWorking = false;      // set a toggle for working
        }
        else
        {
            cout<<"set acc on!"<<endl;
            isWorking = true;
        }
    }
    
    void updateAcc(float accX_, float accY_, float accZ_)    // bypassing acc data and update the acc average value
    {
        accX = accX_;
        accY = accY_;
        accZ = accZ_;
     
        count++;
        count = count%bufferSize;
        avgX = (avgX*(bufferSize-1)+accX)/bufferSize;     // oprimized mean calculation
        avgY = (avgY*(bufferSize-1)+accY)/bufferSize;
        avgZ = (avgZ*(bufferSize-1)+accZ)/bufferSize;
        
        //if(count==50)    
        //   cout<<"X: "<<accX<< " avgX "<< avgX<< " avgY "<< avgY<<" avgZ "<< avgZ<<endl;
        //cout<<avgX<<" ";
        //cout<<"avX: "<<avgX<<" avY: "<<avgY<<" avZ: "<<avgZ<<endl;
        recordGesture();
    }
    
    void recordGesture()
    {
       
        //dataset.open("/home/meka/Desktop/ACCDataset/popup.txt");
        if(recordToggle)
            cout<<avgX<<" "<<accY<<" "<<accZ<<endl; 
        //dataset.close();
    }
    
    float getAvgX()
    {
        return avgX;
    }
    
    
    float getAvgY()
    {
        return avgY;
    }
    
    
    float getAvgZ()
    {
        return avgZ;
    }
    
    float getBufferSize()
    {
        return bufferSize;
    }
    
    void setBufferSize(int size)
    {
        bufferSize = size;
    }
    
    void setBound()
    {
        xBound = avgX;
        yBound = accY;
        zBound = accZ;
        //xBound = avgX;
        //yBound = avgY;
        //zBound = avgZ;
        cout<<"set acc bound: "<<xBound<<" "<< yBound<<" "<<zBound<<endl;
    }
    
    void setRecordToggle()
    {
        if(recordToggle == 1)
            recordToggle = 0;
        else 
            recordToggle = 1;
    }
    bool isNovelty()
    {
        if(isWorking)
        {
            //if(abs(avgZ-zBound)<threshold && abs(avgY-yBound)<threshold && noveltyCount > 400)       // hardcode a place for novelty
            if(accZ>zBound && accY<yBound && noveltyCount>1000)
            {
                cout<<"novelty detected! Y: "<<accY<<" Z: "<<accZ<<endl;
                noveltyCount = 0;
                return true;
            }
            else
            {
                noveltyCount++;
                return false;
            }
        }
        else
            return false;
    }
    
    bool isHovering()
    {
        if(isWorking)
        {
            
            diff = abs(lastAverage-avgX);
            //cout<<"diff: "<<diff<<endl;
            lastAverage = avgX;
            if (diff<100.0 || abs(avgX-refX)<=200)
            {
                if(getMode(diff))
                {
                    lastAverage = avgX;
                    hoverCount = 0;
                    if(!hover)
                    {
                        hover = true;
                        //cout<<"hovering! "<<accX<<endl;
                        refX = avgX;
                        return true;
                    }
                    else return true;
                }
            }
            hoverCount++;
            if(hoverCount>400)
            {
                lastAverage = avgX;
                if(hover)
                {
                //cout<<"striking! "<<accX <<endl;
                hover = false;
                }
                return false;
            }
            else
            {
                
                if(!hover)
                    {
                        hover = true;
                        //cout<<"hovering!"<<accX<<endl;
                        return true;
                    }
                else return true; 
            }
        }
        else 
            return 0;
    }
    
    bool getMode(float diff)
    {
        int index = count % modeSize;
        modeBuffer[index] = diff;
        int total = 0;
        for (int i = 0; i<modeSize; i++){
            if(modeBuffer[i]<1)
                total+=1;
        }
        if(total >=70) 
            return true;
        else 
            return false;
    }
    
    
    
    
private:
    /* basic acc data */
    float accX, accY, accZ;
    float avgX, avgY, avgZ;
    vector <vector <float> > avgBuffer;
    bool isWorking;             // for demo, test for if working
    int bufferSize; 
    int count;                  // count is for avgbuffer index
     // novelty detection
    int noveltyCount;           // noveltyCount for novelty
    int xBound;
    int yBound;
    int zBound;
    float threshold;
    bool recordToggle;
    //ofstream dataset;
    // hovering detection, dedicated to Mason Freestyle
    double refX;                // referenceX memorize the last stable state;
    float prevX;
    int modeSize;
    int hoverCount;             // hover count for hover
    vector<float> modeBuffer;
    float diff, lastAverage;
    bool hover;
    
    
    
    
};


#endif /* defined(__Accel_analysis__Accel_Analysis__) */
