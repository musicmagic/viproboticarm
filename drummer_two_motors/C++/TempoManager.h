 /*
  * A class for tempo changing dynamically 
  *
  * Author:     
  * Annie Zhang (kkanne901@gmail.com)
  *
  *
  */

#ifndef TEMPOMANAGER_H
#define TEMPOMANAGER_H

#include <math.h>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>

#define HIGHBOUND 0.1
#define LOWBOUND 0.05
#define INCRATIO 0.8
#define DECRATIO 1.2

#define INCR_TEMPO_RATIO 1.09
#define DECR_TEMPO_RATIO 0.92

class TempoManager{

public:
    
    bool tempoUp;
    
    TempoManager(){
        tempoUp = true;
    };
   
    int getTempo(int nSamplesPerInterval, float sr){
        return (int)(60.0/(nSamplesPerInterval*sr)/4);
    }

    int updateTempo(int nSamplesPerInterval, float sr){
                
        // change tempo here...
            if (tempoUp){
                if (nSamplesPerInterval*sr >= LOWBOUND)
                    return incTempo(nSamplesPerInterval);
                else
                    tempoUp = false;
            }
            else{
                if (nSamplesPerInterval*sr <= HIGHBOUND)
                    return decTempo(nSamplesPerInterval);
                else
                    tempoUp = true;
            }
        return nSamplesPerInterval;
    }
    
    int incTempo(int nSamplesPerInterval){
        return (int)(nSamplesPerInterval*INCRATIO);
    }
    
    int decTempo(int nSamplesPerInterval){
        return (int)(nSamplesPerInterval*DECRATIO);
    }
    
    float changeTempo(float startTempo, float multiplier)
    {
        return startTempo*multiplier;
    }
    
   
};

#endif