#ifndef RUDIMENTSEQUENCER_H
#define RUDIMENTSEQUENCER_H


// a class very similar to the sequencer class except that this will also have the additional capability to stagger the inter sequence element sample time. Which are needed for flam like rudiments

#include <math.h>
#include <vector>

class RudimentSequencer
{
private:
	std::vector<int> m_Sequence;
	std::vector<int> m_OffsetSequence;

	int m_MeanSamplesPerInterval;
	int m_SamplesOffsetPerInterval; // for flam rudiments, the deviation from the mean interval. 

	float m_BaseTempo;
	float m_QuarterNoteTimeInSec;

	float m_SampleRate;
	int m_SequenceIndex;
	int m_MeanSampleCounter; // to keep track of how many samples elapse between two hits. it is this param which would be changing in the case of flam rudiments. 
	int m_HitSampleCounter;

	float k_SixteenthNote; // These are specifying the noteFlow at a particular tempo. 
    float k_EighthNote;
    float k_QuarterNote;
    float k_EighthNoteTriplet;

    float m_CurrentNoteFlow;

    bool isNewSequenceElementReady;

public:
	RudimentSequencer()
	{
		m_SampleRate = 0;
    	m_SequenceIndex = -1;
    	m_MeanSampleCounter = 0;
    	m_HitSampleCounter = 0;

    	m_MeanSamplesPerInterval = 0;
    	m_SamplesOffsetPerInterval = 0;
    	m_BaseTempo = 0;
    	m_QuarterNoteTimeInSec = 0;

    	k_SixteenthNote = 0.25; // these four should be consts but generates warnings with gnu+11 compiler.
        k_EighthNote = 0.5;
        k_QuarterNote = 1;
        k_EighthNoteTriplet = (float)1.0/3.0;

        m_CurrentNoteFlow = k_SixteenthNote;

        isNewSequenceElementReady = false;


	}
	~RudimentSequencer()
	{

	}
    
    void resetIndices()
    {
        m_SequenceIndex = -1;
        m_MeanSampleCounter = 0;
    	m_HitSampleCounter = 0;
    }
	void setParams(float baseTempo,float sampleRate)
	{
		m_BaseTempo = baseTempo;
		m_SampleRate = sampleRate;
		m_QuarterNoteTimeInSec = ((float)60.0)/baseTempo;
		double l_idealTimeInterval = m_QuarterNoteTimeInSec*m_CurrentNoteFlow; // // each sequence element is interpreted as a 16th note.
    	m_MeanSamplesPerInterval = (int)(l_idealTimeInterval / sampleRate);
    	m_SamplesOffsetPerInterval = 0.1*m_MeanSamplesPerInterval; // For regular rudiments the hits are evenly spaced for each stick. 10% of the inter sequence element distance before the main hit
	}
	void setSequence(std::vector<int> inputSeq)
    {
        
    	m_Sequence = inputSeq;
        m_SequenceIndex = -1;
       
    }
    void setOffsetSequence(std::vector<int> inputSeq)
    {
    	m_OffsetSequence = inputSeq;
    }

    void setTempo(float newTempo)
    {
    	m_BaseTempo = newTempo;
    	m_QuarterNoteTimeInSec = ((float)60.0)/newTempo;
    	double l_idealTimeInterval = m_QuarterNoteTimeInSec*m_CurrentNoteFlow; // // each sequence element is interpreted as a 16th note.
    	m_MeanSamplesPerInterval = (int)(l_idealTimeInterval / m_SampleRate);
    	m_SamplesOffsetPerInterval = 0.1*m_MeanSamplesPerInterval;
    }
	void setNoteFlow(int noteFlowType)
    {
    	switch(noteFlowType)
    	{
    		case(k_Sixteenth):
    			m_CurrentNoteFlow = k_SixteenthNote;
    			break;
    		case(k_Eighth):
    			m_CurrentNoteFlow = k_EighthNote;
    			break;
    		case(k_Quarter):
    			m_CurrentNoteFlow = k_QuarterNote;
    			break;
    		case(k_EighthTriplet):
    			m_CurrentNoteFlow = k_EighthNoteTriplet;
    			break;
    		default:
    			break;
    	}
    	setTempo(m_BaseTempo);
    }

    int getCurrentNoteFlow()
    {
        if(m_CurrentNoteFlow == k_SixteenthNote)
            return k_Sixteenth;
        else if(m_CurrentNoteFlow == k_EighthNote)
            return k_Eighth;
        else if(m_CurrentNoteFlow == k_QuarterNote)
            return k_Quarter;
        else if(m_CurrentNoteFlow == k_EighthNoteTriplet)
            return k_EighthTriplet;
        else
            return k_SixteenthNote;
        
    }

    int getSequenceValue()
    {
    	updateHitSample();
    	if(isNewSequenceElementReady && m_HitSampleCounter == 0)
    	{
    		m_SequenceIndex++;
    		if(m_SequenceIndex >= m_Sequence.size())
    		{
    			m_SequenceIndex = 0;
    		}
    		updateSample();
    		return m_Sequence[m_SequenceIndex];
    	}
    	else
    	{
    		updateSample();
    		return -1; 
    	}

    }
	enum NoteFlow_t
    {
    	k_Sixteenth,
    	k_Eighth,
    	k_Quarter,
    	k_EighthTriplet
    };

private:
	void updateHitSample()
	{
		m_HitSampleCounter++;
		if(m_HitSampleCounter >= m_MeanSamplesPerInterval + m_OffsetSequence[(m_SequenceIndex+1)%(m_Sequence.size())]*m_SamplesOffsetPerInterval && !isNewSequenceElementReady)
		{
			isNewSequenceElementReady = true;
			m_HitSampleCounter = 0;
		}
	}
	void updateSample()
	{
		m_MeanSampleCounter++;
		if(m_MeanSampleCounter >= m_MeanSamplesPerInterval)
		{
			m_MeanSampleCounter = 0;
			m_HitSampleCounter = 0;
			isNewSequenceElementReady = false;
		}
	}


};
#endif