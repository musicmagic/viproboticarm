
//counter class for testing
class  counter {
    double  x;
public:
    counter() {
        x = 0.0;
    }
    double output(void) {
        x = x + 1.0;
        return x; 
    }
    double getX() const
    {
        return x;
    }
    void setX(double v)
    {
        x = v;
    }
};