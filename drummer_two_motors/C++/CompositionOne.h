#ifndef COMPOSITIONONE_H
#define COMPOSITIONONE_H

#include <iostream>
#include <cstdlib>
#include <ctime>

#include "Sequencer.h"
#include "TempoManager.h"

#define TOTAL_NUMBER_SECTIONS 3
#define START_TEMPO_THIS 110
#define START_TEMPO_THIS1 220
class CompositionOne
{

	Sequencer *m_PrimarySeq;
	Sequencer *m_SecondarySeq;
    
    TempoManager *m_TempoManager;
    
    
    int m_NumQuarterPrimary;
    int m_NumQuarterSecondary;
	float m_PrimaryBaseTempo;
    float m_SecondaryBaseTempo;
    
	float m_SampleRate;


	int m_CurrentSection;
    int m_TimeBetweenChangesInSamples;
    int m_SampleCounter;
    
    bool isNewPrimaryGenerated;
    bool isNewSecondaryGenerated;
    
    bool m_ByPassP;
    bool m_ByPassS;

public:
	CompositionOne()
	{
		m_PrimarySeq = new Sequencer();
		m_SecondarySeq = new Sequencer();
        
        m_TempoManager = new TempoManager();
        
		m_SampleRate =  0.001;
		m_PrimaryBaseTempo = START_TEMPO_THIS;
        m_SecondaryBaseTempo = START_TEMPO_THIS1;
        m_NumQuarterPrimary = 56;
        m_NumQuarterSecondary = 57; 
        
        m_CurrentSection = -1;
        m_TimeBetweenChangesInSamples = 535000; // 12 seconds at 1000 Hz
        m_SampleCounter = 0;
        
        
        
 		m_PrimarySeq->setParams(m_PrimaryBaseTempo, m_SampleRate);
 		m_SecondarySeq->setParams(m_SecondaryBaseTempo,m_SampleRate); // to start of both will have the same tempo
       
        //initialize sequences; 
        m_PrimarySeq->generateCellBasedSequence(m_NumQuarterPrimary, 1);
        m_SecondarySeq->generateCellBasedSequence(m_NumQuarterSecondary, -1);
        isNewPrimaryGenerated = false;
        isNewSecondaryGenerated = false;
        m_ByPassP = false;
        m_ByPassS = false;
        updateSection();
        std :: cout << " Section is " << m_CurrentSection << std::endl;
	}

	~CompositionOne()
	{
		delete m_PrimarySeq;
		delete m_SecondarySeq;
        delete m_TempoManager;
	}

	int getCurrentSection()
	{
		return m_CurrentSection;
	}

	int getPrimaryStrokeType()
	{
       // std::cout << "Current section " << m_CurrentSection<< std::endl;
        //std::cout << "P Compn1 " <<std::endl;
        
        switch(m_CurrentSection)
        {
            case 0:
                updateCounter();
                break;
            case 1:
                updateCounter();
                if(m_PrimaryBaseTempo <= 135)
                {
                    if(m_SampleCounter%300 == 0)
                    {
                         m_PrimaryBaseTempo = m_TempoManager->changeTempo(m_PrimaryBaseTempo, 1.0002);
                         m_PrimarySeq->setTempo(m_PrimaryBaseTempo);
                    }
                }else
                {
                    m_SampleCounter = 0;
                    setTimer(23); //13 seconds for 3rd section
                    isNewPrimaryGenerated = false;
                    isNewSecondaryGenerated = false;
       
                }
                break;
            case 2:
                updateCounter();
                
                if(isNewPrimaryGenerated == false)
                {
                    std::cout << "first generated "<<std::endl;
                    m_PrimaryBaseTempo = START_TEMPO_THIS; //starting tempo;
                    m_PrimarySeq->setTempo(m_PrimaryBaseTempo);
                    m_PrimarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                    m_PrimarySeq->generateCellBasedSequence(7, -1);
                    isNewPrimaryGenerated = true;
                }  
                break;
            default:
                break;
        }
        
        return m_PrimarySeq->getSequenceValue();
		
	}

	int getSecondaryStrokeType()
	{
        switch(m_CurrentSection)
        {
            case 1:
                if(m_SecondaryBaseTempo >= 108)
                {
                    if(m_SampleCounter%300 == 0)
                    {
                         m_SecondaryBaseTempo = m_TempoManager->changeTempo(m_SecondaryBaseTempo, 0.999);
                         m_SecondarySeq->setTempo(m_SecondaryBaseTempo);
                         if(m_SecondaryBaseTempo >= 113 && m_SecondaryBaseTempo < 117)
                            {
                                m_SecondarySeq->setNoteFlow(Sequencer::k_EighthTriplet);
                            }
                        if(m_SecondaryBaseTempo >= 108 && m_SecondaryBaseTempo < 113)
                            {
                                m_SecondarySeq->setNoteFlow(Sequencer::k_Eighth);
                            }
                    }
                }
                break;
            case 2:
               
                if(isNewSecondaryGenerated == false)
                { 
                    std::cout << "second generated "<<std::endl;
                    m_SecondaryBaseTempo = START_TEMPO_THIS;
                    m_SecondarySeq->setTempo(m_SecondaryBaseTempo);
                    m_SecondarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                    m_SecondarySeq->generateCellBasedSequence(17, 1);
                    isNewSecondaryGenerated = true;
                }
                
                break;
            default:
                break;
                
        }
        //std::cout <<" S comp1 " <<std::endl;
		return m_SecondarySeq->getSequenceValue();
	}
    
    void setNoteFlowForSequencer(int noteflow)
    {
        switch(noteflow)
        {
            case 4:
                std::cout << "Changed note flow to triplet " << std::endl;
                m_SecondarySeq->setNoteFlow(Sequencer::k_EighthTriplet);
                break;
            case 5:
                std::cout << "Changed note flow to sixteenth " << std::endl;
                m_SecondarySeq->setNoteFlow(Sequencer::k_Sixteenth);
                break;
            default:
                break;         
        }
    }
    
    bool getCurrentByPass()
    {
        return false;
    }
   void updateSection()
   {
    m_CurrentSection++;
    switch(m_CurrentSection)
    {
        case 0:
            m_ByPassP = true;
            m_ByPassS = false;
            break;
        default:
            break;
    }
    std::cout << "Section is " << m_CurrentSection <<endl;
   }
    
private:
   void updateCounter()
   {
       
        m_SampleCounter++;
        if(m_SampleCounter >= m_TimeBetweenChangesInSamples)
        {
            updateSection();
            m_SampleCounter = 0;
           // triggerUpdate(m_CurrentSection);
        }
   }
   
   
   
   void setTimer(float seconds)
   {
    m_TimeBetweenChangesInSamples = seconds*1000;
   }
//    void triggerUpdate(int currentSection)
//    {
//         
//    }

};

#endif
