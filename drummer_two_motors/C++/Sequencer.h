#ifndef SEQ_H
#define SEQ_H

#include <vector>
#include <math.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include "CellBasedRhythmGen.h"

#define EXPCURVE(X, Y) (exp(X*log(Y)) - (float)1.0)/(Y - (float)1.0)

class Sequencer
{

	std::vector<int> m_Sequence;
	int m_SamplesPerInterval; // the time distance between two sequence elements in samples.
	float m_BaseTempo; //in bpm, to be specified at instantiation by the composition class. 
	float m_QuarterNoteTimeInSec; // how many seconds equals one quarter note at m_BaseTempo bpm

	float m_SampleRate; 

	int m_SequenceIndex;
	int m_SampleCounter; // to keep track of the number of samples that have elapsed so that the sequence elements can be retrieved at appropriate time only

	CellBasedRhythmGen *m_CellGen; // to do sequence manipulations;

	float k_SixteenthNote; // These are specifying the noteFlow at a particular tempo. 
    float k_EighthNote;
    float k_QuarterNote;
    float k_EighthNoteTriplet;

    float m_CurrentNoteFlow;

   

    //no more random modes. there would randomization functions which will be called by the composition if the composition requires randmoziaton of the sequence
public:
    Sequencer()
    {
    	std::srand(unsigned (std :: time(0)));
    	m_SampleRate = 0;
    	m_SequenceIndex = -1;
    	m_SampleCounter = 0;
    	
    	m_SamplesPerInterval = 0;
    	m_BaseTempo = 0;
    	m_QuarterNoteTimeInSec = 0;


    	k_SixteenthNote = 0.25; // these four should be consts but generates warnings with gnu+11 compiler.
        k_EighthNote = 0.5;
        k_QuarterNote = 1;
        k_EighthNoteTriplet = (float)1.0/3.0;

        m_CurrentNoteFlow = k_SixteenthNote;

    	m_CellGen = new CellBasedRhythmGen();
    }

    ~Sequencer()
    {
    	delete m_CellGen;
    }

    void setParams(float baseTempo, float sampleRate)
    {
    	m_SampleRate = sampleRate;
    	m_BaseTempo = baseTempo;
    	m_QuarterNoteTimeInSec = ((float)60.0)/baseTempo;
    	double l_idealTimeInterval = m_QuarterNoteTimeInSec*m_CurrentNoteFlow; // // each sequence element is interpreted as a 16th note.
    	m_SamplesPerInterval = (int)(l_idealTimeInterval / sampleRate);
    }

    //setters getters for tempo....etc

    void setTempo(float newTempo)
    {
    	m_BaseTempo = newTempo;
    	m_QuarterNoteTimeInSec = ((float)60.0)/newTempo;
    	double l_idealTimeInterval = m_QuarterNoteTimeInSec*m_CurrentNoteFlow; // // each sequence element is interpreted as a 16th note.
    	m_SamplesPerInterval = (int)(l_idealTimeInterval / m_SampleRate);
    }

    float getTempo()
    {
    	return m_BaseTempo;
    }

    void setNoteFlow(int noteFlowType)
    {
    	switch(noteFlowType)
    	{
    		case(k_Sixteenth):
    			m_CurrentNoteFlow = k_SixteenthNote;
    			break;
    		case(k_Eighth):
    			m_CurrentNoteFlow = k_EighthNote;
    			break;
    		case(k_Quarter):
    			m_CurrentNoteFlow = k_QuarterNote;
    			break;
    		case(k_EighthTriplet):
    			m_CurrentNoteFlow = k_EighthNoteTriplet;
    			break;
    		default:
    			break;
    	}
    	setTempo(m_BaseTempo);
    }

    int getCurrentNoteFlow()
    {
        if(m_CurrentNoteFlow == k_SixteenthNote)
            return k_Sixteenth;
        else if(m_CurrentNoteFlow == k_EighthNote)
            return k_Eighth;
        else if(m_CurrentNoteFlow == k_QuarterNote)
            return k_Quarter;
        else if(m_CurrentNoteFlow == k_EighthNoteTriplet)
            return k_EighthTriplet;
        else
            return k_SixteenthNote;
        
    }
    void setSequence(std::vector<int> inputSeq)
    {
    	m_Sequence = inputSeq;
    }

    // void getSequence(std::vector<int>& outputSeq)
    // {
    //     outputSeq = m_Sequence;
    // }
    double getSamplesPerInterval()
    {
        return m_SamplesPerInterval;
    }
    /******************************************************/

    //Sequence manipulators - These manipulators will be called by the compositions as and when required.

    void inPlaceShuffle()
    {
        std::random_shuffle(m_Sequence.begin(), m_Sequence.end());
    }
    
    void reverseSequence()
    {
        std::reverse(m_Sequence.begin(), m_Sequence.end());
    }

    void generateCellBasedSequence(int numQuarterNotes, int isAllOrNothingAtAll) // 0 for for all zero sequence  and 1 for all 1's sequence, -1 for random combination
    {
        int noteFlowMode = getCurrentNoteFlow();
        m_Sequence.clear();
        m_CellGen->generateCellBasedRhythm(m_Sequence, noteFlowMode, numQuarterNotes, isAllOrNothingAtAll);
       // printSequence(m_Sequence);
    }
    /******************************************************/

    //Sequencer strike functions
    
    void resetSampleCounter()
    {
        m_SampleCounter = 0;
    }
    
    int getSequenceValue() // If a particular section in a composition a sequencer is being played, then this wll be called to get the current stroke generator stroke type
    {
        updateSample();
        if(m_SampleCounter == 1)
        {
            m_SequenceIndex++;
            if(m_SequenceIndex >= m_Sequence.size())
            {
                m_SequenceIndex = 0;
            }
            return m_Sequence[m_SequenceIndex];
        }else
        {
            return -1;
        }

    }
    
     enum NoteFlow_t
    {
    	k_Sixteenth,
    	k_Eighth,
    	k_Quarter,
    	k_EighthTriplet
    };
private:
    void updateSample()
    {
        m_SampleCounter++;
        if(m_SampleCounter >= m_SamplesPerInterval)
        {
            m_SampleCounter = 1;
        }
    }
    
    void printSequence(std::vector<int>&seq)
    {
        for(int i=0; i<seq.size(); i++)
        {
            std::cout << seq[i] << " ";
        }
        std::cout << std::endl;
    }
};

#endif