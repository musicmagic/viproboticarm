#ifndef POT_ANALYSIS_H
#define POT_ANALYSIS_H

#include <cmath>
#include <iostream>
#include <vector>

class  Pot_Analysis {
    float avgBuffer[30];
    std::vector<float> primary; //kp,kd
    std::vector<float> secondary;//kp,kd
    
public:
    Pot_Analysis(){
        
        
    }
    
    ~Pot_Analysis()
    {
    
    }
    void potPrimaryRegress(std::vector<float>& regressedVals, float potValue){
        float max = 1.3;
        float min = .00005;
        
        regressedVals[0] = (potValue - 464)/(2012.0-464.0) * (max - min) + min; //kp
        if( regressedVals[0] > .7){
            regressedVals[1] = 0.005;//kd
        }else{
            regressedVals[1] = .0001;//.00005;
        }
        
        primary = regressedVals;
    }
    
private:
    
    
};

#endif