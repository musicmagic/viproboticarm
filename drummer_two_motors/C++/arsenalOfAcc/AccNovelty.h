//
//  AccNovelty.h
//  acc_novelty
//  A class that detect novelty through accelerometer
//  Created by Minwei Gu on 2/2/14.
//
//

#ifndef __acc_novelty__AccNovelty__
#define __acc_novelty__AccNovelty__

#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <string>
#include <map>

using namespace std;

class AccNovelty{

public:
    AccNovelty(){
        accCount = 0;
        isNovelty = false;
        threshold = 0;
        bridgeCount = 0;
    }
    ~AccNovelty(){
    }
    void parseAccData(string accFile){
    }
    void parseAccData(float accX, float accY, float accZ){
        //cout<<"acc recieved!"<<endl;
        //cout<<"X: "<<accX<<" Y: "<<accY<<" Z: "<<accZ<<endl;
        int key = accCount++;
        vector<float> value(3);
        value[0] = accX;
        value[1] = accY;
        value[2] = accZ;
    }
    void noveltyDetection(int i){
        if (accData.find(i)->second.size()==3)
        {
        if (bridgeCount!=0)
            bridgeCount++;
        if (accData.find(i)->second[0]>0.8)
        {
            bridgeCount+=1;
            cout<<"Novelty Detected at: "<<accCount * 0.001 <<"s"<<endl;
        }
        else if(accData.find(i)->second[0]<0.8 && bridgeCount >=20)
        {
            
            bridgeCount = 0;
        }
    }
}// using a simple moving average filter to detect the novelty
    
   
private:
    int accCount;
    bool isNovelty;
    int bridgeCount;
    float threshold;
    vector<int> noveltyIndex;
    map<int, vector<int> > accData;
};
#endif /* defined(__acc_novelty__AccNovelty__) */
