package machineMusicianship;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.cycling74.max.*;

public class CallAndResponse extends MaxObject
{

	private static final String[] INLET_ASSIST = new String[]{
		"inlet 1 help"
	};
	private static final String[] OUTLET_ASSIST = new String[]{
		"outlet 1 help"
	};
	
	private static final int NoteDensityInterval = 200; //200 ms
	
	
	private volatile List<Integer> intervals = new ArrayList<Integer>();
	private volatile List<Integer> pitches = new ArrayList<Integer>();
	private volatile List<Long> onsetTimes = new ArrayList<Long>();
	private volatile List<Integer> noteDensities = new ArrayList<Integer>();
	private volatile boolean listening = false;
	private volatile boolean listenThread = false;
	private volatile float standardDev;
	private volatile float averageInterval;
	private volatile float pitchStd;
	private volatile float maxInterval;
	private volatile List<Integer> pitchContours = new ArrayList<Integer>();
	
	public CallAndResponse(Atom[] args)
	{
		declareInlets(new int[]{DataTypes.ALL});
		declareOutlets(new int[]{DataTypes.ALL,DataTypes.ALL,DataTypes.ALL});
		
		setInletAssist(INLET_ASSIST);
		setOutletAssist(OUTLET_ASSIST);

	}
    
	public void bang()
	{
		if(getInlet() ==0){
			listening = true;
			listenThread = true;
			listen();
		}else{
			listening = false;
		}
	}

	public void inlet(int midiNote)
	{

		if(listening == true){

			onsetTimes.add(System.currentTimeMillis());
			pitches.add(midiNote);

			if(onsetTimes.size() > 1){

				List<Integer> intervals = new ArrayList<Integer>();
				//get average
				float avg=0;
				for(int i=1; i<onsetTimes.size();i++){
					intervals.add((int) (onsetTimes.get(i) - onsetTimes.get(i-1)));
					avg+= intervals.get(intervals.size()-1);
				}
				avg /= (onsetTimes.size()-1);


				//standard deviation
				float std = 0;
				for(int i=0;i<intervals.size();i++){
					std += Math.pow(intervals.get(i) - avg,2);
				}
				standardDev = (float) Math.sqrt(std / intervals.size());
				averageInterval = avg;
			}

		}		
	}
    
	public void inlet(float f)
	{
	}
    
    
	public void list(Atom[] list)
	{
	}
	
	public void endThread(){
		listenThread = false;
		System.out.println("not listening");
	}
	
	private void play(){
		
		boolean playing = true;
		int waitTime = (int) (noteDensities.get(0)/4);
		int waitTime2 = waitTime / 2;
		long lastOutTime = 0;
		long lastOutTime2 = 0;
		int playTime = 0; //NoteDensityInterval * noteDensities.size() + (int)(NoteDensityInterval/2);
		
		playTime = (int) (onsetTimes.get(onsetTimes.size()-1) - onsetTimes.get(0) + (onsetTimes.get(1) - onsetTimes.get(0)));
		
		long startTime = System.currentTimeMillis();
		int nextTime = (int) (onsetTimes.get(1) - onsetTimes.get(0));
		long changeTime = startTime;
		Random rand = new Random();
		int count=0;
		int pitchToPlay;

		
		waitTime = (int) (noteDensities.get(count)/2);
		if (waitTime < 90)waitTime = 90;
		waitTime2 = (int) (waitTime *1.5);

		while(System.currentTimeMillis() - startTime < playTime){

			if(System.currentTimeMillis() >= (changeTime + nextTime) ){
				count++;
				if(count < noteDensities.size()){
					waitTime = (int) (noteDensities.get(count)/1.7);
					if (waitTime < 90)waitTime = 90;
					waitTime2 = waitTime;
					
					changeTime = System.currentTimeMillis();
					nextTime = (int) (onsetTimes.get(count+1) - onsetTimes.get(count));
					System.out.println(waitTime);
					
					if(System.currentTimeMillis() - lastOutTime >= waitTime || lastOutTime ==0){
						pitchToPlay = rand.nextInt(pitches.size());
						outlet(0,pitches.get(pitchToPlay));
					}
					
				}
			}
			
			
			try {
				Thread.sleep(2);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			Thread.sleep(waitTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void clearVars(){
		intervals.clear();
		pitches.clear();
		noteDensities.clear();
		onsetTimes.clear();
		pitchContours.clear();
	}
	
	private void getContours(){
		
		
		//note density contour
		int playDuration = (int) (onsetTimes.get(onsetTimes.size()-1) - onsetTimes.get(0));
		int nIntervals = playDuration / NoteDensityInterval;
		noteDensities.clear();
		for(int i=0;i<onsetTimes.size()-1;i++){
			noteDensities.add( (int) ((onsetTimes.get(i+1) - onsetTimes.get(i))) );
		}
		
		for(int i=0; i<noteDensities.size();i++){
			System.out.println("noteDensity " + noteDensities.get(i));
		}
		
		
		//pitch contour
		pitchContours.clear();
		for(int i=1;i<pitches.size();i++){
			if(pitches.get(i) > pitches.get(i-1)){
				pitchContours.add(1);
			}
			if(pitches.get(i) < pitches.get(i-1)){
				pitchContours.add(-1);
			}
			if(pitches.get(i) == pitches.get(i-1)){
				pitchContours.add(0);
			}
		}
	
	}
	
	private void getStatistics(){
		
		int maxInterval=-1;
		int interval;
		for(int i=1;i<onsetTimes.size();i++){
			interval = (int) (onsetTimes.get(i) - onsetTimes.get(i-1));
			if(interval > maxInterval){
				maxInterval = interval;
			}
		}
		this.maxInterval = maxInterval;
		System.out.println("max interval "+maxInterval);
		
		
		//pitch standard deviation
		float avg = 0;
		for(int i=0;i<pitches.size();i++){
			avg += pitches.get(i);
		}
		avg /= pitches.size();
		
		float std = 0;
		for(int i=0;i<pitches.size();i++){
			std += Math.pow(pitches.get(i) - avg,2);
		}
		pitchStd = (int) Math.sqrt(std / pitches.size());
		
		
	}
	
	private void listen(){
		
		new Thread(){
			public void run(){
				
				int duration;
				while(listenThread == true){
					
					if(onsetTimes.size() > 2){
						duration = (int) (System.currentTimeMillis() - onsetTimes.get(onsetTimes.size()-1));
						if(duration > (averageInterval + standardDev) && duration > 1200){
							listening = false;
							getContours();
							getStatistics();
							play();
							clearVars();
							listening = true;
						}
					}
					
					try {
						Thread.sleep(5);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
		}.start();	
	}
}




