/*  File    : behavior_controller.cpp
 *
 *  Author:
 *      Mason Bretan (masonbretan@gmail.com)
 *
 *
 *  Abstract:
 *      Controls the behavior modes of the stick where each mode is a class and stored as a pointer 
 *      in the pointers vector PWork
 *
 */

#include <iostream>

#define S_FUNCTION_LEVEL 2
#define S_FUNCTION_NAME  behavior_controller
 
/*
 * Need to include simstruc.h for the definition of the SimStruct and
 * its associated macro definitions.
 */
#include "simstruc.h"
#include "C++/counter.h"
//#include "C++/Stroke_Generator.h"
//#include "C++/Mason_Freestyle.h" 
//#include "C++/AccNovelty.h"
#include "C++/Main_Control.h"

#define IS_PARAM_DOUBLE(pVal) (mxIsNumeric(pVal) && !mxIsLogical(pVal) &&\
!mxIsEmpty(pVal) && !mxIsSparse(pVal) && !mxIsComplex(pVal) && mxIsDouble(pVal))
 
/*====================*
 * S-function methods * 
*********************/
#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS)  && defined(MATLAB_MEX_FILE)
/*
 * Check to make sure that each parameter is 1-d and positive
 */
static void mdlCheckParameters(SimStruct *S)
{

    const mxArray *pVal0 = ssGetSFcnParam(S,0);
 
    if ( !IS_PARAM_DOUBLE(pVal0)) {
        ssSetErrorStatus(S, "Parameter to S-function must be a double scalar");
        return;
    } 
}
#endif



/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
    /* See sfuntmpl.doc for more details on the macros below */

    ssSetNumSFcnParams(S, 1);  /* Number of expected parameters */
#if defined(MATLAB_MEX_FILE)
    if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
        mdlCheckParameters(S);
        if (ssGetErrorStatus(S) != NULL) {
            return;
        }
    } else {
        return; /* Parameter mismatch will be reported by Simulink */
    }
#endif
    ssSetSFcnParamTunable(S, 0, 0);

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);
    
    int numInputs = 9;
    if (!ssSetNumInputPorts(S, numInputs)) return;
    for (int i=0; i<numInputs; i++)
    {
        ssSetInputPortWidth(S, i, 1);
        ssSetInputPortDirectFeedThrough(S, i, 1);
        
        /*
         *analog 1 b0, analog 2 b0, acc x b0, acc y b0, acc z b0, analog 1 b1, analog 2 b1, keyboard
         */
    }

    if (!ssSetNumOutputPorts(S, 6)) return;
    ssSetOutputPortWidth(S, 0, 1);
    ssSetOutputPortWidth(S, 1, 1); 
    ssSetOutputPortWidth(S, 2, 1);
    ssSetOutputPortWidth(S, 3, 1); 
    ssSetOutputPortWidth(S, 4, 1); 
    ssSetOutputPortWidth(S, 5, 1);

    
    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 4); // reserve element in the pointers vector, third one for acc object
    ssSetNumModes(S, 0); // to store a C++ object
    ssSetNumNonsampledZCs(S, 0);

    ssSetSimStateCompliance(S, USE_CUSTOM_SIM_STATE);

    ssSetOptions(S, 0);
}



/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    This function is used to specify the sample time(s) for your
 *    S-function. You must register the same number of sample times as
 *    specified in ssSetNumSampleTimes.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, mxGetScalar(ssGetSFcnParam(S, 0)));
    std::cout << mxGetScalar(ssGetSFcnParam(S,0)) << std::endl;
    ssSetOffsetTime(S, 0, 0.0);
    ssSetModelReferenceSampleTimeDefaultInheritance(S);
}

#define MDL_START  /* Change to #undef to remove function */
#if defined(MDL_START) 
  /* Function: mdlStart =======================================================
   * Abstract:
   *    This function is called once at start of model execution. If you
   *    have states that should be initialized once, this is the place
   *    to do it.
   */
  static void mdlStart(SimStruct *S)
  {
      ssGetPWork(S)[0] = (void *) new counter; // store new C++ object in the poitners vector
      //double param = mxGetScalar(ssGetSFcnParam(S, 0));
      //ssGetPWork(S)[1] = (void *) new Stroke_Generator(param);
      //Stroke_Generator* stroke = (Stroke_Generator*) ssGetPWork(S)[1];
      //stroke->setSequencerParams(120,param); // THE FIRST PARAM IS NOW THE TEMPO IN BPM.... Makes more musical sense that way and by default each value in the sequencer is interpreted as a 16th note. 
      
      //ssGetPWork(S)[2] = (void *) new AccNovelty();
      //AccNovelty* novelty = (AccNovelty*) ssGetPWork(S)[2];
      
      //ssGetPWork(S)[3] = (void *) new Mason_Freestyle();
      //Mason_Freestyle* freeStyle = (Mason_Freestyle*) ssGetPWork(S)[3];
      
      
      //I think we only need one pwork vector elemet
      ssGetPWork(S)[1] = (void *) new Main_Control();
      Main_Control* mainControl = (Main_Control*) ssGetPWork(S)[0];
      
      
      //ssGetPWork(S)[1] = (void *) new Periodic(param);
      //Periodic* periodic = (Periodic*) ssGetPWork(S)[2];
      //periodic->setFrequency(9);
      //periodic->initRamp(1,10,.5);
      
  }
  
  
#endif /*  MDL_START */

/* Function: mdlOutputs =======================================================
 * Abstract:
 *    In this function, you compute the outputs of your S-function
 *    block.
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    //counter *c = (counter *) ssGetPWork(S)[0];
    

   
    
    /*
     *outputs
     *
     *y[0] = position for stick 0
     *y[1] = kp stick 0
     *y[2] = kd stick 0
     *y[3] = position for stick 1
     *y[4] = kp stick 1
     *y[5] = kd stick 1
     *
     **/
    
    /*
     *inputs
     *
     *y[0] = board 0 analog 1 
     *y[1] = board 0 analog 2
     *y[2] = accel x
     *y[3] = accel y
     *y[4] = accel z
     *y[5] = board 1 analog 1 (EMG)
     *y[6] = board 1 analog 2 (poteniometer)
     *
     **/
    
    Main_Control* mainControl = (Main_Control*) ssGetPWork(S)[1]; //the main controller for everything
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S, 0);
    real_T  *y = ssGetOutputPortRealSignal(S,0); 
    
    real_T b0_analog1 = *uPtrs[0]; //
    real_T b0_analog2 = *uPtrs[1];
    float accX = (float)*uPtrs[2];
    float accY = (float)*uPtrs[3];
    float accZ = (float)*uPtrs[4];
    real_T b1_analog1 = *uPtrs[5]; //EMG
    real_T b1_analog2 = *uPtrs[6]; //poteniometer
    int keyBoardPress = *uPtrs[7];
    int udpReceive = *uPtrs[8]; //numerical value --> look in networking.cpp class for definitions of values
    
    if(udpReceive >=0 && udpReceive <100){
        //strike with primary arm
        mainControl->primaryArmStrike();
    }
    if(udpReceive >=100 && udpReceive <200){
        //strike with secondary arm
        mainControl->secondaryArmStrike();
    }
    if(udpReceive >=200 && udpReceive <300){
        mainControl->setCurrentPiece(udpReceive - 200);
    }
    if(udpReceive >=300 && udpReceive <400){
        mainControl->setCurrentSection(udpReceive - 300);
    }
    if(udpReceive >=400 && udpReceive <500){
        mainControl->setPrimaryKp_usingMessage(udpReceive-400);
    }
    if(udpReceive >=500 && udpReceive <600){
        mainControl->setPrimaryKd_usingMessage(udpReceive-500);
    }
    if(udpReceive >=700 && udpReceive <800){
       //kp second arm
    }
    if(udpReceive >=700 && udpReceive <800){
        //kd second arm
    }
    
    
    
    //std::cout<< "pot = " << b1_analog2 <<std::endl;
    //real_T analogVal = potValue;
    //analogVal = (analogVal - 464)/(2012.0-464.0); // hacky scaling. assuming the max and min for the pot
    //analogVal = 1;
    // potValue will be in the range 0-1, linear;
    
    //analogVal = emgValue;
    //analogVal = (analogVal - 2020)/(2050.0-2020.0);
    

    //float keypress = (float)*uPtrs[7];
    //if(keypress != 0){
    //    std::cout<< keypress;
    //}
    
    //std::cout<< "press ... " << keyBoardPress <<std::endl;
    double mReceived = (double)*uPtrs[8];
    if (mReceived > 0)
        std::cout<< "Received ... " << mReceived <<std::endl;
    
    //Stroke_Generator* stroke = (Stroke_Generator*) ssGetPWork(S)[1];
    double pos1= mainControl->updatePrimaryPos();
    //std::cout<< "pos = " << pos1 <<std::endl;
    y[0] = pos1;///*analogVal*/0.8*(stroke->getSequencerStrike()); // 0.7 fixed amplitude so that we can actually play the arm on the drum
    
    mainControl->setPrimaryKpKd_usingPot(b1_analog2);
    double pos =mainControl->updateSecondaryPos(); 
    
    //std::cout<< pos <<std::endl;
    y[1] = mainControl->getPrimaryKp();///*analogVal*/0.8*0.5*errDerivative;
    y[2] = mainControl->getPrimaryKd();
    y[3] =  pos;
    y[4] = .5; 
    y[5] = .02; 
    
    
    // for Minwei's stuff don't comment when using
    mainControl->updateAccData(accX,accY,accZ);
    if(keyBoardPress == 53)         //key "5" enable/disable for all acc tricks
    {
        mainControl->setAccWork(); 
    }
    
    //for mason's stuff, comment out when not using
    //Mason_Freestyle* freeStyle = (Mason_Freestyle*) ssGetPWork(S)[3];
   //y[2] = freeStyle->respondToAcc(accX,accY,accZ);
    
    //y[1] = potValue*0.5*errDerivative;
    //y[0] = potValue*(stroke->playMode(2));
                                                                         
    //The actual output behavior is affected greatly by the kp and kd values. which is now being set manually. There should be some kind of correlation between the patterns, the tempo, the dynamic level targetted and the kp and kd values. 
    
    //Periodic* stroke = (Periodic*) ssGetPWork(S)[2];
    //y[0] = stroke->outputRamp();
    UNUSED_ARG(tid);                             
}                                                

#ifdef MATLAB_MEX_FILE
/* For now mdlG[S]etSimState are only supported in normal simulation */

/* Define to indicate that this S-Function has the mdlG[S]etSimState mothods */
#define MDL_SIM_STATE

/* Function: mdlGetSimState =====================================================
 * Abstract:
 *
 */

static mxArray* mdlGetSimState(SimStruct* S)
{
    
    counter* c = (counter*) ssGetPWork(S)[0];
    mxArray* outSS = mxCreateDoubleMatrix(1,1,mxREAL);
    mxGetPr(outSS)[0] = c->getX();
    return outSS;
     
}

static void mdlSetSimState(SimStruct* S, const mxArray* ma)
{
   
    counter* c = (counter*) ssGetPWork(S)[0];
    c->setX(mxGetPr(ma)[0]);
    
}



#endif


/* Function: mdlTerminate =====================================================
 * Abstract:
 *    In this function, you should perform any actions that are necessary
 *    at the termination of a simulation.  For example, if memory was
 *    allocated in mdlStart, this is the place to free it.
 */
static void mdlTerminate(SimStruct *S)
{

    counter *c = (counter *) ssGetPWork(S)[0];
    delete c;
    Main_Control *mainControl = (Main_Control *) ssGetPWork(S)[1];
    delete mainControl;                                  // object in the termination
 
}                                              // function
/*======================================================*
 * See sfuntmpl.doc for the optional S-function methods *
 *======================================================*/

/*=============================*
 * Required S-function trailer *
 *=============================*/

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif

