<?xml version='1.0' encoding='ISO-8859-1' ?>
<MSR-TASKBROWSER>
  <Main Host='localhost' Port='2345' ResetMessagesParameter='/Control/Reset' Messagemask='*' LangId='' ResetMessagesParameterinc='false' MessageDefFile='' autoconnect='false'>--------------------- 2013-11-05, 16:43:06  meka@meka-maratime,TestManager (dev) ---------------------

added velocity filter cutoff


--------------------- 2013-11-07, 10:33:04  meka@meka-maratime,TestManager (dev) ---------------------

cleaning up

--------------------- 2013-11-07, 11:22:55  meka@meka-maratime,TestManager (dev) ---------------------

clean up more!

--------------------- 2013-11-07, 18:32:33  meka@meka-maratime,TestManager (dev) ---------------------

Cleaned up layout for delivery and control from one tab

--------------------- 2013-11-08, 08:36:45  meka@meka-maratime,TestManager (dev) ---------------------

Finalized Layout
    <CustomColormap>ColorA=C8D0D4
ColorB=E1FFFF
ColorC=A56E3A
ColorD=FFFFFF
ColorE=C8D0D4
ColorF=FFFFFFFF
ColorG=FFFFFFFF
ColorH=FFFFFFFF
ColorI=FFFFFFFF
ColorJ=FFFFFFFF
ColorK=FFFFFFFF
ColorL=FFFFFFFF
ColorM=FFFFFFFF
ColorN=FFFFFFFF
ColorO=FFFFFFFF
ColorP=FFFFFFFF
</CustomColormap>
    <Controlbar>
      <Connection visible='true' top='2' left='11' width='214'/>
      <Buttons visible='true' top='2' left='238' width='643'/>
      <Messages visible='false' top='36' left='11' width='780'/>
      <ParameterEdit visible='true' top='2' left='894' width='380'/>
      <Treeview visible='true' width='349'/>
    </Controlbar>
  </Main>
  <Layout Editing='true' ActivePage='0'>
    <Tab tabname='Drum-Bench Control Motor 0' Backgroundimagedata='' Backgroundimagecompression='1' Backgroundimagetransparent='false' ActivationChannel='' ActivationTrigger='0' isform='false'>
      <Component Classname='tdyntext' Width='290' Height='140' Top='25' Left='395' Movepanel='' Ueberallsichtbar='false' Color='8421504' Alignment='0' withbevel='true' VisiblityControlChannel='' Caption=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdyntext' Width='385' Height='185' Top='5' Left='1060' Movepanel='' Ueberallsichtbar='false' Color='8421504' Alignment='0' withbevel='true' VisiblityControlChannel='' Caption=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdyntext' Width='360' Height='185' Top='5' Left='690' Movepanel='' Ueberallsichtbar='false' Color='8421504' Alignment='0' withbevel='true' VisiblityControlChannel='' Caption=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdyntext' Width='350' Height='35' Top='10' Left='695' Movepanel='' Ueberallsichtbar='false' Color='65408' Alignment='2' withbevel='true' VisiblityControlChannel='' Caption='Impedence Control'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdynradiogroup' Width='95' Height='100' Top='60' Left='400' Movepanel='/param/config/input/input0/quadrature_1_angle/select_input' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Input Type' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' itemlist='0:Constant,1:Sine,2:Square' showvalues='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input0/quadrature_1_angle/select_input'/>
      </Component>
      <Component Classname='tdynedit' Width='180' Height='40' Top='90' Left='500' Movepanel='/param/config/input/input0/quadrature_1_angle/frequency' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Freq. (rad./s)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='0' upperlimit='70' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input0/quadrature_1_angle/frequency'/>
      </Component>
      <Component Classname='tdynedit' Width='180' Height='40' Top='60' Left='500' Movepanel='/param/config/input/input0/quadrature_1_angle/amplitude' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Amp. (rad.)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='-1.5' upperlimit='1.5' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input0/quadrature_1_angle/amplitude'/>
      </Component>
      <Component Classname='tdynedit' Width='240' Height='40' Top='25' Left='145' Movepanel='/param/config/input/input0/quadrature_1_angle/bias' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Nominal Position Bias' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='10' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input0/quadrature_1_angle/bias'/>
      </Component>
      <Component Classname='tdynedit' Width='180' Height='37' Top='120' Left='500' Movepanel='/param/config/input/input0/quadrature_1_angle/slew_rate' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Slew Rate' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input0/quadrature_1_angle/slew_rate'/>
      </Component>
      <Component Classname='tdynmultiscroll' Width='635' Height='340' Top='215' Left='5' Movepanel='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1' Caption='' Label='Motor Position - Quadrature_1' Unit='' Schleppzeiger='true' Shownames='true' Showalias='false' Showunit='false' Showlimits='true' Precision='4' Digits='3' Fullname='false' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='5' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/robot/Actuator/Actuator direct/output_angle1'>
          <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='16711680'/>
          </Farbliste>
        </Instrument>
      </Component>
      <Component Classname='tdynmultiscroll' Width='635' Height='340' Top='555' Left='5' Movepanel='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1_dot,/signals/impedence_controller/Filtered Derivative' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1_dot,/signals/impedence_controller/Filtered Derivative' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='false' Showunit='false' Showlimits='false' Precision='4' Digits='3' Fullname='false' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='5' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/robot/signals/output/quadrature_1_dot/0'>
          <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1_dot,/signals/impedence_controller/Filtered Derivative'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1_dot'/>
            <Kanal Name='/signals/impedence_controller/Filtered Derivative'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='16711680'/>
            <Farbe Wert='255'/>
          </Farbliste>
        </Instrument>
      </Component>
      <Component Classname='tdynradiogroup' Width='240' Height='100' Top='75' Left='145' Movepanel='/param/config/control0/mode_select' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Drum Bench Mode' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' itemlist='1: Pass Thru, 2: Impedence Control, 3: Variable Impedence Control' showvalues='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/mode_select'/>
      </Component>
      <Component Classname='tdynhortrack' Width='175' Height='115' Top='40' Left='695' Movepanel='/param/config/control0/impedence_control/kp' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Joint Stiffness (kp)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='0' upperlimit='1.5' precision='15' hideedit='false' hiderange='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/impedence_control/kp'/>
      </Component>
      <Component Classname='tdynhortrack' Width='170' Height='115' Top='40' Left='875' Movepanel='/param/config/control0/impedence_control/kd' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Joint Damping (kd)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='0' upperlimit='0.1' precision='15' hideedit='false' hiderange='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/impedence_control/kd'/>
      </Component>
      <Component Classname='tdynhortrack' Width='190' Height='120' Top='35' Left='1250' Movepanel='/param/config/control0/var_impedence_control/kd' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Joint Damping (kd)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='0' upperlimit='0.05' precision='15' hideedit='false' hiderange='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/var_impedence_control/kd'/>
      </Component>
      <Component Classname='tdyntext' Width='375' Height='35' Top='10' Left='1065' Movepanel='' Ueberallsichtbar='false' Color='33023' Alignment='2' withbevel='true' VisiblityControlChannel='' Caption='*Variable Impedence Control (Need to setup before use!)*'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdynedit' Width='275' Height='40' Top='145' Left='740' Movepanel='/param/config/control0/impedence_control/imp_cutoff' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Velocity Filter Cutoff (rad/s.)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/impedence_control/imp_cutoff'/>
      </Component>
      <Component Classname='tdynedit' Width='270' Height='40' Top='145' Left='1110' Movepanel='/param/config/control0/var_impedence_control/var_imp_cutoff' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Velocity Filter Cutoff (rad/s.)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/var_impedence_control/var_imp_cutoff'/>
      </Component>
      <Component Classname='tdynedit' Width='180' Height='37' Top='50' Left='1065' Movepanel='/param/config/control0/var_impedence_control/ka' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='ka' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/var_impedence_control/ka'/>
      </Component>
      <Component Classname='tdynedit' Width='180' Height='37' Top='85' Left='1065' Movepanel='/param/config/control0/var_impedence_control/bias' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Sensor Bias' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/var_impedence_control/bias'/>
      </Component>
      <Component Classname='tdynradiogroup' Width='130' Height='200' Top='5' Left='5' Movepanel='/param/config/input/input0/mode' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Setup Mode' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' itemlist='0:*Open*,1:brake,2:*Lock*,3:current,4:voltage,5:output torque,6:motor theta,7:full state,8:motor torque,9:voltage sweep' showvalues='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input0/mode'/>
      </Component>
      <Component Classname='tdyntext' Width='280' Height='35' Top='30' Left='400' Movepanel='' Ueberallsichtbar='false' Color='8421631' Alignment='2' withbevel='true' VisiblityControlChannel='' Caption='Input Generator'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdynmultiscroll' Width='800' Height='680' Top='215' Left='655' Movepanel='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_y,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_z' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_y,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_z' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='false' Showunit='false' Showlimits='false' Precision='5' Digits='0' Fullname='false' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='5' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x'>
          <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_y,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_z'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x'/>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_y'/>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_z'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='16711680'/>
            <Farbe Wert='255'/>
            <Farbe Wert='32768'/>
          </Farbliste>
        </Instrument>
      </Component>
    </Tab>
    <Tab tabname='Drum-Bench Control Motor 1' Backgroundimagedata='' Backgroundimagecompression='1' Backgroundimagetransparent='false' ActivationChannel='' ActivationTrigger='0' isform='false'>
      <Component Classname='tdyntext' Width='290' Height='140' Top='25' Left='395' Movepanel='' Ueberallsichtbar='false' Color='8421504' Alignment='0' withbevel='true' VisiblityControlChannel='' Caption=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdyntext' Width='385' Height='185' Top='5' Left='1060' Movepanel='' Ueberallsichtbar='false' Color='8421504' Alignment='0' withbevel='true' VisiblityControlChannel='' Caption=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdyntext' Width='360' Height='185' Top='5' Left='690' Movepanel='' Ueberallsichtbar='false' Color='8421504' Alignment='0' withbevel='true' VisiblityControlChannel='' Caption=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdyntext' Width='350' Height='35' Top='10' Left='695' Movepanel='' Ueberallsichtbar='false' Color='65408' Alignment='2' withbevel='true' VisiblityControlChannel='' Caption='Impedence Control'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdynradiogroup' Width='95' Height='100' Top='60' Left='400' Movepanel='/param/config/input/input1/quadrature_1_angle/select_input' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Input Type' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' itemlist='0:Constant,1:Sine,2:Square' showvalues='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input1/quadrature_1_angle/select_input'/>
      </Component>
      <Component Classname='tdynedit' Width='180' Height='40' Top='90' Left='500' Movepanel='/param/config/input/input1/quadrature_1_angle/frequency' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Freq. (rad./s)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='0' upperlimit='70' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input1/quadrature_1_angle/frequency'/>
      </Component>
      <Component Classname='tdynedit' Width='180' Height='40' Top='60' Left='500' Movepanel='/param/config/input/input1/quadrature_1_angle/amplitude' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Amp. (rad.)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='-1.5' upperlimit='1.5' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input1/quadrature_1_angle/amplitude'/>
      </Component>
      <Component Classname='tdynedit' Width='240' Height='40' Top='25' Left='145' Movepanel='/param/config/input/input1/quadrature_1_angle/bias' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Nominal Position Bias' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='10' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input1/quadrature_1_angle/bias'/>
      </Component>
      <Component Classname='tdynedit' Width='180' Height='37' Top='120' Left='500' Movepanel='/param/config/input/input1/quadrature_1_angle/slew_rate' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Slew Rate' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input1/quadrature_1_angle/slew_rate'/>
      </Component>
      <Component Classname='tdynmultiscroll' Width='635' Height='340' Top='215' Left='5' Movepanel='/signals/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1' Caption='' Label='Motor Position - Quadrature_1' Unit='' Schleppzeiger='true' Shownames='true' Showalias='false' Showunit='false' Showlimits='true' Precision='4' Digits='3' Fullname='false' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='5' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/robot/Actuator/Actuator direct/output_angle1'>
          <Kanal Name='/signals/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='255'/>
          </Farbliste>
        </Instrument>
      </Component>
      <Component Classname='tdynmultiscroll' Width='635' Height='340' Top='555' Left='5' Movepanel='/signals/impedence_controller1/Filtered Derivative,/signals/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1_dot' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/impedence_controller1/Filtered Derivative,/signals/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1_dot' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='false' Showunit='false' Showlimits='false' Precision='4' Digits='3' Fullname='false' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='5' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/robot/signals/output/quadrature_1_dot/0'>
          <Kanal Name='/signals/impedence_controller1/Filtered Derivative,/signals/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1_dot'/>
          <Kanaele>
            <Kanal Name='/signals/impedence_controller1/Filtered Derivative'/>
            <Kanal Name='/signals/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1_dot'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='32768'/>
            <Farbe Wert='0'/>
          </Farbliste>
        </Instrument>
      </Component>
      <Component Classname='tdynradiogroup' Width='240' Height='100' Top='75' Left='145' Movepanel='/param/config/control1/mode_select' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Drum Bench Mode' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' itemlist='1: Pass Thru, 2: Impedence Control, 3: Variable Impedence Control' showvalues='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control1/mode_select'/>
      </Component>
      <Component Classname='tdynhortrack' Width='175' Height='115' Top='40' Left='695' Movepanel='/param/config/control1/impedence_control/kp' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Joint Stiffness (kp)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='0' upperlimit='1.5' precision='15' hideedit='false' hiderange='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control1/impedence_control/kp'/>
      </Component>
      <Component Classname='tdynhortrack' Width='170' Height='115' Top='40' Left='875' Movepanel='/param/config/control1/impedence_control/kd' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Joint Damping (kd)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='0' upperlimit='.5' precision='15' hideedit='false' hiderange='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control1/impedence_control/kd'/>
      </Component>
      <Component Classname='tdynhortrack' Width='190' Height='120' Top='35' Left='1250' Movepanel='/param/config/control0/var_impedence_control/kd' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Joint Damping (kd)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='0' upperlimit='0.05' precision='15' hideedit='false' hiderange='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/var_impedence_control/kd'/>
      </Component>
      <Component Classname='tdyntext' Width='375' Height='35' Top='10' Left='1065' Movepanel='' Ueberallsichtbar='false' Color='33023' Alignment='2' withbevel='true' VisiblityControlChannel='' Caption='*Variable Impedence Control (Need to setup before use!)*'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdynedit' Width='275' Height='40' Top='145' Left='740' Movepanel='/param/config/control0/impedence_control/imp_cutoff' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Velocity Filter Cutoff (rad/s.)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/impedence_control/imp_cutoff'/>
      </Component>
      <Component Classname='tdynedit' Width='270' Height='40' Top='145' Left='1110' Movepanel='/param/config/control0/var_impedence_control/var_imp_cutoff' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Velocity Filter Cutoff (rad/s.)' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/var_impedence_control/var_imp_cutoff'/>
      </Component>
      <Component Classname='tdynedit' Width='180' Height='37' Top='50' Left='1065' Movepanel='/param/config/control0/impedence_control/imp_cutoff' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='ka' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/impedence_control/imp_cutoff'/>
      </Component>
      <Component Classname='tdynedit' Width='180' Height='37' Top='85' Left='1065' Movepanel='/param/config/control0/var_impedence_control/bias' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Sensor Bias' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/control0/var_impedence_control/bias'/>
      </Component>
      <Component Classname='tdynradiogroup' Width='130' Height='200' Top='5' Left='5' Movepanel='/param/config/input/input1/mode' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Setup Mode' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' itemlist='0:*Open*,1:brake,2:*Lock*,3:current,4:voltage,5:output torque,6:motor theta,7:full state,8:motor torque,9:voltage sweep' showvalues='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input1/mode'/>
      </Component>
      <Component Classname='tdyntext' Width='280' Height='35' Top='30' Left='400' Movepanel='' Ueberallsichtbar='false' Color='8421631' Alignment='2' withbevel='true' VisiblityControlChannel='' Caption='Input Generator'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
      </Component>
      <Component Classname='tdynmultiscroll' Width='800' Height='680' Top='215' Left='655' Movepanel='/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_x,/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_y,/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_z' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_x,/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_y,/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_z' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='false' Showunit='false' Showlimits='false' Precision='5' Digits='0' Fullname='false' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='5' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x'>
          <Kanal Name='/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_x,/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_y,/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_z'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_x'/>
            <Kanal Name='/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_y'/>
            <Kanal Name='/signals/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_z'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='0'/>
            <Farbe Wert='16744576'/>
            <Farbe Wert='16639'/>
          </Farbliste>
        </Instrument>
      </Component>
    </Tab>
    <Tab tabname='General' Backgroundimagedata='' Backgroundimagecompression='1' Backgroundimagetransparent='false' ActivationChannel='' ActivationTrigger='0' isform='false'>
      <Component Classname='tdynmultiscroll' Width='520' Height='395' Top='5' Left='225' Movepanel='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_iq' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_iq' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='false' Showunit='false' Showlimits='true' Precision='6' Digits='3' Fullname='false' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='1' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/robot/piccolo/0'>
          <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_iq'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_iq'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='16711680'/>
          </Farbliste>
        </Instrument>
      </Component>
      <Component Classname='tdynnumeric' Width='95' Height='50' Top='250' Left='60' Movepanel='/signals/marlin_1.1_full/marlin_1.1_basic/marlin ec/debug_' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='Arial' Size='8' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full/marlin_1.1_basic/marlin ec/debug_' MinValue='-8' MaxValue='8' Upperlimit='8' Lowerlimit='-8' Caption='debug_(debug)' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='true' Showunit='true' Showlimits='false' Precision='6' Digits='0' Fullname='false' Autoscale='false' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='-2147483624' Lowercolor='-2147483624' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' ValueasDate='false' ValueasTime='false' ValueasHex='false' ValueasBin='false' Showvalue='true' SelectText='false' Textlist='' Textalign='0' Fontsizeautomatic='true'>
          <Kanal Name='/signals/marlin_1.1_full/marlin_1.1_basic/marlin ec/debug_'/>
        </Instrument>
      </Component>
      <Component Classname='tdynradiogroup' Width='210' Height='250' Top='5' Left='5' Movepanel='/param/config/input/input/mode' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Setup Mode' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' itemlist='0:*Open*,1:brake,2:*Lock*,3:current,4:voltage,5:output torque,6:motor theta,7:full state,8:motor torque,9:voltage sweep' showvalues='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input/mode'/>
      </Component>
      <Component Classname='tdynmultiscroll' Width='415' Height='380' Top='420' Left='235' Movepanel='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_vq_avg' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_vq_avg' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='false' Showunit='false' Showlimits='true' Precision='6' Digits='3' Fullname='false' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='1' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/robot/piccolo/0'>
          <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_vq_avg'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_vq_avg'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='16711680'/>
          </Farbliste>
        </Instrument>
      </Component>
      <Component Classname='tdynmultiscroll' Width='540' Height='380' Top='420' Left='655' Movepanel='/signals/marlin_1.1_full status/marlin_1.1_basic status/bus_v' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/bus_v' MinValue='0' MaxValue='45' Upperlimit='40' Lowerlimit='5' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='false' Showunit='false' Showlimits='true' Precision='6' Digits='3' Fullname='false' Autoscale='false' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='10' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/robot/signals/actuator_chain/Actuator/Actuator/Actuator direct/bus_v_'>
          <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/bus_v'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/bus_v'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='16711680'/>
          </Farbliste>
        </Instrument>
      </Component>
      <Component Classname='tdynmultiscroll' Width='495' Height='390' Top='5' Left='765' Movepanel='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='false' Showunit='false' Showlimits='true' Precision='6' Digits='3' Fullname='false' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='10' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/robot/piccolo/0'>
          <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='255'/>
          </Farbliste>
        </Instrument>
      </Component>
    </Tab>
    <Tab tabname='Accelerometer' Backgroundimagedata='' Backgroundimagecompression='1' Backgroundimagetransparent='false' ActivationChannel='' ActivationTrigger='0' isform='false'>
      <Component Classname='tdynmultiscroll' Width='1405' Height='875' Top='10' Left='15' Movepanel='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_y,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_z' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_y,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_z' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='true' Showunit='false' Showlimits='false' Precision='5' Digits='2' Fullname='true' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='2' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x'>
          <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_y,/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_z'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x'/>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_y'/>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_z'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='16711680'/>
            <Farbe Wert='255'/>
            <Farbe Wert='32768'/>
          </Farbliste>
        </Instrument>
      </Component>
    </Tab>
    <Tab tabname='Potentiometer' Backgroundimagedata='' Backgroundimagecompression='1' Backgroundimagetransparent='false' ActivationChannel='' ActivationTrigger='0' isform='false'>
      <Component Classname='tdynscroll' Width='700' Height='560' Top='45' Left='30' Movepanel='/signals/marlin_1.1_full status/marlin_1.1_basic status/analog_1' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/analog_1' Caption='/signals/marlin_1.1_full status/marlin_1.1_basic status/analog_1' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='true' Showunit='false' Showlimits='false' Precision='5' Digits='2' Fullname='true' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='10' Grid='true' DoTrigger='false'>
          <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/analog_1'/>
        </Instrument>
      </Component>
    </Tab>
    <Tab tabname='param' Backgroundimagedata='' Backgroundimagecompression='1' Backgroundimagetransparent='false' ActivationChannel='' ActivationTrigger='0' isform='false'>
      <Component Classname='tdynbtn' Width='215' Height='35' Top='20' Left='5' Movepanel='/param/config/input/input0/rpc_command' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='send param' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' downvalue='1' upvalue='0'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input0/rpc_command'/>
      </Component>
      <Component Classname='tdynnumeric' Width='90' Height='45' Top='70' Left='5' Movepanel='/signals/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1/2' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='Arial' Size='8' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1/2' MinValue='-8' MaxValue='8' Upperlimit='8' Lowerlimit='-8' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='false' Showalias='true' Showunit='false' Showlimits='false' Precision='3' Digits='2' Fullname='false' Autoscale='false' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='-2147483624' Lowercolor='-2147483624' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' ValueasDate='false' ValueasTime='false' ValueasHex='false' ValueasBin='false' Showvalue='true' SelectText='false' Textlist='' Textalign='0' Fontsizeautomatic='true'>
          <Kanal Name='/signals/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1/2'/>
        </Instrument>
      </Component>
      <Component Classname='tdynbtn' Width='215' Height='35' Top='45' Left='5' Movepanel='/param/config/input/input0/rpc_command' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='save param' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' downvalue='2' upvalue='0'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input0/rpc_command'/>
      </Component>
      <Component Classname='tdynbtn' Width='215' Height='35' Top='175' Left='125' Movepanel='/param/config/input/input_extra/trigger_yaml_write' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='0' Label='send param' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' downvalue='1' upvalue='0'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input_extra/trigger_yaml_write'/>
      </Component>
      <Component Classname='tdynbtn' Width='215' Height='35' Top='15' Left='275' Movepanel='/param/config/input/input1/rpc_command' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='send param' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' downvalue='1' upvalue='0'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input1/rpc_command'/>
      </Component>
      <Component Classname='tdynnumeric' Width='90' Height='45' Top='65' Left='275' Movepanel='/signals/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1/2' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='Arial' Size='8' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1/2' MinValue='-8' MaxValue='8' Upperlimit='8' Lowerlimit='-8' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='false' Showalias='true' Showunit='false' Showlimits='false' Precision='3' Digits='2' Fullname='false' Autoscale='false' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='-2147483624' Lowercolor='-2147483624' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' ValueasDate='false' ValueasTime='false' ValueasHex='false' ValueasBin='false' Showvalue='true' SelectText='false' Textlist='' Textalign='0' Fontsizeautomatic='true'>
          <Kanal Name='/signals/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1/2'/>
        </Instrument>
      </Component>
      <Component Classname='tdynbtn' Width='215' Height='35' Top='40' Left='275' Movepanel='/param/config/input/input1/rpc_command' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='save param' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' downvalue='2' upvalue='0'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input1/rpc_command'/>
      </Component>
    </Tab>
    <Tab tabname='motor current control' Backgroundimagedata='' Backgroundimagecompression='1' Backgroundimagetransparent='false' ActivationChannel='' ActivationTrigger='0' isform='false'>
      <Component Classname='tdynradiogroup' Width='185' Height='130' Top='5' Left='195' Movepanel='/param/config/input/input/motor_iq/select_input' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Input' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' itemlist='0:Constant,1:Sine,2:Square' showvalues='true'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input/motor_iq/select_input'/>
      </Component>
      <Component Classname='tdynedit' Width='215' Height='40' Top='155' Left='195' Movepanel='/param/config/input/input/motor_iq/frequency' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Freq' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input/motor_iq/frequency'/>
      </Component>
      <Component Classname='tdynedit' Width='215' Height='40' Top='125' Left='195' Movepanel='/param/config/input/input/motor_iq/amplitude' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Amplitude' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input/motor_iq/amplitude'/>
      </Component>
      <Component Classname='tdynedit' Width='215' Height='40' Top='185' Left='195' Movepanel='/param/config/input/input/motor_iq/bias' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='Bias' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input/motor_iq/bias'/>
      </Component>
      <Component Classname='tdynedit' Width='185' Height='37' Top='215' Left='195' Movepanel='/param/config/input/input/motor_iq/slew_rate' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='slew' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='92' lowerlimit='' upperlimit='' precision='15' isstring='false'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input/motor_iq/slew_rate'/>
      </Component>
      <Component Classname='tdynmultiscroll' Width='545' Height='340' Top='250' Left='10' Movepanel='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_iq' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_iq' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='true' Showunit='false' Showlimits='true' Precision='6' Digits='3' Fullname='true' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='2' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/robot/Actuator/Actuator direct/output_angle1'>
          <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_iq'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full status/marlin_1.1_basic status/motor_iq'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='16711680'/>
          </Farbliste>
        </Instrument>
      </Component>
    </Tab>
    <Tab tabname='exec' Backgroundimagedata='' Backgroundimagecompression='1' Backgroundimagetransparent='false' ActivationChannel='' ActivationTrigger='0' isform='false'>
      <Component Classname='tdynmultiscroll' Width='300' Height='212' Top='0' Left='0' Movepanel='/Taskinfo/0/ExecTime' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/Taskinfo/0/ExecTime' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='true' Showunit='false' Showlimits='false' Precision='6' Digits='6' Fullname='true' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='60' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/Taskinfo/0/ExecTime'>
          <Kanal Name='/Taskinfo/0/ExecTime'/>
          <Kanaele>
            <Kanal Name='/Taskinfo/0/ExecTime'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='16711680'/>
          </Farbliste>
        </Instrument>
      </Component>
      <Component Classname='tdynmultiscroll' Width='300' Height='212' Top='0' Left='305' Movepanel='/Taskinfo/0/Period' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/Taskinfo/0/Period' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='true' Showunit='false' Showlimits='false' Precision='9' Digits='9' Fullname='true' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='5' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/Taskinfo/0/ExecTime'>
          <Kanal Name='/Taskinfo/0/Period'/>
          <Kanaele>
            <Kanal Name='/Taskinfo/0/Period'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='255'/>
          </Farbliste>
        </Instrument>
      </Component>
      <Component Classname='tdynmultiscroll' Width='300' Height='212' Top='205' Left='0' Movepanel='/Taskinfo/0/Period,/Taskinfo/0/ExecTime' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/Taskinfo/0/Period,/Taskinfo/0/ExecTime' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='true' Showunit='false' Showlimits='false' Precision='9' Digits='9' Fullname='true' Autoscale='true' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='60' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/Taskinfo/0/ExecTime'>
          <Kanal Name='/Taskinfo/0/Period,/Taskinfo/0/ExecTime'/>
          <Kanaele>
            <Kanal Name='/Taskinfo/0/Period'/>
            <Kanal Name='/Taskinfo/0/ExecTime'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='255'/>
            <Farbe Wert='16711680'/>
          </Farbliste>
        </Instrument>
      </Component>
    </Tab>
    <Tab tabname='safety' Backgroundimagedata='' Backgroundimagecompression='1' Backgroundimagetransparent='false' ActivationChannel='' ActivationTrigger='0' isform='false'>
      <Component Classname='tdynbtn' Width='185' Height='37' Top='0' Left='0' Movepanel='/param/config/input/input/rpc_command' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel='' Labelmode='2' Label='check safety' enablechannel='' operandindex='0' Operandvalue='0' SkaleFaktor='1' Editwidth='0' lowerlimit='' upperlimit='' precision='15' downvalue='3' upvalue='0'>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Parameter Name='/param/config/input/input/rpc_command'/>
      </Component>
      <Component Classname='tdynmultibar' Width='980' Height='250' Top='50' Left='0' Movepanel='/signals/marlin_1.1_full/rpc_generator/check safety/analog_limit,/signals/marlin_1.1_full/rpc_generator/check safety/ethercat,/signals/marlin_1.1_full/rpc_generator/check safety/external_panic,/signals/marlin_1.1_full/rpc_generator/check safety/motor encoder,/signals/marlin_1.1_full/rpc_generator/check safety/over_current,/signals/marlin_1.1_full/rpc_generator/check safety/over_voltage,/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_1_high,/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_1_low,/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_2_limit,/signals/marlin_1.1_full/rpc_generator/check safety/under_voltage' Ueberallsichtbar='false' Color='-2147483633' Alignment='2' withbevel='true' VisiblityControlChannel=''>
        <Font Name='MS Sans Serif' Size='10' Style='0' Color='-2147483640'/>
        <Instrument Untersetzung='1' Kanal='/signals/marlin_1.1_full/rpc_generator/check safety/analog_limit,/signals/marlin_1.1_full/rpc_generator/check safety/ethercat,/signals/marlin_1.1_full/rpc_generator/check safety/external_panic,/signals/marlin_1.1_full/rpc_generator/check safety/motor encoder,/signals/marlin_1.1_full/rpc_generator/check safety/over_current,/signals/marlin_1.1_full/rpc_generator/check safety/over_voltage,/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_1_high,/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_1_low,/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_2_limit,/signals/marlin_1.1_full/rpc_generator/check safety/under_voltage' MinValue='-2' MaxValue='2' Upperlimit='2' Lowerlimit='-2' Caption='' Label='' Unit='' Schleppzeiger='true' Shownames='true' Showalias='false' Showunit='false' Showlimits='false' Precision='3' Digits='0' Fullname='false' Autoscale='false' Backcolor='-2147483624' Gridcolor='9474303' Axiscolor='0' Uppercolor='255' Lowercolor='65535' Normcolor='65280' FilterCnt='1' Showaxis='true' Autoaxis='true' Messzeit='60' Grid='true' DoTrigger='false' Stacky='0' triggerkanal='/robot/Actuator/Actuator direct/rpc/check safety/overcurrent' Stack='false'>
          <Kanal Name='/signals/marlin_1.1_full/rpc_generator/check safety/analog_limit,/signals/marlin_1.1_full/rpc_generator/check safety/ethercat,/signals/marlin_1.1_full/rpc_generator/check safety/external_panic,/signals/marlin_1.1_full/rpc_generator/check safety/motor encoder,/signals/marlin_1.1_full/rpc_generator/check safety/over_current,/signals/marlin_1.1_full/rpc_generator/check safety/over_voltage,/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_1_high,/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_1_low,/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_2_limit,/signals/marlin_1.1_full/rpc_generator/check safety/under_voltage'/>
          <Kanaele>
            <Kanal Name='/signals/marlin_1.1_full/rpc_generator/check safety/analog_limit'/>
            <Kanal Name='/signals/marlin_1.1_full/rpc_generator/check safety/ethercat'/>
            <Kanal Name='/signals/marlin_1.1_full/rpc_generator/check safety/external_panic'/>
            <Kanal Name='/signals/marlin_1.1_full/rpc_generator/check safety/motor encoder'/>
            <Kanal Name='/signals/marlin_1.1_full/rpc_generator/check safety/over_current'/>
            <Kanal Name='/signals/marlin_1.1_full/rpc_generator/check safety/over_voltage'/>
            <Kanal Name='/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_1_high'/>
            <Kanal Name='/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_1_low'/>
            <Kanal Name='/signals/marlin_1.1_full/rpc_generator/check safety/quadrature_2_limit'/>
            <Kanal Name='/signals/marlin_1.1_full/rpc_generator/check safety/under_voltage'/>
          </Kanaele>
          <Farbliste>
            <Farbe Wert='16711680'/>
            <Farbe Wert='255'/>
            <Farbe Wert='32768'/>
            <Farbe Wert='0'/>
            <Farbe Wert='16744576'/>
            <Farbe Wert='16639'/>
            <Farbe Wert='65280'/>
            <Farbe Wert='8421504'/>
            <Farbe Wert='12632256'/>
            <Farbe Wert='16711680'/>
          </Farbliste>
        </Instrument>
      </Component>
    </Tab>
  </Layout>
  <Server>
    <Kanaele/>
    <Formelkanaele/>
  </Server>
  <ParameterMask/>
  <Access Admin='D41D8CD98F00B204E9800998ECF8427E'/>
  <Watchdog Channel='kanal_box' Parameter='parameter_box' Interval='1000' Enabled='false' UseConId='false'/>
  <Messageform>
    <Cols>
      <Col index='0' width='300'/>
      <Col index='1' width='80'/>
      <Col index='2' width='80'/>
      <Col index='3' width='80'/>
      <Col index='4' width='73'/>
    </Cols>
  </Messageform>
</MSR-TASKBROWSER>