#ifndef RTW_HEADER_struct_Ff8qvljR6PDUJi6EFt21oB_h_
#define RTW_HEADER_struct_Ff8qvljR6PDUJi6EFt21oB_h_
#include "rtwtypes.h"

typedef struct {
  real_T kp;
  real_T kd;
  real_T imp_cutoff;
} struct_Ff8qvljR6PDUJi6EFt21oB;

#endif                                 /* RTW_HEADER_struct_Ff8qvljR6PDUJi6EFt21oB_h_ */
