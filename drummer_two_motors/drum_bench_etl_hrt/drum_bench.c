/*
 * drum_bench.c
 *
 * Code generation for model "drum_bench".
 *
 * Model version              : 1.526
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Mon Jul 21 13:57:13 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "drum_bench_capi.h"
#include "drum_bench.h"
#include "drum_bench_private.h"

/* S-Function Block: <S95>/marlin_ec
 * Mapped Pdos for block M3EC
 */
static struct pdo_map pdo_map_1[] = {
  { 0x7000, 1, 1032, 0, 0, &drum_bench_B.mode_a },/* In1[0] */
  { 0x7000, 2, 1008, 0, 0, &drum_bench_B.command_e[0] },/* In2[0] */
  { 0x7000, 2, 1008, 0, 1, &drum_bench_B.command_e[1] },/* In2[1] */
  { 0x7000, 2, 1008, 0, 2, &drum_bench_B.command_e[2] },/* In2[2] */
  { 0x7000, 2, 1008, 0, 3, &drum_bench_B.command_e[3] },/* In2[3] */
  { 0x7000, 2, 1008, 0, 4, &drum_bench_B.command_e[4] },/* In2[4] */
  { 0x7000, 2, 1008, 0, 5, &drum_bench_B.command_e[5] },/* In2[5] */
  { 0x7000, 2, 1008, 0, 6, &drum_bench_B.command_e[6] },/* In2[6] */
  { 0x7000, 2, 1008, 0, 7, &drum_bench_B.command_e[7] },/* In2[7] */
  { 0x7000, 2, 1008, 0, 8, &drum_bench_B.command_e[8] },/* In2[8] */
  { 0x7000, 2, 1008, 0, 9, &drum_bench_B.command_e[9] },/* In2[9] */
  { 0x7000, 2, 1008, 0, 10, &drum_bench_B.command_e[10] },/* In2[10] */
  { 0x7000, 2, 1008, 0, 11, &drum_bench_B.command_e[11] },/* In2[11] */
  { 0x7000, 2, 1008, 0, 12, &drum_bench_B.command_e[12] },/* In2[12] */
  { 0x7000, 2, 1008, 0, 13, &drum_bench_B.command_e[13] },/* In2[13] */
  { 0x7000, 2, 1008, 0, 14, &drum_bench_B.command_e[14] },/* In2[14] */
  { 0x7000, 2, 1008, 0, 15, &drum_bench_B.command_e[15] },/* In2[15] */
  { 0x7000, 2, 1008, 0, 16, &drum_bench_B.command_e[16] },/* In2[16] */
  { 0x7000, 2, 1008, 0, 17, &drum_bench_B.command_e[17] },/* In2[17] */
  { 0x7000, 2, 1008, 0, 18, &drum_bench_B.command_e[18] },/* In2[18] */
  { 0x7000, 2, 1008, 0, 19, &drum_bench_B.command_e[19] },/* In2[19] */
  { 0x7000, 2, 1008, 0, 20, &drum_bench_B.command_e[20] },/* In2[20] */
  { 0x7000, 2, 1008, 0, 21, &drum_bench_B.command_e[21] },/* In2[21] */
  { 0x7000, 2, 1008, 0, 22, &drum_bench_B.command_e[22] },/* In2[22] */
  { 0x7000, 2, 1008, 0, 23, &drum_bench_B.command_e[23] },/* In2[23] */
  { 0x7000, 2, 1008, 0, 24, &drum_bench_B.command_e[24] },/* In2[24] */
  { 0x7000, 2, 1008, 0, 25, &drum_bench_B.command_e[25] },/* In2[25] */
  { 0x7000, 2, 1008, 0, 26, &drum_bench_B.command_e[26] },/* In2[26] */
  { 0x7000, 2, 1008, 0, 27, &drum_bench_B.command_e[27] },/* In2[27] */
  { 0x7000, 3, 1008, 0, 0, &drum_bench_B.command_e[28] },/* In3[0] */
  { 0x7000, 3, 1008, 0, 1, &drum_bench_B.command_e[29] },/* In3[1] */
  { 0x7000, 3, 1008, 0, 2, &drum_bench_B.command_e[30] },/* In3[2] */
  { 0x7000, 3, 1008, 0, 3, &drum_bench_B.command_e[31] },/* In3[3] */
  { 0x7000, 3, 1008, 0, 4, &drum_bench_B.command_e[32] },/* In3[4] */
  { 0x7000, 3, 1008, 0, 5, &drum_bench_B.command_e[33] },/* In3[5] */
  { 0x7000, 3, 1008, 0, 6, &drum_bench_B.command_e[34] },/* In3[6] */
  { 0x7000, 3, 1008, 0, 7, &drum_bench_B.command_e[35] },/* In3[7] */
  { 0x7000, 3, 1008, 0, 8, &drum_bench_B.command_e[36] },/* In3[8] */
  { 0x7000, 3, 1008, 0, 9, &drum_bench_B.command_e[37] },/* In3[9] */
  { 0x7000, 3, 1008, 0, 10, &drum_bench_B.command_e[38] },/* In3[10] */
  { 0x7000, 3, 1008, 0, 11, &drum_bench_B.command_e[39] },/* In3[11] */
  { 0x7000, 3, 1008, 0, 12, &drum_bench_B.command_e[40] },/* In3[12] */
  { 0x7000, 3, 1008, 0, 13, &drum_bench_B.command_e[41] },/* In3[13] */
  { 0x7000, 3, 1008, 0, 14, &drum_bench_B.command_e[42] },/* In3[14] */
  { 0x7000, 3, 1008, 0, 15, &drum_bench_B.command_e[43] },/* In3[15] */
  { 0x7000, 3, 1008, 0, 16, &drum_bench_B.command_e[44] },/* In3[16] */
  { 0x7000, 3, 1008, 0, 17, &drum_bench_B.command_e[45] },/* In3[17] */
  { 0x7000, 3, 1008, 0, 18, &drum_bench_B.command_e[46] },/* In3[18] */
  { 0x7000, 3, 1008, 0, 19, &drum_bench_B.command_e[47] },/* In3[19] */
  { 0x7000, 3, 1008, 0, 20, &drum_bench_B.command_e[48] },/* In3[20] */
  { 0x7000, 3, 1008, 0, 21, &drum_bench_B.command_e[49] },/* In3[21] */
  { 0x7000, 3, 1008, 0, 22, &drum_bench_B.command_e[50] },/* In3[22] */
  { 0x7000, 3, 1008, 0, 23, &drum_bench_B.command_e[51] },/* In3[23] */
  { 0x7000, 3, 1008, 0, 24, &drum_bench_B.command_e[52] },/* In3[24] */
  { 0x7000, 3, 1008, 0, 25, &drum_bench_B.command_e[53] },/* In3[25] */
  { 0x7000, 3, 1008, 0, 26, &drum_bench_B.command_e[54] },/* In3[26] */
  { 0x7000, 3, 1008, 0, 27, &drum_bench_B.command_e[55] },/* In3[27] */
  { 0x7000, 4, 1032, 0, 0, &drum_bench_B.Output_o },/* In4[0] */
  { 0x7000, 5, 1008, 0, 0, &drum_bench_B.rpc_i[0] },/* In5[0] */
  { 0x7000, 5, 1008, 0, 1, &drum_bench_B.rpc_i[1] },/* In5[1] */
  { 0x7000, 5, 1008, 0, 2, &drum_bench_B.rpc_i[2] },/* In5[2] */
  { 0x7000, 5, 1008, 0, 3, &drum_bench_B.rpc_i[3] },/* In5[3] */
  { 0x7000, 5, 1008, 0, 4, &drum_bench_B.rpc_i[4] },/* In5[4] */
  { 0x7000, 5, 1008, 0, 5, &drum_bench_B.rpc_i[5] },/* In5[5] */
  { 0x7000, 5, 1008, 0, 6, &drum_bench_B.rpc_i[6] },/* In5[6] */
  { 0x7000, 5, 1008, 0, 7, &drum_bench_B.rpc_i[7] },/* In5[7] */
  { 0x7000, 5, 1008, 0, 8, &drum_bench_B.rpc_i[8] },/* In5[8] */
  { 0x7000, 5, 1008, 0, 9, &drum_bench_B.rpc_i[9] },/* In5[9] */
  { 0x7000, 5, 1008, 0, 10, &drum_bench_B.rpc_i[10] },/* In5[10] */
  { 0x7000, 5, 1008, 0, 11, &drum_bench_B.rpc_i[11] },/* In5[11] */
  { 0x7000, 5, 1008, 0, 12, &drum_bench_B.rpc_i[12] },/* In5[12] */
  { 0x7000, 5, 1008, 0, 13, &drum_bench_B.rpc_i[13] },/* In5[13] */
  { 0x7000, 5, 1008, 0, 14, &drum_bench_B.rpc_i[14] },/* In5[14] */
  { 0x7000, 5, 1008, 0, 15, &drum_bench_B.rpc_i[15] },/* In5[15] */
  { 0x7000, 5, 1008, 0, 16, &drum_bench_B.rpc_i[16] },/* In5[16] */
  { 0x7000, 5, 1008, 0, 17, &drum_bench_B.rpc_i[17] },/* In5[17] */
  { 0x7000, 5, 1008, 0, 18, &drum_bench_B.rpc_i[18] },/* In5[18] */
  { 0x7000, 5, 1008, 0, 19, &drum_bench_B.rpc_i[19] },/* In5[19] */
  { 0x7000, 5, 1008, 0, 20, &drum_bench_B.rpc_i[20] },/* In5[20] */
  { 0x7000, 5, 1008, 0, 21, &drum_bench_B.rpc_i[21] },/* In5[21] */
  { 0x7000, 5, 1008, 0, 22, &drum_bench_B.rpc_i[22] },/* In5[22] */
  { 0x7000, 5, 1008, 0, 23, &drum_bench_B.rpc_i[23] },/* In5[23] */
  { 0x7000, 5, 1008, 0, 24, &drum_bench_B.rpc_i[24] },/* In5[24] */
  { 0x7000, 5, 1008, 0, 25, &drum_bench_B.rpc_i[25] },/* In5[25] */
  { 0x7000, 5, 1008, 0, 26, &drum_bench_B.rpc_i[26] },/* In5[26] */
  { 0x7000, 5, 1008, 0, 27, &drum_bench_B.rpc_i[27] },/* In5[27] */
  { 0x6000, 1, 3032, 0, 0, &drum_bench_B.marlin_ec_o1 },/* Out1[0] */
  { 0x6000, 2, 3032, 0, 0, &drum_bench_B.marlin_ec_o2 },/* Out2[0] */
  { 0x6000, 3, 3032, 0, 0, &drum_bench_B.marlin_ec_o3 },/* Out3[0] */
  { 0x6000, 4, 3032, 0, 0, &drum_bench_B.marlin_ec_o4 },/* Out4[0] */
  { 0x6000, 5, 3032, 0, 0, &drum_bench_B.marlin_ec_o5 },/* Out5[0] */
  { 0x6000, 6, 3032, 0, 0, &drum_bench_B.marlin_ec_o6 },/* Out6[0] */
  { 0x6000, 7, 3032, 0, 0, &drum_bench_B.marlin_ec_o7 },/* Out7[0] */
  { 0x6000, 8, 3032, 0, 0, &drum_bench_B.marlin_ec_o8 },/* Out8[0] */
  { 0x6000, 9, 3032, 0, 0, &drum_bench_B.marlin_ec_o9 },/* Out9[0] */
  { 0x6000, 10, 3032, 0, 0, &drum_bench_B.marlin_ec_o10 },/* Out10[0] */
  { 0x6000, 11, 3032, 0, 0, &drum_bench_B.marlin_ec_o11 },/* Out11[0] */
  { 0x6000, 12, 3032, 0, 0, &drum_bench_B.marlin_ec_o12 },/* Out12[0] */
  { 0x6000, 13, 3032, 0, 0, &drum_bench_B.marlin_ec_o13 },/* Out13[0] */
  { 0x6000, 14, 3032, 0, 0, &drum_bench_B.marlin_ec_o14 },/* Out14[0] */
  { 0x6000, 15, 3032, 0, 0, &drum_bench_B.marlin_ec_o15 },/* Out15[0] */
  { 0x6000, 16, 3032, 0, 0, &drum_bench_B.marlin_ec_o16 },/* Out16[0] */
  { 0x6000, 17, 3032, 0, 0, &drum_bench_B.marlin_ec_o17 },/* Out17[0] */
  { 0x6000, 18, 3032, 0, 0, &drum_bench_B.marlin_ec_o18 },/* Out18[0] */
  { 0x6000, 19, 1032, 0, 0, &drum_bench_B.marlin_ec_o19 },/* Out19[0] */
  { 0x6000, 20, 3032, 0, 0, &drum_bench_B.marlin_ec_o20 },/* Out20[0] */
  { 0x6000, 21, 3032, 0, 0, &drum_bench_B.marlin_ec_o21 },/* Out21[0] */
  { 0x6000, 22, 1008, 0, 0, &drum_bench_B.marlin_ec_o22[0] },/* Out22[0] */
  { 0x6000, 22, 1008, 0, 1, &drum_bench_B.marlin_ec_o22[1] },/* Out22[1] */
  { 0x6000, 22, 1008, 0, 2, &drum_bench_B.marlin_ec_o22[2] },/* Out22[2] */
  { 0x6000, 22, 1008, 0, 3, &drum_bench_B.marlin_ec_o22[3] },/* Out22[3] */
  { 0x6000, 22, 1008, 0, 4, &drum_bench_B.marlin_ec_o22[4] },/* Out22[4] */
  { 0x6000, 22, 1008, 0, 5, &drum_bench_B.marlin_ec_o22[5] },/* Out22[5] */
  { 0x6000, 22, 1008, 0, 6, &drum_bench_B.marlin_ec_o22[6] },/* Out22[6] */
  { 0x6000, 22, 1008, 0, 7, &drum_bench_B.marlin_ec_o22[7] },/* Out22[7] */
  { 0x6000, 22, 1008, 0, 8, &drum_bench_B.marlin_ec_o22[8] },/* Out22[8] */
  { 0x6000, 22, 1008, 0, 9, &drum_bench_B.marlin_ec_o22[9] },/* Out22[9] */
  { 0x6000, 22, 1008, 0, 10, &drum_bench_B.marlin_ec_o22[10] },/* Out22[10] */
  { 0x6000, 22, 1008, 0, 11, &drum_bench_B.marlin_ec_o22[11] },/* Out22[11] */
  { 0x6000, 22, 1008, 0, 12, &drum_bench_B.marlin_ec_o22[12] },/* Out22[12] */
  { 0x6000, 22, 1008, 0, 13, &drum_bench_B.marlin_ec_o22[13] },/* Out22[13] */
  { 0x6000, 22, 1008, 0, 14, &drum_bench_B.marlin_ec_o22[14] },/* Out22[14] */
  { 0x6000, 22, 1008, 0, 15, &drum_bench_B.marlin_ec_o22[15] },/* Out22[15] */
  { 0x6000, 22, 1008, 0, 16, &drum_bench_B.marlin_ec_o22[16] },/* Out22[16] */
  { 0x6000, 22, 1008, 0, 17, &drum_bench_B.marlin_ec_o22[17] },/* Out22[17] */
  { 0x6000, 22, 1008, 0, 18, &drum_bench_B.marlin_ec_o22[18] },/* Out22[18] */
  { 0x6000, 22, 1008, 0, 19, &drum_bench_B.marlin_ec_o22[19] },/* Out22[19] */
  { 0x6000, 22, 1008, 0, 20, &drum_bench_B.marlin_ec_o22[20] },/* Out22[20] */
  { 0x6000, 22, 1008, 0, 21, &drum_bench_B.marlin_ec_o22[21] },/* Out22[21] */
  { 0x6000, 22, 1008, 0, 22, &drum_bench_B.marlin_ec_o22[22] },/* Out22[22] */
  { 0x6000, 22, 1008, 0, 23, &drum_bench_B.marlin_ec_o22[23] },/* Out22[23] */
  { 0x6000, 22, 1008, 0, 24, &drum_bench_B.marlin_ec_o22[24] },/* Out22[24] */
  { 0x6000, 22, 1008, 0, 25, &drum_bench_B.marlin_ec_o22[25] },/* Out22[25] */
  { 0x6000, 22, 1008, 0, 26, &drum_bench_B.marlin_ec_o22[26] },/* Out22[26] */
  { 0x6000, 22, 1008, 0, 27, &drum_bench_B.marlin_ec_o22[27] },/* Out22[27] */
};

/* PDO configuration for M3EC
 */
static ec_pdo_entry_info_t pdo_entry_info_518_00000000_0[] = {
  { 0x7000, 1, 32 },                   /* 0 */
  { 0x7000, 2, 224 },                  /* 1 */
  { 0x7000, 3, 224 },                  /* 2 */
  { 0x7000, 4, 32 },                   /* 3 */
  { 0x7000, 5, 224 },                  /* 4 */
  { 0x6000, 1, 32 },                   /* 5 */
  { 0x6000, 2, 32 },                   /* 6 */
  { 0x6000, 3, 32 },                   /* 7 */
  { 0x6000, 4, 32 },                   /* 8 */
  { 0x6000, 5, 32 },                   /* 9 */
  { 0x6000, 6, 32 },                   /* 10 */
  { 0x6000, 7, 32 },                   /* 11 */
  { 0x6000, 8, 32 },                   /* 12 */
  { 0x6000, 9, 32 },                   /* 13 */
  { 0x6000, 10, 32 },                  /* 14 */
  { 0x6000, 11, 32 },                  /* 15 */
  { 0x6000, 12, 32 },                  /* 16 */
  { 0x6000, 13, 32 },                  /* 17 */
  { 0x6000, 14, 32 },                  /* 18 */
  { 0x6000, 15, 32 },                  /* 19 */
  { 0x6000, 16, 32 },                  /* 20 */
  { 0x6000, 17, 32 },                  /* 21 */
  { 0x6000, 18, 32 },                  /* 22 */
  { 0x6000, 19, 32 },                  /* 23 */
  { 0x6000, 20, 32 },                  /* 24 */
  { 0x6000, 21, 32 },                  /* 25 */
  { 0x6000, 22, 224 },                 /* 26 */
};

static ec_pdo_info_t pdo_info_518_00000000_0[] = {
  { 0x1600, 5, &pdo_entry_info_518_00000000_0[0] },/* */
  { 0x1A00, 22, &pdo_entry_info_518_00000000_0[5U] },/* */
};

static ec_sync_info_t sync_manager_518_00000000_0[] = {
  { 0U, EC_DIR_OUTPUT, 1U, &pdo_info_518_00000000_0[0] },/* */
  { 1U, EC_DIR_INPUT, 1U, &pdo_info_518_00000000_0[1U] },/* */
  { (uint8_T)EC_END, },
};

/* S-Function Block: <S95>/marlin_ec
 * Registering EtherCAT block M3EC with Driver
 */
static struct ec_slave ec_slave_1 = {
  NULL,                                /* Linked list */
  1,                                   /* TID */
  0U, 0U, 0U, 0U,                      /* MasterId, DomainId, SlaveAlias, SlavePosition */
  1304U, 0x00000000,                   /* VendorId, ProductCode */
  0, NULL,                             /* SdoConfigCount, SdoVar */
  0, NULL,                             /* SoeConfigCount, SoeVar */
  sync_manager_518_00000000_0,         /* SyncManager Configuration */

  { 0, },                              /* Distributed Clock Configuration */
  86, 49, pdo_map_1                    /* RxPdo and TxPdo Pdo count and Configuration */
};

/* S-Function Block: <S160>/marlin_ec
 * Mapped Pdos for block M3EC
 */
static struct pdo_map pdo_map_2[] = {
  { 0x7000, 1, 1032, 0, 0, &drum_bench_B.mode_e },/* In1[0] */
  { 0x7000, 2, 1008, 0, 0, &drum_bench_B.command_c[0] },/* In2[0] */
  { 0x7000, 2, 1008, 0, 1, &drum_bench_B.command_c[1] },/* In2[1] */
  { 0x7000, 2, 1008, 0, 2, &drum_bench_B.command_c[2] },/* In2[2] */
  { 0x7000, 2, 1008, 0, 3, &drum_bench_B.command_c[3] },/* In2[3] */
  { 0x7000, 2, 1008, 0, 4, &drum_bench_B.command_c[4] },/* In2[4] */
  { 0x7000, 2, 1008, 0, 5, &drum_bench_B.command_c[5] },/* In2[5] */
  { 0x7000, 2, 1008, 0, 6, &drum_bench_B.command_c[6] },/* In2[6] */
  { 0x7000, 2, 1008, 0, 7, &drum_bench_B.command_c[7] },/* In2[7] */
  { 0x7000, 2, 1008, 0, 8, &drum_bench_B.command_c[8] },/* In2[8] */
  { 0x7000, 2, 1008, 0, 9, &drum_bench_B.command_c[9] },/* In2[9] */
  { 0x7000, 2, 1008, 0, 10, &drum_bench_B.command_c[10] },/* In2[10] */
  { 0x7000, 2, 1008, 0, 11, &drum_bench_B.command_c[11] },/* In2[11] */
  { 0x7000, 2, 1008, 0, 12, &drum_bench_B.command_c[12] },/* In2[12] */
  { 0x7000, 2, 1008, 0, 13, &drum_bench_B.command_c[13] },/* In2[13] */
  { 0x7000, 2, 1008, 0, 14, &drum_bench_B.command_c[14] },/* In2[14] */
  { 0x7000, 2, 1008, 0, 15, &drum_bench_B.command_c[15] },/* In2[15] */
  { 0x7000, 2, 1008, 0, 16, &drum_bench_B.command_c[16] },/* In2[16] */
  { 0x7000, 2, 1008, 0, 17, &drum_bench_B.command_c[17] },/* In2[17] */
  { 0x7000, 2, 1008, 0, 18, &drum_bench_B.command_c[18] },/* In2[18] */
  { 0x7000, 2, 1008, 0, 19, &drum_bench_B.command_c[19] },/* In2[19] */
  { 0x7000, 2, 1008, 0, 20, &drum_bench_B.command_c[20] },/* In2[20] */
  { 0x7000, 2, 1008, 0, 21, &drum_bench_B.command_c[21] },/* In2[21] */
  { 0x7000, 2, 1008, 0, 22, &drum_bench_B.command_c[22] },/* In2[22] */
  { 0x7000, 2, 1008, 0, 23, &drum_bench_B.command_c[23] },/* In2[23] */
  { 0x7000, 2, 1008, 0, 24, &drum_bench_B.command_c[24] },/* In2[24] */
  { 0x7000, 2, 1008, 0, 25, &drum_bench_B.command_c[25] },/* In2[25] */
  { 0x7000, 2, 1008, 0, 26, &drum_bench_B.command_c[26] },/* In2[26] */
  { 0x7000, 2, 1008, 0, 27, &drum_bench_B.command_c[27] },/* In2[27] */
  { 0x7000, 3, 1008, 0, 0, &drum_bench_B.command_c[28] },/* In3[0] */
  { 0x7000, 3, 1008, 0, 1, &drum_bench_B.command_c[29] },/* In3[1] */
  { 0x7000, 3, 1008, 0, 2, &drum_bench_B.command_c[30] },/* In3[2] */
  { 0x7000, 3, 1008, 0, 3, &drum_bench_B.command_c[31] },/* In3[3] */
  { 0x7000, 3, 1008, 0, 4, &drum_bench_B.command_c[32] },/* In3[4] */
  { 0x7000, 3, 1008, 0, 5, &drum_bench_B.command_c[33] },/* In3[5] */
  { 0x7000, 3, 1008, 0, 6, &drum_bench_B.command_c[34] },/* In3[6] */
  { 0x7000, 3, 1008, 0, 7, &drum_bench_B.command_c[35] },/* In3[7] */
  { 0x7000, 3, 1008, 0, 8, &drum_bench_B.command_c[36] },/* In3[8] */
  { 0x7000, 3, 1008, 0, 9, &drum_bench_B.command_c[37] },/* In3[9] */
  { 0x7000, 3, 1008, 0, 10, &drum_bench_B.command_c[38] },/* In3[10] */
  { 0x7000, 3, 1008, 0, 11, &drum_bench_B.command_c[39] },/* In3[11] */
  { 0x7000, 3, 1008, 0, 12, &drum_bench_B.command_c[40] },/* In3[12] */
  { 0x7000, 3, 1008, 0, 13, &drum_bench_B.command_c[41] },/* In3[13] */
  { 0x7000, 3, 1008, 0, 14, &drum_bench_B.command_c[42] },/* In3[14] */
  { 0x7000, 3, 1008, 0, 15, &drum_bench_B.command_c[43] },/* In3[15] */
  { 0x7000, 3, 1008, 0, 16, &drum_bench_B.command_c[44] },/* In3[16] */
  { 0x7000, 3, 1008, 0, 17, &drum_bench_B.command_c[45] },/* In3[17] */
  { 0x7000, 3, 1008, 0, 18, &drum_bench_B.command_c[46] },/* In3[18] */
  { 0x7000, 3, 1008, 0, 19, &drum_bench_B.command_c[47] },/* In3[19] */
  { 0x7000, 3, 1008, 0, 20, &drum_bench_B.command_c[48] },/* In3[20] */
  { 0x7000, 3, 1008, 0, 21, &drum_bench_B.command_c[49] },/* In3[21] */
  { 0x7000, 3, 1008, 0, 22, &drum_bench_B.command_c[50] },/* In3[22] */
  { 0x7000, 3, 1008, 0, 23, &drum_bench_B.command_c[51] },/* In3[23] */
  { 0x7000, 3, 1008, 0, 24, &drum_bench_B.command_c[52] },/* In3[24] */
  { 0x7000, 3, 1008, 0, 25, &drum_bench_B.command_c[53] },/* In3[25] */
  { 0x7000, 3, 1008, 0, 26, &drum_bench_B.command_c[54] },/* In3[26] */
  { 0x7000, 3, 1008, 0, 27, &drum_bench_B.command_c[55] },/* In3[27] */
  { 0x7000, 4, 1032, 0, 0, &drum_bench_B.Output_i4 },/* In4[0] */
  { 0x7000, 5, 1008, 0, 0, &drum_bench_B.rpc_n[0] },/* In5[0] */
  { 0x7000, 5, 1008, 0, 1, &drum_bench_B.rpc_n[1] },/* In5[1] */
  { 0x7000, 5, 1008, 0, 2, &drum_bench_B.rpc_n[2] },/* In5[2] */
  { 0x7000, 5, 1008, 0, 3, &drum_bench_B.rpc_n[3] },/* In5[3] */
  { 0x7000, 5, 1008, 0, 4, &drum_bench_B.rpc_n[4] },/* In5[4] */
  { 0x7000, 5, 1008, 0, 5, &drum_bench_B.rpc_n[5] },/* In5[5] */
  { 0x7000, 5, 1008, 0, 6, &drum_bench_B.rpc_n[6] },/* In5[6] */
  { 0x7000, 5, 1008, 0, 7, &drum_bench_B.rpc_n[7] },/* In5[7] */
  { 0x7000, 5, 1008, 0, 8, &drum_bench_B.rpc_n[8] },/* In5[8] */
  { 0x7000, 5, 1008, 0, 9, &drum_bench_B.rpc_n[9] },/* In5[9] */
  { 0x7000, 5, 1008, 0, 10, &drum_bench_B.rpc_n[10] },/* In5[10] */
  { 0x7000, 5, 1008, 0, 11, &drum_bench_B.rpc_n[11] },/* In5[11] */
  { 0x7000, 5, 1008, 0, 12, &drum_bench_B.rpc_n[12] },/* In5[12] */
  { 0x7000, 5, 1008, 0, 13, &drum_bench_B.rpc_n[13] },/* In5[13] */
  { 0x7000, 5, 1008, 0, 14, &drum_bench_B.rpc_n[14] },/* In5[14] */
  { 0x7000, 5, 1008, 0, 15, &drum_bench_B.rpc_n[15] },/* In5[15] */
  { 0x7000, 5, 1008, 0, 16, &drum_bench_B.rpc_n[16] },/* In5[16] */
  { 0x7000, 5, 1008, 0, 17, &drum_bench_B.rpc_n[17] },/* In5[17] */
  { 0x7000, 5, 1008, 0, 18, &drum_bench_B.rpc_n[18] },/* In5[18] */
  { 0x7000, 5, 1008, 0, 19, &drum_bench_B.rpc_n[19] },/* In5[19] */
  { 0x7000, 5, 1008, 0, 20, &drum_bench_B.rpc_n[20] },/* In5[20] */
  { 0x7000, 5, 1008, 0, 21, &drum_bench_B.rpc_n[21] },/* In5[21] */
  { 0x7000, 5, 1008, 0, 22, &drum_bench_B.rpc_n[22] },/* In5[22] */
  { 0x7000, 5, 1008, 0, 23, &drum_bench_B.rpc_n[23] },/* In5[23] */
  { 0x7000, 5, 1008, 0, 24, &drum_bench_B.rpc_n[24] },/* In5[24] */
  { 0x7000, 5, 1008, 0, 25, &drum_bench_B.rpc_n[25] },/* In5[25] */
  { 0x7000, 5, 1008, 0, 26, &drum_bench_B.rpc_n[26] },/* In5[26] */
  { 0x7000, 5, 1008, 0, 27, &drum_bench_B.rpc_n[27] },/* In5[27] */
  { 0x6000, 1, 3032, 0, 0, &drum_bench_B.marlin_ec_o1_k },/* Out1[0] */
  { 0x6000, 2, 3032, 0, 0, &drum_bench_B.marlin_ec_o2_f },/* Out2[0] */
  { 0x6000, 3, 3032, 0, 0, &drum_bench_B.marlin_ec_o3_h },/* Out3[0] */
  { 0x6000, 4, 3032, 0, 0, &drum_bench_B.marlin_ec_o4_a },/* Out4[0] */
  { 0x6000, 5, 3032, 0, 0, &drum_bench_B.marlin_ec_o5_l },/* Out5[0] */
  { 0x6000, 6, 3032, 0, 0, &drum_bench_B.marlin_ec_o6_j },/* Out6[0] */
  { 0x6000, 7, 3032, 0, 0, &drum_bench_B.marlin_ec_o7_c },/* Out7[0] */
  { 0x6000, 8, 3032, 0, 0, &drum_bench_B.marlin_ec_o8_j },/* Out8[0] */
  { 0x6000, 9, 3032, 0, 0, &drum_bench_B.marlin_ec_o9_j },/* Out9[0] */
  { 0x6000, 10, 3032, 0, 0, &drum_bench_B.marlin_ec_o10_n },/* Out10[0] */
  { 0x6000, 11, 3032, 0, 0, &drum_bench_B.marlin_ec_o11_d },/* Out11[0] */
  { 0x6000, 12, 3032, 0, 0, &drum_bench_B.marlin_ec_o12_h },/* Out12[0] */
  { 0x6000, 13, 3032, 0, 0, &drum_bench_B.marlin_ec_o13_b },/* Out13[0] */
  { 0x6000, 14, 3032, 0, 0, &drum_bench_B.marlin_ec_o14_e },/* Out14[0] */
  { 0x6000, 15, 3032, 0, 0, &drum_bench_B.marlin_ec_o15_h },/* Out15[0] */
  { 0x6000, 16, 3032, 0, 0, &drum_bench_B.marlin_ec_o16_c },/* Out16[0] */
  { 0x6000, 17, 3032, 0, 0, &drum_bench_B.marlin_ec_o17_a },/* Out17[0] */
  { 0x6000, 18, 3032, 0, 0, &drum_bench_B.marlin_ec_o18_g },/* Out18[0] */
  { 0x6000, 19, 1032, 0, 0, &drum_bench_B.marlin_ec_o19_n },/* Out19[0] */
  { 0x6000, 20, 3032, 0, 0, &drum_bench_B.marlin_ec_o20_k },/* Out20[0] */
  { 0x6000, 21, 3032, 0, 0, &drum_bench_B.marlin_ec_o21_m },/* Out21[0] */
  { 0x6000, 22, 1008, 0, 0, &drum_bench_B.marlin_ec_o22_a[0] },/* Out22[0] */
  { 0x6000, 22, 1008, 0, 1, &drum_bench_B.marlin_ec_o22_a[1] },/* Out22[1] */
  { 0x6000, 22, 1008, 0, 2, &drum_bench_B.marlin_ec_o22_a[2] },/* Out22[2] */
  { 0x6000, 22, 1008, 0, 3, &drum_bench_B.marlin_ec_o22_a[3] },/* Out22[3] */
  { 0x6000, 22, 1008, 0, 4, &drum_bench_B.marlin_ec_o22_a[4] },/* Out22[4] */
  { 0x6000, 22, 1008, 0, 5, &drum_bench_B.marlin_ec_o22_a[5] },/* Out22[5] */
  { 0x6000, 22, 1008, 0, 6, &drum_bench_B.marlin_ec_o22_a[6] },/* Out22[6] */
  { 0x6000, 22, 1008, 0, 7, &drum_bench_B.marlin_ec_o22_a[7] },/* Out22[7] */
  { 0x6000, 22, 1008, 0, 8, &drum_bench_B.marlin_ec_o22_a[8] },/* Out22[8] */
  { 0x6000, 22, 1008, 0, 9, &drum_bench_B.marlin_ec_o22_a[9] },/* Out22[9] */
  { 0x6000, 22, 1008, 0, 10, &drum_bench_B.marlin_ec_o22_a[10] },/* Out22[10] */
  { 0x6000, 22, 1008, 0, 11, &drum_bench_B.marlin_ec_o22_a[11] },/* Out22[11] */
  { 0x6000, 22, 1008, 0, 12, &drum_bench_B.marlin_ec_o22_a[12] },/* Out22[12] */
  { 0x6000, 22, 1008, 0, 13, &drum_bench_B.marlin_ec_o22_a[13] },/* Out22[13] */
  { 0x6000, 22, 1008, 0, 14, &drum_bench_B.marlin_ec_o22_a[14] },/* Out22[14] */
  { 0x6000, 22, 1008, 0, 15, &drum_bench_B.marlin_ec_o22_a[15] },/* Out22[15] */
  { 0x6000, 22, 1008, 0, 16, &drum_bench_B.marlin_ec_o22_a[16] },/* Out22[16] */
  { 0x6000, 22, 1008, 0, 17, &drum_bench_B.marlin_ec_o22_a[17] },/* Out22[17] */
  { 0x6000, 22, 1008, 0, 18, &drum_bench_B.marlin_ec_o22_a[18] },/* Out22[18] */
  { 0x6000, 22, 1008, 0, 19, &drum_bench_B.marlin_ec_o22_a[19] },/* Out22[19] */
  { 0x6000, 22, 1008, 0, 20, &drum_bench_B.marlin_ec_o22_a[20] },/* Out22[20] */
  { 0x6000, 22, 1008, 0, 21, &drum_bench_B.marlin_ec_o22_a[21] },/* Out22[21] */
  { 0x6000, 22, 1008, 0, 22, &drum_bench_B.marlin_ec_o22_a[22] },/* Out22[22] */
  { 0x6000, 22, 1008, 0, 23, &drum_bench_B.marlin_ec_o22_a[23] },/* Out22[23] */
  { 0x6000, 22, 1008, 0, 24, &drum_bench_B.marlin_ec_o22_a[24] },/* Out22[24] */
  { 0x6000, 22, 1008, 0, 25, &drum_bench_B.marlin_ec_o22_a[25] },/* Out22[25] */
  { 0x6000, 22, 1008, 0, 26, &drum_bench_B.marlin_ec_o22_a[26] },/* Out22[26] */
  { 0x6000, 22, 1008, 0, 27, &drum_bench_B.marlin_ec_o22_a[27] },/* Out22[27] */
};

/* S-Function Block: <S160>/marlin_ec
 * Registering EtherCAT block M3EC with Driver
 */
static struct ec_slave ec_slave_2 = {
  NULL,                                /* Linked list */
  1,                                   /* TID */
  0U, 0U, 0U, 0U,                      /* MasterId, DomainId, SlaveAlias, SlavePosition */
  1304U, 0x00000000,                   /* VendorId, ProductCode */
  0, NULL,                             /* SdoConfigCount, SdoVar */
  0, NULL,                             /* SoeConfigCount, SoeVar */
  sync_manager_518_00000000_0,         /* SyncManager Configuration */

  { 0, },                              /* Distributed Clock Configuration */
  86, 49, pdo_map_2                    /* RxPdo and TxPdo Pdo count and Configuration */
};

/* Block signals (auto storage) */
B_drum_bench_T drum_bench_B;

/* Block states (auto storage) */
DW_drum_bench_T drum_bench_DW;

/* Previous zero-crossings (trigger) states */
PrevZCX_drum_bench_T drum_bench_PrevZCX;

/* Real-time model */
RT_MODEL_drum_bench_T drum_bench_M_;
RT_MODEL_drum_bench_T *const drum_bench_M = &drum_bench_M_;

/* Forward declaration for local functions */
static void drum_bench_twister_state_vector(uint32_T mt[625], real_T seed);
static real_T drum_bench_eml_rand_mt19937ar(uint32_T state[625]);
static real_T drum_bench_rand(DW_MATLABFunction1_drum_bench_T *localDW);
static void rate_scheduler(void);

/* All EtherLAB error messages go in here */
char etl_errbuf[256];

/*
 *   This function updates active task flag for each subrate.
 * The function is called at model base rate, hence the
 * generated code self-manages all its subrates.
 */
static void rate_scheduler(void)
{
  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (drum_bench_M->Timing.TaskCounters.TID[2])++;
  if ((drum_bench_M->Timing.TaskCounters.TID[2]) > 2) {/* Sample time: [0.0003s, 0.0s] */
    drum_bench_M->Timing.TaskCounters.TID[2] = 0;
  }

  drum_bench_M->Timing.sampleHits[2] = (drum_bench_M->Timing.TaskCounters.TID[2]
    == 0);
  (drum_bench_M->Timing.TaskCounters.TID[3])++;
  if ((drum_bench_M->Timing.TaskCounters.TID[3]) > 9) {/* Sample time: [0.001s, 0.0s] */
    drum_bench_M->Timing.TaskCounters.TID[3] = 0;
  }

  drum_bench_M->Timing.sampleHits[3] = (drum_bench_M->Timing.TaskCounters.TID[3]
    == 0);
  (drum_bench_M->Timing.TaskCounters.TID[4])++;
  if ((drum_bench_M->Timing.TaskCounters.TID[4]) > 99) {/* Sample time: [0.01s, 0.0s] */
    drum_bench_M->Timing.TaskCounters.TID[4] = 0;
  }

  drum_bench_M->Timing.sampleHits[4] = (drum_bench_M->Timing.TaskCounters.TID[4]
    == 0);
  (drum_bench_M->Timing.TaskCounters.TID[5])++;
  if ((drum_bench_M->Timing.TaskCounters.TID[5]) > 383) {/* Sample time: [0.0384s, 0.0s] */
    drum_bench_M->Timing.TaskCounters.TID[5] = 0;
  }

  drum_bench_M->Timing.sampleHits[5] = (drum_bench_M->Timing.TaskCounters.TID[5]
    == 0);
}

/*
 * Output and update for atomic system:
 *    '<S15>/mean'
 *    '<S15>/mean1'
 *    '<S15>/mean2'
 *    '<S16>/mean'
 *    '<S16>/mean1'
 *    '<S16>/mean2'
 */
void drum_bench_mean(const real_T rtu_u[128], B_mean_drum_bench_T *localB)
{
  real_T x[128];
  real_T y;
  int32_T k;

  /* MATLAB Function 'Behavior Control/EMG Filter/mean': '<S21>:1' */
  /* '<S21>:1:4' */
  memcpy(&x[0], &rtu_u[0], sizeof(real_T) << 7U);
  y = x[0];
  for (k = 0; k < 127; k++) {
    y += x[k + 1];
  }

  localB->y = y / 128.0;
}

/*
 * Initial conditions for atomic system:
 *    '<S15>/slopeChange'
 *    '<S16>/slopeChange'
 */
void drum_bench_slopeChange_Init(DW_slopeChange_drum_bench_T *localDW)
{
  localDW->lastSlope = 1.0;
  localDW->lastVal = 0.0;
}

/*
 * Output and update for atomic system:
 *    '<S15>/slopeChange'
 *    '<S16>/slopeChange'
 */
void drum_bench_slopeChange(real_T rtu_u, B_slopeChange_drum_bench_T *localB,
  DW_slopeChange_drum_bench_T *localDW)
{
  real_T currentSlope;
  int32_T output;

  /* MATLAB Function 'Behavior Control/EMG Filter/slopeChange': '<S24>:1' */
  /* '<S24>:1:7' */
  currentSlope = rtu_u - localDW->lastVal;
  if (currentSlope < 0.0) {
    currentSlope = -1.0;
  } else if (currentSlope > 0.0) {
    currentSlope = 1.0;
  } else {
    if (currentSlope == 0.0) {
      currentSlope = 0.0;
    }
  }

  if (currentSlope != localDW->lastSlope) {
    /* '<S24>:1:8' */
    /* '<S24>:1:9' */
    localDW->lastSlope = currentSlope;

    /* '<S24>:1:10' */
    output = 1;
  } else {
    /* '<S24>:1:12' */
    output = 0;
  }

  /* '<S24>:1:15' */
  localDW->lastVal = rtu_u;

  /* '<S24>:1:16' */
  localB->y = output;
}

/*
 * Initial conditions for atomic system:
 *    '<S15>/smooth'
 *    '<S16>/smooth'
 */
void drum_bench_smooth_Init(DW_smooth_drum_bench_T *localDW)
{
  int32_T i;
  for (i = 0; i < 1500; i++) {
    localDW->buf[i] = (rtNaN);
  }
}

/*
 * Output and update for atomic system:
 *    '<S15>/smooth'
 *    '<S16>/smooth'
 */
void drum_bench_smooth(real_T rtu_u, B_smooth_drum_bench_T *localB,
  DW_smooth_drum_bench_T *localDW)
{
  real_T atmp;
  int32_T j;
  int32_T g_k;

  /* MATLAB Function 'Behavior Control/EMG Filter/smooth': '<S25>:1' */
  /* '<S25>:1:9' */
  g_k = 1;
  while (g_k <= 1) {
    for (j = 0; j + 1 < 2; j++) {
      atmp = localDW->buf[j + 1499];
      for (g_k = 1500; g_k >= 2; g_k--) {
        localDW->buf[(j + g_k) - 1] = localDW->buf[(j + g_k) - 2];
      }

      localDW->buf[j] = atmp;
    }

    g_k = 2;
  }

  /* '<S25>:1:10' */
  localDW->buf[0] = rtu_u;

  /* '<S25>:1:11' */
  /* '<S25>:1:12' */
  atmp = localDW->buf[0];
  for (g_k = 0; g_k < 1499; g_k++) {
    atmp += localDW->buf[g_k + 1];
  }

  localB->y = atmp / 1500.0;
}

/*
 * Initial conditions for atomic system:
 *    '<S15>/smooth1'
 *    '<S15>/smooth2'
 *    '<S16>/smooth1'
 *    '<S16>/smooth2'
 */
void drum_bench_smooth1_Init(DW_smooth1_drum_bench_T *localDW)
{
  int32_T i;
  for (i = 0; i < 500; i++) {
    localDW->buf[i] = (rtNaN);
  }
}

/*
 * Output and update for atomic system:
 *    '<S15>/smooth1'
 *    '<S15>/smooth2'
 *    '<S16>/smooth1'
 *    '<S16>/smooth2'
 */
void drum_bench_smooth1(real_T rtu_u, B_smooth1_drum_bench_T *localB,
  DW_smooth1_drum_bench_T *localDW)
{
  real_T atmp;
  int32_T j;
  int32_T g_k;

  /* MATLAB Function 'Behavior Control/EMG Filter/smooth1': '<S26>:1' */
  /* '<S26>:1:9' */
  g_k = 1;
  while (g_k <= 1) {
    for (j = 0; j + 1 < 2; j++) {
      atmp = localDW->buf[j + 499];
      for (g_k = 500; g_k >= 2; g_k--) {
        localDW->buf[(j + g_k) - 1] = localDW->buf[(j + g_k) - 2];
      }

      localDW->buf[j] = atmp;
    }

    g_k = 2;
  }

  /* '<S26>:1:10' */
  localDW->buf[0] = rtu_u;

  /* '<S26>:1:11' */
  /* '<S26>:1:12' */
  atmp = localDW->buf[0];
  for (g_k = 0; g_k < 499; g_k++) {
    atmp += localDW->buf[g_k + 1];
  }

  localB->y = atmp / 500.0;
}

/*
 * Initial conditions for atomic system:
 *    '<S15>/smooth3'
 *    '<S16>/smooth3'
 */
void drum_bench_smooth3_Init(DW_smooth3_drum_bench_T *localDW)
{
  int32_T i;
  for (i = 0; i < 7; i++) {
    localDW->buf[i] = (rtNaN);
  }
}

/*
 * Output and update for atomic system:
 *    '<S15>/smooth3'
 *    '<S16>/smooth3'
 */
void drum_bench_smooth3(real_T rtu_u, B_smooth3_drum_bench_T *localB,
  DW_smooth3_drum_bench_T *localDW)
{
  real_T atmp;
  int32_T j;
  int32_T g_k;

  /* MATLAB Function 'Behavior Control/EMG Filter/smooth3': '<S28>:1' */
  /* '<S28>:1:9' */
  g_k = 1;
  while (g_k <= 1) {
    for (j = 0; j + 1 < 2; j++) {
      atmp = localDW->buf[j + 6];
      for (g_k = 7; g_k >= 2; g_k--) {
        localDW->buf[(j + g_k) - 1] = localDW->buf[(j + g_k) - 2];
      }

      localDW->buf[j] = atmp;
    }

    g_k = 2;
  }

  /* '<S28>:1:10' */
  localDW->buf[0] = rtu_u;

  /* '<S28>:1:11' */
  /* '<S28>:1:12' */
  atmp = localDW->buf[0];
  for (g_k = 0; g_k < 6; g_k++) {
    atmp += localDW->buf[g_k + 1];
  }

  localB->y = atmp / 7.0;
}

/*
 * Initial conditions for atomic system:
 *    '<S15>/zeroCrossing'
 *    '<S16>/zeroCrossing'
 */
void drum_bench_zeroCrossing_Init(DW_zeroCrossing_drum_bench_T *localDW)
{
  localDW->lastSign = 1.0;
}

/*
 * Output and update for atomic system:
 *    '<S15>/zeroCrossing'
 *    '<S16>/zeroCrossing'
 */
void drum_bench_zeroCrossing(real_T rtu_u, B_zeroCrossing_drum_bench_T *localB,
  DW_zeroCrossing_drum_bench_T *localDW)
{
  real_T currentSign;
  int32_T output;

  /* MATLAB Function 'Behavior Control/EMG Filter/zeroCrossing': '<S29>:1' */
  /* '<S29>:1:8' */
  currentSign = rtu_u;
  if (currentSign < 0.0) {
    currentSign = -1.0;
  } else if (currentSign > 0.0) {
    currentSign = 1.0;
  } else {
    if (currentSign == 0.0) {
      currentSign = 0.0;
    }
  }

  if (localDW->lastSign != currentSign) {
    /* '<S29>:1:9' */
    /* '<S29>:1:10' */
    localDW->lastSign = currentSign;

    /* '<S29>:1:11' */
    output = 1;
  } else {
    /* '<S29>:1:13' */
    output = 0;
  }

  /* '<S29>:1:15' */
  localB->y = output;
}

/*
 * Initial conditions for atomic system:
 *    '<S2>/Filtered Derivative'
 *    '<S3>/Filtered Derivative'
 *    '<S12>/Filtered Derivative'
 *    '<S13>/Filtered Derivative'
 */
void drum_be_FilteredDerivative_Init(DW_FilteredDerivative_drum_be_T *localDW)
{
  /* InitializeConditions for DiscreteIntegrator: '<S43>/Filter' */
  localDW->Filter_DSTATE = 0.0;
}

/*
 * Outputs for atomic system:
 *    '<S2>/Filtered Derivative'
 *    '<S3>/Filtered Derivative'
 *    '<S12>/Filtered Derivative'
 *    '<S13>/Filtered Derivative'
 */
void drum_bench_FilteredDerivative(real_T rtu_u, B_FilteredDerivative_drum_ben_T
  *localB, DW_FilteredDerivative_drum_be_T *localDW, real_T rtp_N)
{
  /* DiscreteIntegrator: '<S43>/Filter' */
  localB->Filter = localDW->Filter_DSTATE;

  /* Sum: '<S43>/Sum' */
  localB->Sum = rtu_u - localB->Filter;

  /* Gain: '<S43>/Filter Coefficient' */
  localB->FilterCoefficient = rtp_N * localB->Sum;
}

/*
 * Update for atomic system:
 *    '<S2>/Filtered Derivative'
 *    '<S3>/Filtered Derivative'
 *    '<S12>/Filtered Derivative'
 *    '<S13>/Filtered Derivative'
 */
void drum__FilteredDerivative_Update(B_FilteredDerivative_drum_ben_T *localB,
  DW_FilteredDerivative_drum_be_T *localDW)
{
  /* Update for DiscreteIntegrator: '<S43>/Filter' */
  localDW->Filter_DSTATE = 0.0003 * localB->FilterCoefficient +
    localDW->Filter_DSTATE;
}

/*
 * Start for enable system:
 *    '<S112>/enabled subsystem'
 *    '<S177>/enabled subsystem'
 */
void drum_ben_enabledsubsystem_Start(B_enabledsubsystem_drum_bench_T *localB)
{
  int32_T i;

  /* VirtualOutportStart for Outport: '<S124>/Out1' */
  for (i = 0; i < 20; i++) {
    localB->In1[i] = drum_bench_ConstP.pooled54[i];
  }

  /* End of VirtualOutportStart for Outport: '<S124>/Out1' */
}

/*
 * Output and update for enable system:
 *    '<S112>/enabled subsystem'
 *    '<S177>/enabled subsystem'
 */
void drum_bench_enabledsubsystem(boolean_T rtu_0, const uint8_T rtu_1[20],
  B_enabledsubsystem_drum_bench_T *localB)
{
  int32_T i;

  /* Outputs for Enabled SubSystem: '<S112>/enabled subsystem' incorporates:
   *  EnablePort: '<S124>/Enable'
   */
  if (rtu_0) {
    /* Inport: '<S124>/In1' */
    for (i = 0; i < 20; i++) {
      localB->In1[i] = rtu_1[i];
    }

    /* End of Inport: '<S124>/In1' */
  }

  /* End of Outputs for SubSystem: '<S112>/enabled subsystem' */
}

/*
 * Start for enable system:
 *    '<S113>/Enabled Subsystem'
 *    '<S178>/Enabled Subsystem'
 */
void drum_ben_EnabledSubsystem_Start(B_EnabledSubsystem_drum_bench_T *localB)
{
  int32_T i;

  /* VirtualOutportStart for Outport: '<S126>/Out1' */
  for (i = 0; i < 20; i++) {
    localB->In1[i] = drum_bench_ConstP.pooled54[i];
  }

  /* End of VirtualOutportStart for Outport: '<S126>/Out1' */
}

/*
 * Output and update for enable system:
 *    '<S113>/Enabled Subsystem'
 *    '<S178>/Enabled Subsystem'
 */
void drum_bench_EnabledSubsystem(boolean_T rtu_0, const uint8_T rtu_1[20],
  B_EnabledSubsystem_drum_bench_T *localB)
{
  int32_T i;

  /* Outputs for Enabled SubSystem: '<S113>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S126>/Enable'
   */
  if (rtu_0) {
    /* Inport: '<S126>/In1' */
    for (i = 0; i < 20; i++) {
      localB->In1[i] = rtu_1[i];
    }

    /* End of Inport: '<S126>/In1' */
  }

  /* End of Outputs for SubSystem: '<S113>/Enabled Subsystem' */
}

/* Function for MATLAB Function: '<S127>/MATLAB Function1' */
static void drum_bench_twister_state_vector(uint32_T mt[625], real_T seed)
{
  uint32_T r;
  int32_T mti;
  real_T tmp;
  tmp = seed;
  if (tmp < 4.294967296E+9) {
    if (tmp >= 0.0) {
      r = (uint32_T)tmp;
    } else {
      r = 0U;
    }
  } else {
    r = MAX_uint32_T;
  }

  mt[0] = r;
  for (mti = 0; mti < 623; mti++) {
    r = ((r >> 30U ^ r) * 1812433253U + mti) + 1U;
    mt[mti + 1] = r;
  }

  mt[624] = 624U;
}

/* Function for MATLAB Function: '<S127>/MATLAB Function1' */
static real_T drum_bench_eml_rand_mt19937ar(uint32_T state[625])
{
  real_T r;
  real_T b_r;
  uint32_T u[2];
  uint32_T mti;
  uint32_T y;
  int32_T j;
  int32_T k;
  boolean_T b_isvalid;
  int32_T exitg;
  boolean_T exitg_0;

  /* ========================= COPYRIGHT NOTICE ============================ */
  /*  This is a uniform (0,1) pseudorandom number generator based on:        */
  /*                                                                         */
  /*  A C-program for MT19937, with initialization improved 2002/1/26.       */
  /*  Coded by Takuji Nishimura and Makoto Matsumoto.                        */
  /*                                                                         */
  /*  Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,      */
  /*  All rights reserved.                                                   */
  /*                                                                         */
  /*  Redistribution and use in source and binary forms, with or without     */
  /*  modification, are permitted provided that the following conditions     */
  /*  are met:                                                               */
  /*                                                                         */
  /*    1. Redistributions of source code must retain the above copyright    */
  /*       notice, this list of conditions and the following disclaimer.     */
  /*                                                                         */
  /*    2. Redistributions in binary form must reproduce the above copyright */
  /*       notice, this list of conditions and the following disclaimer      */
  /*       in the documentation and/or other materials provided with the     */
  /*       distribution.                                                     */
  /*                                                                         */
  /*    3. The names of its contributors may not be used to endorse or       */
  /*       promote products derived from this software without specific      */
  /*       prior written permission.                                         */
  /*                                                                         */
  /*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
  /*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
  /*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
  /*  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT  */
  /*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
  /*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
  /*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
  /*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
  /*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
  /*  (INCLUDING  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE */
  /*  OF THIS  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  */
  /*                                                                         */
  /* =============================   END   ================================= */
  do {
    exitg = 0;
    u[0] = 0U;
    u[1] = 0U;
    for (j = 0; j < 2; j++) {
      mti = state[624] + 1U;
      if (mti >= 625U) {
        for (k = 0; k < 227; k++) {
          y = (state[1 + k] & 2147483647U) | (state[k] & 2147483648U);
          if ((int32_T)(y & 1U) == 0) {
            mti = y >> 1U;
          } else {
            mti = y >> 1U ^ 2567483615U;
          }

          state[k] = state[397 + k] ^ mti;
        }

        for (k = 0; k < 396; k++) {
          y = (state[k + 227] & 2147483648U) | (state[228 + k] & 2147483647U);
          if ((int32_T)(y & 1U) == 0) {
            mti = y >> 1U;
          } else {
            mti = y >> 1U ^ 2567483615U;
          }

          state[k + 227] = state[k] ^ mti;
        }

        y = (state[623] & 2147483648U) | (state[0] & 2147483647U);
        if ((int32_T)(y & 1U) == 0) {
          mti = y >> 1U;
        } else {
          mti = y >> 1U ^ 2567483615U;
        }

        state[623] = state[396] ^ mti;
        mti = 1U;
      }

      y = state[(int32_T)mti - 1];
      state[624] = mti;
      y ^= y >> 11U;
      y ^= y << 7U & 2636928640U;
      y ^= y << 15U & 4022730752U;
      y ^= y >> 18U;
      u[j] = y;
    }

    b_r = ((real_T)(u[0] >> 5U) * 6.7108864E+7 + (real_T)(u[1] >> 6U)) *
      1.1102230246251565E-16;
    if (b_r == 0.0) {
      if ((state[624] >= 1U) && (state[624] < 625U)) {
        b_isvalid = TRUE;
      } else {
        b_isvalid = FALSE;
      }

      if (b_isvalid) {
        b_isvalid = FALSE;
        k = 1;
        exitg_0 = FALSE;
        while ((!exitg_0) && (k < 625)) {
          if (state[k - 1] == 0U) {
            k++;
          } else {
            b_isvalid = TRUE;
            exitg_0 = TRUE;
          }
        }
      }

      if (!b_isvalid) {
        drum_bench_twister_state_vector(state, 5489.0);
      }
    } else {
      exitg = 1;
    }
  } while (exitg == 0);

  r = b_r;
  return r;
}

/* Function for MATLAB Function: '<S127>/MATLAB Function1' */
static real_T drum_bench_rand(DW_MATLABFunction1_drum_bench_T *localDW)
{
  real_T r;
  real_T b;
  int32_T hi;
  uint32_T test;
  uint32_T test_0;
  if (localDW->method == 4U) {
    hi = (int32_T)(localDW->state / 127773U);
    test = (localDW->state - hi * 127773U) * 16807U;
    test_0 = 2836U * hi;
    if (test < test_0) {
      test = (test - test_0) + 2147483647U;
    } else {
      test -= test_0;
    }

    localDW->state = test;
    r = (real_T)test * 4.6566128752457969E-10;
  } else if (localDW->method == 5U) {
    test = 69069U * localDW->state_g[0] + 1234567U;
    test_0 = localDW->state_g[1] << 13 ^ localDW->state_g[1];
    test_0 ^= test_0 >> 17;
    test_0 ^= test_0 << 5;
    localDW->state_g[0] = test;
    localDW->state_g[1] = test_0;
    r = (real_T)(test + test_0) * 2.328306436538696E-10;
  } else {
    if (!localDW->state_not_empty) {
      memset(&localDW->state_gf[0], 0, 625U * sizeof(uint32_T));
      drum_bench_twister_state_vector(localDW->state_gf, 5489.0);
      localDW->state_not_empty = TRUE;
    }

    b = drum_bench_eml_rand_mt19937ar(localDW->state_gf);
    r = b;
  }

  return r;
}

real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/*
 * Initial conditions for atomic system:
 *    '<S127>/MATLAB Function1'
 *    '<S192>/MATLAB Function1'
 */
void drum_bench_MATLABFunction1_Init(DW_MATLABFunction1_drum_bench_T *localDW)
{
  localDW->state_not_empty = FALSE;
  localDW->i = 0.0;
  localDW->id_sent = 0.0F;
  localDW->state = 1144108930U;
  localDW->state_g[0] = 362436069U;
  localDW->state_g[1] = 521288629U;
  localDW->method = 7U;
}

/*
 * Output and update for atomic system:
 *    '<S127>/MATLAB Function1'
 *    '<S192>/MATLAB Function1'
 */
void drum_bench_MATLABFunction1(const real32_T rtu_u[76], const
  MarlinFullInternalRPA *rtu_rpa, uint32_T rtu_rpc_type,
  B_MATLABFunction1_drum_bench_T *localB, DW_MATLABFunction1_drum_bench_T
  *localDW)
{
  uint32_T rpc_type;
  int32_T tmp;
  real_T tmp_0;
  rpc_type = rtu_rpc_type;

  /* MATLAB Function 'dsp param iterator/MATLAB Function1': '<S129>:1' */
  if ((((real_T)rtu_rpa->type == rtu_rpc_type) && (rtu_rpa->id ==
        localDW->id_sent)) || (localDW->id_sent == 0.0F)) {
    /* '<S129>:1:10' */
    /* '<S129>:1:11' */
    localDW->i++;
    if (localDW->i > 76.0) {
      /* '<S129>:1:12' */
      /* '<S129>:1:13' */
      localDW->i = 1.0;
    }

    /* '<S129>:1:15' */
    localDW->id_sent = (real32_T)drum_bench_rand(localDW);
  }

  /* '<S129>:1:18' */
  /* '<S129>:1:19' */
  /* '<S129>:1:20' */
  /* '<S129>:1:21' */
  if (rpc_type > 2147483647U) {
    rpc_type = 2147483647U;
  }

  localB->type = (int32_T)rpc_type;
  localB->id = localDW->id_sent;
  tmp_0 = rt_roundd_snf(localDW->i);
  if (tmp_0 < 2.147483648E+9) {
    if (tmp_0 >= -2.147483648E+9) {
      tmp = (int32_T)tmp_0;
    } else {
      tmp = MIN_int32_T;
    }
  } else {
    tmp = MAX_int32_T;
  }

  localB->index = tmp;
  localB->value = rtu_u[(int32_T)localDW->i - 1];
}

/*
 * Output and update for atomic system:
 *    '<S94>/select rpc'
 *    '<S159>/select rpc'
 */
void drum_bench_selectrpc(real_T rtu_rpc_command, const MarlinFullInternalRPC
  rtu_u[15], B_selectrpc_drum_bench_T *localB)
{
  int32_T i;

  /* MATLAB Function 'marlin_1.1_full/rpc_generator/select rpc': '<S118>:1' */
  /* active_id = find(u.request,1,'first'); */
  /*  active_id = 0; */
  /*  for i = 1:length(u) */
  /*      if (u(i).request) */
  /*          active_id = i; */
  /*          break; */
  /*      end */
  /*  end */
  if ((rtu_rpc_command != 0.0) && (rtu_rpc_command >= 1.0) && (rtu_rpc_command <
       15.0)) {
    /* '<S118>:1:17' */
    /* '<S118>:1:18' */
    localB->type = rtu_u[(int32_T)rtu_rpc_command - 1].type;

    /* '<S118>:1:19' */
    localB->id = rtu_u[(int32_T)rtu_rpc_command - 1].id;

    /* '<S118>:1:20' */
    for (i = 0; i < 20; i++) {
      localB->data[i] = rtu_u[(int32_T)rtu_rpc_command - 1].data[i];
    }
  } else {
    /* '<S118>:1:22' */
    localB->type = 0;

    /* '<S118>:1:23' */
    localB->id = 0.0F;

    /* '<S118>:1:24' */
    for (i = 0; i < 20; i++) {
      localB->data[i] = 0U;
    }
  }
}

real_T rt_powd_snf(real_T u0, real_T u1)
{
  real_T y;
  real_T tmp;
  real_T tmp_0;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else {
    tmp = fabs(u0);
    tmp_0 = fabs(u1);
    if (rtIsInf(u1)) {
      if (tmp == 1.0) {
        y = (rtNaN);
      } else if (tmp > 1.0) {
        if (u1 > 0.0) {
          y = (rtInf);
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = (rtInf);
      }
    } else if (tmp_0 == 0.0) {
      y = 1.0;
    } else if (tmp_0 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > floor(u1))) {
      y = (rtNaN);
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

void MWDSPCG_FFT_Interleave_R2BR_D(const real_T x[], creal_T y[], const int32_T
  nChans, const int32_T nRows)
{
  int32_T br_j;
  int32_T yIdx;
  int32_T uIdx;
  int32_T j;
  int32_T nChansBy;
  int32_T bit_fftLen;

  /* S-Function (sdspfft2): '<S32>/FFT' */
  /* Bit-reverses the input data simultaneously with the interleaving operation,
     obviating the need for explicit data reordering later.  This requires an
     FFT with bit-reversed inputs.
   */
  br_j = 0;
  yIdx = 0;
  uIdx = 0;
  for (nChansBy = nChans >> 1; nChansBy != 0; nChansBy--) {
    for (j = nRows; j - 1 > 0; j--) {
      y[yIdx + br_j].re = x[uIdx];
      y[yIdx + br_j].im = x[uIdx + nRows];
      uIdx++;

      /* Compute next bit-reversed destination index */
      bit_fftLen = nRows;
      do {
        bit_fftLen = (int32_T)((uint32_T)bit_fftLen >> 1);
        br_j ^= bit_fftLen;
      } while (!((br_j & bit_fftLen) != 0));
    }

    y[yIdx + br_j].re = x[uIdx];
    y[yIdx + br_j].im = x[uIdx + nRows];
    uIdx = (uIdx + nRows) + 1;
    yIdx += nRows << 1;
    br_j = 0;
  }

  /* For an odd number of channels, prepare the last channel
     for a double-length real signal algorithm.  No actual
     interleaving is required, just a copy of the last column
     of real data, but now placed in bit-reversed order.
     We need to cast the real u pointer to a cDType_T pointer,
     in order to fake the interleaving, and cut the number
     of elements in half (half as many complex interleaved
     elements as compared to real non-interleaved elements).
   */
  if ((nChans & 1) != 0) {
    for (j = nRows >> 1; j - 1 > 0; j--) {
      y[yIdx + br_j].re = x[uIdx];
      y[yIdx + br_j].im = x[uIdx + 1];
      uIdx += 2;

      /* Compute next bit-reversed destination index */
      bit_fftLen = nRows >> 1;
      do {
        bit_fftLen = (int32_T)((uint32_T)bit_fftLen >> 1);
        br_j ^= bit_fftLen;
      } while (!((br_j & bit_fftLen) != 0));
    }

    y[yIdx + br_j].re = x[uIdx];
    y[yIdx + br_j].im = x[uIdx + 1];
  }

  /* End of S-Function (sdspfft2): '<S32>/FFT' */
}

void MWDSPCG_R2DIT_TBLS_Z(creal_T y[], const int32_T nChans, const int32_T nRows,
  const int32_T fftLen, const int32_T offset, const real_T tablePtr[], const
  int32_T twiddleStep, const boolean_T isInverse)
{
  int32_T nHalf;
  real_T twidRe;
  real_T twidIm;
  int32_T nQtr;
  int32_T fwdInvFactor;
  int32_T iCh;
  int32_T offsetCh;
  int32_T idelta;
  int32_T ix;
  int32_T k;
  int32_T kratio;
  int32_T istart;
  int32_T i;
  int32_T j;
  int32_T i_0;
  real_T tmp_re;
  real_T tmp_im;

  /* S-Function (sdspfft2): '<S32>/FFT' */
  /* DSP System Toolbox Decimation in Time FFT  */
  /* Computation performed using table lookup  */
  /* Output type: complex real_T */
  nHalf = fftLen >> 1;
  nHalf *= twiddleStep;
  nQtr = nHalf >> 1;
  fwdInvFactor = isInverse ? -1 : 1;

  /* For each channel */
  offsetCh = offset;
  for (iCh = 0; iCh < nChans; iCh++) {
    /* Perform butterflies for the first stage, where no multiply is required. */
    for (ix = offsetCh; ix < (fftLen + offsetCh) - 1; ix += 2) {
      i_0 = ix + 1;
      tmp_re = y[i_0].re;
      tmp_im = y[i_0].im;
      y[i_0].re = y[ix].re - tmp_re;
      y[i_0].im = y[ix].im - tmp_im;
      y[ix].re += tmp_re;
      y[ix].im += tmp_im;
    }

    idelta = 2;
    k = fftLen >> 2;
    kratio = k * twiddleStep;
    while (k > 0) {
      istart = offsetCh;
      i = istart;

      /* Perform the first butterfly in each remaining stage, where no multiply is required. */
      for (ix = 0; ix < k; ix++) {
        i_0 = i + idelta;
        tmp_re = y[i_0].re;
        tmp_im = y[i_0].im;
        y[i_0].re = y[i].re - tmp_re;
        y[i_0].im = y[i].im - tmp_im;
        y[i].re += tmp_re;
        y[i].im += tmp_im;
        i += idelta << 1;
      }

      /* Perform remaining butterflies */
      for (j = kratio; j < nHalf; j += kratio) {
        i = istart + 1;
        twidRe = tablePtr[j];
        twidIm = tablePtr[j + nQtr] * (real_T)fwdInvFactor;
        for (ix = 0; ix < k; ix++) {
          i_0 = i + idelta;
          tmp_re = y[i_0].re * twidRe - y[i_0].im * twidIm;
          tmp_im = y[i_0].re * twidIm + y[i_0].im * twidRe;
          y[i_0].re = y[i].re - tmp_re;
          y[i_0].im = y[i].im - tmp_im;
          y[i].re += tmp_re;
          y[i].im += tmp_im;
          i += idelta << 1;
        }

        istart++;
      }

      idelta <<= 1;
      k >>= 1;
      kratio >>= 1;
    }

    /* Point to next channel */
    offsetCh += nRows;
  }

  /* End of S-Function (sdspfft2): '<S32>/FFT' */
}

void MWDSPCG_FFT_DblLen_Z(creal_T y[], const int32_T nChans, const int32_T nRows,
  const real_T twiddleTable[], const int32_T twiddleStep)
{
  real_T accRe;
  real_T tempOut0Im;
  real_T tempOut1Re;
  real_T tempOut1Im;
  real_T temp2Re;
  int32_T N;
  int32_T N_0;
  int32_T W;
  int32_T yIdx;
  int32_T ix;
  int32_T k;
  real_T cTemp_re;

  /* S-Function (sdspfft2): '<S32>/FFT' */
  /* In-place "double-length" data recovery
     Table-based mem-optimized twiddle computation

     Used to recover linear-ordered length-N point complex FFT result
     from a linear-ordered complex length-N/2 point FFT, performed
     on N interleaved real values.
   */
  N = nRows >> 1;
  N_0 = N >> 1;
  W = N_0 * twiddleStep;
  yIdx = (nChans - 1) * nRows;
  if (nRows > 2) {
    temp2Re = y[N_0 + yIdx].re;
    tempOut0Im = y[N_0 + yIdx].im;
    y[(N + N_0) + yIdx].re = temp2Re;
    y[(N + N_0) + yIdx].im = tempOut0Im;
    y[N_0 + yIdx].re = temp2Re;
    y[N_0 + yIdx].im = -tempOut0Im;
  }

  if (nRows > 1) {
    accRe = y[yIdx].re;
    accRe -= y[yIdx].im;
    y[N + yIdx].re = accRe;
    y[N + yIdx].im = 0.0;
  }

  accRe = y[yIdx].re;
  accRe += y[yIdx].im;
  y[yIdx].re = accRe;
  y[yIdx].im = 0.0;
  k = twiddleStep;
  for (ix = 1; ix < N_0; ix++) {
    accRe = y[ix + yIdx].re;
    accRe += y[(N - ix) + yIdx].re;
    accRe /= 2.0;
    temp2Re = accRe;
    accRe = y[ix + yIdx].im;
    accRe -= y[(N - ix) + yIdx].im;
    accRe /= 2.0;
    tempOut0Im = accRe;
    accRe = y[ix + yIdx].im;
    accRe += y[(N - ix) + yIdx].im;
    accRe /= 2.0;
    tempOut1Re = accRe;
    accRe = y[(N - ix) + yIdx].re;
    accRe -= y[ix + yIdx].re;
    accRe /= 2.0;
    y[ix + yIdx].re = tempOut1Re;
    y[ix + yIdx].im = accRe;
    cTemp_re = y[ix + yIdx].re * twiddleTable[k] - -twiddleTable[W - k] * y[ix +
      yIdx].im;
    accRe = y[ix + yIdx].im * twiddleTable[k] + -twiddleTable[W - k] * y[ix +
      yIdx].re;
    tempOut1Re = cTemp_re;
    tempOut1Im = accRe;
    accRe = temp2Re;
    cTemp_re = tempOut0Im;
    accRe += tempOut1Re;
    cTemp_re += tempOut1Im;
    y[ix + yIdx].re = accRe;
    y[ix + yIdx].im = cTemp_re;
    cTemp_re = y[ix + yIdx].re;
    accRe = -y[ix + yIdx].im;
    y[(nRows - ix) + yIdx].re = cTemp_re;
    y[(nRows - ix) + yIdx].im = accRe;
    accRe = temp2Re;
    cTemp_re = tempOut0Im;
    accRe -= tempOut1Re;
    cTemp_re -= tempOut1Im;
    y[(N + ix) + yIdx].re = accRe;
    y[(N + ix) + yIdx].im = cTemp_re;
    accRe = y[(N + ix) + yIdx].re;
    cTemp_re = y[(N + ix) + yIdx].im;
    y[(N - ix) + yIdx].re = accRe;
    y[(N - ix) + yIdx].im = -cTemp_re;
    k += twiddleStep;
  }

  /* End of S-Function (sdspfft2): '<S32>/FFT' */
}

/* Model output function */
static void drum_bench_output(void)
{
  int32_T currentOffset;
  real_T output;
  real_T x[40];
  real_T x_0[20];
  int32_T frameSize;
  int32_T idxN;
  int32_T idxN_0;
  real_T acc;
  ZCEventType zcEvent;
  int32_T i;
  creal_T u;
  real_T u_0;

  /* user code (Output function Body) */

  /* EtherCAT Process for Sample Time [0.0003] */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0 && rtmIsMajorTimeStep
      (drum_bench_M)) {
    ecs_receive(1);

#ifdef ASYNC_ECAT

    ecs_send(1);

#endif

  }

  /* EtherCAT Process for Sample Time [9.9999999999999991E-5] */
  if (1 && rtmIsMajorTimeStep(drum_bench_M)) {
    ecs_receive(0);

#ifdef ASYNC_ECAT

    ecs_send(0);

#endif

  }

  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* S-Function Block: <S95>/marlin_ec */
    /* Output Port 1 written directly by slave */
    /* Output Port 2 written directly by slave */
    /* Output Port 3 written directly by slave */
    /* Output Port 4 written directly by slave */
    /* Output Port 5 written directly by slave */
    /* Output Port 6 written directly by slave */
    /* Output Port 7 written directly by slave */
    /* Output Port 8 written directly by slave */
    /* Output Port 9 written directly by slave */
    /* Output Port 10 written directly by slave */
    /* Output Port 11 written directly by slave */
    /* Output Port 12 written directly by slave */
    /* Output Port 13 written directly by slave */
    /* Output Port 14 written directly by slave */
    /* Output Port 15 written directly by slave */
    /* Output Port 16 written directly by slave */
    /* Output Port 17 written directly by slave */
    /* Output Port 18 written directly by slave */
    /* Output Port 19 written directly by slave */
    /* Output Port 20 written directly by slave */
    /* Output Port 21 written directly by slave */
    /* Output Port 22 written directly by slave */

    /* DataTypeConversion: '<S95>/motor_torque_' */
    drum_bench_B.motor_torque = drum_bench_B.marlin_ec_o1;

    /* DataTypeConversion: '<S95>/motor_iq_' */
    drum_bench_B.motor_iq = drum_bench_B.marlin_ec_o2;

    /* DataTypeConversion: '<S95>/motor_vq_avg_' */
    drum_bench_B.motor_vq_avg = drum_bench_B.marlin_ec_o3;

    /* DataTypeConversion: '<S95>/analog_1_' */
    drum_bench_B.analog_1 = drum_bench_B.marlin_ec_o4;

    /* DataTypeConversion: '<S95>/analog_1_dot_' */
    drum_bench_B.analog_1_dot = drum_bench_B.marlin_ec_o5;

    /* DataTypeConversion: '<S95>/analog_2_' */
    drum_bench_B.analog_2 = drum_bench_B.marlin_ec_o6;

    /* DataTypeConversion: '<S95>/analog_2_dot_' */
    drum_bench_B.analog_2_dot = drum_bench_B.marlin_ec_o7;

    /* DataTypeConversion: '<S95>/analog_diff_' */
    drum_bench_B.analog_diff = drum_bench_B.marlin_ec_o8;

    /* DataTypeConversion: '<S95>/analog_diff_dot_' */
    drum_bench_B.analog_diff_dot = drum_bench_B.marlin_ec_o9;

    /* DataTypeConversion: '<S95>/quadrature_1_' */
    drum_bench_B.quadrature_1 = drum_bench_B.marlin_ec_o10;

    /* DataTypeConversion: '<S95>/quadrature_1_dot_' */
    drum_bench_B.quadrature_1_dot = drum_bench_B.marlin_ec_o11;

    /* DataTypeConversion: '<S95>/quadrature_2_' */
    drum_bench_B.quadrature_2 = drum_bench_B.marlin_ec_o12;

    /* DataTypeConversion: '<S95>/quadrature_2_dot_' */
    drum_bench_B.quadrature_2_dot = drum_bench_B.marlin_ec_o13;

    /* DataTypeConversion: '<S95>/ssi_' */
    drum_bench_B.ssi = drum_bench_B.marlin_ec_o14;

    /* DataTypeConversion: '<S95>/ssi_dot_' */
    drum_bench_B.ssi_dot = drum_bench_B.marlin_ec_o15;

    /* DataTypeConversion: '<S95>/accelerometer_x_' */
    drum_bench_B.accelerometer_x = drum_bench_B.marlin_ec_o16;

    /* DataTypeConversion: '<S95>/accelerometer_y_' */
    drum_bench_B.accelerometer_y = drum_bench_B.marlin_ec_o17;

    /* DataTypeConversion: '<S95>/accelerometer_z_' */
    drum_bench_B.accelerometer_z = drum_bench_B.marlin_ec_o18;

    /* DataTypeConversion: '<S95>/faults_' */
    drum_bench_B.faults = drum_bench_B.marlin_ec_o19;

    /* DataTypeConversion: '<S95>/bus_v_' */
    drum_bench_B.bus_v = drum_bench_B.marlin_ec_o20;

    /* BusCreator: '<S8>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */
    drum_bench_B.BusConversion_InsertedFor_BusCr.motor_torque =
      drum_bench_B.motor_torque;
    drum_bench_B.BusConversion_InsertedFor_BusCr.motor_iq =
      drum_bench_B.motor_iq;
    drum_bench_B.BusConversion_InsertedFor_BusCr.motor_vq_avg =
      drum_bench_B.motor_vq_avg;
    drum_bench_B.BusConversion_InsertedFor_BusCr.analog_1 =
      drum_bench_B.analog_1;
    drum_bench_B.BusConversion_InsertedFor_BusCr.analog_1_dot =
      drum_bench_B.analog_1_dot;
    drum_bench_B.BusConversion_InsertedFor_BusCr.analog_2 =
      drum_bench_B.analog_2;
    drum_bench_B.BusConversion_InsertedFor_BusCr.analog_2_dot =
      drum_bench_B.analog_2_dot;
    drum_bench_B.BusConversion_InsertedFor_BusCr.analog_diff =
      drum_bench_B.analog_diff;
    drum_bench_B.BusConversion_InsertedFor_BusCr.analog_diff_dot =
      drum_bench_B.analog_diff_dot;
    drum_bench_B.BusConversion_InsertedFor_BusCr.quadrature_1 =
      drum_bench_B.quadrature_1;
    drum_bench_B.BusConversion_InsertedFor_BusCr.quadrature_1_dot =
      drum_bench_B.quadrature_1_dot;
    drum_bench_B.BusConversion_InsertedFor_BusCr.quadrature_2 =
      drum_bench_B.quadrature_2;
    drum_bench_B.BusConversion_InsertedFor_BusCr.quadrature_2_dot =
      drum_bench_B.quadrature_2_dot;
    drum_bench_B.BusConversion_InsertedFor_BusCr.ssi = drum_bench_B.ssi;
    drum_bench_B.BusConversion_InsertedFor_BusCr.ssi_dot = drum_bench_B.ssi_dot;
    drum_bench_B.BusConversion_InsertedFor_BusCr.accelerometer_x =
      drum_bench_B.accelerometer_x;
    drum_bench_B.BusConversion_InsertedFor_BusCr.accelerometer_y =
      drum_bench_B.accelerometer_y;
    drum_bench_B.BusConversion_InsertedFor_BusCr.accelerometer_z =
      drum_bench_B.accelerometer_z;
    drum_bench_B.BusConversion_InsertedFor_BusCr.faults = drum_bench_B.faults;
    drum_bench_B.BusConversion_InsertedFor_BusCr.bus_v = drum_bench_B.bus_v;
    for (i = 0; i < 28; i++) {
      /* DataTypeConversion: '<S95>/rpa_packet_' */
      drum_bench_B.rpa_packet[i] = drum_bench_B.marlin_ec_o22[i];
      drum_bench_B.BusConversion_InsertedFor_BusCr.rpa_packet[i] =
        drum_bench_B.rpa_packet[i];
    }

    /* End of BusCreator: '<S8>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */

    /* Delay: '<S93>/Delay' */
    drum_bench_B.Delay[0] = drum_bench_DW.Delay_DSTATE[0];
    drum_bench_B.Delay[1] = drum_bench_DW.Delay_DSTATE[1];

    /* Gain: '<S93>/eA1' */
    drum_bench_B.eA1[0] = 0.0;
    drum_bench_B.eA1[0] +=
      drum_bench_P.config.marlins.M0.motor.thermal_model.eA1[0] *
      drum_bench_B.Delay[0];
    drum_bench_B.eA1[0] +=
      drum_bench_P.config.marlins.M0.motor.thermal_model.eA1[2] *
      drum_bench_B.Delay[1];
    drum_bench_B.eA1[1] = 0.0;
    drum_bench_B.eA1[1] +=
      drum_bench_P.config.marlins.M0.motor.thermal_model.eA1[1] *
      drum_bench_B.Delay[0];
    drum_bench_B.eA1[1] +=
      drum_bench_P.config.marlins.M0.motor.thermal_model.eA1[3] *
      drum_bench_B.Delay[1];

    /* Math: '<S93>/Math Function'
     *
     * About '<S93>/Math Function':
     *  Operator: magnitude^2
     */
    output = drum_bench_B.motor_iq;
    drum_bench_B.MathFunction = output * output;

    /* Gain: '<S93>/resistance' */
    drum_bench_B.resistance = drum_bench_P.config.marlins.M0.motor.resistance *
      drum_bench_B.MathFunction;

    /* Gain: '<S93>/eA2' */
    drum_bench_B.eA2[0] =
      drum_bench_P.config.marlins.M0.motor.thermal_model.eA2[0] *
      drum_bench_B.resistance;
    drum_bench_B.eA2[1] =
      drum_bench_P.config.marlins.M0.motor.thermal_model.eA2[1] *
      drum_bench_B.resistance;

    /* Sum: '<S93>/Sum' */
    drum_bench_B.Sum[0] = drum_bench_B.eA1[0] + drum_bench_B.eA2[0];
    drum_bench_B.Sum[1] = drum_bench_B.eA1[1] + drum_bench_B.eA2[1];

    /* BusCreator: '<S8>/Bus Creator' */
    drum_bench_B.BusCreator.marlin_basic_status =
      drum_bench_B.BusConversion_InsertedFor_BusCr;
    drum_bench_B.BusCreator.temperature_motor_winding = drum_bench_B.Sum[0];
    drum_bench_B.BusCreator.temperature_motor_housing = drum_bench_B.Sum[1];

    /* BusSelector: '<S10>/Bus Selector1' */
    drum_bench_B.marlin_basic_status =
      drum_bench_B.BusCreator.marlin_basic_status;
    drum_bench_B.temperature_motor_winding =
      drum_bench_B.BusCreator.temperature_motor_winding;
    drum_bench_B.temperature_motor_housing =
      drum_bench_B.BusCreator.temperature_motor_housing;

    /* BusSelector: '<S221>/Bus Selector' */
    drum_bench_B.motor_torque_b = drum_bench_B.marlin_basic_status.motor_torque;
    drum_bench_B.motor_iq_k = drum_bench_B.marlin_basic_status.motor_iq;
    drum_bench_B.motor_vq_avg_h = drum_bench_B.marlin_basic_status.motor_vq_avg;
    drum_bench_B.analog_1_k = drum_bench_B.marlin_basic_status.analog_1;
    drum_bench_B.analog_1_dot_e = drum_bench_B.marlin_basic_status.analog_1_dot;
    drum_bench_B.analog_2_o = drum_bench_B.marlin_basic_status.analog_2;
    drum_bench_B.analog_2_dot_i = drum_bench_B.marlin_basic_status.analog_2_dot;
    drum_bench_B.analog_diff_g = drum_bench_B.marlin_basic_status.analog_diff;
    drum_bench_B.analog_diff_dot_b =
      drum_bench_B.marlin_basic_status.analog_diff_dot;
    drum_bench_B.quadrature_1_h = drum_bench_B.marlin_basic_status.quadrature_1;
    drum_bench_B.quadrature_1_dot_m =
      drum_bench_B.marlin_basic_status.quadrature_1_dot;
    drum_bench_B.quadrature_2_m = drum_bench_B.marlin_basic_status.quadrature_2;
    drum_bench_B.quadrature_2_dot_h =
      drum_bench_B.marlin_basic_status.quadrature_2_dot;
    drum_bench_B.ssi_g = drum_bench_B.marlin_basic_status.ssi;
    drum_bench_B.ssi_dot_g = drum_bench_B.marlin_basic_status.ssi_dot;
    drum_bench_B.accelerometer_x_f =
      drum_bench_B.marlin_basic_status.accelerometer_x;
    drum_bench_B.accelerometer_y_n =
      drum_bench_B.marlin_basic_status.accelerometer_y;
    drum_bench_B.accelerometer_z_m =
      drum_bench_B.marlin_basic_status.accelerometer_z;
    drum_bench_B.faults_c = drum_bench_B.marlin_basic_status.faults;
    drum_bench_B.bus_v_i = drum_bench_B.marlin_basic_status.bus_v;
    for (i = 0; i < 28; i++) {
      drum_bench_B.rpa_packet_n[i] =
        drum_bench_B.marlin_basic_status.rpa_packet[i];
    }

    /* End of BusSelector: '<S221>/Bus Selector' */

    /* DiscreteFir: '<S223>/Generated Filter Block' */
    idxN = drum_bench_DW.GeneratedFilterBlock_circBuf;
    i = 0;
    while (i < 1) {
      i = 1;
      acc = drum_bench_B.accelerometer_x_f * (-0.0019411429428760618);
      for (frameSize = idxN; frameSize < 23; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      for (frameSize = 0; frameSize < idxN; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      idxN--;
      if (idxN < 0) {
        idxN = 22;
      }

      drum_bench_DW.GeneratedFilterBlock_states[idxN] =
        drum_bench_B.accelerometer_x_f;
      drum_bench_B.GeneratedFilterBlock = acc;
      i = 1;
    }

    drum_bench_DW.GeneratedFilterBlock_circBuf = idxN;

    /* End of DiscreteFir: '<S223>/Generated Filter Block' */

    /* DiscreteFir: '<S222>/Generated Filter Block' */
    idxN = drum_bench_DW.GeneratedFilterBlock_circBuf_n;
    i = 0;
    while (i < 1) {
      i = 1;
      acc = drum_bench_B.accelerometer_y_n * (-0.0019411429428760618);
      for (frameSize = idxN; frameSize < 23; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_f[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      for (frameSize = 0; frameSize < idxN; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_f[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      idxN--;
      if (idxN < 0) {
        idxN = 22;
      }

      drum_bench_DW.GeneratedFilterBlock_states_f[idxN] =
        drum_bench_B.accelerometer_y_n;
      drum_bench_B.GeneratedFilterBlock_b = acc;
      i = 1;
    }

    drum_bench_DW.GeneratedFilterBlock_circBuf_n = idxN;

    /* End of DiscreteFir: '<S222>/Generated Filter Block' */

    /* DiscreteFir: '<S224>/Generated Filter Block' */
    idxN = drum_bench_DW.GeneratedFilterBlock_circBuf_b;
    i = 0;
    while (i < 1) {
      i = 1;
      acc = drum_bench_B.accelerometer_z_m * (-0.0019411429428760618);
      for (frameSize = idxN; frameSize < 23; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_c[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      for (frameSize = 0; frameSize < idxN; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_c[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      idxN--;
      if (idxN < 0) {
        idxN = 22;
      }

      drum_bench_DW.GeneratedFilterBlock_states_c[idxN] =
        drum_bench_B.accelerometer_z_m;
      drum_bench_B.GeneratedFilterBlock_c = acc;
      i = 1;
    }

    drum_bench_DW.GeneratedFilterBlock_circBuf_b = idxN;

    /* End of DiscreteFir: '<S224>/Generated Filter Block' */

    /* BusCreator: '<S10>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */
    drum_bench_B.BusConversion_InsertedFor_Bus_m.motor_torque =
      drum_bench_B.motor_torque_b;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.motor_iq =
      drum_bench_B.motor_iq_k;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.motor_vq_avg =
      drum_bench_B.motor_vq_avg_h;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.analog_1 =
      drum_bench_B.analog_1_k;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.analog_1_dot =
      drum_bench_B.analog_1_dot_e;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.analog_2 =
      drum_bench_B.analog_2_o;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.analog_2_dot =
      drum_bench_B.analog_2_dot_i;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.analog_diff =
      drum_bench_B.analog_diff_g;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.analog_diff_dot =
      drum_bench_B.analog_diff_dot_b;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.quadrature_1 =
      drum_bench_B.quadrature_1_h;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.quadrature_1_dot =
      drum_bench_B.quadrature_1_dot_m;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.quadrature_2 =
      drum_bench_B.quadrature_2_m;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.quadrature_2_dot =
      drum_bench_B.quadrature_2_dot_h;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.ssi = drum_bench_B.ssi_g;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.ssi_dot =
      drum_bench_B.ssi_dot_g;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.accelerometer_x =
      drum_bench_B.GeneratedFilterBlock;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.accelerometer_y =
      drum_bench_B.GeneratedFilterBlock_b;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.accelerometer_z =
      drum_bench_B.GeneratedFilterBlock_c;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.faults = drum_bench_B.faults_c;
    drum_bench_B.BusConversion_InsertedFor_Bus_m.bus_v = drum_bench_B.bus_v_i;
    for (i = 0; i < 28; i++) {
      drum_bench_B.BusConversion_InsertedFor_Bus_m.rpa_packet[i] =
        drum_bench_B.rpa_packet_n[i];
    }

    /* End of BusCreator: '<S10>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */

    /* BusCreator: '<S10>/Bus Creator' */
    drum_bench_B.BusCreator_i.marlin_basic_status =
      drum_bench_B.BusConversion_InsertedFor_Bus_m;
    drum_bench_B.BusCreator_i.temperature_motor_winding =
      drum_bench_B.temperature_motor_winding;
    drum_bench_B.BusCreator_i.temperature_motor_housing =
      drum_bench_B.temperature_motor_housing;

    /* BusSelector: '<S6>/Bus Selector' */
    drum_bench_B.marlin_basic_status_i =
      drum_bench_B.BusCreator_i.marlin_basic_status;
    drum_bench_B.temperature_motor_winding_j =
      drum_bench_B.BusCreator_i.temperature_motor_winding;
    drum_bench_B.temperature_motor_housing_c =
      drum_bench_B.BusCreator_i.temperature_motor_housing;

    /* BusSelector: '<S65>/Bus Selector' */
    drum_bench_B.motor_torque_m =
      drum_bench_B.marlin_basic_status_i.motor_torque;
    drum_bench_B.motor_iq_a = drum_bench_B.marlin_basic_status_i.motor_iq;
    drum_bench_B.motor_vq_avg_f =
      drum_bench_B.marlin_basic_status_i.motor_vq_avg;
    drum_bench_B.analog_1_g = drum_bench_B.marlin_basic_status_i.analog_1;
    drum_bench_B.analog_1_dot_h =
      drum_bench_B.marlin_basic_status_i.analog_1_dot;
    drum_bench_B.analog_2_k = drum_bench_B.marlin_basic_status_i.analog_2;
    drum_bench_B.analog_2_dot_m =
      drum_bench_B.marlin_basic_status_i.analog_2_dot;
    drum_bench_B.analog_diff_h = drum_bench_B.marlin_basic_status_i.analog_diff;
    drum_bench_B.analog_diff_dot_k =
      drum_bench_B.marlin_basic_status_i.analog_diff_dot;
    drum_bench_B.quadrature_1_ha =
      drum_bench_B.marlin_basic_status_i.quadrature_1;
    drum_bench_B.quadrature_1_dot_l =
      drum_bench_B.marlin_basic_status_i.quadrature_1_dot;
    drum_bench_B.quadrature_2_o =
      drum_bench_B.marlin_basic_status_i.quadrature_2;
    drum_bench_B.quadrature_2_dot_l =
      drum_bench_B.marlin_basic_status_i.quadrature_2_dot;
    drum_bench_B.ssi_c = drum_bench_B.marlin_basic_status_i.ssi;
    drum_bench_B.ssi_dot_f = drum_bench_B.marlin_basic_status_i.ssi_dot;
    drum_bench_B.accelerometer_x_n =
      drum_bench_B.marlin_basic_status_i.accelerometer_x;
    drum_bench_B.accelerometer_y_a =
      drum_bench_B.marlin_basic_status_i.accelerometer_y;
    drum_bench_B.accelerometer_z_l =
      drum_bench_B.marlin_basic_status_i.accelerometer_z;
    drum_bench_B.faults_p = drum_bench_B.marlin_basic_status_i.faults;
    drum_bench_B.bus_v_k = drum_bench_B.marlin_basic_status_i.bus_v;
    for (i = 0; i < 28; i++) {
      drum_bench_B.rpa_packet_k[i] =
        drum_bench_B.marlin_basic_status_i.rpa_packet[i];
    }

    /* End of BusSelector: '<S65>/Bus Selector' */

    /* DataTypeConversion: '<S65>/accelerometer_x' */
    drum_bench_B.accelerometer_x_e = drum_bench_B.accelerometer_x_n;

    /* DataTypeConversion: '<S65>/accelerometer_y' */
    drum_bench_B.accelerometer_y_b = drum_bench_B.accelerometer_y_a;

    /* DataTypeConversion: '<S65>/accelerometer_z' */
    drum_bench_B.accelerometer_z_d = drum_bench_B.accelerometer_z_l;

    /* DataTypeConversion: '<S65>/analog_1' */
    drum_bench_B.analog_1_o = drum_bench_B.analog_1_g;

    /* DataTypeConversion: '<S65>/analog_1_dot' */
    drum_bench_B.analog_1_dot_d = drum_bench_B.analog_1_dot_h;

    /* DataTypeConversion: '<S65>/analog_2' */
    drum_bench_B.analog_2_p = drum_bench_B.analog_2_k;

    /* DataTypeConversion: '<S65>/analog_2_dot' */
    drum_bench_B.analog_2_dot_l = drum_bench_B.analog_2_dot_m;

    /* DataTypeConversion: '<S65>/analog_diff' */
    drum_bench_B.analog_diff_b = drum_bench_B.analog_diff_h;

    /* DataTypeConversion: '<S65>/analog_diff_dot' */
    drum_bench_B.analog_diff_dot_f = drum_bench_B.analog_diff_dot_k;

    /* DataTypeConversion: '<S65>/bus_v' */
    drum_bench_B.bus_v_d = drum_bench_B.bus_v_k;

    /* DataTypeConversion: '<S66>/raw_integer' */
    drum_bench_B.raw_integer = drum_bench_B.faults_p;

    /* DataTypeConversion: '<S67>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_n = (uint8_T)((uint8_T)
      drum_bench_B.raw_integer & 1);

    /* DataTypeConversion: '<S67>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly = drum_bench_B.ExtractDesiredBits_n;

    /* DataTypeConversion: '<S68>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_pr = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer >> 1) & 1);

    /* DataTypeConversion: '<S68>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_c = drum_bench_B.ExtractDesiredBits_pr;

    /* DataTypeConversion: '<S69>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_dw = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer >> 10) & 1);

    /* DataTypeConversion: '<S69>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_j = drum_bench_B.ExtractDesiredBits_dw;

    /* DataTypeConversion: '<S70>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_k = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer >> 2) & 1);

    /* DataTypeConversion: '<S70>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_b = drum_bench_B.ExtractDesiredBits_k;

    /* DataTypeConversion: '<S71>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_l = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer >> 3) & 1);

    /* DataTypeConversion: '<S71>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_jt = drum_bench_B.ExtractDesiredBits_l;

    /* DataTypeConversion: '<S72>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_d = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer >> 4) & 1);

    /* DataTypeConversion: '<S72>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_h = drum_bench_B.ExtractDesiredBits_d;

    /* DataTypeConversion: '<S73>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_h = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer >> 5) & 1);

    /* DataTypeConversion: '<S73>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_m = drum_bench_B.ExtractDesiredBits_h;

    /* DataTypeConversion: '<S74>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_ng = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer >> 6) & 1);

    /* DataTypeConversion: '<S74>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_i = drum_bench_B.ExtractDesiredBits_ng;

    /* DataTypeConversion: '<S75>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_fo = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer >> 7) & 1);

    /* DataTypeConversion: '<S75>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_mj = drum_bench_B.ExtractDesiredBits_fo;

    /* DataTypeConversion: '<S76>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_cg = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer >> 8) & 1);

    /* DataTypeConversion: '<S76>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_jz = drum_bench_B.ExtractDesiredBits_cg;

    /* DataTypeConversion: '<S77>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer >> 9) & 1);

    /* DataTypeConversion: '<S77>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_d = drum_bench_B.ExtractDesiredBits;

    /* DataTypeConversion: '<S66>/analog_' */
    drum_bench_B.analog = (drum_bench_B.ModifyScalingOnly_i != 0);

    /* DataTypeConversion: '<S66>/communication_fault_' */
    drum_bench_B.communication_fault = (drum_bench_B.ModifyScalingOnly_b != 0);

    /* DataTypeConversion: '<S66>/current_' */
    drum_bench_B.current = (drum_bench_B.ModifyScalingOnly_h != 0);

    /* DataTypeConversion: '<S66>/ethercat_' */
    drum_bench_B.ethercat_m = (drum_bench_B.ModifyScalingOnly_jz != 0);

    /* DataTypeConversion: '<S66>/external_panic_' */
    drum_bench_B.external_panic_e = (drum_bench_B.ModifyScalingOnly_jt != 0);

    /* DataTypeConversion: '<S66>/hard_fault_' */
    drum_bench_B.hard_fault = (drum_bench_B.ModifyScalingOnly_c != 0);

    /* DataTypeConversion: '<S66>/quadrature_' */
    drum_bench_B.quadrature = (drum_bench_B.ModifyScalingOnly_mj != 0);

    /* DataTypeConversion: '<S66>/soft_fault_' */
    drum_bench_B.soft_fault = (drum_bench_B.ModifyScalingOnly != 0);

    /* DataTypeConversion: '<S66>/ssi_' */
    drum_bench_B.ssi_i = (drum_bench_B.ModifyScalingOnly_j != 0);

    /* DataTypeConversion: '<S66>/temperature_' */
    drum_bench_B.temperature = (drum_bench_B.ModifyScalingOnly_d != 0);

    /* DataTypeConversion: '<S66>/voltage_' */
    drum_bench_B.voltage = (drum_bench_B.ModifyScalingOnly_m != 0);

    /* DataTypeConversion: '<S65>/motor_iq' */
    drum_bench_B.motor_iq_g = drum_bench_B.motor_iq_a;

    /* DataTypeConversion: '<S65>/motor_torque' */
    drum_bench_B.motor_torque_f = drum_bench_B.motor_torque_m;

    /* DataTypeConversion: '<S65>/motor_vq_avg' */
    drum_bench_B.motor_vq_avg_b = drum_bench_B.motor_vq_avg_f;

    /* DataTypeConversion: '<S65>/quadrature_1' */
    drum_bench_B.quadrature_1_j = drum_bench_B.quadrature_1_ha;

    /* DataTypeConversion: '<S65>/quadrature_1_dot' */
    drum_bench_B.quadrature_1_dot_c = drum_bench_B.quadrature_1_dot_l;

    /* DataTypeConversion: '<S65>/quadrature_2' */
    drum_bench_B.quadrature_2_l = drum_bench_B.quadrature_2_o;

    /* DataTypeConversion: '<S65>/quadrature_2_dot' */
    drum_bench_B.quadrature_2_dot_o = drum_bench_B.quadrature_2_dot_l;

    /* Unpack: <S65>/rpa_float */
    (void) memcpy(&drum_bench_B.rpa_float[0], &drum_bench_B.rpa_packet_k[0],
                  28);

    /* DataTypeConversion: '<S65>/ssi' */
    drum_bench_B.ssi_f = drum_bench_B.ssi_c;

    /* DataTypeConversion: '<S65>/ssi_dot' */
    drum_bench_B.ssi_dot_k = drum_bench_B.ssi_dot_f;

    /* DataTypeConversion: '<S6>/temperature_motor_housing' */
    drum_bench_B.temperature_motor_housing_j =
      drum_bench_B.temperature_motor_housing_c;

    /* DataTypeConversion: '<S6>/temperature_motor_winding' */
    drum_bench_B.temperature_motor_winding_h =
      drum_bench_B.temperature_motor_winding_j;

    /* S-Function Block: <S160>/marlin_ec */
    /* Output Port 1 written directly by slave */
    /* Output Port 2 written directly by slave */
    /* Output Port 3 written directly by slave */
    /* Output Port 4 written directly by slave */
    /* Output Port 5 written directly by slave */
    /* Output Port 6 written directly by slave */
    /* Output Port 7 written directly by slave */
    /* Output Port 8 written directly by slave */
    /* Output Port 9 written directly by slave */
    /* Output Port 10 written directly by slave */
    /* Output Port 11 written directly by slave */
    /* Output Port 12 written directly by slave */
    /* Output Port 13 written directly by slave */
    /* Output Port 14 written directly by slave */
    /* Output Port 15 written directly by slave */
    /* Output Port 16 written directly by slave */
    /* Output Port 17 written directly by slave */
    /* Output Port 18 written directly by slave */
    /* Output Port 19 written directly by slave */
    /* Output Port 20 written directly by slave */
    /* Output Port 21 written directly by slave */
    /* Output Port 22 written directly by slave */

    /* DataTypeConversion: '<S160>/motor_torque_' */
    drum_bench_B.motor_torque_l = drum_bench_B.marlin_ec_o1_k;

    /* DataTypeConversion: '<S160>/motor_iq_' */
    drum_bench_B.motor_iq_b = drum_bench_B.marlin_ec_o2_f;

    /* DataTypeConversion: '<S160>/motor_vq_avg_' */
    drum_bench_B.motor_vq_avg_e = drum_bench_B.marlin_ec_o3_h;

    /* DataTypeConversion: '<S160>/analog_1_' */
    drum_bench_B.analog_1_on = drum_bench_B.marlin_ec_o4_a;

    /* DataTypeConversion: '<S160>/analog_1_dot_' */
    drum_bench_B.analog_1_dot_k = drum_bench_B.marlin_ec_o5_l;

    /* DataTypeConversion: '<S160>/analog_2_' */
    drum_bench_B.analog_2_h = drum_bench_B.marlin_ec_o6_j;

    /* DataTypeConversion: '<S160>/analog_2_dot_' */
    drum_bench_B.analog_2_dot_h = drum_bench_B.marlin_ec_o7_c;

    /* DataTypeConversion: '<S160>/analog_diff_' */
    drum_bench_B.analog_diff_o = drum_bench_B.marlin_ec_o8_j;

    /* DataTypeConversion: '<S160>/analog_diff_dot_' */
    drum_bench_B.analog_diff_dot_m = drum_bench_B.marlin_ec_o9_j;

    /* DataTypeConversion: '<S160>/quadrature_1_' */
    drum_bench_B.quadrature_1_i = drum_bench_B.marlin_ec_o10_n;

    /* DataTypeConversion: '<S160>/quadrature_1_dot_' */
    drum_bench_B.quadrature_1_dot_e = drum_bench_B.marlin_ec_o11_d;

    /* DataTypeConversion: '<S160>/quadrature_2_' */
    drum_bench_B.quadrature_2_h = drum_bench_B.marlin_ec_o12_h;

    /* DataTypeConversion: '<S160>/quadrature_2_dot_' */
    drum_bench_B.quadrature_2_dot_b = drum_bench_B.marlin_ec_o13_b;

    /* DataTypeConversion: '<S160>/ssi_' */
    drum_bench_B.ssi_b = drum_bench_B.marlin_ec_o14_e;

    /* DataTypeConversion: '<S160>/ssi_dot_' */
    drum_bench_B.ssi_dot_b = drum_bench_B.marlin_ec_o15_h;

    /* DataTypeConversion: '<S160>/accelerometer_x_' */
    drum_bench_B.accelerometer_x_ej = drum_bench_B.marlin_ec_o16_c;

    /* DataTypeConversion: '<S160>/accelerometer_y_' */
    drum_bench_B.accelerometer_y_j = drum_bench_B.marlin_ec_o17_a;

    /* DataTypeConversion: '<S160>/accelerometer_z_' */
    drum_bench_B.accelerometer_z_i = drum_bench_B.marlin_ec_o18_g;

    /* DataTypeConversion: '<S160>/faults_' */
    drum_bench_B.faults_k = drum_bench_B.marlin_ec_o19_n;

    /* DataTypeConversion: '<S160>/bus_v_' */
    drum_bench_B.bus_v_l = drum_bench_B.marlin_ec_o20_k;

    /* BusCreator: '<S9>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */
    drum_bench_B.BusConversion_InsertedFor_Bus_a.motor_torque =
      drum_bench_B.motor_torque_l;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.motor_iq =
      drum_bench_B.motor_iq_b;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.motor_vq_avg =
      drum_bench_B.motor_vq_avg_e;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.analog_1 =
      drum_bench_B.analog_1_on;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.analog_1_dot =
      drum_bench_B.analog_1_dot_k;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.analog_2 =
      drum_bench_B.analog_2_h;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.analog_2_dot =
      drum_bench_B.analog_2_dot_h;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.analog_diff =
      drum_bench_B.analog_diff_o;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.analog_diff_dot =
      drum_bench_B.analog_diff_dot_m;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.quadrature_1 =
      drum_bench_B.quadrature_1_i;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.quadrature_1_dot =
      drum_bench_B.quadrature_1_dot_e;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.quadrature_2 =
      drum_bench_B.quadrature_2_h;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.quadrature_2_dot =
      drum_bench_B.quadrature_2_dot_b;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.ssi = drum_bench_B.ssi_b;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.ssi_dot =
      drum_bench_B.ssi_dot_b;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.accelerometer_x =
      drum_bench_B.accelerometer_x_ej;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.accelerometer_y =
      drum_bench_B.accelerometer_y_j;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.accelerometer_z =
      drum_bench_B.accelerometer_z_i;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.faults = drum_bench_B.faults_k;
    drum_bench_B.BusConversion_InsertedFor_Bus_a.bus_v = drum_bench_B.bus_v_l;
    for (i = 0; i < 28; i++) {
      /* DataTypeConversion: '<S160>/rpa_packet_' */
      drum_bench_B.rpa_packet_b[i] = drum_bench_B.marlin_ec_o22_a[i];
      drum_bench_B.BusConversion_InsertedFor_Bus_a.rpa_packet[i] =
        drum_bench_B.rpa_packet_b[i];
    }

    /* End of BusCreator: '<S9>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */

    /* Delay: '<S158>/Delay' */
    drum_bench_B.Delay_l[0] = drum_bench_DW.Delay_DSTATE_n[0];
    drum_bench_B.Delay_l[1] = drum_bench_DW.Delay_DSTATE_n[1];

    /* Gain: '<S158>/eA1' */
    drum_bench_B.eA1_c[0] = 0.0;
    drum_bench_B.eA1_c[0] +=
      drum_bench_P.config.marlins.M1.motor.thermal_model.eA1[0] *
      drum_bench_B.Delay_l[0];
    drum_bench_B.eA1_c[0] +=
      drum_bench_P.config.marlins.M1.motor.thermal_model.eA1[2] *
      drum_bench_B.Delay_l[1];
    drum_bench_B.eA1_c[1] = 0.0;
    drum_bench_B.eA1_c[1] +=
      drum_bench_P.config.marlins.M1.motor.thermal_model.eA1[1] *
      drum_bench_B.Delay_l[0];
    drum_bench_B.eA1_c[1] +=
      drum_bench_P.config.marlins.M1.motor.thermal_model.eA1[3] *
      drum_bench_B.Delay_l[1];

    /* Math: '<S158>/Math Function'
     *
     * About '<S158>/Math Function':
     *  Operator: magnitude^2
     */
    output = drum_bench_B.motor_iq_b;
    drum_bench_B.MathFunction_p = output * output;

    /* Gain: '<S158>/resistance' */
    drum_bench_B.resistance_e = drum_bench_P.config.marlins.M1.motor.resistance *
      drum_bench_B.MathFunction_p;

    /* Gain: '<S158>/eA2' */
    drum_bench_B.eA2_f[0] =
      drum_bench_P.config.marlins.M1.motor.thermal_model.eA2[0] *
      drum_bench_B.resistance_e;
    drum_bench_B.eA2_f[1] =
      drum_bench_P.config.marlins.M1.motor.thermal_model.eA2[1] *
      drum_bench_B.resistance_e;

    /* Sum: '<S158>/Sum' */
    drum_bench_B.Sum_d[0] = drum_bench_B.eA1_c[0] + drum_bench_B.eA2_f[0];
    drum_bench_B.Sum_d[1] = drum_bench_B.eA1_c[1] + drum_bench_B.eA2_f[1];

    /* BusCreator: '<S9>/Bus Creator' */
    drum_bench_B.BusCreator_it.marlin_basic_status =
      drum_bench_B.BusConversion_InsertedFor_Bus_a;
    drum_bench_B.BusCreator_it.temperature_motor_winding = drum_bench_B.Sum_d[0];
    drum_bench_B.BusCreator_it.temperature_motor_housing = drum_bench_B.Sum_d[1];

    /* BusSelector: '<S11>/Bus Selector1' */
    drum_bench_B.marlin_basic_status_m =
      drum_bench_B.BusCreator_it.marlin_basic_status;
    drum_bench_B.temperature_motor_winding_e =
      drum_bench_B.BusCreator_it.temperature_motor_winding;
    drum_bench_B.temperature_motor_housing_b =
      drum_bench_B.BusCreator_it.temperature_motor_housing;

    /* BusSelector: '<S225>/Bus Selector' */
    drum_bench_B.motor_torque_h =
      drum_bench_B.marlin_basic_status_m.motor_torque;
    drum_bench_B.motor_iq_d = drum_bench_B.marlin_basic_status_m.motor_iq;
    drum_bench_B.motor_vq_avg_i =
      drum_bench_B.marlin_basic_status_m.motor_vq_avg;
    drum_bench_B.analog_1_gs = drum_bench_B.marlin_basic_status_m.analog_1;
    drum_bench_B.analog_1_dot_j =
      drum_bench_B.marlin_basic_status_m.analog_1_dot;
    drum_bench_B.analog_2_f = drum_bench_B.marlin_basic_status_m.analog_2;
    drum_bench_B.analog_2_dot_c =
      drum_bench_B.marlin_basic_status_m.analog_2_dot;
    drum_bench_B.analog_diff_n = drum_bench_B.marlin_basic_status_m.analog_diff;
    drum_bench_B.analog_diff_dot_e =
      drum_bench_B.marlin_basic_status_m.analog_diff_dot;
    drum_bench_B.quadrature_1_c =
      drum_bench_B.marlin_basic_status_m.quadrature_1;
    drum_bench_B.quadrature_1_dot_o =
      drum_bench_B.marlin_basic_status_m.quadrature_1_dot;
    drum_bench_B.quadrature_2_hx =
      drum_bench_B.marlin_basic_status_m.quadrature_2;
    drum_bench_B.quadrature_2_dot_g =
      drum_bench_B.marlin_basic_status_m.quadrature_2_dot;
    drum_bench_B.ssi_o = drum_bench_B.marlin_basic_status_m.ssi;
    drum_bench_B.ssi_dot_o = drum_bench_B.marlin_basic_status_m.ssi_dot;
    drum_bench_B.accelerometer_x_l =
      drum_bench_B.marlin_basic_status_m.accelerometer_x;
    drum_bench_B.accelerometer_y_m =
      drum_bench_B.marlin_basic_status_m.accelerometer_y;
    drum_bench_B.accelerometer_z_b =
      drum_bench_B.marlin_basic_status_m.accelerometer_z;
    drum_bench_B.faults_k5 = drum_bench_B.marlin_basic_status_m.faults;
    drum_bench_B.bus_v_a = drum_bench_B.marlin_basic_status_m.bus_v;
    for (i = 0; i < 28; i++) {
      drum_bench_B.rpa_packet_i[i] =
        drum_bench_B.marlin_basic_status_m.rpa_packet[i];
    }

    /* End of BusSelector: '<S225>/Bus Selector' */

    /* DiscreteFir: '<S227>/Generated Filter Block' */
    idxN = drum_bench_DW.GeneratedFilterBlock_circBuf_o;
    i = 0;
    while (i < 1) {
      i = 1;
      acc = drum_bench_B.accelerometer_x_l * (-0.0019411429428760618);
      for (frameSize = idxN; frameSize < 23; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_ch[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      for (frameSize = 0; frameSize < idxN; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_ch[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      idxN--;
      if (idxN < 0) {
        idxN = 22;
      }

      drum_bench_DW.GeneratedFilterBlock_states_ch[idxN] =
        drum_bench_B.accelerometer_x_l;
      drum_bench_B.GeneratedFilterBlock_b0 = acc;
      i = 1;
    }

    drum_bench_DW.GeneratedFilterBlock_circBuf_o = idxN;

    /* End of DiscreteFir: '<S227>/Generated Filter Block' */

    /* DiscreteFir: '<S226>/Generated Filter Block' */
    idxN = drum_bench_DW.GeneratedFilterBlock_circBuf_k;
    i = 0;
    while (i < 1) {
      i = 1;
      acc = drum_bench_B.accelerometer_y_m * (-0.0019411429428760618);
      for (frameSize = idxN; frameSize < 23; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_k[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      for (frameSize = 0; frameSize < idxN; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_k[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      idxN--;
      if (idxN < 0) {
        idxN = 22;
      }

      drum_bench_DW.GeneratedFilterBlock_states_k[idxN] =
        drum_bench_B.accelerometer_y_m;
      drum_bench_B.GeneratedFilterBlock_g = acc;
      i = 1;
    }

    drum_bench_DW.GeneratedFilterBlock_circBuf_k = idxN;

    /* End of DiscreteFir: '<S226>/Generated Filter Block' */

    /* DiscreteFir: '<S228>/Generated Filter Block' */
    idxN_0 = drum_bench_DW.GeneratedFilterBlock_circBuf_bj;
    i = 0;
    while (i < 1) {
      i = 1;
      acc = drum_bench_B.accelerometer_z_b * (-0.0019411429428760618);
      for (frameSize = idxN_0; frameSize < 23; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_l[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      for (frameSize = 0; frameSize < idxN_0; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_l[frameSize] *
          drum_bench_ConstP.pooled4[i];
        i++;
      }

      idxN_0--;
      if (idxN_0 < 0) {
        idxN_0 = 22;
      }

      drum_bench_DW.GeneratedFilterBlock_states_l[idxN_0] =
        drum_bench_B.accelerometer_z_b;
      drum_bench_B.GeneratedFilterBlock_p = acc;
      i = 1;
    }

    drum_bench_DW.GeneratedFilterBlock_circBuf_bj = idxN_0;

    /* End of DiscreteFir: '<S228>/Generated Filter Block' */

    /* BusCreator: '<S11>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.motor_torque =
      drum_bench_B.motor_torque_h;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.motor_iq =
      drum_bench_B.motor_iq_d;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.motor_vq_avg =
      drum_bench_B.motor_vq_avg_i;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.analog_1 =
      drum_bench_B.analog_1_gs;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.analog_1_dot =
      drum_bench_B.analog_1_dot_j;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.analog_2 =
      drum_bench_B.analog_2_f;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.analog_2_dot =
      drum_bench_B.analog_2_dot_c;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.analog_diff =
      drum_bench_B.analog_diff_n;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.analog_diff_dot =
      drum_bench_B.analog_diff_dot_e;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.quadrature_1 =
      drum_bench_B.quadrature_1_c;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.quadrature_1_dot =
      drum_bench_B.quadrature_1_dot_o;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.quadrature_2 =
      drum_bench_B.quadrature_2_hx;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.quadrature_2_dot =
      drum_bench_B.quadrature_2_dot_g;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.ssi = drum_bench_B.ssi_o;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.ssi_dot =
      drum_bench_B.ssi_dot_o;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.accelerometer_x =
      drum_bench_B.GeneratedFilterBlock_b0;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.accelerometer_y =
      drum_bench_B.GeneratedFilterBlock_g;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.accelerometer_z =
      drum_bench_B.GeneratedFilterBlock_p;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.faults = drum_bench_B.faults_k5;
    drum_bench_B.BusConversion_InsertedFor_Bu_mc.bus_v = drum_bench_B.bus_v_a;
    for (i = 0; i < 28; i++) {
      drum_bench_B.BusConversion_InsertedFor_Bu_mc.rpa_packet[i] =
        drum_bench_B.rpa_packet_i[i];
    }

    /* End of BusCreator: '<S11>/BusConversion_InsertedFor_Bus Creator_at_inport_0' */

    /* BusCreator: '<S11>/Bus Creator' */
    drum_bench_B.BusCreator_m.marlin_basic_status =
      drum_bench_B.BusConversion_InsertedFor_Bu_mc;
    drum_bench_B.BusCreator_m.temperature_motor_winding =
      drum_bench_B.temperature_motor_winding_e;
    drum_bench_B.BusCreator_m.temperature_motor_housing =
      drum_bench_B.temperature_motor_housing_b;

    /* BusSelector: '<S7>/Bus Selector' */
    drum_bench_B.marlin_basic_status_l =
      drum_bench_B.BusCreator_m.marlin_basic_status;
    drum_bench_B.temperature_motor_winding_k =
      drum_bench_B.BusCreator_m.temperature_motor_winding;
    drum_bench_B.temperature_motor_housing_jp =
      drum_bench_B.BusCreator_m.temperature_motor_housing;

    /* BusSelector: '<S78>/Bus Selector' */
    drum_bench_B.motor_torque_p =
      drum_bench_B.marlin_basic_status_l.motor_torque;
    drum_bench_B.motor_iq_f = drum_bench_B.marlin_basic_status_l.motor_iq;
    drum_bench_B.motor_vq_avg_n =
      drum_bench_B.marlin_basic_status_l.motor_vq_avg;
    drum_bench_B.analog_1_m = drum_bench_B.marlin_basic_status_l.analog_1;
    drum_bench_B.analog_1_dot_f =
      drum_bench_B.marlin_basic_status_l.analog_1_dot;
    drum_bench_B.analog_2_b = drum_bench_B.marlin_basic_status_l.analog_2;
    drum_bench_B.analog_2_dot_e =
      drum_bench_B.marlin_basic_status_l.analog_2_dot;
    drum_bench_B.analog_diff_hz = drum_bench_B.marlin_basic_status_l.analog_diff;
    drum_bench_B.analog_diff_dot_h =
      drum_bench_B.marlin_basic_status_l.analog_diff_dot;
    drum_bench_B.quadrature_1_a =
      drum_bench_B.marlin_basic_status_l.quadrature_1;
    drum_bench_B.quadrature_1_dot_j =
      drum_bench_B.marlin_basic_status_l.quadrature_1_dot;
    drum_bench_B.quadrature_2_lz =
      drum_bench_B.marlin_basic_status_l.quadrature_2;
    drum_bench_B.quadrature_2_dot_o4 =
      drum_bench_B.marlin_basic_status_l.quadrature_2_dot;
    drum_bench_B.ssi_n = drum_bench_B.marlin_basic_status_l.ssi;
    drum_bench_B.ssi_dot_l = drum_bench_B.marlin_basic_status_l.ssi_dot;
    drum_bench_B.accelerometer_x_fe =
      drum_bench_B.marlin_basic_status_l.accelerometer_x;
    drum_bench_B.accelerometer_y_jg =
      drum_bench_B.marlin_basic_status_l.accelerometer_y;
    drum_bench_B.accelerometer_z_g =
      drum_bench_B.marlin_basic_status_l.accelerometer_z;
    drum_bench_B.faults_o = drum_bench_B.marlin_basic_status_l.faults;
    drum_bench_B.bus_v_p = drum_bench_B.marlin_basic_status_l.bus_v;
    for (i = 0; i < 28; i++) {
      drum_bench_B.rpa_packet_g[i] =
        drum_bench_B.marlin_basic_status_l.rpa_packet[i];
    }

    /* End of BusSelector: '<S78>/Bus Selector' */

    /* DataTypeConversion: '<S78>/accelerometer_x' */
    drum_bench_B.accelerometer_x_d = drum_bench_B.accelerometer_x_fe;

    /* DataTypeConversion: '<S78>/accelerometer_y' */
    drum_bench_B.accelerometer_y_e = drum_bench_B.accelerometer_y_jg;

    /* DataTypeConversion: '<S78>/accelerometer_z' */
    drum_bench_B.accelerometer_z_bk = drum_bench_B.accelerometer_z_g;

    /* DataTypeConversion: '<S78>/analog_1' */
    drum_bench_B.analog_1_b = drum_bench_B.analog_1_m;

    /* DataTypeConversion: '<S78>/analog_1_dot' */
    drum_bench_B.analog_1_dot_b = drum_bench_B.analog_1_dot_f;

    /* DataTypeConversion: '<S78>/analog_2' */
    drum_bench_B.analog_2_a = drum_bench_B.analog_2_b;

    /* DataTypeConversion: '<S78>/analog_2_dot' */
    drum_bench_B.analog_2_dot_k = drum_bench_B.analog_2_dot_e;

    /* DataTypeConversion: '<S78>/analog_diff' */
    drum_bench_B.analog_diff_ni = drum_bench_B.analog_diff_hz;

    /* DataTypeConversion: '<S78>/analog_diff_dot' */
    drum_bench_B.analog_diff_dot_i = drum_bench_B.analog_diff_dot_h;

    /* DataTypeConversion: '<S78>/bus_v' */
    drum_bench_B.bus_v_pu = drum_bench_B.bus_v_p;

    /* DataTypeConversion: '<S79>/raw_integer' */
    drum_bench_B.raw_integer_l = drum_bench_B.faults_o;

    /* DataTypeConversion: '<S80>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_p = (uint8_T)((uint8_T)
      drum_bench_B.raw_integer_l & 1);

    /* DataTypeConversion: '<S80>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_je = drum_bench_B.ExtractDesiredBits_p;

    /* DataTypeConversion: '<S81>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_f = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer_l >> 1) & 1);

    /* DataTypeConversion: '<S81>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_o = drum_bench_B.ExtractDesiredBits_f;

    /* DataTypeConversion: '<S82>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_i1 = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer_l >> 10) & 1);

    /* DataTypeConversion: '<S82>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_p = drum_bench_B.ExtractDesiredBits_i1;

    /* DataTypeConversion: '<S83>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_f2 = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer_l >> 2) & 1);

    /* DataTypeConversion: '<S83>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_oc = drum_bench_B.ExtractDesiredBits_f2;

    /* DataTypeConversion: '<S84>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_j = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer_l >> 3) & 1);

    /* DataTypeConversion: '<S84>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_l = drum_bench_B.ExtractDesiredBits_j;

    /* DataTypeConversion: '<S85>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_lr = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer_l >> 4) & 1);

    /* DataTypeConversion: '<S85>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_e = drum_bench_B.ExtractDesiredBits_lr;

    /* DataTypeConversion: '<S86>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_o = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer_l >> 5) & 1);

    /* DataTypeConversion: '<S86>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_f = drum_bench_B.ExtractDesiredBits_o;

    /* DataTypeConversion: '<S87>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_g = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer_l >> 6) & 1);

    /* DataTypeConversion: '<S87>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_cr = drum_bench_B.ExtractDesiredBits_g;

    /* DataTypeConversion: '<S88>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_kb = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer_l >> 7) & 1);

    /* DataTypeConversion: '<S88>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_n = drum_bench_B.ExtractDesiredBits_kb;

    /* DataTypeConversion: '<S89>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_i = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer_l >> 8) & 1);

    /* DataTypeConversion: '<S89>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_k = drum_bench_B.ExtractDesiredBits_i;

    /* DataTypeConversion: '<S90>/Extract Desired Bits' */
    drum_bench_B.ExtractDesiredBits_c = (uint8_T)((uint8_T)
      (drum_bench_B.raw_integer_l >> 9) & 1);

    /* DataTypeConversion: '<S90>/Modify Scaling Only' */
    drum_bench_B.ModifyScalingOnly_pi = drum_bench_B.ExtractDesiredBits_c;

    /* DataTypeConversion: '<S79>/analog_' */
    drum_bench_B.analog_i = (drum_bench_B.ModifyScalingOnly_cr != 0);

    /* DataTypeConversion: '<S79>/communication_fault_' */
    drum_bench_B.communication_fault_o = (drum_bench_B.ModifyScalingOnly_oc != 0);

    /* DataTypeConversion: '<S79>/current_' */
    drum_bench_B.current_h = (drum_bench_B.ModifyScalingOnly_e != 0);

    /* DataTypeConversion: '<S79>/ethercat_' */
    drum_bench_B.ethercat_e = (drum_bench_B.ModifyScalingOnly_k != 0);

    /* DataTypeConversion: '<S79>/external_panic_' */
    drum_bench_B.external_panic_o = (drum_bench_B.ModifyScalingOnly_l != 0);

    /* DataTypeConversion: '<S79>/hard_fault_' */
    drum_bench_B.hard_fault_e = (drum_bench_B.ModifyScalingOnly_o != 0);

    /* DataTypeConversion: '<S79>/quadrature_' */
    drum_bench_B.quadrature_l = (drum_bench_B.ModifyScalingOnly_n != 0);

    /* DataTypeConversion: '<S79>/soft_fault_' */
    drum_bench_B.soft_fault_a = (drum_bench_B.ModifyScalingOnly_je != 0);

    /* DataTypeConversion: '<S79>/ssi_' */
    drum_bench_B.ssi_m = (drum_bench_B.ModifyScalingOnly_p != 0);

    /* DataTypeConversion: '<S79>/temperature_' */
    drum_bench_B.temperature_b = (drum_bench_B.ModifyScalingOnly_pi != 0);

    /* DataTypeConversion: '<S79>/voltage_' */
    drum_bench_B.voltage_f = (drum_bench_B.ModifyScalingOnly_f != 0);

    /* DataTypeConversion: '<S78>/motor_iq' */
    drum_bench_B.motor_iq_gt = drum_bench_B.motor_iq_f;

    /* DataTypeConversion: '<S78>/motor_torque' */
    drum_bench_B.motor_torque_pz = drum_bench_B.motor_torque_p;

    /* DataTypeConversion: '<S78>/motor_vq_avg' */
    drum_bench_B.motor_vq_avg_is = drum_bench_B.motor_vq_avg_n;

    /* DataTypeConversion: '<S78>/quadrature_1' */
    drum_bench_B.quadrature_1_ib = drum_bench_B.quadrature_1_a;

    /* DataTypeConversion: '<S78>/quadrature_1_dot' */
    drum_bench_B.quadrature_1_dot_f = drum_bench_B.quadrature_1_dot_j;

    /* DataTypeConversion: '<S78>/quadrature_2' */
    drum_bench_B.quadrature_2_g = drum_bench_B.quadrature_2_lz;

    /* DataTypeConversion: '<S78>/quadrature_2_dot' */
    drum_bench_B.quadrature_2_dot_e = drum_bench_B.quadrature_2_dot_o4;

    /* Unpack: <S78>/rpa_float */
    (void) memcpy(&drum_bench_B.rpa_float_f[0], &drum_bench_B.rpa_packet_g[0],
                  28);

    /* DataTypeConversion: '<S78>/ssi' */
    drum_bench_B.ssi_h = drum_bench_B.ssi_n;

    /* DataTypeConversion: '<S78>/ssi_dot' */
    drum_bench_B.ssi_dot_o1 = drum_bench_B.ssi_dot_l;

    /* DataTypeConversion: '<S7>/temperature_motor_housing' */
    drum_bench_B.temperature_motor_housing_m =
      drum_bench_B.temperature_motor_housing_jp;

    /* DataTypeConversion: '<S7>/temperature_motor_winding' */
    drum_bench_B.temperature_motor_winding_i =
      drum_bench_B.temperature_motor_winding_k;

    /* BusSelector: '<S1>/Bus Selector' */
    drum_bench_B.analog_1_l =
      drum_bench_B.BusCreator_m.marlin_basic_status.analog_1;
    drum_bench_B.analog_2_f2 =
      drum_bench_B.BusCreator_m.marlin_basic_status.analog_2;

    /* BusSelector: '<S1>/Bus Selector1' */
    drum_bench_B.analog_1_g0 =
      drum_bench_B.BusCreator_i.marlin_basic_status.analog_1;
    drum_bench_B.analog_2_n =
      drum_bench_B.BusCreator_i.marlin_basic_status.analog_2;
    drum_bench_B.accelerometer_x_fm =
      drum_bench_B.BusCreator_i.marlin_basic_status.accelerometer_x;
    drum_bench_B.accelerometer_y_o =
      drum_bench_B.BusCreator_i.marlin_basic_status.accelerometer_y;
    drum_bench_B.accelerometer_z_c =
      drum_bench_B.BusCreator_i.marlin_basic_status.accelerometer_z;
  }

  if (drum_bench_M->Timing.TaskCounters.TID[3] == 0) {
    /* Level2 S-Function Block: '<S1>/C++ S-function1' (keyboard_press) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[0];
      sfcnOutputs(rts, 3);
    }

    /* Level2 S-Function Block: '<S1>/C++ S-function2' (networking) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[1];
      sfcnOutputs(rts, 3);
    }
  }

  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* Gain: '<S16>/Gain' */
    drum_bench_B.Gain = 1.0 * drum_bench_B.analog_1_g0;

    /* DiscreteFir: '<S30>/Generated Filter Block' */
    idxN = drum_bench_DW.GeneratedFilterBlock_circBuf_a;
    i = 0;
    while (i < 1) {
      i = 1;
      acc = drum_bench_B.Gain * 0.000668502224994789;
      for (frameSize = idxN; frameSize < 140; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_l3[frameSize] *
          drum_bench_ConstP.pooled8[i];
        i++;
      }

      for (frameSize = 0; frameSize < idxN; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_l3[frameSize] *
          drum_bench_ConstP.pooled8[i];
        i++;
      }

      idxN--;
      if (idxN < 0) {
        idxN = 139;
      }

      drum_bench_DW.GeneratedFilterBlock_states_l3[idxN] = drum_bench_B.Gain;
      drum_bench_B.GeneratedFilterBlock_o = acc;
      i = 1;
    }

    drum_bench_DW.GeneratedFilterBlock_circBuf_a = idxN;

    /* End of DiscreteFir: '<S30>/Generated Filter Block' */

    /* S-Function (sdspbiquad): '<S33>/Generated Filter Block' */
    output = 0.00054476472560799436 * drum_bench_B.GeneratedFilterBlock_o;
    output -= (-1.9868634599328121) *
      drum_bench_DW.GeneratedFilterBlock_FILT_STATE[0];
    output -= 0.98964031792422746 *
      drum_bench_DW.GeneratedFilterBlock_FILT_STATE[1];
    acc = 1.0019797777514476 * output;
    acc = (-2.0) * drum_bench_DW.GeneratedFilterBlock_FILT_STATE[0] + acc;
    acc = 1.0019739328100437 * drum_bench_DW.GeneratedFilterBlock_FILT_STATE[1]
      + acc;
    drum_bench_DW.GeneratedFilterBlock_FILT_STATE[1] =
      drum_bench_DW.GeneratedFilterBlock_FILT_STATE[0];
    drum_bench_DW.GeneratedFilterBlock_FILT_STATE[0] = output;
    output = 1.0 * acc;
    output -= (-1.9798516213532769) *
      drum_bench_DW.GeneratedFilterBlock_FILT_STATE[2];
    output -= 0.9853900749863409 *
      drum_bench_DW.GeneratedFilterBlock_FILT_STATE[3];
    acc = 1.0019804130748231 * output;
    acc = (-2.0) * drum_bench_DW.GeneratedFilterBlock_FILT_STATE[2] + acc;
    acc = 1.0019745687509363 * drum_bench_DW.GeneratedFilterBlock_FILT_STATE[3]
      + acc;
    drum_bench_DW.GeneratedFilterBlock_FILT_STATE[3] =
      drum_bench_DW.GeneratedFilterBlock_FILT_STATE[2];
    drum_bench_DW.GeneratedFilterBlock_FILT_STATE[2] = output;
    output = 1.0 * acc;
    output -= (-1.9712803138284749) *
      drum_bench_DW.GeneratedFilterBlock_FILT_STATE[4];
    output -= 0.97517787618064922 *
      drum_bench_DW.GeneratedFilterBlock_FILT_STATE[5];
    acc = 1.0019830177037872 * output;
    acc = (-1.9999999999999998) * drum_bench_DW.GeneratedFilterBlock_FILT_STATE
      [4] + acc;
    acc = 1.001971328438497 * drum_bench_DW.GeneratedFilterBlock_FILT_STATE[5] +
      acc;
    drum_bench_DW.GeneratedFilterBlock_FILT_STATE[5] =
      drum_bench_DW.GeneratedFilterBlock_FILT_STATE[4];
    drum_bench_DW.GeneratedFilterBlock_FILT_STATE[4] = output;
    drum_bench_B.GeneratedFilterBlock_j = 1779.5174067186181 * acc;

    /* Sum: '<S16>/Subtract' incorporates:
     *  Constant: '<S16>/Constant1'
     */
    drum_bench_B.Subtract = drum_bench_B.GeneratedFilterBlock_j - 1895.0;

    /* Abs: '<S16>/Abs' */
    drum_bench_B.Abs = fabs(drum_bench_B.Subtract);

    /* MATLAB Function: '<S16>/smooth' */
    drum_bench_smooth(drum_bench_B.Abs, &drum_bench_B.sf_smooth,
                      &drum_bench_DW.sf_smooth);

    /* Gain: '<S16>/Gain1' */
    drum_bench_B.Gain1 = 500.0 * drum_bench_B.sf_smooth.y;

    /* Math: '<S16>/Math Function' incorporates:
     *  Constant: '<S16>/Constant'
     */
    acc = drum_bench_B.Gain1;
    output = 20.0;
    if ((acc < 0.0) && (output > floor(output))) {
      drum_bench_B.MathFunction_i = -rt_powd_snf(-acc, output);
    } else {
      drum_bench_B.MathFunction_i = rt_powd_snf(acc, output);
    }

    /* End of Math: '<S16>/Math Function' */

    /* Math: '<S16>/Math Function1'
     *
     * About '<S16>/Math Function1':
     *  Operator: log
     */
    drum_bench_B.MathFunction1 = log(drum_bench_B.MathFunction_i);

    /* Buffer: '<S16>/Buffer3' */
    idxN = 1;
    frameSize = 256 - drum_bench_DW.Buffer3_inBufPtrIdx;
    idxN_0 = drum_bench_DW.Buffer3_inBufPtrIdx;
    if (frameSize <= 1) {
      i = 0;
      while (i < frameSize) {
        drum_bench_DW.Buffer3_CircBuf[idxN_0] = drum_bench_B.MathFunction1;
        i = 1;
      }

      idxN_0 = 0;
      idxN = 1 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_DW.Buffer3_CircBuf[idxN_0 + i] = drum_bench_B.MathFunction1;
    }

    drum_bench_DW.Buffer3_inBufPtrIdx++;
    if (drum_bench_DW.Buffer3_inBufPtrIdx >= 256) {
      drum_bench_DW.Buffer3_inBufPtrIdx -= 256;
    }

    drum_bench_DW.Buffer3_bufferLength++;
    if (drum_bench_DW.Buffer3_bufferLength > 256) {
      drum_bench_DW.Buffer3_outBufPtrIdx = (drum_bench_DW.Buffer3_outBufPtrIdx +
        drum_bench_DW.Buffer3_bufferLength) - 256;
      if (drum_bench_DW.Buffer3_outBufPtrIdx > 256) {
        drum_bench_DW.Buffer3_outBufPtrIdx -= 256;
      }

      drum_bench_DW.Buffer3_bufferLength = 256;
    }
  }

  /* Buffer: '<S16>/Buffer3' */
  if (drum_bench_M->Timing.TaskCounters.TID[5] == 0) {
    drum_bench_DW.Buffer3_bufferLength -= 128;
    if (drum_bench_DW.Buffer3_bufferLength < 0) {
      drum_bench_DW.Buffer3_outBufPtrIdx += drum_bench_DW.Buffer3_bufferLength;
      if (drum_bench_DW.Buffer3_outBufPtrIdx < 0) {
        drum_bench_DW.Buffer3_outBufPtrIdx += 256;
      }

      drum_bench_DW.Buffer3_bufferLength = 0;
    }

    idxN_0 = 0;
    currentOffset = drum_bench_DW.Buffer3_outBufPtrIdx;
    if (currentOffset < 0) {
      currentOffset += 256;
    }

    frameSize = 256 - currentOffset;
    idxN = 128;
    if (frameSize <= 128) {
      for (i = 0; i < frameSize; i++) {
        drum_bench_B.Buffer3[i] = drum_bench_DW.Buffer3_CircBuf[currentOffset +
          i];
      }

      idxN_0 = frameSize;
      currentOffset = 0;
      idxN = 128 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_B.Buffer3[idxN_0 + i] =
        drum_bench_DW.Buffer3_CircBuf[currentOffset + i];
    }

    currentOffset += idxN;
    drum_bench_DW.Buffer3_outBufPtrIdx = currentOffset;

    /* MATLAB Function: '<S16>/mean2' */
    drum_bench_mean(drum_bench_B.Buffer3, &drum_bench_B.sf_mean2);
  }

  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* MATLAB Function: '<S16>/zeroCrossing' */
    drum_bench_zeroCrossing(drum_bench_B.Subtract, &drum_bench_B.sf_zeroCrossing,
      &drum_bench_DW.sf_zeroCrossing);

    /* Buffer: '<S16>/Buffer2' */
    idxN = 1;
    frameSize = 256 - drum_bench_DW.Buffer2_inBufPtrIdx;
    idxN_0 = drum_bench_DW.Buffer2_inBufPtrIdx;
    if (frameSize <= 1) {
      i = 0;
      while (i < frameSize) {
        drum_bench_DW.Buffer2_CircBuf[idxN_0] = drum_bench_B.sf_zeroCrossing.y;
        i = 1;
      }

      idxN_0 = 0;
      idxN = 1 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_DW.Buffer2_CircBuf[idxN_0 + i] = drum_bench_B.sf_zeroCrossing.y;
    }

    drum_bench_DW.Buffer2_inBufPtrIdx++;
    if (drum_bench_DW.Buffer2_inBufPtrIdx >= 256) {
      drum_bench_DW.Buffer2_inBufPtrIdx -= 256;
    }

    drum_bench_DW.Buffer2_bufferLength++;
    if (drum_bench_DW.Buffer2_bufferLength > 256) {
      drum_bench_DW.Buffer2_outBufPtrIdx = (drum_bench_DW.Buffer2_outBufPtrIdx +
        drum_bench_DW.Buffer2_bufferLength) - 256;
      if (drum_bench_DW.Buffer2_outBufPtrIdx > 256) {
        drum_bench_DW.Buffer2_outBufPtrIdx -= 256;
      }

      drum_bench_DW.Buffer2_bufferLength = 256;
    }
  }

  /* Buffer: '<S16>/Buffer2' */
  if (drum_bench_M->Timing.TaskCounters.TID[5] == 0) {
    drum_bench_DW.Buffer2_bufferLength -= 128;
    if (drum_bench_DW.Buffer2_bufferLength < 0) {
      drum_bench_DW.Buffer2_outBufPtrIdx += drum_bench_DW.Buffer2_bufferLength;
      if (drum_bench_DW.Buffer2_outBufPtrIdx < 0) {
        drum_bench_DW.Buffer2_outBufPtrIdx += 256;
      }

      drum_bench_DW.Buffer2_bufferLength = 0;
    }

    idxN_0 = 0;
    currentOffset = drum_bench_DW.Buffer2_outBufPtrIdx;
    if (currentOffset < 0) {
      currentOffset += 256;
    }

    frameSize = 256 - currentOffset;
    idxN = 128;
    if (frameSize <= 128) {
      for (i = 0; i < frameSize; i++) {
        drum_bench_B.Buffer2[i] = drum_bench_DW.Buffer2_CircBuf[currentOffset +
          i];
      }

      idxN_0 = frameSize;
      currentOffset = 0;
      idxN = 128 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_B.Buffer2[idxN_0 + i] =
        drum_bench_DW.Buffer2_CircBuf[currentOffset + i];
    }

    currentOffset += idxN;
    drum_bench_DW.Buffer2_outBufPtrIdx = currentOffset;

    /* MATLAB Function: '<S16>/mean' */
    drum_bench_mean(drum_bench_B.Buffer2, &drum_bench_B.sf_mean);
  }

  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* MATLAB Function: '<S16>/slopeChange' */
    drum_bench_slopeChange(drum_bench_B.Subtract, &drum_bench_B.sf_slopeChange,
      &drum_bench_DW.sf_slopeChange);

    /* Buffer: '<S16>/Buffer1' */
    idxN = 1;
    frameSize = 256 - drum_bench_DW.Buffer1_inBufPtrIdx;
    idxN_0 = drum_bench_DW.Buffer1_inBufPtrIdx;
    if (frameSize <= 1) {
      i = 0;
      while (i < frameSize) {
        drum_bench_DW.Buffer1_CircBuf[idxN_0] = drum_bench_B.sf_slopeChange.y;
        i = 1;
      }

      idxN_0 = 0;
      idxN = 1 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_DW.Buffer1_CircBuf[idxN_0 + i] = drum_bench_B.sf_slopeChange.y;
    }

    drum_bench_DW.Buffer1_inBufPtrIdx++;
    if (drum_bench_DW.Buffer1_inBufPtrIdx >= 256) {
      drum_bench_DW.Buffer1_inBufPtrIdx -= 256;
    }

    drum_bench_DW.Buffer1_bufferLength++;
    if (drum_bench_DW.Buffer1_bufferLength > 256) {
      drum_bench_DW.Buffer1_outBufPtrIdx = (drum_bench_DW.Buffer1_outBufPtrIdx +
        drum_bench_DW.Buffer1_bufferLength) - 256;
      if (drum_bench_DW.Buffer1_outBufPtrIdx > 256) {
        drum_bench_DW.Buffer1_outBufPtrIdx -= 256;
      }

      drum_bench_DW.Buffer1_bufferLength = 256;
    }
  }

  /* Buffer: '<S16>/Buffer1' */
  if (drum_bench_M->Timing.TaskCounters.TID[5] == 0) {
    drum_bench_DW.Buffer1_bufferLength -= 128;
    if (drum_bench_DW.Buffer1_bufferLength < 0) {
      drum_bench_DW.Buffer1_outBufPtrIdx += drum_bench_DW.Buffer1_bufferLength;
      if (drum_bench_DW.Buffer1_outBufPtrIdx < 0) {
        drum_bench_DW.Buffer1_outBufPtrIdx += 256;
      }

      drum_bench_DW.Buffer1_bufferLength = 0;
    }

    idxN_0 = 0;
    currentOffset = drum_bench_DW.Buffer1_outBufPtrIdx;
    if (currentOffset < 0) {
      currentOffset += 256;
    }

    frameSize = 256 - currentOffset;
    idxN = 128;
    if (frameSize <= 128) {
      for (i = 0; i < frameSize; i++) {
        drum_bench_B.Buffer1[i] = drum_bench_DW.Buffer1_CircBuf[currentOffset +
          i];
      }

      idxN_0 = frameSize;
      currentOffset = 0;
      idxN = 128 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_B.Buffer1[idxN_0 + i] =
        drum_bench_DW.Buffer1_CircBuf[currentOffset + i];
    }

    currentOffset += idxN;
    drum_bench_DW.Buffer1_outBufPtrIdx = currentOffset;

    /* MATLAB Function: '<S16>/mean1' */
    drum_bench_mean(drum_bench_B.Buffer1, &drum_bench_B.sf_mean1);
  }

  /* Buffer: '<S16>/Buffer' */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    idxN = 1;
    frameSize = 256 - drum_bench_DW.Buffer_inBufPtrIdx;
    idxN_0 = drum_bench_DW.Buffer_inBufPtrIdx;
    if (frameSize <= 1) {
      i = 0;
      while (i < frameSize) {
        drum_bench_DW.Buffer_CircBuf[idxN_0] = drum_bench_B.Subtract;
        i = 1;
      }

      idxN_0 = 0;
      idxN = 1 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_DW.Buffer_CircBuf[idxN_0 + i] = drum_bench_B.Subtract;
    }

    drum_bench_DW.Buffer_inBufPtrIdx++;
    if (drum_bench_DW.Buffer_inBufPtrIdx >= 256) {
      drum_bench_DW.Buffer_inBufPtrIdx -= 256;
    }

    drum_bench_DW.Buffer_bufferLength++;
    if (drum_bench_DW.Buffer_bufferLength > 256) {
      drum_bench_DW.Buffer_outBufPtrIdx = (drum_bench_DW.Buffer_outBufPtrIdx +
        drum_bench_DW.Buffer_bufferLength) - 256;
      if (drum_bench_DW.Buffer_outBufPtrIdx > 256) {
        drum_bench_DW.Buffer_outBufPtrIdx -= 256;
      }

      drum_bench_DW.Buffer_bufferLength = 256;
    }
  }

  if (drum_bench_M->Timing.TaskCounters.TID[5] == 0) {
    drum_bench_DW.Buffer_bufferLength -= 128;
    if (drum_bench_DW.Buffer_bufferLength < 0) {
      drum_bench_DW.Buffer_outBufPtrIdx += drum_bench_DW.Buffer_bufferLength;
      if (drum_bench_DW.Buffer_outBufPtrIdx < 0) {
        drum_bench_DW.Buffer_outBufPtrIdx += 256;
      }

      drum_bench_DW.Buffer_bufferLength = 0;
    }

    idxN_0 = 0;
    currentOffset = drum_bench_DW.Buffer_outBufPtrIdx;
    if (currentOffset < 0) {
      currentOffset += 256;
    }

    frameSize = 256 - currentOffset;
    idxN = 128;
    if (frameSize <= 128) {
      for (i = 0; i < frameSize; i++) {
        drum_bench_B.Buffer[i] = drum_bench_DW.Buffer_CircBuf[currentOffset + i];
      }

      idxN_0 = frameSize;
      currentOffset = 0;
      idxN = 128 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_B.Buffer[idxN_0 + i] =
        drum_bench_DW.Buffer_CircBuf[currentOffset + i];
    }

    currentOffset += idxN;
    drum_bench_DW.Buffer_outBufPtrIdx = currentOffset;

    /* S-Function (sdspwindow2): '<S16>/Window Function' */
    for (i = 0; i < 128; i++) {
      drum_bench_B.WindowFunction[i] = drum_bench_B.Buffer[i] *
        drum_bench_ConstP.pooled15[i];
    }

    /* End of S-Function (sdspwindow2): '<S16>/Window Function' */

    /* S-Function (sdspfft2): '<S32>/FFT' */
    MWDSPCG_FFT_Interleave_R2BR_D(&drum_bench_B.WindowFunction[0U],
      &drum_bench_B.FFT[0U], 1, 128);
    MWDSPCG_R2DIT_TBLS_Z(&drum_bench_B.FFT[0U], 1, 128, 64, 0,
                         drum_bench_ConstP.pooled16, 2, FALSE);
    MWDSPCG_FFT_DblLen_Z(&drum_bench_B.FFT[0U], 1, 128,
                         drum_bench_ConstP.pooled16, 1);

    /* Math: '<S32>/Magnitude Squared'
     *
     * About '<S32>/Magnitude Squared':
     *  Operator: magnitude^2
     */
    for (i = 0; i < 128; i++) {
      u = drum_bench_B.FFT[i];
      acc = u.re * u.re + u.im * u.im;
      drum_bench_B.MagnitudeSquared[i] = acc;
    }

    /* End of Math: '<S32>/Magnitude Squared' */

    /* MATLAB Function: '<S16>/MATLAB Function' */
    /* MATLAB Function 'Behavior Control/EMG Filter1/MATLAB Function': '<S31>:1' */
    /* '<S31>:1:8' */
    for (i = 0; i < 40; i++) {
      x[i] = drum_bench_B.MagnitudeSquared[i] - drum_bench_DW.buf[i];
    }

    output = x[0];
    for (i = 0; i < 39; i++) {
      output += x[i + 1];
    }

    /* '<S31>:1:9' */
    memcpy(&drum_bench_DW.buf[0], &drum_bench_B.MagnitudeSquared[0], sizeof
           (real_T) << 7U);

    /* '<S31>:1:10' */
    drum_bench_B.y = output;

    /* End of MATLAB Function: '<S16>/MATLAB Function' */
  }

  /* End of Buffer: '<S16>/Buffer' */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* Gain: '<S15>/Gain' */
    drum_bench_B.Gain_n = 1.0 * drum_bench_B.analog_1_l;

    /* DiscreteFir: '<S17>/Generated Filter Block' */
    idxN = drum_bench_DW.GeneratedFilterBlock_circBuf_o2;
    i = 0;
    while (i < 1) {
      i = 1;
      acc = drum_bench_B.Gain_n * 0.000668502224994789;
      for (frameSize = idxN; frameSize < 140; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_kq[frameSize] *
          drum_bench_ConstP.pooled8[i];
        i++;
      }

      for (frameSize = 0; frameSize < idxN; frameSize++) {
        acc += drum_bench_DW.GeneratedFilterBlock_states_kq[frameSize] *
          drum_bench_ConstP.pooled8[i];
        i++;
      }

      idxN--;
      if (idxN < 0) {
        idxN = 139;
      }

      drum_bench_DW.GeneratedFilterBlock_states_kq[idxN] = drum_bench_B.Gain_n;
      drum_bench_B.GeneratedFilterBlock_gi = acc;
      i = 1;
    }

    drum_bench_DW.GeneratedFilterBlock_circBuf_o2 = idxN;

    /* End of DiscreteFir: '<S17>/Generated Filter Block' */

    /* S-Function (sdspbiquad): '<S20>/Generated Filter Block' */
    output = 0.00054476472560799436 * drum_bench_B.GeneratedFilterBlock_gi;
    output -= (-1.9868634599328121) *
      drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[0];
    output -= 0.98964031792422746 *
      drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[1];
    acc = 1.0019797777514476 * output;
    acc = (-2.0) * drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[0] + acc;
    acc = 1.0019739328100437 * drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[1]
      + acc;
    drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[1] =
      drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[0];
    drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[0] = output;
    output = 1.0 * acc;
    output -= (-1.9798516213532769) *
      drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[2];
    output -= 0.9853900749863409 *
      drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[3];
    acc = 1.0019804130748231 * output;
    acc = (-2.0) * drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[2] + acc;
    acc = 1.0019745687509363 * drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[3]
      + acc;
    drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[3] =
      drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[2];
    drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[2] = output;
    output = 1.0 * acc;
    output -= (-1.9712803138284749) *
      drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[4];
    output -= 0.97517787618064922 *
      drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[5];
    acc = 1.0019830177037872 * output;
    acc = (-1.9999999999999998) * drum_bench_DW.GeneratedFilterBlock_FILT_STA_d
      [4] + acc;
    acc = 1.001971328438497 * drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[5] +
      acc;
    drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[5] =
      drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[4];
    drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[4] = output;
    drum_bench_B.GeneratedFilterBlock_n = 1779.5174067186181 * acc;

    /* Sum: '<S15>/Subtract' incorporates:
     *  Constant: '<S15>/Constant1'
     */
    drum_bench_B.Subtract_d = drum_bench_B.GeneratedFilterBlock_n - 1902.0;

    /* Abs: '<S15>/Abs' */
    drum_bench_B.Abs_n = fabs(drum_bench_B.Subtract_d);

    /* MATLAB Function: '<S15>/smooth' */
    drum_bench_smooth(drum_bench_B.Abs_n, &drum_bench_B.sf_smooth_a,
                      &drum_bench_DW.sf_smooth_a);

    /* Gain: '<S15>/Gain1' */
    drum_bench_B.Gain1_k = 500.0 * drum_bench_B.sf_smooth_a.y;

    /* Math: '<S15>/Math Function' incorporates:
     *  Constant: '<S15>/Constant'
     */
    acc = drum_bench_B.Gain1_k;
    output = 20.0;
    if ((acc < 0.0) && (output > floor(output))) {
      drum_bench_B.MathFunction_o = -rt_powd_snf(-acc, output);
    } else {
      drum_bench_B.MathFunction_o = rt_powd_snf(acc, output);
    }

    /* End of Math: '<S15>/Math Function' */

    /* Math: '<S15>/Math Function1'
     *
     * About '<S15>/Math Function1':
     *  Operator: log
     */
    drum_bench_B.MathFunction1_n = log(drum_bench_B.MathFunction_o);

    /* Buffer: '<S15>/Buffer3' */
    idxN = 1;
    frameSize = 256 - drum_bench_DW.Buffer3_inBufPtrIdx_m;
    idxN_0 = drum_bench_DW.Buffer3_inBufPtrIdx_m;
    if (frameSize <= 1) {
      i = 0;
      while (i < frameSize) {
        drum_bench_DW.Buffer3_CircBuf_k[idxN_0] = drum_bench_B.MathFunction1_n;
        i = 1;
      }

      idxN_0 = 0;
      idxN = 1 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_DW.Buffer3_CircBuf_k[idxN_0 + i] = drum_bench_B.MathFunction1_n;
    }

    drum_bench_DW.Buffer3_inBufPtrIdx_m++;
    if (drum_bench_DW.Buffer3_inBufPtrIdx_m >= 256) {
      drum_bench_DW.Buffer3_inBufPtrIdx_m -= 256;
    }

    drum_bench_DW.Buffer3_bufferLength_b++;
    if (drum_bench_DW.Buffer3_bufferLength_b > 256) {
      drum_bench_DW.Buffer3_outBufPtrIdx_a =
        (drum_bench_DW.Buffer3_outBufPtrIdx_a +
         drum_bench_DW.Buffer3_bufferLength_b) - 256;
      if (drum_bench_DW.Buffer3_outBufPtrIdx_a > 256) {
        drum_bench_DW.Buffer3_outBufPtrIdx_a -= 256;
      }

      drum_bench_DW.Buffer3_bufferLength_b = 256;
    }
  }

  /* Buffer: '<S15>/Buffer3' */
  if (drum_bench_M->Timing.TaskCounters.TID[5] == 0) {
    drum_bench_DW.Buffer3_bufferLength_b -= 128;
    if (drum_bench_DW.Buffer3_bufferLength_b < 0) {
      drum_bench_DW.Buffer3_outBufPtrIdx_a +=
        drum_bench_DW.Buffer3_bufferLength_b;
      if (drum_bench_DW.Buffer3_outBufPtrIdx_a < 0) {
        drum_bench_DW.Buffer3_outBufPtrIdx_a += 256;
      }

      drum_bench_DW.Buffer3_bufferLength_b = 0;
    }

    idxN_0 = 0;
    currentOffset = drum_bench_DW.Buffer3_outBufPtrIdx_a;
    if (currentOffset < 0) {
      currentOffset += 256;
    }

    frameSize = 256 - currentOffset;
    idxN = 128;
    if (frameSize <= 128) {
      for (i = 0; i < frameSize; i++) {
        drum_bench_B.Buffer3_k[i] =
          drum_bench_DW.Buffer3_CircBuf_k[currentOffset + i];
      }

      idxN_0 = frameSize;
      currentOffset = 0;
      idxN = 128 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_B.Buffer3_k[idxN_0 + i] =
        drum_bench_DW.Buffer3_CircBuf_k[currentOffset + i];
    }

    currentOffset += idxN;
    drum_bench_DW.Buffer3_outBufPtrIdx_a = currentOffset;

    /* MATLAB Function: '<S15>/mean2' */
    drum_bench_mean(drum_bench_B.Buffer3_k, &drum_bench_B.sf_mean2_b);
  }

  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* MATLAB Function: '<S15>/zeroCrossing' */
    drum_bench_zeroCrossing(drum_bench_B.Subtract_d,
      &drum_bench_B.sf_zeroCrossing_f, &drum_bench_DW.sf_zeroCrossing_f);

    /* MATLAB Function: '<S15>/smooth1' */
    drum_bench_smooth1(drum_bench_B.sf_zeroCrossing_f.y,
                       &drum_bench_B.sf_smooth1, &drum_bench_DW.sf_smooth1);

    /* Buffer: '<S15>/Buffer2' */
    idxN = 1;
    frameSize = 256 - drum_bench_DW.Buffer2_inBufPtrIdx_o;
    idxN_0 = drum_bench_DW.Buffer2_inBufPtrIdx_o;
    if (frameSize <= 1) {
      i = 0;
      while (i < frameSize) {
        drum_bench_DW.Buffer2_CircBuf_o[idxN_0] = drum_bench_B.sf_smooth1.y;
        i = 1;
      }

      idxN_0 = 0;
      idxN = 1 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_DW.Buffer2_CircBuf_o[idxN_0 + i] = drum_bench_B.sf_smooth1.y;
    }

    drum_bench_DW.Buffer2_inBufPtrIdx_o++;
    if (drum_bench_DW.Buffer2_inBufPtrIdx_o >= 256) {
      drum_bench_DW.Buffer2_inBufPtrIdx_o -= 256;
    }

    drum_bench_DW.Buffer2_bufferLength_l++;
    if (drum_bench_DW.Buffer2_bufferLength_l > 256) {
      drum_bench_DW.Buffer2_outBufPtrIdx_p =
        (drum_bench_DW.Buffer2_outBufPtrIdx_p +
         drum_bench_DW.Buffer2_bufferLength_l) - 256;
      if (drum_bench_DW.Buffer2_outBufPtrIdx_p > 256) {
        drum_bench_DW.Buffer2_outBufPtrIdx_p -= 256;
      }

      drum_bench_DW.Buffer2_bufferLength_l = 256;
    }
  }

  /* Buffer: '<S15>/Buffer2' */
  if (drum_bench_M->Timing.TaskCounters.TID[5] == 0) {
    drum_bench_DW.Buffer2_bufferLength_l -= 128;
    if (drum_bench_DW.Buffer2_bufferLength_l < 0) {
      drum_bench_DW.Buffer2_outBufPtrIdx_p +=
        drum_bench_DW.Buffer2_bufferLength_l;
      if (drum_bench_DW.Buffer2_outBufPtrIdx_p < 0) {
        drum_bench_DW.Buffer2_outBufPtrIdx_p += 256;
      }

      drum_bench_DW.Buffer2_bufferLength_l = 0;
    }

    idxN_0 = 0;
    currentOffset = drum_bench_DW.Buffer2_outBufPtrIdx_p;
    if (currentOffset < 0) {
      currentOffset += 256;
    }

    frameSize = 256 - currentOffset;
    idxN = 128;
    if (frameSize <= 128) {
      for (i = 0; i < frameSize; i++) {
        drum_bench_B.Buffer2_o[i] =
          drum_bench_DW.Buffer2_CircBuf_o[currentOffset + i];
      }

      idxN_0 = frameSize;
      currentOffset = 0;
      idxN = 128 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_B.Buffer2_o[idxN_0 + i] =
        drum_bench_DW.Buffer2_CircBuf_o[currentOffset + i];
    }

    currentOffset += idxN;
    drum_bench_DW.Buffer2_outBufPtrIdx_p = currentOffset;

    /* MATLAB Function: '<S15>/mean' */
    drum_bench_mean(drum_bench_B.Buffer2_o, &drum_bench_B.sf_mean_p);
  }

  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* MATLAB Function: '<S15>/slopeChange' */
    drum_bench_slopeChange(drum_bench_B.Subtract_d,
      &drum_bench_B.sf_slopeChange_n, &drum_bench_DW.sf_slopeChange_n);

    /* MATLAB Function: '<S15>/smooth2' */
    drum_bench_smooth1(drum_bench_B.sf_slopeChange_n.y, &drum_bench_B.sf_smooth2,
                       &drum_bench_DW.sf_smooth2);

    /* Buffer: '<S15>/Buffer1' */
    idxN = 1;
    frameSize = 256 - drum_bench_DW.Buffer1_inBufPtrIdx_b;
    idxN_0 = drum_bench_DW.Buffer1_inBufPtrIdx_b;
    if (frameSize <= 1) {
      i = 0;
      while (i < frameSize) {
        drum_bench_DW.Buffer1_CircBuf_a[idxN_0] = drum_bench_B.sf_smooth2.y;
        i = 1;
      }

      idxN_0 = 0;
      idxN = 1 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_DW.Buffer1_CircBuf_a[idxN_0 + i] = drum_bench_B.sf_smooth2.y;
    }

    drum_bench_DW.Buffer1_inBufPtrIdx_b++;
    if (drum_bench_DW.Buffer1_inBufPtrIdx_b >= 256) {
      drum_bench_DW.Buffer1_inBufPtrIdx_b -= 256;
    }

    drum_bench_DW.Buffer1_bufferLength_e++;
    if (drum_bench_DW.Buffer1_bufferLength_e > 256) {
      drum_bench_DW.Buffer1_outBufPtrIdx_l =
        (drum_bench_DW.Buffer1_outBufPtrIdx_l +
         drum_bench_DW.Buffer1_bufferLength_e) - 256;
      if (drum_bench_DW.Buffer1_outBufPtrIdx_l > 256) {
        drum_bench_DW.Buffer1_outBufPtrIdx_l -= 256;
      }

      drum_bench_DW.Buffer1_bufferLength_e = 256;
    }
  }

  /* Buffer: '<S15>/Buffer1' */
  if (drum_bench_M->Timing.TaskCounters.TID[5] == 0) {
    drum_bench_DW.Buffer1_bufferLength_e -= 128;
    if (drum_bench_DW.Buffer1_bufferLength_e < 0) {
      drum_bench_DW.Buffer1_outBufPtrIdx_l +=
        drum_bench_DW.Buffer1_bufferLength_e;
      if (drum_bench_DW.Buffer1_outBufPtrIdx_l < 0) {
        drum_bench_DW.Buffer1_outBufPtrIdx_l += 256;
      }

      drum_bench_DW.Buffer1_bufferLength_e = 0;
    }

    idxN_0 = 0;
    currentOffset = drum_bench_DW.Buffer1_outBufPtrIdx_l;
    if (currentOffset < 0) {
      currentOffset += 256;
    }

    frameSize = 256 - currentOffset;
    idxN = 128;
    if (frameSize <= 128) {
      for (i = 0; i < frameSize; i++) {
        drum_bench_B.Buffer1_g[i] =
          drum_bench_DW.Buffer1_CircBuf_a[currentOffset + i];
      }

      idxN_0 = frameSize;
      currentOffset = 0;
      idxN = 128 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_B.Buffer1_g[idxN_0 + i] =
        drum_bench_DW.Buffer1_CircBuf_a[currentOffset + i];
    }

    currentOffset += idxN;
    drum_bench_DW.Buffer1_outBufPtrIdx_l = currentOffset;

    /* MATLAB Function: '<S15>/mean1' */
    drum_bench_mean(drum_bench_B.Buffer1_g, &drum_bench_B.sf_mean1_m);
  }

  /* Buffer: '<S15>/Buffer' */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    idxN = 1;
    frameSize = 256 - drum_bench_DW.Buffer_inBufPtrIdx_d;
    idxN_0 = drum_bench_DW.Buffer_inBufPtrIdx_d;
    if (frameSize <= 1) {
      i = 0;
      while (i < frameSize) {
        drum_bench_DW.Buffer_CircBuf_f[idxN_0] = drum_bench_B.Subtract_d;
        i = 1;
      }

      idxN_0 = 0;
      idxN = 1 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_DW.Buffer_CircBuf_f[idxN_0 + i] = drum_bench_B.Subtract_d;
    }

    drum_bench_DW.Buffer_inBufPtrIdx_d++;
    if (drum_bench_DW.Buffer_inBufPtrIdx_d >= 256) {
      drum_bench_DW.Buffer_inBufPtrIdx_d -= 256;
    }

    drum_bench_DW.Buffer_bufferLength_k++;
    if (drum_bench_DW.Buffer_bufferLength_k > 256) {
      drum_bench_DW.Buffer_outBufPtrIdx_f = (drum_bench_DW.Buffer_outBufPtrIdx_f
        + drum_bench_DW.Buffer_bufferLength_k) - 256;
      if (drum_bench_DW.Buffer_outBufPtrIdx_f > 256) {
        drum_bench_DW.Buffer_outBufPtrIdx_f -= 256;
      }

      drum_bench_DW.Buffer_bufferLength_k = 256;
    }
  }

  if (drum_bench_M->Timing.TaskCounters.TID[5] == 0) {
    drum_bench_DW.Buffer_bufferLength_k -= 128;
    if (drum_bench_DW.Buffer_bufferLength_k < 0) {
      drum_bench_DW.Buffer_outBufPtrIdx_f += drum_bench_DW.Buffer_bufferLength_k;
      if (drum_bench_DW.Buffer_outBufPtrIdx_f < 0) {
        drum_bench_DW.Buffer_outBufPtrIdx_f += 256;
      }

      drum_bench_DW.Buffer_bufferLength_k = 0;
    }

    idxN_0 = 0;
    currentOffset = drum_bench_DW.Buffer_outBufPtrIdx_f;
    if (currentOffset < 0) {
      currentOffset += 256;
    }

    frameSize = 256 - currentOffset;
    idxN = 128;
    if (frameSize <= 128) {
      for (i = 0; i < frameSize; i++) {
        drum_bench_B.Buffer_h[i] = drum_bench_DW.Buffer_CircBuf_f[currentOffset
          + i];
      }

      idxN_0 = frameSize;
      currentOffset = 0;
      idxN = 128 - frameSize;
    }

    for (i = 0; i < idxN; i++) {
      drum_bench_B.Buffer_h[idxN_0 + i] =
        drum_bench_DW.Buffer_CircBuf_f[currentOffset + i];
    }

    currentOffset += idxN;
    drum_bench_DW.Buffer_outBufPtrIdx_f = currentOffset;

    /* S-Function (sdspwindow2): '<S15>/Window Function' */
    for (i = 0; i < 128; i++) {
      drum_bench_B.WindowFunction_c[i] = drum_bench_B.Buffer_h[i] *
        drum_bench_ConstP.pooled15[i];
    }

    /* End of S-Function (sdspwindow2): '<S15>/Window Function' */

    /* S-Function (sdspfft2): '<S19>/FFT' */
    MWDSPCG_FFT_Interleave_R2BR_D(&drum_bench_B.WindowFunction_c[0U],
      &drum_bench_B.FFT_f[0U], 1, 128);
    MWDSPCG_R2DIT_TBLS_Z(&drum_bench_B.FFT_f[0U], 1, 128, 64, 0,
                         drum_bench_ConstP.pooled16, 2, FALSE);
    MWDSPCG_FFT_DblLen_Z(&drum_bench_B.FFT_f[0U], 1, 128,
                         drum_bench_ConstP.pooled16, 1);

    /* Math: '<S19>/Magnitude Squared'
     *
     * About '<S19>/Magnitude Squared':
     *  Operator: magnitude^2
     */
    for (i = 0; i < 128; i++) {
      u = drum_bench_B.FFT_f[i];
      acc = u.re * u.re + u.im * u.im;
      drum_bench_B.MagnitudeSquared_h[i] = acc;
    }

    /* End of Math: '<S19>/Magnitude Squared' */

    /* MATLAB Function: '<S15>/MATLAB Function' */
    /* MATLAB Function 'Behavior Control/EMG Filter/MATLAB Function': '<S18>:1' */
    /* '<S18>:1:8' */
    for (i = 0; i < 20; i++) {
      x_0[i] = drum_bench_B.MagnitudeSquared_h[i] - drum_bench_DW.buf_j[i];
    }

    output = x_0[0];
    for (i = 0; i < 19; i++) {
      output += x_0[i + 1];
    }

    /* '<S18>:1:9' */
    memcpy(&drum_bench_DW.buf_j[0], &drum_bench_B.MagnitudeSquared_h[0], sizeof
           (real_T) << 7U);

    /* '<S18>:1:10' */
    drum_bench_B.y_d = output;

    /* End of MATLAB Function: '<S15>/MATLAB Function' */
  }

  /* End of Buffer: '<S15>/Buffer' */
  if (drum_bench_M->Timing.TaskCounters.TID[4] == 0) {
    /* Level2 S-Function Block: '<S1>/C++ S-function3' (EMG) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[2];
      sfcnOutputs(rts, 4);
    }
  }

  if (drum_bench_M->Timing.TaskCounters.TID[3] == 0) {
    /* Level2 S-Function Block: '<S1>/Inlined C++ S-Function' (behavior_controller) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[3];
      sfcnOutputs(rts, 3);
    }

    /* DataTypeConversion: '<S1>/Data Type Conversion' */
    drum_bench_B.DataTypeConversion = drum_bench_B.InlinedCSFunction_o1;

    /* DataTypeConversion: '<S1>/Data Type Conversion1' */
    drum_bench_B.DataTypeConversion1 = drum_bench_B.InlinedCSFunction_o4;
  }

  /* MATLAB Function: '<S15>/smooth3' */
  drum_bench_smooth3(0.0, &drum_bench_B.sf_smooth3, &drum_bench_DW.sf_smooth3);

  /* MATLAB Function: '<S16>/smooth1' */
  drum_bench_smooth1(0.0, &drum_bench_B.sf_smooth1_p,
                     &drum_bench_DW.sf_smooth1_p);

  /* MATLAB Function: '<S16>/smooth2' */
  drum_bench_smooth1(0.0, &drum_bench_B.sf_smooth2_h,
                     &drum_bench_DW.sf_smooth2_h);

  /* MATLAB Function: '<S16>/smooth3' */
  drum_bench_smooth3(0.0, &drum_bench_B.sf_smooth3_d,
                     &drum_bench_DW.sf_smooth3_d);

  /* Sum: '<S48>/Sum' incorporates:
   *  Constant: '<S48>/Constant'
   */
  drum_bench_B.Sum_c = drum_bench_B.DataTypeConversion +
    drum_bench_P.config.input.input0.quadrature_1_angle.bias;

  /* RateLimiter: '<S48>/Rate Limiter' */
  output = drum_bench_B.Sum_c - drum_bench_DW.PrevY;
  if (output > 10.0) {
    drum_bench_B.RateLimiter = drum_bench_DW.PrevY + 10.0;
  } else if (output < (-10.0)) {
    drum_bench_B.RateLimiter = drum_bench_DW.PrevY + (-10.0);
  } else {
    drum_bench_B.RateLimiter = drum_bench_B.Sum_c;
  }

  drum_bench_DW.PrevY = drum_bench_B.RateLimiter;

  /* End of RateLimiter: '<S48>/Rate Limiter' */

  /* DataTypeConversion: '<S48>/Data Type Conversion' */
  drum_bench_B.DataTypeConversion_b = drum_bench_B.RateLimiter;

  /* Sum: '<S49>/Sum' incorporates:
   *  Constant: '<S49>/Constant'
   */
  drum_bench_B.Sum_p = 0.0 +
    drum_bench_P.config.input.input0.quadrature_2_angle.bias;

  /* RateLimiter: '<S49>/Rate Limiter' */
  output = drum_bench_B.Sum_p - drum_bench_DW.PrevY_a;
  if (output > 9.9999999999999991E-5) {
    drum_bench_B.RateLimiter_o = drum_bench_DW.PrevY_a + 9.9999999999999991E-5;
  } else if (output < (-9.9999999999999991E-5)) {
    drum_bench_B.RateLimiter_o = drum_bench_DW.PrevY_a + (-9.9999999999999991E-5);
  } else {
    drum_bench_B.RateLimiter_o = drum_bench_B.Sum_p;
  }

  drum_bench_DW.PrevY_a = drum_bench_B.RateLimiter_o;

  /* End of RateLimiter: '<S49>/Rate Limiter' */

  /* DataTypeConversion: '<S49>/Data Type Conversion' */
  drum_bench_B.DataTypeConversion_a = drum_bench_B.RateLimiter_o;

  /* Sum: '<S45>/Sum' incorporates:
   *  Constant: '<S45>/Constant'
   */
  drum_bench_B.Sum_cn = 0.0 + drum_bench_P.config.input.input0.analog.bias;

  /* RateLimiter: '<S45>/Rate Limiter' */
  output = drum_bench_B.Sum_cn - drum_bench_DW.PrevY_h;
  if (output > 9.9999999999999991E-5) {
    drum_bench_B.RateLimiter_b = drum_bench_DW.PrevY_h + 9.9999999999999991E-5;
  } else if (output < (-9.9999999999999991E-5)) {
    drum_bench_B.RateLimiter_b = drum_bench_DW.PrevY_h + (-9.9999999999999991E-5);
  } else {
    drum_bench_B.RateLimiter_b = drum_bench_B.Sum_cn;
  }

  drum_bench_DW.PrevY_h = drum_bench_B.RateLimiter_b;

  /* End of RateLimiter: '<S45>/Rate Limiter' */

  /* DataTypeConversion: '<S45>/Data Type Conversion' */
  drum_bench_B.DataTypeConversion_c = drum_bench_B.RateLimiter_b;

  /* Sum: '<S46>/Sum' incorporates:
   *  Constant: '<S46>/Constant'
   */
  drum_bench_B.Sum_df = 0.0 + drum_bench_P.config.input.input0.motor_iq.bias;

  /* RateLimiter: '<S46>/Rate Limiter' */
  output = drum_bench_B.Sum_df - drum_bench_DW.PrevY_d;
  if (output > 1.0E+196) {
    drum_bench_B.RateLimiter_k = drum_bench_DW.PrevY_d + 1.0E+196;
  } else if (output < (-1.0E+196)) {
    drum_bench_B.RateLimiter_k = drum_bench_DW.PrevY_d + (-1.0E+196);
  } else {
    drum_bench_B.RateLimiter_k = drum_bench_B.Sum_df;
  }

  drum_bench_DW.PrevY_d = drum_bench_B.RateLimiter_k;

  /* End of RateLimiter: '<S46>/Rate Limiter' */

  /* DataTypeConversion: '<S46>/Data Type Conversion' */
  drum_bench_B.DataTypeConversion_n = drum_bench_B.RateLimiter_k;

  /* Sum: '<S47>/Sum' incorporates:
   *  Constant: '<S47>/Constant'
   */
  drum_bench_B.Sum_l = 0.0 + drum_bench_P.config.input.input0.motor_vq.bias;

  /* RateLimiter: '<S47>/Rate Limiter' */
  output = drum_bench_B.Sum_l - drum_bench_DW.PrevY_dx;
  if (output > 9.9999999999999991E-5) {
    drum_bench_B.RateLimiter_e = drum_bench_DW.PrevY_dx + 9.9999999999999991E-5;
  } else if (output < (-9.9999999999999991E-5)) {
    drum_bench_B.RateLimiter_e = drum_bench_DW.PrevY_dx +
      (-9.9999999999999991E-5);
  } else {
    drum_bench_B.RateLimiter_e = drum_bench_B.Sum_l;
  }

  drum_bench_DW.PrevY_dx = drum_bench_B.RateLimiter_e;

  /* End of RateLimiter: '<S47>/Rate Limiter' */

  /* DataTypeConversion: '<S47>/Data Type Conversion' */
  drum_bench_B.DataTypeConversion_d = drum_bench_B.RateLimiter_e;

  /* BusCreator: '<S4>/Bus Creator' incorporates:
   *  Constant: '<S4>/analog_diff_dot'
   *  Constant: '<S4>/mode'
   *  Constant: '<S4>/motor_id'
   *  Constant: '<S4>/motor_torque'
   *  Constant: '<S4>/motor_vd'
   *  Constant: '<S4>/quadrature_1_angle_dot'
   *  Constant: '<S4>/quadrature_2_angle_dot'
   *  Constant: '<S4>/rpc_command'
   */
  drum_bench_B.BusCreator_n.mode = drum_bench_P.config.input.input0.mode;
  drum_bench_B.BusCreator_n.quadrature_1_angle =
    drum_bench_B.DataTypeConversion_b;
  drum_bench_B.BusCreator_n.quadrature_1_angle_dot =
    drum_bench_P.config.input.input0.quadrature_1_angle_dot;
  drum_bench_B.BusCreator_n.quadrature_2_angle =
    drum_bench_B.DataTypeConversion_a;
  drum_bench_B.BusCreator_n.quadrature_2_angle_dot =
    drum_bench_P.config.input.input0.quadrature_2_angle_dot;
  drum_bench_B.BusCreator_n.analog_diff = drum_bench_B.DataTypeConversion_c;
  drum_bench_B.BusCreator_n.analog_diff_dot =
    drum_bench_P.config.input.input0.analog_dot;
  drum_bench_B.BusCreator_n.motor_torque =
    drum_bench_P.config.input.input0.motor_torque;
  drum_bench_B.BusCreator_n.motor_iq = drum_bench_B.DataTypeConversion_n;
  drum_bench_B.BusCreator_n.motor_id = drum_bench_P.config.input.input0.motor_id;
  drum_bench_B.BusCreator_n.motor_vq = drum_bench_B.DataTypeConversion_d;
  drum_bench_B.BusCreator_n.motor_vd = drum_bench_P.config.input.input0.motor_vd;
  drum_bench_B.BusCreator_n.rpc = drum_bench_P.config.input.input0.rpc_command;

  /* BusSelector: '<S2>/Bus Selector2' */
  drum_bench_B.quadrature_1_angle = drum_bench_B.BusCreator_n.quadrature_1_angle;
  drum_bench_B.quadrature_1_angle_dot =
    drum_bench_B.BusCreator_n.quadrature_1_angle_dot;
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* BusSelector: '<S2>/Bus Selector3' */
    drum_bench_B.quadrature_1_n =
      drum_bench_B.BusCreator_i.marlin_basic_status.quadrature_1;
  }

  /* Sum: '<S2>/Sum' */
  drum_bench_B.Sum_m = drum_bench_B.quadrature_1_angle -
    drum_bench_B.quadrature_1_n;

  /* Product: '<S2>/Product1' */
  drum_bench_B.Product1 = drum_bench_B.Sum_m * drum_bench_B.InlinedCSFunction_o2;

  /* Saturate: '<S2>/Saturation' */
  acc = drum_bench_B.Product1;
  output = (-12.0);
  u_0 = 12.0;
  if (acc >= u_0) {
    drum_bench_B.Saturation = u_0;
  } else if (acc <= output) {
    drum_bench_B.Saturation = output;
  } else {
    drum_bench_B.Saturation = acc;
  }

  /* End of Saturate: '<S2>/Saturation' */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* Outputs for Atomic SubSystem: '<S2>/Filtered Derivative' */
    drum_bench_FilteredDerivative(drum_bench_B.quadrature_1_n,
      &drum_bench_B.FilteredDerivative, &drum_bench_DW.FilteredDerivative,
      drum_bench_P.config.control0.impedence_control.imp_cutoff);

    /* End of Outputs for SubSystem: '<S2>/Filtered Derivative' */
  }

  /* Sum: '<S2>/Sum1' */
  drum_bench_B.Sum1 = drum_bench_B.quadrature_1_angle_dot -
    drum_bench_B.FilteredDerivative.FilterCoefficient;

  /* Product: '<S2>/Product2' */
  drum_bench_B.Product2 = drum_bench_B.Sum1 * drum_bench_B.InlinedCSFunction_o3;

  /* Sum: '<S2>/Sum2' */
  drum_bench_B.Sum2 = drum_bench_B.Saturation + drum_bench_B.Product2;

  /* DataTypeConversion: '<S2>/motor_torque' */
  drum_bench_B.motor_torque_pj = drum_bench_B.Sum2;

  /* BusAssignment: '<S2>/Bus Assignment' incorporates:
   *  Constant: '<S2>/current mode'
   */
  drum_bench_B.BusAssignment = drum_bench_B.BusCreator_n;
  drum_bench_B.BusAssignment.motor_iq = drum_bench_B.motor_torque_pj;
  drum_bench_B.BusAssignment.mode = 3.0;

  /* BusSelector: '<S12>/Bus Selector2' */
  drum_bench_B.quadrature_1_angle_k =
    drum_bench_B.BusCreator_n.quadrature_1_angle;
  drum_bench_B.quadrature_1_angle_dot_o =
    drum_bench_B.BusCreator_n.quadrature_1_angle_dot;
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* BusSelector: '<S12>/Bus Selector1' */
    drum_bench_B.quadrature_1_f =
      drum_bench_B.BusCreator_i.marlin_basic_status.quadrature_1;
    drum_bench_B.analog_1_i =
      drum_bench_B.BusCreator_i.marlin_basic_status.analog_1;
  }

  /* Sum: '<S12>/Sum' */
  drum_bench_B.Sum_e = drum_bench_B.quadrature_1_angle_k -
    drum_bench_B.quadrature_1_f;
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* Bias: '<S12>/bias' */
    drum_bench_B.bias = drum_bench_B.analog_1_i +
      drum_bench_P.config.control0.var_impedence_control.bias;

    /* Gain: '<S12>/ka' */
    drum_bench_B.ka = drum_bench_P.config.control0.var_impedence_control.ka *
      drum_bench_B.bias;
  }

  /* Product: '<S12>/Product' */
  drum_bench_B.Product = drum_bench_B.Sum_e * drum_bench_B.ka;
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* Outputs for Atomic SubSystem: '<S12>/Filtered Derivative' */
    drum_bench_FilteredDerivative(drum_bench_B.quadrature_1_f,
      &drum_bench_B.FilteredDerivative_c, &drum_bench_DW.FilteredDerivative_c,
      drum_bench_P.config.control0.var_impedence_control.var_imp_cutoff);

    /* End of Outputs for SubSystem: '<S12>/Filtered Derivative' */
  }

  /* Sum: '<S12>/Sum1' */
  drum_bench_B.Sum1_o = drum_bench_B.quadrature_1_angle_dot_o -
    drum_bench_B.FilteredDerivative_c.FilterCoefficient;

  /* Gain: '<S12>/kd' */
  drum_bench_B.kd = drum_bench_P.config.control0.var_impedence_control.kd *
    drum_bench_B.Sum1_o;

  /* Sum: '<S12>/Sum2' */
  drum_bench_B.Sum2_b = drum_bench_B.Product + drum_bench_B.kd;

  /* BusAssignment: '<S12>/Bus Assignment' incorporates:
   *  Constant: '<S12>/current mode'
   */
  drum_bench_B.BusAssignment_n = drum_bench_B.BusCreator_n;
  drum_bench_B.BusAssignment_n.motor_iq = drum_bench_B.Sum2_b;
  drum_bench_B.BusAssignment_n.mode = 3.0;

  /* MultiPortSwitch: '<Root>/mode switch' */
  switch ((int32_T)drum_bench_B.CSfunction1) {
   case 1:
    drum_bench_B.modeswitch = drum_bench_B.BusCreator_n;
    break;

   case 2:
    drum_bench_B.modeswitch = drum_bench_B.BusAssignment;
    break;

   case 3:
    drum_bench_B.modeswitch = drum_bench_B.BusAssignment_n;
    break;

   default:
    drum_bench_B.modeswitch = drum_bench_B.BusCreator_n;
    break;
  }

  /* End of MultiPortSwitch: '<Root>/mode switch' */

  /* BusSelector: '<Root>/Bus Selector' */
  drum_bench_B.mode = drum_bench_B.modeswitch.mode;
  drum_bench_B.motor_torque_h4 = drum_bench_B.modeswitch.motor_torque;
  drum_bench_B.motor_iq_b0 = drum_bench_B.modeswitch.motor_iq;

  /* Sum: '<S58>/Sum' incorporates:
   *  Constant: '<S58>/Constant'
   */
  drum_bench_B.Sum_cq = drum_bench_B.DataTypeConversion1 +
    drum_bench_P.config.input.input1.quadrature_1_angle.bias;

  /* RateLimiter: '<S58>/Rate Limiter' */
  output = drum_bench_B.Sum_cq - drum_bench_DW.PrevY_m;
  if (output > 10.0) {
    drum_bench_B.RateLimiter_d = drum_bench_DW.PrevY_m + 10.0;
  } else if (output < (-10.0)) {
    drum_bench_B.RateLimiter_d = drum_bench_DW.PrevY_m + (-10.0);
  } else {
    drum_bench_B.RateLimiter_d = drum_bench_B.Sum_cq;
  }

  drum_bench_DW.PrevY_m = drum_bench_B.RateLimiter_d;

  /* End of RateLimiter: '<S58>/Rate Limiter' */

  /* DataTypeConversion: '<S58>/Data Type Conversion' */
  drum_bench_B.DataTypeConversion_m = drum_bench_B.RateLimiter_d;

  /* Sum: '<S59>/Sum' incorporates:
   *  Constant: '<S59>/Constant'
   */
  drum_bench_B.Sum_a = 0.0 +
    drum_bench_P.config.input.input1.quadrature_2_angle.bias;

  /* RateLimiter: '<S59>/Rate Limiter' */
  output = drum_bench_B.Sum_a - drum_bench_DW.PrevY_b;
  if (output > 9.9999999999999991E-5) {
    drum_bench_B.RateLimiter_f = drum_bench_DW.PrevY_b + 9.9999999999999991E-5;
  } else if (output < (-9.9999999999999991E-5)) {
    drum_bench_B.RateLimiter_f = drum_bench_DW.PrevY_b + (-9.9999999999999991E-5);
  } else {
    drum_bench_B.RateLimiter_f = drum_bench_B.Sum_a;
  }

  drum_bench_DW.PrevY_b = drum_bench_B.RateLimiter_f;

  /* End of RateLimiter: '<S59>/Rate Limiter' */

  /* DataTypeConversion: '<S59>/Data Type Conversion' */
  drum_bench_B.DataTypeConversion_c2 = drum_bench_B.RateLimiter_f;

  /* Sum: '<S55>/Sum' incorporates:
   *  Constant: '<S55>/Constant'
   */
  drum_bench_B.Sum_g = 0.0 + drum_bench_P.config.input.input1.analog.bias;

  /* RateLimiter: '<S55>/Rate Limiter' */
  output = drum_bench_B.Sum_g - drum_bench_DW.PrevY_f;
  if (output > 9.9999999999999991E-5) {
    drum_bench_B.RateLimiter_b5 = drum_bench_DW.PrevY_f + 9.9999999999999991E-5;
  } else if (output < (-9.9999999999999991E-5)) {
    drum_bench_B.RateLimiter_b5 = drum_bench_DW.PrevY_f +
      (-9.9999999999999991E-5);
  } else {
    drum_bench_B.RateLimiter_b5 = drum_bench_B.Sum_g;
  }

  drum_bench_DW.PrevY_f = drum_bench_B.RateLimiter_b5;

  /* End of RateLimiter: '<S55>/Rate Limiter' */

  /* DataTypeConversion: '<S55>/Data Type Conversion' */
  drum_bench_B.DataTypeConversion_l = drum_bench_B.RateLimiter_b5;

  /* Sum: '<S56>/Sum' incorporates:
   *  Constant: '<S56>/Constant'
   */
  drum_bench_B.Sum_b = 0.0 + drum_bench_P.config.input.input1.motor_iq.bias;

  /* RateLimiter: '<S56>/Rate Limiter' */
  output = drum_bench_B.Sum_b - drum_bench_DW.PrevY_d5;
  if (output > 1.0E+196) {
    drum_bench_B.RateLimiter_eb = drum_bench_DW.PrevY_d5 + 1.0E+196;
  } else if (output < (-1.0E+196)) {
    drum_bench_B.RateLimiter_eb = drum_bench_DW.PrevY_d5 + (-1.0E+196);
  } else {
    drum_bench_B.RateLimiter_eb = drum_bench_B.Sum_b;
  }

  drum_bench_DW.PrevY_d5 = drum_bench_B.RateLimiter_eb;

  /* End of RateLimiter: '<S56>/Rate Limiter' */

  /* DataTypeConversion: '<S56>/Data Type Conversion' */
  drum_bench_B.DataTypeConversion_j = drum_bench_B.RateLimiter_eb;

  /* Sum: '<S57>/Sum' incorporates:
   *  Constant: '<S57>/Constant'
   */
  drum_bench_B.Sum_o = 0.0 + drum_bench_P.config.input.input1.motor_vq.bias;

  /* RateLimiter: '<S57>/Rate Limiter' */
  output = drum_bench_B.Sum_o - drum_bench_DW.PrevY_k;
  if (output > 9.9999999999999991E-5) {
    drum_bench_B.RateLimiter_ol = drum_bench_DW.PrevY_k + 9.9999999999999991E-5;
  } else if (output < (-9.9999999999999991E-5)) {
    drum_bench_B.RateLimiter_ol = drum_bench_DW.PrevY_k +
      (-9.9999999999999991E-5);
  } else {
    drum_bench_B.RateLimiter_ol = drum_bench_B.Sum_o;
  }

  drum_bench_DW.PrevY_k = drum_bench_B.RateLimiter_ol;

  /* End of RateLimiter: '<S57>/Rate Limiter' */

  /* DataTypeConversion: '<S57>/Data Type Conversion' */
  drum_bench_B.DataTypeConversion_e = drum_bench_B.RateLimiter_ol;

  /* BusCreator: '<S5>/Bus Creator' incorporates:
   *  Constant: '<S5>/analog_diff_dot'
   *  Constant: '<S5>/mode'
   *  Constant: '<S5>/motor_id'
   *  Constant: '<S5>/motor_torque'
   *  Constant: '<S5>/motor_vd'
   *  Constant: '<S5>/quadrature_1_angle_dot'
   *  Constant: '<S5>/quadrature_2_angle_dot'
   *  Constant: '<S5>/rpc_command'
   */
  drum_bench_B.BusCreator_f.mode = drum_bench_P.config.input.input1.mode;
  drum_bench_B.BusCreator_f.quadrature_1_angle =
    drum_bench_B.DataTypeConversion_m;
  drum_bench_B.BusCreator_f.quadrature_1_angle_dot =
    drum_bench_P.config.input.input1.quadrature_1_angle_dot;
  drum_bench_B.BusCreator_f.quadrature_2_angle =
    drum_bench_B.DataTypeConversion_c2;
  drum_bench_B.BusCreator_f.quadrature_2_angle_dot =
    drum_bench_P.config.input.input1.quadrature_2_angle_dot;
  drum_bench_B.BusCreator_f.analog_diff = drum_bench_B.DataTypeConversion_l;
  drum_bench_B.BusCreator_f.analog_diff_dot =
    drum_bench_P.config.input.input1.analog_dot;
  drum_bench_B.BusCreator_f.motor_torque =
    drum_bench_P.config.input.input1.motor_torque;
  drum_bench_B.BusCreator_f.motor_iq = drum_bench_B.DataTypeConversion_j;
  drum_bench_B.BusCreator_f.motor_id = drum_bench_P.config.input.input1.motor_id;
  drum_bench_B.BusCreator_f.motor_vq = drum_bench_B.DataTypeConversion_e;
  drum_bench_B.BusCreator_f.motor_vd = drum_bench_P.config.input.input1.motor_vd;
  drum_bench_B.BusCreator_f.rpc = drum_bench_P.config.input.input1.rpc_command;

  /* BusSelector: '<S3>/Bus Selector2' */
  drum_bench_B.quadrature_1_angle_j =
    drum_bench_B.BusCreator_f.quadrature_1_angle;
  drum_bench_B.quadrature_1_angle_dot_of =
    drum_bench_B.BusCreator_f.quadrature_1_angle_dot;
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* BusSelector: '<S3>/Bus Selector3' */
    drum_bench_B.quadrature_1_nt =
      drum_bench_B.BusCreator_m.marlin_basic_status.quadrature_1;
  }

  /* Sum: '<S3>/Sum' */
  drum_bench_B.Sum_f = drum_bench_B.quadrature_1_angle_j -
    drum_bench_B.quadrature_1_nt;

  /* Product: '<S3>/Product1' */
  drum_bench_B.Product1_j = drum_bench_B.Sum_f *
    drum_bench_B.InlinedCSFunction_o5;

  /* Saturate: '<S3>/Saturation' */
  acc = drum_bench_B.Product1_j;
  output = (-12.0);
  u_0 = 12.0;
  if (acc >= u_0) {
    drum_bench_B.Saturation_l = u_0;
  } else if (acc <= output) {
    drum_bench_B.Saturation_l = output;
  } else {
    drum_bench_B.Saturation_l = acc;
  }

  /* End of Saturate: '<S3>/Saturation' */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* Outputs for Atomic SubSystem: '<S3>/Filtered Derivative' */
    drum_bench_FilteredDerivative(drum_bench_B.quadrature_1_nt,
      &drum_bench_B.FilteredDerivative_n, &drum_bench_DW.FilteredDerivative_n,
      drum_bench_P.config.control1.impedence_control.imp_cutoff);

    /* End of Outputs for SubSystem: '<S3>/Filtered Derivative' */
  }

  /* Sum: '<S3>/Sum1' */
  drum_bench_B.Sum1_h = drum_bench_B.quadrature_1_angle_dot_of -
    drum_bench_B.FilteredDerivative_n.FilterCoefficient;

  /* Product: '<S3>/Product2' */
  drum_bench_B.Product2_g = drum_bench_B.Sum1_h *
    drum_bench_B.InlinedCSFunction_o6;

  /* Sum: '<S3>/Sum2' */
  drum_bench_B.Sum2_e = drum_bench_B.Saturation_l + drum_bench_B.Product2_g;

  /* DataTypeConversion: '<S3>/motor_torque' */
  drum_bench_B.motor_torque_bg = drum_bench_B.Sum2_e;

  /* BusAssignment: '<S3>/Bus Assignment' incorporates:
   *  Constant: '<S3>/current mode'
   */
  drum_bench_B.BusAssignment_n4 = drum_bench_B.BusCreator_f;
  drum_bench_B.BusAssignment_n4.motor_iq = drum_bench_B.motor_torque_bg;
  drum_bench_B.BusAssignment_n4.mode = 3.0;

  /* BusSelector: '<S13>/Bus Selector2' */
  drum_bench_B.quadrature_1_angle_o =
    drum_bench_B.BusCreator_f.quadrature_1_angle;
  drum_bench_B.quadrature_1_angle_dot_e =
    drum_bench_B.BusCreator_f.quadrature_1_angle_dot;
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* BusSelector: '<S13>/Bus Selector1' */
    drum_bench_B.quadrature_1_o =
      drum_bench_B.BusCreator_m.marlin_basic_status.quadrature_1;
    drum_bench_B.analog_1_h =
      drum_bench_B.BusCreator_m.marlin_basic_status.analog_1;
  }

  /* Sum: '<S13>/Sum' */
  drum_bench_B.Sum_co = drum_bench_B.quadrature_1_angle_o -
    drum_bench_B.quadrature_1_o;
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* Bias: '<S13>/bias' */
    drum_bench_B.bias_c = drum_bench_B.analog_1_h +
      drum_bench_P.config.control1.var_impedence_control.bias;

    /* Gain: '<S13>/ka' */
    drum_bench_B.ka_l = drum_bench_P.config.control1.var_impedence_control.ka *
      drum_bench_B.bias_c;
  }

  /* Product: '<S13>/Product' */
  drum_bench_B.Product_i = drum_bench_B.Sum_co * drum_bench_B.ka_l;
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* Outputs for Atomic SubSystem: '<S13>/Filtered Derivative' */
    drum_bench_FilteredDerivative(drum_bench_B.quadrature_1_o,
      &drum_bench_B.FilteredDerivative_p, &drum_bench_DW.FilteredDerivative_p,
      drum_bench_P.config.control1.var_impedence_control.var_imp_cutoff);

    /* End of Outputs for SubSystem: '<S13>/Filtered Derivative' */
  }

  /* Sum: '<S13>/Sum1' */
  drum_bench_B.Sum1_e = drum_bench_B.quadrature_1_angle_dot_e -
    drum_bench_B.FilteredDerivative_p.FilterCoefficient;

  /* Gain: '<S13>/kd' */
  drum_bench_B.kd_l = drum_bench_P.config.control1.var_impedence_control.kd *
    drum_bench_B.Sum1_e;

  /* Sum: '<S13>/Sum2' */
  drum_bench_B.Sum2_o = drum_bench_B.Product_i + drum_bench_B.kd_l;

  /* BusAssignment: '<S13>/Bus Assignment' incorporates:
   *  Constant: '<S13>/current mode'
   */
  drum_bench_B.BusAssignment_nx = drum_bench_B.BusCreator_f;
  drum_bench_B.BusAssignment_nx.motor_iq = drum_bench_B.Sum2_o;
  drum_bench_B.BusAssignment_nx.mode = 3.0;

  /* MultiPortSwitch: '<Root>/mode switch1' */
  switch ((int32_T)drum_bench_B.CSfunction1) {
   case 1:
    drum_bench_B.modeswitch1 = drum_bench_B.BusCreator_f;
    break;

   case 2:
    drum_bench_B.modeswitch1 = drum_bench_B.BusAssignment_n4;
    break;

   case 3:
    drum_bench_B.modeswitch1 = drum_bench_B.BusAssignment_nx;
    break;

   default:
    drum_bench_B.modeswitch1 = drum_bench_B.BusCreator_f;
    break;
  }

  /* End of MultiPortSwitch: '<Root>/mode switch1' */

  /* BusSelector: '<Root>/Bus Selector1' */
  drum_bench_B.mode_l = drum_bench_B.modeswitch1.mode;
  drum_bench_B.motor_torque_px = drum_bench_B.modeswitch1.motor_torque;
  drum_bench_B.motor_iq_n = drum_bench_B.modeswitch1.motor_iq;

  /* Gain: '<S2>/kd' */
  drum_bench_B.kd_b = drum_bench_P.config.control0.impedence_control.kd * 0.0;

  /* Gain: '<S2>/kp' */
  drum_bench_B.kp = drum_bench_P.config.control0.impedence_control.kp * 0.0;

  /* Gain: '<S3>/kd' */
  drum_bench_B.kd_j = drum_bench_P.config.control1.impedence_control.kd * 0.0;

  /* Gain: '<S3>/kp' */
  drum_bench_B.kp_h = drum_bench_P.config.control1.impedence_control.kp * 0.0;

  /* Clock: '<S50>/Clock1' */
  drum_bench_B.Clock1 = drum_bench_M->Timing.t[0];

  /* Product: '<S50>/Product' incorporates:
   *  Constant: '<S50>/deltaFreq'
   *  Constant: '<S50>/targetTime'
   */
  acc = drum_bench_P.config.input.input0.analog.frequency - 0.1;
  output = 6.2831853071795862 * acc;
  drum_bench_B.Product_j = output / 100.0;

  /* Gain: '<S50>/Gain' */
  drum_bench_B.Gain_g = 0.5 * drum_bench_B.Product_j;

  /* Product: '<S50>/Product1' */
  drum_bench_B.Product1_k = drum_bench_B.Clock1 * drum_bench_B.Gain_g;

  /* Sum: '<S50>/Sum' incorporates:
   *  Constant: '<S50>/initialFreq'
   */
  drum_bench_B.Sum_j = drum_bench_B.Product1_k + 0.62831853071795862;

  /* Product: '<S50>/Product2' */
  drum_bench_B.Product2_n = drum_bench_B.Clock1 * drum_bench_B.Sum_j;

  /* Trigonometry: '<S50>/Output' */
  drum_bench_B.Output = sin(drum_bench_B.Product2_n);

  /* Sin: '<S45>/Sine Wave' */
  drum_bench_B.SineWave = sin(drum_bench_P.config.input.input0.analog.frequency *
    drum_bench_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S45>/Sign' */
  acc = drum_bench_B.SineWave;
  if (acc < 0.0) {
    drum_bench_B.Sign = -1.0;
  } else if (acc > 0.0) {
    drum_bench_B.Sign = 1.0;
  } else if (acc == 0.0) {
    drum_bench_B.Sign = 0.0;
  } else {
    drum_bench_B.Sign = acc;
  }

  /* End of Signum: '<S45>/Sign' */

  /* MultiPortSwitch: '<S45>/Multiport Switch' incorporates:
   *  Constant: '<S45>/Constant1'
   *  Constant: '<S45>/select input'
   */
  switch ((int32_T)drum_bench_P.config.input.input0.analog.select_input) {
   case 0:
    drum_bench_B.MultiportSwitch = 0.0;
    break;

   case 1:
    drum_bench_B.MultiportSwitch = drum_bench_B.SineWave;
    break;

   case 2:
    drum_bench_B.MultiportSwitch = drum_bench_B.Sign;
    break;

   case 3:
    drum_bench_B.MultiportSwitch = drum_bench_B.Output;
    break;

   default:
    drum_bench_B.MultiportSwitch = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S45>/Multiport Switch' */

  /* Gain: '<S45>/Gain' */
  drum_bench_B.Gain_a = drum_bench_P.config.input.input0.analog.amplitude * 0.0;

  /* Clock: '<S51>/Clock1' */
  drum_bench_B.Clock1_n = drum_bench_M->Timing.t[0];

  /* Product: '<S51>/Product' incorporates:
   *  Constant: '<S51>/deltaFreq'
   *  Constant: '<S51>/targetTime'
   */
  acc = drum_bench_P.config.input.input0.motor_iq.frequency - 0.1;
  output = 6.2831853071795862 * acc;
  drum_bench_B.Product_p = output / 100.0;

  /* Gain: '<S51>/Gain' */
  drum_bench_B.Gain_b = 0.5 * drum_bench_B.Product_p;

  /* Product: '<S51>/Product1' */
  drum_bench_B.Product1_o = drum_bench_B.Clock1_n * drum_bench_B.Gain_b;

  /* Sum: '<S51>/Sum' incorporates:
   *  Constant: '<S51>/initialFreq'
   */
  drum_bench_B.Sum_k = drum_bench_B.Product1_o + 0.62831853071795862;

  /* Product: '<S51>/Product2' */
  drum_bench_B.Product2_p = drum_bench_B.Clock1_n * drum_bench_B.Sum_k;

  /* Trigonometry: '<S51>/Output' */
  drum_bench_B.Output_p = sin(drum_bench_B.Product2_p);

  /* Sin: '<S46>/Sine Wave' */
  drum_bench_B.SineWave_h = sin
    (drum_bench_P.config.input.input0.motor_iq.frequency *
     drum_bench_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S46>/Sign' */
  acc = drum_bench_B.SineWave_h;
  if (acc < 0.0) {
    drum_bench_B.Sign_a = -1.0;
  } else if (acc > 0.0) {
    drum_bench_B.Sign_a = 1.0;
  } else if (acc == 0.0) {
    drum_bench_B.Sign_a = 0.0;
  } else {
    drum_bench_B.Sign_a = acc;
  }

  /* End of Signum: '<S46>/Sign' */

  /* MultiPortSwitch: '<S46>/Multiport Switch' incorporates:
   *  Constant: '<S46>/Constant1'
   *  Constant: '<S46>/select input'
   */
  switch ((int32_T)drum_bench_P.config.input.input0.motor_iq.select_input) {
   case 0:
    drum_bench_B.MultiportSwitch_e = 0.0;
    break;

   case 1:
    drum_bench_B.MultiportSwitch_e = drum_bench_B.SineWave_h;
    break;

   case 2:
    drum_bench_B.MultiportSwitch_e = drum_bench_B.Sign_a;
    break;

   case 3:
    drum_bench_B.MultiportSwitch_e = drum_bench_B.Output_p;
    break;

   default:
    drum_bench_B.MultiportSwitch_e = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S46>/Multiport Switch' */

  /* Gain: '<S46>/Gain' */
  drum_bench_B.Gain_bc = drum_bench_P.config.input.input0.motor_iq.amplitude *
    0.0;

  /* Clock: '<S52>/Clock1' */
  drum_bench_B.Clock1_e = drum_bench_M->Timing.t[0];

  /* Product: '<S52>/Product' incorporates:
   *  Constant: '<S52>/deltaFreq'
   *  Constant: '<S52>/targetTime'
   */
  acc = drum_bench_P.config.input.input0.motor_vq.frequency - 0.1;
  output = 6.2831853071795862 * acc;
  drum_bench_B.Product_b = output / 100.0;

  /* Gain: '<S52>/Gain' */
  drum_bench_B.Gain_nn = 0.5 * drum_bench_B.Product_b;

  /* Product: '<S52>/Product1' */
  drum_bench_B.Product1_c = drum_bench_B.Clock1_e * drum_bench_B.Gain_nn;

  /* Sum: '<S52>/Sum' incorporates:
   *  Constant: '<S52>/initialFreq'
   */
  drum_bench_B.Sum_kn = drum_bench_B.Product1_c + 0.62831853071795862;

  /* Product: '<S52>/Product2' */
  drum_bench_B.Product2_no = drum_bench_B.Clock1_e * drum_bench_B.Sum_kn;

  /* Trigonometry: '<S52>/Output' */
  drum_bench_B.Output_n = sin(drum_bench_B.Product2_no);

  /* Sin: '<S47>/Sine Wave' */
  drum_bench_B.SineWave_f = sin
    (drum_bench_P.config.input.input0.motor_vq.frequency *
     drum_bench_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S47>/Sign' */
  acc = drum_bench_B.SineWave_f;
  if (acc < 0.0) {
    drum_bench_B.Sign_m = -1.0;
  } else if (acc > 0.0) {
    drum_bench_B.Sign_m = 1.0;
  } else if (acc == 0.0) {
    drum_bench_B.Sign_m = 0.0;
  } else {
    drum_bench_B.Sign_m = acc;
  }

  /* End of Signum: '<S47>/Sign' */

  /* MultiPortSwitch: '<S47>/Multiport Switch' incorporates:
   *  Constant: '<S47>/Constant1'
   *  Constant: '<S47>/select input'
   */
  switch ((int32_T)drum_bench_P.config.input.input0.motor_vq.select_input) {
   case 0:
    drum_bench_B.MultiportSwitch_l = 0.0;
    break;

   case 1:
    drum_bench_B.MultiportSwitch_l = drum_bench_B.SineWave_f;
    break;

   case 2:
    drum_bench_B.MultiportSwitch_l = drum_bench_B.Sign_m;
    break;

   case 3:
    drum_bench_B.MultiportSwitch_l = drum_bench_B.Output_n;
    break;

   default:
    drum_bench_B.MultiportSwitch_l = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S47>/Multiport Switch' */

  /* Gain: '<S47>/Gain' */
  drum_bench_B.Gain_l = drum_bench_P.config.input.input0.motor_vq.amplitude *
    0.0;

  /* Clock: '<S53>/Clock1' */
  drum_bench_B.Clock1_g = drum_bench_M->Timing.t[0];

  /* Product: '<S53>/Product' incorporates:
   *  Constant: '<S53>/deltaFreq'
   *  Constant: '<S53>/targetTime'
   */
  acc = drum_bench_P.config.input.input0.quadrature_1_angle.frequency - 0.1;
  output = 6.2831853071795862 * acc;
  drum_bench_B.Product_d = output / 100.0;

  /* Gain: '<S53>/Gain' */
  drum_bench_B.Gain_nc = 0.5 * drum_bench_B.Product_d;

  /* Product: '<S53>/Product1' */
  drum_bench_B.Product1_i = drum_bench_B.Clock1_g * drum_bench_B.Gain_nc;

  /* Sum: '<S53>/Sum' incorporates:
   *  Constant: '<S53>/initialFreq'
   */
  drum_bench_B.Sum_i = drum_bench_B.Product1_i + 0.62831853071795862;

  /* Product: '<S53>/Product2' */
  drum_bench_B.Product2_i = drum_bench_B.Clock1_g * drum_bench_B.Sum_i;

  /* Trigonometry: '<S53>/Output' */
  drum_bench_B.Output_h = sin(drum_bench_B.Product2_i);

  /* Sin: '<S48>/Sine Wave' */
  drum_bench_B.SineWave_l = sin
    (drum_bench_P.config.input.input0.quadrature_1_angle.frequency *
     drum_bench_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S48>/Sign' */
  acc = drum_bench_B.SineWave_l;
  if (acc < 0.0) {
    drum_bench_B.Sign_f = -1.0;
  } else if (acc > 0.0) {
    drum_bench_B.Sign_f = 1.0;
  } else if (acc == 0.0) {
    drum_bench_B.Sign_f = 0.0;
  } else {
    drum_bench_B.Sign_f = acc;
  }

  /* End of Signum: '<S48>/Sign' */

  /* MultiPortSwitch: '<S48>/Multiport Switch' incorporates:
   *  Constant: '<S48>/Constant1'
   *  Constant: '<S48>/select input'
   */
  switch ((int32_T)
          drum_bench_P.config.input.input0.quadrature_1_angle.select_input) {
   case 0:
    drum_bench_B.MultiportSwitch_n = 0.0;
    break;

   case 1:
    drum_bench_B.MultiportSwitch_n = drum_bench_B.SineWave_l;
    break;

   case 2:
    drum_bench_B.MultiportSwitch_n = drum_bench_B.Sign_f;
    break;

   case 3:
    drum_bench_B.MultiportSwitch_n = drum_bench_B.Output_h;
    break;

   default:
    drum_bench_B.MultiportSwitch_n = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S48>/Multiport Switch' */

  /* Gain: '<S48>/Gain' */
  drum_bench_B.Gain_f =
    drum_bench_P.config.input.input0.quadrature_1_angle.amplitude * 0.0;

  /* Clock: '<S54>/Clock1' */
  drum_bench_B.Clock1_j = drum_bench_M->Timing.t[0];

  /* Product: '<S54>/Product' incorporates:
   *  Constant: '<S54>/deltaFreq'
   *  Constant: '<S54>/targetTime'
   */
  acc = drum_bench_P.config.input.input0.quadrature_2_angle.frequency - 0.1;
  output = 6.2831853071795862 * acc;
  drum_bench_B.Product_h = output / 100.0;

  /* Gain: '<S54>/Gain' */
  drum_bench_B.Gain_nz = 0.5 * drum_bench_B.Product_h;

  /* Product: '<S54>/Product1' */
  drum_bench_B.Product1_f = drum_bench_B.Clock1_j * drum_bench_B.Gain_nz;

  /* Sum: '<S54>/Sum' incorporates:
   *  Constant: '<S54>/initialFreq'
   */
  drum_bench_B.Sum_bg = drum_bench_B.Product1_f + 0.62831853071795862;

  /* Product: '<S54>/Product2' */
  drum_bench_B.Product2_l = drum_bench_B.Clock1_j * drum_bench_B.Sum_bg;

  /* Trigonometry: '<S54>/Output' */
  drum_bench_B.Output_c = sin(drum_bench_B.Product2_l);

  /* Sin: '<S49>/Sine Wave' */
  drum_bench_B.SineWave_n = sin
    (drum_bench_P.config.input.input0.quadrature_2_angle.frequency *
     drum_bench_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S49>/Sign' */
  acc = drum_bench_B.SineWave_n;
  if (acc < 0.0) {
    drum_bench_B.Sign_i = -1.0;
  } else if (acc > 0.0) {
    drum_bench_B.Sign_i = 1.0;
  } else if (acc == 0.0) {
    drum_bench_B.Sign_i = 0.0;
  } else {
    drum_bench_B.Sign_i = acc;
  }

  /* End of Signum: '<S49>/Sign' */

  /* MultiPortSwitch: '<S49>/Multiport Switch' incorporates:
   *  Constant: '<S49>/Constant1'
   *  Constant: '<S49>/select input'
   */
  switch ((int32_T)
          drum_bench_P.config.input.input0.quadrature_2_angle.select_input) {
   case 0:
    drum_bench_B.MultiportSwitch_b = 0.0;
    break;

   case 1:
    drum_bench_B.MultiportSwitch_b = drum_bench_B.SineWave_n;
    break;

   case 2:
    drum_bench_B.MultiportSwitch_b = drum_bench_B.Sign_i;
    break;

   case 3:
    drum_bench_B.MultiportSwitch_b = drum_bench_B.Output_c;
    break;

   default:
    drum_bench_B.MultiportSwitch_b = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S49>/Multiport Switch' */

  /* Gain: '<S49>/Gain' */
  drum_bench_B.Gain_gk =
    drum_bench_P.config.input.input0.quadrature_2_angle.amplitude * 0.0;

  /* Clock: '<S60>/Clock1' */
  drum_bench_B.Clock1_gp = drum_bench_M->Timing.t[0];

  /* Product: '<S60>/Product' incorporates:
   *  Constant: '<S60>/deltaFreq'
   *  Constant: '<S60>/targetTime'
   */
  acc = drum_bench_P.config.input.input1.analog.frequency - 0.1;
  output = 6.2831853071795862 * acc;
  drum_bench_B.Product_bh = output / 100.0;

  /* Gain: '<S60>/Gain' */
  drum_bench_B.Gain_a3 = 0.5 * drum_bench_B.Product_bh;

  /* Product: '<S60>/Product1' */
  drum_bench_B.Product1_e = drum_bench_B.Clock1_gp * drum_bench_B.Gain_a3;

  /* Sum: '<S60>/Sum' incorporates:
   *  Constant: '<S60>/initialFreq'
   */
  drum_bench_B.Sum_i2 = drum_bench_B.Product1_e + 0.62831853071795862;

  /* Product: '<S60>/Product2' */
  drum_bench_B.Product2_d = drum_bench_B.Clock1_gp * drum_bench_B.Sum_i2;

  /* Trigonometry: '<S60>/Output' */
  drum_bench_B.Output_i = sin(drum_bench_B.Product2_d);

  /* Sin: '<S55>/Sine Wave' */
  drum_bench_B.SineWave_ff = sin
    (drum_bench_P.config.input.input1.analog.frequency * drum_bench_M->Timing.t
     [0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S55>/Sign' */
  acc = drum_bench_B.SineWave_ff;
  if (acc < 0.0) {
    drum_bench_B.Sign_ml = -1.0;
  } else if (acc > 0.0) {
    drum_bench_B.Sign_ml = 1.0;
  } else if (acc == 0.0) {
    drum_bench_B.Sign_ml = 0.0;
  } else {
    drum_bench_B.Sign_ml = acc;
  }

  /* End of Signum: '<S55>/Sign' */

  /* MultiPortSwitch: '<S55>/Multiport Switch' incorporates:
   *  Constant: '<S55>/Constant1'
   *  Constant: '<S55>/select input'
   */
  switch ((int32_T)drum_bench_P.config.input.input1.analog.select_input) {
   case 0:
    drum_bench_B.MultiportSwitch_h = 0.0;
    break;

   case 1:
    drum_bench_B.MultiportSwitch_h = drum_bench_B.SineWave_ff;
    break;

   case 2:
    drum_bench_B.MultiportSwitch_h = drum_bench_B.Sign_ml;
    break;

   case 3:
    drum_bench_B.MultiportSwitch_h = drum_bench_B.Output_i;
    break;

   default:
    drum_bench_B.MultiportSwitch_h = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S55>/Multiport Switch' */

  /* Gain: '<S55>/Gain' */
  drum_bench_B.Gain_ncb = drum_bench_P.config.input.input1.analog.amplitude *
    0.0;

  /* Clock: '<S61>/Clock1' */
  drum_bench_B.Clock1_f = drum_bench_M->Timing.t[0];

  /* Product: '<S61>/Product' incorporates:
   *  Constant: '<S61>/deltaFreq'
   *  Constant: '<S61>/targetTime'
   */
  acc = drum_bench_P.config.input.input1.motor_iq.frequency - 0.1;
  output = 6.2831853071795862 * acc;
  drum_bench_B.Product_ie = output / 100.0;

  /* Gain: '<S61>/Gain' */
  drum_bench_B.Gain_d = 0.5 * drum_bench_B.Product_ie;

  /* Product: '<S61>/Product1' */
  drum_bench_B.Product1_p = drum_bench_B.Clock1_f * drum_bench_B.Gain_d;

  /* Sum: '<S61>/Sum' incorporates:
   *  Constant: '<S61>/initialFreq'
   */
  drum_bench_B.Sum_c3 = drum_bench_B.Product1_p + 0.62831853071795862;

  /* Product: '<S61>/Product2' */
  drum_bench_B.Product2_np = drum_bench_B.Clock1_f * drum_bench_B.Sum_c3;

  /* Trigonometry: '<S61>/Output' */
  drum_bench_B.Output_f = sin(drum_bench_B.Product2_np);

  /* Sin: '<S56>/Sine Wave' */
  drum_bench_B.SineWave_hf = sin
    (drum_bench_P.config.input.input1.motor_iq.frequency *
     drum_bench_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S56>/Sign' */
  acc = drum_bench_B.SineWave_hf;
  if (acc < 0.0) {
    drum_bench_B.Sign_h = -1.0;
  } else if (acc > 0.0) {
    drum_bench_B.Sign_h = 1.0;
  } else if (acc == 0.0) {
    drum_bench_B.Sign_h = 0.0;
  } else {
    drum_bench_B.Sign_h = acc;
  }

  /* End of Signum: '<S56>/Sign' */

  /* MultiPortSwitch: '<S56>/Multiport Switch' incorporates:
   *  Constant: '<S56>/Constant1'
   *  Constant: '<S56>/select input'
   */
  switch ((int32_T)drum_bench_P.config.input.input1.motor_iq.select_input) {
   case 0:
    drum_bench_B.MultiportSwitch_o = 0.0;
    break;

   case 1:
    drum_bench_B.MultiportSwitch_o = drum_bench_B.SineWave_hf;
    break;

   case 2:
    drum_bench_B.MultiportSwitch_o = drum_bench_B.Sign_h;
    break;

   case 3:
    drum_bench_B.MultiportSwitch_o = drum_bench_B.Output_f;
    break;

   default:
    drum_bench_B.MultiportSwitch_o = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S56>/Multiport Switch' */

  /* Gain: '<S56>/Gain' */
  drum_bench_B.Gain_gv = drum_bench_P.config.input.input1.motor_iq.amplitude *
    0.0;

  /* Clock: '<S62>/Clock1' */
  drum_bench_B.Clock1_l = drum_bench_M->Timing.t[0];

  /* Product: '<S62>/Product' incorporates:
   *  Constant: '<S62>/deltaFreq'
   *  Constant: '<S62>/targetTime'
   */
  acc = drum_bench_P.config.input.input1.motor_vq.frequency - 0.1;
  output = 6.2831853071795862 * acc;
  drum_bench_B.Product_k = output / 100.0;

  /* Gain: '<S62>/Gain' */
  drum_bench_B.Gain_h = 0.5 * drum_bench_B.Product_k;

  /* Product: '<S62>/Product1' */
  drum_bench_B.Product1_pt = drum_bench_B.Clock1_l * drum_bench_B.Gain_h;

  /* Sum: '<S62>/Sum' incorporates:
   *  Constant: '<S62>/initialFreq'
   */
  drum_bench_B.Sum_dl = drum_bench_B.Product1_pt + 0.62831853071795862;

  /* Product: '<S62>/Product2' */
  drum_bench_B.Product2_j = drum_bench_B.Clock1_l * drum_bench_B.Sum_dl;

  /* Trigonometry: '<S62>/Output' */
  drum_bench_B.Output_e = sin(drum_bench_B.Product2_j);

  /* Sin: '<S57>/Sine Wave' */
  drum_bench_B.SineWave_g = sin
    (drum_bench_P.config.input.input1.motor_vq.frequency *
     drum_bench_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S57>/Sign' */
  acc = drum_bench_B.SineWave_g;
  if (acc < 0.0) {
    drum_bench_B.Sign_k = -1.0;
  } else if (acc > 0.0) {
    drum_bench_B.Sign_k = 1.0;
  } else if (acc == 0.0) {
    drum_bench_B.Sign_k = 0.0;
  } else {
    drum_bench_B.Sign_k = acc;
  }

  /* End of Signum: '<S57>/Sign' */

  /* MultiPortSwitch: '<S57>/Multiport Switch' incorporates:
   *  Constant: '<S57>/Constant1'
   *  Constant: '<S57>/select input'
   */
  switch ((int32_T)drum_bench_P.config.input.input1.motor_vq.select_input) {
   case 0:
    drum_bench_B.MultiportSwitch_a = 0.0;
    break;

   case 1:
    drum_bench_B.MultiportSwitch_a = drum_bench_B.SineWave_g;
    break;

   case 2:
    drum_bench_B.MultiportSwitch_a = drum_bench_B.Sign_k;
    break;

   case 3:
    drum_bench_B.MultiportSwitch_a = drum_bench_B.Output_e;
    break;

   default:
    drum_bench_B.MultiportSwitch_a = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S57>/Multiport Switch' */

  /* Gain: '<S57>/Gain' */
  drum_bench_B.Gain_i = drum_bench_P.config.input.input1.motor_vq.amplitude *
    0.0;

  /* Clock: '<S63>/Clock1' */
  drum_bench_B.Clock1_d = drum_bench_M->Timing.t[0];

  /* Product: '<S63>/Product' incorporates:
   *  Constant: '<S63>/deltaFreq'
   *  Constant: '<S63>/targetTime'
   */
  acc = drum_bench_P.config.input.input1.quadrature_1_angle.frequency - 0.1;
  output = 6.2831853071795862 * acc;
  drum_bench_B.Product_kg = output / 100.0;

  /* Gain: '<S63>/Gain' */
  drum_bench_B.Gain_c = 0.5 * drum_bench_B.Product_kg;

  /* Product: '<S63>/Product1' */
  drum_bench_B.Product1_jw = drum_bench_B.Clock1_d * drum_bench_B.Gain_c;

  /* Sum: '<S63>/Sum' incorporates:
   *  Constant: '<S63>/initialFreq'
   */
  drum_bench_B.Sum_en = drum_bench_B.Product1_jw + 0.62831853071795862;

  /* Product: '<S63>/Product2' */
  drum_bench_B.Product2_gz = drum_bench_B.Clock1_d * drum_bench_B.Sum_en;

  /* Trigonometry: '<S63>/Output' */
  drum_bench_B.Output_m = sin(drum_bench_B.Product2_gz);

  /* Sin: '<S58>/Sine Wave' */
  drum_bench_B.SineWave_m = sin
    (drum_bench_P.config.input.input1.quadrature_1_angle.frequency *
     drum_bench_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S58>/Sign' */
  acc = drum_bench_B.SineWave_m;
  if (acc < 0.0) {
    drum_bench_B.Sign_l = -1.0;
  } else if (acc > 0.0) {
    drum_bench_B.Sign_l = 1.0;
  } else if (acc == 0.0) {
    drum_bench_B.Sign_l = 0.0;
  } else {
    drum_bench_B.Sign_l = acc;
  }

  /* End of Signum: '<S58>/Sign' */

  /* MultiPortSwitch: '<S58>/Multiport Switch' incorporates:
   *  Constant: '<S58>/Constant1'
   *  Constant: '<S58>/select input'
   */
  switch ((int32_T)
          drum_bench_P.config.input.input1.quadrature_1_angle.select_input) {
   case 0:
    drum_bench_B.MultiportSwitch_i = 0.0;
    break;

   case 1:
    drum_bench_B.MultiportSwitch_i = drum_bench_B.SineWave_m;
    break;

   case 2:
    drum_bench_B.MultiportSwitch_i = drum_bench_B.Sign_l;
    break;

   case 3:
    drum_bench_B.MultiportSwitch_i = drum_bench_B.Output_m;
    break;

   default:
    drum_bench_B.MultiportSwitch_i = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S58>/Multiport Switch' */

  /* Gain: '<S58>/Gain' */
  drum_bench_B.Gain_m =
    drum_bench_P.config.input.input1.quadrature_1_angle.amplitude * 0.0;

  /* Clock: '<S64>/Clock1' */
  drum_bench_B.Clock1_h = drum_bench_M->Timing.t[0];

  /* Product: '<S64>/Product' incorporates:
   *  Constant: '<S64>/deltaFreq'
   *  Constant: '<S64>/targetTime'
   */
  acc = drum_bench_P.config.input.input1.quadrature_2_angle.frequency - 0.1;
  output = 6.2831853071795862 * acc;
  drum_bench_B.Product_if = output / 100.0;

  /* Gain: '<S64>/Gain' */
  drum_bench_B.Gain_p = 0.5 * drum_bench_B.Product_if;

  /* Product: '<S64>/Product1' */
  drum_bench_B.Product1_j2 = drum_bench_B.Clock1_h * drum_bench_B.Gain_p;

  /* Sum: '<S64>/Sum' incorporates:
   *  Constant: '<S64>/initialFreq'
   */
  drum_bench_B.Sum_ke = drum_bench_B.Product1_j2 + 0.62831853071795862;

  /* Product: '<S64>/Product2' */
  drum_bench_B.Product2_gp = drum_bench_B.Clock1_h * drum_bench_B.Sum_ke;

  /* Trigonometry: '<S64>/Output' */
  drum_bench_B.Output_l = sin(drum_bench_B.Product2_gp);

  /* Sin: '<S59>/Sine Wave' */
  drum_bench_B.SineWave_ld = sin
    (drum_bench_P.config.input.input1.quadrature_2_angle.frequency *
     drum_bench_M->Timing.t[0] + 0.0) * 1.0 + 0.0;

  /* Signum: '<S59>/Sign' */
  acc = drum_bench_B.SineWave_ld;
  if (acc < 0.0) {
    drum_bench_B.Sign_ip = -1.0;
  } else if (acc > 0.0) {
    drum_bench_B.Sign_ip = 1.0;
  } else if (acc == 0.0) {
    drum_bench_B.Sign_ip = 0.0;
  } else {
    drum_bench_B.Sign_ip = acc;
  }

  /* End of Signum: '<S59>/Sign' */

  /* MultiPortSwitch: '<S59>/Multiport Switch' incorporates:
   *  Constant: '<S59>/Constant1'
   *  Constant: '<S59>/select input'
   */
  switch ((int32_T)
          drum_bench_P.config.input.input1.quadrature_2_angle.select_input) {
   case 0:
    drum_bench_B.MultiportSwitch_k = 0.0;
    break;

   case 1:
    drum_bench_B.MultiportSwitch_k = drum_bench_B.SineWave_ld;
    break;

   case 2:
    drum_bench_B.MultiportSwitch_k = drum_bench_B.Sign_ip;
    break;

   case 3:
    drum_bench_B.MultiportSwitch_k = drum_bench_B.Output_l;
    break;

   default:
    drum_bench_B.MultiportSwitch_k = 0.0;
    break;
  }

  /* End of MultiPortSwitch: '<S59>/Multiport Switch' */

  /* Gain: '<S59>/Gain' */
  drum_bench_B.Gain_nh =
    drum_bench_P.config.input.input1.quadrature_2_angle.amplitude * 0.0;

  /* BusSelector: '<S8>/Bus Selector' */
  drum_bench_B.rpc = drum_bench_B.modeswitch.rpc;
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* UnitDelay: '<S96>/Output' */
    drum_bench_B.Output_o = drum_bench_DW.Output_DSTATE;

    /* Sum: '<S97>/FixPt Sum1' incorporates:
     *  Constant: '<S97>/FixPt Constant'
     */
    drum_bench_B.FixPtSum1 = drum_bench_B.Output_o + 1U;

    /* Switch: '<S98>/FixPt Switch' incorporates:
     *  Constant: '<S98>/Constant'
     */
    if (drum_bench_B.FixPtSum1 > 4294967295U) {
      drum_bench_B.FixPtSwitch = 0U;
    } else {
      drum_bench_B.FixPtSwitch = drum_bench_B.FixPtSum1;
    }

    /* End of Switch: '<S98>/FixPt Switch' */

    /* DataTypeConversion: '<S95>/debug_' */
    drum_bench_B.debug = drum_bench_B.marlin_ec_o21;
  }

  /* BusSelector: '<S92>/Bus Selector' */
  drum_bench_B.mode_k = drum_bench_B.modeswitch.mode;

  /* BusSelector: '<S92>/Bus Selector1' */
  drum_bench_B.motor_iq_kg = drum_bench_B.modeswitch.motor_iq;
  drum_bench_B.motor_id = drum_bench_B.modeswitch.motor_id;

  /* BusSelector: '<S92>/Bus Selector2' */
  drum_bench_B.motor_vq = drum_bench_B.modeswitch.motor_vq;
  drum_bench_B.motor_vd = drum_bench_B.modeswitch.motor_vd;

  /* BusSelector: '<S92>/Bus Selector3' */
  drum_bench_B.analog_diff_g1 = drum_bench_B.modeswitch.analog_diff;
  drum_bench_B.analog_diff_dot_o = drum_bench_B.modeswitch.analog_diff_dot;

  /* BusSelector: '<S92>/Bus Selector4' */
  drum_bench_B.quadrature_1_angle_b = drum_bench_B.modeswitch.quadrature_1_angle;
  drum_bench_B.quadrature_1_angle_dot_j =
    drum_bench_B.modeswitch.quadrature_1_angle_dot;

  /* BusSelector: '<S92>/Bus Selector5' */
  drum_bench_B.motor_torque_mv = drum_bench_B.modeswitch.motor_torque;

  /* MultiPortSwitch: '<S92>/Multiport Switch' incorporates:
   *  Constant: '<S100>/mode'
   *  Constant: '<S101>/mode'
   *  Constant: '<S102>/mode'
   *  Constant: '<S103>/mode'
   *  Constant: '<S104>/mode'
   *  Constant: '<S105>/mode'
   *  Constant: '<S106>/mode'
   *  Constant: '<S99>/mode'
   */
  switch ((int32_T)drum_bench_B.mode_k) {
   case 0:
    drum_bench_B.mode_a = 0U;
    break;

   case 1:
    drum_bench_B.mode_a = 1U;
    break;

   case 2:
    drum_bench_B.mode_a = 2U;
    break;

   case 3:
    drum_bench_B.mode_a = 3U;
    break;

   case 4:
    drum_bench_B.mode_a = 4U;
    break;

   case 5:
    drum_bench_B.mode_a = 5U;
    break;

   case 6:
    drum_bench_B.mode_a = 6U;
    break;

   case 7:
    drum_bench_B.mode_a = 8U;
    break;

   default:
    drum_bench_B.mode_a = 0U;
    break;
  }

  /* End of MultiPortSwitch: '<S92>/Multiport Switch' */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    for (i = 0; i < 14; i++) {
      /* DataTypeConversion: '<S103>/Data Type Conversion1' incorporates:
       *  Constant: '<S103>/filler'
       */
      drum_bench_B.DataTypeConversion1_f[i] = (real32_T)
        drum_bench_ConstP.pooled29[i];

      /* DataTypeConversion: '<S100>/Data Type Conversion1' incorporates:
       *  Constant: '<S100>/filler'
       */
      drum_bench_B.DataTypeConversion1_o[i] = (real32_T)
        drum_bench_ConstP.pooled29[i];
    }

    /* Pack: <S103>/Byte Pack3 */
    (void) memcpy(&drum_bench_B.command[0], &drum_bench_B.DataTypeConversion1_f
                  [0],
                  56);

    /* Pack: <S100>/Byte Pack3 */
    (void) memcpy(&drum_bench_B.command_j[0],
                  &drum_bench_B.DataTypeConversion1_o[0],
                  56);

    /* DataTypeConversion: '<S104>/Data Type Conversion1' incorporates:
     *  Constant: '<S104>/filler'
     *  Constant: '<S92>/lock_current'
     */
    drum_bench_B.DataTypeConversion1_oj[0] = (real32_T)
      drum_bench_P.config.marlins.M0.motor.phase_lock_current;
    for (i = 0; i < 13; i++) {
      drum_bench_B.DataTypeConversion1_oj[i + 1] = (real32_T)
        drum_bench_ConstP.pooled30[i];
    }

    /* End of DataTypeConversion: '<S104>/Data Type Conversion1' */

    /* Pack: <S104>/Byte Pack3 */
    (void) memcpy(&drum_bench_B.command_k[0],
                  &drum_bench_B.DataTypeConversion1_oj[0],
                  56);
  }

  /* DataTypeConversion: '<S101>/Data Type Conversion1' incorporates:
   *  Constant: '<S101>/filler'
   */
  drum_bench_B.DataTypeConversion1_b[0] = (real32_T)drum_bench_B.motor_iq_kg;
  drum_bench_B.DataTypeConversion1_b[1] = (real32_T)drum_bench_B.motor_id;
  for (i = 0; i < 12; i++) {
    drum_bench_B.DataTypeConversion1_b[i + 2] = (real32_T)
      drum_bench_ConstP.pooled31[i];
  }

  /* End of DataTypeConversion: '<S101>/Data Type Conversion1' */

  /* Pack: <S101>/Byte Pack2 */
  (void) memcpy(&drum_bench_B.command_l[0], &drum_bench_B.DataTypeConversion1_b
                [0],
                56);

  /* DataTypeConversion: '<S106>/Data Type Conversion1' incorporates:
   *  Constant: '<S106>/filler'
   */
  drum_bench_B.DataTypeConversion1_e[0] = (real32_T)drum_bench_B.motor_vq;
  drum_bench_B.DataTypeConversion1_e[1] = (real32_T)drum_bench_B.motor_vd;
  for (i = 0; i < 12; i++) {
    drum_bench_B.DataTypeConversion1_e[i + 2] = (real32_T)
      drum_bench_ConstP.pooled31[i];
  }

  /* End of DataTypeConversion: '<S106>/Data Type Conversion1' */

  /* Pack: <S106>/Byte Pack2 */
  (void) memcpy(&drum_bench_B.command_f[0], &drum_bench_B.DataTypeConversion1_e
                [0],
                56);

  /* DataTypeConversion: '<S99>/Data Type Conversion1' incorporates:
   *  Constant: '<S99>/filler'
   */
  drum_bench_B.DataTypeConversion1_fh[0] = (real32_T)drum_bench_B.analog_diff_g1;
  drum_bench_B.DataTypeConversion1_fh[1] = (real32_T)
    drum_bench_B.analog_diff_dot_o;
  for (i = 0; i < 12; i++) {
    drum_bench_B.DataTypeConversion1_fh[i + 2] = (real32_T)
      drum_bench_ConstP.pooled31[i];
  }

  /* End of DataTypeConversion: '<S99>/Data Type Conversion1' */

  /* Pack: <S99>/Byte Pack2 */
  (void) memcpy(&drum_bench_B.command_g[0],
                &drum_bench_B.DataTypeConversion1_fh[0],
                56);

  /* DataTypeConversion: '<S105>/Data Type Conversion1' incorporates:
   *  Constant: '<S105>/filler'
   */
  drum_bench_B.DataTypeConversion1_fm[0] = (real32_T)
    drum_bench_B.quadrature_1_angle_b;
  drum_bench_B.DataTypeConversion1_fm[1] = (real32_T)
    drum_bench_B.quadrature_1_angle_dot_j;
  for (i = 0; i < 12; i++) {
    drum_bench_B.DataTypeConversion1_fm[i + 2] = (real32_T)
      drum_bench_ConstP.pooled31[i];
  }

  /* End of DataTypeConversion: '<S105>/Data Type Conversion1' */

  /* Pack: <S105>/Byte Pack2 */
  (void) memcpy(&drum_bench_B.command_gi[0],
                &drum_bench_B.DataTypeConversion1_fm[0],
                56);

  /* DataTypeConversion: '<S102>/Data Type Conversion1' incorporates:
   *  Constant: '<S102>/filler'
   */
  drum_bench_B.DataTypeConversion1_h[0] = (real32_T)drum_bench_B.motor_torque_mv;
  for (i = 0; i < 13; i++) {
    drum_bench_B.DataTypeConversion1_h[i + 1] = (real32_T)
      drum_bench_ConstP.pooled30[i];
  }

  /* End of DataTypeConversion: '<S102>/Data Type Conversion1' */

  /* Pack: <S102>/Byte Pack2 */
  (void) memcpy(&drum_bench_B.command_a[0], &drum_bench_B.DataTypeConversion1_h
                [0],
                56);

  /* MultiPortSwitch: '<S92>/Multiport Switch' */
  switch ((int32_T)drum_bench_B.mode_k) {
   case 0:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_e[i] = drum_bench_B.command[i];
    }
    break;

   case 1:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_e[i] = drum_bench_B.command_j[i];
    }
    break;

   case 2:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_e[i] = drum_bench_B.command_k[i];
    }
    break;

   case 3:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_e[i] = drum_bench_B.command_l[i];
    }
    break;

   case 4:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_e[i] = drum_bench_B.command_f[i];
    }
    break;

   case 5:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_e[i] = drum_bench_B.command_g[i];
    }
    break;

   case 6:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_e[i] = drum_bench_B.command_gi[i];
    }
    break;

   case 7:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_e[i] = drum_bench_B.command_a[i];
    }
    break;

   default:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_e[i] = drum_bench_B.command[i];
    }
    break;
  }

  /* End of MultiPortSwitch: '<S92>/Multiport Switch' */

  /* BusCreator: '<S107>/Bus Creator' incorporates:
   *  Constant: '<S107>/id'
   *  Constant: '<S107>/type'
   */
  drum_bench_B.BusCreator_nf.type = 6;
  drum_bench_B.BusCreator_nf.id = 0.0F;

  /* BusCreator: '<S108>/Bus Creator' incorporates:
   *  Constant: '<S108>/id'
   *  Constant: '<S108>/type'
   */
  drum_bench_B.BusCreator_k.type = 7;
  drum_bench_B.BusCreator_k.id = 1.0F;

  /* BusCreator: '<S109>/Bus Creator' incorporates:
   *  Constant: '<S109>/id'
   *  Constant: '<S109>/type'
   */
  drum_bench_B.BusCreator_iq.type = 7;
  drum_bench_B.BusCreator_iq.id = 0.0F;

  /* BusCreator: '<S110>/Bus Creator' incorporates:
   *  Constant: '<S110>/id'
   *  Constant: '<S110>/type'
   */
  drum_bench_B.BusCreator_o.type = 7;
  drum_bench_B.BusCreator_o.id = 0.0F;

  /* BusCreator: '<S111>/Bus Creator' incorporates:
   *  Constant: '<S111>/id'
   *  Constant: '<S111>/type'
   */
  drum_bench_B.BusCreator_l.type = 7;
  drum_bench_B.BusCreator_l.id = 0.0F;
  for (i = 0; i < 20; i++) {
    /* BusCreator: '<S107>/Bus Creator' */
    drum_bench_B.BusCreator_nf.data[i] = drum_bench_ConstB.data[i];

    /* BusCreator: '<S108>/Bus Creator' */
    drum_bench_B.BusCreator_k.data[i] = drum_bench_ConstB.data_p[i];

    /* BusCreator: '<S109>/Bus Creator' */
    drum_bench_B.BusCreator_iq.data[i] = drum_bench_ConstB.data_f[i];

    /* BusCreator: '<S110>/Bus Creator' */
    drum_bench_B.BusCreator_o.data[i] = drum_bench_ConstB.data_c[i];
    drum_bench_B.BusCreator_l.data[i] = drum_bench_ConstB.data_h[i];
  }

  /* End of BusCreator: '<S111>/Bus Creator' */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* Constant: '<S130>/id' */
    drum_bench_B.VectorConcatenate_d[0] =
      drum_bench_P.config.marlins.M0.amp.current_zero_a;

    /* Constant: '<S130>/id1' */
    drum_bench_B.VectorConcatenate_d[1] =
      drum_bench_P.config.marlins.M0.amp.current_zero_b;

    /* Constant: '<S130>/id2' */
    drum_bench_B.VectorConcatenate_d[2] =
      drum_bench_P.config.marlins.M0.amp.deadband;

    /* Constant: '<S130>/id3' */
    drum_bench_B.VectorConcatenate_d[3] =
      drum_bench_P.config.marlins.M0.amp.led_brightness;

    /* Constant: '<S130>/id4' */
    drum_bench_B.VectorConcatenate_d[4] =
      drum_bench_P.config.marlins.M0.amp.nominal_bus_voltage;

    /* Constant: '<S130>/id5' */
    drum_bench_B.VectorConcatenate_d[5] =
      drum_bench_P.config.marlins.M0.amp.current_sensor_limit;

    /* Constant: '<S144>/id' */
    drum_bench_B.VectorConcatenate_d[6] =
      drum_bench_P.config.marlins.M0.amp.logger_10kHz.trigger_level;

    /* Constant: '<S143>/id' */
    drum_bench_B.VectorConcatenate_d[7] =
      drum_bench_P.config.marlins.M0.amp.logger_100kHz.trigger_level;

    /* Constant: '<S136>/id' */
    drum_bench_B.VectorConcatenate_d[8] =
      drum_bench_P.config.marlins.M0.control_current.current_filter_bandwidth;

    /* Constant: '<S136>/id1' */
    drum_bench_B.VectorConcatenate_d[9] =
      drum_bench_P.config.marlins.M0.control_current.fingerprint_gain;

    /* Constant: '<S136>/id2' */
    drum_bench_B.VectorConcatenate_d[10] =
      drum_bench_P.config.marlins.M0.control_current.ki;

    /* Constant: '<S136>/id3' */
    drum_bench_B.VectorConcatenate_d[11] =
      drum_bench_P.config.marlins.M0.control_current.ki_limit;

    /* Constant: '<S136>/id4' */
    drum_bench_B.VectorConcatenate_d[12] =
      drum_bench_P.config.marlins.M0.control_current.kp;

    /* Constant: '<S150>/id' */
    drum_bench_B.VectorConcatenate_d[13] =
      drum_bench_P.config.marlins.M0.control_motor_angle.pid_deadband.deadband;

    /* Constant: '<S151>/id' */
    drum_bench_B.VectorConcatenate_d[14] =
      drum_bench_P.config.marlins.M0.control_motor_angle.pid_interpolate.enabled;

    /* Constant: '<S149>/id' */
    drum_bench_B.VectorConcatenate_d[15] =
      drum_bench_P.config.marlins.M0.control_motor_angle.pid.kd;

    /* Constant: '<S149>/id1' */
    drum_bench_B.VectorConcatenate_d[16] =
      drum_bench_P.config.marlins.M0.control_motor_angle.pid.ki;

    /* Constant: '<S149>/id2' */
    drum_bench_B.VectorConcatenate_d[17] =
      drum_bench_P.config.marlins.M0.control_motor_angle.pid.ki_limit;

    /* Constant: '<S149>/id3' */
    drum_bench_B.VectorConcatenate_d[18] =
      drum_bench_P.config.marlins.M0.control_motor_angle.pid.kp;

    /* Constant: '<S152>/id' */
    drum_bench_B.VectorConcatenate_d[19] =
      drum_bench_P.config.marlins.M0.control_quadrature_2_angle.pid.kd;

    /* Constant: '<S152>/id1' */
    drum_bench_B.VectorConcatenate_d[20] =
      drum_bench_P.config.marlins.M0.control_quadrature_2_angle.pid.ki;

    /* Constant: '<S152>/id2' */
    drum_bench_B.VectorConcatenate_d[21] =
      drum_bench_P.config.marlins.M0.control_quadrature_2_angle.pid.ki_limit;

    /* Constant: '<S152>/id3' */
    drum_bench_B.VectorConcatenate_d[22] =
      drum_bench_P.config.marlins.M0.control_quadrature_2_angle.pid.kp;

    /* Constant: '<S148>/id' */
    drum_bench_B.VectorConcatenate_d[23] =
      drum_bench_P.config.marlins.M0.control_analog.pid.kd;

    /* Constant: '<S148>/id1' */
    drum_bench_B.VectorConcatenate_d[24] =
      drum_bench_P.config.marlins.M0.control_analog.pid.ki;

    /* Constant: '<S148>/id2' */
    drum_bench_B.VectorConcatenate_d[25] =
      drum_bench_P.config.marlins.M0.control_analog.pid.ki_limit;

    /* Constant: '<S148>/id3' */
    drum_bench_B.VectorConcatenate_d[26] =
      drum_bench_P.config.marlins.M0.control_analog.pid.kp;

    /* Constant: '<S135>/id' */
    drum_bench_B.VectorConcatenate_d[27] =
      drum_bench_P.config.marlins.M0.control_analog.source;

    /* Constant: '<S139>/id' */
    drum_bench_B.VectorConcatenate_d[28] =
      drum_bench_P.config.marlins.M0.motor.current_limit;

    /* Constant: '<S139>/id1' */
    drum_bench_B.VectorConcatenate_d[29] =
      drum_bench_P.config.marlins.M0.motor.voltage_limit;

    /* Constant: '<S139>/id2' */
    drum_bench_B.VectorConcatenate_d[30] =
      drum_bench_P.config.marlins.M0.motor.encoder.counts_per_revolution;

    /* Constant: '<S139>/id3' */
    drum_bench_B.VectorConcatenate_d[31] =
      drum_bench_P.config.marlins.M0.motor.km;

    /* Constant: '<S139>/id4' */
    drum_bench_B.VectorConcatenate_d[32] =
      drum_bench_P.config.marlins.M0.motor.num_poles;

    /* Constant: '<S139>/id5' */
    drum_bench_B.VectorConcatenate_d[33] =
      drum_bench_P.config.marlins.M0.motor.resistance;

    /* Constant: '<S139>/id6' */
    drum_bench_B.VectorConcatenate_d[34] =
      drum_bench_P.config.marlins.M0.motor.electrical_offset;

    /* Constant: '<S139>/id7' */
    drum_bench_B.VectorConcatenate_d[35] =
      drum_bench_P.config.marlins.M0.motor.v_per_a;

    /* Constant: '<S134>/id' */
    drum_bench_B.VectorConcatenate_d[36] =
      drum_bench_P.config.marlins.M0.bridge.thermal_model.eA1;

    /* Constant: '<S134>/id1' */
    drum_bench_B.VectorConcatenate_d[37] =
      drum_bench_P.config.marlins.M0.bridge.thermal_model.eA2;

    /* Constant: '<S153>/id' */
    drum_bench_B.VectorConcatenate_d[38] =
      drum_bench_P.config.marlins.M0.quadrature_1.sensor.bias;

    /* Constant: '<S153>/id1' */
    drum_bench_B.VectorConcatenate_d[39] =
      drum_bench_P.config.marlins.M0.quadrature_1.sensor.gain;

    /* Constant: '<S153>/id6' */
    drum_bench_B.VectorConcatenate_d[40] =
      drum_bench_P.config.marlins.M0.quadrature_1.sensor.limit_high;

    /* Constant: '<S153>/id3' */
    drum_bench_B.VectorConcatenate_d[41] =
      drum_bench_P.config.marlins.M0.quadrature_1.sensor.limit_low;

    /* Constant: '<S153>/id4' */
    drum_bench_B.VectorConcatenate_d[42] =
      drum_bench_P.config.marlins.M0.quadrature_1.sensor.filter_bandwidth;

    /* Constant: '<S153>/id5' */
    drum_bench_B.VectorConcatenate_d[43] =
      drum_bench_P.config.marlins.M0.quadrature_1.sensor.dot_filter_bandwidth;

    /* Constant: '<S140>/id' */
    drum_bench_B.VectorConcatenate_d[44] =
      drum_bench_P.config.marlins.M0.quadrature_1.enable_index;

    /* Constant: '<S154>/id' */
    drum_bench_B.VectorConcatenate_d[45] =
      drum_bench_P.config.marlins.M0.quadrature_2.sensor.bias;

    /* Constant: '<S154>/id1' */
    drum_bench_B.VectorConcatenate_d[46] =
      drum_bench_P.config.marlins.M0.quadrature_2.sensor.gain;

    /* Constant: '<S154>/id6' */
    drum_bench_B.VectorConcatenate_d[47] =
      drum_bench_P.config.marlins.M0.quadrature_2.sensor.limit_high;

    /* Constant: '<S154>/id3' */
    drum_bench_B.VectorConcatenate_d[48] =
      drum_bench_P.config.marlins.M0.quadrature_2.sensor.limit_low;

    /* Constant: '<S154>/id4' */
    drum_bench_B.VectorConcatenate_d[49] =
      drum_bench_P.config.marlins.M0.quadrature_2.sensor.filter_bandwidth;

    /* Constant: '<S154>/id5' */
    drum_bench_B.VectorConcatenate_d[50] =
      drum_bench_P.config.marlins.M0.quadrature_2.sensor.dot_filter_bandwidth;

    /* Constant: '<S141>/id' */
    drum_bench_B.VectorConcatenate_d[51] =
      drum_bench_P.config.marlins.M0.quadrature_2.enable_index;

    /* Constant: '<S145>/id' */
    drum_bench_B.VectorConcatenate_d[52] =
      drum_bench_P.config.marlins.M0.analog_1.sensor.bias;

    /* Constant: '<S145>/id1' */
    drum_bench_B.VectorConcatenate_d[53] =
      drum_bench_P.config.marlins.M0.analog_1.sensor.gain;

    /* Constant: '<S145>/id6' */
    drum_bench_B.VectorConcatenate_d[54] =
      drum_bench_P.config.marlins.M0.analog_1.sensor.limit_high;

    /* Constant: '<S145>/id3' */
    drum_bench_B.VectorConcatenate_d[55] =
      drum_bench_P.config.marlins.M0.analog_1.sensor.limit_low;

    /* Constant: '<S145>/id4' */
    drum_bench_B.VectorConcatenate_d[56] =
      drum_bench_P.config.marlins.M0.analog_1.sensor.filter_bandwidth;

    /* Constant: '<S145>/id5' */
    drum_bench_B.VectorConcatenate_d[57] =
      drum_bench_P.config.marlins.M0.analog_1.sensor.dot_filter_bandwidth;

    /* Constant: '<S146>/id' */
    drum_bench_B.VectorConcatenate_d[58] =
      drum_bench_P.config.marlins.M0.analog_2.sensor.bias;

    /* Constant: '<S146>/id1' */
    drum_bench_B.VectorConcatenate_d[59] =
      drum_bench_P.config.marlins.M0.analog_2.sensor.gain;

    /* Constant: '<S146>/id6' */
    drum_bench_B.VectorConcatenate_d[60] =
      drum_bench_P.config.marlins.M0.analog_2.sensor.limit_high;

    /* Constant: '<S146>/id3' */
    drum_bench_B.VectorConcatenate_d[61] =
      drum_bench_P.config.marlins.M0.analog_2.sensor.limit_low;

    /* Constant: '<S146>/id4' */
    drum_bench_B.VectorConcatenate_d[62] =
      drum_bench_P.config.marlins.M0.analog_2.sensor.filter_bandwidth;

    /* Constant: '<S146>/id5' */
    drum_bench_B.VectorConcatenate_d[63] =
      drum_bench_P.config.marlins.M0.analog_2.sensor.dot_filter_bandwidth;

    /* Constant: '<S147>/id' */
    drum_bench_B.VectorConcatenate_d[64] =
      drum_bench_P.config.marlins.M0.analog_diff.sensor.bias;

    /* Constant: '<S147>/id1' */
    drum_bench_B.VectorConcatenate_d[65] =
      drum_bench_P.config.marlins.M0.analog_diff.sensor.gain;

    /* Constant: '<S147>/id6' */
    drum_bench_B.VectorConcatenate_d[66] =
      drum_bench_P.config.marlins.M0.analog_diff.sensor.limit_high;

    /* Constant: '<S147>/id3' */
    drum_bench_B.VectorConcatenate_d[67] =
      drum_bench_P.config.marlins.M0.analog_diff.sensor.limit_low;

    /* Constant: '<S147>/id4' */
    drum_bench_B.VectorConcatenate_d[68] =
      drum_bench_P.config.marlins.M0.analog_diff.sensor.filter_bandwidth;

    /* Constant: '<S147>/id5' */
    drum_bench_B.VectorConcatenate_d[69] =
      drum_bench_P.config.marlins.M0.analog_diff.sensor.dot_filter_bandwidth;

    /* Constant: '<S155>/id' */
    drum_bench_B.VectorConcatenate_d[70] =
      drum_bench_P.config.marlins.M0.ssi.sensor.bias;

    /* Constant: '<S155>/id1' */
    drum_bench_B.VectorConcatenate_d[71] =
      drum_bench_P.config.marlins.M0.ssi.sensor.gain;

    /* Constant: '<S155>/id6' */
    drum_bench_B.VectorConcatenate_d[72] =
      drum_bench_P.config.marlins.M0.ssi.sensor.limit_high;

    /* Constant: '<S155>/id3' */
    drum_bench_B.VectorConcatenate_d[73] =
      drum_bench_P.config.marlins.M0.ssi.sensor.limit_low;

    /* Constant: '<S155>/id4' */
    drum_bench_B.VectorConcatenate_d[74] =
      drum_bench_P.config.marlins.M0.ssi.sensor.filter_bandwidth;

    /* Constant: '<S155>/id5' */
    drum_bench_B.VectorConcatenate_d[75] =
      drum_bench_P.config.marlins.M0.ssi.sensor.dot_filter_bandwidth;

    /* DataTypeConversion: '<S128>/Data Type Conversion' */
    for (i = 0; i < 76; i++) {
      drum_bench_B.DataTypeConversion_f[i] = (real32_T)
        drum_bench_B.VectorConcatenate_d[i];
    }

    /* End of DataTypeConversion: '<S128>/Data Type Conversion' */

    /* Unpack: <S94>/Byte Unpack */
    (void) memcpy(&drum_bench_B.type, &drum_bench_B.rpa_packet[0],
                  4);
    (void) memcpy(&drum_bench_B.id, &drum_bench_B.rpa_packet[4],
                  4);
    (void) memcpy(&drum_bench_B.data[0], &drum_bench_B.rpa_packet[8],
                  20);

    /* BusCreator: '<S127>/BusConversion_InsertedFor_MATLAB Function1_at_inport_1' */
    drum_bench_B.BusConversion_InsertedFor_MATLA.type = drum_bench_B.type;
    drum_bench_B.BusConversion_InsertedFor_MATLA.id = drum_bench_B.id;
    for (i = 0; i < 20; i++) {
      drum_bench_B.BusConversion_InsertedFor_MATLA.data[i] = drum_bench_B.data[i];
    }

    /* End of BusCreator: '<S127>/BusConversion_InsertedFor_MATLAB Function1_at_inport_1' */

    /* MATLAB Function: '<S127>/MATLAB Function1' incorporates:
     *  Constant: '<S127>/Constant2'
     */
    drum_bench_MATLABFunction1(drum_bench_B.DataTypeConversion_f,
      &drum_bench_B.BusConversion_InsertedFor_MATLA, 1U,
      &drum_bench_B.sf_MATLABFunction1, &drum_bench_DW.sf_MATLABFunction1);

    /* Pack: <S127>/Byte Pack */
    (void) memcpy(&drum_bench_B.data_m[0], &drum_bench_ConstP.pooled43,
                  4);
    (void) memcpy(&drum_bench_B.data_m[4],
                  &drum_bench_B.sf_MATLABFunction1.index,
                  4);
    (void) memcpy(&drum_bench_B.data_m[8],
                  &drum_bench_B.sf_MATLABFunction1.value,
                  4);
    (void) memcpy(&drum_bench_B.data_m[12], drum_bench_ConstP.pooled52,
                  8);

    /* BusCreator: '<S127>/Bus Creator' */
    drum_bench_B.BusCreator_a.type = drum_bench_B.sf_MATLABFunction1.type;
    drum_bench_B.BusCreator_a.id = drum_bench_B.sf_MATLABFunction1.id;
    for (i = 0; i < 20; i++) {
      drum_bench_B.BusCreator_a.data[i] = drum_bench_B.data_m[i];
    }

    /* End of BusCreator: '<S127>/Bus Creator' */
  }

  /* BusCreator: '<S119>/Bus Creator' incorporates:
   *  Constant: '<S119>/id'
   *  Constant: '<S119>/type'
   */
  drum_bench_B.BusCreator_h.type = 1;
  drum_bench_B.BusCreator_h.id = 0.0F;

  /* BusCreator: '<S112>/Bus Creator' incorporates:
   *  Constant: '<S112>/id'
   *  Constant: '<S112>/type'
   */
  drum_bench_B.BusCreator_d.type = 2;
  drum_bench_B.BusCreator_d.id = 0.0F;

  /* BusCreator: '<S122>/Bus Creator' incorporates:
   *  Constant: '<S122>/id'
   *  Constant: '<S122>/type'
   */
  drum_bench_B.BusCreator_b.type = 3;
  drum_bench_B.BusCreator_b.id = 0.0F;

  /* BusCreator: '<S121>/Bus Creator' incorporates:
   *  Constant: '<S121>/id'
   *  Constant: '<S121>/type'
   */
  drum_bench_B.BusCreator_bc.type = 3;
  drum_bench_B.BusCreator_bc.id = 0.0F;

  /* BusCreator: '<S113>/Bus Creator' incorporates:
   *  Constant: '<S113>/id'
   *  Constant: '<S113>/type'
   */
  drum_bench_B.BusCreator_ld.type = 3;
  drum_bench_B.BusCreator_ld.id = 1.0F;
  for (i = 0; i < 20; i++) {
    /* BusCreator: '<S119>/Bus Creator' */
    drum_bench_B.BusCreator_h.data[i] = drum_bench_ConstB.data_e[i];

    /* BusCreator: '<S112>/Bus Creator' */
    drum_bench_B.BusCreator_d.data[i] = drum_bench_ConstB.data_d[i];

    /* BusCreator: '<S122>/Bus Creator' */
    drum_bench_B.BusCreator_b.data[i] = drum_bench_ConstB.data_cu[i];

    /* BusCreator: '<S121>/Bus Creator' */
    drum_bench_B.BusCreator_bc.data[i] = drum_bench_ConstB.data_o[i];
    drum_bench_B.BusCreator_ld.data[i] = drum_bench_ConstB.data_cz[i];
  }

  /* End of BusCreator: '<S113>/Bus Creator' */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* DataTypeConversion: '<S114>/Data Type Conversion' incorporates:
     *  Constant: '<S114>/value'
     *  Constant: '<S114>/value1'
     */
    acc = floor(1.0);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_g[0] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;
    acc = floor(drum_bench_P.config.marlins.M0.debug_address_index);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_g[1] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;

    /* End of DataTypeConversion: '<S114>/Data Type Conversion' */

    /* Pack: <S114>/Byte Pack */
    (void) memcpy(&drum_bench_B.data_e[0], &drum_bench_B.DataTypeConversion_g[0],
                  8);
    (void) memcpy(&drum_bench_B.data_e[8], drum_bench_ConstP.pooled53,
                  12);

    /* BusCreator: '<S114>/Bus Creator' incorporates:
     *  Constant: '<S114>/id'
     *  Constant: '<S114>/type'
     */
    drum_bench_B.BusCreator_c.type = 3;
    drum_bench_B.BusCreator_c.id = 1.1F;

    /* DataTypeConversion: '<S115>/Data Type Conversion' incorporates:
     *  Constant: '<S115>/value'
     *  Constant: '<S115>/value1'
     */
    acc = floor(1.0);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_h[0] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;
    acc = floor(drum_bench_P.config.marlins.M0.debug_address_index);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_h[1] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;

    /* End of DataTypeConversion: '<S115>/Data Type Conversion' */

    /* Pack: <S115>/Byte Pack */
    (void) memcpy(&drum_bench_B.data_b[0], &drum_bench_B.DataTypeConversion_h[0],
                  8);
    (void) memcpy(&drum_bench_B.data_b[8], drum_bench_ConstP.pooled53,
                  12);

    /* BusCreator: '<S115>/Bus Creator' incorporates:
     *  Constant: '<S115>/id'
     *  Constant: '<S115>/type'
     */
    drum_bench_B.BusCreator_hl.type = 3;
    drum_bench_B.BusCreator_hl.id = 1.1F;

    /* DataTypeConversion: '<S116>/Data Type Conversion' incorporates:
     *  Constant: '<S116>/value'
     *  Constant: '<S116>/value1'
     */
    acc = floor(1.0);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_ee[0] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;
    acc = floor(drum_bench_P.config.marlins.M0.debug_address_index);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_ee[1] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;

    /* End of DataTypeConversion: '<S116>/Data Type Conversion' */

    /* Pack: <S116>/Byte Pack */
    (void) memcpy(&drum_bench_B.data_n[0], &drum_bench_B.DataTypeConversion_ee[0],
                  8);
    (void) memcpy(&drum_bench_B.data_n[8], drum_bench_ConstP.pooled53,
                  12);

    /* BusCreator: '<S116>/Bus Creator' incorporates:
     *  Constant: '<S116>/id'
     *  Constant: '<S116>/type'
     */
    drum_bench_B.BusCreator_nh.type = 3;
    drum_bench_B.BusCreator_nh.id = 1.1F;
    for (i = 0; i < 20; i++) {
      /* BusCreator: '<S114>/Bus Creator' */
      drum_bench_B.BusCreator_c.data[i] = drum_bench_B.data_e[i];

      /* BusCreator: '<S115>/Bus Creator' */
      drum_bench_B.BusCreator_hl.data[i] = drum_bench_B.data_b[i];
      drum_bench_B.BusCreator_nh.data[i] = drum_bench_B.data_n[i];
    }

    /* End of BusCreator: '<S116>/Bus Creator' */
  }

  /* BusCreator: '<S120>/Bus Creator' incorporates:
   *  Constant: '<S120>/Constant'
   *  Constant: '<S120>/Constant1'
   *  Constant: '<S120>/Constant2'
   */
  drum_bench_B.BusCreator_g.type = 10;
  drum_bench_B.BusCreator_g.id = 20.0F;
  for (i = 0; i < 20; i++) {
    drum_bench_B.BusCreator_g.data[i] = drum_bench_ConstP.pooled55[i];
  }

  /* End of BusCreator: '<S120>/Bus Creator' */

  /* Concatenate: '<S94>/Vector Concatenate' */
  drum_bench_B.VectorConcatenate[0] = drum_bench_B.BusCreator_a;
  drum_bench_B.VectorConcatenate[1] = drum_bench_B.BusCreator_h;
  drum_bench_B.VectorConcatenate[2] = drum_bench_B.BusCreator_d;
  drum_bench_B.VectorConcatenate[3] = drum_bench_B.BusCreator_b;
  drum_bench_B.VectorConcatenate[4] = drum_bench_B.BusCreator_bc;
  drum_bench_B.VectorConcatenate[5] = drum_bench_B.BusCreator_ld;
  drum_bench_B.VectorConcatenate[6] = drum_bench_B.BusCreator_nf;
  drum_bench_B.VectorConcatenate[7] = drum_bench_B.BusCreator_k;
  drum_bench_B.VectorConcatenate[8] = drum_bench_B.BusCreator_iq;
  drum_bench_B.VectorConcatenate[9] = drum_bench_B.BusCreator_l;
  drum_bench_B.VectorConcatenate[10] = drum_bench_B.BusCreator_o;
  drum_bench_B.VectorConcatenate[11] = drum_bench_B.BusCreator_c;
  drum_bench_B.VectorConcatenate[12] = drum_bench_B.BusCreator_hl;
  drum_bench_B.VectorConcatenate[13] = drum_bench_B.BusCreator_nh;
  drum_bench_B.VectorConcatenate[14] = drum_bench_B.BusCreator_g;

  /* MATLAB Function: '<S94>/select rpc' */
  drum_bench_selectrpc(drum_bench_B.rpc, drum_bench_B.VectorConcatenate,
                       &drum_bench_B.sf_selectrpc);

  /* Pack: <S94>/Byte Pack */
  (void) memcpy(&drum_bench_B.rpc_i[0], &drum_bench_B.sf_selectrpc.type,
                4);
  (void) memcpy(&drum_bench_B.rpc_i[4], &drum_bench_B.sf_selectrpc.id,
                4);
  (void) memcpy(&drum_bench_B.rpc_i[8], &drum_bench_B.sf_selectrpc.data[0],
                20);
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* RelationalOperator: '<S123>/Compare' incorporates:
     *  Constant: '<S123>/Constant'
     */
    drum_bench_B.Compare = (drum_bench_B.type == 2);

    /* Outputs for Enabled SubSystem: '<S112>/enabled subsystem' */
    drum_bench_enabledsubsystem(drum_bench_B.Compare, drum_bench_B.data,
      &drum_bench_B.enabledsubsystem);

    /* End of Outputs for SubSystem: '<S112>/enabled subsystem' */

    /* Unpack: <S112>/Byte Unpack */
    (void) memcpy(&drum_bench_B.ByteUnpack_e[0],
                  &drum_bench_B.enabledsubsystem.In1[0],
                  20);

    /* DataTypeConversion: '<S112>/Data Type Conversion' */
    for (i = 0; i < 10; i++) {
      drum_bench_B.DataTypeConversion_cm[i] = drum_bench_B.ByteUnpack_e[i];
    }

    /* End of DataTypeConversion: '<S112>/Data Type Conversion' */

    /* DataTypeConversion: '<S112>/analog_limit' */
    drum_bench_B.analog_limit = drum_bench_B.ByteUnpack_e[3];

    /* DataTypeConversion: '<S112>/ethercat' */
    drum_bench_B.ethercat = drum_bench_B.ByteUnpack_e[8];

    /* DataTypeConversion: '<S112>/external_panic' */
    drum_bench_B.external_panic = drum_bench_B.ByteUnpack_e[9];

    /* DataTypeConversion: '<S112>/motor encoder' */
    drum_bench_B.motor_encoder = drum_bench_B.ByteUnpack_e[4];

    /* DataTypeConversion: '<S112>/over_current' */
    drum_bench_B.over_current = drum_bench_B.ByteUnpack_e[0];

    /* DataTypeConversion: '<S112>/over_voltage' */
    drum_bench_B.over_voltage = drum_bench_B.ByteUnpack_e[2];

    /* DataTypeConversion: '<S112>/quadrature_1_high' */
    drum_bench_B.quadrature_1_high = drum_bench_B.ByteUnpack_e[6];

    /* DataTypeConversion: '<S112>/quadrature_1_low' */
    drum_bench_B.quadrature_1_low = drum_bench_B.ByteUnpack_e[5];

    /* DataTypeConversion: '<S112>/quadrature_2_limit' */
    drum_bench_B.quadrature_2_limit = drum_bench_B.ByteUnpack_e[7];

    /* DataTypeConversion: '<S112>/under_voltage' */
    drum_bench_B.under_voltage = drum_bench_B.ByteUnpack_e[1];

    /* RelationalOperator: '<S125>/Compare' incorporates:
     *  Constant: '<S125>/Constant'
     */
    drum_bench_B.Compare_a = (drum_bench_B.type == 3);

    /* Outputs for Enabled SubSystem: '<S113>/Enabled Subsystem' */
    drum_bench_EnabledSubsystem(drum_bench_B.Compare_a, drum_bench_B.data,
      &drum_bench_B.EnabledSubsystem);

    /* End of Outputs for SubSystem: '<S113>/Enabled Subsystem' */

    /* Unpack: <S113>/Byte Unpack */
    (void) memcpy(&drum_bench_B.ByteUnpack[0],
                  &drum_bench_B.EnabledSubsystem.In1[0],
                  20);

    /* DataTypeConversion: '<S113>/load a' */
    drum_bench_B.loada = drum_bench_B.ByteUnpack[0];

    /* DataTypeConversion: '<S113>/load a1' */
    drum_bench_B.loada1 = drum_bench_B.ByteUnpack[2];

    /* DataTypeConversion: '<S113>/load a2' */
    drum_bench_B.loada2 = drum_bench_B.ByteUnpack[3];

    /* DataTypeConversion: '<S113>/load a3' */
    drum_bench_B.loada3 = drum_bench_B.ByteUnpack[4];

    /* DataTypeConversion: '<S113>/load b' */
    drum_bench_B.loadb = drum_bench_B.ByteUnpack[1];

    /* DataTypeConversion: '<S117>/current_index' */
    drum_bench_B.current_index = drum_bench_B.sf_MATLABFunction1.index;
  }

  /* BusSelector: '<S9>/Bus Selector' */
  drum_bench_B.rpc_l = drum_bench_B.modeswitch1.rpc;
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* UnitDelay: '<S161>/Output' */
    drum_bench_B.Output_i4 = drum_bench_DW.Output_DSTATE_h;

    /* Sum: '<S162>/FixPt Sum1' incorporates:
     *  Constant: '<S162>/FixPt Constant'
     */
    drum_bench_B.FixPtSum1_e = drum_bench_B.Output_i4 + 1U;

    /* Switch: '<S163>/FixPt Switch' incorporates:
     *  Constant: '<S163>/Constant'
     */
    if (drum_bench_B.FixPtSum1_e > 4294967295U) {
      drum_bench_B.FixPtSwitch_b = 0U;
    } else {
      drum_bench_B.FixPtSwitch_b = drum_bench_B.FixPtSum1_e;
    }

    /* End of Switch: '<S163>/FixPt Switch' */

    /* DataTypeConversion: '<S160>/debug_' */
    drum_bench_B.debug_d = drum_bench_B.marlin_ec_o21_m;
  }

  /* BusSelector: '<S157>/Bus Selector' */
  drum_bench_B.mode_g = drum_bench_B.modeswitch1.mode;

  /* BusSelector: '<S157>/Bus Selector1' */
  drum_bench_B.motor_iq_m = drum_bench_B.modeswitch1.motor_iq;
  drum_bench_B.motor_id_k = drum_bench_B.modeswitch1.motor_id;

  /* BusSelector: '<S157>/Bus Selector2' */
  drum_bench_B.motor_vq_d = drum_bench_B.modeswitch1.motor_vq;
  drum_bench_B.motor_vd_m = drum_bench_B.modeswitch1.motor_vd;

  /* BusSelector: '<S157>/Bus Selector3' */
  drum_bench_B.analog_diff_i = drum_bench_B.modeswitch1.analog_diff;
  drum_bench_B.analog_diff_dot_ky = drum_bench_B.modeswitch1.analog_diff_dot;

  /* BusSelector: '<S157>/Bus Selector4' */
  drum_bench_B.quadrature_1_angle_c =
    drum_bench_B.modeswitch1.quadrature_1_angle;
  drum_bench_B.quadrature_1_angle_dot_d =
    drum_bench_B.modeswitch1.quadrature_1_angle_dot;

  /* BusSelector: '<S157>/Bus Selector5' */
  drum_bench_B.motor_torque_hy = drum_bench_B.modeswitch1.motor_torque;

  /* MultiPortSwitch: '<S157>/Multiport Switch' incorporates:
   *  Constant: '<S164>/mode'
   *  Constant: '<S165>/mode'
   *  Constant: '<S166>/mode'
   *  Constant: '<S167>/mode'
   *  Constant: '<S168>/mode'
   *  Constant: '<S169>/mode'
   *  Constant: '<S170>/mode'
   *  Constant: '<S171>/mode'
   */
  switch ((int32_T)drum_bench_B.mode_g) {
   case 0:
    drum_bench_B.mode_e = 0U;
    break;

   case 1:
    drum_bench_B.mode_e = 1U;
    break;

   case 2:
    drum_bench_B.mode_e = 2U;
    break;

   case 3:
    drum_bench_B.mode_e = 3U;
    break;

   case 4:
    drum_bench_B.mode_e = 4U;
    break;

   case 5:
    drum_bench_B.mode_e = 5U;
    break;

   case 6:
    drum_bench_B.mode_e = 6U;
    break;

   case 7:
    drum_bench_B.mode_e = 8U;
    break;

   default:
    drum_bench_B.mode_e = 0U;
    break;
  }

  /* End of MultiPortSwitch: '<S157>/Multiport Switch' */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    for (i = 0; i < 14; i++) {
      /* DataTypeConversion: '<S168>/Data Type Conversion1' incorporates:
       *  Constant: '<S168>/filler'
       */
      drum_bench_B.DataTypeConversion1_d[i] = (real32_T)
        drum_bench_ConstP.pooled29[i];

      /* DataTypeConversion: '<S165>/Data Type Conversion1' incorporates:
       *  Constant: '<S165>/filler'
       */
      drum_bench_B.DataTypeConversion1_bm[i] = (real32_T)
        drum_bench_ConstP.pooled29[i];
    }

    /* Pack: <S168>/Byte Pack3 */
    (void) memcpy(&drum_bench_B.command_li[0],
                  &drum_bench_B.DataTypeConversion1_d[0],
                  56);

    /* Pack: <S165>/Byte Pack3 */
    (void) memcpy(&drum_bench_B.command_n[0],
                  &drum_bench_B.DataTypeConversion1_bm[0],
                  56);

    /* DataTypeConversion: '<S169>/Data Type Conversion1' incorporates:
     *  Constant: '<S157>/lock_current'
     *  Constant: '<S169>/filler'
     */
    drum_bench_B.DataTypeConversion1_ev[0] = (real32_T)
      drum_bench_P.config.marlins.M1.motor.phase_lock_current;
    for (i = 0; i < 13; i++) {
      drum_bench_B.DataTypeConversion1_ev[i + 1] = (real32_T)
        drum_bench_ConstP.pooled30[i];
    }

    /* End of DataTypeConversion: '<S169>/Data Type Conversion1' */

    /* Pack: <S169>/Byte Pack3 */
    (void) memcpy(&drum_bench_B.command_o[0],
                  &drum_bench_B.DataTypeConversion1_ev[0],
                  56);
  }

  /* DataTypeConversion: '<S166>/Data Type Conversion1' incorporates:
   *  Constant: '<S166>/filler'
   */
  drum_bench_B.DataTypeConversion1_a[0] = (real32_T)drum_bench_B.motor_iq_m;
  drum_bench_B.DataTypeConversion1_a[1] = (real32_T)drum_bench_B.motor_id_k;
  for (i = 0; i < 12; i++) {
    drum_bench_B.DataTypeConversion1_a[i + 2] = (real32_T)
      drum_bench_ConstP.pooled31[i];
  }

  /* End of DataTypeConversion: '<S166>/Data Type Conversion1' */

  /* Pack: <S166>/Byte Pack2 */
  (void) memcpy(&drum_bench_B.command_i[0], &drum_bench_B.DataTypeConversion1_a
                [0],
                56);

  /* DataTypeConversion: '<S171>/Data Type Conversion1' incorporates:
   *  Constant: '<S171>/filler'
   */
  drum_bench_B.DataTypeConversion1_ey[0] = (real32_T)drum_bench_B.motor_vq_d;
  drum_bench_B.DataTypeConversion1_ey[1] = (real32_T)drum_bench_B.motor_vd_m;
  for (i = 0; i < 12; i++) {
    drum_bench_B.DataTypeConversion1_ey[i + 2] = (real32_T)
      drum_bench_ConstP.pooled31[i];
  }

  /* End of DataTypeConversion: '<S171>/Data Type Conversion1' */

  /* Pack: <S171>/Byte Pack2 */
  (void) memcpy(&drum_bench_B.command_h[0],
                &drum_bench_B.DataTypeConversion1_ey[0],
                56);

  /* DataTypeConversion: '<S164>/Data Type Conversion1' incorporates:
   *  Constant: '<S164>/filler'
   */
  drum_bench_B.DataTypeConversion1_fj[0] = (real32_T)drum_bench_B.analog_diff_i;
  drum_bench_B.DataTypeConversion1_fj[1] = (real32_T)
    drum_bench_B.analog_diff_dot_ky;
  for (i = 0; i < 12; i++) {
    drum_bench_B.DataTypeConversion1_fj[i + 2] = (real32_T)
      drum_bench_ConstP.pooled31[i];
  }

  /* End of DataTypeConversion: '<S164>/Data Type Conversion1' */

  /* Pack: <S164>/Byte Pack2 */
  (void) memcpy(&drum_bench_B.command_hl[0],
                &drum_bench_B.DataTypeConversion1_fj[0],
                56);

  /* DataTypeConversion: '<S170>/Data Type Conversion1' incorporates:
   *  Constant: '<S170>/filler'
   */
  drum_bench_B.DataTypeConversion1_aj[0] = (real32_T)
    drum_bench_B.quadrature_1_angle_c;
  drum_bench_B.DataTypeConversion1_aj[1] = (real32_T)
    drum_bench_B.quadrature_1_angle_dot_d;
  for (i = 0; i < 12; i++) {
    drum_bench_B.DataTypeConversion1_aj[i + 2] = (real32_T)
      drum_bench_ConstP.pooled31[i];
  }

  /* End of DataTypeConversion: '<S170>/Data Type Conversion1' */

  /* Pack: <S170>/Byte Pack2 */
  (void) memcpy(&drum_bench_B.command_gh[0],
                &drum_bench_B.DataTypeConversion1_aj[0],
                56);

  /* DataTypeConversion: '<S167>/Data Type Conversion1' incorporates:
   *  Constant: '<S167>/filler'
   */
  drum_bench_B.DataTypeConversion1_c[0] = (real32_T)drum_bench_B.motor_torque_hy;
  for (i = 0; i < 13; i++) {
    drum_bench_B.DataTypeConversion1_c[i + 1] = (real32_T)
      drum_bench_ConstP.pooled30[i];
  }

  /* End of DataTypeConversion: '<S167>/Data Type Conversion1' */

  /* Pack: <S167>/Byte Pack2 */
  (void) memcpy(&drum_bench_B.command_jo[0],
                &drum_bench_B.DataTypeConversion1_c[0],
                56);

  /* MultiPortSwitch: '<S157>/Multiport Switch' */
  switch ((int32_T)drum_bench_B.mode_g) {
   case 0:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_c[i] = drum_bench_B.command_li[i];
    }
    break;

   case 1:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_c[i] = drum_bench_B.command_n[i];
    }
    break;

   case 2:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_c[i] = drum_bench_B.command_o[i];
    }
    break;

   case 3:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_c[i] = drum_bench_B.command_i[i];
    }
    break;

   case 4:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_c[i] = drum_bench_B.command_h[i];
    }
    break;

   case 5:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_c[i] = drum_bench_B.command_hl[i];
    }
    break;

   case 6:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_c[i] = drum_bench_B.command_gh[i];
    }
    break;

   case 7:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_c[i] = drum_bench_B.command_jo[i];
    }
    break;

   default:
    for (i = 0; i < 56; i++) {
      drum_bench_B.command_c[i] = drum_bench_B.command_li[i];
    }
    break;
  }

  /* End of MultiPortSwitch: '<S157>/Multiport Switch' */

  /* BusCreator: '<S172>/Bus Creator' incorporates:
   *  Constant: '<S172>/id'
   *  Constant: '<S172>/type'
   */
  drum_bench_B.BusCreator_br.type = 6;
  drum_bench_B.BusCreator_br.id = 0.0F;

  /* BusCreator: '<S173>/Bus Creator' incorporates:
   *  Constant: '<S173>/id'
   *  Constant: '<S173>/type'
   */
  drum_bench_B.BusCreator_ih.type = 7;
  drum_bench_B.BusCreator_ih.id = 1.0F;

  /* BusCreator: '<S174>/Bus Creator' incorporates:
   *  Constant: '<S174>/id'
   *  Constant: '<S174>/type'
   */
  drum_bench_B.BusCreator_g1.type = 7;
  drum_bench_B.BusCreator_g1.id = 0.0F;

  /* BusCreator: '<S175>/Bus Creator' incorporates:
   *  Constant: '<S175>/id'
   *  Constant: '<S175>/type'
   */
  drum_bench_B.BusCreator_i2.type = 7;
  drum_bench_B.BusCreator_i2.id = 0.0F;

  /* BusCreator: '<S176>/Bus Creator' incorporates:
   *  Constant: '<S176>/id'
   *  Constant: '<S176>/type'
   */
  drum_bench_B.BusCreator_lg.type = 7;
  drum_bench_B.BusCreator_lg.id = 0.0F;
  for (i = 0; i < 20; i++) {
    /* BusCreator: '<S172>/Bus Creator' */
    drum_bench_B.BusCreator_br.data[i] = drum_bench_ConstB.data_ef[i];

    /* BusCreator: '<S173>/Bus Creator' */
    drum_bench_B.BusCreator_ih.data[i] = drum_bench_ConstB.data_c4[i];

    /* BusCreator: '<S174>/Bus Creator' */
    drum_bench_B.BusCreator_g1.data[i] = drum_bench_ConstB.data_g[i];

    /* BusCreator: '<S175>/Bus Creator' */
    drum_bench_B.BusCreator_i2.data[i] = drum_bench_ConstB.data_b[i];
    drum_bench_B.BusCreator_lg.data[i] = drum_bench_ConstB.data_fw[i];
  }

  /* End of BusCreator: '<S176>/Bus Creator' */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* Constant: '<S195>/id' */
    drum_bench_B.VectorConcatenate_e[0] =
      drum_bench_P.config.marlins.M1.amp.current_zero_a;

    /* Constant: '<S195>/id1' */
    drum_bench_B.VectorConcatenate_e[1] =
      drum_bench_P.config.marlins.M1.amp.current_zero_b;

    /* Constant: '<S195>/id2' */
    drum_bench_B.VectorConcatenate_e[2] =
      drum_bench_P.config.marlins.M1.amp.deadband;

    /* Constant: '<S195>/id3' */
    drum_bench_B.VectorConcatenate_e[3] =
      drum_bench_P.config.marlins.M1.amp.led_brightness;

    /* Constant: '<S195>/id4' */
    drum_bench_B.VectorConcatenate_e[4] =
      drum_bench_P.config.marlins.M1.amp.nominal_bus_voltage;

    /* Constant: '<S195>/id5' */
    drum_bench_B.VectorConcatenate_e[5] =
      drum_bench_P.config.marlins.M1.amp.current_sensor_limit;

    /* Constant: '<S209>/id' */
    drum_bench_B.VectorConcatenate_e[6] =
      drum_bench_P.config.marlins.M1.amp.logger_10kHz.trigger_level;

    /* Constant: '<S208>/id' */
    drum_bench_B.VectorConcatenate_e[7] =
      drum_bench_P.config.marlins.M1.amp.logger_100kHz.trigger_level;

    /* Constant: '<S201>/id' */
    drum_bench_B.VectorConcatenate_e[8] =
      drum_bench_P.config.marlins.M1.control_current.current_filter_bandwidth;

    /* Constant: '<S201>/id1' */
    drum_bench_B.VectorConcatenate_e[9] =
      drum_bench_P.config.marlins.M1.control_current.fingerprint_gain;

    /* Constant: '<S201>/id2' */
    drum_bench_B.VectorConcatenate_e[10] =
      drum_bench_P.config.marlins.M1.control_current.ki;

    /* Constant: '<S201>/id3' */
    drum_bench_B.VectorConcatenate_e[11] =
      drum_bench_P.config.marlins.M1.control_current.ki_limit;

    /* Constant: '<S201>/id4' */
    drum_bench_B.VectorConcatenate_e[12] =
      drum_bench_P.config.marlins.M1.control_current.kp;

    /* Constant: '<S215>/id' */
    drum_bench_B.VectorConcatenate_e[13] =
      drum_bench_P.config.marlins.M1.control_motor_angle.pid_deadband.deadband;

    /* Constant: '<S216>/id' */
    drum_bench_B.VectorConcatenate_e[14] =
      drum_bench_P.config.marlins.M1.control_motor_angle.pid_interpolate.enabled;

    /* Constant: '<S214>/id' */
    drum_bench_B.VectorConcatenate_e[15] =
      drum_bench_P.config.marlins.M1.control_motor_angle.pid.kd;

    /* Constant: '<S214>/id1' */
    drum_bench_B.VectorConcatenate_e[16] =
      drum_bench_P.config.marlins.M1.control_motor_angle.pid.ki;

    /* Constant: '<S214>/id2' */
    drum_bench_B.VectorConcatenate_e[17] =
      drum_bench_P.config.marlins.M1.control_motor_angle.pid.ki_limit;

    /* Constant: '<S214>/id3' */
    drum_bench_B.VectorConcatenate_e[18] =
      drum_bench_P.config.marlins.M1.control_motor_angle.pid.kp;

    /* Constant: '<S217>/id' */
    drum_bench_B.VectorConcatenate_e[19] =
      drum_bench_P.config.marlins.M1.control_quadrature_2_angle.pid.kd;

    /* Constant: '<S217>/id1' */
    drum_bench_B.VectorConcatenate_e[20] =
      drum_bench_P.config.marlins.M1.control_quadrature_2_angle.pid.ki;

    /* Constant: '<S217>/id2' */
    drum_bench_B.VectorConcatenate_e[21] =
      drum_bench_P.config.marlins.M1.control_quadrature_2_angle.pid.ki_limit;

    /* Constant: '<S217>/id3' */
    drum_bench_B.VectorConcatenate_e[22] =
      drum_bench_P.config.marlins.M1.control_quadrature_2_angle.pid.kp;

    /* Constant: '<S213>/id' */
    drum_bench_B.VectorConcatenate_e[23] =
      drum_bench_P.config.marlins.M1.control_analog.pid.kd;

    /* Constant: '<S213>/id1' */
    drum_bench_B.VectorConcatenate_e[24] =
      drum_bench_P.config.marlins.M1.control_analog.pid.ki;

    /* Constant: '<S213>/id2' */
    drum_bench_B.VectorConcatenate_e[25] =
      drum_bench_P.config.marlins.M1.control_analog.pid.ki_limit;

    /* Constant: '<S213>/id3' */
    drum_bench_B.VectorConcatenate_e[26] =
      drum_bench_P.config.marlins.M1.control_analog.pid.kp;

    /* Constant: '<S200>/id' */
    drum_bench_B.VectorConcatenate_e[27] =
      drum_bench_P.config.marlins.M1.control_analog.source;

    /* Constant: '<S204>/id' */
    drum_bench_B.VectorConcatenate_e[28] =
      drum_bench_P.config.marlins.M1.motor.current_limit;

    /* Constant: '<S204>/id1' */
    drum_bench_B.VectorConcatenate_e[29] =
      drum_bench_P.config.marlins.M1.motor.voltage_limit;

    /* Constant: '<S204>/id2' */
    drum_bench_B.VectorConcatenate_e[30] =
      drum_bench_P.config.marlins.M1.motor.encoder.counts_per_revolution;

    /* Constant: '<S204>/id3' */
    drum_bench_B.VectorConcatenate_e[31] =
      drum_bench_P.config.marlins.M1.motor.km;

    /* Constant: '<S204>/id4' */
    drum_bench_B.VectorConcatenate_e[32] =
      drum_bench_P.config.marlins.M1.motor.num_poles;

    /* Constant: '<S204>/id5' */
    drum_bench_B.VectorConcatenate_e[33] =
      drum_bench_P.config.marlins.M1.motor.resistance;

    /* Constant: '<S204>/id6' */
    drum_bench_B.VectorConcatenate_e[34] =
      drum_bench_P.config.marlins.M1.motor.electrical_offset;

    /* Constant: '<S204>/id7' */
    drum_bench_B.VectorConcatenate_e[35] =
      drum_bench_P.config.marlins.M1.motor.v_per_a;

    /* Constant: '<S199>/id' */
    drum_bench_B.VectorConcatenate_e[36] =
      drum_bench_P.config.marlins.M1.bridge.thermal_model.eA1;

    /* Constant: '<S199>/id1' */
    drum_bench_B.VectorConcatenate_e[37] =
      drum_bench_P.config.marlins.M1.bridge.thermal_model.eA2;

    /* Constant: '<S218>/id' */
    drum_bench_B.VectorConcatenate_e[38] =
      drum_bench_P.config.marlins.M1.quadrature_1.sensor.bias;

    /* Constant: '<S218>/id1' */
    drum_bench_B.VectorConcatenate_e[39] =
      drum_bench_P.config.marlins.M1.quadrature_1.sensor.gain;

    /* Constant: '<S218>/id6' */
    drum_bench_B.VectorConcatenate_e[40] =
      drum_bench_P.config.marlins.M1.quadrature_1.sensor.limit_high;

    /* Constant: '<S218>/id3' */
    drum_bench_B.VectorConcatenate_e[41] =
      drum_bench_P.config.marlins.M1.quadrature_1.sensor.limit_low;

    /* Constant: '<S218>/id4' */
    drum_bench_B.VectorConcatenate_e[42] =
      drum_bench_P.config.marlins.M1.quadrature_1.sensor.filter_bandwidth;

    /* Constant: '<S218>/id5' */
    drum_bench_B.VectorConcatenate_e[43] =
      drum_bench_P.config.marlins.M1.quadrature_1.sensor.dot_filter_bandwidth;

    /* Constant: '<S205>/id' */
    drum_bench_B.VectorConcatenate_e[44] =
      drum_bench_P.config.marlins.M1.quadrature_1.enable_index;

    /* Constant: '<S219>/id' */
    drum_bench_B.VectorConcatenate_e[45] =
      drum_bench_P.config.marlins.M1.quadrature_2.sensor.bias;

    /* Constant: '<S219>/id1' */
    drum_bench_B.VectorConcatenate_e[46] =
      drum_bench_P.config.marlins.M1.quadrature_2.sensor.gain;

    /* Constant: '<S219>/id6' */
    drum_bench_B.VectorConcatenate_e[47] =
      drum_bench_P.config.marlins.M1.quadrature_2.sensor.limit_high;

    /* Constant: '<S219>/id3' */
    drum_bench_B.VectorConcatenate_e[48] =
      drum_bench_P.config.marlins.M1.quadrature_2.sensor.limit_low;

    /* Constant: '<S219>/id4' */
    drum_bench_B.VectorConcatenate_e[49] =
      drum_bench_P.config.marlins.M1.quadrature_2.sensor.filter_bandwidth;

    /* Constant: '<S219>/id5' */
    drum_bench_B.VectorConcatenate_e[50] =
      drum_bench_P.config.marlins.M1.quadrature_2.sensor.dot_filter_bandwidth;

    /* Constant: '<S206>/id' */
    drum_bench_B.VectorConcatenate_e[51] =
      drum_bench_P.config.marlins.M1.quadrature_2.enable_index;

    /* Constant: '<S210>/id' */
    drum_bench_B.VectorConcatenate_e[52] =
      drum_bench_P.config.marlins.M1.analog_1.sensor.bias;

    /* Constant: '<S210>/id1' */
    drum_bench_B.VectorConcatenate_e[53] =
      drum_bench_P.config.marlins.M1.analog_1.sensor.gain;

    /* Constant: '<S210>/id6' */
    drum_bench_B.VectorConcatenate_e[54] =
      drum_bench_P.config.marlins.M1.analog_1.sensor.limit_high;

    /* Constant: '<S210>/id3' */
    drum_bench_B.VectorConcatenate_e[55] =
      drum_bench_P.config.marlins.M1.analog_1.sensor.limit_low;

    /* Constant: '<S210>/id4' */
    drum_bench_B.VectorConcatenate_e[56] =
      drum_bench_P.config.marlins.M1.analog_1.sensor.filter_bandwidth;

    /* Constant: '<S210>/id5' */
    drum_bench_B.VectorConcatenate_e[57] =
      drum_bench_P.config.marlins.M1.analog_1.sensor.dot_filter_bandwidth;

    /* Constant: '<S211>/id' */
    drum_bench_B.VectorConcatenate_e[58] =
      drum_bench_P.config.marlins.M1.analog_2.sensor.bias;

    /* Constant: '<S211>/id1' */
    drum_bench_B.VectorConcatenate_e[59] =
      drum_bench_P.config.marlins.M1.analog_2.sensor.gain;

    /* Constant: '<S211>/id6' */
    drum_bench_B.VectorConcatenate_e[60] =
      drum_bench_P.config.marlins.M1.analog_2.sensor.limit_high;

    /* Constant: '<S211>/id3' */
    drum_bench_B.VectorConcatenate_e[61] =
      drum_bench_P.config.marlins.M1.analog_2.sensor.limit_low;

    /* Constant: '<S211>/id4' */
    drum_bench_B.VectorConcatenate_e[62] =
      drum_bench_P.config.marlins.M1.analog_2.sensor.filter_bandwidth;

    /* Constant: '<S211>/id5' */
    drum_bench_B.VectorConcatenate_e[63] =
      drum_bench_P.config.marlins.M1.analog_2.sensor.dot_filter_bandwidth;

    /* Constant: '<S212>/id' */
    drum_bench_B.VectorConcatenate_e[64] =
      drum_bench_P.config.marlins.M1.analog_diff.sensor.bias;

    /* Constant: '<S212>/id1' */
    drum_bench_B.VectorConcatenate_e[65] =
      drum_bench_P.config.marlins.M1.analog_diff.sensor.gain;

    /* Constant: '<S212>/id6' */
    drum_bench_B.VectorConcatenate_e[66] =
      drum_bench_P.config.marlins.M1.analog_diff.sensor.limit_high;

    /* Constant: '<S212>/id3' */
    drum_bench_B.VectorConcatenate_e[67] =
      drum_bench_P.config.marlins.M1.analog_diff.sensor.limit_low;

    /* Constant: '<S212>/id4' */
    drum_bench_B.VectorConcatenate_e[68] =
      drum_bench_P.config.marlins.M1.analog_diff.sensor.filter_bandwidth;

    /* Constant: '<S212>/id5' */
    drum_bench_B.VectorConcatenate_e[69] =
      drum_bench_P.config.marlins.M1.analog_diff.sensor.dot_filter_bandwidth;

    /* Constant: '<S220>/id' */
    drum_bench_B.VectorConcatenate_e[70] =
      drum_bench_P.config.marlins.M1.ssi.sensor.bias;

    /* Constant: '<S220>/id1' */
    drum_bench_B.VectorConcatenate_e[71] =
      drum_bench_P.config.marlins.M1.ssi.sensor.gain;

    /* Constant: '<S220>/id6' */
    drum_bench_B.VectorConcatenate_e[72] =
      drum_bench_P.config.marlins.M1.ssi.sensor.limit_high;

    /* Constant: '<S220>/id3' */
    drum_bench_B.VectorConcatenate_e[73] =
      drum_bench_P.config.marlins.M1.ssi.sensor.limit_low;

    /* Constant: '<S220>/id4' */
    drum_bench_B.VectorConcatenate_e[74] =
      drum_bench_P.config.marlins.M1.ssi.sensor.filter_bandwidth;

    /* Constant: '<S220>/id5' */
    drum_bench_B.VectorConcatenate_e[75] =
      drum_bench_P.config.marlins.M1.ssi.sensor.dot_filter_bandwidth;

    /* DataTypeConversion: '<S193>/Data Type Conversion' */
    for (i = 0; i < 76; i++) {
      drum_bench_B.DataTypeConversion_aa[i] = (real32_T)
        drum_bench_B.VectorConcatenate_e[i];
    }

    /* End of DataTypeConversion: '<S193>/Data Type Conversion' */

    /* Unpack: <S159>/Byte Unpack */
    (void) memcpy(&drum_bench_B.type_b, &drum_bench_B.rpa_packet_b[0],
                  4);
    (void) memcpy(&drum_bench_B.id_p, &drum_bench_B.rpa_packet_b[4],
                  4);
    (void) memcpy(&drum_bench_B.data_a[0], &drum_bench_B.rpa_packet_b[8],
                  20);

    /* BusCreator: '<S192>/BusConversion_InsertedFor_MATLAB Function1_at_inport_1' */
    drum_bench_B.BusConversion_InsertedFor_MAT_c.type = drum_bench_B.type_b;
    drum_bench_B.BusConversion_InsertedFor_MAT_c.id = drum_bench_B.id_p;
    for (i = 0; i < 20; i++) {
      drum_bench_B.BusConversion_InsertedFor_MAT_c.data[i] =
        drum_bench_B.data_a[i];
    }

    /* End of BusCreator: '<S192>/BusConversion_InsertedFor_MATLAB Function1_at_inport_1' */

    /* MATLAB Function: '<S192>/MATLAB Function1' incorporates:
     *  Constant: '<S192>/Constant2'
     */
    drum_bench_MATLABFunction1(drum_bench_B.DataTypeConversion_aa,
      &drum_bench_B.BusConversion_InsertedFor_MAT_c, 1U,
      &drum_bench_B.sf_MATLABFunction1_a, &drum_bench_DW.sf_MATLABFunction1_a);

    /* Pack: <S192>/Byte Pack */
    (void) memcpy(&drum_bench_B.data_e5[0], &drum_bench_ConstP.pooled43,
                  4);
    (void) memcpy(&drum_bench_B.data_e5[4],
                  &drum_bench_B.sf_MATLABFunction1_a.index,
                  4);
    (void) memcpy(&drum_bench_B.data_e5[8],
                  &drum_bench_B.sf_MATLABFunction1_a.value,
                  4);
    (void) memcpy(&drum_bench_B.data_e5[12], drum_bench_ConstP.pooled52,
                  8);

    /* BusCreator: '<S192>/Bus Creator' */
    drum_bench_B.BusCreator_j.type = drum_bench_B.sf_MATLABFunction1_a.type;
    drum_bench_B.BusCreator_j.id = drum_bench_B.sf_MATLABFunction1_a.id;
    for (i = 0; i < 20; i++) {
      drum_bench_B.BusCreator_j.data[i] = drum_bench_B.data_e5[i];
    }

    /* End of BusCreator: '<S192>/Bus Creator' */
  }

  /* BusCreator: '<S184>/Bus Creator' incorporates:
   *  Constant: '<S184>/id'
   *  Constant: '<S184>/type'
   */
  drum_bench_B.BusCreator_dy.type = 1;
  drum_bench_B.BusCreator_dy.id = 0.0F;

  /* BusCreator: '<S177>/Bus Creator' incorporates:
   *  Constant: '<S177>/id'
   *  Constant: '<S177>/type'
   */
  drum_bench_B.BusCreator_d1.type = 2;
  drum_bench_B.BusCreator_d1.id = 0.0F;

  /* BusCreator: '<S187>/Bus Creator' incorporates:
   *  Constant: '<S187>/id'
   *  Constant: '<S187>/type'
   */
  drum_bench_B.BusCreator_fc.type = 3;
  drum_bench_B.BusCreator_fc.id = 0.0F;

  /* BusCreator: '<S186>/Bus Creator' incorporates:
   *  Constant: '<S186>/id'
   *  Constant: '<S186>/type'
   */
  drum_bench_B.BusCreator_bw.type = 3;
  drum_bench_B.BusCreator_bw.id = 0.0F;

  /* BusCreator: '<S178>/Bus Creator' incorporates:
   *  Constant: '<S178>/id'
   *  Constant: '<S178>/type'
   */
  drum_bench_B.BusCreator_ow.type = 3;
  drum_bench_B.BusCreator_ow.id = 1.0F;
  for (i = 0; i < 20; i++) {
    /* BusCreator: '<S184>/Bus Creator' */
    drum_bench_B.BusCreator_dy.data[i] = drum_bench_ConstB.data_ec[i];

    /* BusCreator: '<S177>/Bus Creator' */
    drum_bench_B.BusCreator_d1.data[i] = drum_bench_ConstB.data_og[i];

    /* BusCreator: '<S187>/Bus Creator' */
    drum_bench_B.BusCreator_fc.data[i] = drum_bench_ConstB.data_bu[i];

    /* BusCreator: '<S186>/Bus Creator' */
    drum_bench_B.BusCreator_bw.data[i] = drum_bench_ConstB.data_ci[i];
    drum_bench_B.BusCreator_ow.data[i] = drum_bench_ConstB.data_e0[i];
  }

  /* End of BusCreator: '<S178>/Bus Creator' */
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* DataTypeConversion: '<S179>/Data Type Conversion' incorporates:
     *  Constant: '<S179>/value'
     *  Constant: '<S179>/value1'
     */
    acc = floor(1.0);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_k[0] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;
    acc = floor(drum_bench_P.config.marlins.M1.debug_address_index);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_k[1] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;

    /* End of DataTypeConversion: '<S179>/Data Type Conversion' */

    /* Pack: <S179>/Byte Pack */
    (void) memcpy(&drum_bench_B.data_o[0], &drum_bench_B.DataTypeConversion_k[0],
                  8);
    (void) memcpy(&drum_bench_B.data_o[8], drum_bench_ConstP.pooled53,
                  12);

    /* BusCreator: '<S179>/Bus Creator' incorporates:
     *  Constant: '<S179>/id'
     *  Constant: '<S179>/type'
     */
    drum_bench_B.BusCreator_dj.type = 3;
    drum_bench_B.BusCreator_dj.id = 1.1F;

    /* DataTypeConversion: '<S180>/Data Type Conversion' incorporates:
     *  Constant: '<S180>/value'
     *  Constant: '<S180>/value1'
     */
    acc = floor(1.0);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_k0[0] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;
    acc = floor(drum_bench_P.config.marlins.M1.debug_address_index);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_k0[1] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;

    /* End of DataTypeConversion: '<S180>/Data Type Conversion' */

    /* Pack: <S180>/Byte Pack */
    (void) memcpy(&drum_bench_B.data_bl[0], &drum_bench_B.DataTypeConversion_k0
                  [0],
                  8);
    (void) memcpy(&drum_bench_B.data_bl[8], drum_bench_ConstP.pooled53,
                  12);

    /* BusCreator: '<S180>/Bus Creator' incorporates:
     *  Constant: '<S180>/id'
     *  Constant: '<S180>/type'
     */
    drum_bench_B.BusCreator_om.type = 3;
    drum_bench_B.BusCreator_om.id = 1.1F;

    /* DataTypeConversion: '<S181>/Data Type Conversion' incorporates:
     *  Constant: '<S181>/value'
     *  Constant: '<S181>/value1'
     */
    acc = floor(1.0);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_iv[0] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;
    acc = floor(drum_bench_P.config.marlins.M1.debug_address_index);
    if (rtIsNaN(acc) || rtIsInf(acc)) {
      acc = 0.0;
    } else {
      acc = fmod(acc, 4.294967296E+9);
    }

    drum_bench_B.DataTypeConversion_iv[1] = acc < 0.0 ? (uint32_T)-(int32_T)
      (uint32_T)-acc : (uint32_T)acc;

    /* End of DataTypeConversion: '<S181>/Data Type Conversion' */

    /* Pack: <S181>/Byte Pack */
    (void) memcpy(&drum_bench_B.data_mh[0], &drum_bench_B.DataTypeConversion_iv
                  [0],
                  8);
    (void) memcpy(&drum_bench_B.data_mh[8], drum_bench_ConstP.pooled53,
                  12);

    /* BusCreator: '<S181>/Bus Creator' incorporates:
     *  Constant: '<S181>/id'
     *  Constant: '<S181>/type'
     */
    drum_bench_B.BusCreator_co.type = 3;
    drum_bench_B.BusCreator_co.id = 1.1F;
    for (i = 0; i < 20; i++) {
      /* BusCreator: '<S179>/Bus Creator' */
      drum_bench_B.BusCreator_dj.data[i] = drum_bench_B.data_o[i];

      /* BusCreator: '<S180>/Bus Creator' */
      drum_bench_B.BusCreator_om.data[i] = drum_bench_B.data_bl[i];
      drum_bench_B.BusCreator_co.data[i] = drum_bench_B.data_mh[i];
    }

    /* End of BusCreator: '<S181>/Bus Creator' */
  }

  /* BusCreator: '<S185>/Bus Creator' incorporates:
   *  Constant: '<S185>/Constant'
   *  Constant: '<S185>/Constant1'
   *  Constant: '<S185>/Constant2'
   */
  drum_bench_B.BusCreator_gs.type = 10;
  drum_bench_B.BusCreator_gs.id = 20.0F;
  for (i = 0; i < 20; i++) {
    drum_bench_B.BusCreator_gs.data[i] = drum_bench_ConstP.pooled55[i];
  }

  /* End of BusCreator: '<S185>/Bus Creator' */

  /* Concatenate: '<S159>/Vector Concatenate' */
  drum_bench_B.VectorConcatenate_a[0] = drum_bench_B.BusCreator_j;
  drum_bench_B.VectorConcatenate_a[1] = drum_bench_B.BusCreator_dy;
  drum_bench_B.VectorConcatenate_a[2] = drum_bench_B.BusCreator_d1;
  drum_bench_B.VectorConcatenate_a[3] = drum_bench_B.BusCreator_fc;
  drum_bench_B.VectorConcatenate_a[4] = drum_bench_B.BusCreator_bw;
  drum_bench_B.VectorConcatenate_a[5] = drum_bench_B.BusCreator_ow;
  drum_bench_B.VectorConcatenate_a[6] = drum_bench_B.BusCreator_br;
  drum_bench_B.VectorConcatenate_a[7] = drum_bench_B.BusCreator_ih;
  drum_bench_B.VectorConcatenate_a[8] = drum_bench_B.BusCreator_g1;
  drum_bench_B.VectorConcatenate_a[9] = drum_bench_B.BusCreator_lg;
  drum_bench_B.VectorConcatenate_a[10] = drum_bench_B.BusCreator_i2;
  drum_bench_B.VectorConcatenate_a[11] = drum_bench_B.BusCreator_dj;
  drum_bench_B.VectorConcatenate_a[12] = drum_bench_B.BusCreator_om;
  drum_bench_B.VectorConcatenate_a[13] = drum_bench_B.BusCreator_co;
  drum_bench_B.VectorConcatenate_a[14] = drum_bench_B.BusCreator_gs;

  /* MATLAB Function: '<S159>/select rpc' */
  drum_bench_selectrpc(drum_bench_B.rpc_l, drum_bench_B.VectorConcatenate_a,
                       &drum_bench_B.sf_selectrpc_j);

  /* Pack: <S159>/Byte Pack */
  (void) memcpy(&drum_bench_B.rpc_n[0], &drum_bench_B.sf_selectrpc_j.type,
                4);
  (void) memcpy(&drum_bench_B.rpc_n[4], &drum_bench_B.sf_selectrpc_j.id,
                4);
  (void) memcpy(&drum_bench_B.rpc_n[8], &drum_bench_B.sf_selectrpc_j.data[0],
                20);
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* RelationalOperator: '<S188>/Compare' incorporates:
     *  Constant: '<S188>/Constant'
     */
    drum_bench_B.Compare_b = (drum_bench_B.type_b == 2);

    /* Outputs for Enabled SubSystem: '<S177>/enabled subsystem' */
    drum_bench_enabledsubsystem(drum_bench_B.Compare_b, drum_bench_B.data_a,
      &drum_bench_B.enabledsubsystem_l);

    /* End of Outputs for SubSystem: '<S177>/enabled subsystem' */

    /* Unpack: <S177>/Byte Unpack */
    (void) memcpy(&drum_bench_B.ByteUnpack_eb[0],
                  &drum_bench_B.enabledsubsystem_l.In1[0],
                  20);

    /* DataTypeConversion: '<S177>/Data Type Conversion' */
    for (i = 0; i < 10; i++) {
      drum_bench_B.DataTypeConversion_i[i] = drum_bench_B.ByteUnpack_eb[i];
    }

    /* End of DataTypeConversion: '<S177>/Data Type Conversion' */

    /* DataTypeConversion: '<S177>/analog_limit' */
    drum_bench_B.analog_limit_k = drum_bench_B.ByteUnpack_eb[3];

    /* DataTypeConversion: '<S177>/ethercat' */
    drum_bench_B.ethercat_l = drum_bench_B.ByteUnpack_eb[8];

    /* DataTypeConversion: '<S177>/external_panic' */
    drum_bench_B.external_panic_l = drum_bench_B.ByteUnpack_eb[9];

    /* DataTypeConversion: '<S177>/motor encoder' */
    drum_bench_B.motor_encoder_h = drum_bench_B.ByteUnpack_eb[4];

    /* DataTypeConversion: '<S177>/over_current' */
    drum_bench_B.over_current_p = drum_bench_B.ByteUnpack_eb[0];

    /* DataTypeConversion: '<S177>/over_voltage' */
    drum_bench_B.over_voltage_o = drum_bench_B.ByteUnpack_eb[2];

    /* DataTypeConversion: '<S177>/quadrature_1_high' */
    drum_bench_B.quadrature_1_high_p = drum_bench_B.ByteUnpack_eb[6];

    /* DataTypeConversion: '<S177>/quadrature_1_low' */
    drum_bench_B.quadrature_1_low_c = drum_bench_B.ByteUnpack_eb[5];

    /* DataTypeConversion: '<S177>/quadrature_2_limit' */
    drum_bench_B.quadrature_2_limit_a = drum_bench_B.ByteUnpack_eb[7];

    /* DataTypeConversion: '<S177>/under_voltage' */
    drum_bench_B.under_voltage_l = drum_bench_B.ByteUnpack_eb[1];

    /* RelationalOperator: '<S190>/Compare' incorporates:
     *  Constant: '<S190>/Constant'
     */
    drum_bench_B.Compare_aw = (drum_bench_B.type_b == 3);

    /* Outputs for Enabled SubSystem: '<S178>/Enabled Subsystem' */
    drum_bench_EnabledSubsystem(drum_bench_B.Compare_aw, drum_bench_B.data_a,
      &drum_bench_B.EnabledSubsystem_k);

    /* End of Outputs for SubSystem: '<S178>/Enabled Subsystem' */

    /* Unpack: <S178>/Byte Unpack */
    (void) memcpy(&drum_bench_B.ByteUnpack_h[0],
                  &drum_bench_B.EnabledSubsystem_k.In1[0],
                  20);

    /* DataTypeConversion: '<S178>/load a' */
    drum_bench_B.loada_c = drum_bench_B.ByteUnpack_h[0];

    /* DataTypeConversion: '<S178>/load a1' */
    drum_bench_B.loada1_h = drum_bench_B.ByteUnpack_h[2];

    /* DataTypeConversion: '<S178>/load a2' */
    drum_bench_B.loada2_p = drum_bench_B.ByteUnpack_h[3];

    /* DataTypeConversion: '<S178>/load a3' */
    drum_bench_B.loada3_o = drum_bench_B.ByteUnpack_h[4];

    /* DataTypeConversion: '<S178>/load b' */
    drum_bench_B.loadb_a = drum_bench_B.ByteUnpack_h[1];

    /* DataTypeConversion: '<S182>/current_index' */
    drum_bench_B.current_index_c = drum_bench_B.sf_MATLABFunction1_a.index;
  }

  /* DataTypeConversion: '<Root>/mode0' */
  drum_bench_B.mode0 = drum_bench_B.mode;

  /* DataTypeConversion: '<Root>/mode1' */
  drum_bench_B.mode1 = drum_bench_B.mode_l;

  /* DataTypeConversion: '<Root>/motor_iq0' */
  drum_bench_B.motor_iq0 = drum_bench_B.motor_iq_b0;

  /* DataTypeConversion: '<Root>/motor_iq1' */
  drum_bench_B.motor_iq1 = drum_bench_B.motor_iq_n;

  /* DataTypeConversion: '<Root>/motor_torque0' */
  drum_bench_B.motor_torque0 = drum_bench_B.motor_torque_h4;

  /* DataTypeConversion: '<Root>/motor_torque1' */
  drum_bench_B.motor_torque1 = drum_bench_B.motor_torque_px;

  /* S-Function Block: <Root>/Domain State
   */
  {
    ec_domain_state_t state;
    ecrt_domain_state( (ec_domain_t*)drum_bench_DW.DomainState_PWORK.DomainPtr,
                      &state);
    drum_bench_B.DomainState_o1 = state.working_counter;
    drum_bench_B.DomainState_o2 = state.wc_state;
  }

  /* S-Function Block: <Root>/Master State
   */
  {
    ec_master_state_t state;
    ecrt_master_state( (ec_master_t*)drum_bench_DW.MasterState_PWORK.MasterPtr,
                      &state);
    drum_bench_B.MasterState_o1 = state.slaves_responding;
    drum_bench_B.MasterState_o2 = state.al_states;
    drum_bench_B.MasterState_o3 = state.link_up ? 1 : 0;
  }

  /* Constant: '<Root>/mode select' */
  drum_bench_B.modeselect = drum_bench_P.config.control0.mode_select;

  /* Constant: '<Root>/mode select1' */
  drum_bench_B.modeselect1 = drum_bench_P.config.control1.mode_select;

  /* Outputs for Triggered SubSystem: '<S14>/Triggered Subsystem' incorporates:
   *  TriggerPort: '<S231>/Trigger'
   */
  /* Constant: '<S14>/Constant' */
  zcEvent = rt_ZCFcn(RISING_ZERO_CROSSING,
                     &drum_bench_PrevZCX.TriggeredSubsystem_Trig_ZCE,
                     (drum_bench_P.config.input.input_extra.trigger_yaml_write));
  if (zcEvent != NO_ZCEVENT) {
    /* S-Function (write_yaml_params): '<S231>/S-Function' */
    write_yaml_params_Outputs_wrapper( );
  }

  /* End of Outputs for SubSystem: '<S14>/Triggered Subsystem' */
}

/* Model update function */
static void drum_bench_update(void)
{
  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* S-Function Block: <S95>/marlin_ec
     */
    /* Input Port 1 written directly by slave */
    /* Input Port 2 written directly by slave */
    /* Input Port 3 written directly by slave */
    /* Input Port 4 written directly by slave */
    /* Input Port 5 written directly by slave */

    /* Update for Delay: '<S93>/Delay' */
    drum_bench_DW.Delay_DSTATE[0] = drum_bench_B.Sum[0];
    drum_bench_DW.Delay_DSTATE[1] = drum_bench_B.Sum[1];

    /* S-Function Block: <S160>/marlin_ec
     */
    /* Input Port 1 written directly by slave */
    /* Input Port 2 written directly by slave */
    /* Input Port 3 written directly by slave */
    /* Input Port 4 written directly by slave */
    /* Input Port 5 written directly by slave */

    /* Update for Delay: '<S158>/Delay' */
    drum_bench_DW.Delay_DSTATE_n[0] = drum_bench_B.Sum_d[0];
    drum_bench_DW.Delay_DSTATE_n[1] = drum_bench_B.Sum_d[1];

    /* Update for Atomic SubSystem: '<S2>/Filtered Derivative' */
    drum__FilteredDerivative_Update(&drum_bench_B.FilteredDerivative,
      &drum_bench_DW.FilteredDerivative);

    /* End of Update for SubSystem: '<S2>/Filtered Derivative' */

    /* Update for Atomic SubSystem: '<S12>/Filtered Derivative' */
    drum__FilteredDerivative_Update(&drum_bench_B.FilteredDerivative_c,
      &drum_bench_DW.FilteredDerivative_c);

    /* End of Update for SubSystem: '<S12>/Filtered Derivative' */

    /* Update for Atomic SubSystem: '<S3>/Filtered Derivative' */
    drum__FilteredDerivative_Update(&drum_bench_B.FilteredDerivative_n,
      &drum_bench_DW.FilteredDerivative_n);

    /* End of Update for SubSystem: '<S3>/Filtered Derivative' */

    /* Update for Atomic SubSystem: '<S13>/Filtered Derivative' */
    drum__FilteredDerivative_Update(&drum_bench_B.FilteredDerivative_p,
      &drum_bench_DW.FilteredDerivative_p);

    /* End of Update for SubSystem: '<S13>/Filtered Derivative' */

    /* Update for UnitDelay: '<S96>/Output' */
    drum_bench_DW.Output_DSTATE = drum_bench_B.FixPtSwitch;

    /* Update for UnitDelay: '<S161>/Output' */
    drum_bench_DW.Output_DSTATE_h = drum_bench_B.FixPtSwitch_b;
  }

  /* user code (Update function Trailer) */

  /* EtherCAT Queue for Sample Time [0.0003] */
#ifndef ASYNC_ECAT

  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0 && rtmIsMajorTimeStep
      (drum_bench_M)) {
    ecs_send(1);
  }

#endif

  /* EtherCAT Queue for Sample Time [9.9999999999999991E-5] */
#ifndef ASYNC_ECAT

  if (1 && rtmIsMajorTimeStep(drum_bench_M)) {
    ecs_send(0);
  }

#endif

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++drum_bench_M->Timing.clockTick0)) {
    ++drum_bench_M->Timing.clockTickH0;
  }

  drum_bench_M->Timing.t[0] = drum_bench_M->Timing.clockTick0 *
    drum_bench_M->Timing.stepSize0 + drum_bench_M->Timing.clockTickH0 *
    drum_bench_M->Timing.stepSize0 * 4294967296.0;

  {
    /* Update absolute timer for sample time: [9.9999999999999991E-5s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++drum_bench_M->Timing.clockTick1)) {
      ++drum_bench_M->Timing.clockTickH1;
    }

    drum_bench_M->Timing.t[1] = drum_bench_M->Timing.clockTick1 *
      drum_bench_M->Timing.stepSize1 + drum_bench_M->Timing.clockTickH1 *
      drum_bench_M->Timing.stepSize1 * 4294967296.0;
  }

  if (drum_bench_M->Timing.TaskCounters.TID[2] == 0) {
    /* Update absolute timer for sample time: [0.0003s, 0.0s] */
    /* The "clockTick2" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick2"
     * and "Timing.stepSize2". Size of "clockTick2" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick2 and the high bits
     * Timing.clockTickH2. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++drum_bench_M->Timing.clockTick2)) {
      ++drum_bench_M->Timing.clockTickH2;
    }

    drum_bench_M->Timing.t[2] = drum_bench_M->Timing.clockTick2 *
      drum_bench_M->Timing.stepSize2 + drum_bench_M->Timing.clockTickH2 *
      drum_bench_M->Timing.stepSize2 * 4294967296.0;
  }

  if (drum_bench_M->Timing.TaskCounters.TID[3] == 0) {
    /* Update absolute timer for sample time: [0.001s, 0.0s] */
    /* The "clockTick3" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick3"
     * and "Timing.stepSize3". Size of "clockTick3" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick3 and the high bits
     * Timing.clockTickH3. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++drum_bench_M->Timing.clockTick3)) {
      ++drum_bench_M->Timing.clockTickH3;
    }

    drum_bench_M->Timing.t[3] = drum_bench_M->Timing.clockTick3 *
      drum_bench_M->Timing.stepSize3 + drum_bench_M->Timing.clockTickH3 *
      drum_bench_M->Timing.stepSize3 * 4294967296.0;
  }

  if (drum_bench_M->Timing.TaskCounters.TID[4] == 0) {
    /* Update absolute timer for sample time: [0.01s, 0.0s] */
    /* The "clockTick4" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick4"
     * and "Timing.stepSize4". Size of "clockTick4" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick4 and the high bits
     * Timing.clockTickH4. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++drum_bench_M->Timing.clockTick4)) {
      ++drum_bench_M->Timing.clockTickH4;
    }

    drum_bench_M->Timing.t[4] = drum_bench_M->Timing.clockTick4 *
      drum_bench_M->Timing.stepSize4 + drum_bench_M->Timing.clockTickH4 *
      drum_bench_M->Timing.stepSize4 * 4294967296.0;
  }

  if (drum_bench_M->Timing.TaskCounters.TID[5] == 0) {
    /* Update absolute timer for sample time: [0.0384s, 0.0s] */
    /* The "clockTick5" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick5"
     * and "Timing.stepSize5". Size of "clockTick5" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick5 and the high bits
     * Timing.clockTickH5. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++drum_bench_M->Timing.clockTick5)) {
      ++drum_bench_M->Timing.clockTickH5;
    }

    drum_bench_M->Timing.t[5] = drum_bench_M->Timing.clockTick5 *
      drum_bench_M->Timing.stepSize5 + drum_bench_M->Timing.clockTickH5 *
      drum_bench_M->Timing.stepSize5 * 4294967296.0;
  }

  rate_scheduler();
}

/* Model initialize function */
void drum_bench_initialize(void)
{
  {
    /* user code (Start function Header) */
    const char __attribute__((unused)) *errstr_rc;
    const struct ec_slave *slave_head;
    const struct ec_slave **slave_list = &slave_head;

    /* Zero terminated list of sample times for EtherCAT */
    unsigned int ec_st[5] = {
      100000U,
      300000U,
      1000000U,
      10000000U,
      38400000U,
    };

    /* Start for S-Function (ec_slave3_runtime): '<S95>/marlin_ec' */

    /* End Modification */

    /* S-Function Block: <S95>/marlin_ec
     * Registering EtherCAT block M3EC with Driver.
     */

    /* Modification to EtherLab allowing alias and position to get set
     * at runtime. (see Rob or Advait)
     */
    ec_slave_1.alias = *&(drum_bench_P.config.marlins.M0.slave_alias);
    ec_slave_1.position = *&(drum_bench_P.config.marlins.M0.slave_position);

    /* End Modification */
    *slave_list = &ec_slave_1;
    slave_list = &ec_slave_1.next;

    /* Start for S-Function (ec_slave3_runtime): '<S160>/marlin_ec' */

    /* End Modification */

    /* S-Function Block: <S160>/marlin_ec
     * Registering EtherCAT block M3EC with Driver.
     */

    /* Modification to EtherLab allowing alias and position to get set
     * at runtime. (see Rob or Advait)
     */
    ec_slave_2.alias = *&(drum_bench_P.config.marlins.M1.slave_alias);
    ec_slave_2.position = *&(drum_bench_P.config.marlins.M1.slave_position);

    /* End Modification */
    *slave_list = &ec_slave_2;
    slave_list = &ec_slave_2.next;

    /* Level2 S-Function Block: '<S1>/C++ S-function1' (keyboard_press) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[0];
      sfcnStart(rts);
      if (ssGetErrorStatus(rts) != (NULL))
        return;
    }

    /* Level2 S-Function Block: '<S1>/C++ S-function2' (networking) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[1];
      sfcnStart(rts);
      if (ssGetErrorStatus(rts) != (NULL))
        return;
    }

    /* Level2 S-Function Block: '<S1>/C++ S-function3' (EMG) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[2];
      sfcnStart(rts);
      if (ssGetErrorStatus(rts) != (NULL))
        return;
    }

    /* Level2 S-Function Block: '<S1>/Inlined C++ S-Function' (behavior_controller) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[3];
      sfcnStart(rts);
      if (ssGetErrorStatus(rts) != (NULL))
        return;
    }

    /* Start for Enabled SubSystem: '<S112>/enabled subsystem' */
    drum_ben_enabledsubsystem_Start(&drum_bench_B.enabledsubsystem);

    /* End of Start for SubSystem: '<S112>/enabled subsystem' */

    /* Start for Enabled SubSystem: '<S113>/Enabled Subsystem' */
    drum_ben_EnabledSubsystem_Start(&drum_bench_B.EnabledSubsystem);

    /* End of Start for SubSystem: '<S113>/Enabled Subsystem' */

    /* Start for Enabled SubSystem: '<S177>/enabled subsystem' */
    drum_ben_enabledsubsystem_Start(&drum_bench_B.enabledsubsystem_l);

    /* End of Start for SubSystem: '<S177>/enabled subsystem' */

    /* Start for Enabled SubSystem: '<S178>/Enabled Subsystem' */
    drum_ben_EnabledSubsystem_Start(&drum_bench_B.EnabledSubsystem_k);

    /* End of Start for SubSystem: '<S178>/Enabled Subsystem' */

    /* S-Function Block: <Root>/Domain State
     */
    drum_bench_DW.DomainState_PWORK.DomainPtr = ecs_get_domain_ptr(0U, 0U, 1, 1,
      0, &errstr_rc);
    if (!drum_bench_DW.DomainState_PWORK.DomainPtr) {
      snprintf(etl_errbuf, sizeof(etl_errbuf),
               "Getting input domain pointer in "
               "drum_bench/Domain State failed: %s",
               errstr_rc);
      rtmSetErrorStatus(drum_bench_M, etl_errbuf);
      return;
    }

    /* S-Function Block: <Root>/Master State
     */
    errstr_rc = ecs_setup_master(0, 0U,
      &drum_bench_DW.MasterState_PWORK.MasterPtr);
    if (errstr_rc) {
      snprintf(etl_errbuf, sizeof(etl_errbuf),
               "Setting up master in drum_bench/Master State "
               "failed: %s", errstr_rc);
      rtmSetErrorStatus(drum_bench_M, etl_errbuf);
      return;
    }

    /* user code (Start function Trailer) */

    /* Starting EtherCAT subsystem */
    if ((errstr_rc = ecs_start(slave_head, ec_st, 5, 1))) {
      snprintf(etl_errbuf, sizeof(etl_errbuf),
               "Starting EtherCAT subsystem failed: %s", errstr_rc);
      rtmSetErrorStatus(drum_bench_M, etl_errbuf);
      return;
    }
  }

  drum_bench_PrevZCX.TriggeredSubsystem_Trig_ZCE = UNINITIALIZED_ZCSIG;

  {
    int32_T i;

    /* InitializeConditions for Delay: '<S93>/Delay' */
    drum_bench_DW.Delay_DSTATE[0] = 0.0;
    drum_bench_DW.Delay_DSTATE[1] = 0.0;

    /* InitializeConditions for DiscreteFir: '<S223>/Generated Filter Block' */
    drum_bench_DW.GeneratedFilterBlock_circBuf = 0;

    /* InitializeConditions for DiscreteFir: '<S222>/Generated Filter Block' */
    drum_bench_DW.GeneratedFilterBlock_circBuf_n = 0;

    /* InitializeConditions for DiscreteFir: '<S224>/Generated Filter Block' */
    drum_bench_DW.GeneratedFilterBlock_circBuf_b = 0;

    /* InitializeConditions for Delay: '<S158>/Delay' */
    drum_bench_DW.Delay_DSTATE_n[0] = 0.0;
    drum_bench_DW.Delay_DSTATE_n[1] = 0.0;

    /* InitializeConditions for DiscreteFir: '<S227>/Generated Filter Block' */
    drum_bench_DW.GeneratedFilterBlock_circBuf_o = 0;

    /* InitializeConditions for DiscreteFir: '<S226>/Generated Filter Block' */
    drum_bench_DW.GeneratedFilterBlock_circBuf_k = 0;

    /* InitializeConditions for DiscreteFir: '<S228>/Generated Filter Block' */
    drum_bench_DW.GeneratedFilterBlock_circBuf_bj = 0;
    for (i = 0; i < 23; i++) {
      /* InitializeConditions for DiscreteFir: '<S223>/Generated Filter Block' */
      drum_bench_DW.GeneratedFilterBlock_states[i] = 0.0;

      /* InitializeConditions for DiscreteFir: '<S222>/Generated Filter Block' */
      drum_bench_DW.GeneratedFilterBlock_states_f[i] = 0.0;

      /* InitializeConditions for DiscreteFir: '<S224>/Generated Filter Block' */
      drum_bench_DW.GeneratedFilterBlock_states_c[i] = 0.0;

      /* InitializeConditions for DiscreteFir: '<S227>/Generated Filter Block' */
      drum_bench_DW.GeneratedFilterBlock_states_ch[i] = 0.0;

      /* InitializeConditions for DiscreteFir: '<S226>/Generated Filter Block' */
      drum_bench_DW.GeneratedFilterBlock_states_k[i] = 0.0;
      drum_bench_DW.GeneratedFilterBlock_states_l[i] = 0.0;
    }

    /* End of InitializeConditions for DiscreteFir: '<S228>/Generated Filter Block' */

    /* InitializeConditions for DiscreteFir: '<S30>/Generated Filter Block' */
    drum_bench_DW.GeneratedFilterBlock_circBuf_a = 0;
    for (i = 0; i < 140; i++) {
      drum_bench_DW.GeneratedFilterBlock_states_l3[i] = 0.0;
    }

    /* End of InitializeConditions for DiscreteFir: '<S30>/Generated Filter Block' */

    /* InitializeConditions for S-Function (sdspbiquad): '<S33>/Generated Filter Block' */
    for (i = 0; i < 6; i++) {
      drum_bench_DW.GeneratedFilterBlock_FILT_STATE[i] = 0.0;
    }

    /* End of InitializeConditions for S-Function (sdspbiquad): '<S33>/Generated Filter Block' */

    /* InitializeConditions for MATLAB Function: '<S16>/smooth' */
    drum_bench_smooth_Init(&drum_bench_DW.sf_smooth);

    /* InitializeConditions for Buffer: '<S16>/Buffer3' */
    for (i = 0; i < 256; i++) {
      drum_bench_DW.Buffer3_CircBuf[i] = 0.0;
    }

    drum_bench_DW.Buffer3_inBufPtrIdx = 128;
    drum_bench_DW.Buffer3_bufferLength = 128;
    drum_bench_DW.Buffer3_outBufPtrIdx = 0;

    /* End of InitializeConditions for Buffer: '<S16>/Buffer3' */

    /* InitializeConditions for MATLAB Function: '<S16>/zeroCrossing' */
    drum_bench_zeroCrossing_Init(&drum_bench_DW.sf_zeroCrossing);

    /* InitializeConditions for Buffer: '<S16>/Buffer2' */
    for (i = 0; i < 256; i++) {
      drum_bench_DW.Buffer2_CircBuf[i] = 0.0;
    }

    drum_bench_DW.Buffer2_inBufPtrIdx = 128;
    drum_bench_DW.Buffer2_bufferLength = 128;
    drum_bench_DW.Buffer2_outBufPtrIdx = 0;

    /* End of InitializeConditions for Buffer: '<S16>/Buffer2' */

    /* InitializeConditions for MATLAB Function: '<S16>/slopeChange' */
    drum_bench_slopeChange_Init(&drum_bench_DW.sf_slopeChange);

    /* InitializeConditions for Buffer: '<S16>/Buffer1' */
    drum_bench_DW.Buffer1_inBufPtrIdx = 128;
    drum_bench_DW.Buffer1_bufferLength = 128;
    drum_bench_DW.Buffer1_outBufPtrIdx = 0;
    for (i = 0; i < 256; i++) {
      /* InitializeConditions for Buffer: '<S16>/Buffer1' */
      drum_bench_DW.Buffer1_CircBuf[i] = 0.0;

      /* InitializeConditions for Buffer: '<S16>/Buffer' */
      drum_bench_DW.Buffer_CircBuf[i] = 0.0;
    }

    /* InitializeConditions for Buffer: '<S16>/Buffer' */
    drum_bench_DW.Buffer_inBufPtrIdx = 128;
    drum_bench_DW.Buffer_bufferLength = 128;
    drum_bench_DW.Buffer_outBufPtrIdx = 0;

    /* InitializeConditions for S-Function (sdspwindow2): '<S16>/Window Function' */
    drum_bench_DW.WindowFunction_FLAG = FALSE;

    /* InitializeConditions for MATLAB Function: '<S16>/MATLAB Function' */
    memset(&drum_bench_DW.buf[0], 0, sizeof(real_T) << 7U);

    /* InitializeConditions for DiscreteFir: '<S17>/Generated Filter Block' */
    drum_bench_DW.GeneratedFilterBlock_circBuf_o2 = 0;
    for (i = 0; i < 140; i++) {
      drum_bench_DW.GeneratedFilterBlock_states_kq[i] = 0.0;
    }

    /* End of InitializeConditions for DiscreteFir: '<S17>/Generated Filter Block' */

    /* InitializeConditions for S-Function (sdspbiquad): '<S20>/Generated Filter Block' */
    for (i = 0; i < 6; i++) {
      drum_bench_DW.GeneratedFilterBlock_FILT_STA_d[i] = 0.0;
    }

    /* End of InitializeConditions for S-Function (sdspbiquad): '<S20>/Generated Filter Block' */

    /* InitializeConditions for MATLAB Function: '<S15>/smooth' */
    drum_bench_smooth_Init(&drum_bench_DW.sf_smooth_a);

    /* InitializeConditions for Buffer: '<S15>/Buffer3' */
    for (i = 0; i < 256; i++) {
      drum_bench_DW.Buffer3_CircBuf_k[i] = 0.0;
    }

    drum_bench_DW.Buffer3_inBufPtrIdx_m = 128;
    drum_bench_DW.Buffer3_bufferLength_b = 128;
    drum_bench_DW.Buffer3_outBufPtrIdx_a = 0;

    /* End of InitializeConditions for Buffer: '<S15>/Buffer3' */

    /* InitializeConditions for MATLAB Function: '<S15>/zeroCrossing' */
    drum_bench_zeroCrossing_Init(&drum_bench_DW.sf_zeroCrossing_f);

    /* InitializeConditions for MATLAB Function: '<S15>/smooth1' */
    drum_bench_smooth1_Init(&drum_bench_DW.sf_smooth1);

    /* InitializeConditions for Buffer: '<S15>/Buffer2' */
    for (i = 0; i < 256; i++) {
      drum_bench_DW.Buffer2_CircBuf_o[i] = 0.0;
    }

    drum_bench_DW.Buffer2_inBufPtrIdx_o = 128;
    drum_bench_DW.Buffer2_bufferLength_l = 128;
    drum_bench_DW.Buffer2_outBufPtrIdx_p = 0;

    /* End of InitializeConditions for Buffer: '<S15>/Buffer2' */

    /* InitializeConditions for MATLAB Function: '<S15>/slopeChange' */
    drum_bench_slopeChange_Init(&drum_bench_DW.sf_slopeChange_n);

    /* InitializeConditions for MATLAB Function: '<S15>/smooth2' */
    drum_bench_smooth1_Init(&drum_bench_DW.sf_smooth2);

    /* InitializeConditions for Buffer: '<S15>/Buffer1' */
    drum_bench_DW.Buffer1_inBufPtrIdx_b = 128;
    drum_bench_DW.Buffer1_bufferLength_e = 128;
    drum_bench_DW.Buffer1_outBufPtrIdx_l = 0;
    for (i = 0; i < 256; i++) {
      /* InitializeConditions for Buffer: '<S15>/Buffer1' */
      drum_bench_DW.Buffer1_CircBuf_a[i] = 0.0;

      /* InitializeConditions for Buffer: '<S15>/Buffer' */
      drum_bench_DW.Buffer_CircBuf_f[i] = 0.0;
    }

    /* InitializeConditions for Buffer: '<S15>/Buffer' */
    drum_bench_DW.Buffer_inBufPtrIdx_d = 128;
    drum_bench_DW.Buffer_bufferLength_k = 128;
    drum_bench_DW.Buffer_outBufPtrIdx_f = 0;

    /* InitializeConditions for S-Function (sdspwindow2): '<S15>/Window Function' */
    drum_bench_DW.WindowFunction_FLAG_e = FALSE;

    /* InitializeConditions for MATLAB Function: '<S15>/MATLAB Function' */
    memset(&drum_bench_DW.buf_j[0], 0, sizeof(real_T) << 7U);

    /* InitializeConditions for MATLAB Function: '<S15>/smooth3' */
    drum_bench_smooth3_Init(&drum_bench_DW.sf_smooth3);

    /* InitializeConditions for MATLAB Function: '<S16>/smooth1' */
    drum_bench_smooth1_Init(&drum_bench_DW.sf_smooth1_p);

    /* InitializeConditions for MATLAB Function: '<S16>/smooth2' */
    drum_bench_smooth1_Init(&drum_bench_DW.sf_smooth2_h);

    /* InitializeConditions for MATLAB Function: '<S16>/smooth3' */
    drum_bench_smooth3_Init(&drum_bench_DW.sf_smooth3_d);

    /* InitializeConditions for RateLimiter: '<S48>/Rate Limiter' */
    drum_bench_DW.PrevY = 0.0;

    /* InitializeConditions for RateLimiter: '<S49>/Rate Limiter' */
    drum_bench_DW.PrevY_a = 0.0;

    /* InitializeConditions for RateLimiter: '<S45>/Rate Limiter' */
    drum_bench_DW.PrevY_h = 0.0;

    /* InitializeConditions for RateLimiter: '<S46>/Rate Limiter' */
    drum_bench_DW.PrevY_d = 0.0;

    /* InitializeConditions for RateLimiter: '<S47>/Rate Limiter' */
    drum_bench_DW.PrevY_dx = 0.0;

    /* InitializeConditions for Atomic SubSystem: '<S2>/Filtered Derivative' */
    drum_be_FilteredDerivative_Init(&drum_bench_DW.FilteredDerivative);

    /* End of InitializeConditions for SubSystem: '<S2>/Filtered Derivative' */

    /* InitializeConditions for Atomic SubSystem: '<S12>/Filtered Derivative' */
    drum_be_FilteredDerivative_Init(&drum_bench_DW.FilteredDerivative_c);

    /* End of InitializeConditions for SubSystem: '<S12>/Filtered Derivative' */

    /* InitializeConditions for RateLimiter: '<S58>/Rate Limiter' */
    drum_bench_DW.PrevY_m = 0.0;

    /* InitializeConditions for RateLimiter: '<S59>/Rate Limiter' */
    drum_bench_DW.PrevY_b = 0.0;

    /* InitializeConditions for RateLimiter: '<S55>/Rate Limiter' */
    drum_bench_DW.PrevY_f = 0.0;

    /* InitializeConditions for RateLimiter: '<S56>/Rate Limiter' */
    drum_bench_DW.PrevY_d5 = 0.0;

    /* InitializeConditions for RateLimiter: '<S57>/Rate Limiter' */
    drum_bench_DW.PrevY_k = 0.0;

    /* InitializeConditions for Atomic SubSystem: '<S3>/Filtered Derivative' */
    drum_be_FilteredDerivative_Init(&drum_bench_DW.FilteredDerivative_n);

    /* End of InitializeConditions for SubSystem: '<S3>/Filtered Derivative' */

    /* InitializeConditions for Atomic SubSystem: '<S13>/Filtered Derivative' */
    drum_be_FilteredDerivative_Init(&drum_bench_DW.FilteredDerivative_p);

    /* End of InitializeConditions for SubSystem: '<S13>/Filtered Derivative' */

    /* InitializeConditions for UnitDelay: '<S96>/Output' */
    drum_bench_DW.Output_DSTATE = 0U;

    /* InitializeConditions for MATLAB Function: '<S127>/MATLAB Function1' */
    drum_bench_MATLABFunction1_Init(&drum_bench_DW.sf_MATLABFunction1);

    /* InitializeConditions for UnitDelay: '<S161>/Output' */
    drum_bench_DW.Output_DSTATE_h = 0U;

    /* InitializeConditions for MATLAB Function: '<S192>/MATLAB Function1' */
    drum_bench_MATLABFunction1_Init(&drum_bench_DW.sf_MATLABFunction1_a);
  }
}

/* Model terminate function */
void drum_bench_terminate(void)
{
  /* user code (Terminate function Body) */

  /* Shutting down EtherCAT subsystem */
  ecs_end(5);

  /* Level2 S-Function Block: '<S1>/C++ S-function1' (keyboard_press) */
  {
    SimStruct *rts = drum_bench_M->childSfunctions[0];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S1>/C++ S-function2' (networking) */
  {
    SimStruct *rts = drum_bench_M->childSfunctions[1];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S1>/C++ S-function3' (EMG) */
  {
    SimStruct *rts = drum_bench_M->childSfunctions[2];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S1>/Inlined C++ S-Function' (behavior_controller) */
  {
    SimStruct *rts = drum_bench_M->childSfunctions[3];
    sfcnTerminate(rts);
  }
}

/*========================================================================*
 * Start of Classic call interface                                        *
 *========================================================================*/
void MdlOutputs(int_T tid)
{
  drum_bench_output();
  UNUSED_PARAMETER(tid);
}

void MdlUpdate(int_T tid)
{
  drum_bench_update();
  UNUSED_PARAMETER(tid);
}

void MdlInitializeSizes(void)
{
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
}

void MdlStart(void)
{
  drum_bench_initialize();
}

void MdlTerminate(void)
{
  drum_bench_terminate();
}

/* Registration function */
RT_MODEL_drum_bench_T *drum_bench(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)drum_bench_M, 0,
                sizeof(RT_MODEL_drum_bench_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&drum_bench_M->solverInfo,
                          &drum_bench_M->Timing.simTimeStep);
    rtsiSetTPtr(&drum_bench_M->solverInfo, &rtmGetTPtr(drum_bench_M));
    rtsiSetStepSizePtr(&drum_bench_M->solverInfo,
                       &drum_bench_M->Timing.stepSize0);
    rtsiSetErrorStatusPtr(&drum_bench_M->solverInfo, (&rtmGetErrorStatus
      (drum_bench_M)));
    rtsiSetRTModelPtr(&drum_bench_M->solverInfo, drum_bench_M);
  }

  rtsiSetSimTimeStep(&drum_bench_M->solverInfo, MAJOR_TIME_STEP);
  rtsiSetSolverName(&drum_bench_M->solverInfo,"FixedStepDiscrete");
  drum_bench_M->solverInfoPtr = (&drum_bench_M->solverInfo);

  /* Initialize timing info */
  {
    int_T *mdlTsMap = drum_bench_M->Timing.sampleTimeTaskIDArray;
    int_T i;
    for (i = 0; i < 6; i++) {
      mdlTsMap[i] = i;
    }

    drum_bench_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    drum_bench_M->Timing.sampleTimes = (&drum_bench_M->Timing.sampleTimesArray[0]);
    drum_bench_M->Timing.offsetTimes = (&drum_bench_M->Timing.offsetTimesArray[0]);

    /* task periods */
    drum_bench_M->Timing.sampleTimes[0] = (0.0);
    drum_bench_M->Timing.sampleTimes[1] = (9.9999999999999991E-5);
    drum_bench_M->Timing.sampleTimes[2] = (0.0003);
    drum_bench_M->Timing.sampleTimes[3] = (0.001);
    drum_bench_M->Timing.sampleTimes[4] = (0.01);
    drum_bench_M->Timing.sampleTimes[5] = (0.0384);

    /* task offsets */
    drum_bench_M->Timing.offsetTimes[0] = (0.0);
    drum_bench_M->Timing.offsetTimes[1] = (0.0);
    drum_bench_M->Timing.offsetTimes[2] = (0.0);
    drum_bench_M->Timing.offsetTimes[3] = (0.0);
    drum_bench_M->Timing.offsetTimes[4] = (0.0);
    drum_bench_M->Timing.offsetTimes[5] = (0.0);
  }

  rtmSetTPtr(drum_bench_M, &drum_bench_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = drum_bench_M->Timing.sampleHitArray;
    int_T i;
    for (i = 0; i < 6; i++) {
      mdlSampleHits[i] = 1;
    }

    drum_bench_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(drum_bench_M, -1);
  drum_bench_M->Timing.stepSize0 = 9.9999999999999991E-5;
  drum_bench_M->Timing.stepSize1 = 9.9999999999999991E-5;
  drum_bench_M->Timing.stepSize2 = 0.0003;
  drum_bench_M->Timing.stepSize3 = 0.001;
  drum_bench_M->Timing.stepSize4 = 0.01;
  drum_bench_M->Timing.stepSize5 = 0.0384;
  drum_bench_M->solverInfoPtr = (&drum_bench_M->solverInfo);
  drum_bench_M->Timing.stepSize = (9.9999999999999991E-5);
  rtsiSetFixedStepSize(&drum_bench_M->solverInfo, 9.9999999999999991E-5);
  rtsiSetSolverMode(&drum_bench_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  drum_bench_M->ModelData.blockIO = ((void *) &drum_bench_B);
  (void) memset(((void *) &drum_bench_B), 0,
                sizeof(B_drum_bench_T));

  /* parameters */
  drum_bench_M->ModelData.defaultParam = ((real_T *)&drum_bench_P);

  /* states (dwork) */
  drum_bench_M->ModelData.dwork = ((void *) &drum_bench_DW);
  (void) memset((void *)&drum_bench_DW, 0,
                sizeof(DW_drum_bench_T));

  /* Initialize DataMapInfo substructure containing ModelMap for C API */
  drum_bench_InitializeDataMapInfo(drum_bench_M);

  /* child S-Function registration */
  {
    RTWSfcnInfo *sfcnInfo = &drum_bench_M->NonInlinedSFcns.sfcnInfo;
    drum_bench_M->sfcnInfo = (sfcnInfo);
    rtssSetErrorStatusPtr(sfcnInfo, (&rtmGetErrorStatus(drum_bench_M)));
    rtssSetNumRootSampTimesPtr(sfcnInfo, &drum_bench_M->Sizes.numSampTimes);
    drum_bench_M->NonInlinedSFcns.taskTimePtrs[0] = &(rtmGetTPtr(drum_bench_M)[0]);
    drum_bench_M->NonInlinedSFcns.taskTimePtrs[1] = &(rtmGetTPtr(drum_bench_M)[1]);
    drum_bench_M->NonInlinedSFcns.taskTimePtrs[2] = &(rtmGetTPtr(drum_bench_M)[2]);
    drum_bench_M->NonInlinedSFcns.taskTimePtrs[3] = &(rtmGetTPtr(drum_bench_M)[3]);
    drum_bench_M->NonInlinedSFcns.taskTimePtrs[4] = &(rtmGetTPtr(drum_bench_M)[4]);
    drum_bench_M->NonInlinedSFcns.taskTimePtrs[5] = &(rtmGetTPtr(drum_bench_M)[5]);
    rtssSetTPtrPtr(sfcnInfo,drum_bench_M->NonInlinedSFcns.taskTimePtrs);
    rtssSetTStartPtr(sfcnInfo, &rtmGetTStart(drum_bench_M));
    rtssSetTFinalPtr(sfcnInfo, &rtmGetTFinal(drum_bench_M));
    rtssSetTimeOfLastOutputPtr(sfcnInfo, &rtmGetTimeOfLastOutput(drum_bench_M));
    rtssSetStepSizePtr(sfcnInfo, &drum_bench_M->Timing.stepSize);
    rtssSetStopRequestedPtr(sfcnInfo, &rtmGetStopRequested(drum_bench_M));
    rtssSetDerivCacheNeedsResetPtr(sfcnInfo,
      &drum_bench_M->ModelData.derivCacheNeedsReset);
    rtssSetZCCacheNeedsResetPtr(sfcnInfo,
      &drum_bench_M->ModelData.zCCacheNeedsReset);
    rtssSetBlkStateChangePtr(sfcnInfo, &drum_bench_M->ModelData.blkStateChange);
    rtssSetSampleHitsPtr(sfcnInfo, &drum_bench_M->Timing.sampleHits);
    rtssSetPerTaskSampleHitsPtr(sfcnInfo,
      &drum_bench_M->Timing.perTaskSampleHits);
    rtssSetSimModePtr(sfcnInfo, &drum_bench_M->simMode);
    rtssSetSolverInfoPtr(sfcnInfo, &drum_bench_M->solverInfoPtr);
  }

  drum_bench_M->Sizes.numSFcns = (4);

  /* register each child */
  {
    (void) memset((void *)&drum_bench_M->NonInlinedSFcns.childSFunctions[0], 0,
                  4*sizeof(SimStruct));
    drum_bench_M->childSfunctions =
      (&drum_bench_M->NonInlinedSFcns.childSFunctionPtrs[0]);
    drum_bench_M->childSfunctions[0] =
      (&drum_bench_M->NonInlinedSFcns.childSFunctions[0]);
    drum_bench_M->childSfunctions[1] =
      (&drum_bench_M->NonInlinedSFcns.childSFunctions[1]);
    drum_bench_M->childSfunctions[2] =
      (&drum_bench_M->NonInlinedSFcns.childSFunctions[2]);
    drum_bench_M->childSfunctions[3] =
      (&drum_bench_M->NonInlinedSFcns.childSFunctions[3]);

    /* Level2 S-Function Block: drum_bench/<S1>/C++ S-function1 (keyboard_press) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[0];

      /* timing info */
      time_T *sfcnPeriod = drum_bench_M->NonInlinedSFcns.Sfcn0.sfcnPeriod;
      time_T *sfcnOffset = drum_bench_M->NonInlinedSFcns.Sfcn0.sfcnOffset;
      int_T *sfcnTsMap = drum_bench_M->NonInlinedSFcns.Sfcn0.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &drum_bench_M->NonInlinedSFcns.blkInfo2[0]);
      }

      ssSetRTWSfcnInfo(rts, drum_bench_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &drum_bench_M->NonInlinedSFcns.methods2[0]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &drum_bench_M->NonInlinedSFcns.methods3[0]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &drum_bench_M->NonInlinedSFcns.statesInfo2[0]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &drum_bench_M->NonInlinedSFcns.Sfcn0.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 1);
          ssSetOutputPortSignal(rts, 0, ((real_T *) &drum_bench_B.CSfunction1));
        }
      }

      /* path info */
      ssSetModelName(rts, "C++ S-function1");
      ssSetPath(rts, "drum_bench/Behavior Control/C++ S-function1");
      ssSetRTModel(rts,drum_bench_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &drum_bench_M->NonInlinedSFcns.Sfcn0.params;
        ssSetSFcnParamsCount(rts, 1);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)drum_bench_ConstP.pooled5);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &drum_bench_DW.CSfunction1_PWORK);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &drum_bench_M->NonInlinedSFcns.Sfcn0.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &drum_bench_M->NonInlinedSFcns.Sfcn0.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &drum_bench_DW.CSfunction1_PWORK);
      }

      /* registration */
      keyboard_press(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.001);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 3;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: drum_bench/<S1>/C++ S-function2 (networking) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[1];

      /* timing info */
      time_T *sfcnPeriod = drum_bench_M->NonInlinedSFcns.Sfcn1.sfcnPeriod;
      time_T *sfcnOffset = drum_bench_M->NonInlinedSFcns.Sfcn1.sfcnOffset;
      int_T *sfcnTsMap = drum_bench_M->NonInlinedSFcns.Sfcn1.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &drum_bench_M->NonInlinedSFcns.blkInfo2[1]);
      }

      ssSetRTWSfcnInfo(rts, drum_bench_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &drum_bench_M->NonInlinedSFcns.methods2[1]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &drum_bench_M->NonInlinedSFcns.methods3[1]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &drum_bench_M->NonInlinedSFcns.statesInfo2[1]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &drum_bench_M->NonInlinedSFcns.Sfcn1.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 1);
          ssSetOutputPortSignal(rts, 0, ((real_T *) &drum_bench_B.CSfunction2));
        }
      }

      /* path info */
      ssSetModelName(rts, "C++ S-function2");
      ssSetPath(rts, "drum_bench/Behavior Control/C++ S-function2");
      ssSetRTModel(rts,drum_bench_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &drum_bench_M->NonInlinedSFcns.Sfcn1.params;
        ssSetSFcnParamsCount(rts, 1);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)drum_bench_ConstP.pooled5);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &drum_bench_DW.CSfunction2_PWORK);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &drum_bench_M->NonInlinedSFcns.Sfcn1.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &drum_bench_M->NonInlinedSFcns.Sfcn1.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &drum_bench_DW.CSfunction2_PWORK);
      }

      /* registration */
      networking(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.001);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 3;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: drum_bench/<S1>/C++ S-function3 (EMG) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[2];

      /* timing info */
      time_T *sfcnPeriod = drum_bench_M->NonInlinedSFcns.Sfcn2.sfcnPeriod;
      time_T *sfcnOffset = drum_bench_M->NonInlinedSFcns.Sfcn2.sfcnOffset;
      int_T *sfcnTsMap = drum_bench_M->NonInlinedSFcns.Sfcn2.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &drum_bench_M->NonInlinedSFcns.blkInfo2[2]);
      }

      ssSetRTWSfcnInfo(rts, drum_bench_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &drum_bench_M->NonInlinedSFcns.methods2[2]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &drum_bench_M->NonInlinedSFcns.methods3[2]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &drum_bench_M->NonInlinedSFcns.statesInfo2[2]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 8);
        ssSetPortInfoForInputs(rts,
          &drum_bench_M->NonInlinedSFcns.Sfcn2.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn2.UPtrs0;
          sfcnUPtrs[0] = &drum_bench_B.sf_mean2.y;
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 1);
        }

        /* port 1 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn2.UPtrs1;
          sfcnUPtrs[0] = &drum_bench_B.sf_mean.y;
          ssSetInputPortSignalPtrs(rts, 1, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 1, 1);
          ssSetInputPortWidth(rts, 1, 1);
        }

        /* port 2 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn2.UPtrs2;
          sfcnUPtrs[0] = &drum_bench_B.sf_mean1.y;
          ssSetInputPortSignalPtrs(rts, 2, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 2, 1);
          ssSetInputPortWidth(rts, 2, 1);
        }

        /* port 3 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn2.UPtrs3;
          sfcnUPtrs[0] = &drum_bench_B.y;
          ssSetInputPortSignalPtrs(rts, 3, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 3, 1);
          ssSetInputPortWidth(rts, 3, 1);
        }

        /* port 4 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn2.UPtrs4;
          sfcnUPtrs[0] = &drum_bench_B.sf_mean2_b.y;
          ssSetInputPortSignalPtrs(rts, 4, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 4, 1);
          ssSetInputPortWidth(rts, 4, 1);
        }

        /* port 5 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn2.UPtrs5;
          sfcnUPtrs[0] = &drum_bench_B.sf_mean_p.y;
          ssSetInputPortSignalPtrs(rts, 5, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 5, 1);
          ssSetInputPortWidth(rts, 5, 1);
        }

        /* port 6 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn2.UPtrs6;
          sfcnUPtrs[0] = &drum_bench_B.sf_mean1_m.y;
          ssSetInputPortSignalPtrs(rts, 6, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 6, 1);
          ssSetInputPortWidth(rts, 6, 1);
        }

        /* port 7 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn2.UPtrs7;
          sfcnUPtrs[0] = &drum_bench_B.y_d;
          ssSetInputPortSignalPtrs(rts, 7, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 7, 1);
          ssSetInputPortWidth(rts, 7, 1);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &drum_bench_M->NonInlinedSFcns.Sfcn2.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 1);
          ssSetOutputPortSignal(rts, 0, ((real_T *) &drum_bench_B.CSfunction3));
        }
      }

      /* path info */
      ssSetModelName(rts, "C++ S-function3");
      ssSetPath(rts, "drum_bench/Behavior Control/C++ S-function3");
      ssSetRTModel(rts,drum_bench_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &drum_bench_M->NonInlinedSFcns.Sfcn2.params;
        ssSetSFcnParamsCount(rts, 1);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)drum_bench_ConstP.pooled5);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &drum_bench_DW.CSfunction3_PWORK[0]);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &drum_bench_M->NonInlinedSFcns.Sfcn2.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &drum_bench_M->NonInlinedSFcns.Sfcn2.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 2);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &drum_bench_DW.CSfunction3_PWORK[0]);
      }

      /* registration */
      EMG(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 4;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetInputPortConnected(rts, 1, 1);
      _ssSetInputPortConnected(rts, 2, 1);
      _ssSetInputPortConnected(rts, 3, 1);
      _ssSetInputPortConnected(rts, 4, 1);
      _ssSetInputPortConnected(rts, 5, 1);
      _ssSetInputPortConnected(rts, 6, 1);
      _ssSetInputPortConnected(rts, 7, 1);
      _ssSetOutputPortConnected(rts, 0, 0);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
      ssSetInputPortBufferDstPort(rts, 1, -1);
      ssSetInputPortBufferDstPort(rts, 2, -1);
      ssSetInputPortBufferDstPort(rts, 3, -1);
      ssSetInputPortBufferDstPort(rts, 4, -1);
      ssSetInputPortBufferDstPort(rts, 5, -1);
      ssSetInputPortBufferDstPort(rts, 6, -1);
      ssSetInputPortBufferDstPort(rts, 7, -1);
    }

    /* Level2 S-Function Block: drum_bench/<S1>/Inlined C++ S-Function (behavior_controller) */
    {
      SimStruct *rts = drum_bench_M->childSfunctions[3];

      /* timing info */
      time_T *sfcnPeriod = drum_bench_M->NonInlinedSFcns.Sfcn3.sfcnPeriod;
      time_T *sfcnOffset = drum_bench_M->NonInlinedSFcns.Sfcn3.sfcnOffset;
      int_T *sfcnTsMap = drum_bench_M->NonInlinedSFcns.Sfcn3.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &drum_bench_M->NonInlinedSFcns.blkInfo2[3]);
      }

      ssSetRTWSfcnInfo(rts, drum_bench_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &drum_bench_M->NonInlinedSFcns.methods2[3]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &drum_bench_M->NonInlinedSFcns.methods3[3]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &drum_bench_M->NonInlinedSFcns.statesInfo2[3]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 9);
        ssSetPortInfoForInputs(rts,
          &drum_bench_M->NonInlinedSFcns.Sfcn3.inputPortInfo[0]);

        /* port 0 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn3.UPtrs0;
          sfcnUPtrs[0] = &drum_bench_B.analog_1_g0;
          ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 1);
        }

        /* port 1 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn3.UPtrs1;
          sfcnUPtrs[0] = &drum_bench_B.analog_2_n;
          ssSetInputPortSignalPtrs(rts, 1, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 1, 1);
          ssSetInputPortWidth(rts, 1, 1);
        }

        /* port 2 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn3.UPtrs2;
          sfcnUPtrs[0] = &drum_bench_B.accelerometer_x_fm;
          ssSetInputPortSignalPtrs(rts, 2, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 2, 1);
          ssSetInputPortWidth(rts, 2, 1);
        }

        /* port 3 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn3.UPtrs3;
          sfcnUPtrs[0] = &drum_bench_B.accelerometer_y_o;
          ssSetInputPortSignalPtrs(rts, 3, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 3, 1);
          ssSetInputPortWidth(rts, 3, 1);
        }

        /* port 4 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn3.UPtrs4;
          sfcnUPtrs[0] = &drum_bench_B.accelerometer_z_c;
          ssSetInputPortSignalPtrs(rts, 4, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 4, 1);
          ssSetInputPortWidth(rts, 4, 1);
        }

        /* port 5 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn3.UPtrs5;
          sfcnUPtrs[0] = &drum_bench_B.MathFunction1_n;
          ssSetInputPortSignalPtrs(rts, 5, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 5, 1);
          ssSetInputPortWidth(rts, 5, 1);
        }

        /* port 6 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn3.UPtrs6;
          sfcnUPtrs[0] = &drum_bench_B.analog_2_f2;
          ssSetInputPortSignalPtrs(rts, 6, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 6, 1);
          ssSetInputPortWidth(rts, 6, 1);
        }

        /* port 7 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn3.UPtrs7;
          sfcnUPtrs[0] = &drum_bench_B.CSfunction1;
          ssSetInputPortSignalPtrs(rts, 7, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 7, 1);
          ssSetInputPortWidth(rts, 7, 1);
        }

        /* port 8 */
        {
          real_T const **sfcnUPtrs = (real_T const **)
            &drum_bench_M->NonInlinedSFcns.Sfcn3.UPtrs8;
          sfcnUPtrs[0] = &drum_bench_B.CSfunction2;
          ssSetInputPortSignalPtrs(rts, 8, (InputPtrsType)&sfcnUPtrs[0]);
          _ssSetInputPortNumDimensions(rts, 8, 1);
          ssSetInputPortWidth(rts, 8, 1);
        }
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &drum_bench_M->NonInlinedSFcns.Sfcn3.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 6);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 1);
          ssSetOutputPortSignal(rts, 0, ((real_T *)
            &drum_bench_B.InlinedCSFunction_o1));
        }

        /* port 1 */
        {
          _ssSetOutputPortNumDimensions(rts, 1, 1);
          ssSetOutputPortWidth(rts, 1, 1);
          ssSetOutputPortSignal(rts, 1, ((real_T *)
            &drum_bench_B.InlinedCSFunction_o2));
        }

        /* port 2 */
        {
          _ssSetOutputPortNumDimensions(rts, 2, 1);
          ssSetOutputPortWidth(rts, 2, 1);
          ssSetOutputPortSignal(rts, 2, ((real_T *)
            &drum_bench_B.InlinedCSFunction_o3));
        }

        /* port 3 */
        {
          _ssSetOutputPortNumDimensions(rts, 3, 1);
          ssSetOutputPortWidth(rts, 3, 1);
          ssSetOutputPortSignal(rts, 3, ((real_T *)
            &drum_bench_B.InlinedCSFunction_o4));
        }

        /* port 4 */
        {
          _ssSetOutputPortNumDimensions(rts, 4, 1);
          ssSetOutputPortWidth(rts, 4, 1);
          ssSetOutputPortSignal(rts, 4, ((real_T *)
            &drum_bench_B.InlinedCSFunction_o5));
        }

        /* port 5 */
        {
          _ssSetOutputPortNumDimensions(rts, 5, 1);
          ssSetOutputPortWidth(rts, 5, 1);
          ssSetOutputPortSignal(rts, 5, ((real_T *)
            &drum_bench_B.InlinedCSFunction_o6));
        }
      }

      /* path info */
      ssSetModelName(rts, "Inlined C++\nS-Function");
      ssSetPath(rts, "drum_bench/Behavior Control/Inlined C++ S-Function");
      ssSetRTModel(rts,drum_bench_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &drum_bench_M->NonInlinedSFcns.Sfcn3.params;
        ssSetSFcnParamsCount(rts, 1);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)drum_bench_ConstP.pooled5);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &drum_bench_DW.InlinedCSFunction_PWORK[0]);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &drum_bench_M->NonInlinedSFcns.Sfcn3.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &drum_bench_M->NonInlinedSFcns.Sfcn3.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 4);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &drum_bench_DW.InlinedCSFunction_PWORK[0]);
      }

      /* registration */
      behavior_controller(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.001);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 3;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);
      _ssSetInputPortConnected(rts, 1, 1);
      _ssSetInputPortConnected(rts, 2, 1);
      _ssSetInputPortConnected(rts, 3, 1);
      _ssSetInputPortConnected(rts, 4, 1);
      _ssSetInputPortConnected(rts, 5, 1);
      _ssSetInputPortConnected(rts, 6, 1);
      _ssSetInputPortConnected(rts, 7, 1);
      _ssSetInputPortConnected(rts, 8, 1);
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 1, 1);
      _ssSetOutputPortConnected(rts, 2, 1);
      _ssSetOutputPortConnected(rts, 3, 1);
      _ssSetOutputPortConnected(rts, 4, 1);
      _ssSetOutputPortConnected(rts, 5, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);
      _ssSetOutputPortBeingMerged(rts, 1, 0);
      _ssSetOutputPortBeingMerged(rts, 2, 0);
      _ssSetOutputPortBeingMerged(rts, 3, 0);
      _ssSetOutputPortBeingMerged(rts, 4, 0);
      _ssSetOutputPortBeingMerged(rts, 5, 0);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
      ssSetInputPortBufferDstPort(rts, 1, -1);
      ssSetInputPortBufferDstPort(rts, 2, -1);
      ssSetInputPortBufferDstPort(rts, 3, -1);
      ssSetInputPortBufferDstPort(rts, 4, -1);
      ssSetInputPortBufferDstPort(rts, 5, -1);
      ssSetInputPortBufferDstPort(rts, 6, -1);
      ssSetInputPortBufferDstPort(rts, 7, -1);
      ssSetInputPortBufferDstPort(rts, 8, -1);
    }
  }

  /* Initialize Sizes */
  drum_bench_M->Sizes.numContStates = (0);/* Number of continuous states */
  drum_bench_M->Sizes.numY = (0);      /* Number of model outputs */
  drum_bench_M->Sizes.numU = (0);      /* Number of model inputs */
  drum_bench_M->Sizes.sysDirFeedThru = (0);/* The model is not direct feedthrough */
  drum_bench_M->Sizes.numSampTimes = (6);/* Number of sample times */
  drum_bench_M->Sizes.numBlocks = (1157);/* Number of blocks */
  drum_bench_M->Sizes.numBlockIO = (799);/* Number of block outputs */
  drum_bench_M->Sizes.numBlockPrms = (1);/* Sum of parameter "widths" */
  return drum_bench_M;
}

/*========================================================================*
 * End of Classic call interface                                          *
 *========================================================================*/
