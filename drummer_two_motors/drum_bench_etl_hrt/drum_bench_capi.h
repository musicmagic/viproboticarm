/*
 * drum_bench_capi.h
 *
 * Code generation for model "drum_bench".
 *
 * Model version              : 1.526
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Mon Jul 21 13:57:13 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef _RTW_HEADER_drum_bench_capi_h_
#define _RTW_HEADER_drum_bench_capi_h_
#include "drum_bench.h"

extern void drum_bench_InitializeDataMapInfo(RT_MODEL_drum_bench_T *const
  drum_bench_M
  );

#endif                                 /* _RTW_HEADER_drum_bench_capi_h_ */

/* EOF: drum_bench_capi.h */
