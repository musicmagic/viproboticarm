%implements s33_b0_f1knk_Functions "C"

%function rt_powd_snf_Fcn0(block) void
%assign fcnBuff = ""
%openfile fcnBuff
if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = %<LibGetMathConstant(LibRealNonFinite("nan"), FcnGetDataTypeIdFromName("double"))>;
} else {
    tmp = fabs(u0);
    tmp_0 = fabs(u1);
    if (rtIsInf(u1)) {
        if (tmp == 1.0) {
            y = %<LibGetMathConstant(LibRealNonFinite("nan"), FcnGetDataTypeIdFromName("double"))>;
        } else if (tmp > 1.0) {
            if (u1 > 0.0) {
                y = %<LibGetMathConstant(LibRealNonFinite("inf"), FcnGetDataTypeIdFromName("double"))>;
            } else {
                y = 0.0;
            }
        } else if (u1 > 0.0) {
            y = 0.0;
        } else {
            y = %<LibGetMathConstant(LibRealNonFinite("inf"), FcnGetDataTypeIdFromName("double"))>;
        }
    } else if (tmp_0 == 0.0) {
        y = 1.0;
    } else if (tmp_0 == 1.0) {
        if (u1 > 0.0) {
            y = u0;
        } else {
            y = 1.0 / u0;
        }
    } else if (u1 == 2.0) {
        y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
        y = sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > floor(u1))) {
        y = %<LibGetMathConstant(LibRealNonFinite("nan"), FcnGetDataTypeIdFromName("double"))>;
    } else {
        y = pow(u0, u1);
    }
}
return y;
%closefile fcnBuff
%assign fcnProtoType = ""
%openfile fcnProtoType
extern real_T rt_powd_snf(\
%assign comma = ""
%<comma>real_T u0\
%assign comma = ", "
%<comma>real_T u1\
%assign comma = ", "
%closefile fcnProtoType
%<SLibCG_SetFcnProtoType(11, 0, fcnProtoType)>
%openfile localVarsBuf
real_T y;
real_T tmp;
real_T tmp_0;
%closefile localVarsBuf
%return localVarsBuf + fcnBuff
%endfunction

%function MWDSPCG_FFT_Interleave_R2BR_D_Fcn1(block) void
%assign fcnBuff = ""
%openfile fcnBuff
/* S-Function (sdspfft2): '<S32>/FFT' */
/* Bit-reverses the input data simultaneously with the interleaving operation, 
  obviating the need for explicit data reordering later.  This requires an 
  FFT with bit-reversed inputs. 
 */
br_j = 0;
yIdx = 0;
uIdx = 0;
for (nChansBy = nChans >> 1; nChansBy != 0; nChansBy--) {
    for (j = nRows; j - 1 > 0; j--) {
        y[yIdx + br_j].re = x[uIdx];
        y[yIdx + br_j].im = x[uIdx + nRows];
        uIdx++;
        /* Compute next bit-reversed destination index */
        bit_fftLen = nRows;
        do {
            bit_fftLen = (int32_T)((uint32_T)bit_fftLen >> 1);
            br_j ^= bit_fftLen;
        } while (!((br_j & bit_fftLen) != 0));
    }
    y[yIdx + br_j].re = x[uIdx];
    y[yIdx + br_j].im = x[uIdx + nRows];
    uIdx = (uIdx + nRows) + 1;
    yIdx += nRows << 1;
    br_j = 0;
}
/* For an odd number of channels, prepare the last channel 
 for a double-length real signal algorithm.  No actual 
 interleaving is required, just a copy of the last column 
 of real data, but now placed in bit-reversed order. 
 We need to cast the real u pointer to a cDType_T pointer, 
 in order to fake the interleaving, and cut the number 
 of elements in half (half as many complex interleaved 
 elements as compared to real non-interleaved elements). 
 */
if ((nChans & 1) != 0) {
    for (j = nRows >> 1; j - 1 > 0; j--) {
        y[yIdx + br_j].re = x[uIdx];
        y[yIdx + br_j].im = x[uIdx + 1];
        uIdx += 2;
        /* Compute next bit-reversed destination index */
        bit_fftLen = nRows >> 1;
        do {
            bit_fftLen = (int32_T)((uint32_T)bit_fftLen >> 1);
            br_j ^= bit_fftLen;
        } while (!((br_j & bit_fftLen) != 0));
    }
    y[yIdx + br_j].re = x[uIdx];
    y[yIdx + br_j].im = x[uIdx + 1];
}
/* End of S-Function (sdspfft2): '<S32>/FFT' */
%closefile fcnBuff
%assign fcnProtoType = ""
%openfile fcnProtoType
extern void MWDSPCG_FFT_Interleave_R2BR_D(\
%assign comma = ""
%<comma>const real_T x[]\
%assign comma = ", "
%<comma>creal_T y[]\
%assign comma = ", "
%<comma>const int32_T nChans\
%assign comma = ", "
%<comma>const int32_T nRows\
%assign comma = ", "
%closefile fcnProtoType
%<SLibCG_SetFcnProtoType(11, 1, fcnProtoType)>
%openfile localVarsBuf
int32_T br_j;
int32_T yIdx;
int32_T uIdx;
int32_T j;
int32_T nChansBy;
int32_T bit_fftLen;
%closefile localVarsBuf
%return localVarsBuf + fcnBuff
%endfunction

%function MWDSPCG_R2DIT_TBLS_Z_Fcn2(block) void
%assign fcnBuff = ""
%openfile fcnBuff
/* S-Function (sdspfft2): '<S32>/FFT' */
/* DSP System Toolbox Decimation in Time FFT  */
/* Computation performed using table lookup  */
/* Output type: complex real_T */
nHalf = fftLen >> 1;
nHalf *= twiddleStep;
nQtr = nHalf >> 1;
fwdInvFactor = isInverse ? -1 : 1;
/* For each channel */
offsetCh = offset;
for (iCh = 0; iCh < nChans; iCh++) {
    /* Perform butterflies for the first stage, where no multiply is required. */
    for (ix = offsetCh; ix < (fftLen + offsetCh) - 1; ix += 2) {
        i_0 = ix + 1;
        tmp_re = y[i_0].re;
        tmp_im = y[i_0].im;
        y[i_0].re = y[ix].re - tmp_re;
        y[i_0].im = y[ix].im - tmp_im;
        y[ix].re += tmp_re;
        y[ix].im += tmp_im;
    }
    idelta = 2;
    k = fftLen >> 2;
    kratio = k * twiddleStep;
    while (k > 0) {
        istart = offsetCh;
        i = istart;
        /* Perform the first butterfly in each remaining stage, where no multiply is required. */
        for (ix = 0; ix < k; ix++) {
            i_0 = i + idelta;
            tmp_re = y[i_0].re;
            tmp_im = y[i_0].im;
            y[i_0].re = y[i].re - tmp_re;
            y[i_0].im = y[i].im - tmp_im;
            y[i].re += tmp_re;
            y[i].im += tmp_im;
            i += idelta << 1;
        }
        /* Perform remaining butterflies */
        for (j = kratio; j < nHalf; j += kratio) {
            i = istart + 1;
            twidRe = tablePtr[j];
            twidIm = tablePtr[j + nQtr] * (real_T)fwdInvFactor;
            for (ix = 0; ix < k; ix++) {
                i_0 = i + idelta;
                tmp_re = y[i_0].re * twidRe - y[i_0].im * twidIm;
                tmp_im = y[i_0].re * twidIm + y[i_0].im * twidRe;
                y[i_0].re = y[i].re - tmp_re;
                y[i_0].im = y[i].im - tmp_im;
                y[i].re += tmp_re;
                y[i].im += tmp_im;
                i += idelta << 1;
            }
            istart++;
        }
        idelta <<= 1;
        k >>= 1;
        kratio >>= 1;
    }
    /* Point to next channel */
    offsetCh += nRows;
}
/* End of S-Function (sdspfft2): '<S32>/FFT' */
%closefile fcnBuff
%assign fcnProtoType = ""
%openfile fcnProtoType
extern void MWDSPCG_R2DIT_TBLS_Z(\
%assign comma = ""
%<comma>creal_T y[]\
%assign comma = ", "
%<comma>const int32_T nChans\
%assign comma = ", "
%<comma>const int32_T nRows\
%assign comma = ", "
%<comma>const int32_T fftLen\
%assign comma = ", "
%<comma>const int32_T offset\
%assign comma = ", "
%<comma>const real_T tablePtr[]\
%assign comma = ", "
%<comma>const int32_T twiddleStep\
%assign comma = ", "
%<comma>const boolean_T isInverse\
%assign comma = ", "
%closefile fcnProtoType
%<SLibCG_SetFcnProtoType(11, 2, fcnProtoType)>
%openfile localVarsBuf
int32_T nHalf;
real_T twidRe;
real_T twidIm;
int32_T nQtr;
int32_T fwdInvFactor;
int32_T iCh;
int32_T offsetCh;
int32_T idelta;
int32_T ix;
int32_T k;
int32_T kratio;
int32_T istart;
int32_T i;
int32_T j;
int32_T i_0;
real_T tmp_re;
real_T tmp_im;
%closefile localVarsBuf
%return localVarsBuf + fcnBuff
%endfunction

%function MWDSPCG_FFT_DblLen_Z_Fcn3(block) void
%assign fcnBuff = ""
%openfile fcnBuff
/* S-Function (sdspfft2): '<S32>/FFT' */
/* In-place "double-length" data recovery 
  Table-based mem-optimized twiddle computation 
  
  Used to recover linear-ordered length-N point complex FFT result 
  from a linear-ordered complex length-N/2 point FFT, performed 
  on N interleaved real values. 
 */
N = nRows >> 1;
N_0 = N >> 1;
W = N_0 * twiddleStep;
yIdx = (nChans - 1) * nRows;
if (nRows > 2) {
    temp2Re = y[N_0 + yIdx].re;
    tempOut0Im = y[N_0 + yIdx].im;
    y[(N + N_0) + yIdx].re = temp2Re;
    y[(N + N_0) + yIdx].im = tempOut0Im;
    y[N_0 + yIdx].re = temp2Re;
    y[N_0 + yIdx].im = -tempOut0Im;
}
if (nRows > 1) {
    accRe = y[yIdx].re;
    accRe -= y[yIdx].im;
    y[N + yIdx].re = accRe;
    y[N + yIdx].im = 0.0;
}
accRe = y[yIdx].re;
accRe += y[yIdx].im;
y[yIdx].re = accRe;
y[yIdx].im = 0.0;
k = twiddleStep;
for (ix = 1; ix < N_0; ix++) {
    accRe = y[ix + yIdx].re;
    accRe += y[(N - ix) + yIdx].re;
    accRe /= 2.0;
    temp2Re = accRe;
    accRe = y[ix + yIdx].im;
    accRe -= y[(N - ix) + yIdx].im;
    accRe /= 2.0;
    tempOut0Im = accRe;
    accRe = y[ix + yIdx].im;
    accRe += y[(N - ix) + yIdx].im;
    accRe /= 2.0;
    tempOut1Re = accRe;
    accRe = y[(N - ix) + yIdx].re;
    accRe -= y[ix + yIdx].re;
    accRe /= 2.0;
    y[ix + yIdx].re = tempOut1Re;
    y[ix + yIdx].im = accRe;
    cTemp_re = y[ix + yIdx].re * twiddleTable[k] - -twiddleTable[W - k] * y[ix + yIdx].im;
    accRe = y[ix + yIdx].im * twiddleTable[k] + -twiddleTable[W - k] * y[ix + yIdx].re;
    tempOut1Re = cTemp_re;
    tempOut1Im = accRe;
    accRe = temp2Re;
    cTemp_re = tempOut0Im;
    accRe += tempOut1Re;
    cTemp_re += tempOut1Im;
    y[ix + yIdx].re = accRe;
    y[ix + yIdx].im = cTemp_re;
    cTemp_re = y[ix + yIdx].re;
    accRe = -y[ix + yIdx].im;
    y[(nRows - ix) + yIdx].re = cTemp_re;
    y[(nRows - ix) + yIdx].im = accRe;
    accRe = temp2Re;
    cTemp_re = tempOut0Im;
    accRe -= tempOut1Re;
    cTemp_re -= tempOut1Im;
    y[(N + ix) + yIdx].re = accRe;
    y[(N + ix) + yIdx].im = cTemp_re;
    accRe = y[(N + ix) + yIdx].re;
    cTemp_re = y[(N + ix) + yIdx].im;
    y[(N - ix) + yIdx].re = accRe;
    y[(N - ix) + yIdx].im = -cTemp_re;
    k += twiddleStep;
}
/* End of S-Function (sdspfft2): '<S32>/FFT' */
%closefile fcnBuff
%assign fcnProtoType = ""
%openfile fcnProtoType
extern void MWDSPCG_FFT_DblLen_Z(\
%assign comma = ""
%<comma>creal_T y[]\
%assign comma = ", "
%<comma>const int32_T nChans\
%assign comma = ", "
%<comma>const int32_T nRows\
%assign comma = ", "
%<comma>const real_T twiddleTable[]\
%assign comma = ", "
%<comma>const int32_T twiddleStep\
%assign comma = ", "
%closefile fcnProtoType
%<SLibCG_SetFcnProtoType(11, 3, fcnProtoType)>
%openfile localVarsBuf
real_T accRe;
real_T tempOut0Im;
real_T tempOut1Re;
real_T tempOut1Im;
real_T temp2Re;
int32_T N;
int32_T N_0;
int32_T W;
int32_T yIdx;
int32_T ix;
int32_T k;
real_T cTemp_re;
%closefile localVarsBuf
%return localVarsBuf + fcnBuff
%endfunction

