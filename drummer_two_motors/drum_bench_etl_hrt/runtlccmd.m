function runtlccmd
% RUNTLCCMD - run tlc command (regenerate C code from .rtw file) for model drum_bench
% This function will run the tlc command stored in the variable 
% "tlccmd" in tlccmd.mat, whose contents is as follows:
% 
% 	tlc
% 	-r
% 	/home/meka/meka/rsc-drummer/drummer_two_motors/drum_bench_etl_hrt/drum_bench.rtw
% 	/opt/etherlab/rtw/etherlab/etherlab.tlc
% 	-O/home/meka/meka/rsc-drummer/drummer_two_motors/drum_bench_etl_hrt
% 	-I/opt/etherlab/rtw/etherlab
% 	-I/home/meka/meka/rsc-base/simulink/library/c
% 	-I/opt/etherlab/rtw/blocks/EtherCAT
% 	-I/usr/local/MATLAB/R2013b/toolbox/simulink/fixedandfloat/tlc_c
% 	-I/usr/local/MATLAB/R2013b/toolbox/target/foundation/blks/mex/tlc_c
% 	-I/home/meka/meka/rsc-drummer/drummer_two_motors/drum_bench_etl_hrt/tlc
% 	-I/usr/local/MATLAB/R2013b/rtw/c/tlc/mw
% 	-I/usr/local/MATLAB/R2013b/rtw/c/tlc/lib
% 	-I/usr/local/MATLAB/R2013b/rtw/c/tlc/blocks
% 	-I/usr/local/MATLAB/R2013b/rtw/c/tlc/fixpt
% 	-I/usr/local/MATLAB/R2013b/stateflow/c/tlc
% 	-aFoldNonRolledExpr=1
% 	-aInlineInvariantSignals=0
% 	-aInlineParameters=1
% 	-aLocalBlockOutputs=0
% 	-aRollThreshold=5
% 	-aForceBlockIOInitOptimize=0
% 	-aGenerateReport=0
% 	-aGenCodeOnly=0
% 	-aRTWVerbose=1
% 	-aIncludeHyperlinkInReport=0
% 	-aLaunchReport=0
% 	-aGenerateTraceInfo=0
% 	-aForceParamTrailComments=0
% 	-aGenerateComments=1
% 	-aIgnoreCustomStorageClasses=1
% 	-aIncHierarchyInIds=0
% 	-aMaxRTWIdLen=31
% 	-aShowEliminatedStatements=0
% 	-aIncDataTypeInIds=0
% 	-aInsertBlockDesc=0
% 	-aIgnoreTestpoints=0
% 	-aSimulinkBlockComments=1
% 	-aInlinedPrmAccess="Literals"
% 	-aTargetFcnLib="ansi_tfl_table_tmw.mat"
% 	-aIsPILTarget=0
% 	-aLogVarNameModifier="rt_"
% 	-aGenerateFullHeader=1
% 	-aExtMode=0
% 	-aExtModeStaticAlloc=0
% 	-aExtModeTesting=0
% 	-aExtModeStaticAllocSize=1000000
% 	-aExtModeTransport=0
% 	-aRTWCAPISignals=1
% 	-aRTWCAPIParams=1
% 	-aGenerateASAP2=0
% 	-aMatFileLogging=0
% 	-aBufferTime=2
% 	-aOverrunCount=1
% 	-aStackSize=2000
% 	-aVerboseBuild=0
% 	-aEtherLabDir="/opt/etherlab"
% 	-aModulePayload="untitled.mdl "
% 	-aGenerateTraceInfo=0
% 	-aIgnoreTestpoints=0
% 	-aProtectedModelReferenceTarget=0
% 	-p10000

   disp('This function will be obsoleted in a future release.') 
   mdl = 'drum_bench';

   sysopen = ~isempty(strmatch(mdl, find_system('type', 'block_diagram'), 'exact'));

   if ~sysopen

      disp([mfilename ': Error: model ' mdl ' is not open. Please open model ' mdl ' and then run ' mfilename ' again.']);

   else

      rtwprivate('rtwattic', 'setBuildDir', '/home/meka/meka/rsc-drummer/drummer_two_motors/drum_bench_etl_hrt');
      rtwprivate('ec_set_replacement_flag', 'drum_bench');
      load tlccmd.mat;
      savedpwd = pwd;
      cd ..;
      coder.internal.ModelCodegenMgr.setInstance(modelCodegenMgr);
      tlccmd{end+1} = '-aSLCGUseRTWContext=0';
      feval(tlccmd{:});
      coder.internal.ModelCodegenMgr.setInstance([]);
      rtwprivate rtwattic clean;
      cd(savedpwd);

   end
