#ifndef RTW_HEADER_struct_jkDUSheYYfvtbbR4tr6mvD_h_
#define RTW_HEADER_struct_jkDUSheYYfvtbbR4tr6mvD_h_
#include "rtwtypes.h"

typedef struct {
  real_T amplitude;
  real_T bias;
  real_T frequency;
  real_T select_input;
  real_T slew_rate;
} struct_jkDUSheYYfvtbbR4tr6mvD;

#endif                                 /* RTW_HEADER_struct_jkDUSheYYfvtbbR4tr6mvD_h_ */
