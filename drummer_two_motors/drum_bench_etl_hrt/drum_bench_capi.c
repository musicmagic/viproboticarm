/*
 * drum_bench_capi.c
 *
 * Code generation for model "drum_bench".
 *
 * Model version              : 1.526
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Mon Jul 21 13:57:13 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "rtw_capi.h"
#ifdef HOST_CAPI_BUILD
#include "drum_bench_capi_host.h"
#define sizeof(s)                      ((size_t)(0xFFFF))
#undef rt_offsetof
#define rt_offsetof(s,el)              ((uint16_T)(0xFFFF))
#define TARGET_CONST
#define TARGET_STRING(s)               (s)
#else                                  /* HOST_CAPI_BUILD */
#include "drum_bench.h"
#include "drum_bench_private.h"
#ifdef LIGHT_WEIGHT_CAPI
#define TARGET_CONST
#define TARGET_STRING(s)               (NULL)
#else
#define TARGET_CONST                   const
#define TARGET_STRING(s)               (s)
#endif
#endif                                 /* HOST_CAPI_BUILD */

/* Block output signal information */
static const rtwCAPI_Signals rtBlockSignals[] = {
  /* addrMapIndex, sysNum, blockPath,
   * signalName, portNumber, dataTypeIndex, dimIndex, fxpIndex, sTimeIndex
   */
  { 0, 0, TARGET_STRING("drum_bench/Bus Selector"),
    TARGET_STRING("mode"), 0, 0, 0, 0, 0 },

  { 1, 0, TARGET_STRING("drum_bench/Bus Selector"),
    TARGET_STRING("motor_torque"), 1, 0, 0, 0, 0 },

  { 2, 0, TARGET_STRING("drum_bench/Bus Selector"),
    TARGET_STRING("motor_iq"), 2, 0, 0, 0, 0 },

  { 3, 0, TARGET_STRING("drum_bench/Bus Selector1"),
    TARGET_STRING("mode"), 0, 0, 0, 0, 0 },

  { 4, 0, TARGET_STRING("drum_bench/Bus Selector1"),
    TARGET_STRING("motor_torque"), 1, 0, 0, 0, 0 },

  { 5, 0, TARGET_STRING("drum_bench/Bus Selector1"),
    TARGET_STRING("motor_iq"), 2, 0, 0, 0, 0 },

  { 6, 0, TARGET_STRING("drum_bench/mode select"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 7, 0, TARGET_STRING("drum_bench/mode select1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 8, 0, TARGET_STRING("drum_bench/mode select2"),
    TARGET_STRING(""), 0, 0, 0, 0, 1 },

  { 9, 0, TARGET_STRING("drum_bench/mode0"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 10, 0, TARGET_STRING("drum_bench/mode1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 11, 0, TARGET_STRING("drum_bench/motor_iq0"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 12, 0, TARGET_STRING("drum_bench/motor_iq1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 13, 0, TARGET_STRING("drum_bench/motor_torque0"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 14, 0, TARGET_STRING("drum_bench/motor_torque1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 15, 0, TARGET_STRING("drum_bench/mode switch"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 16, 0, TARGET_STRING("drum_bench/mode switch1"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 17, 0, TARGET_STRING("drum_bench/Domain State"),
    TARGET_STRING(""), 0, 2, 0, 0, 2 },

  { 18, 0, TARGET_STRING("drum_bench/Domain State"),
    TARGET_STRING(""), 1, 3, 0, 0, 2 },

  { 19, 0, TARGET_STRING("drum_bench/Master State"),
    TARGET_STRING(""), 0, 2, 0, 0, 2 },

  { 20, 0, TARGET_STRING("drum_bench/Master State"),
    TARGET_STRING(""), 1, 3, 0, 0, 2 },

  { 21, 0, TARGET_STRING("drum_bench/Master State"),
    TARGET_STRING(""), 2, 4, 0, 0, 2 },

  { 22, 0, TARGET_STRING("drum_bench/Behavior Control/Bus Selector"),
    TARGET_STRING("analog_1"), 0, 0, 0, 0, 3 },

  { 23, 0, TARGET_STRING("drum_bench/Behavior Control/Bus Selector"),
    TARGET_STRING("analog_2"), 1, 0, 0, 0, 3 },

  { 24, 0, TARGET_STRING("drum_bench/Behavior Control/Bus Selector1"),
    TARGET_STRING("analog_1"), 0, 0, 0, 0, 3 },

  { 25, 0, TARGET_STRING("drum_bench/Behavior Control/Bus Selector1"),
    TARGET_STRING("analog_2"), 1, 0, 0, 0, 3 },

  { 26, 0, TARGET_STRING("drum_bench/Behavior Control/Bus Selector1"),
    TARGET_STRING("accelerometer_x"), 2, 0, 0, 0, 3 },

  { 27, 0, TARGET_STRING("drum_bench/Behavior Control/Bus Selector1"),
    TARGET_STRING("accelerometer_y"), 3, 0, 0, 0, 3 },

  { 28, 0, TARGET_STRING("drum_bench/Behavior Control/Bus Selector1"),
    TARGET_STRING("accelerometer_z"), 4, 0, 0, 0, 3 },

  { 29, 0, TARGET_STRING("drum_bench/Behavior Control/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 4 },

  { 30, 0, TARGET_STRING("drum_bench/Behavior Control/Data Type Conversion1"),
    TARGET_STRING(""), 0, 0, 0, 0, 4 },

  { 31, 0, TARGET_STRING("drum_bench/Behavior Control/C++ S-function1"),
    TARGET_STRING(""), 0, 0, 0, 0, 4 },

  { 32, 0, TARGET_STRING("drum_bench/Behavior Control/C++ S-function2"),
    TARGET_STRING(""), 0, 0, 0, 0, 4 },

  { 33, 0, TARGET_STRING("drum_bench/Behavior Control/C++ S-function3"),
    TARGET_STRING(""), 0, 0, 0, 0, 5 },

  { 34, 0, TARGET_STRING("drum_bench/Behavior Control/Inlined C++ S-Function"),
    TARGET_STRING(""), 0, 0, 0, 0, 4 },

  { 35, 0, TARGET_STRING("drum_bench/Behavior Control/Inlined C++ S-Function"),
    TARGET_STRING(""), 1, 0, 0, 0, 4 },

  { 36, 0, TARGET_STRING("drum_bench/Behavior Control/Inlined C++ S-Function"),
    TARGET_STRING(""), 2, 0, 0, 0, 4 },

  { 37, 0, TARGET_STRING("drum_bench/Behavior Control/Inlined C++ S-Function"),
    TARGET_STRING(""), 3, 0, 0, 0, 4 },

  { 38, 0, TARGET_STRING("drum_bench/Behavior Control/Inlined C++ S-Function"),
    TARGET_STRING(""), 4, 0, 0, 0, 4 },

  { 39, 0, TARGET_STRING("drum_bench/Behavior Control/Inlined C++ S-Function"),
    TARGET_STRING(""), 5, 0, 0, 0, 4 },

  { 40, 21, TARGET_STRING("drum_bench/impedence_controller/Filtered Derivative"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 41, 0, TARGET_STRING("drum_bench/impedence_controller/Bus Assignment"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 42, 0, TARGET_STRING("drum_bench/impedence_controller/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 43, 0, TARGET_STRING("drum_bench/impedence_controller/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 44, 0, TARGET_STRING("drum_bench/impedence_controller/Bus Selector3"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 3 },

  { 45, 0, TARGET_STRING("drum_bench/impedence_controller/motor_torque"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 46, 0, TARGET_STRING("drum_bench/impedence_controller/kd"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 47, 0, TARGET_STRING("drum_bench/impedence_controller/kp"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 48, 0, TARGET_STRING("drum_bench/impedence_controller/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 49, 0, TARGET_STRING("drum_bench/impedence_controller/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 50, 0, TARGET_STRING("drum_bench/impedence_controller/Saturation"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 51, 0, TARGET_STRING("drum_bench/impedence_controller/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 52, 0, TARGET_STRING("drum_bench/impedence_controller/Sum1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 53, 0, TARGET_STRING("drum_bench/impedence_controller/Sum2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 54, 22, TARGET_STRING("drum_bench/impedence_controller1/Filtered Derivative"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 55, 0, TARGET_STRING("drum_bench/impedence_controller1/Bus Assignment"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 56, 0, TARGET_STRING("drum_bench/impedence_controller1/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 57, 0, TARGET_STRING("drum_bench/impedence_controller1/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 58, 0, TARGET_STRING("drum_bench/impedence_controller1/Bus Selector3"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 3 },

  { 59, 0, TARGET_STRING("drum_bench/impedence_controller1/motor_torque"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 60, 0, TARGET_STRING("drum_bench/impedence_controller1/kd"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 61, 0, TARGET_STRING("drum_bench/impedence_controller1/kp"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 62, 0, TARGET_STRING("drum_bench/impedence_controller1/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 63, 0, TARGET_STRING("drum_bench/impedence_controller1/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 64, 0, TARGET_STRING("drum_bench/impedence_controller1/Saturation"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 65, 0, TARGET_STRING("drum_bench/impedence_controller1/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 66, 0, TARGET_STRING("drum_bench/impedence_controller1/Sum1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 67, 0, TARGET_STRING("drum_bench/impedence_controller1/Sum2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 68, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/Bus Creator"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 69, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/Bus Creator"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 70, 0, TARGET_STRING("drum_bench/marlin_1.1_full status/Bus Selector"),
    TARGET_STRING("marlin_basic_status"), 0, 5, 0, 0, 3 },

  { 71, 0, TARGET_STRING("drum_bench/marlin_1.1_full status/Bus Selector"),
    TARGET_STRING("temperature_motor_winding"), 1, 0, 0, 0, 3 },

  { 72, 0, TARGET_STRING("drum_bench/marlin_1.1_full status/Bus Selector"),
    TARGET_STRING("temperature_motor_housing"), 2, 0, 0, 0, 3 },

  { 73, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/temperature_motor_housing"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 74, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/temperature_motor_winding"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 75, 0, TARGET_STRING("drum_bench/marlin_1.1_full status1/Bus Selector"),
    TARGET_STRING("marlin_basic_status"), 0, 5, 0, 0, 3 },

  { 76, 0, TARGET_STRING("drum_bench/marlin_1.1_full status1/Bus Selector"),
    TARGET_STRING("temperature_motor_winding"), 1, 0, 0, 0, 3 },

  { 77, 0, TARGET_STRING("drum_bench/marlin_1.1_full status1/Bus Selector"),
    TARGET_STRING("temperature_motor_housing"), 2, 0, 0, 0, 3 },

  { 78, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/temperature_motor_housing"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 79, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/temperature_motor_winding"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 80, 0, TARGET_STRING("drum_bench/marlin_1.1_full1/Bus Creator"),
    TARGET_STRING(""), 0, 6, 0, 0, 3 },

  { 81, 0, TARGET_STRING("drum_bench/marlin_1.1_full1/Bus Selector"),
    TARGET_STRING("rpc"), 0, 0, 0, 0, 0 },

  { 82, 0, TARGET_STRING("drum_bench/marlin_1.1_full2/Bus Creator"),
    TARGET_STRING(""), 0, 6, 0, 0, 3 },

  { 83, 0, TARGET_STRING("drum_bench/marlin_1.1_full2/Bus Selector"),
    TARGET_STRING("rpc"), 0, 0, 0, 0, 0 },

  { 84, 0, TARGET_STRING("drum_bench/mason process/Bus Creator"),
    TARGET_STRING(""), 0, 6, 0, 0, 3 },

  { 85, 0, TARGET_STRING("drum_bench/mason process/Bus Selector1"),
    TARGET_STRING("marlin_basic_status"), 0, 5, 0, 0, 3 },

  { 86, 0, TARGET_STRING("drum_bench/mason process/Bus Selector1"),
    TARGET_STRING("temperature_motor_winding"), 1, 0, 0, 0, 3 },

  { 87, 0, TARGET_STRING("drum_bench/mason process/Bus Selector1"),
    TARGET_STRING("temperature_motor_housing"), 2, 0, 0, 0, 3 },

  { 88, 0, TARGET_STRING("drum_bench/mason process1/Bus Creator"),
    TARGET_STRING(""), 0, 6, 0, 0, 3 },

  { 89, 0, TARGET_STRING("drum_bench/mason process1/Bus Selector1"),
    TARGET_STRING("marlin_basic_status"), 0, 5, 0, 0, 3 },

  { 90, 0, TARGET_STRING("drum_bench/mason process1/Bus Selector1"),
    TARGET_STRING("temperature_motor_winding"), 1, 0, 0, 0, 3 },

  { 91, 0, TARGET_STRING("drum_bench/mason process1/Bus Selector1"),
    TARGET_STRING("temperature_motor_housing"), 2, 0, 0, 0, 3 },

  { 92, 31, TARGET_STRING(
    "drum_bench/var_impedence_controller/Filtered Derivative"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 93, 0, TARGET_STRING("drum_bench/var_impedence_controller/bias"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 94, 0, TARGET_STRING("drum_bench/var_impedence_controller/Bus Assignment"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 95, 0, TARGET_STRING("drum_bench/var_impedence_controller/Bus Selector1"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 3 },

  { 96, 0, TARGET_STRING("drum_bench/var_impedence_controller/Bus Selector1"),
    TARGET_STRING("analog_1"), 1, 0, 0, 0, 3 },

  { 97, 0, TARGET_STRING("drum_bench/var_impedence_controller/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 98, 0, TARGET_STRING("drum_bench/var_impedence_controller/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 99, 0, TARGET_STRING("drum_bench/var_impedence_controller/ka"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 100, 0, TARGET_STRING("drum_bench/var_impedence_controller/kd"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 101, 0, TARGET_STRING("drum_bench/var_impedence_controller/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 102, 0, TARGET_STRING("drum_bench/var_impedence_controller/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 103, 0, TARGET_STRING("drum_bench/var_impedence_controller/Sum1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 104, 0, TARGET_STRING("drum_bench/var_impedence_controller/Sum2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 105, 32, TARGET_STRING(
    "drum_bench/var_impedence_controller1/Filtered Derivative"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 106, 0, TARGET_STRING("drum_bench/var_impedence_controller1/bias"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 107, 0, TARGET_STRING("drum_bench/var_impedence_controller1/Bus Assignment"),
    TARGET_STRING(""), 0, 1, 0, 0, 0 },

  { 108, 0, TARGET_STRING("drum_bench/var_impedence_controller1/Bus Selector1"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 3 },

  { 109, 0, TARGET_STRING("drum_bench/var_impedence_controller1/Bus Selector1"),
    TARGET_STRING("analog_1"), 1, 0, 0, 0, 3 },

  { 110, 0, TARGET_STRING("drum_bench/var_impedence_controller1/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 111, 0, TARGET_STRING("drum_bench/var_impedence_controller1/Bus Selector2"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 112, 0, TARGET_STRING("drum_bench/var_impedence_controller1/ka"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 113, 0, TARGET_STRING("drum_bench/var_impedence_controller1/kd"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 114, 0, TARGET_STRING("drum_bench/var_impedence_controller1/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 115, 0, TARGET_STRING("drum_bench/var_impedence_controller1/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 116, 0, TARGET_STRING("drum_bench/var_impedence_controller1/Sum1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 117, 0, TARGET_STRING("drum_bench/var_impedence_controller1/Sum2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 118, 1, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter/MATLAB Function"),
    TARGET_STRING(""), 0, 0, 0, 0, 6 },

  { 119, 2, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/mean"),
    TARGET_STRING(""), 0, 0, 0, 0, 6 },

  { 120, 3, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/mean1"),
    TARGET_STRING(""), 0, 0, 0, 0, 6 },

  { 121, 4, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/mean2"),
    TARGET_STRING(""), 0, 0, 0, 0, 6 },

  { 122, 5, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/slopeChange"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 123, 6, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/smooth"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 124, 7, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/smooth1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 125, 8, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/smooth2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 126, 9, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/smooth3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 127, 10, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/zeroCrossing"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 128, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/Abs"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 129, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 130, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/Gain1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 131, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/Math Function"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 132, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter/Math Function1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 133, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter/Window Function"),
    TARGET_STRING(""), 0, 0, 2, 0, 7 },

  { 134, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/Subtract"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 135, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/Buffer"),
    TARGET_STRING(""), 0, 0, 2, 0, 7 },

  { 136, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/Buffer1"),
    TARGET_STRING(""), 0, 0, 2, 0, 7 },

  { 137, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/Buffer2"),
    TARGET_STRING(""), 0, 0, 2, 0, 7 },

  { 138, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter/Buffer3"),
    TARGET_STRING(""), 0, 0, 2, 0, 7 },

  { 139, 11, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter1/MATLAB Function"),
    TARGET_STRING(""), 0, 0, 0, 0, 6 },

  { 140, 12, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/mean"),
    TARGET_STRING(""), 0, 0, 0, 0, 6 },

  { 141, 13, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/mean1"),
    TARGET_STRING(""), 0, 0, 0, 0, 6 },

  { 142, 14, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/mean2"),
    TARGET_STRING(""), 0, 0, 0, 0, 6 },

  { 143, 15, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/slopeChange"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 144, 16, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/smooth"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 145, 17, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/smooth1"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 146, 18, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/smooth2"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 147, 19, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/smooth3"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 148, 20, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter1/zeroCrossing"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 149, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/Abs"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 150, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 151, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/Gain1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 152, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter1/Math Function"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 153, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter1/Math Function1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 154, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter1/Window Function"),
    TARGET_STRING(""), 0, 0, 2, 0, 7 },

  { 155, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/Subtract"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 156, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/Buffer"),
    TARGET_STRING(""), 0, 0, 2, 0, 7 },

  { 157, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/Buffer1"),
    TARGET_STRING(""), 0, 0, 2, 0, 7 },

  { 158, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/Buffer2"),
    TARGET_STRING(""), 0, 0, 2, 0, 7 },

  { 159, 0, TARGET_STRING("drum_bench/Behavior Control/EMG Filter1/Buffer3"),
    TARGET_STRING(""), 0, 0, 2, 0, 7 },

  { 160, 21, TARGET_STRING(
    "drum_bench/impedence_controller/Filtered Derivative/Filter"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 161, 21, TARGET_STRING(
    "drum_bench/impedence_controller/Filtered Derivative/Filter Coefficient"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 162, 21, TARGET_STRING(
    "drum_bench/impedence_controller/Filtered Derivative/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 163, 22, TARGET_STRING(
    "drum_bench/impedence_controller1/Filtered Derivative/Filter"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 164, 22, TARGET_STRING(
    "drum_bench/impedence_controller1/Filtered Derivative/Filter Coefficient"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 165, 22, TARGET_STRING(
    "drum_bench/impedence_controller1/Filtered Derivative/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 166, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 167, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 168, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 169, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 170, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 171, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 172, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 173, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 174, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 175, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 176, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 177, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 178, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 179, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 180, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 181, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 182, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 183, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 184, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 185, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 186, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 187, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 188, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 189, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 190, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 191, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 192, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 193, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 194, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 195, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 196, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 197, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 198, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 199, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 200, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 201, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 202, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 203, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 204, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 205, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 206, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 207, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 208, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 209, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 210, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 211, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 212, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 213, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 214, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 215, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 216, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 217, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 218, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 219, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 220, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 221, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 222, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 223, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 224, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 225, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 226, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 227, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 228, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 229, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 230, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 231, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Multiport Switch"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 232, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Rate Limiter"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 233, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Sign"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 234, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Sine Wave"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 235, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 236, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 3 },

  { 237, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_iq"), 1, 0, 0, 0, 3 },

  { 238, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_vq_avg"), 2, 0, 0, 0, 3 },

  { 239, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1"), 3, 0, 0, 0, 3 },

  { 240, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1_dot"), 4, 0, 0, 0, 3 },

  { 241, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2"), 5, 0, 0, 0, 3 },

  { 242, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2_dot"), 6, 0, 0, 0, 3 },

  { 243, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff"), 7, 0, 0, 0, 3 },

  { 244, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff_dot"), 8, 0, 0, 0, 3 },

  { 245, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1"), 9, 0, 0, 0, 3 },

  { 246, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1_dot"), 10, 0, 0, 0, 3 },

  { 247, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2"), 11, 0, 0, 0, 3 },

  { 248, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2_dot"), 12, 0, 0, 0, 3 },

  { 249, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi"), 13, 0, 0, 0, 3 },

  { 250, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi_dot"), 14, 0, 0, 0, 3 },

  { 251, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_x"), 15, 0, 0, 0, 3 },

  { 252, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_y"), 16, 0, 0, 0, 3 },

  { 253, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_z"), 17, 0, 0, 0, 3 },

  { 254, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("faults"), 18, 2, 0, 0, 3 },

  { 255, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("bus_v"), 19, 0, 0, 0, 3 },

  { 256, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("rpa_packet"), 20, 3, 1, 0, 3 },

  { 257, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_x"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 258, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_y"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 259, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/accelerometer_z"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 260, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/analog_1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 261, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/analog_1_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 262, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/analog_2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 263, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/analog_2_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 264, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/analog_diff"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 265, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/analog_diff_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 266, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/bus_v"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 267, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/motor_iq"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 268, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/motor_torque"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 269, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/motor_vq_avg"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 270, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 271, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/quadrature_1_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 272, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/quadrature_2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 273, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/quadrature_2_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 274, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/ssi"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 275, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/ssi_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 276, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/rpa_float"),
    TARGET_STRING(""), 0, 7, 3, 0, 3 },

  { 277, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 3 },

  { 278, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_iq"), 1, 0, 0, 0, 3 },

  { 279, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_vq_avg"), 2, 0, 0, 0, 3 },

  { 280, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1"), 3, 0, 0, 0, 3 },

  { 281, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1_dot"), 4, 0, 0, 0, 3 },

  { 282, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2"), 5, 0, 0, 0, 3 },

  { 283, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2_dot"), 6, 0, 0, 0, 3 },

  { 284, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff"), 7, 0, 0, 0, 3 },

  { 285, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff_dot"), 8, 0, 0, 0, 3 },

  { 286, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1"), 9, 0, 0, 0, 3 },

  { 287, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1_dot"), 10, 0, 0, 0, 3 },

  { 288, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2"), 11, 0, 0, 0, 3 },

  { 289, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2_dot"), 12, 0, 0, 0, 3 },

  { 290, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi"), 13, 0, 0, 0, 3 },

  { 291, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi_dot"), 14, 0, 0, 0, 3 },

  { 292, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_x"), 15, 0, 0, 0, 3 },

  { 293, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_y"), 16, 0, 0, 0, 3 },

  { 294, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_z"), 17, 0, 0, 0, 3 },

  { 295, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("faults"), 18, 2, 0, 0, 3 },

  { 296, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("bus_v"), 19, 0, 0, 0, 3 },

  { 297, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("rpa_packet"), 20, 3, 1, 0, 3 },

  { 298, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_x"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 299, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_y"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 300, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/accelerometer_z"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 301, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/analog_1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 302, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/analog_1_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 303, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/analog_2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 304, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/analog_2_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 305, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/analog_diff"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 306, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/analog_diff_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 307, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/bus_v"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 308, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/motor_iq"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 309, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/motor_torque"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 310, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/motor_vq_avg"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 311, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 312, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_1_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 313, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 314, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/quadrature_2_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 315, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/ssi"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 316, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/ssi_dot"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 317, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/rpa_float"),
    TARGET_STRING(""), 0, 7, 3, 0, 3 },

  { 318, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector"),
    TARGET_STRING("mode"), 0, 0, 0, 0, 0 },

  { 319, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector1"),
    TARGET_STRING("motor_iq"), 0, 0, 0, 0, 0 },

  { 320, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector1"),
    TARGET_STRING("motor_id"), 1, 0, 0, 0, 0 },

  { 321, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector2"),
    TARGET_STRING("motor_vq"), 0, 0, 0, 0, 0 },

  { 322, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector2"),
    TARGET_STRING("motor_vd"), 1, 0, 0, 0, 0 },

  { 323, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector3"),
    TARGET_STRING("analog_diff"), 0, 0, 0, 0, 0 },

  { 324, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector3"),
    TARGET_STRING("analog_diff_dot"), 1, 0, 0, 0, 0 },

  { 325, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector4"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 326, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector4"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 327, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Bus Selector5"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 0 },

  { 328, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Multiport Switch"),
    TARGET_STRING("mode"), 0, 2, 0, 0, 0 },

  { 329, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/Multiport Switch"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 330, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/motor_temperature_model/eA1"),
    TARGET_STRING(""), 0, 0, 5, 0, 3 },

  { 331, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/motor_temperature_model/eA2"),
    TARGET_STRING(""), 0, 0, 5, 0, 3 },

  { 332, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/motor_temperature_model/resistance"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 333, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/motor_temperature_model/Math Function"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 334, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/motor_temperature_model/Sum"),
    TARGET_STRING(""), 0, 0, 5, 0, 3 },

  { 335, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/motor_temperature_model/Delay"),
    TARGET_STRING(""), 0, 0, 5, 0, 3 },

  { 336, 26, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/select rpc"),
    TARGET_STRING("type"), 0, 8, 0, 0, 0 },

  { 337, 26, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/select rpc"),
    TARGET_STRING("id"), 1, 7, 0, 0, 0 },

  { 338, 26, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/select rpc"),
    TARGET_STRING("data"), 2, 3, 6, 0, 0 },

  { 339, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/Vector Concatenate"),
    TARGET_STRING(""), 0, 9, 7, 0, 0 },

  { 340, 0, TARGET_STRING("drum_bench/marlin_1.1_full1/rpc_generator/Byte Pack"),
    TARGET_STRING("rpc"), 0, 3, 1, 0, 0 },

  { 341, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/Byte Unpack"),
    TARGET_STRING("type"), 0, 8, 0, 0, 3 },

  { 342, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/Byte Unpack"),
    TARGET_STRING("id"), 1, 7, 0, 0, 3 },

  { 343, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/Byte Unpack"),
    TARGET_STRING("data"), 2, 3, 6, 0, 3 },

  { 344, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector"),
    TARGET_STRING("mode"), 0, 0, 0, 0, 0 },

  { 345, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector1"),
    TARGET_STRING("motor_iq"), 0, 0, 0, 0, 0 },

  { 346, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector1"),
    TARGET_STRING("motor_id"), 1, 0, 0, 0, 0 },

  { 347, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector2"),
    TARGET_STRING("motor_vq"), 0, 0, 0, 0, 0 },

  { 348, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector2"),
    TARGET_STRING("motor_vd"), 1, 0, 0, 0, 0 },

  { 349, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector3"),
    TARGET_STRING("analog_diff"), 0, 0, 0, 0, 0 },

  { 350, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector3"),
    TARGET_STRING("analog_diff_dot"), 1, 0, 0, 0, 0 },

  { 351, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector4"),
    TARGET_STRING("quadrature_1_angle"), 0, 0, 0, 0, 0 },

  { 352, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector4"),
    TARGET_STRING("quadrature_1_angle_dot"), 1, 0, 0, 0, 0 },

  { 353, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Bus Selector5"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 0 },

  { 354, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Multiport Switch"),
    TARGET_STRING("mode"), 0, 2, 0, 0, 0 },

  { 355, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/Multiport Switch"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 356, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/motor_temperature_model/eA1"),
    TARGET_STRING(""), 0, 0, 5, 0, 3 },

  { 357, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/motor_temperature_model/eA2"),
    TARGET_STRING(""), 0, 0, 5, 0, 3 },

  { 358, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/motor_temperature_model/resistance"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 359, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/motor_temperature_model/Math Function"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 360, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/motor_temperature_model/Sum"),
    TARGET_STRING(""), 0, 0, 5, 0, 3 },

  { 361, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/motor_temperature_model/Delay"),
    TARGET_STRING(""), 0, 0, 5, 0, 3 },

  { 362, 30, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/select rpc"),
    TARGET_STRING("type"), 0, 8, 0, 0, 0 },

  { 363, 30, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/select rpc"),
    TARGET_STRING("id"), 1, 7, 0, 0, 0 },

  { 364, 30, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/select rpc"),
    TARGET_STRING("data"), 2, 3, 6, 0, 0 },

  { 365, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/Vector Concatenate"),
    TARGET_STRING(""), 0, 9, 7, 0, 0 },

  { 366, 0, TARGET_STRING("drum_bench/marlin_1.1_full2/rpc_generator/Byte Pack"),
    TARGET_STRING("rpc"), 0, 3, 1, 0, 0 },

  { 367, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/Byte Unpack"),
    TARGET_STRING("type"), 0, 8, 0, 0, 3 },

  { 368, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/Byte Unpack"),
    TARGET_STRING("id"), 1, 7, 0, 0, 3 },

  { 369, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/Byte Unpack"),
    TARGET_STRING("data"), 2, 3, 6, 0, 3 },

  { 370, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 3 },

  { 371, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_iq"), 1, 0, 0, 0, 3 },

  { 372, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_vq_avg"), 2, 0, 0, 0, 3 },

  { 373, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1"), 3, 0, 0, 0, 3 },

  { 374, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1_dot"), 4, 0, 0, 0, 3 },

  { 375, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2"), 5, 0, 0, 0, 3 },

  { 376, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2_dot"), 6, 0, 0, 0, 3 },

  { 377, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff"), 7, 0, 0, 0, 3 },

  { 378, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff_dot"), 8, 0, 0, 0, 3 },

  { 379, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1"), 9, 0, 0, 0, 3 },

  { 380, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1_dot"), 10, 0, 0, 0, 3 },

  { 381, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2"), 11, 0, 0, 0, 3 },

  { 382, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2_dot"), 12, 0, 0, 0, 3 },

  { 383, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi"), 13, 0, 0, 0, 3 },

  { 384, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi_dot"), 14, 0, 0, 0, 3 },

  { 385, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_x"), 15, 0, 0, 0, 3 },

  { 386, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_y"), 16, 0, 0, 0, 3 },

  { 387, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_z"), 17, 0, 0, 0, 3 },

  { 388, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("faults"), 18, 2, 0, 0, 3 },

  { 389, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("bus_v"), 19, 0, 0, 0, 3 },

  { 390, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("rpa_packet"), 20, 3, 1, 0, 3 },

  { 391, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 3 },

  { 392, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_iq"), 1, 0, 0, 0, 3 },

  { 393, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("motor_vq_avg"), 2, 0, 0, 0, 3 },

  { 394, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1"), 3, 0, 0, 0, 3 },

  { 395, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_1_dot"), 4, 0, 0, 0, 3 },

  { 396, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2"), 5, 0, 0, 0, 3 },

  { 397, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_2_dot"), 6, 0, 0, 0, 3 },

  { 398, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff"), 7, 0, 0, 0, 3 },

  { 399, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("analog_diff_dot"), 8, 0, 0, 0, 3 },

  { 400, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1"), 9, 0, 0, 0, 3 },

  { 401, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_1_dot"), 10, 0, 0, 0, 3 },

  { 402, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2"), 11, 0, 0, 0, 3 },

  { 403, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("quadrature_2_dot"), 12, 0, 0, 0, 3 },

  { 404, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi"), 13, 0, 0, 0, 3 },

  { 405, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("ssi_dot"), 14, 0, 0, 0, 3 },

  { 406, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_x"), 15, 0, 0, 0, 3 },

  { 407, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_y"), 16, 0, 0, 0, 3 },

  { 408, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("accelerometer_z"), 17, 0, 0, 0, 3 },

  { 409, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("faults"), 18, 2, 0, 0, 3 },

  { 410, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("bus_v"), 19, 0, 0, 0, 3 },

  { 411, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Bus Selector"),
    TARGET_STRING("rpa_packet"), 20, 3, 1, 0, 3 },

  { 412, 31, TARGET_STRING(
    "drum_bench/var_impedence_controller/Filtered Derivative/Filter"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 413, 31, TARGET_STRING(
    "drum_bench/var_impedence_controller/Filtered Derivative/Filter Coefficient"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 414, 31, TARGET_STRING(
    "drum_bench/var_impedence_controller/Filtered Derivative/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 415, 32, TARGET_STRING(
    "drum_bench/var_impedence_controller1/Filtered Derivative/Filter"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 416, 32, TARGET_STRING(
    "drum_bench/var_impedence_controller1/Filtered Derivative/Filter Coefficient"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 417, 32, TARGET_STRING(
    "drum_bench/var_impedence_controller1/Filtered Derivative/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 418, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter/Lowpass Filter/Generated Filter Block"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 419, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter/Magnitude FFT/Magnitude Squared"),
    TARGET_STRING(""), 0, 0, 2, 0, 6 },

  { 420, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter/Magnitude FFT/FFT"),
    TARGET_STRING(""), 0, 10, 2, 0, 6 },

  { 421, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter/Peak-Notch Filter/Generated Filter Block"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 422, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter1/Lowpass Filter/Generated Filter Block"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 423, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter1/Magnitude FFT/Magnitude Squared"),
    TARGET_STRING(""), 0, 0, 2, 0, 6 },

  { 424, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter1/Magnitude FFT/FFT"),
    TARGET_STRING(""), 0, 10, 2, 0, 6 },

  { 425, 0, TARGET_STRING(
    "drum_bench/Behavior Control/EMG Filter1/Peak-Notch Filter/Generated Filter Block"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 426, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 427, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 428, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 429, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 430, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 431, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 432, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/analog_diff/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 433, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 434, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 435, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 436, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 437, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 438, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 439, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_iq/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 440, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 441, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 442, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 443, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 444, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 445, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 446, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/motor_vq/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 447, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 448, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 449, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 450, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 451, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 452, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 453, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 454, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 455, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 456, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 457, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 458, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 459, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 460, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 461, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 462, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 463, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 464, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 465, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 466, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 467, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/analog_diff/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 468, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 469, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 470, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 471, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 472, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 473, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 474, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_iq/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 475, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 476, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 477, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 478, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 479, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 480, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 481, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/motor_vq/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 482, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 483, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 484, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 485, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 486, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 487, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 488, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 489, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Clock1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 490, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Gain"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 491, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Product"),
    TARGET_STRING(""), 0, 0, 0, 0, 0 },

  { 492, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Product1"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 493, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Product2"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 494, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Sum"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 495, 0, TARGET_STRING(
    "drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal/Output"),
    TARGET_STRING(""), 0, 0, 0, 0, 2 },

  { 496, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/analog_"),
    TARGET_STRING("analog"), 0, 4, 0, 0, 3 },

  { 497, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/communication_fault_"),
    TARGET_STRING("communication_fault"), 0, 4, 0, 0, 3 },

  { 498, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/current_"),
    TARGET_STRING("current"), 0, 4, 0, 0, 3 },

  { 499, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/ethercat_"),
    TARGET_STRING("ethercat"), 0, 4, 0, 0, 3 },

  { 500, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/external_panic_"),
    TARGET_STRING("external_panic"), 0, 4, 0, 0, 3 },

  { 501, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/hard_fault_"),
    TARGET_STRING("hard_fault"), 0, 4, 0, 0, 3 },

  { 502, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/quadrature_"),
    TARGET_STRING("quadrature"), 0, 4, 0, 0, 3 },

  { 503, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/raw_integer"),
    TARGET_STRING("raw_integer"), 0, 2, 0, 0, 3 },

  { 504, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/soft_fault_"),
    TARGET_STRING("soft_fault"), 0, 4, 0, 0, 3 },

  { 505, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/ssi_"),
    TARGET_STRING("ssi"), 0, 4, 0, 0, 3 },

  { 506, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/temperature_"),
    TARGET_STRING("temperature"), 0, 4, 0, 0, 3 },

  { 507, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/voltage_"),
    TARGET_STRING("voltage"), 0, 4, 0, 0, 3 },

  { 508, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/analog_"),
    TARGET_STRING("analog"), 0, 4, 0, 0, 3 },

  { 509, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/communication_fault_"),
    TARGET_STRING("communication_fault"), 0, 4, 0, 0, 3 },

  { 510, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/current_"),
    TARGET_STRING("current"), 0, 4, 0, 0, 3 },

  { 511, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/ethercat_"),
    TARGET_STRING("ethercat"), 0, 4, 0, 0, 3 },

  { 512, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/external_panic_"),
    TARGET_STRING("external_panic"), 0, 4, 0, 0, 3 },

  { 513, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/hard_fault_"),
    TARGET_STRING("hard_fault"), 0, 4, 0, 0, 3 },

  { 514, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/quadrature_"),
    TARGET_STRING("quadrature"), 0, 4, 0, 0, 3 },

  { 515, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/raw_integer"),
    TARGET_STRING("raw_integer"), 0, 2, 0, 0, 3 },

  { 516, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/soft_fault_"),
    TARGET_STRING("soft_fault"), 0, 4, 0, 0, 3 },

  { 517, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/ssi_"),
    TARGET_STRING("ssi"), 0, 4, 0, 0, 3 },

  { 518, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/temperature_"),
    TARGET_STRING("temperature"), 0, 4, 0, 0, 3 },

  { 519, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/voltage_"),
    TARGET_STRING("voltage"), 0, 4, 0, 0, 3 },

  { 520, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/accelerometer_x_"),
    TARGET_STRING("accelerometer_x"), 0, 0, 0, 0, 3 },

  { 521, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/accelerometer_y_"),
    TARGET_STRING("accelerometer_y"), 0, 0, 0, 0, 3 },

  { 522, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/accelerometer_z_"),
    TARGET_STRING("accelerometer_z"), 0, 0, 0, 0, 3 },

  { 523, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_1_"),
    TARGET_STRING("analog_1"), 0, 0, 0, 0, 3 },

  { 524, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_1_dot_"),
    TARGET_STRING("analog_1_dot"), 0, 0, 0, 0, 3 },

  { 525, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_2_"),
    TARGET_STRING("analog_2"), 0, 0, 0, 0, 3 },

  { 526, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_2_dot_"),
    TARGET_STRING("analog_2_dot"), 0, 0, 0, 0, 3 },

  { 527, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_diff_"),
    TARGET_STRING("analog_diff"), 0, 0, 0, 0, 3 },

  { 528, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/analog_diff_dot_"),
    TARGET_STRING("analog_diff_dot"), 0, 0, 0, 0, 3 },

  { 529, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/bus_v_"),
    TARGET_STRING("bus_v"), 0, 0, 0, 0, 3 },

  { 530, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/debug_"),
    TARGET_STRING("debug"), 0, 0, 0, 0, 3 },

  { 531, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/faults_"),
    TARGET_STRING("faults"), 0, 2, 0, 0, 3 },

  { 532, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/motor_iq_"),
    TARGET_STRING("motor_iq"), 0, 0, 0, 0, 3 },

  { 533, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/motor_torque_"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 3 },

  { 534, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/motor_vq_avg_"),
    TARGET_STRING("motor_vq_avg"), 0, 0, 0, 0, 3 },

  { 535, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/quadrature_1_"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 3 },

  { 536, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/quadrature_1_dot_"),
    TARGET_STRING("quadrature_1_dot"), 0, 0, 0, 0, 3 },

  { 537, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/quadrature_2_"),
    TARGET_STRING("quadrature_2"), 0, 0, 0, 0, 3 },

  { 538, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/quadrature_2_dot_"),
    TARGET_STRING("quadrature_2_dot"), 0, 0, 0, 0, 3 },

  { 539, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/rpa_packet_"),
    TARGET_STRING("rpa_packet"), 0, 3, 1, 0, 3 },

  { 540, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/ssi_"),
    TARGET_STRING("ssi"), 0, 0, 0, 0, 3 },

  { 541, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/ssi_dot_"),
    TARGET_STRING("ssi_dot"), 0, 0, 0, 0, 3 },

  { 542, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 543, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 1, 7, 0, 0, 3 },

  { 544, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 2, 7, 0, 0, 3 },

  { 545, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 3, 7, 0, 0, 3 },

  { 546, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 4, 7, 0, 0, 3 },

  { 547, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 5, 7, 0, 0, 3 },

  { 548, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 6, 7, 0, 0, 3 },

  { 549, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 7, 7, 0, 0, 3 },

  { 550, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 8, 7, 0, 0, 3 },

  { 551, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 9, 7, 0, 0, 3 },

  { 552, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 10, 7, 0, 0, 3 },

  { 553, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 11, 7, 0, 0, 3 },

  { 554, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 12, 7, 0, 0, 3 },

  { 555, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 13, 7, 0, 0, 3 },

  { 556, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 14, 7, 0, 0, 3 },

  { 557, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 15, 7, 0, 0, 3 },

  { 558, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 16, 7, 0, 0, 3 },

  { 559, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 17, 7, 0, 0, 3 },

  { 560, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 18, 2, 0, 0, 3 },

  { 561, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 19, 7, 0, 0, 3 },

  { 562, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 20, 7, 0, 0, 3 },

  { 563, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 21, 3, 1, 0, 3 },

  { 564, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/analog_diff/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 0 },

  { 565, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/analog_diff/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 566, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/brake/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 3 },

  { 567, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/brake/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 4, 0, 3 },

  { 568, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/current/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 0 },

  { 569, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/current/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 570, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/motor_torque/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 0 },

  { 571, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/motor_torque/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 572, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/open/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 3 },

  { 573, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/open/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 4, 0, 3 },

  { 574, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/phase lock/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 3 },

  { 575, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/phase lock/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 4, 0, 3 },

  { 576, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/quadrature_1/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 0 },

  { 577, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/quadrature_1/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 578, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/voltage/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 0 },

  { 579, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/voltage/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 580, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/10 khz logger read/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 581, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/10 khz logger read/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 582, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/10 khz logger start/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 583, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/10 khz logger start/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 584, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/100 khz logger read/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 585, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/100 khz logger read/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 586, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/100 khz logger read2/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 587, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/100 khz logger read2/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 588, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/100 khz logger start/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 589, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/100 khz logger start/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 590, 23, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/enabled subsystem"),
    TARGET_STRING(""), 0, 3, 6, 0, 3 },

  { 591, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 592, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 9, 0, 3 },

  { 593, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/analog_limit"),
    TARGET_STRING("analog_limit"), 0, 0, 0, 0, 3 },

  { 594, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/ethercat"),
    TARGET_STRING("ethercat"), 0, 0, 0, 0, 3 },

  { 595, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/external_panic"),
    TARGET_STRING("external_panic"), 0, 0, 0, 0, 3 },

  { 596, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/motor encoder"),
    TARGET_STRING("motor_encoder"), 0, 0, 0, 0, 3 },

  { 597, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/over_current"),
    TARGET_STRING("over_current"), 0, 0, 0, 0, 3 },

  { 598, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/over_voltage"),
    TARGET_STRING("over_voltage"), 0, 0, 0, 0, 3 },

  { 599, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/quadrature_1_high"),
    TARGET_STRING("quadrature_1_high"), 0, 0, 0, 0, 3 },

  { 600, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/quadrature_1_low"),
    TARGET_STRING("quadrature_1_low"), 0, 0, 0, 0, 3 },

  { 601, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/quadrature_2_limit"),
    TARGET_STRING("quadrature_2_limit"), 0, 0, 0, 0, 3 },

  { 602, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/under_voltage"),
    TARGET_STRING("under_voltage"), 0, 0, 0, 0, 3 },

  { 603, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 604, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/Byte Unpack"),
    TARGET_STRING(""), 0, 11, 9, 0, 3 },

  { 605, 24, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/Enabled Subsystem"),
    TARGET_STRING(""), 0, 3, 6, 0, 3 },

  { 606, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 607, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/load a"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 608, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/load a1"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 609, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/load a2"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 610, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/load a3"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 611, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/load b"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 612, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 613, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/Byte Unpack"),
    TARGET_STRING(""), 0, 7, 10, 0, 3 },

  { 614, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get status debug/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 3 },

  { 615, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get status debug/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 5, 0, 3 },

  { 616, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get status debug/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 3 },

  { 617, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get status debug1/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 3 },

  { 618, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get status debug1/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 5, 0, 3 },

  { 619, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get status debug1/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 3 },

  { 620, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get status debug2/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 3 },

  { 621, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get status debug2/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 5, 0, 3 },

  { 622, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get status debug2/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 3 },

  { 623, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/current_index"),
    TARGET_STRING(""), 0, 8, 0, 0, 3 },

  { 624, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/store param/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 625, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/store param/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 626, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/testrpc/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 627, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/zero both encoder angles/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 628, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/zero both encoder angles/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 629, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/zero output torque/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 630, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/zero output torque/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 631, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/accelerometer_x_"),
    TARGET_STRING("accelerometer_x"), 0, 0, 0, 0, 3 },

  { 632, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/accelerometer_y_"),
    TARGET_STRING("accelerometer_y"), 0, 0, 0, 0, 3 },

  { 633, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/accelerometer_z_"),
    TARGET_STRING("accelerometer_z"), 0, 0, 0, 0, 3 },

  { 634, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_1_"),
    TARGET_STRING("analog_1"), 0, 0, 0, 0, 3 },

  { 635, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_1_dot_"),
    TARGET_STRING("analog_1_dot"), 0, 0, 0, 0, 3 },

  { 636, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_2_"),
    TARGET_STRING("analog_2"), 0, 0, 0, 0, 3 },

  { 637, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_2_dot_"),
    TARGET_STRING("analog_2_dot"), 0, 0, 0, 0, 3 },

  { 638, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_diff_"),
    TARGET_STRING("analog_diff"), 0, 0, 0, 0, 3 },

  { 639, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/analog_diff_dot_"),
    TARGET_STRING("analog_diff_dot"), 0, 0, 0, 0, 3 },

  { 640, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/bus_v_"),
    TARGET_STRING("bus_v"), 0, 0, 0, 0, 3 },

  { 641, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/debug_"),
    TARGET_STRING("debug"), 0, 0, 0, 0, 3 },

  { 642, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/faults_"),
    TARGET_STRING("faults"), 0, 2, 0, 0, 3 },

  { 643, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/motor_iq_"),
    TARGET_STRING("motor_iq"), 0, 0, 0, 0, 3 },

  { 644, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/motor_torque_"),
    TARGET_STRING("motor_torque"), 0, 0, 0, 0, 3 },

  { 645, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/motor_vq_avg_"),
    TARGET_STRING("motor_vq_avg"), 0, 0, 0, 0, 3 },

  { 646, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/quadrature_1_"),
    TARGET_STRING("quadrature_1"), 0, 0, 0, 0, 3 },

  { 647, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/quadrature_1_dot_"),
    TARGET_STRING("quadrature_1_dot"), 0, 0, 0, 0, 3 },

  { 648, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/quadrature_2_"),
    TARGET_STRING("quadrature_2"), 0, 0, 0, 0, 3 },

  { 649, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/quadrature_2_dot_"),
    TARGET_STRING("quadrature_2_dot"), 0, 0, 0, 0, 3 },

  { 650, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/rpa_packet_"),
    TARGET_STRING("rpa_packet"), 0, 3, 1, 0, 3 },

  { 651, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/ssi_"),
    TARGET_STRING("ssi"), 0, 0, 0, 0, 3 },

  { 652, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/ssi_dot_"),
    TARGET_STRING("ssi_dot"), 0, 0, 0, 0, 3 },

  { 653, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 654, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 1, 7, 0, 0, 3 },

  { 655, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 2, 7, 0, 0, 3 },

  { 656, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 3, 7, 0, 0, 3 },

  { 657, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 4, 7, 0, 0, 3 },

  { 658, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 5, 7, 0, 0, 3 },

  { 659, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 6, 7, 0, 0, 3 },

  { 660, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 7, 7, 0, 0, 3 },

  { 661, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 8, 7, 0, 0, 3 },

  { 662, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 9, 7, 0, 0, 3 },

  { 663, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 10, 7, 0, 0, 3 },

  { 664, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 11, 7, 0, 0, 3 },

  { 665, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 12, 7, 0, 0, 3 },

  { 666, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 13, 7, 0, 0, 3 },

  { 667, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 14, 7, 0, 0, 3 },

  { 668, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 15, 7, 0, 0, 3 },

  { 669, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 16, 7, 0, 0, 3 },

  { 670, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 17, 7, 0, 0, 3 },

  { 671, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 18, 2, 0, 0, 3 },

  { 672, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 19, 7, 0, 0, 3 },

  { 673, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 20, 7, 0, 0, 3 },

  { 674, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/marlin_ec"),
    TARGET_STRING(""), 21, 3, 1, 0, 3 },

  { 675, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/analog_diff/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 0 },

  { 676, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/analog_diff/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 677, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/brake/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 3 },

  { 678, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/brake/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 4, 0, 3 },

  { 679, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/current/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 0 },

  { 680, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/current/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 681, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/motor_torque/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 0 },

  { 682, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/motor_torque/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 683, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/open/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 3 },

  { 684, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/open/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 4, 0, 3 },

  { 685, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/phase lock/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 3 },

  { 686, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/phase lock/Byte Pack3"),
    TARGET_STRING("command"), 0, 3, 4, 0, 3 },

  { 687, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/quadrature_1/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 0 },

  { 688, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/quadrature_1/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 689, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/voltage/Data Type Conversion1"),
    TARGET_STRING(""), 0, 7, 8, 0, 0 },

  { 690, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/voltage/Byte Pack2"),
    TARGET_STRING("command"), 0, 3, 4, 0, 0 },

  { 691, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/10 khz logger read/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 692, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/10 khz logger read/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 693, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/10 khz logger start/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 694, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/10 khz logger start/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 695, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/100 khz logger read/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 696, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/100 khz logger read/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 697, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/100 khz logger read2/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 698, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/100 khz logger read2/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 699, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/100 khz logger start/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 700, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/100 khz logger start/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 701, 27, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/enabled subsystem"),
    TARGET_STRING(""), 0, 3, 6, 0, 3 },

  { 702, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 703, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/Data Type Conversion"),
    TARGET_STRING(""), 0, 0, 9, 0, 3 },

  { 704, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/analog_limit"),
    TARGET_STRING("analog_limit"), 0, 0, 0, 0, 3 },

  { 705, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/ethercat"),
    TARGET_STRING("ethercat"), 0, 0, 0, 0, 3 },

  { 706, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/external_panic"),
    TARGET_STRING("external_panic"), 0, 0, 0, 0, 3 },

  { 707, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/motor encoder"),
    TARGET_STRING("motor_encoder"), 0, 0, 0, 0, 3 },

  { 708, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/over_current"),
    TARGET_STRING("over_current"), 0, 0, 0, 0, 3 },

  { 709, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/over_voltage"),
    TARGET_STRING("over_voltage"), 0, 0, 0, 0, 3 },

  { 710, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/quadrature_1_high"),
    TARGET_STRING("quadrature_1_high"), 0, 0, 0, 0, 3 },

  { 711, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/quadrature_1_low"),
    TARGET_STRING("quadrature_1_low"), 0, 0, 0, 0, 3 },

  { 712, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/quadrature_2_limit"),
    TARGET_STRING("quadrature_2_limit"), 0, 0, 0, 0, 3 },

  { 713, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/under_voltage"),
    TARGET_STRING("under_voltage"), 0, 0, 0, 0, 3 },

  { 714, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 715, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/Byte Unpack"),
    TARGET_STRING(""), 0, 11, 9, 0, 3 },

  { 716, 28, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/Enabled Subsystem"),
    TARGET_STRING(""), 0, 3, 6, 0, 3 },

  { 717, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 718, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/load a"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 719, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/load a1"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 720, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/load a2"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 721, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/load a3"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 722, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/load b"),
    TARGET_STRING(""), 0, 7, 0, 0, 3 },

  { 723, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 724, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/Byte Unpack"),
    TARGET_STRING(""), 0, 7, 10, 0, 3 },

  { 725, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get status debug/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 3 },

  { 726, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get status debug/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 5, 0, 3 },

  { 727, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get status debug/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 3 },

  { 728, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get status debug1/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 3 },

  { 729, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get status debug1/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 5, 0, 3 },

  { 730, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get status debug1/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 3 },

  { 731, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get status debug2/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 3 },

  { 732, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get status debug2/Data Type Conversion"),
    TARGET_STRING(""), 0, 2, 5, 0, 3 },

  { 733, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get status debug2/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 3 },

  { 734, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/current_index"),
    TARGET_STRING(""), 0, 8, 0, 0, 3 },

  { 735, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/store param/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 736, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/store param/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 737, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/testrpc/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 738, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/zero both encoder angles/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 739, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/zero both encoder angles/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 740, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/zero output torque/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 0 },

  { 741, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/zero output torque/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 1 },

  { 742, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Lowpass Filter/Generated Filter Block"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 743, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Lowpass Filter1/Generated Filter Block"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 744, 0, TARGET_STRING(
    "drum_bench/mason process/marlin_1.1_basic status/Lowpass Filter2/Generated Filter Block"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 745, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Lowpass Filter/Generated Filter Block"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 746, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Lowpass Filter1/Generated Filter Block"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 747, 0, TARGET_STRING(
    "drum_bench/mason process1/marlin_1.1_basic status/Lowpass Filter2/Generated Filter Block"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 748, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 749, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 750, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 2, 3 },

  { 751, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 752, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 3, 3 },

  { 753, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 754, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 4, 3 },

  { 755, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 756, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 5, 3 },

  { 757, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 758, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 6, 3 },

  { 759, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 760, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 7, 3 },

  { 761, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 762, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 8, 3 },

  { 763, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 764, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 9, 3 },

  { 765, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 766, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 10, 3 },

  { 767, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 768, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 11, 3 },

  { 769, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 770, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 771, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 772, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 2, 3 },

  { 773, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 774, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 3, 3 },

  { 775, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 776, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 4, 3 },

  { 777, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 778, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 5, 3 },

  { 779, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 780, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 6, 3 },

  { 781, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 782, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 7, 3 },

  { 783, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 784, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 8, 3 },

  { 785, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 786, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 9, 3 },

  { 787, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 788, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 10, 3 },

  { 789, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 790, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9/Extract Desired Bits"),
    TARGET_STRING(""), 0, 3, 0, 11, 3 },

  { 791, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9/Modify Scaling Only"),
    TARGET_STRING(""), 0, 3, 0, 1, 3 },

  { 792, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running/Output"),
    TARGET_STRING(""), 0, 2, 0, 0, 3 },

  { 793, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/Compare To Constant/Compare"),
    TARGET_STRING(""), 0, 4, 0, 0, 3 },

  { 794, 23, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/check safety/enabled subsystem/In1"),
    TARGET_STRING(""), 0, 3, 6, 0, 3 },

  { 795, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/Compare To Constant/Compare"),
    TARGET_STRING(""), 0, 4, 0, 0, 3 },

  { 796, 24, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/Enabled Subsystem/In1"),
    TARGET_STRING(""), 0, 3, 6, 0, 3 },

  { 797, 25, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING("type"), 0, 8, 0, 0, 3 },

  { 798, 25, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING("id"), 1, 7, 0, 0, 3 },

  { 799, 25, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING(""), 2, 8, 0, 0, 3 },

  { 800, 25, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING(""), 3, 7, 0, 0, 3 },

  { 801, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 3 },

  { 802, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 3 },

  { 803, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 11, 0, 3 },

  { 804, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/Data Type Conversion"),
    TARGET_STRING(""), 0, 7, 11, 0, 3 },

  { 805, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running/Output"),
    TARGET_STRING(""), 0, 2, 0, 0, 3 },

  { 806, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/Compare To Constant/Compare"),
    TARGET_STRING(""), 0, 4, 0, 0, 3 },

  { 807, 27, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/check safety/enabled subsystem/In1"),
    TARGET_STRING(""), 0, 3, 6, 0, 3 },

  { 808, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/Compare To Constant/Compare"),
    TARGET_STRING(""), 0, 4, 0, 0, 3 },

  { 809, 28, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/Enabled Subsystem/In1"),
    TARGET_STRING(""), 0, 3, 6, 0, 3 },

  { 810, 29, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING("type"), 0, 8, 0, 0, 3 },

  { 811, 29, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING("id"), 1, 7, 0, 0, 3 },

  { 812, 29, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING(""), 2, 8, 0, 0, 3 },

  { 813, 29, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1"),
    TARGET_STRING(""), 3, 7, 0, 0, 3 },

  { 814, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/Bus Creator"),
    TARGET_STRING(""), 0, 9, 0, 0, 3 },

  { 815, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/Byte Pack"),
    TARGET_STRING("data"), 0, 3, 6, 0, 3 },

  { 816, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 11, 0, 3 },

  { 817, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/Data Type Conversion"),
    TARGET_STRING(""), 0, 7, 11, 0, 3 },

  { 818, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running/Increment Real World/FixPt Sum1"),
    TARGET_STRING(""), 0, 2, 0, 0, 3 },

  { 819, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running/Wrap To Zero/FixPt Switch"),
    TARGET_STRING(""), 0, 2, 0, 0, 3 },

  { 820, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 3 },

  { 821, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 822, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 823, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 824, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 825, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 826, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 827, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 828, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 829, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 830, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/bridge/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 14, 0, 3 },

  { 831, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/bridge/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 832, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/bridge/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 833, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 3 },

  { 834, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 835, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 3 },

  { 836, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 837, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 838, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 839, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 840, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 841, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 842, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 3 },

  { 843, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 3 },

  { 844, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 845, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 846, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 847, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 848, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 849, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 850, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 851, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor/id7"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 852, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 17, 0, 3 },

  { 853, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 854, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 17, 0, 3 },

  { 855, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 856, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 857, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running/Increment Real World/FixPt Sum1"),
    TARGET_STRING(""), 0, 2, 0, 0, 3 },

  { 858, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running/Wrap To Zero/FixPt Switch"),
    TARGET_STRING(""), 0, 2, 0, 0, 3 },

  { 859, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 3 },

  { 860, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 861, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 862, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 863, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 864, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 865, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 866, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 867, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 868, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 869, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/bridge/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 14, 0, 3 },

  { 870, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/bridge/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 871, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/bridge/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 872, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 3 },

  { 873, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 874, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 15, 0, 3 },

  { 875, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 876, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 877, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 878, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 879, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 880, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 881, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 3 },

  { 882, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 12, 0, 3 },

  { 883, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 884, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 885, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 886, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 887, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 888, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 889, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 890, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor/id7"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 891, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 17, 0, 3 },

  { 892, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 893, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 17, 0, 3 },

  { 894, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 895, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 896, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 897, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 898, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 899, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 900, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 901, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 902, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 903, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 904, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 905, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 906, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 907, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 908, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 909, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 910, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 911, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 912, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 913, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 914, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 915, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 916, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 917, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 918, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 919, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 920, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 921, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 3 },

  { 922, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 923, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 924, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 925, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 926, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 3 },

  { 927, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 928, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 929, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 930, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 931, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 932, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 933, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 934, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 935, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 3 },

  { 936, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 937, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 938, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 939, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 940, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 941, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 942, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 943, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 944, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 945, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 946, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 947, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 948, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 949, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 950, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 951, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 952, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 953, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 954, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 955, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 956, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 957, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 958, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 959, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 960, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 961, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 962, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 963, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 964, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 965, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 966, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 967, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 968, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 969, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 970, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 971, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 972, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 973, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 974, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 975, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 976, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 977, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 978, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 979, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 980, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 981, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 982, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 983, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 984, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 985, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 986, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 3 },

  { 987, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 988, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 989, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 990, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 991, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 3 },

  { 992, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 993, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 994, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 995, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 996, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 997, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 998, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 999, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1000, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 16, 0, 3 },

  { 1001, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1002, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1003, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id2"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1004, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1005, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 1006, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1007, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1008, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1009, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1010, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1011, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1012, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 1013, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1014, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1015, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1016, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1017, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1018, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1019, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/Vector Concatenate"),
    TARGET_STRING(""), 0, 0, 13, 0, 3 },

  { 1020, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1021, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id1"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1022, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id3"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1023, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id4"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1024, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id5"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  { 1025, 0, TARGET_STRING(
    "drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor/id6"),
    TARGET_STRING(""), 0, 0, 0, 0, 3 },

  {
    0, 0, (NULL), (NULL), 0, 0, 0, 0, 0
  }
};

/* Individual block tuning is not valid when inline parameters is *
 * selected. An empty map is produced to provide a consistent     *
 * interface independent  of inlining parameters.                 *
 */
static const rtwCAPI_BlockParameters rtBlockParameters[] = {
  /* addrMapIndex, blockPath,
   * paramName, dataTypeIndex, dimIndex, fixPtIdx
   */
  {
    0, (NULL), (NULL), 0, 0, 0
  }
};

/* Tunable variable parameters */
static const rtwCAPI_ModelParameters rtModelParameters[] = {
  /* addrMapIndex, varName, dataTypeIndex, dimIndex, fixPtIndex */
  { 1026, TARGET_STRING("config"), 38, 0, 0 },

  { 0, (NULL), 0, 0, 0 }
};

#ifndef HOST_CAPI_BUILD

/* Declare Data Addresses statically */
static void* rtDataAddrMap[] = {
  &drum_bench_B.mode,                  /* 0: Signal */
  &drum_bench_B.motor_torque_h4,       /* 1: Signal */
  &drum_bench_B.motor_iq_b0,           /* 2: Signal */
  &drum_bench_B.mode_l,                /* 3: Signal */
  &drum_bench_B.motor_torque_px,       /* 4: Signal */
  &drum_bench_B.motor_iq_n,            /* 5: Signal */
  &drum_bench_B.modeselect,            /* 6: Signal */
  &drum_bench_B.modeselect1,           /* 7: Signal */
  (void *) &drum_bench_ConstB.modeselect2,/* 8: Signal */
  &drum_bench_B.mode0,                 /* 9: Signal */
  &drum_bench_B.mode1,                 /* 10: Signal */
  &drum_bench_B.motor_iq0,             /* 11: Signal */
  &drum_bench_B.motor_iq1,             /* 12: Signal */
  &drum_bench_B.motor_torque0,         /* 13: Signal */
  &drum_bench_B.motor_torque1,         /* 14: Signal */
  &drum_bench_B.modeswitch,            /* 15: Signal */
  &drum_bench_B.modeswitch1,           /* 16: Signal */
  &drum_bench_B.DomainState_o1,        /* 17: Signal */
  &drum_bench_B.DomainState_o2,        /* 18: Signal */
  &drum_bench_B.MasterState_o1,        /* 19: Signal */
  &drum_bench_B.MasterState_o2,        /* 20: Signal */
  &drum_bench_B.MasterState_o3,        /* 21: Signal */
  &drum_bench_B.analog_1_l,            /* 22: Signal */
  &drum_bench_B.analog_2_f2,           /* 23: Signal */
  &drum_bench_B.analog_1_g0,           /* 24: Signal */
  &drum_bench_B.analog_2_n,            /* 25: Signal */
  &drum_bench_B.accelerometer_x_fm,    /* 26: Signal */
  &drum_bench_B.accelerometer_y_o,     /* 27: Signal */
  &drum_bench_B.accelerometer_z_c,     /* 28: Signal */
  &drum_bench_B.DataTypeConversion,    /* 29: Signal */
  &drum_bench_B.DataTypeConversion1,   /* 30: Signal */
  &drum_bench_B.CSfunction1,           /* 31: Signal */
  &drum_bench_B.CSfunction2,           /* 32: Signal */
  &drum_bench_B.CSfunction3,           /* 33: Signal */
  &drum_bench_B.InlinedCSFunction_o1,  /* 34: Signal */
  &drum_bench_B.InlinedCSFunction_o2,  /* 35: Signal */
  &drum_bench_B.InlinedCSFunction_o3,  /* 36: Signal */
  &drum_bench_B.InlinedCSFunction_o4,  /* 37: Signal */
  &drum_bench_B.InlinedCSFunction_o5,  /* 38: Signal */
  &drum_bench_B.InlinedCSFunction_o6,  /* 39: Signal */
  &drum_bench_B.FilteredDerivative.FilterCoefficient,/* 40: Signal */
  &drum_bench_B.BusAssignment,         /* 41: Signal */
  &drum_bench_B.quadrature_1_angle,    /* 42: Signal */
  &drum_bench_B.quadrature_1_angle_dot,/* 43: Signal */
  &drum_bench_B.quadrature_1_n,        /* 44: Signal */
  &drum_bench_B.motor_torque_pj,       /* 45: Signal */
  &drum_bench_B.kd_b,                  /* 46: Signal */
  &drum_bench_B.kp,                    /* 47: Signal */
  &drum_bench_B.Product1,              /* 48: Signal */
  &drum_bench_B.Product2,              /* 49: Signal */
  &drum_bench_B.Saturation,            /* 50: Signal */
  &drum_bench_B.Sum_m,                 /* 51: Signal */
  &drum_bench_B.Sum1,                  /* 52: Signal */
  &drum_bench_B.Sum2,                  /* 53: Signal */
  &drum_bench_B.FilteredDerivative_n.FilterCoefficient,/* 54: Signal */
  &drum_bench_B.BusAssignment_n4,      /* 55: Signal */
  &drum_bench_B.quadrature_1_angle_j,  /* 56: Signal */
  &drum_bench_B.quadrature_1_angle_dot_of,/* 57: Signal */
  &drum_bench_B.quadrature_1_nt,       /* 58: Signal */
  &drum_bench_B.motor_torque_bg,       /* 59: Signal */
  &drum_bench_B.kd_j,                  /* 60: Signal */
  &drum_bench_B.kp_h,                  /* 61: Signal */
  &drum_bench_B.Product1_j,            /* 62: Signal */
  &drum_bench_B.Product2_g,            /* 63: Signal */
  &drum_bench_B.Saturation_l,          /* 64: Signal */
  &drum_bench_B.Sum_f,                 /* 65: Signal */
  &drum_bench_B.Sum1_h,                /* 66: Signal */
  &drum_bench_B.Sum2_e,                /* 67: Signal */
  &drum_bench_B.BusCreator_n,          /* 68: Signal */
  &drum_bench_B.BusCreator_f,          /* 69: Signal */
  &drum_bench_B.marlin_basic_status_i, /* 70: Signal */
  &drum_bench_B.temperature_motor_winding_j,/* 71: Signal */
  &drum_bench_B.temperature_motor_housing_c,/* 72: Signal */
  &drum_bench_B.temperature_motor_housing_j,/* 73: Signal */
  &drum_bench_B.temperature_motor_winding_h,/* 74: Signal */
  &drum_bench_B.marlin_basic_status_l, /* 75: Signal */
  &drum_bench_B.temperature_motor_winding_k,/* 76: Signal */
  &drum_bench_B.temperature_motor_housing_jp,/* 77: Signal */
  &drum_bench_B.temperature_motor_housing_m,/* 78: Signal */
  &drum_bench_B.temperature_motor_winding_i,/* 79: Signal */
  &drum_bench_B.BusCreator,            /* 80: Signal */
  &drum_bench_B.rpc,                   /* 81: Signal */
  &drum_bench_B.BusCreator_it,         /* 82: Signal */
  &drum_bench_B.rpc_l,                 /* 83: Signal */
  &drum_bench_B.BusCreator_i,          /* 84: Signal */
  &drum_bench_B.marlin_basic_status,   /* 85: Signal */
  &drum_bench_B.temperature_motor_winding,/* 86: Signal */
  &drum_bench_B.temperature_motor_housing,/* 87: Signal */
  &drum_bench_B.BusCreator_m,          /* 88: Signal */
  &drum_bench_B.marlin_basic_status_m, /* 89: Signal */
  &drum_bench_B.temperature_motor_winding_e,/* 90: Signal */
  &drum_bench_B.temperature_motor_housing_b,/* 91: Signal */
  &drum_bench_B.FilteredDerivative_c.FilterCoefficient,/* 92: Signal */
  &drum_bench_B.bias,                  /* 93: Signal */
  &drum_bench_B.BusAssignment_n,       /* 94: Signal */
  &drum_bench_B.quadrature_1_f,        /* 95: Signal */
  &drum_bench_B.analog_1_i,            /* 96: Signal */
  &drum_bench_B.quadrature_1_angle_k,  /* 97: Signal */
  &drum_bench_B.quadrature_1_angle_dot_o,/* 98: Signal */
  &drum_bench_B.ka,                    /* 99: Signal */
  &drum_bench_B.kd,                    /* 100: Signal */
  &drum_bench_B.Product,               /* 101: Signal */
  &drum_bench_B.Sum_e,                 /* 102: Signal */
  &drum_bench_B.Sum1_o,                /* 103: Signal */
  &drum_bench_B.Sum2_b,                /* 104: Signal */
  &drum_bench_B.FilteredDerivative_p.FilterCoefficient,/* 105: Signal */
  &drum_bench_B.bias_c,                /* 106: Signal */
  &drum_bench_B.BusAssignment_nx,      /* 107: Signal */
  &drum_bench_B.quadrature_1_o,        /* 108: Signal */
  &drum_bench_B.analog_1_h,            /* 109: Signal */
  &drum_bench_B.quadrature_1_angle_o,  /* 110: Signal */
  &drum_bench_B.quadrature_1_angle_dot_e,/* 111: Signal */
  &drum_bench_B.ka_l,                  /* 112: Signal */
  &drum_bench_B.kd_l,                  /* 113: Signal */
  &drum_bench_B.Product_i,             /* 114: Signal */
  &drum_bench_B.Sum_co,                /* 115: Signal */
  &drum_bench_B.Sum1_e,                /* 116: Signal */
  &drum_bench_B.Sum2_o,                /* 117: Signal */
  &drum_bench_B.y_d,                   /* 118: Signal */
  &drum_bench_B.sf_mean_p.y,           /* 119: Signal */
  &drum_bench_B.sf_mean1_m.y,          /* 120: Signal */
  &drum_bench_B.sf_mean2_b.y,          /* 121: Signal */
  &drum_bench_B.sf_slopeChange_n.y,    /* 122: Signal */
  &drum_bench_B.sf_smooth_a.y,         /* 123: Signal */
  &drum_bench_B.sf_smooth1.y,          /* 124: Signal */
  &drum_bench_B.sf_smooth2.y,          /* 125: Signal */
  &drum_bench_B.sf_smooth3.y,          /* 126: Signal */
  &drum_bench_B.sf_zeroCrossing_f.y,   /* 127: Signal */
  &drum_bench_B.Abs_n,                 /* 128: Signal */
  &drum_bench_B.Gain_n,                /* 129: Signal */
  &drum_bench_B.Gain1_k,               /* 130: Signal */
  &drum_bench_B.MathFunction_o,        /* 131: Signal */
  &drum_bench_B.MathFunction1_n,       /* 132: Signal */
  &drum_bench_B.WindowFunction_c[0],   /* 133: Signal */
  &drum_bench_B.Subtract_d,            /* 134: Signal */
  &drum_bench_B.Buffer_h[0],           /* 135: Signal */
  &drum_bench_B.Buffer1_g[0],          /* 136: Signal */
  &drum_bench_B.Buffer2_o[0],          /* 137: Signal */
  &drum_bench_B.Buffer3_k[0],          /* 138: Signal */
  &drum_bench_B.y,                     /* 139: Signal */
  &drum_bench_B.sf_mean.y,             /* 140: Signal */
  &drum_bench_B.sf_mean1.y,            /* 141: Signal */
  &drum_bench_B.sf_mean2.y,            /* 142: Signal */
  &drum_bench_B.sf_slopeChange.y,      /* 143: Signal */
  &drum_bench_B.sf_smooth.y,           /* 144: Signal */
  &drum_bench_B.sf_smooth1_p.y,        /* 145: Signal */
  &drum_bench_B.sf_smooth2_h.y,        /* 146: Signal */
  &drum_bench_B.sf_smooth3_d.y,        /* 147: Signal */
  &drum_bench_B.sf_zeroCrossing.y,     /* 148: Signal */
  &drum_bench_B.Abs,                   /* 149: Signal */
  &drum_bench_B.Gain,                  /* 150: Signal */
  &drum_bench_B.Gain1,                 /* 151: Signal */
  &drum_bench_B.MathFunction_i,        /* 152: Signal */
  &drum_bench_B.MathFunction1,         /* 153: Signal */
  &drum_bench_B.WindowFunction[0],     /* 154: Signal */
  &drum_bench_B.Subtract,              /* 155: Signal */
  &drum_bench_B.Buffer[0],             /* 156: Signal */
  &drum_bench_B.Buffer1[0],            /* 157: Signal */
  &drum_bench_B.Buffer2[0],            /* 158: Signal */
  &drum_bench_B.Buffer3[0],            /* 159: Signal */
  &drum_bench_B.FilteredDerivative.Filter,/* 160: Signal */
  &drum_bench_B.FilteredDerivative.FilterCoefficient,/* 161: Signal */
  &drum_bench_B.FilteredDerivative.Sum,/* 162: Signal */
  &drum_bench_B.FilteredDerivative_n.Filter,/* 163: Signal */
  &drum_bench_B.FilteredDerivative_n.FilterCoefficient,/* 164: Signal */
  &drum_bench_B.FilteredDerivative_n.Sum,/* 165: Signal */
  &drum_bench_B.DataTypeConversion_c,  /* 166: Signal */
  &drum_bench_B.Gain_a,                /* 167: Signal */
  &drum_bench_B.MultiportSwitch,       /* 168: Signal */
  &drum_bench_B.RateLimiter_b,         /* 169: Signal */
  &drum_bench_B.Sign,                  /* 170: Signal */
  &drum_bench_B.SineWave,              /* 171: Signal */
  &drum_bench_B.Sum_cn,                /* 172: Signal */
  &drum_bench_B.DataTypeConversion_n,  /* 173: Signal */
  &drum_bench_B.Gain_bc,               /* 174: Signal */
  &drum_bench_B.MultiportSwitch_e,     /* 175: Signal */
  &drum_bench_B.RateLimiter_k,         /* 176: Signal */
  &drum_bench_B.Sign_a,                /* 177: Signal */
  &drum_bench_B.SineWave_h,            /* 178: Signal */
  &drum_bench_B.Sum_df,                /* 179: Signal */
  &drum_bench_B.DataTypeConversion_d,  /* 180: Signal */
  &drum_bench_B.Gain_l,                /* 181: Signal */
  &drum_bench_B.MultiportSwitch_l,     /* 182: Signal */
  &drum_bench_B.RateLimiter_e,         /* 183: Signal */
  &drum_bench_B.Sign_m,                /* 184: Signal */
  &drum_bench_B.SineWave_f,            /* 185: Signal */
  &drum_bench_B.Sum_l,                 /* 186: Signal */
  &drum_bench_B.DataTypeConversion_b,  /* 187: Signal */
  &drum_bench_B.Gain_f,                /* 188: Signal */
  &drum_bench_B.MultiportSwitch_n,     /* 189: Signal */
  &drum_bench_B.RateLimiter,           /* 190: Signal */
  &drum_bench_B.Sign_f,                /* 191: Signal */
  &drum_bench_B.SineWave_l,            /* 192: Signal */
  &drum_bench_B.Sum_c,                 /* 193: Signal */
  &drum_bench_B.DataTypeConversion_a,  /* 194: Signal */
  &drum_bench_B.Gain_gk,               /* 195: Signal */
  &drum_bench_B.MultiportSwitch_b,     /* 196: Signal */
  &drum_bench_B.RateLimiter_o,         /* 197: Signal */
  &drum_bench_B.Sign_i,                /* 198: Signal */
  &drum_bench_B.SineWave_n,            /* 199: Signal */
  &drum_bench_B.Sum_p,                 /* 200: Signal */
  &drum_bench_B.DataTypeConversion_l,  /* 201: Signal */
  &drum_bench_B.Gain_ncb,              /* 202: Signal */
  &drum_bench_B.MultiportSwitch_h,     /* 203: Signal */
  &drum_bench_B.RateLimiter_b5,        /* 204: Signal */
  &drum_bench_B.Sign_ml,               /* 205: Signal */
  &drum_bench_B.SineWave_ff,           /* 206: Signal */
  &drum_bench_B.Sum_g,                 /* 207: Signal */
  &drum_bench_B.DataTypeConversion_j,  /* 208: Signal */
  &drum_bench_B.Gain_gv,               /* 209: Signal */
  &drum_bench_B.MultiportSwitch_o,     /* 210: Signal */
  &drum_bench_B.RateLimiter_eb,        /* 211: Signal */
  &drum_bench_B.Sign_h,                /* 212: Signal */
  &drum_bench_B.SineWave_hf,           /* 213: Signal */
  &drum_bench_B.Sum_b,                 /* 214: Signal */
  &drum_bench_B.DataTypeConversion_e,  /* 215: Signal */
  &drum_bench_B.Gain_i,                /* 216: Signal */
  &drum_bench_B.MultiportSwitch_a,     /* 217: Signal */
  &drum_bench_B.RateLimiter_ol,        /* 218: Signal */
  &drum_bench_B.Sign_k,                /* 219: Signal */
  &drum_bench_B.SineWave_g,            /* 220: Signal */
  &drum_bench_B.Sum_o,                 /* 221: Signal */
  &drum_bench_B.DataTypeConversion_m,  /* 222: Signal */
  &drum_bench_B.Gain_m,                /* 223: Signal */
  &drum_bench_B.MultiportSwitch_i,     /* 224: Signal */
  &drum_bench_B.RateLimiter_d,         /* 225: Signal */
  &drum_bench_B.Sign_l,                /* 226: Signal */
  &drum_bench_B.SineWave_m,            /* 227: Signal */
  &drum_bench_B.Sum_cq,                /* 228: Signal */
  &drum_bench_B.DataTypeConversion_c2, /* 229: Signal */
  &drum_bench_B.Gain_nh,               /* 230: Signal */
  &drum_bench_B.MultiportSwitch_k,     /* 231: Signal */
  &drum_bench_B.RateLimiter_f,         /* 232: Signal */
  &drum_bench_B.Sign_ip,               /* 233: Signal */
  &drum_bench_B.SineWave_ld,           /* 234: Signal */
  &drum_bench_B.Sum_a,                 /* 235: Signal */
  &drum_bench_B.motor_torque_m,        /* 236: Signal */
  &drum_bench_B.motor_iq_a,            /* 237: Signal */
  &drum_bench_B.motor_vq_avg_f,        /* 238: Signal */
  &drum_bench_B.analog_1_g,            /* 239: Signal */
  &drum_bench_B.analog_1_dot_h,        /* 240: Signal */
  &drum_bench_B.analog_2_k,            /* 241: Signal */
  &drum_bench_B.analog_2_dot_m,        /* 242: Signal */
  &drum_bench_B.analog_diff_h,         /* 243: Signal */
  &drum_bench_B.analog_diff_dot_k,     /* 244: Signal */
  &drum_bench_B.quadrature_1_ha,       /* 245: Signal */
  &drum_bench_B.quadrature_1_dot_l,    /* 246: Signal */
  &drum_bench_B.quadrature_2_o,        /* 247: Signal */
  &drum_bench_B.quadrature_2_dot_l,    /* 248: Signal */
  &drum_bench_B.ssi_c,                 /* 249: Signal */
  &drum_bench_B.ssi_dot_f,             /* 250: Signal */
  &drum_bench_B.accelerometer_x_n,     /* 251: Signal */
  &drum_bench_B.accelerometer_y_a,     /* 252: Signal */
  &drum_bench_B.accelerometer_z_l,     /* 253: Signal */
  &drum_bench_B.faults_p,              /* 254: Signal */
  &drum_bench_B.bus_v_k,               /* 255: Signal */
  &drum_bench_B.rpa_packet_k[0],       /* 256: Signal */
  &drum_bench_B.accelerometer_x_e,     /* 257: Signal */
  &drum_bench_B.accelerometer_y_b,     /* 258: Signal */
  &drum_bench_B.accelerometer_z_d,     /* 259: Signal */
  &drum_bench_B.analog_1_o,            /* 260: Signal */
  &drum_bench_B.analog_1_dot_d,        /* 261: Signal */
  &drum_bench_B.analog_2_p,            /* 262: Signal */
  &drum_bench_B.analog_2_dot_l,        /* 263: Signal */
  &drum_bench_B.analog_diff_b,         /* 264: Signal */
  &drum_bench_B.analog_diff_dot_f,     /* 265: Signal */
  &drum_bench_B.bus_v_d,               /* 266: Signal */
  &drum_bench_B.motor_iq_g,            /* 267: Signal */
  &drum_bench_B.motor_torque_f,        /* 268: Signal */
  &drum_bench_B.motor_vq_avg_b,        /* 269: Signal */
  &drum_bench_B.quadrature_1_j,        /* 270: Signal */
  &drum_bench_B.quadrature_1_dot_c,    /* 271: Signal */
  &drum_bench_B.quadrature_2_l,        /* 272: Signal */
  &drum_bench_B.quadrature_2_dot_o,    /* 273: Signal */
  &drum_bench_B.ssi_f,                 /* 274: Signal */
  &drum_bench_B.ssi_dot_k,             /* 275: Signal */
  &drum_bench_B.rpa_float[0],          /* 276: Signal */
  &drum_bench_B.motor_torque_p,        /* 277: Signal */
  &drum_bench_B.motor_iq_f,            /* 278: Signal */
  &drum_bench_B.motor_vq_avg_n,        /* 279: Signal */
  &drum_bench_B.analog_1_m,            /* 280: Signal */
  &drum_bench_B.analog_1_dot_f,        /* 281: Signal */
  &drum_bench_B.analog_2_b,            /* 282: Signal */
  &drum_bench_B.analog_2_dot_e,        /* 283: Signal */
  &drum_bench_B.analog_diff_hz,        /* 284: Signal */
  &drum_bench_B.analog_diff_dot_h,     /* 285: Signal */
  &drum_bench_B.quadrature_1_a,        /* 286: Signal */
  &drum_bench_B.quadrature_1_dot_j,    /* 287: Signal */
  &drum_bench_B.quadrature_2_lz,       /* 288: Signal */
  &drum_bench_B.quadrature_2_dot_o4,   /* 289: Signal */
  &drum_bench_B.ssi_n,                 /* 290: Signal */
  &drum_bench_B.ssi_dot_l,             /* 291: Signal */
  &drum_bench_B.accelerometer_x_fe,    /* 292: Signal */
  &drum_bench_B.accelerometer_y_jg,    /* 293: Signal */
  &drum_bench_B.accelerometer_z_g,     /* 294: Signal */
  &drum_bench_B.faults_o,              /* 295: Signal */
  &drum_bench_B.bus_v_p,               /* 296: Signal */
  &drum_bench_B.rpa_packet_g[0],       /* 297: Signal */
  &drum_bench_B.accelerometer_x_d,     /* 298: Signal */
  &drum_bench_B.accelerometer_y_e,     /* 299: Signal */
  &drum_bench_B.accelerometer_z_bk,    /* 300: Signal */
  &drum_bench_B.analog_1_b,            /* 301: Signal */
  &drum_bench_B.analog_1_dot_b,        /* 302: Signal */
  &drum_bench_B.analog_2_a,            /* 303: Signal */
  &drum_bench_B.analog_2_dot_k,        /* 304: Signal */
  &drum_bench_B.analog_diff_ni,        /* 305: Signal */
  &drum_bench_B.analog_diff_dot_i,     /* 306: Signal */
  &drum_bench_B.bus_v_pu,              /* 307: Signal */
  &drum_bench_B.motor_iq_gt,           /* 308: Signal */
  &drum_bench_B.motor_torque_pz,       /* 309: Signal */
  &drum_bench_B.motor_vq_avg_is,       /* 310: Signal */
  &drum_bench_B.quadrature_1_ib,       /* 311: Signal */
  &drum_bench_B.quadrature_1_dot_f,    /* 312: Signal */
  &drum_bench_B.quadrature_2_g,        /* 313: Signal */
  &drum_bench_B.quadrature_2_dot_e,    /* 314: Signal */
  &drum_bench_B.ssi_h,                 /* 315: Signal */
  &drum_bench_B.ssi_dot_o1,            /* 316: Signal */
  &drum_bench_B.rpa_float_f[0],        /* 317: Signal */
  &drum_bench_B.mode_k,                /* 318: Signal */
  &drum_bench_B.motor_iq_kg,           /* 319: Signal */
  &drum_bench_B.motor_id,              /* 320: Signal */
  &drum_bench_B.motor_vq,              /* 321: Signal */
  &drum_bench_B.motor_vd,              /* 322: Signal */
  &drum_bench_B.analog_diff_g1,        /* 323: Signal */
  &drum_bench_B.analog_diff_dot_o,     /* 324: Signal */
  &drum_bench_B.quadrature_1_angle_b,  /* 325: Signal */
  &drum_bench_B.quadrature_1_angle_dot_j,/* 326: Signal */
  &drum_bench_B.motor_torque_mv,       /* 327: Signal */
  &drum_bench_B.mode_a,                /* 328: Signal */
  &drum_bench_B.command_e[0],          /* 329: Signal */
  &drum_bench_B.eA1[0],                /* 330: Signal */
  &drum_bench_B.eA2[0],                /* 331: Signal */
  &drum_bench_B.resistance,            /* 332: Signal */
  &drum_bench_B.MathFunction,          /* 333: Signal */
  &drum_bench_B.Sum[0],                /* 334: Signal */
  &drum_bench_B.Delay[0],              /* 335: Signal */
  &drum_bench_B.sf_selectrpc.type,     /* 336: Signal */
  &drum_bench_B.sf_selectrpc.id,       /* 337: Signal */
  &drum_bench_B.sf_selectrpc.data[0],  /* 338: Signal */
  &drum_bench_B.VectorConcatenate[0],  /* 339: Signal */
  &drum_bench_B.rpc_i[0],              /* 340: Signal */
  &drum_bench_B.type,                  /* 341: Signal */
  &drum_bench_B.id,                    /* 342: Signal */
  &drum_bench_B.data[0],               /* 343: Signal */
  &drum_bench_B.mode_g,                /* 344: Signal */
  &drum_bench_B.motor_iq_m,            /* 345: Signal */
  &drum_bench_B.motor_id_k,            /* 346: Signal */
  &drum_bench_B.motor_vq_d,            /* 347: Signal */
  &drum_bench_B.motor_vd_m,            /* 348: Signal */
  &drum_bench_B.analog_diff_i,         /* 349: Signal */
  &drum_bench_B.analog_diff_dot_ky,    /* 350: Signal */
  &drum_bench_B.quadrature_1_angle_c,  /* 351: Signal */
  &drum_bench_B.quadrature_1_angle_dot_d,/* 352: Signal */
  &drum_bench_B.motor_torque_hy,       /* 353: Signal */
  &drum_bench_B.mode_e,                /* 354: Signal */
  &drum_bench_B.command_c[0],          /* 355: Signal */
  &drum_bench_B.eA1_c[0],              /* 356: Signal */
  &drum_bench_B.eA2_f[0],              /* 357: Signal */
  &drum_bench_B.resistance_e,          /* 358: Signal */
  &drum_bench_B.MathFunction_p,        /* 359: Signal */
  &drum_bench_B.Sum_d[0],              /* 360: Signal */
  &drum_bench_B.Delay_l[0],            /* 361: Signal */
  &drum_bench_B.sf_selectrpc_j.type,   /* 362: Signal */
  &drum_bench_B.sf_selectrpc_j.id,     /* 363: Signal */
  &drum_bench_B.sf_selectrpc_j.data[0],/* 364: Signal */
  &drum_bench_B.VectorConcatenate_a[0],/* 365: Signal */
  &drum_bench_B.rpc_n[0],              /* 366: Signal */
  &drum_bench_B.type_b,                /* 367: Signal */
  &drum_bench_B.id_p,                  /* 368: Signal */
  &drum_bench_B.data_a[0],             /* 369: Signal */
  &drum_bench_B.motor_torque_b,        /* 370: Signal */
  &drum_bench_B.motor_iq_k,            /* 371: Signal */
  &drum_bench_B.motor_vq_avg_h,        /* 372: Signal */
  &drum_bench_B.analog_1_k,            /* 373: Signal */
  &drum_bench_B.analog_1_dot_e,        /* 374: Signal */
  &drum_bench_B.analog_2_o,            /* 375: Signal */
  &drum_bench_B.analog_2_dot_i,        /* 376: Signal */
  &drum_bench_B.analog_diff_g,         /* 377: Signal */
  &drum_bench_B.analog_diff_dot_b,     /* 378: Signal */
  &drum_bench_B.quadrature_1_h,        /* 379: Signal */
  &drum_bench_B.quadrature_1_dot_m,    /* 380: Signal */
  &drum_bench_B.quadrature_2_m,        /* 381: Signal */
  &drum_bench_B.quadrature_2_dot_h,    /* 382: Signal */
  &drum_bench_B.ssi_g,                 /* 383: Signal */
  &drum_bench_B.ssi_dot_g,             /* 384: Signal */
  &drum_bench_B.accelerometer_x_f,     /* 385: Signal */
  &drum_bench_B.accelerometer_y_n,     /* 386: Signal */
  &drum_bench_B.accelerometer_z_m,     /* 387: Signal */
  &drum_bench_B.faults_c,              /* 388: Signal */
  &drum_bench_B.bus_v_i,               /* 389: Signal */
  &drum_bench_B.rpa_packet_n[0],       /* 390: Signal */
  &drum_bench_B.motor_torque_h,        /* 391: Signal */
  &drum_bench_B.motor_iq_d,            /* 392: Signal */
  &drum_bench_B.motor_vq_avg_i,        /* 393: Signal */
  &drum_bench_B.analog_1_gs,           /* 394: Signal */
  &drum_bench_B.analog_1_dot_j,        /* 395: Signal */
  &drum_bench_B.analog_2_f,            /* 396: Signal */
  &drum_bench_B.analog_2_dot_c,        /* 397: Signal */
  &drum_bench_B.analog_diff_n,         /* 398: Signal */
  &drum_bench_B.analog_diff_dot_e,     /* 399: Signal */
  &drum_bench_B.quadrature_1_c,        /* 400: Signal */
  &drum_bench_B.quadrature_1_dot_o,    /* 401: Signal */
  &drum_bench_B.quadrature_2_hx,       /* 402: Signal */
  &drum_bench_B.quadrature_2_dot_g,    /* 403: Signal */
  &drum_bench_B.ssi_o,                 /* 404: Signal */
  &drum_bench_B.ssi_dot_o,             /* 405: Signal */
  &drum_bench_B.accelerometer_x_l,     /* 406: Signal */
  &drum_bench_B.accelerometer_y_m,     /* 407: Signal */
  &drum_bench_B.accelerometer_z_b,     /* 408: Signal */
  &drum_bench_B.faults_k5,             /* 409: Signal */
  &drum_bench_B.bus_v_a,               /* 410: Signal */
  &drum_bench_B.rpa_packet_i[0],       /* 411: Signal */
  &drum_bench_B.FilteredDerivative_c.Filter,/* 412: Signal */
  &drum_bench_B.FilteredDerivative_c.FilterCoefficient,/* 413: Signal */
  &drum_bench_B.FilteredDerivative_c.Sum,/* 414: Signal */
  &drum_bench_B.FilteredDerivative_p.Filter,/* 415: Signal */
  &drum_bench_B.FilteredDerivative_p.FilterCoefficient,/* 416: Signal */
  &drum_bench_B.FilteredDerivative_p.Sum,/* 417: Signal */
  &drum_bench_B.GeneratedFilterBlock_gi,/* 418: Signal */
  &drum_bench_B.MagnitudeSquared_h[0], /* 419: Signal */
  &drum_bench_B.FFT_f[0].re,           /* 420: Signal */
  &drum_bench_B.GeneratedFilterBlock_n,/* 421: Signal */
  &drum_bench_B.GeneratedFilterBlock_o,/* 422: Signal */
  &drum_bench_B.MagnitudeSquared[0],   /* 423: Signal */
  &drum_bench_B.FFT[0].re,             /* 424: Signal */
  &drum_bench_B.GeneratedFilterBlock_j,/* 425: Signal */
  &drum_bench_B.Clock1,                /* 426: Signal */
  &drum_bench_B.Gain_g,                /* 427: Signal */
  &drum_bench_B.Product_j,             /* 428: Signal */
  &drum_bench_B.Product1_k,            /* 429: Signal */
  &drum_bench_B.Product2_n,            /* 430: Signal */
  &drum_bench_B.Sum_j,                 /* 431: Signal */
  &drum_bench_B.Output,                /* 432: Signal */
  &drum_bench_B.Clock1_n,              /* 433: Signal */
  &drum_bench_B.Gain_b,                /* 434: Signal */
  &drum_bench_B.Product_p,             /* 435: Signal */
  &drum_bench_B.Product1_o,            /* 436: Signal */
  &drum_bench_B.Product2_p,            /* 437: Signal */
  &drum_bench_B.Sum_k,                 /* 438: Signal */
  &drum_bench_B.Output_p,              /* 439: Signal */
  &drum_bench_B.Clock1_e,              /* 440: Signal */
  &drum_bench_B.Gain_nn,               /* 441: Signal */
  &drum_bench_B.Product_b,             /* 442: Signal */
  &drum_bench_B.Product1_c,            /* 443: Signal */
  &drum_bench_B.Product2_no,           /* 444: Signal */
  &drum_bench_B.Sum_kn,                /* 445: Signal */
  &drum_bench_B.Output_n,              /* 446: Signal */
  &drum_bench_B.Clock1_g,              /* 447: Signal */
  &drum_bench_B.Gain_nc,               /* 448: Signal */
  &drum_bench_B.Product_d,             /* 449: Signal */
  &drum_bench_B.Product1_i,            /* 450: Signal */
  &drum_bench_B.Product2_i,            /* 451: Signal */
  &drum_bench_B.Sum_i,                 /* 452: Signal */
  &drum_bench_B.Output_h,              /* 453: Signal */
  &drum_bench_B.Clock1_j,              /* 454: Signal */
  &drum_bench_B.Gain_nz,               /* 455: Signal */
  &drum_bench_B.Product_h,             /* 456: Signal */
  &drum_bench_B.Product1_f,            /* 457: Signal */
  &drum_bench_B.Product2_l,            /* 458: Signal */
  &drum_bench_B.Sum_bg,                /* 459: Signal */
  &drum_bench_B.Output_c,              /* 460: Signal */
  &drum_bench_B.Clock1_gp,             /* 461: Signal */
  &drum_bench_B.Gain_a3,               /* 462: Signal */
  &drum_bench_B.Product_bh,            /* 463: Signal */
  &drum_bench_B.Product1_e,            /* 464: Signal */
  &drum_bench_B.Product2_d,            /* 465: Signal */
  &drum_bench_B.Sum_i2,                /* 466: Signal */
  &drum_bench_B.Output_i,              /* 467: Signal */
  &drum_bench_B.Clock1_f,              /* 468: Signal */
  &drum_bench_B.Gain_d,                /* 469: Signal */
  &drum_bench_B.Product_ie,            /* 470: Signal */
  &drum_bench_B.Product1_p,            /* 471: Signal */
  &drum_bench_B.Product2_np,           /* 472: Signal */
  &drum_bench_B.Sum_c3,                /* 473: Signal */
  &drum_bench_B.Output_f,              /* 474: Signal */
  &drum_bench_B.Clock1_l,              /* 475: Signal */
  &drum_bench_B.Gain_h,                /* 476: Signal */
  &drum_bench_B.Product_k,             /* 477: Signal */
  &drum_bench_B.Product1_pt,           /* 478: Signal */
  &drum_bench_B.Product2_j,            /* 479: Signal */
  &drum_bench_B.Sum_dl,                /* 480: Signal */
  &drum_bench_B.Output_e,              /* 481: Signal */
  &drum_bench_B.Clock1_d,              /* 482: Signal */
  &drum_bench_B.Gain_c,                /* 483: Signal */
  &drum_bench_B.Product_kg,            /* 484: Signal */
  &drum_bench_B.Product1_jw,           /* 485: Signal */
  &drum_bench_B.Product2_gz,           /* 486: Signal */
  &drum_bench_B.Sum_en,                /* 487: Signal */
  &drum_bench_B.Output_m,              /* 488: Signal */
  &drum_bench_B.Clock1_h,              /* 489: Signal */
  &drum_bench_B.Gain_p,                /* 490: Signal */
  &drum_bench_B.Product_if,            /* 491: Signal */
  &drum_bench_B.Product1_j2,           /* 492: Signal */
  &drum_bench_B.Product2_gp,           /* 493: Signal */
  &drum_bench_B.Sum_ke,                /* 494: Signal */
  &drum_bench_B.Output_l,              /* 495: Signal */
  &drum_bench_B.analog,                /* 496: Signal */
  &drum_bench_B.communication_fault,   /* 497: Signal */
  &drum_bench_B.current,               /* 498: Signal */
  &drum_bench_B.ethercat_m,            /* 499: Signal */
  &drum_bench_B.external_panic_e,      /* 500: Signal */
  &drum_bench_B.hard_fault,            /* 501: Signal */
  &drum_bench_B.quadrature,            /* 502: Signal */
  &drum_bench_B.raw_integer,           /* 503: Signal */
  &drum_bench_B.soft_fault,            /* 504: Signal */
  &drum_bench_B.ssi_i,                 /* 505: Signal */
  &drum_bench_B.temperature,           /* 506: Signal */
  &drum_bench_B.voltage,               /* 507: Signal */
  &drum_bench_B.analog_i,              /* 508: Signal */
  &drum_bench_B.communication_fault_o, /* 509: Signal */
  &drum_bench_B.current_h,             /* 510: Signal */
  &drum_bench_B.ethercat_e,            /* 511: Signal */
  &drum_bench_B.external_panic_o,      /* 512: Signal */
  &drum_bench_B.hard_fault_e,          /* 513: Signal */
  &drum_bench_B.quadrature_l,          /* 514: Signal */
  &drum_bench_B.raw_integer_l,         /* 515: Signal */
  &drum_bench_B.soft_fault_a,          /* 516: Signal */
  &drum_bench_B.ssi_m,                 /* 517: Signal */
  &drum_bench_B.temperature_b,         /* 518: Signal */
  &drum_bench_B.voltage_f,             /* 519: Signal */
  &drum_bench_B.accelerometer_x,       /* 520: Signal */
  &drum_bench_B.accelerometer_y,       /* 521: Signal */
  &drum_bench_B.accelerometer_z,       /* 522: Signal */
  &drum_bench_B.analog_1,              /* 523: Signal */
  &drum_bench_B.analog_1_dot,          /* 524: Signal */
  &drum_bench_B.analog_2,              /* 525: Signal */
  &drum_bench_B.analog_2_dot,          /* 526: Signal */
  &drum_bench_B.analog_diff,           /* 527: Signal */
  &drum_bench_B.analog_diff_dot,       /* 528: Signal */
  &drum_bench_B.bus_v,                 /* 529: Signal */
  &drum_bench_B.debug,                 /* 530: Signal */
  &drum_bench_B.faults,                /* 531: Signal */
  &drum_bench_B.motor_iq,              /* 532: Signal */
  &drum_bench_B.motor_torque,          /* 533: Signal */
  &drum_bench_B.motor_vq_avg,          /* 534: Signal */
  &drum_bench_B.quadrature_1,          /* 535: Signal */
  &drum_bench_B.quadrature_1_dot,      /* 536: Signal */
  &drum_bench_B.quadrature_2,          /* 537: Signal */
  &drum_bench_B.quadrature_2_dot,      /* 538: Signal */
  &drum_bench_B.rpa_packet[0],         /* 539: Signal */
  &drum_bench_B.ssi,                   /* 540: Signal */
  &drum_bench_B.ssi_dot,               /* 541: Signal */
  &drum_bench_B.marlin_ec_o1,          /* 542: Signal */
  &drum_bench_B.marlin_ec_o2,          /* 543: Signal */
  &drum_bench_B.marlin_ec_o3,          /* 544: Signal */
  &drum_bench_B.marlin_ec_o4,          /* 545: Signal */
  &drum_bench_B.marlin_ec_o5,          /* 546: Signal */
  &drum_bench_B.marlin_ec_o6,          /* 547: Signal */
  &drum_bench_B.marlin_ec_o7,          /* 548: Signal */
  &drum_bench_B.marlin_ec_o8,          /* 549: Signal */
  &drum_bench_B.marlin_ec_o9,          /* 550: Signal */
  &drum_bench_B.marlin_ec_o10,         /* 551: Signal */
  &drum_bench_B.marlin_ec_o11,         /* 552: Signal */
  &drum_bench_B.marlin_ec_o12,         /* 553: Signal */
  &drum_bench_B.marlin_ec_o13,         /* 554: Signal */
  &drum_bench_B.marlin_ec_o14,         /* 555: Signal */
  &drum_bench_B.marlin_ec_o15,         /* 556: Signal */
  &drum_bench_B.marlin_ec_o16,         /* 557: Signal */
  &drum_bench_B.marlin_ec_o17,         /* 558: Signal */
  &drum_bench_B.marlin_ec_o18,         /* 559: Signal */
  &drum_bench_B.marlin_ec_o19,         /* 560: Signal */
  &drum_bench_B.marlin_ec_o20,         /* 561: Signal */
  &drum_bench_B.marlin_ec_o21,         /* 562: Signal */
  &drum_bench_B.marlin_ec_o22[0],      /* 563: Signal */
  &drum_bench_B.DataTypeConversion1_fh[0],/* 564: Signal */
  &drum_bench_B.command_g[0],          /* 565: Signal */
  &drum_bench_B.DataTypeConversion1_o[0],/* 566: Signal */
  &drum_bench_B.command_j[0],          /* 567: Signal */
  &drum_bench_B.DataTypeConversion1_b[0],/* 568: Signal */
  &drum_bench_B.command_l[0],          /* 569: Signal */
  &drum_bench_B.DataTypeConversion1_h[0],/* 570: Signal */
  &drum_bench_B.command_a[0],          /* 571: Signal */
  &drum_bench_B.DataTypeConversion1_f[0],/* 572: Signal */
  &drum_bench_B.command[0],            /* 573: Signal */
  &drum_bench_B.DataTypeConversion1_oj[0],/* 574: Signal */
  &drum_bench_B.command_k[0],          /* 575: Signal */
  &drum_bench_B.DataTypeConversion1_fm[0],/* 576: Signal */
  &drum_bench_B.command_gi[0],         /* 577: Signal */
  &drum_bench_B.DataTypeConversion1_e[0],/* 578: Signal */
  &drum_bench_B.command_f[0],          /* 579: Signal */
  &drum_bench_B.BusCreator_nf,         /* 580: Signal */
  (void *) &drum_bench_ConstB.data[0], /* 581: Signal */
  &drum_bench_B.BusCreator_k,          /* 582: Signal */
  (void *) &drum_bench_ConstB.data_p[0],/* 583: Signal */
  &drum_bench_B.BusCreator_iq,         /* 584: Signal */
  (void *) &drum_bench_ConstB.data_f[0],/* 585: Signal */
  &drum_bench_B.BusCreator_o,          /* 586: Signal */
  (void *) &drum_bench_ConstB.data_c[0],/* 587: Signal */
  &drum_bench_B.BusCreator_l,          /* 588: Signal */
  (void *) &drum_bench_ConstB.data_h[0],/* 589: Signal */
  &drum_bench_B.enabledsubsystem.In1[0],/* 590: Signal */
  &drum_bench_B.BusCreator_d,          /* 591: Signal */
  &drum_bench_B.DataTypeConversion_cm[0],/* 592: Signal */
  &drum_bench_B.analog_limit,          /* 593: Signal */
  &drum_bench_B.ethercat,              /* 594: Signal */
  &drum_bench_B.external_panic,        /* 595: Signal */
  &drum_bench_B.motor_encoder,         /* 596: Signal */
  &drum_bench_B.over_current,          /* 597: Signal */
  &drum_bench_B.over_voltage,          /* 598: Signal */
  &drum_bench_B.quadrature_1_high,     /* 599: Signal */
  &drum_bench_B.quadrature_1_low,      /* 600: Signal */
  &drum_bench_B.quadrature_2_limit,    /* 601: Signal */
  &drum_bench_B.under_voltage,         /* 602: Signal */
  (void *) &drum_bench_ConstB.data_d[0],/* 603: Signal */
  &drum_bench_B.ByteUnpack_e[0],       /* 604: Signal */
  &drum_bench_B.EnabledSubsystem.In1[0],/* 605: Signal */
  &drum_bench_B.BusCreator_ld,         /* 606: Signal */
  &drum_bench_B.loada,                 /* 607: Signal */
  &drum_bench_B.loada1,                /* 608: Signal */
  &drum_bench_B.loada2,                /* 609: Signal */
  &drum_bench_B.loada3,                /* 610: Signal */
  &drum_bench_B.loadb,                 /* 611: Signal */
  (void *) &drum_bench_ConstB.data_cz[0],/* 612: Signal */
  &drum_bench_B.ByteUnpack[0],         /* 613: Signal */
  &drum_bench_B.BusCreator_c,          /* 614: Signal */
  &drum_bench_B.DataTypeConversion_g[0],/* 615: Signal */
  &drum_bench_B.data_e[0],             /* 616: Signal */
  &drum_bench_B.BusCreator_hl,         /* 617: Signal */
  &drum_bench_B.DataTypeConversion_h[0],/* 618: Signal */
  &drum_bench_B.data_b[0],             /* 619: Signal */
  &drum_bench_B.BusCreator_nh,         /* 620: Signal */
  &drum_bench_B.DataTypeConversion_ee[0],/* 621: Signal */
  &drum_bench_B.data_n[0],             /* 622: Signal */
  &drum_bench_B.current_index,         /* 623: Signal */
  &drum_bench_B.BusCreator_h,          /* 624: Signal */
  (void *) &drum_bench_ConstB.data_e[0],/* 625: Signal */
  &drum_bench_B.BusCreator_g,          /* 626: Signal */
  &drum_bench_B.BusCreator_bc,         /* 627: Signal */
  (void *) &drum_bench_ConstB.data_o[0],/* 628: Signal */
  &drum_bench_B.BusCreator_b,          /* 629: Signal */
  (void *) &drum_bench_ConstB.data_cu[0],/* 630: Signal */
  &drum_bench_B.accelerometer_x_ej,    /* 631: Signal */
  &drum_bench_B.accelerometer_y_j,     /* 632: Signal */
  &drum_bench_B.accelerometer_z_i,     /* 633: Signal */
  &drum_bench_B.analog_1_on,           /* 634: Signal */
  &drum_bench_B.analog_1_dot_k,        /* 635: Signal */
  &drum_bench_B.analog_2_h,            /* 636: Signal */
  &drum_bench_B.analog_2_dot_h,        /* 637: Signal */
  &drum_bench_B.analog_diff_o,         /* 638: Signal */
  &drum_bench_B.analog_diff_dot_m,     /* 639: Signal */
  &drum_bench_B.bus_v_l,               /* 640: Signal */
  &drum_bench_B.debug_d,               /* 641: Signal */
  &drum_bench_B.faults_k,              /* 642: Signal */
  &drum_bench_B.motor_iq_b,            /* 643: Signal */
  &drum_bench_B.motor_torque_l,        /* 644: Signal */
  &drum_bench_B.motor_vq_avg_e,        /* 645: Signal */
  &drum_bench_B.quadrature_1_i,        /* 646: Signal */
  &drum_bench_B.quadrature_1_dot_e,    /* 647: Signal */
  &drum_bench_B.quadrature_2_h,        /* 648: Signal */
  &drum_bench_B.quadrature_2_dot_b,    /* 649: Signal */
  &drum_bench_B.rpa_packet_b[0],       /* 650: Signal */
  &drum_bench_B.ssi_b,                 /* 651: Signal */
  &drum_bench_B.ssi_dot_b,             /* 652: Signal */
  &drum_bench_B.marlin_ec_o1_k,        /* 653: Signal */
  &drum_bench_B.marlin_ec_o2_f,        /* 654: Signal */
  &drum_bench_B.marlin_ec_o3_h,        /* 655: Signal */
  &drum_bench_B.marlin_ec_o4_a,        /* 656: Signal */
  &drum_bench_B.marlin_ec_o5_l,        /* 657: Signal */
  &drum_bench_B.marlin_ec_o6_j,        /* 658: Signal */
  &drum_bench_B.marlin_ec_o7_c,        /* 659: Signal */
  &drum_bench_B.marlin_ec_o8_j,        /* 660: Signal */
  &drum_bench_B.marlin_ec_o9_j,        /* 661: Signal */
  &drum_bench_B.marlin_ec_o10_n,       /* 662: Signal */
  &drum_bench_B.marlin_ec_o11_d,       /* 663: Signal */
  &drum_bench_B.marlin_ec_o12_h,       /* 664: Signal */
  &drum_bench_B.marlin_ec_o13_b,       /* 665: Signal */
  &drum_bench_B.marlin_ec_o14_e,       /* 666: Signal */
  &drum_bench_B.marlin_ec_o15_h,       /* 667: Signal */
  &drum_bench_B.marlin_ec_o16_c,       /* 668: Signal */
  &drum_bench_B.marlin_ec_o17_a,       /* 669: Signal */
  &drum_bench_B.marlin_ec_o18_g,       /* 670: Signal */
  &drum_bench_B.marlin_ec_o19_n,       /* 671: Signal */
  &drum_bench_B.marlin_ec_o20_k,       /* 672: Signal */
  &drum_bench_B.marlin_ec_o21_m,       /* 673: Signal */
  &drum_bench_B.marlin_ec_o22_a[0],    /* 674: Signal */
  &drum_bench_B.DataTypeConversion1_fj[0],/* 675: Signal */
  &drum_bench_B.command_hl[0],         /* 676: Signal */
  &drum_bench_B.DataTypeConversion1_bm[0],/* 677: Signal */
  &drum_bench_B.command_n[0],          /* 678: Signal */
  &drum_bench_B.DataTypeConversion1_a[0],/* 679: Signal */
  &drum_bench_B.command_i[0],          /* 680: Signal */
  &drum_bench_B.DataTypeConversion1_c[0],/* 681: Signal */
  &drum_bench_B.command_jo[0],         /* 682: Signal */
  &drum_bench_B.DataTypeConversion1_d[0],/* 683: Signal */
  &drum_bench_B.command_li[0],         /* 684: Signal */
  &drum_bench_B.DataTypeConversion1_ev[0],/* 685: Signal */
  &drum_bench_B.command_o[0],          /* 686: Signal */
  &drum_bench_B.DataTypeConversion1_aj[0],/* 687: Signal */
  &drum_bench_B.command_gh[0],         /* 688: Signal */
  &drum_bench_B.DataTypeConversion1_ey[0],/* 689: Signal */
  &drum_bench_B.command_h[0],          /* 690: Signal */
  &drum_bench_B.BusCreator_br,         /* 691: Signal */
  (void *) &drum_bench_ConstB.data_ef[0],/* 692: Signal */
  &drum_bench_B.BusCreator_ih,         /* 693: Signal */
  (void *) &drum_bench_ConstB.data_c4[0],/* 694: Signal */
  &drum_bench_B.BusCreator_g1,         /* 695: Signal */
  (void *) &drum_bench_ConstB.data_g[0],/* 696: Signal */
  &drum_bench_B.BusCreator_i2,         /* 697: Signal */
  (void *) &drum_bench_ConstB.data_b[0],/* 698: Signal */
  &drum_bench_B.BusCreator_lg,         /* 699: Signal */
  (void *) &drum_bench_ConstB.data_fw[0],/* 700: Signal */
  &drum_bench_B.enabledsubsystem_l.In1[0],/* 701: Signal */
  &drum_bench_B.BusCreator_d1,         /* 702: Signal */
  &drum_bench_B.DataTypeConversion_i[0],/* 703: Signal */
  &drum_bench_B.analog_limit_k,        /* 704: Signal */
  &drum_bench_B.ethercat_l,            /* 705: Signal */
  &drum_bench_B.external_panic_l,      /* 706: Signal */
  &drum_bench_B.motor_encoder_h,       /* 707: Signal */
  &drum_bench_B.over_current_p,        /* 708: Signal */
  &drum_bench_B.over_voltage_o,        /* 709: Signal */
  &drum_bench_B.quadrature_1_high_p,   /* 710: Signal */
  &drum_bench_B.quadrature_1_low_c,    /* 711: Signal */
  &drum_bench_B.quadrature_2_limit_a,  /* 712: Signal */
  &drum_bench_B.under_voltage_l,       /* 713: Signal */
  (void *) &drum_bench_ConstB.data_og[0],/* 714: Signal */
  &drum_bench_B.ByteUnpack_eb[0],      /* 715: Signal */
  &drum_bench_B.EnabledSubsystem_k.In1[0],/* 716: Signal */
  &drum_bench_B.BusCreator_ow,         /* 717: Signal */
  &drum_bench_B.loada_c,               /* 718: Signal */
  &drum_bench_B.loada1_h,              /* 719: Signal */
  &drum_bench_B.loada2_p,              /* 720: Signal */
  &drum_bench_B.loada3_o,              /* 721: Signal */
  &drum_bench_B.loadb_a,               /* 722: Signal */
  (void *) &drum_bench_ConstB.data_e0[0],/* 723: Signal */
  &drum_bench_B.ByteUnpack_h[0],       /* 724: Signal */
  &drum_bench_B.BusCreator_dj,         /* 725: Signal */
  &drum_bench_B.DataTypeConversion_k[0],/* 726: Signal */
  &drum_bench_B.data_o[0],             /* 727: Signal */
  &drum_bench_B.BusCreator_om,         /* 728: Signal */
  &drum_bench_B.DataTypeConversion_k0[0],/* 729: Signal */
  &drum_bench_B.data_bl[0],            /* 730: Signal */
  &drum_bench_B.BusCreator_co,         /* 731: Signal */
  &drum_bench_B.DataTypeConversion_iv[0],/* 732: Signal */
  &drum_bench_B.data_mh[0],            /* 733: Signal */
  &drum_bench_B.current_index_c,       /* 734: Signal */
  &drum_bench_B.BusCreator_dy,         /* 735: Signal */
  (void *) &drum_bench_ConstB.data_ec[0],/* 736: Signal */
  &drum_bench_B.BusCreator_gs,         /* 737: Signal */
  &drum_bench_B.BusCreator_bw,         /* 738: Signal */
  (void *) &drum_bench_ConstB.data_ci[0],/* 739: Signal */
  &drum_bench_B.BusCreator_fc,         /* 740: Signal */
  (void *) &drum_bench_ConstB.data_bu[0],/* 741: Signal */
  &drum_bench_B.GeneratedFilterBlock_b,/* 742: Signal */
  &drum_bench_B.GeneratedFilterBlock,  /* 743: Signal */
  &drum_bench_B.GeneratedFilterBlock_c,/* 744: Signal */
  &drum_bench_B.GeneratedFilterBlock_g,/* 745: Signal */
  &drum_bench_B.GeneratedFilterBlock_b0,/* 746: Signal */
  &drum_bench_B.GeneratedFilterBlock_p,/* 747: Signal */
  &drum_bench_B.ExtractDesiredBits_n,  /* 748: Signal */
  &drum_bench_B.ModifyScalingOnly,     /* 749: Signal */
  &drum_bench_B.ExtractDesiredBits_pr, /* 750: Signal */
  &drum_bench_B.ModifyScalingOnly_c,   /* 751: Signal */
  &drum_bench_B.ExtractDesiredBits_dw, /* 752: Signal */
  &drum_bench_B.ModifyScalingOnly_j,   /* 753: Signal */
  &drum_bench_B.ExtractDesiredBits_k,  /* 754: Signal */
  &drum_bench_B.ModifyScalingOnly_b,   /* 755: Signal */
  &drum_bench_B.ExtractDesiredBits_l,  /* 756: Signal */
  &drum_bench_B.ModifyScalingOnly_jt,  /* 757: Signal */
  &drum_bench_B.ExtractDesiredBits_d,  /* 758: Signal */
  &drum_bench_B.ModifyScalingOnly_h,   /* 759: Signal */
  &drum_bench_B.ExtractDesiredBits_h,  /* 760: Signal */
  &drum_bench_B.ModifyScalingOnly_m,   /* 761: Signal */
  &drum_bench_B.ExtractDesiredBits_ng, /* 762: Signal */
  &drum_bench_B.ModifyScalingOnly_i,   /* 763: Signal */
  &drum_bench_B.ExtractDesiredBits_fo, /* 764: Signal */
  &drum_bench_B.ModifyScalingOnly_mj,  /* 765: Signal */
  &drum_bench_B.ExtractDesiredBits_cg, /* 766: Signal */
  &drum_bench_B.ModifyScalingOnly_jz,  /* 767: Signal */
  &drum_bench_B.ExtractDesiredBits,    /* 768: Signal */
  &drum_bench_B.ModifyScalingOnly_d,   /* 769: Signal */
  &drum_bench_B.ExtractDesiredBits_p,  /* 770: Signal */
  &drum_bench_B.ModifyScalingOnly_je,  /* 771: Signal */
  &drum_bench_B.ExtractDesiredBits_f,  /* 772: Signal */
  &drum_bench_B.ModifyScalingOnly_o,   /* 773: Signal */
  &drum_bench_B.ExtractDesiredBits_i1, /* 774: Signal */
  &drum_bench_B.ModifyScalingOnly_p,   /* 775: Signal */
  &drum_bench_B.ExtractDesiredBits_f2, /* 776: Signal */
  &drum_bench_B.ModifyScalingOnly_oc,  /* 777: Signal */
  &drum_bench_B.ExtractDesiredBits_j,  /* 778: Signal */
  &drum_bench_B.ModifyScalingOnly_l,   /* 779: Signal */
  &drum_bench_B.ExtractDesiredBits_lr, /* 780: Signal */
  &drum_bench_B.ModifyScalingOnly_e,   /* 781: Signal */
  &drum_bench_B.ExtractDesiredBits_o,  /* 782: Signal */
  &drum_bench_B.ModifyScalingOnly_f,   /* 783: Signal */
  &drum_bench_B.ExtractDesiredBits_g,  /* 784: Signal */
  &drum_bench_B.ModifyScalingOnly_cr,  /* 785: Signal */
  &drum_bench_B.ExtractDesiredBits_kb, /* 786: Signal */
  &drum_bench_B.ModifyScalingOnly_n,   /* 787: Signal */
  &drum_bench_B.ExtractDesiredBits_i,  /* 788: Signal */
  &drum_bench_B.ModifyScalingOnly_k,   /* 789: Signal */
  &drum_bench_B.ExtractDesiredBits_c,  /* 790: Signal */
  &drum_bench_B.ModifyScalingOnly_pi,  /* 791: Signal */
  &drum_bench_B.Output_o,              /* 792: Signal */
  &drum_bench_B.Compare,               /* 793: Signal */
  &drum_bench_B.enabledsubsystem.In1[0],/* 794: Signal */
  &drum_bench_B.Compare_a,             /* 795: Signal */
  &drum_bench_B.EnabledSubsystem.In1[0],/* 796: Signal */
  &drum_bench_B.sf_MATLABFunction1.type,/* 797: Signal */
  &drum_bench_B.sf_MATLABFunction1.id, /* 798: Signal */
  &drum_bench_B.sf_MATLABFunction1.index,/* 799: Signal */
  &drum_bench_B.sf_MATLABFunction1.value,/* 800: Signal */
  &drum_bench_B.BusCreator_a,          /* 801: Signal */
  &drum_bench_B.data_m[0],             /* 802: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 803: Signal */
  &drum_bench_B.DataTypeConversion_f[0],/* 804: Signal */
  &drum_bench_B.Output_i4,             /* 805: Signal */
  &drum_bench_B.Compare_b,             /* 806: Signal */
  &drum_bench_B.enabledsubsystem_l.In1[0],/* 807: Signal */
  &drum_bench_B.Compare_aw,            /* 808: Signal */
  &drum_bench_B.EnabledSubsystem_k.In1[0],/* 809: Signal */
  &drum_bench_B.sf_MATLABFunction1_a.type,/* 810: Signal */
  &drum_bench_B.sf_MATLABFunction1_a.id,/* 811: Signal */
  &drum_bench_B.sf_MATLABFunction1_a.index,/* 812: Signal */
  &drum_bench_B.sf_MATLABFunction1_a.value,/* 813: Signal */
  &drum_bench_B.BusCreator_j,          /* 814: Signal */
  &drum_bench_B.data_e5[0],            /* 815: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 816: Signal */
  &drum_bench_B.DataTypeConversion_aa[0],/* 817: Signal */
  &drum_bench_B.FixPtSum1,             /* 818: Signal */
  &drum_bench_B.FixPtSwitch,           /* 819: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 820: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 821: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 822: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 823: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 824: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 825: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 826: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 827: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 828: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 829: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 830: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 831: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 832: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 833: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 834: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 835: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 836: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 837: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 838: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 839: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 840: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 841: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 842: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 843: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 844: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 845: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 846: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 847: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 848: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 849: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 850: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 851: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 852: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 853: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 854: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 855: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 856: Signal */
  &drum_bench_B.FixPtSum1_e,           /* 857: Signal */
  &drum_bench_B.FixPtSwitch_b,         /* 858: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 859: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 860: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 861: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 862: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 863: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 864: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 865: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 866: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 867: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 868: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 869: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 870: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 871: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 872: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 873: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 874: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 875: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 876: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 877: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 878: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 879: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 880: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 881: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 882: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 883: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 884: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 885: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 886: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 887: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 888: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 889: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 890: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 891: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 892: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 893: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 894: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 895: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 896: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 897: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 898: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 899: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 900: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 901: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 902: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 903: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 904: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 905: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 906: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 907: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 908: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 909: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 910: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 911: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 912: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 913: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 914: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 915: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 916: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 917: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 918: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 919: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 920: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 921: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 922: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 923: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 924: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 925: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 926: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 927: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 928: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 929: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 930: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 931: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 932: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 933: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 934: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 935: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 936: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 937: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 938: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 939: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 940: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 941: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 942: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 943: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 944: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 945: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 946: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 947: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 948: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 949: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 950: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 951: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 952: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 953: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 954: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 955: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 956: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 957: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 958: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 959: Signal */
  &drum_bench_B.VectorConcatenate_d[0],/* 960: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 961: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 962: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 963: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 964: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 965: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 966: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 967: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 968: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 969: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 970: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 971: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 972: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 973: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 974: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 975: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 976: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 977: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 978: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 979: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 980: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 981: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 982: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 983: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 984: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 985: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 986: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 987: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 988: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 989: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 990: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 991: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 992: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 993: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 994: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 995: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 996: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 997: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 998: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 999: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1000: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1001: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1002: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1003: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1004: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1005: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1006: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1007: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1008: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1009: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1010: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1011: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1012: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1013: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1014: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1015: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1016: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1017: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1018: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1019: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1020: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1021: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1022: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1023: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1024: Signal */
  &drum_bench_B.VectorConcatenate_e[0],/* 1025: Signal */
  &drum_bench_P.config,                /* 1026: Model Parameter */
};

/* Declare Data Run-Time Dimension Buffer Addresses statically */
static int32_T* rtVarDimsAddrMap[] = {
  (NULL)
};

#endif

/* Data Type Map - use dataTypeMapIndex to access this structure */
static TARGET_CONST rtwCAPI_DataTypeMap rtDataTypeMap[] = {
  /* cName, mwName, numElements, elemMapIndex, dataSize, slDataId, *
   * isComplex, isPointer */
  { "double", "real_T", 0, 0, sizeof(real_T), SS_DOUBLE, 0, 0 },

  { "struct", "MarlinFullCommand", 13, 1, sizeof(MarlinFullCommand), SS_STRUCT,
    0, 0 },

  { "unsigned int", "uint32_T", 0, 0, sizeof(uint32_T), SS_UINT32, 0, 0 },

  { "unsigned char", "uint8_T", 0, 0, sizeof(uint8_T), SS_UINT8, 0, 0 },

  { "unsigned char", "boolean_T", 0, 0, sizeof(boolean_T), SS_BOOLEAN, 0, 0 },

  { "struct", "MarlinBasicStatus", 21, 14, sizeof(MarlinBasicStatus), SS_STRUCT,
    0, 0 },

  { "struct", "MarlinFullStatus", 3, 35, sizeof(MarlinFullStatus), SS_STRUCT, 0,
    0 },

  { "float", "real32_T", 0, 0, sizeof(real32_T), SS_SINGLE, 0, 0 },

  { "int", "int32_T", 0, 0, sizeof(int32_T), SS_INT32, 0, 0 },

  { "struct", "MarlinFullInternalRPC", 3, 38, sizeof(MarlinFullInternalRPC),
    SS_STRUCT, 0, 0 },

  { "struct", "creal_T", 0, 0, sizeof(creal_T), SS_DOUBLE, 1, 0 },

  { "unsigned short", "uint16_T", 0, 0, sizeof(uint16_T), SS_UINT16, 0, 0 },

  { "struct", "struct_RIpY2xbTXEXKeuue36idi", 1, 41, sizeof
    (struct_RIpY2xbTXEXKeuue36idi), SS_STRUCT, 0, 0 },

  { "struct", "struct_Nj98fhwEUm6O969J2tt7yD", 8, 42, sizeof
    (struct_Nj98fhwEUm6O969J2tt7yD), SS_STRUCT, 0, 0 },

  { "struct", "struct_9qZLCMMOhVY4AxrMQejy0", 5, 50, sizeof
    (struct_9qZLCMMOhVY4AxrMQejy0), SS_STRUCT, 0, 0 },

  { "struct", "struct_QBpPZnk5niXRFhzN7Lj31C", 1, 55, sizeof
    (struct_QBpPZnk5niXRFhzN7Lj31C), SS_STRUCT, 0, 0 },

  { "struct", "struct_AAoDtaShZdSuxE8MfKAZuD", 1, 56, sizeof
    (struct_AAoDtaShZdSuxE8MfKAZuD), SS_STRUCT, 0, 0 },

  { "struct", "struct_bL7nOhahIPgI0X3SLeRjmE", 4, 57, sizeof
    (struct_bL7nOhahIPgI0X3SLeRjmE), SS_STRUCT, 0, 0 },

  { "struct", "struct_fqoitRciEBSWbMiMs1xp6G", 3, 61, sizeof
    (struct_fqoitRciEBSWbMiMs1xp6G), SS_STRUCT, 0, 0 },

  { "struct", "struct_LTw73MkwrOKgjTdKqzeYAH", 1, 64, sizeof
    (struct_LTw73MkwrOKgjTdKqzeYAH), SS_STRUCT, 0, 0 },

  { "struct", "struct_jqZJIiVwXC2PGh1UGLYaAD", 2, 65, sizeof
    (struct_jqZJIiVwXC2PGh1UGLYaAD), SS_STRUCT, 0, 0 },

  { "struct", "struct_INE22yG6rcc1ImNexAY7TC", 1, 67, sizeof
    (struct_INE22yG6rcc1ImNexAY7TC), SS_STRUCT, 0, 0 },

  { "struct", "struct_gK2Jy8LweAj0bYVyYXESZG", 4, 68, sizeof
    (struct_gK2Jy8LweAj0bYVyYXESZG), SS_STRUCT, 0, 0 },

  { "struct", "struct_pXLF1SG7JVIGRnIbnNXxpC", 10, 72, sizeof
    (struct_pXLF1SG7JVIGRnIbnNXxpC), SS_STRUCT, 0, 0 },

  { "struct", "struct_fxT5Nq3tGzQ2GEaQv1zUtC", 6, 82, sizeof
    (struct_fxT5Nq3tGzQ2GEaQv1zUtC), SS_STRUCT, 0, 0 },

  { "struct", "struct_8LDeTuq9MAK8ojrlDlByvH", 2, 88, sizeof
    (struct_8LDeTuq9MAK8ojrlDlByvH), SS_STRUCT, 0, 0 },

  { "struct", "struct_KXOvmy8ymuqsyffV7WWpcG", 1, 90, sizeof
    (struct_KXOvmy8ymuqsyffV7WWpcG), SS_STRUCT, 0, 0 },

  { "struct", "struct_KoXNgBBld1DARpq3izFtbF", 2, 91, sizeof
    (struct_KoXNgBBld1DARpq3izFtbF), SS_STRUCT, 0, 0 },

  { "struct", "struct_Se3YeAXB2onI5ODlwrYQo", 1, 93, sizeof
    (struct_Se3YeAXB2onI5ODlwrYQo), SS_STRUCT, 0, 0 },

  { "struct", "struct_BCyM1TQF0epB1PvNrxpCnF", 17, 94, sizeof
    (struct_BCyM1TQF0epB1PvNrxpCnF), SS_STRUCT, 0, 0 },

  { "struct", "struct_0tMNkeucayrJub3Th2yZ6E", 2, 111, sizeof
    (struct_0tMNkeucayrJub3Th2yZ6E), SS_STRUCT, 0, 0 },

  { "struct", "struct_jkDUSheYYfvtbbR4tr6mvD", 5, 113, sizeof
    (struct_jkDUSheYYfvtbbR4tr6mvD), SS_STRUCT, 0, 0 },

  { "struct", "struct_6bi3MChDTb2LN2fRyJtpm", 20, 118, sizeof
    (struct_6bi3MChDTb2LN2fRyJtpm), SS_STRUCT, 0, 0 },

  { "struct", "struct_om0sqrv6CVBccOjddaiW7E", 1, 138, sizeof
    (struct_om0sqrv6CVBccOjddaiW7E), SS_STRUCT, 0, 0 },

  { "struct", "struct_CXnTNOFtLHj3Uh94en2j8C", 3, 139, sizeof
    (struct_CXnTNOFtLHj3Uh94en2j8C), SS_STRUCT, 0, 0 },

  { "struct", "struct_Ff8qvljR6PDUJi6EFt21oB", 3, 142, sizeof
    (struct_Ff8qvljR6PDUJi6EFt21oB), SS_STRUCT, 0, 0 },

  { "struct", "struct_qmv5O2WOgyNocQPcrfDxtE", 4, 145, sizeof
    (struct_qmv5O2WOgyNocQPcrfDxtE), SS_STRUCT, 0, 0 },

  { "struct", "struct_FlGEfMk29qJHlFXdbDtRXC", 3, 149, sizeof
    (struct_FlGEfMk29qJHlFXdbDtRXC), SS_STRUCT, 0, 0 },

  { "struct", "struct_Kzi606PJBfEKmP4ngorXDD", 4, 152, sizeof
    (struct_Kzi606PJBfEKmP4ngorXDD), SS_STRUCT, 0, 0 }
};

#ifdef HOST_CAPI_BUILD
#undef sizeof
#endif

/* Structure Element Map - use elemMapIndex to access this structure */
static TARGET_CONST rtwCAPI_ElementMap rtElementMap[] = {
  /* elementName, elementOffset, dataTypeIndex, dimIndex, fxpIndex */
  { (NULL), 0, 0, 0, 0 },

  { "mode", rt_offsetof(MarlinFullCommand, mode), 0, 0, 0 },

  { "quadrature_1_angle", rt_offsetof(MarlinFullCommand, quadrature_1_angle), 0,
    0, 0 },

  { "quadrature_1_angle_dot", rt_offsetof(MarlinFullCommand,
    quadrature_1_angle_dot), 0, 0, 0 },

  { "quadrature_2_angle", rt_offsetof(MarlinFullCommand, quadrature_2_angle), 0,
    0, 0 },

  { "quadrature_2_angle_dot", rt_offsetof(MarlinFullCommand,
    quadrature_2_angle_dot), 0, 0, 0 },

  { "analog_diff", rt_offsetof(MarlinFullCommand, analog_diff), 0, 0, 0 },

  { "analog_diff_dot", rt_offsetof(MarlinFullCommand, analog_diff_dot), 0, 0, 0
  },

  { "motor_torque", rt_offsetof(MarlinFullCommand, motor_torque), 0, 0, 0 },

  { "motor_iq", rt_offsetof(MarlinFullCommand, motor_iq), 0, 0, 0 },

  { "motor_id", rt_offsetof(MarlinFullCommand, motor_id), 0, 0, 0 },

  { "motor_vq", rt_offsetof(MarlinFullCommand, motor_vq), 0, 0, 0 },

  { "motor_vd", rt_offsetof(MarlinFullCommand, motor_vd), 0, 0, 0 },

  { "rpc", rt_offsetof(MarlinFullCommand, rpc), 0, 0, 0 },

  { "motor_torque", rt_offsetof(MarlinBasicStatus, motor_torque), 0, 0, 0 },

  { "motor_iq", rt_offsetof(MarlinBasicStatus, motor_iq), 0, 0, 0 },

  { "motor_vq_avg", rt_offsetof(MarlinBasicStatus, motor_vq_avg), 0, 0, 0 },

  { "analog_1", rt_offsetof(MarlinBasicStatus, analog_1), 0, 0, 0 },

  { "analog_1_dot", rt_offsetof(MarlinBasicStatus, analog_1_dot), 0, 0, 0 },

  { "analog_2", rt_offsetof(MarlinBasicStatus, analog_2), 0, 0, 0 },

  { "analog_2_dot", rt_offsetof(MarlinBasicStatus, analog_2_dot), 0, 0, 0 },

  { "analog_diff", rt_offsetof(MarlinBasicStatus, analog_diff), 0, 0, 0 },

  { "analog_diff_dot", rt_offsetof(MarlinBasicStatus, analog_diff_dot), 0, 0, 0
  },

  { "quadrature_1", rt_offsetof(MarlinBasicStatus, quadrature_1), 0, 0, 0 },

  { "quadrature_1_dot", rt_offsetof(MarlinBasicStatus, quadrature_1_dot), 0, 0,
    0 },

  { "quadrature_2", rt_offsetof(MarlinBasicStatus, quadrature_2), 0, 0, 0 },

  { "quadrature_2_dot", rt_offsetof(MarlinBasicStatus, quadrature_2_dot), 0, 0,
    0 },

  { "ssi", rt_offsetof(MarlinBasicStatus, ssi), 0, 0, 0 },

  { "ssi_dot", rt_offsetof(MarlinBasicStatus, ssi_dot), 0, 0, 0 },

  { "accelerometer_x", rt_offsetof(MarlinBasicStatus, accelerometer_x), 0, 0, 0
  },

  { "accelerometer_y", rt_offsetof(MarlinBasicStatus, accelerometer_y), 0, 0, 0
  },

  { "accelerometer_z", rt_offsetof(MarlinBasicStatus, accelerometer_z), 0, 0, 0
  },

  { "faults", rt_offsetof(MarlinBasicStatus, faults), 2, 0, 0 },

  { "bus_v", rt_offsetof(MarlinBasicStatus, bus_v), 0, 0, 0 },

  { "rpa_packet", rt_offsetof(MarlinBasicStatus, rpa_packet), 3, 1, 0 },

  { "marlin_basic_status", rt_offsetof(MarlinFullStatus, marlin_basic_status), 5,
    0, 0 },

  { "temperature_motor_winding", rt_offsetof(MarlinFullStatus,
    temperature_motor_winding), 0, 0, 0 },

  { "temperature_motor_housing", rt_offsetof(MarlinFullStatus,
    temperature_motor_housing), 0, 0, 0 },

  { "type", rt_offsetof(MarlinFullInternalRPC, type), 8, 0, 0 },

  { "id", rt_offsetof(MarlinFullInternalRPC, id), 7, 0, 0 },

  { "data", rt_offsetof(MarlinFullInternalRPC, data), 3, 6, 0 },

  { "trigger_level", rt_offsetof(struct_RIpY2xbTXEXKeuue36idi, trigger_level), 0,
    18, 0 },

  { "current_zero_a", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, current_zero_a),
    0, 18, 0 },

  { "current_zero_b", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, current_zero_b),
    0, 18, 0 },

  { "deadband", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, deadband), 0, 18, 0 },

  { "led_brightness", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, led_brightness),
    0, 18, 0 },

  { "nominal_bus_voltage", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD,
    nominal_bus_voltage), 0, 18, 0 },

  { "current_sensor_limit", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD,
    current_sensor_limit), 0, 18, 0 },

  { "logger_10kHz", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, logger_10kHz), 12,
    18, 0 },

  { "logger_100kHz", rt_offsetof(struct_Nj98fhwEUm6O969J2tt7yD, logger_100kHz),
    12, 18, 0 },

  { "current_filter_bandwidth", rt_offsetof(struct_9qZLCMMOhVY4AxrMQejy0,
    current_filter_bandwidth), 0, 18, 0 },

  { "fingerprint_gain", rt_offsetof(struct_9qZLCMMOhVY4AxrMQejy0,
    fingerprint_gain), 0, 18, 0 },

  { "ki", rt_offsetof(struct_9qZLCMMOhVY4AxrMQejy0, ki), 0, 18, 0 },

  { "ki_limit", rt_offsetof(struct_9qZLCMMOhVY4AxrMQejy0, ki_limit), 0, 18, 0 },

  { "kp", rt_offsetof(struct_9qZLCMMOhVY4AxrMQejy0, kp), 0, 18, 0 },

  { "deadband", rt_offsetof(struct_QBpPZnk5niXRFhzN7Lj31C, deadband), 0, 18, 0 },

  { "enabled", rt_offsetof(struct_AAoDtaShZdSuxE8MfKAZuD, enabled), 0, 18, 0 },

  { "kd", rt_offsetof(struct_bL7nOhahIPgI0X3SLeRjmE, kd), 0, 18, 0 },

  { "ki", rt_offsetof(struct_bL7nOhahIPgI0X3SLeRjmE, ki), 0, 18, 0 },

  { "ki_limit", rt_offsetof(struct_bL7nOhahIPgI0X3SLeRjmE, ki_limit), 0, 18, 0 },

  { "kp", rt_offsetof(struct_bL7nOhahIPgI0X3SLeRjmE, kp), 0, 18, 0 },

  { "pid_deadband", rt_offsetof(struct_fqoitRciEBSWbMiMs1xp6G, pid_deadband), 15,
    18, 0 },

  { "pid_interpolate", rt_offsetof(struct_fqoitRciEBSWbMiMs1xp6G,
    pid_interpolate), 16, 18, 0 },

  { "pid", rt_offsetof(struct_fqoitRciEBSWbMiMs1xp6G, pid), 17, 18, 0 },

  { "pid", rt_offsetof(struct_LTw73MkwrOKgjTdKqzeYAH, pid), 17, 18, 0 },

  { "pid", rt_offsetof(struct_jqZJIiVwXC2PGh1UGLYaAD, pid), 17, 18, 0 },

  { "source", rt_offsetof(struct_jqZJIiVwXC2PGh1UGLYaAD, source), 0, 18, 0 },

  { "counts_per_revolution", rt_offsetof(struct_INE22yG6rcc1ImNexAY7TC,
    counts_per_revolution), 0, 18, 0 },

  { "eA1", rt_offsetof(struct_gK2Jy8LweAj0bYVyYXESZG, eA1), 0, 19, 0 },

  { "eA2", rt_offsetof(struct_gK2Jy8LweAj0bYVyYXESZG, eA2), 0, 20, 0 },

  { "temperature_allowable_motor_winding", rt_offsetof
    (struct_gK2Jy8LweAj0bYVyYXESZG, temperature_allowable_motor_winding), 0, 18,
    0 },

  { "temperature_allowable_motor_housing", rt_offsetof
    (struct_gK2Jy8LweAj0bYVyYXESZG, temperature_allowable_motor_housing), 0, 18,
    0 },

  { "current_limit", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, current_limit),
    0, 18, 0 },

  { "voltage_limit", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, voltage_limit),
    0, 18, 0 },

  { "km", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, km), 0, 18, 0 },

  { "resistance", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, resistance), 0, 18,
    0 },

  { "num_poles", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, num_poles), 0, 18, 0
  },

  { "electrical_offset", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC,
    electrical_offset), 0, 18, 0 },

  { "v_per_a", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, v_per_a), 0, 18, 0 },

  { "encoder", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, encoder), 21, 18, 0 },

  { "thermal_model", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC, thermal_model),
    22, 18, 0 },

  { "phase_lock_current", rt_offsetof(struct_pXLF1SG7JVIGRnIbnNXxpC,
    phase_lock_current), 0, 18, 0 },

  { "bias", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC, bias), 0, 18, 0 },

  { "gain", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC, gain), 0, 18, 0 },

  { "limit_high", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC, limit_high), 0, 18,
    0 },

  { "limit_low", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC, limit_low), 0, 18, 0
  },

  { "filter_bandwidth", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC,
    filter_bandwidth), 0, 18, 0 },

  { "dot_filter_bandwidth", rt_offsetof(struct_fxT5Nq3tGzQ2GEaQv1zUtC,
    dot_filter_bandwidth), 0, 18, 0 },

  { "sensor", rt_offsetof(struct_8LDeTuq9MAK8ojrlDlByvH, sensor), 24, 18, 0 },

  { "enable_index", rt_offsetof(struct_8LDeTuq9MAK8ojrlDlByvH, enable_index), 0,
    18, 0 },

  { "sensor", rt_offsetof(struct_KXOvmy8ymuqsyffV7WWpcG, sensor), 24, 18, 0 },

  { "eA1", rt_offsetof(struct_KoXNgBBld1DARpq3izFtbF, eA1), 0, 18, 0 },

  { "eA2", rt_offsetof(struct_KoXNgBBld1DARpq3izFtbF, eA2), 0, 18, 0 },

  { "thermal_model", rt_offsetof(struct_Se3YeAXB2onI5ODlwrYQo, thermal_model),
    27, 18, 0 },

  { "slave_alias", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, slave_alias), 0,
    18, 0 },

  { "slave_position", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, slave_position),
    0, 18, 0 },

  { "debug_address_index", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF,
    debug_address_index), 0, 18, 0 },

  { "amp", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, amp), 13, 18, 0 },

  { "control_current", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF,
    control_current), 14, 18, 0 },

  { "control_motor_angle", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF,
    control_motor_angle), 18, 18, 0 },

  { "control_quadrature_2_angle", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF,
    control_quadrature_2_angle), 19, 18, 0 },

  { "control_analog", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, control_analog),
    20, 18, 0 },

  { "motor", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, motor), 23, 18, 0 },

  { "quadrature_1", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, quadrature_1), 25,
    18, 0 },

  { "quadrature_2", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, quadrature_2), 25,
    18, 0 },

  { "ssi", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, ssi), 26, 18, 0 },

  { "analog_1", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, analog_1), 26, 18, 0
  },

  { "analog_2", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, analog_2), 26, 18, 0
  },

  { "analog_diff", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, analog_diff), 26,
    18, 0 },

  { "bridge", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, bridge), 28, 18, 0 },

  { "fingerprint", rt_offsetof(struct_BCyM1TQF0epB1PvNrxpCnF, fingerprint), 0,
    21, 0 },

  { "M0", rt_offsetof(struct_0tMNkeucayrJub3Th2yZ6E, M0), 29, 18, 0 },

  { "M1", rt_offsetof(struct_0tMNkeucayrJub3Th2yZ6E, M1), 29, 18, 0 },

  { "amplitude", rt_offsetof(struct_jkDUSheYYfvtbbR4tr6mvD, amplitude), 0, 18, 0
  },

  { "bias", rt_offsetof(struct_jkDUSheYYfvtbbR4tr6mvD, bias), 0, 18, 0 },

  { "frequency", rt_offsetof(struct_jkDUSheYYfvtbbR4tr6mvD, frequency), 0, 18, 0
  },

  { "select_input", rt_offsetof(struct_jkDUSheYYfvtbbR4tr6mvD, select_input), 0,
    18, 0 },

  { "slew_rate", rt_offsetof(struct_jkDUSheYYfvtbbR4tr6mvD, slew_rate), 0, 18, 0
  },

  { "mode", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, mode), 0, 18, 0 },

  { "quadrature_1_angle", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_1_angle), 31, 18, 0 },

  { "quadrature_1_angle_dot", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_1_angle_dot), 0, 18, 0 },

  { "quadrature_1_angle_dotdot", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_1_angle_dotdot), 0, 18, 0 },

  { "quadrature_2_angle", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_2_angle), 31, 18, 0 },

  { "quadrature_2_angle_dot", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_2_angle_dot), 0, 18, 0 },

  { "quadrature_2_angle_dotdot", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_2_angle_dotdot), 0, 18, 0 },

  { "analog", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, analog), 31, 18, 0 },

  { "analog_dot", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, analog_dot), 0, 18,
    0 },

  { "motor_torque", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, motor_torque), 0,
    18, 0 },

  { "motor_iq", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, motor_iq), 31, 18, 0 },

  { "motor_id", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, motor_id), 0, 18, 0 },

  { "motor_vq", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, motor_vq), 31, 18, 0 },

  { "rpc_command", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, rpc_command), 0, 18,
    0 },

  { "motor_vd", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, motor_vd), 0, 18, 0 },

  { "quadrature_1_angle_gain", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_1_angle_gain), 0, 18, 0 },

  { "quadrature_1_angle_dot_gain", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    quadrature_1_angle_dot_gain), 0, 18, 0 },

  { "analog_gain", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, analog_gain), 0, 18,
    0 },

  { "analog_dot_gain", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm, analog_dot_gain),
    0, 18, 0 },

  { "trigger_zero_bias", rt_offsetof(struct_6bi3MChDTb2LN2fRyJtpm,
    trigger_zero_bias), 0, 18, 0 },

  { "trigger_yaml_write", rt_offsetof(struct_om0sqrv6CVBccOjddaiW7E,
    trigger_yaml_write), 0, 18, 0 },

  { "input0", rt_offsetof(struct_CXnTNOFtLHj3Uh94en2j8C, input0), 32, 18, 0 },

  { "input1", rt_offsetof(struct_CXnTNOFtLHj3Uh94en2j8C, input1), 32, 18, 0 },

  { "input_extra", rt_offsetof(struct_CXnTNOFtLHj3Uh94en2j8C, input_extra), 33,
    18, 0 },

  { "kp", rt_offsetof(struct_Ff8qvljR6PDUJi6EFt21oB, kp), 0, 18, 0 },

  { "kd", rt_offsetof(struct_Ff8qvljR6PDUJi6EFt21oB, kd), 0, 18, 0 },

  { "imp_cutoff", rt_offsetof(struct_Ff8qvljR6PDUJi6EFt21oB, imp_cutoff), 0, 18,
    0 },

  { "bias", rt_offsetof(struct_qmv5O2WOgyNocQPcrfDxtE, bias), 0, 18, 0 },

  { "ka", rt_offsetof(struct_qmv5O2WOgyNocQPcrfDxtE, ka), 0, 18, 0 },

  { "kd", rt_offsetof(struct_qmv5O2WOgyNocQPcrfDxtE, kd), 0, 18, 0 },

  { "var_imp_cutoff", rt_offsetof(struct_qmv5O2WOgyNocQPcrfDxtE, var_imp_cutoff),
    0, 18, 0 },

  { "mode_select", rt_offsetof(struct_FlGEfMk29qJHlFXdbDtRXC, mode_select), 0,
    18, 0 },

  { "impedence_control", rt_offsetof(struct_FlGEfMk29qJHlFXdbDtRXC,
    impedence_control), 35, 18, 0 },

  { "var_impedence_control", rt_offsetof(struct_FlGEfMk29qJHlFXdbDtRXC,
    var_impedence_control), 36, 18, 0 },

  { "marlins", rt_offsetof(struct_Kzi606PJBfEKmP4ngorXDD, marlins), 30, 18, 0 },

  { "input", rt_offsetof(struct_Kzi606PJBfEKmP4ngorXDD, input), 34, 18, 0 },

  { "control0", rt_offsetof(struct_Kzi606PJBfEKmP4ngorXDD, control0), 37, 18, 0
  },

  { "control1", rt_offsetof(struct_Kzi606PJBfEKmP4ngorXDD, control1), 37, 18, 0
  }
};

/* Dimension Map - use dimensionMapIndex to access elements of ths structure*/
static const rtwCAPI_DimensionMap rtDimensionMap[] = {
  /* dataOrientation, dimArrayIndex, numDims, vardimsIndex */
  { rtwCAPI_SCALAR, 0, 2, 0 },

  { rtwCAPI_VECTOR, 2, 2, 0 },

  { rtwCAPI_MATRIX_COL_MAJOR, 4, 2, 0 },

  { rtwCAPI_MATRIX_COL_MAJOR, 6, 2, 0 },

  { rtwCAPI_VECTOR, 8, 2, 0 },

  { rtwCAPI_VECTOR, 10, 2, 0 },

  { rtwCAPI_VECTOR, 12, 2, 0 },

  { rtwCAPI_VECTOR, 14, 2, 0 },

  { rtwCAPI_VECTOR, 16, 2, 0 },

  { rtwCAPI_VECTOR, 18, 2, 0 },

  { rtwCAPI_VECTOR, 20, 2, 0 },

  { rtwCAPI_VECTOR, 22, 2, 0 },

  { rtwCAPI_VECTOR, 24, 2, 0 },

  { rtwCAPI_VECTOR, 26, 2, 0 },

  { rtwCAPI_VECTOR, 28, 2, 0 },

  { rtwCAPI_VECTOR, 30, 2, 0 },

  { rtwCAPI_VECTOR, 32, 2, 0 },

  { rtwCAPI_VECTOR, 34, 2, 0 },

  { rtwCAPI_MATRIX_COL_MAJOR, 0, 2, 0 },

  { rtwCAPI_MATRIX_COL_MAJOR, 36, 2, 0 },

  { rtwCAPI_MATRIX_COL_MAJOR, 10, 2, 0 },

  { rtwCAPI_MATRIX_COL_MAJOR, 38, 2, 0 }
};

/* Dimension Array- use dimArrayIndex to access elements of this array */
static const uint_T rtDimensionArray[] = {
  1,                                   /* 0 */
  1,                                   /* 1 */
  28,                                  /* 2 */
  1,                                   /* 3 */
  128,                                 /* 4 */
  1,                                   /* 5 */
  7,                                   /* 6 */
  1,                                   /* 7 */
  56,                                  /* 8 */
  1,                                   /* 9 */
  2,                                   /* 10 */
  1,                                   /* 11 */
  20,                                  /* 12 */
  1,                                   /* 13 */
  15,                                  /* 14 */
  1,                                   /* 15 */
  14,                                  /* 16 */
  1,                                   /* 17 */
  10,                                  /* 18 */
  1,                                   /* 19 */
  5,                                   /* 20 */
  1,                                   /* 21 */
  76,                                  /* 22 */
  1,                                   /* 23 */
  1,                                   /* 24 */
  8,                                   /* 25 */
  1,                                   /* 26 */
  6,                                   /* 27 */
  1,                                   /* 28 */
  2,                                   /* 29 */
  1,                                   /* 30 */
  5,                                   /* 31 */
  1,                                   /* 32 */
  4,                                   /* 33 */
  1,                                   /* 34 */
  7,                                   /* 35 */
  2,                                   /* 36 */
  2,                                   /* 37 */
  2000,                                /* 38 */
  1                                    /* 39 */
};

/* C-API stores floating point values in an array. The elements of this  *
 * are unique. This ensures that values which are shared across the model*
 * are stored in the most efficient way. These values are referenced by  *
 *           - rtwCAPI_FixPtMap.fracSlopePtr,                            *
 *           - rtwCAPI_FixPtMap.biasPtr,                                 *
 *           - rtwCAPI_SampleTimeMap.samplePeriodPtr,                    *
 *           - rtwCAPI_SampleTimeMap.sampleOffsetPtr                     */
static const real_T rtcapiStoredFloats[] = {
  9.9999999999999991E-5, 0.0, 0.0003, 0.001, 0.01, 0.0384, 1.0
};

/* Fixed Point Map */
static const rtwCAPI_FixPtMap rtFixPtMap[] = {
  /* fracSlopePtr, biasPtr, scaleType, wordLength, exponent, isSigned */
  { (NULL), (NULL), rtwCAPI_FIX_RESERVED, 0, 0, 0 },

  { (const void *) &rtcapiStoredFloats[6], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 0, 0 },

  { (const void *) &rtcapiStoredFloats[6], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 1, 0 },

  { (const void *) &rtcapiStoredFloats[6], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 10, 0 },

  { (const void *) &rtcapiStoredFloats[6], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 2, 0 },

  { (const void *) &rtcapiStoredFloats[6], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 3, 0 },

  { (const void *) &rtcapiStoredFloats[6], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 4, 0 },

  { (const void *) &rtcapiStoredFloats[6], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 5, 0 },

  { (const void *) &rtcapiStoredFloats[6], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 6, 0 },

  { (const void *) &rtcapiStoredFloats[6], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 7, 0 },

  { (const void *) &rtcapiStoredFloats[6], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 8, 0 },

  { (const void *) &rtcapiStoredFloats[6], (const void *) &rtcapiStoredFloats[1],
    rtwCAPI_FIX_UNIFORM_SCALING, 1, 9, 0 }
};

/* Sample Time Map - use sTimeIndex to access elements of ths structure */
static const rtwCAPI_SampleTimeMap rtSampleTimeMap[] = {
  /* samplePeriodPtr, sampleOffsetPtr, tid, samplingMode */
  { (const void *) &rtcapiStoredFloats[0], (const void *) &rtcapiStoredFloats[1],
    1, 0 },

  { (NULL), (NULL), -2, 0 },

  { (const void *) &rtcapiStoredFloats[1], (const void *) &rtcapiStoredFloats[1],
    0, 0 },

  { (const void *) &rtcapiStoredFloats[2], (const void *) &rtcapiStoredFloats[1],
    2, 0 },

  { (const void *) &rtcapiStoredFloats[3], (const void *) &rtcapiStoredFloats[1],
    3, 0 },

  { (const void *) &rtcapiStoredFloats[4], (const void *) &rtcapiStoredFloats[1],
    4, 0 },

  { (const void *) &rtcapiStoredFloats[5], (const void *) &rtcapiStoredFloats[1],
    5, 0 },

  { (const void *) &rtcapiStoredFloats[5], (const void *) &rtcapiStoredFloats[1],
    5, 1 }
};

static rtwCAPI_ModelMappingStaticInfo mmiStatic = {
  /* Signals:{signals, numSignals},
   * Params: {blockParameters, numBlockParameters,
   *          modelParameters, numModelParameters},
   * States: {states, numStates},
   * Root Inputs: {rootInputs, numRootInputs}
   * Root Outputs: {rootOutputs, numRootOutputs}
   * Maps:   {dataTypeMap, dimensionMap, fixPtMap,
   *          elementMap, sampleTimeMap, dimensionArray},
   * TargetType: targetType
   */
  { rtBlockSignals, 1026,
    (NULL), 0,
    (NULL), 0 },

  { rtBlockParameters, 0,
    rtModelParameters, 1 },

  { (NULL), 0 },

  { rtDataTypeMap, rtDimensionMap, rtFixPtMap,
    rtElementMap, rtSampleTimeMap, rtDimensionArray },
  "float", (NULL), 0,
};

/* Function to get C API Model Mapping Static Info */
const rtwCAPI_ModelMappingStaticInfo*
  drum_bench_GetCAPIStaticMap()
{
  return &mmiStatic;
}

/* Cache pointers into DataMapInfo substructure of RTModel */
#ifndef HOST_CAPI_BUILD

void drum_bench_InitializeDataMapInfo(RT_MODEL_drum_bench_T *const drum_bench_M)
{
  /* Set C-API version */
  rtwCAPI_SetVersion(drum_bench_M->DataMapInfo.mmi, 1);

  /* Cache static C-API data into the Real-time Model Data structure */
  rtwCAPI_SetStaticMap(drum_bench_M->DataMapInfo.mmi, &mmiStatic);

  /* Cache static C-API logging data into the Real-time Model Data structure */
  rtwCAPI_SetLoggingStaticMap(drum_bench_M->DataMapInfo.mmi, (NULL));

  /* Cache C-API Data Addresses into the Real-Time Model Data structure */
  rtwCAPI_SetDataAddressMap(drum_bench_M->DataMapInfo.mmi, rtDataAddrMap);

  /* Cache C-API Data Run-Time Dimension Buffer Addresses into the Real-Time Model Data structure */
  rtwCAPI_SetVarDimsAddressMap(drum_bench_M->DataMapInfo.mmi, rtVarDimsAddrMap);

  /* Cache the instance C-API logging pointer */
  rtwCAPI_SetInstanceLoggingInfo(drum_bench_M->DataMapInfo.mmi, (NULL));

  /* Set reference to submodels */
  rtwCAPI_SetChildMMIArray(drum_bench_M->DataMapInfo.mmi, (NULL));
  rtwCAPI_SetChildMMIArrayLen(drum_bench_M->DataMapInfo.mmi, 0);
}

#else                                  /* HOST_CAPI_BUILD */
#ifdef __cplusplus

extern "C" {

#endif

  void drum_bench_host_InitializeDataMapInfo(drum_bench_host_DataMapInfo_T
    *dataMap, const char *path)
  {
    /* Set C-API version */
    rtwCAPI_SetVersion(dataMap->mmi, 1);

    /* Cache static C-API data into the Real-time Model Data structure */
    rtwCAPI_SetStaticMap(dataMap->mmi, &mmiStatic);

    /* host data address map is NULL */
    rtwCAPI_SetDataAddressMap(dataMap->mmi, NULL);

    /* host vardims address map is NULL */
    rtwCAPI_SetVarDimsAddressMap(dataMap->mmi, NULL);

    /* Set Instance specific path */
    rtwCAPI_SetPath(dataMap->mmi, path);
    rtwCAPI_SetFullPath(dataMap->mmi, NULL);

    /* Set reference to submodels */
    rtwCAPI_SetChildMMIArray(dataMap->mmi, (NULL));
    rtwCAPI_SetChildMMIArrayLen(dataMap->mmi, 0);
  }

#ifdef __cplusplus

}
#endif
#endif                                 /* HOST_CAPI_BUILD */

/* EOF: drum_bench_capi.c */
