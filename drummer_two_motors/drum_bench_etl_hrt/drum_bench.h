/*
 * drum_bench.h
 *
 * Code generation for model "drum_bench".
 *
 * Model version              : 1.526
 * Simulink Coder version : 8.5 (R2013b) 08-Aug-2013
 * C source code generated on : Mon Jul 21 13:57:13 2014
 *
 * Target selection: etherlab.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_drum_bench_h_
#define RTW_HEADER_drum_bench_h_
#include "rtw_modelmap.h"
#ifndef drum_bench_COMMON_INCLUDES_
# define drum_bench_COMMON_INCLUDES_
#include <math.h>
#include <stddef.h>
#include <string.h>
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "ecrt_support.h"
#include "stdio.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"
#include "rtGetNaN.h"
#include "rt_defines.h"
#include "rt_zcfcn.h"
#endif                                 /* drum_bench_COMMON_INCLUDES_ */

#include "drum_bench_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetBlkStateChangeFlag
# define rtmGetBlkStateChangeFlag(rtm) ((rtm)->ModelData.blkStateChange)
#endif

#ifndef rtmSetBlkStateChangeFlag
# define rtmSetBlkStateChangeFlag(rtm, val) ((rtm)->ModelData.blkStateChange = (val))
#endif

#ifndef rtmGetBlockIO
# define rtmGetBlockIO(rtm)            ((rtm)->ModelData.blockIO)
#endif

#ifndef rtmSetBlockIO
# define rtmSetBlockIO(rtm, val)       ((rtm)->ModelData.blockIO = (val))
#endif

#ifndef rtmGetChecksums
# define rtmGetChecksums(rtm)          ((rtm)->Sizes.checksums)
#endif

#ifndef rtmSetChecksums
# define rtmSetChecksums(rtm, val)     ((rtm)->Sizes.checksums = (val))
#endif

#ifndef rtmGetConstBlockIO
# define rtmGetConstBlockIO(rtm)       ((rtm)->ModelData.constBlockIO)
#endif

#ifndef rtmSetConstBlockIO
# define rtmSetConstBlockIO(rtm, val)  ((rtm)->ModelData.constBlockIO = (val))
#endif

#ifndef rtmGetContStateDisabled
# define rtmGetContStateDisabled(rtm)  ((rtm)->ModelData.contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
# define rtmSetContStateDisabled(rtm, val) ((rtm)->ModelData.contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
# define rtmGetContStates(rtm)         ((rtm)->ModelData.contStates)
#endif

#ifndef rtmSetContStates
# define rtmSetContStates(rtm, val)    ((rtm)->ModelData.contStates = (val))
#endif

#ifndef rtmGetDataMapInfo
# define rtmGetDataMapInfo(rtm)        ((rtm)->DataMapInfo)
#endif

#ifndef rtmSetDataMapInfo
# define rtmSetDataMapInfo(rtm, val)   ((rtm)->DataMapInfo = (val))
#endif

#ifndef rtmGetDefaultParam
# define rtmGetDefaultParam(rtm)       ((rtm)->ModelData.defaultParam)
#endif

#ifndef rtmSetDefaultParam
# define rtmSetDefaultParam(rtm, val)  ((rtm)->ModelData.defaultParam = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
# define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->ModelData.derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
# define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->ModelData.derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetDirectFeedThrough
# define rtmGetDirectFeedThrough(rtm)  ((rtm)->Sizes.sysDirFeedThru)
#endif

#ifndef rtmSetDirectFeedThrough
# define rtmSetDirectFeedThrough(rtm, val) ((rtm)->Sizes.sysDirFeedThru = (val))
#endif

#ifndef rtmGetErrorStatusFlag
# define rtmGetErrorStatusFlag(rtm)    ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatusFlag
# define rtmSetErrorStatusFlag(rtm, val) ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmSetFinalTime
# define rtmSetFinalTime(rtm, val)     ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetFirstInitCondFlag
# define rtmGetFirstInitCondFlag(rtm)  ()
#endif

#ifndef rtmSetFirstInitCondFlag
# define rtmSetFirstInitCondFlag(rtm, val) ()
#endif

#ifndef rtmGetIntgData
# define rtmGetIntgData(rtm)           ()
#endif

#ifndef rtmSetIntgData
# define rtmSetIntgData(rtm, val)      ()
#endif

#ifndef rtmGetMdlRefGlobalTID
# define rtmGetMdlRefGlobalTID(rtm)    ()
#endif

#ifndef rtmSetMdlRefGlobalTID
# define rtmSetMdlRefGlobalTID(rtm, val) ()
#endif

#ifndef rtmGetMdlRefTriggerTID
# define rtmGetMdlRefTriggerTID(rtm)   ()
#endif

#ifndef rtmSetMdlRefTriggerTID
# define rtmSetMdlRefTriggerTID(rtm, val) ()
#endif

#ifndef rtmGetModelMappingInfo
# define rtmGetModelMappingInfo(rtm)   ((rtm)->SpecialInfo.mappingInfo)
#endif

#ifndef rtmSetModelMappingInfo
# define rtmSetModelMappingInfo(rtm, val) ((rtm)->SpecialInfo.mappingInfo = (val))
#endif

#ifndef rtmGetModelName
# define rtmGetModelName(rtm)          ((rtm)->modelName)
#endif

#ifndef rtmSetModelName
# define rtmSetModelName(rtm, val)     ((rtm)->modelName = (val))
#endif

#ifndef rtmGetNonInlinedSFcns
# define rtmGetNonInlinedSFcns(rtm)    ((rtm)->NonInlinedSFcns)
#endif

#ifndef rtmSetNonInlinedSFcns
# define rtmSetNonInlinedSFcns(rtm, val) ((rtm)->NonInlinedSFcns = (val))
#endif

#ifndef rtmGetNumBlockIO
# define rtmGetNumBlockIO(rtm)         ((rtm)->Sizes.numBlockIO)
#endif

#ifndef rtmSetNumBlockIO
# define rtmSetNumBlockIO(rtm, val)    ((rtm)->Sizes.numBlockIO = (val))
#endif

#ifndef rtmGetNumBlockParams
# define rtmGetNumBlockParams(rtm)     ((rtm)->Sizes.numBlockPrms)
#endif

#ifndef rtmSetNumBlockParams
# define rtmSetNumBlockParams(rtm, val) ((rtm)->Sizes.numBlockPrms = (val))
#endif

#ifndef rtmGetNumBlocks
# define rtmGetNumBlocks(rtm)          ((rtm)->Sizes.numBlocks)
#endif

#ifndef rtmSetNumBlocks
# define rtmSetNumBlocks(rtm, val)     ((rtm)->Sizes.numBlocks = (val))
#endif

#ifndef rtmGetNumContStates
# define rtmGetNumContStates(rtm)      ((rtm)->Sizes.numContStates)
#endif

#ifndef rtmSetNumContStates
# define rtmSetNumContStates(rtm, val) ((rtm)->Sizes.numContStates = (val))
#endif

#ifndef rtmGetNumDWork
# define rtmGetNumDWork(rtm)           ((rtm)->Sizes.numDwork)
#endif

#ifndef rtmSetNumDWork
# define rtmSetNumDWork(rtm, val)      ((rtm)->Sizes.numDwork = (val))
#endif

#ifndef rtmGetNumInputPorts
# define rtmGetNumInputPorts(rtm)      ((rtm)->Sizes.numIports)
#endif

#ifndef rtmSetNumInputPorts
# define rtmSetNumInputPorts(rtm, val) ((rtm)->Sizes.numIports = (val))
#endif

#ifndef rtmGetNumNonSampledZCs
# define rtmGetNumNonSampledZCs(rtm)   ((rtm)->Sizes.numNonSampZCs)
#endif

#ifndef rtmSetNumNonSampledZCs
# define rtmSetNumNonSampledZCs(rtm, val) ((rtm)->Sizes.numNonSampZCs = (val))
#endif

#ifndef rtmGetNumOutputPorts
# define rtmGetNumOutputPorts(rtm)     ((rtm)->Sizes.numOports)
#endif

#ifndef rtmSetNumOutputPorts
# define rtmSetNumOutputPorts(rtm, val) ((rtm)->Sizes.numOports = (val))
#endif

#ifndef rtmGetNumSFcnParams
# define rtmGetNumSFcnParams(rtm)      ((rtm)->Sizes.numSFcnPrms)
#endif

#ifndef rtmSetNumSFcnParams
# define rtmSetNumSFcnParams(rtm, val) ((rtm)->Sizes.numSFcnPrms = (val))
#endif

#ifndef rtmGetNumSFunctions
# define rtmGetNumSFunctions(rtm)      ((rtm)->Sizes.numSFcns)
#endif

#ifndef rtmSetNumSFunctions
# define rtmSetNumSFunctions(rtm, val) ((rtm)->Sizes.numSFcns = (val))
#endif

#ifndef rtmGetNumSampleTimes
# define rtmGetNumSampleTimes(rtm)     ((rtm)->Sizes.numSampTimes)
#endif

#ifndef rtmSetNumSampleTimes
# define rtmSetNumSampleTimes(rtm, val) ((rtm)->Sizes.numSampTimes = (val))
#endif

#ifndef rtmGetNumU
# define rtmGetNumU(rtm)               ((rtm)->Sizes.numU)
#endif

#ifndef rtmSetNumU
# define rtmSetNumU(rtm, val)          ((rtm)->Sizes.numU = (val))
#endif

#ifndef rtmGetNumY
# define rtmGetNumY(rtm)               ((rtm)->Sizes.numY)
#endif

#ifndef rtmSetNumY
# define rtmSetNumY(rtm, val)          ((rtm)->Sizes.numY = (val))
#endif

#ifndef rtmGetOdeF
# define rtmGetOdeF(rtm)               ()
#endif

#ifndef rtmSetOdeF
# define rtmSetOdeF(rtm, val)          ()
#endif

#ifndef rtmGetOdeY
# define rtmGetOdeY(rtm)               ()
#endif

#ifndef rtmSetOdeY
# define rtmSetOdeY(rtm, val)          ()
#endif

#ifndef rtmGetOffsetTimeArray
# define rtmGetOffsetTimeArray(rtm)    ((rtm)->Timing.offsetTimesArray)
#endif

#ifndef rtmSetOffsetTimeArray
# define rtmSetOffsetTimeArray(rtm, val) ((rtm)->Timing.offsetTimesArray = (val))
#endif

#ifndef rtmGetOffsetTimePtr
# define rtmGetOffsetTimePtr(rtm)      ((rtm)->Timing.offsetTimes)
#endif

#ifndef rtmSetOffsetTimePtr
# define rtmSetOffsetTimePtr(rtm, val) ((rtm)->Timing.offsetTimes = (val))
#endif

#ifndef rtmGetOptions
# define rtmGetOptions(rtm)            ((rtm)->Sizes.options)
#endif

#ifndef rtmSetOptions
# define rtmSetOptions(rtm, val)       ((rtm)->Sizes.options = (val))
#endif

#ifndef rtmGetParamIsMalloced
# define rtmGetParamIsMalloced(rtm)    ()
#endif

#ifndef rtmSetParamIsMalloced
# define rtmSetParamIsMalloced(rtm, val) ()
#endif

#ifndef rtmGetPath
# define rtmGetPath(rtm)               ((rtm)->path)
#endif

#ifndef rtmSetPath
# define rtmSetPath(rtm, val)          ((rtm)->path = (val))
#endif

#ifndef rtmGetPerTaskSampleHits
# define rtmGetPerTaskSampleHits(rtm)  ()
#endif

#ifndef rtmSetPerTaskSampleHits
# define rtmSetPerTaskSampleHits(rtm, val) ()
#endif

#ifndef rtmGetPerTaskSampleHitsArray
# define rtmGetPerTaskSampleHitsArray(rtm) ((rtm)->Timing.perTaskSampleHitsArray)
#endif

#ifndef rtmSetPerTaskSampleHitsArray
# define rtmSetPerTaskSampleHitsArray(rtm, val) ((rtm)->Timing.perTaskSampleHitsArray = (val))
#endif

#ifndef rtmGetPerTaskSampleHitsPtr
# define rtmGetPerTaskSampleHitsPtr(rtm) ((rtm)->Timing.perTaskSampleHits)
#endif

#ifndef rtmSetPerTaskSampleHitsPtr
# define rtmSetPerTaskSampleHitsPtr(rtm, val) ((rtm)->Timing.perTaskSampleHits = (val))
#endif

#ifndef rtmGetPrevZCSigState
# define rtmGetPrevZCSigState(rtm)     ((rtm)->ModelData.prevZCSigState)
#endif

#ifndef rtmSetPrevZCSigState
# define rtmSetPrevZCSigState(rtm, val) ((rtm)->ModelData.prevZCSigState = (val))
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmSetRTWExtModeInfo
# define rtmSetRTWExtModeInfo(rtm, val) ((rtm)->extModeInfo = (val))
#endif

#ifndef rtmGetRTWGeneratedSFcn
# define rtmGetRTWGeneratedSFcn(rtm)   ((rtm)->Sizes.rtwGenSfcn)
#endif

#ifndef rtmSetRTWGeneratedSFcn
# define rtmSetRTWGeneratedSFcn(rtm, val) ((rtm)->Sizes.rtwGenSfcn = (val))
#endif

#ifndef rtmGetRTWLogInfo
# define rtmGetRTWLogInfo(rtm)         ()
#endif

#ifndef rtmSetRTWLogInfo
# define rtmSetRTWLogInfo(rtm, val)    ()
#endif

#ifndef rtmGetRTWRTModelMethodsInfo
# define rtmGetRTWRTModelMethodsInfo(rtm) ()
#endif

#ifndef rtmSetRTWRTModelMethodsInfo
# define rtmSetRTWRTModelMethodsInfo(rtm, val) ()
#endif

#ifndef rtmGetRTWSfcnInfo
# define rtmGetRTWSfcnInfo(rtm)        ((rtm)->sfcnInfo)
#endif

#ifndef rtmSetRTWSfcnInfo
# define rtmSetRTWSfcnInfo(rtm, val)   ((rtm)->sfcnInfo = (val))
#endif

#ifndef rtmGetRTWSolverInfo
# define rtmGetRTWSolverInfo(rtm)      ((rtm)->solverInfo)
#endif

#ifndef rtmSetRTWSolverInfo
# define rtmSetRTWSolverInfo(rtm, val) ((rtm)->solverInfo = (val))
#endif

#ifndef rtmGetRTWSolverInfoPtr
# define rtmGetRTWSolverInfoPtr(rtm)   ((rtm)->solverInfoPtr)
#endif

#ifndef rtmSetRTWSolverInfoPtr
# define rtmSetRTWSolverInfoPtr(rtm, val) ((rtm)->solverInfoPtr = (val))
#endif

#ifndef rtmGetReservedForXPC
# define rtmGetReservedForXPC(rtm)     ((rtm)->SpecialInfo.xpcData)
#endif

#ifndef rtmSetReservedForXPC
# define rtmSetReservedForXPC(rtm, val) ((rtm)->SpecialInfo.xpcData = (val))
#endif

#ifndef rtmGetRootDWork
# define rtmGetRootDWork(rtm)          ((rtm)->ModelData.dwork)
#endif

#ifndef rtmSetRootDWork
# define rtmSetRootDWork(rtm, val)     ((rtm)->ModelData.dwork = (val))
#endif

#ifndef rtmGetSFunctions
# define rtmGetSFunctions(rtm)         ((rtm)->childSfunctions)
#endif

#ifndef rtmSetSFunctions
# define rtmSetSFunctions(rtm, val)    ((rtm)->childSfunctions = (val))
#endif

#ifndef rtmGetSampleHitArray
# define rtmGetSampleHitArray(rtm)     ((rtm)->Timing.sampleHitArray)
#endif

#ifndef rtmSetSampleHitArray
# define rtmSetSampleHitArray(rtm, val) ((rtm)->Timing.sampleHitArray = (val))
#endif

#ifndef rtmGetSampleHitPtr
# define rtmGetSampleHitPtr(rtm)       ((rtm)->Timing.sampleHits)
#endif

#ifndef rtmSetSampleHitPtr
# define rtmSetSampleHitPtr(rtm, val)  ((rtm)->Timing.sampleHits = (val))
#endif

#ifndef rtmGetSampleTimeArray
# define rtmGetSampleTimeArray(rtm)    ((rtm)->Timing.sampleTimesArray)
#endif

#ifndef rtmSetSampleTimeArray
# define rtmSetSampleTimeArray(rtm, val) ((rtm)->Timing.sampleTimesArray = (val))
#endif

#ifndef rtmGetSampleTimePtr
# define rtmGetSampleTimePtr(rtm)      ((rtm)->Timing.sampleTimes)
#endif

#ifndef rtmSetSampleTimePtr
# define rtmSetSampleTimePtr(rtm, val) ((rtm)->Timing.sampleTimes = (val))
#endif

#ifndef rtmGetSampleTimeTaskIDArray
# define rtmGetSampleTimeTaskIDArray(rtm) ((rtm)->Timing.sampleTimeTaskIDArray)
#endif

#ifndef rtmSetSampleTimeTaskIDArray
# define rtmSetSampleTimeTaskIDArray(rtm, val) ((rtm)->Timing.sampleTimeTaskIDArray = (val))
#endif

#ifndef rtmGetSampleTimeTaskIDPtr
# define rtmGetSampleTimeTaskIDPtr(rtm) ((rtm)->Timing.sampleTimeTaskIDPtr)
#endif

#ifndef rtmSetSampleTimeTaskIDPtr
# define rtmSetSampleTimeTaskIDPtr(rtm, val) ((rtm)->Timing.sampleTimeTaskIDPtr = (val))
#endif

#ifndef rtmGetSimMode
# define rtmGetSimMode(rtm)            ((rtm)->simMode)
#endif

#ifndef rtmSetSimMode
# define rtmSetSimMode(rtm, val)       ((rtm)->simMode = (val))
#endif

#ifndef rtmGetSimTimeStep
# define rtmGetSimTimeStep(rtm)        ((rtm)->Timing.simTimeStep)
#endif

#ifndef rtmSetSimTimeStep
# define rtmSetSimTimeStep(rtm, val)   ((rtm)->Timing.simTimeStep = (val))
#endif

#ifndef rtmGetStartTime
# define rtmGetStartTime(rtm)          ((rtm)->Timing.tStart)
#endif

#ifndef rtmSetStartTime
# define rtmSetStartTime(rtm, val)     ((rtm)->Timing.tStart = (val))
#endif

#ifndef rtmGetStepSize
# define rtmGetStepSize(rtm)           ((rtm)->Timing.stepSize)
#endif

#ifndef rtmSetStepSize
# define rtmSetStepSize(rtm, val)      ((rtm)->Timing.stepSize = (val))
#endif

#ifndef rtmGetStopRequestedFlag
# define rtmGetStopRequestedFlag(rtm)  ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequestedFlag
# define rtmSetStopRequestedFlag(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetTaskCounters
# define rtmGetTaskCounters(rtm)       ((rtm)->Timing.TaskCounters)
#endif

#ifndef rtmSetTaskCounters
# define rtmSetTaskCounters(rtm, val)  ((rtm)->Timing.TaskCounters = (val))
#endif

#ifndef rtmGetTaskTimeArray
# define rtmGetTaskTimeArray(rtm)      ((rtm)->Timing.tArray)
#endif

#ifndef rtmSetTaskTimeArray
# define rtmSetTaskTimeArray(rtm, val) ((rtm)->Timing.tArray = (val))
#endif

#ifndef rtmGetTimePtr
# define rtmGetTimePtr(rtm)            ((rtm)->Timing.t)
#endif

#ifndef rtmSetTimePtr
# define rtmSetTimePtr(rtm, val)       ((rtm)->Timing.t = (val))
#endif

#ifndef rtmGetTimingData
# define rtmGetTimingData(rtm)         ((rtm)->Timing.timingData)
#endif

#ifndef rtmSetTimingData
# define rtmSetTimingData(rtm, val)    ((rtm)->Timing.timingData = (val))
#endif

#ifndef rtmGetU
# define rtmGetU(rtm)                  ((rtm)->ModelData.inputs)
#endif

#ifndef rtmSetU
# define rtmSetU(rtm, val)             ((rtm)->ModelData.inputs = (val))
#endif

#ifndef rtmGetVarNextHitTimesListPtr
# define rtmGetVarNextHitTimesListPtr(rtm) ((rtm)->Timing.varNextHitTimesList)
#endif

#ifndef rtmSetVarNextHitTimesListPtr
# define rtmSetVarNextHitTimesListPtr(rtm, val) ((rtm)->Timing.varNextHitTimesList = (val))
#endif

#ifndef rtmGetY
# define rtmGetY(rtm)                  ((rtm)->ModelData.outputs)
#endif

#ifndef rtmSetY
# define rtmSetY(rtm, val)             ((rtm)->ModelData.outputs = (val))
#endif

#ifndef rtmGetZCCacheNeedsReset
# define rtmGetZCCacheNeedsReset(rtm)  ((rtm)->ModelData.zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
# define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->ModelData.zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetZCSignalValues
# define rtmGetZCSignalValues(rtm)     ((rtm)->ModelData.zcSignalValues)
#endif

#ifndef rtmSetZCSignalValues
# define rtmSetZCSignalValues(rtm, val) ((rtm)->ModelData.zcSignalValues = (val))
#endif

#ifndef rtmGet_TimeOfLastOutput
# define rtmGet_TimeOfLastOutput(rtm)  ((rtm)->Timing.timeOfLastOutput)
#endif

#ifndef rtmSet_TimeOfLastOutput
# define rtmSet_TimeOfLastOutput(rtm, val) ((rtm)->Timing.timeOfLastOutput = (val))
#endif

#ifndef rtmGetdX
# define rtmGetdX(rtm)                 ((rtm)->ModelData.derivs)
#endif

#ifndef rtmSetdX
# define rtmSetdX(rtm, val)            ((rtm)->ModelData.derivs = (val))
#endif

#ifndef rtmGetChecksumVal
# define rtmGetChecksumVal(rtm, idx)   ((rtm)->Sizes.checksums[idx])
#endif

#ifndef rtmSetChecksumVal
# define rtmSetChecksumVal(rtm, idx, val) ((rtm)->Sizes.checksums[idx] = (val))
#endif

#ifndef rtmGetDWork
# define rtmGetDWork(rtm, idx)         ((rtm)->ModelData.dwork[idx])
#endif

#ifndef rtmSetDWork
# define rtmSetDWork(rtm, idx, val)    ((rtm)->ModelData.dwork[idx] = (val))
#endif

#ifndef rtmGetOffsetTime
# define rtmGetOffsetTime(rtm, idx)    ((rtm)->Timing.offsetTimes[idx])
#endif

#ifndef rtmSetOffsetTime
# define rtmSetOffsetTime(rtm, idx, val) ((rtm)->Timing.offsetTimes[idx] = (val))
#endif

#ifndef rtmGetSFunction
# define rtmGetSFunction(rtm, idx)     ((rtm)->childSfunctions[idx])
#endif

#ifndef rtmSetSFunction
# define rtmSetSFunction(rtm, idx, val) ((rtm)->childSfunctions[idx] = (val))
#endif

#ifndef rtmGetSampleTime
# define rtmGetSampleTime(rtm, idx)    ((rtm)->Timing.sampleTimes[idx])
#endif

#ifndef rtmSetSampleTime
# define rtmSetSampleTime(rtm, idx, val) ((rtm)->Timing.sampleTimes[idx] = (val))
#endif

#ifndef rtmGetSampleTimeTaskID
# define rtmGetSampleTimeTaskID(rtm, idx) ((rtm)->Timing.sampleTimeTaskIDPtr[idx])
#endif

#ifndef rtmSetSampleTimeTaskID
# define rtmSetSampleTimeTaskID(rtm, idx, val) ((rtm)->Timing.sampleTimeTaskIDPtr[idx] = (val))
#endif

#ifndef rtmGetVarNextHitTimeList
# define rtmGetVarNextHitTimeList(rtm, idx) ((rtm)->Timing.varNextHitTimesList[idx])
#endif

#ifndef rtmSetVarNextHitTimeList
# define rtmSetVarNextHitTimeList(rtm, idx, val) ((rtm)->Timing.varNextHitTimesList[idx] = (val))
#endif

#ifndef rtmIsContinuousTask
# define rtmIsContinuousTask(rtm, tid) ((tid) == 0)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmIsMajorTimeStep
# define rtmIsMajorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MAJOR_TIME_STEP)
#endif

#ifndef rtmIsMinorTimeStep
# define rtmIsMinorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MINOR_TIME_STEP)
#endif

#ifndef rtmIsSampleHit
# define rtmIsSampleHit(rtm, sti, tid) ((rtmIsMajorTimeStep((rtm)) && (rtm)->Timing.sampleHits[(rtm)->Timing.sampleTimeTaskIDPtr[sti]]))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmSetT
# define rtmSetT(rtm, val)                                       /* Do Nothing */
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmSetTFinal
# define rtmSetTFinal(rtm, val)        ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               ((rtm)->Timing.t)
#endif

#ifndef rtmSetTPtr
# define rtmSetTPtr(rtm, val)          ((rtm)->Timing.t = (val))
#endif

#ifndef rtmGetTStart
# define rtmGetTStart(rtm)             ((rtm)->Timing.tStart)
#endif

#ifndef rtmSetTStart
# define rtmSetTStart(rtm, val)        ((rtm)->Timing.tStart = (val))
#endif

#ifndef rtmGetTaskTime
# define rtmGetTaskTime(rtm, sti)      (rtmGetTPtr((rtm))[(rtm)->Timing.sampleTimeTaskIDPtr[sti]])
#endif

#ifndef rtmSetTaskTime
# define rtmSetTaskTime(rtm, sti, val) (rtmGetTPtr((rtm))[sti] = (val))
#endif

#ifndef rtmGetTimeOfLastOutput
# define rtmGetTimeOfLastOutput(rtm)   ((rtm)->Timing.timeOfLastOutput)
#endif

#ifdef rtmGetRTWSolverInfo
#undef rtmGetRTWSolverInfo
#endif

#define rtmGetRTWSolverInfo(rtm)       &((rtm)->solverInfo)

/* Definition for use in the target main file */
#define drum_bench_rtModel             RT_MODEL_drum_bench_T

/* Block signals for system '<S15>/mean' */
typedef struct {
  real_T y;                            /* '<S15>/mean' */
} B_mean_drum_bench_T;

/* Block signals for system '<S15>/slopeChange' */
typedef struct {
  real_T y;                            /* '<S15>/slopeChange' */
} B_slopeChange_drum_bench_T;

/* Block states (auto storage) for system '<S15>/slopeChange' */
typedef struct {
  real_T lastSlope;                    /* '<S15>/slopeChange' */
  real_T lastVal;                      /* '<S15>/slopeChange' */
} DW_slopeChange_drum_bench_T;

/* Block signals for system '<S15>/smooth' */
typedef struct {
  real_T y;                            /* '<S15>/smooth' */
} B_smooth_drum_bench_T;

/* Block states (auto storage) for system '<S15>/smooth' */
typedef struct {
  real_T buf[1500];                    /* '<S15>/smooth' */
} DW_smooth_drum_bench_T;

/* Block signals for system '<S15>/smooth1' */
typedef struct {
  real_T y;                            /* '<S15>/smooth1' */
} B_smooth1_drum_bench_T;

/* Block states (auto storage) for system '<S15>/smooth1' */
typedef struct {
  real_T buf[500];                     /* '<S15>/smooth1' */
} DW_smooth1_drum_bench_T;

/* Block signals for system '<S15>/smooth3' */
typedef struct {
  real_T y;                            /* '<S15>/smooth3' */
} B_smooth3_drum_bench_T;

/* Block states (auto storage) for system '<S15>/smooth3' */
typedef struct {
  real_T buf[7];                       /* '<S15>/smooth3' */
} DW_smooth3_drum_bench_T;

/* Block signals for system '<S15>/zeroCrossing' */
typedef struct {
  real_T y;                            /* '<S15>/zeroCrossing' */
} B_zeroCrossing_drum_bench_T;

/* Block states (auto storage) for system '<S15>/zeroCrossing' */
typedef struct {
  real_T lastSign;                     /* '<S15>/zeroCrossing' */
} DW_zeroCrossing_drum_bench_T;

/* Block signals for system '<S2>/Filtered Derivative' */
typedef struct {
  real_T Filter;                       /* '<S43>/Filter' */
  real_T Sum;                          /* '<S43>/Sum' */
  real_T FilterCoefficient;            /* '<S43>/Filter Coefficient' */
} B_FilteredDerivative_drum_ben_T;

/* Block states (auto storage) for system '<S2>/Filtered Derivative' */
typedef struct {
  real_T Filter_DSTATE;                /* '<S43>/Filter' */
} DW_FilteredDerivative_drum_be_T;

/* Block signals for system '<S112>/enabled subsystem' */
typedef struct {
  uint8_T In1[20];                     /* '<S124>/In1' */
} B_enabledsubsystem_drum_bench_T;

/* Block signals for system '<S113>/Enabled Subsystem' */
typedef struct {
  uint8_T In1[20];                     /* '<S126>/In1' */
} B_EnabledSubsystem_drum_bench_T;

/* Block signals for system '<S127>/MATLAB Function1' */
typedef struct {
  real32_T id;                         /* '<S127>/MATLAB Function1' */
  real32_T value;                      /* '<S127>/MATLAB Function1' */
  int32_T type;                        /* '<S127>/MATLAB Function1' */
  int32_T index;                       /* '<S127>/MATLAB Function1' */
} B_MATLABFunction1_drum_bench_T;

/* Block states (auto storage) for system '<S127>/MATLAB Function1' */
typedef struct {
  real_T i;                            /* '<S127>/MATLAB Function1' */
  real32_T id_sent;                    /* '<S127>/MATLAB Function1' */
  uint32_T method;                     /* '<S127>/MATLAB Function1' */
  uint32_T state;                      /* '<S127>/MATLAB Function1' */
  uint32_T state_g[2];                 /* '<S127>/MATLAB Function1' */
  uint32_T state_gf[625];              /* '<S127>/MATLAB Function1' */
  boolean_T state_not_empty;           /* '<S127>/MATLAB Function1' */
} DW_MATLABFunction1_drum_bench_T;

/* Block signals for system '<S94>/select rpc' */
typedef struct {
  real32_T id;                         /* '<S94>/select rpc' */
  int32_T type;                        /* '<S94>/select rpc' */
  uint8_T data[20];                    /* '<S94>/select rpc' */
} B_selectrpc_drum_bench_T;

/* Block signals (auto storage) */
typedef struct {
  MarlinFullStatus BusCreator;         /* '<S8>/Bus Creator' */
  MarlinFullStatus BusCreator_i;       /* '<S10>/Bus Creator' */
  MarlinFullStatus BusCreator_it;      /* '<S9>/Bus Creator' */
  MarlinFullStatus BusCreator_m;       /* '<S11>/Bus Creator' */
  MarlinBasicStatus BusConversion_InsertedFor_BusCr;
  MarlinBasicStatus marlin_basic_status;/* '<S10>/Bus Selector1' */
  MarlinBasicStatus BusConversion_InsertedFor_Bus_m;
  MarlinBasicStatus marlin_basic_status_i;/* '<S6>/Bus Selector' */
  MarlinBasicStatus BusConversion_InsertedFor_Bus_a;
  MarlinBasicStatus marlin_basic_status_m;/* '<S11>/Bus Selector1' */
  MarlinBasicStatus BusConversion_InsertedFor_Bu_mc;
  MarlinBasicStatus marlin_basic_status_l;/* '<S7>/Bus Selector' */
  MarlinFullCommand BusCreator_n;      /* '<S4>/Bus Creator' */
  MarlinFullCommand BusAssignment;     /* '<S2>/Bus Assignment' */
  MarlinFullCommand BusAssignment_n;   /* '<S12>/Bus Assignment' */
  MarlinFullCommand modeswitch;        /* '<Root>/mode switch' */
  MarlinFullCommand BusCreator_f;      /* '<S5>/Bus Creator' */
  MarlinFullCommand BusAssignment_n4;  /* '<S3>/Bus Assignment' */
  MarlinFullCommand BusAssignment_nx;  /* '<S13>/Bus Assignment' */
  MarlinFullCommand modeswitch1;       /* '<Root>/mode switch1' */
  MarlinFullInternalRPC BusCreator_nf; /* '<S107>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_k;  /* '<S108>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_iq; /* '<S109>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_o;  /* '<S110>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_l;  /* '<S111>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_a;  /* '<S127>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_h;  /* '<S119>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_d;  /* '<S112>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_b;  /* '<S122>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_bc; /* '<S121>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_ld; /* '<S113>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_c;  /* '<S114>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_hl; /* '<S115>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_nh; /* '<S116>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_g;  /* '<S120>/Bus Creator' */
  MarlinFullInternalRPC VectorConcatenate[15];/* '<S94>/Vector Concatenate' */
  MarlinFullInternalRPC BusCreator_br; /* '<S172>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_ih; /* '<S173>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_g1; /* '<S174>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_i2; /* '<S175>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_lg; /* '<S176>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_j;  /* '<S192>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_dy; /* '<S184>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_d1; /* '<S177>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_fc; /* '<S187>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_bw; /* '<S186>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_ow; /* '<S178>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_dj; /* '<S179>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_om; /* '<S180>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_co; /* '<S181>/Bus Creator' */
  MarlinFullInternalRPC BusCreator_gs; /* '<S185>/Bus Creator' */
  MarlinFullInternalRPC VectorConcatenate_a[15];/* '<S159>/Vector Concatenate' */
  MarlinFullInternalRPA BusConversion_InsertedFor_MATLA;
  MarlinFullInternalRPA BusConversion_InsertedFor_MAT_c;
  creal_T FFT[128];                    /* '<S32>/FFT' */
  creal_T FFT_f[128];                  /* '<S19>/FFT' */
  real_T motor_torque;                 /* '<S95>/motor_torque_' */
  real_T motor_iq;                     /* '<S95>/motor_iq_' */
  real_T motor_vq_avg;                 /* '<S95>/motor_vq_avg_' */
  real_T analog_1;                     /* '<S95>/analog_1_' */
  real_T analog_1_dot;                 /* '<S95>/analog_1_dot_' */
  real_T analog_2;                     /* '<S95>/analog_2_' */
  real_T analog_2_dot;                 /* '<S95>/analog_2_dot_' */
  real_T analog_diff;                  /* '<S95>/analog_diff_' */
  real_T analog_diff_dot;              /* '<S95>/analog_diff_dot_' */
  real_T quadrature_1;                 /* '<S95>/quadrature_1_' */
  real_T quadrature_1_dot;             /* '<S95>/quadrature_1_dot_' */
  real_T quadrature_2;                 /* '<S95>/quadrature_2_' */
  real_T quadrature_2_dot;             /* '<S95>/quadrature_2_dot_' */
  real_T ssi;                          /* '<S95>/ssi_' */
  real_T ssi_dot;                      /* '<S95>/ssi_dot_' */
  real_T accelerometer_x;              /* '<S95>/accelerometer_x_' */
  real_T accelerometer_y;              /* '<S95>/accelerometer_y_' */
  real_T accelerometer_z;              /* '<S95>/accelerometer_z_' */
  real_T bus_v;                        /* '<S95>/bus_v_' */
  real_T Delay[2];                     /* '<S93>/Delay' */
  real_T eA1[2];                       /* '<S93>/eA1' */
  real_T MathFunction;                 /* '<S93>/Math Function' */
  real_T resistance;                   /* '<S93>/resistance' */
  real_T eA2[2];                       /* '<S93>/eA2' */
  real_T Sum[2];                       /* '<S93>/Sum' */
  real_T temperature_motor_winding;    /* '<S10>/Bus Selector1' */
  real_T temperature_motor_housing;    /* '<S10>/Bus Selector1' */
  real_T motor_torque_b;               /* '<S221>/Bus Selector' */
  real_T motor_iq_k;                   /* '<S221>/Bus Selector' */
  real_T motor_vq_avg_h;               /* '<S221>/Bus Selector' */
  real_T analog_1_k;                   /* '<S221>/Bus Selector' */
  real_T analog_1_dot_e;               /* '<S221>/Bus Selector' */
  real_T analog_2_o;                   /* '<S221>/Bus Selector' */
  real_T analog_2_dot_i;               /* '<S221>/Bus Selector' */
  real_T analog_diff_g;                /* '<S221>/Bus Selector' */
  real_T analog_diff_dot_b;            /* '<S221>/Bus Selector' */
  real_T quadrature_1_h;               /* '<S221>/Bus Selector' */
  real_T quadrature_1_dot_m;           /* '<S221>/Bus Selector' */
  real_T quadrature_2_m;               /* '<S221>/Bus Selector' */
  real_T quadrature_2_dot_h;           /* '<S221>/Bus Selector' */
  real_T ssi_g;                        /* '<S221>/Bus Selector' */
  real_T ssi_dot_g;                    /* '<S221>/Bus Selector' */
  real_T accelerometer_x_f;            /* '<S221>/Bus Selector' */
  real_T accelerometer_y_n;            /* '<S221>/Bus Selector' */
  real_T accelerometer_z_m;            /* '<S221>/Bus Selector' */
  real_T bus_v_i;                      /* '<S221>/Bus Selector' */
  real_T GeneratedFilterBlock;         /* '<S223>/Generated Filter Block' */
  real_T GeneratedFilterBlock_b;       /* '<S222>/Generated Filter Block' */
  real_T GeneratedFilterBlock_c;       /* '<S224>/Generated Filter Block' */
  real_T temperature_motor_winding_j;  /* '<S6>/Bus Selector' */
  real_T temperature_motor_housing_c;  /* '<S6>/Bus Selector' */
  real_T motor_torque_m;               /* '<S65>/Bus Selector' */
  real_T motor_iq_a;                   /* '<S65>/Bus Selector' */
  real_T motor_vq_avg_f;               /* '<S65>/Bus Selector' */
  real_T analog_1_g;                   /* '<S65>/Bus Selector' */
  real_T analog_1_dot_h;               /* '<S65>/Bus Selector' */
  real_T analog_2_k;                   /* '<S65>/Bus Selector' */
  real_T analog_2_dot_m;               /* '<S65>/Bus Selector' */
  real_T analog_diff_h;                /* '<S65>/Bus Selector' */
  real_T analog_diff_dot_k;            /* '<S65>/Bus Selector' */
  real_T quadrature_1_ha;              /* '<S65>/Bus Selector' */
  real_T quadrature_1_dot_l;           /* '<S65>/Bus Selector' */
  real_T quadrature_2_o;               /* '<S65>/Bus Selector' */
  real_T quadrature_2_dot_l;           /* '<S65>/Bus Selector' */
  real_T ssi_c;                        /* '<S65>/Bus Selector' */
  real_T ssi_dot_f;                    /* '<S65>/Bus Selector' */
  real_T accelerometer_x_n;            /* '<S65>/Bus Selector' */
  real_T accelerometer_y_a;            /* '<S65>/Bus Selector' */
  real_T accelerometer_z_l;            /* '<S65>/Bus Selector' */
  real_T bus_v_k;                      /* '<S65>/Bus Selector' */
  real_T accelerometer_x_e;            /* '<S65>/accelerometer_x' */
  real_T accelerometer_y_b;            /* '<S65>/accelerometer_y' */
  real_T accelerometer_z_d;            /* '<S65>/accelerometer_z' */
  real_T analog_1_o;                   /* '<S65>/analog_1' */
  real_T analog_1_dot_d;               /* '<S65>/analog_1_dot' */
  real_T analog_2_p;                   /* '<S65>/analog_2' */
  real_T analog_2_dot_l;               /* '<S65>/analog_2_dot' */
  real_T analog_diff_b;                /* '<S65>/analog_diff' */
  real_T analog_diff_dot_f;            /* '<S65>/analog_diff_dot' */
  real_T bus_v_d;                      /* '<S65>/bus_v' */
  real_T motor_iq_g;                   /* '<S65>/motor_iq' */
  real_T motor_torque_f;               /* '<S65>/motor_torque' */
  real_T motor_vq_avg_b;               /* '<S65>/motor_vq_avg' */
  real_T quadrature_1_j;               /* '<S65>/quadrature_1' */
  real_T quadrature_1_dot_c;           /* '<S65>/quadrature_1_dot' */
  real_T quadrature_2_l;               /* '<S65>/quadrature_2' */
  real_T quadrature_2_dot_o;           /* '<S65>/quadrature_2_dot' */
  real_T ssi_f;                        /* '<S65>/ssi' */
  real_T ssi_dot_k;                    /* '<S65>/ssi_dot' */
  real_T temperature_motor_housing_j;  /* '<S6>/temperature_motor_housing' */
  real_T temperature_motor_winding_h;  /* '<S6>/temperature_motor_winding' */
  real_T motor_torque_l;               /* '<S160>/motor_torque_' */
  real_T motor_iq_b;                   /* '<S160>/motor_iq_' */
  real_T motor_vq_avg_e;               /* '<S160>/motor_vq_avg_' */
  real_T analog_1_on;                  /* '<S160>/analog_1_' */
  real_T analog_1_dot_k;               /* '<S160>/analog_1_dot_' */
  real_T analog_2_h;                   /* '<S160>/analog_2_' */
  real_T analog_2_dot_h;               /* '<S160>/analog_2_dot_' */
  real_T analog_diff_o;                /* '<S160>/analog_diff_' */
  real_T analog_diff_dot_m;            /* '<S160>/analog_diff_dot_' */
  real_T quadrature_1_i;               /* '<S160>/quadrature_1_' */
  real_T quadrature_1_dot_e;           /* '<S160>/quadrature_1_dot_' */
  real_T quadrature_2_h;               /* '<S160>/quadrature_2_' */
  real_T quadrature_2_dot_b;           /* '<S160>/quadrature_2_dot_' */
  real_T ssi_b;                        /* '<S160>/ssi_' */
  real_T ssi_dot_b;                    /* '<S160>/ssi_dot_' */
  real_T accelerometer_x_ej;           /* '<S160>/accelerometer_x_' */
  real_T accelerometer_y_j;            /* '<S160>/accelerometer_y_' */
  real_T accelerometer_z_i;            /* '<S160>/accelerometer_z_' */
  real_T bus_v_l;                      /* '<S160>/bus_v_' */
  real_T Delay_l[2];                   /* '<S158>/Delay' */
  real_T eA1_c[2];                     /* '<S158>/eA1' */
  real_T MathFunction_p;               /* '<S158>/Math Function' */
  real_T resistance_e;                 /* '<S158>/resistance' */
  real_T eA2_f[2];                     /* '<S158>/eA2' */
  real_T Sum_d[2];                     /* '<S158>/Sum' */
  real_T temperature_motor_winding_e;  /* '<S11>/Bus Selector1' */
  real_T temperature_motor_housing_b;  /* '<S11>/Bus Selector1' */
  real_T motor_torque_h;               /* '<S225>/Bus Selector' */
  real_T motor_iq_d;                   /* '<S225>/Bus Selector' */
  real_T motor_vq_avg_i;               /* '<S225>/Bus Selector' */
  real_T analog_1_gs;                  /* '<S225>/Bus Selector' */
  real_T analog_1_dot_j;               /* '<S225>/Bus Selector' */
  real_T analog_2_f;                   /* '<S225>/Bus Selector' */
  real_T analog_2_dot_c;               /* '<S225>/Bus Selector' */
  real_T analog_diff_n;                /* '<S225>/Bus Selector' */
  real_T analog_diff_dot_e;            /* '<S225>/Bus Selector' */
  real_T quadrature_1_c;               /* '<S225>/Bus Selector' */
  real_T quadrature_1_dot_o;           /* '<S225>/Bus Selector' */
  real_T quadrature_2_hx;              /* '<S225>/Bus Selector' */
  real_T quadrature_2_dot_g;           /* '<S225>/Bus Selector' */
  real_T ssi_o;                        /* '<S225>/Bus Selector' */
  real_T ssi_dot_o;                    /* '<S225>/Bus Selector' */
  real_T accelerometer_x_l;            /* '<S225>/Bus Selector' */
  real_T accelerometer_y_m;            /* '<S225>/Bus Selector' */
  real_T accelerometer_z_b;            /* '<S225>/Bus Selector' */
  real_T bus_v_a;                      /* '<S225>/Bus Selector' */
  real_T GeneratedFilterBlock_b0;      /* '<S227>/Generated Filter Block' */
  real_T GeneratedFilterBlock_g;       /* '<S226>/Generated Filter Block' */
  real_T GeneratedFilterBlock_p;       /* '<S228>/Generated Filter Block' */
  real_T temperature_motor_winding_k;  /* '<S7>/Bus Selector' */
  real_T temperature_motor_housing_jp; /* '<S7>/Bus Selector' */
  real_T motor_torque_p;               /* '<S78>/Bus Selector' */
  real_T motor_iq_f;                   /* '<S78>/Bus Selector' */
  real_T motor_vq_avg_n;               /* '<S78>/Bus Selector' */
  real_T analog_1_m;                   /* '<S78>/Bus Selector' */
  real_T analog_1_dot_f;               /* '<S78>/Bus Selector' */
  real_T analog_2_b;                   /* '<S78>/Bus Selector' */
  real_T analog_2_dot_e;               /* '<S78>/Bus Selector' */
  real_T analog_diff_hz;               /* '<S78>/Bus Selector' */
  real_T analog_diff_dot_h;            /* '<S78>/Bus Selector' */
  real_T quadrature_1_a;               /* '<S78>/Bus Selector' */
  real_T quadrature_1_dot_j;           /* '<S78>/Bus Selector' */
  real_T quadrature_2_lz;              /* '<S78>/Bus Selector' */
  real_T quadrature_2_dot_o4;          /* '<S78>/Bus Selector' */
  real_T ssi_n;                        /* '<S78>/Bus Selector' */
  real_T ssi_dot_l;                    /* '<S78>/Bus Selector' */
  real_T accelerometer_x_fe;           /* '<S78>/Bus Selector' */
  real_T accelerometer_y_jg;           /* '<S78>/Bus Selector' */
  real_T accelerometer_z_g;            /* '<S78>/Bus Selector' */
  real_T bus_v_p;                      /* '<S78>/Bus Selector' */
  real_T accelerometer_x_d;            /* '<S78>/accelerometer_x' */
  real_T accelerometer_y_e;            /* '<S78>/accelerometer_y' */
  real_T accelerometer_z_bk;           /* '<S78>/accelerometer_z' */
  real_T analog_1_b;                   /* '<S78>/analog_1' */
  real_T analog_1_dot_b;               /* '<S78>/analog_1_dot' */
  real_T analog_2_a;                   /* '<S78>/analog_2' */
  real_T analog_2_dot_k;               /* '<S78>/analog_2_dot' */
  real_T analog_diff_ni;               /* '<S78>/analog_diff' */
  real_T analog_diff_dot_i;            /* '<S78>/analog_diff_dot' */
  real_T bus_v_pu;                     /* '<S78>/bus_v' */
  real_T motor_iq_gt;                  /* '<S78>/motor_iq' */
  real_T motor_torque_pz;              /* '<S78>/motor_torque' */
  real_T motor_vq_avg_is;              /* '<S78>/motor_vq_avg' */
  real_T quadrature_1_ib;              /* '<S78>/quadrature_1' */
  real_T quadrature_1_dot_f;           /* '<S78>/quadrature_1_dot' */
  real_T quadrature_2_g;               /* '<S78>/quadrature_2' */
  real_T quadrature_2_dot_e;           /* '<S78>/quadrature_2_dot' */
  real_T ssi_h;                        /* '<S78>/ssi' */
  real_T ssi_dot_o1;                   /* '<S78>/ssi_dot' */
  real_T temperature_motor_housing_m;  /* '<S7>/temperature_motor_housing' */
  real_T temperature_motor_winding_i;  /* '<S7>/temperature_motor_winding' */
  real_T analog_1_l;                   /* '<S1>/Bus Selector' */
  real_T analog_2_f2;                  /* '<S1>/Bus Selector' */
  real_T analog_1_g0;                  /* '<S1>/Bus Selector1' */
  real_T analog_2_n;                   /* '<S1>/Bus Selector1' */
  real_T accelerometer_x_fm;           /* '<S1>/Bus Selector1' */
  real_T accelerometer_y_o;            /* '<S1>/Bus Selector1' */
  real_T accelerometer_z_c;            /* '<S1>/Bus Selector1' */
  real_T CSfunction1;                  /* '<S1>/C++ S-function1' */
  real_T CSfunction2;                  /* '<S1>/C++ S-function2' */
  real_T Gain;                         /* '<S16>/Gain' */
  real_T GeneratedFilterBlock_o;       /* '<S30>/Generated Filter Block' */
  real_T GeneratedFilterBlock_j;       /* '<S33>/Generated Filter Block' */
  real_T Subtract;                     /* '<S16>/Subtract' */
  real_T Abs;                          /* '<S16>/Abs' */
  real_T Gain1;                        /* '<S16>/Gain1' */
  real_T MathFunction_i;               /* '<S16>/Math Function' */
  real_T MathFunction1;                /* '<S16>/Math Function1' */
  real_T Buffer3[128];                 /* '<S16>/Buffer3' */
  real_T Buffer2[128];                 /* '<S16>/Buffer2' */
  real_T Buffer1[128];                 /* '<S16>/Buffer1' */
  real_T Buffer[128];                  /* '<S16>/Buffer' */
  real_T WindowFunction[128];          /* '<S16>/Window Function' */
  real_T MagnitudeSquared[128];        /* '<S32>/Magnitude Squared' */
  real_T Gain_n;                       /* '<S15>/Gain' */
  real_T GeneratedFilterBlock_gi;      /* '<S17>/Generated Filter Block' */
  real_T GeneratedFilterBlock_n;       /* '<S20>/Generated Filter Block' */
  real_T Subtract_d;                   /* '<S15>/Subtract' */
  real_T Abs_n;                        /* '<S15>/Abs' */
  real_T Gain1_k;                      /* '<S15>/Gain1' */
  real_T MathFunction_o;               /* '<S15>/Math Function' */
  real_T MathFunction1_n;              /* '<S15>/Math Function1' */
  real_T Buffer3_k[128];               /* '<S15>/Buffer3' */
  real_T Buffer2_o[128];               /* '<S15>/Buffer2' */
  real_T Buffer1_g[128];               /* '<S15>/Buffer1' */
  real_T Buffer_h[128];                /* '<S15>/Buffer' */
  real_T WindowFunction_c[128];        /* '<S15>/Window Function' */
  real_T MagnitudeSquared_h[128];      /* '<S19>/Magnitude Squared' */
  real_T CSfunction3;                  /* '<S1>/C++ S-function3' */
  real_T InlinedCSFunction_o1;         /* '<S1>/Inlined C++ S-Function' */
  real_T InlinedCSFunction_o2;         /* '<S1>/Inlined C++ S-Function' */
  real_T InlinedCSFunction_o3;         /* '<S1>/Inlined C++ S-Function' */
  real_T InlinedCSFunction_o4;         /* '<S1>/Inlined C++ S-Function' */
  real_T InlinedCSFunction_o5;         /* '<S1>/Inlined C++ S-Function' */
  real_T InlinedCSFunction_o6;         /* '<S1>/Inlined C++ S-Function' */
  real_T DataTypeConversion;           /* '<S1>/Data Type Conversion' */
  real_T DataTypeConversion1;          /* '<S1>/Data Type Conversion1' */
  real_T Sum_c;                        /* '<S48>/Sum' */
  real_T RateLimiter;                  /* '<S48>/Rate Limiter' */
  real_T DataTypeConversion_b;         /* '<S48>/Data Type Conversion' */
  real_T Sum_p;                        /* '<S49>/Sum' */
  real_T RateLimiter_o;                /* '<S49>/Rate Limiter' */
  real_T DataTypeConversion_a;         /* '<S49>/Data Type Conversion' */
  real_T Sum_cn;                       /* '<S45>/Sum' */
  real_T RateLimiter_b;                /* '<S45>/Rate Limiter' */
  real_T DataTypeConversion_c;         /* '<S45>/Data Type Conversion' */
  real_T Sum_df;                       /* '<S46>/Sum' */
  real_T RateLimiter_k;                /* '<S46>/Rate Limiter' */
  real_T DataTypeConversion_n;         /* '<S46>/Data Type Conversion' */
  real_T Sum_l;                        /* '<S47>/Sum' */
  real_T RateLimiter_e;                /* '<S47>/Rate Limiter' */
  real_T DataTypeConversion_d;         /* '<S47>/Data Type Conversion' */
  real_T quadrature_1_angle;           /* '<S2>/Bus Selector2' */
  real_T quadrature_1_angle_dot;       /* '<S2>/Bus Selector2' */
  real_T quadrature_1_n;               /* '<S2>/Bus Selector3' */
  real_T Sum_m;                        /* '<S2>/Sum' */
  real_T Product1;                     /* '<S2>/Product1' */
  real_T Saturation;                   /* '<S2>/Saturation' */
  real_T Sum1;                         /* '<S2>/Sum1' */
  real_T Product2;                     /* '<S2>/Product2' */
  real_T Sum2;                         /* '<S2>/Sum2' */
  real_T motor_torque_pj;              /* '<S2>/motor_torque' */
  real_T quadrature_1_angle_k;         /* '<S12>/Bus Selector2' */
  real_T quadrature_1_angle_dot_o;     /* '<S12>/Bus Selector2' */
  real_T quadrature_1_f;               /* '<S12>/Bus Selector1' */
  real_T analog_1_i;                   /* '<S12>/Bus Selector1' */
  real_T Sum_e;                        /* '<S12>/Sum' */
  real_T bias;                         /* '<S12>/bias' */
  real_T ka;                           /* '<S12>/ka' */
  real_T Product;                      /* '<S12>/Product' */
  real_T Sum1_o;                       /* '<S12>/Sum1' */
  real_T kd;                           /* '<S12>/kd' */
  real_T Sum2_b;                       /* '<S12>/Sum2' */
  real_T mode;                         /* '<Root>/Bus Selector' */
  real_T motor_torque_h4;              /* '<Root>/Bus Selector' */
  real_T motor_iq_b0;                  /* '<Root>/Bus Selector' */
  real_T Sum_cq;                       /* '<S58>/Sum' */
  real_T RateLimiter_d;                /* '<S58>/Rate Limiter' */
  real_T DataTypeConversion_m;         /* '<S58>/Data Type Conversion' */
  real_T Sum_a;                        /* '<S59>/Sum' */
  real_T RateLimiter_f;                /* '<S59>/Rate Limiter' */
  real_T DataTypeConversion_c2;        /* '<S59>/Data Type Conversion' */
  real_T Sum_g;                        /* '<S55>/Sum' */
  real_T RateLimiter_b5;               /* '<S55>/Rate Limiter' */
  real_T DataTypeConversion_l;         /* '<S55>/Data Type Conversion' */
  real_T Sum_b;                        /* '<S56>/Sum' */
  real_T RateLimiter_eb;               /* '<S56>/Rate Limiter' */
  real_T DataTypeConversion_j;         /* '<S56>/Data Type Conversion' */
  real_T Sum_o;                        /* '<S57>/Sum' */
  real_T RateLimiter_ol;               /* '<S57>/Rate Limiter' */
  real_T DataTypeConversion_e;         /* '<S57>/Data Type Conversion' */
  real_T quadrature_1_angle_j;         /* '<S3>/Bus Selector2' */
  real_T quadrature_1_angle_dot_of;    /* '<S3>/Bus Selector2' */
  real_T quadrature_1_nt;              /* '<S3>/Bus Selector3' */
  real_T Sum_f;                        /* '<S3>/Sum' */
  real_T Product1_j;                   /* '<S3>/Product1' */
  real_T Saturation_l;                 /* '<S3>/Saturation' */
  real_T Sum1_h;                       /* '<S3>/Sum1' */
  real_T Product2_g;                   /* '<S3>/Product2' */
  real_T Sum2_e;                       /* '<S3>/Sum2' */
  real_T motor_torque_bg;              /* '<S3>/motor_torque' */
  real_T quadrature_1_angle_o;         /* '<S13>/Bus Selector2' */
  real_T quadrature_1_angle_dot_e;     /* '<S13>/Bus Selector2' */
  real_T quadrature_1_o;               /* '<S13>/Bus Selector1' */
  real_T analog_1_h;                   /* '<S13>/Bus Selector1' */
  real_T Sum_co;                       /* '<S13>/Sum' */
  real_T bias_c;                       /* '<S13>/bias' */
  real_T ka_l;                         /* '<S13>/ka' */
  real_T Product_i;                    /* '<S13>/Product' */
  real_T Sum1_e;                       /* '<S13>/Sum1' */
  real_T kd_l;                         /* '<S13>/kd' */
  real_T Sum2_o;                       /* '<S13>/Sum2' */
  real_T mode_l;                       /* '<Root>/Bus Selector1' */
  real_T motor_torque_px;              /* '<Root>/Bus Selector1' */
  real_T motor_iq_n;                   /* '<Root>/Bus Selector1' */
  real_T kd_b;                         /* '<S2>/kd' */
  real_T kp;                           /* '<S2>/kp' */
  real_T kd_j;                         /* '<S3>/kd' */
  real_T kp_h;                         /* '<S3>/kp' */
  real_T Clock1;                       /* '<S50>/Clock1' */
  real_T Product_j;                    /* '<S50>/Product' */
  real_T Gain_g;                       /* '<S50>/Gain' */
  real_T Product1_k;                   /* '<S50>/Product1' */
  real_T Sum_j;                        /* '<S50>/Sum' */
  real_T Product2_n;                   /* '<S50>/Product2' */
  real_T Output;                       /* '<S50>/Output' */
  real_T SineWave;                     /* '<S45>/Sine Wave' */
  real_T Sign;                         /* '<S45>/Sign' */
  real_T MultiportSwitch;              /* '<S45>/Multiport Switch' */
  real_T Gain_a;                       /* '<S45>/Gain' */
  real_T Clock1_n;                     /* '<S51>/Clock1' */
  real_T Product_p;                    /* '<S51>/Product' */
  real_T Gain_b;                       /* '<S51>/Gain' */
  real_T Product1_o;                   /* '<S51>/Product1' */
  real_T Sum_k;                        /* '<S51>/Sum' */
  real_T Product2_p;                   /* '<S51>/Product2' */
  real_T Output_p;                     /* '<S51>/Output' */
  real_T SineWave_h;                   /* '<S46>/Sine Wave' */
  real_T Sign_a;                       /* '<S46>/Sign' */
  real_T MultiportSwitch_e;            /* '<S46>/Multiport Switch' */
  real_T Gain_bc;                      /* '<S46>/Gain' */
  real_T Clock1_e;                     /* '<S52>/Clock1' */
  real_T Product_b;                    /* '<S52>/Product' */
  real_T Gain_nn;                      /* '<S52>/Gain' */
  real_T Product1_c;                   /* '<S52>/Product1' */
  real_T Sum_kn;                       /* '<S52>/Sum' */
  real_T Product2_no;                  /* '<S52>/Product2' */
  real_T Output_n;                     /* '<S52>/Output' */
  real_T SineWave_f;                   /* '<S47>/Sine Wave' */
  real_T Sign_m;                       /* '<S47>/Sign' */
  real_T MultiportSwitch_l;            /* '<S47>/Multiport Switch' */
  real_T Gain_l;                       /* '<S47>/Gain' */
  real_T Clock1_g;                     /* '<S53>/Clock1' */
  real_T Product_d;                    /* '<S53>/Product' */
  real_T Gain_nc;                      /* '<S53>/Gain' */
  real_T Product1_i;                   /* '<S53>/Product1' */
  real_T Sum_i;                        /* '<S53>/Sum' */
  real_T Product2_i;                   /* '<S53>/Product2' */
  real_T Output_h;                     /* '<S53>/Output' */
  real_T SineWave_l;                   /* '<S48>/Sine Wave' */
  real_T Sign_f;                       /* '<S48>/Sign' */
  real_T MultiportSwitch_n;            /* '<S48>/Multiport Switch' */
  real_T Gain_f;                       /* '<S48>/Gain' */
  real_T Clock1_j;                     /* '<S54>/Clock1' */
  real_T Product_h;                    /* '<S54>/Product' */
  real_T Gain_nz;                      /* '<S54>/Gain' */
  real_T Product1_f;                   /* '<S54>/Product1' */
  real_T Sum_bg;                       /* '<S54>/Sum' */
  real_T Product2_l;                   /* '<S54>/Product2' */
  real_T Output_c;                     /* '<S54>/Output' */
  real_T SineWave_n;                   /* '<S49>/Sine Wave' */
  real_T Sign_i;                       /* '<S49>/Sign' */
  real_T MultiportSwitch_b;            /* '<S49>/Multiport Switch' */
  real_T Gain_gk;                      /* '<S49>/Gain' */
  real_T Clock1_gp;                    /* '<S60>/Clock1' */
  real_T Product_bh;                   /* '<S60>/Product' */
  real_T Gain_a3;                      /* '<S60>/Gain' */
  real_T Product1_e;                   /* '<S60>/Product1' */
  real_T Sum_i2;                       /* '<S60>/Sum' */
  real_T Product2_d;                   /* '<S60>/Product2' */
  real_T Output_i;                     /* '<S60>/Output' */
  real_T SineWave_ff;                  /* '<S55>/Sine Wave' */
  real_T Sign_ml;                      /* '<S55>/Sign' */
  real_T MultiportSwitch_h;            /* '<S55>/Multiport Switch' */
  real_T Gain_ncb;                     /* '<S55>/Gain' */
  real_T Clock1_f;                     /* '<S61>/Clock1' */
  real_T Product_ie;                   /* '<S61>/Product' */
  real_T Gain_d;                       /* '<S61>/Gain' */
  real_T Product1_p;                   /* '<S61>/Product1' */
  real_T Sum_c3;                       /* '<S61>/Sum' */
  real_T Product2_np;                  /* '<S61>/Product2' */
  real_T Output_f;                     /* '<S61>/Output' */
  real_T SineWave_hf;                  /* '<S56>/Sine Wave' */
  real_T Sign_h;                       /* '<S56>/Sign' */
  real_T MultiportSwitch_o;            /* '<S56>/Multiport Switch' */
  real_T Gain_gv;                      /* '<S56>/Gain' */
  real_T Clock1_l;                     /* '<S62>/Clock1' */
  real_T Product_k;                    /* '<S62>/Product' */
  real_T Gain_h;                       /* '<S62>/Gain' */
  real_T Product1_pt;                  /* '<S62>/Product1' */
  real_T Sum_dl;                       /* '<S62>/Sum' */
  real_T Product2_j;                   /* '<S62>/Product2' */
  real_T Output_e;                     /* '<S62>/Output' */
  real_T SineWave_g;                   /* '<S57>/Sine Wave' */
  real_T Sign_k;                       /* '<S57>/Sign' */
  real_T MultiportSwitch_a;            /* '<S57>/Multiport Switch' */
  real_T Gain_i;                       /* '<S57>/Gain' */
  real_T Clock1_d;                     /* '<S63>/Clock1' */
  real_T Product_kg;                   /* '<S63>/Product' */
  real_T Gain_c;                       /* '<S63>/Gain' */
  real_T Product1_jw;                  /* '<S63>/Product1' */
  real_T Sum_en;                       /* '<S63>/Sum' */
  real_T Product2_gz;                  /* '<S63>/Product2' */
  real_T Output_m;                     /* '<S63>/Output' */
  real_T SineWave_m;                   /* '<S58>/Sine Wave' */
  real_T Sign_l;                       /* '<S58>/Sign' */
  real_T MultiportSwitch_i;            /* '<S58>/Multiport Switch' */
  real_T Gain_m;                       /* '<S58>/Gain' */
  real_T Clock1_h;                     /* '<S64>/Clock1' */
  real_T Product_if;                   /* '<S64>/Product' */
  real_T Gain_p;                       /* '<S64>/Gain' */
  real_T Product1_j2;                  /* '<S64>/Product1' */
  real_T Sum_ke;                       /* '<S64>/Sum' */
  real_T Product2_gp;                  /* '<S64>/Product2' */
  real_T Output_l;                     /* '<S64>/Output' */
  real_T SineWave_ld;                  /* '<S59>/Sine Wave' */
  real_T Sign_ip;                      /* '<S59>/Sign' */
  real_T MultiportSwitch_k;            /* '<S59>/Multiport Switch' */
  real_T Gain_nh;                      /* '<S59>/Gain' */
  real_T rpc;                          /* '<S8>/Bus Selector' */
  real_T debug;                        /* '<S95>/debug_' */
  real_T mode_k;                       /* '<S92>/Bus Selector' */
  real_T motor_iq_kg;                  /* '<S92>/Bus Selector1' */
  real_T motor_id;                     /* '<S92>/Bus Selector1' */
  real_T motor_vq;                     /* '<S92>/Bus Selector2' */
  real_T motor_vd;                     /* '<S92>/Bus Selector2' */
  real_T analog_diff_g1;               /* '<S92>/Bus Selector3' */
  real_T analog_diff_dot_o;            /* '<S92>/Bus Selector3' */
  real_T quadrature_1_angle_b;         /* '<S92>/Bus Selector4' */
  real_T quadrature_1_angle_dot_j;     /* '<S92>/Bus Selector4' */
  real_T motor_torque_mv;              /* '<S92>/Bus Selector5' */
  real_T VectorConcatenate_d[76];      /* '<S128>/Vector Concatenate' */
  real_T DataTypeConversion_cm[10];    /* '<S112>/Data Type Conversion' */
  real_T analog_limit;                 /* '<S112>/analog_limit' */
  real_T ethercat;                     /* '<S112>/ethercat' */
  real_T external_panic;               /* '<S112>/external_panic' */
  real_T motor_encoder;                /* '<S112>/motor encoder' */
  real_T over_current;                 /* '<S112>/over_current' */
  real_T over_voltage;                 /* '<S112>/over_voltage' */
  real_T quadrature_1_high;            /* '<S112>/quadrature_1_high' */
  real_T quadrature_1_low;             /* '<S112>/quadrature_1_low' */
  real_T quadrature_2_limit;           /* '<S112>/quadrature_2_limit' */
  real_T under_voltage;                /* '<S112>/under_voltage' */
  real_T rpc_l;                        /* '<S9>/Bus Selector' */
  real_T debug_d;                      /* '<S160>/debug_' */
  real_T mode_g;                       /* '<S157>/Bus Selector' */
  real_T motor_iq_m;                   /* '<S157>/Bus Selector1' */
  real_T motor_id_k;                   /* '<S157>/Bus Selector1' */
  real_T motor_vq_d;                   /* '<S157>/Bus Selector2' */
  real_T motor_vd_m;                   /* '<S157>/Bus Selector2' */
  real_T analog_diff_i;                /* '<S157>/Bus Selector3' */
  real_T analog_diff_dot_ky;           /* '<S157>/Bus Selector3' */
  real_T quadrature_1_angle_c;         /* '<S157>/Bus Selector4' */
  real_T quadrature_1_angle_dot_d;     /* '<S157>/Bus Selector4' */
  real_T motor_torque_hy;              /* '<S157>/Bus Selector5' */
  real_T VectorConcatenate_e[76];      /* '<S193>/Vector Concatenate' */
  real_T DataTypeConversion_i[10];     /* '<S177>/Data Type Conversion' */
  real_T analog_limit_k;               /* '<S177>/analog_limit' */
  real_T ethercat_l;                   /* '<S177>/ethercat' */
  real_T external_panic_l;             /* '<S177>/external_panic' */
  real_T motor_encoder_h;              /* '<S177>/motor encoder' */
  real_T over_current_p;               /* '<S177>/over_current' */
  real_T over_voltage_o;               /* '<S177>/over_voltage' */
  real_T quadrature_1_high_p;          /* '<S177>/quadrature_1_high' */
  real_T quadrature_1_low_c;           /* '<S177>/quadrature_1_low' */
  real_T quadrature_2_limit_a;         /* '<S177>/quadrature_2_limit' */
  real_T under_voltage_l;              /* '<S177>/under_voltage' */
  real_T mode0;                        /* '<Root>/mode0' */
  real_T mode1;                        /* '<Root>/mode1' */
  real_T motor_iq0;                    /* '<Root>/motor_iq0' */
  real_T motor_iq1;                    /* '<Root>/motor_iq1' */
  real_T motor_torque0;                /* '<Root>/motor_torque0' */
  real_T motor_torque1;                /* '<Root>/motor_torque1' */
  real_T modeselect;                   /* '<Root>/mode select' */
  real_T modeselect1;                  /* '<Root>/mode select1' */
  real_T y;                            /* '<S16>/MATLAB Function' */
  real_T y_d;                          /* '<S15>/MATLAB Function' */
  real32_T marlin_ec_o1;               /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o2;               /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o3;               /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o4;               /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o5;               /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o6;               /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o7;               /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o8;               /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o9;               /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o10;              /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o11;              /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o12;              /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o13;              /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o14;              /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o15;              /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o16;              /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o17;              /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o18;              /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o20;              /* '<S95>/marlin_ec' */
  real32_T marlin_ec_o21;              /* '<S95>/marlin_ec' */
  real32_T rpa_float[7];               /* '<S65>/rpa_float' */
  real32_T marlin_ec_o1_k;             /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o2_f;             /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o3_h;             /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o4_a;             /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o5_l;             /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o6_j;             /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o7_c;             /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o8_j;             /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o9_j;             /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o10_n;            /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o11_d;            /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o12_h;            /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o13_b;            /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o14_e;            /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o15_h;            /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o16_c;            /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o17_a;            /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o18_g;            /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o20_k;            /* '<S160>/marlin_ec' */
  real32_T marlin_ec_o21_m;            /* '<S160>/marlin_ec' */
  real32_T rpa_float_f[7];             /* '<S78>/rpa_float' */
  real32_T DataTypeConversion1_f[14];  /* '<S103>/Data Type Conversion1' */
  real32_T DataTypeConversion1_o[14];  /* '<S100>/Data Type Conversion1' */
  real32_T DataTypeConversion1_oj[14]; /* '<S104>/Data Type Conversion1' */
  real32_T DataTypeConversion1_b[14];  /* '<S101>/Data Type Conversion1' */
  real32_T DataTypeConversion1_e[14];  /* '<S106>/Data Type Conversion1' */
  real32_T DataTypeConversion1_fh[14]; /* '<S99>/Data Type Conversion1' */
  real32_T DataTypeConversion1_fm[14]; /* '<S105>/Data Type Conversion1' */
  real32_T DataTypeConversion1_h[14];  /* '<S102>/Data Type Conversion1' */
  real32_T DataTypeConversion_f[76];   /* '<S128>/Data Type Conversion' */
  real32_T id;                         /* '<S94>/Byte Unpack' */
  real32_T ByteUnpack[5];              /* '<S113>/Byte Unpack' */
  real32_T loada;                      /* '<S113>/load a' */
  real32_T loada1;                     /* '<S113>/load a1' */
  real32_T loada2;                     /* '<S113>/load a2' */
  real32_T loada3;                     /* '<S113>/load a3' */
  real32_T loadb;                      /* '<S113>/load b' */
  real32_T DataTypeConversion1_d[14];  /* '<S168>/Data Type Conversion1' */
  real32_T DataTypeConversion1_bm[14]; /* '<S165>/Data Type Conversion1' */
  real32_T DataTypeConversion1_ev[14]; /* '<S169>/Data Type Conversion1' */
  real32_T DataTypeConversion1_a[14];  /* '<S166>/Data Type Conversion1' */
  real32_T DataTypeConversion1_ey[14]; /* '<S171>/Data Type Conversion1' */
  real32_T DataTypeConversion1_fj[14]; /* '<S164>/Data Type Conversion1' */
  real32_T DataTypeConversion1_aj[14]; /* '<S170>/Data Type Conversion1' */
  real32_T DataTypeConversion1_c[14];  /* '<S167>/Data Type Conversion1' */
  real32_T DataTypeConversion_aa[76];  /* '<S193>/Data Type Conversion' */
  real32_T id_p;                       /* '<S159>/Byte Unpack' */
  real32_T ByteUnpack_h[5];            /* '<S178>/Byte Unpack' */
  real32_T loada_c;                    /* '<S178>/load a' */
  real32_T loada1_h;                   /* '<S178>/load a1' */
  real32_T loada2_p;                   /* '<S178>/load a2' */
  real32_T loada3_o;                   /* '<S178>/load a3' */
  real32_T loadb_a;                    /* '<S178>/load b' */
  int32_T type;                        /* '<S94>/Byte Unpack' */
  int32_T current_index;               /* '<S117>/current_index' */
  int32_T type_b;                      /* '<S159>/Byte Unpack' */
  int32_T current_index_c;             /* '<S182>/current_index' */
  uint32_T marlin_ec_o19;              /* '<S95>/marlin_ec' */
  uint32_T faults;                     /* '<S95>/faults_' */
  uint32_T faults_c;                   /* '<S221>/Bus Selector' */
  uint32_T faults_p;                   /* '<S65>/Bus Selector' */
  uint32_T raw_integer;                /* '<S66>/raw_integer' */
  uint32_T marlin_ec_o19_n;            /* '<S160>/marlin_ec' */
  uint32_T faults_k;                   /* '<S160>/faults_' */
  uint32_T faults_k5;                  /* '<S225>/Bus Selector' */
  uint32_T faults_o;                   /* '<S78>/Bus Selector' */
  uint32_T raw_integer_l;              /* '<S79>/raw_integer' */
  uint32_T Output_o;                   /* '<S96>/Output' */
  uint32_T FixPtSum1;                  /* '<S97>/FixPt Sum1' */
  uint32_T FixPtSwitch;                /* '<S98>/FixPt Switch' */
  uint32_T mode_a;                     /* '<S92>/Multiport Switch' */
  uint32_T DataTypeConversion_g[2];    /* '<S114>/Data Type Conversion' */
  uint32_T DataTypeConversion_h[2];    /* '<S115>/Data Type Conversion' */
  uint32_T DataTypeConversion_ee[2];   /* '<S116>/Data Type Conversion' */
  uint32_T Output_i4;                  /* '<S161>/Output' */
  uint32_T FixPtSum1_e;                /* '<S162>/FixPt Sum1' */
  uint32_T FixPtSwitch_b;              /* '<S163>/FixPt Switch' */
  uint32_T mode_e;                     /* '<S157>/Multiport Switch' */
  uint32_T DataTypeConversion_k[2];    /* '<S179>/Data Type Conversion' */
  uint32_T DataTypeConversion_k0[2];   /* '<S180>/Data Type Conversion' */
  uint32_T DataTypeConversion_iv[2];   /* '<S181>/Data Type Conversion' */
  uint32_T DomainState_o1;             /* '<Root>/Domain State' */
  uint32_T MasterState_o1;             /* '<Root>/Master State' */
  uint16_T ByteUnpack_e[10];           /* '<S112>/Byte Unpack' */
  uint16_T ByteUnpack_eb[10];          /* '<S177>/Byte Unpack' */
  uint8_T marlin_ec_o22[28];           /* '<S95>/marlin_ec' */
  uint8_T rpa_packet[28];              /* '<S95>/rpa_packet_' */
  uint8_T rpa_packet_n[28];            /* '<S221>/Bus Selector' */
  uint8_T rpa_packet_k[28];            /* '<S65>/Bus Selector' */
  uint8_T marlin_ec_o22_a[28];         /* '<S160>/marlin_ec' */
  uint8_T rpa_packet_b[28];            /* '<S160>/rpa_packet_' */
  uint8_T rpa_packet_i[28];            /* '<S225>/Bus Selector' */
  uint8_T rpa_packet_g[28];            /* '<S78>/Bus Selector' */
  uint8_T command[56];                 /* '<S103>/Byte Pack3' */
  uint8_T command_j[56];               /* '<S100>/Byte Pack3' */
  uint8_T command_k[56];               /* '<S104>/Byte Pack3' */
  uint8_T command_l[56];               /* '<S101>/Byte Pack2' */
  uint8_T command_f[56];               /* '<S106>/Byte Pack2' */
  uint8_T command_g[56];               /* '<S99>/Byte Pack2' */
  uint8_T command_gi[56];              /* '<S105>/Byte Pack2' */
  uint8_T command_a[56];               /* '<S102>/Byte Pack2' */
  uint8_T command_e[56];               /* '<S92>/Multiport Switch' */
  uint8_T data[20];                    /* '<S94>/Byte Unpack' */
  uint8_T data_m[20];                  /* '<S127>/Byte Pack' */
  uint8_T data_e[20];                  /* '<S114>/Byte Pack' */
  uint8_T data_b[20];                  /* '<S115>/Byte Pack' */
  uint8_T data_n[20];                  /* '<S116>/Byte Pack' */
  uint8_T rpc_i[28];                   /* '<S94>/Byte Pack' */
  uint8_T command_li[56];              /* '<S168>/Byte Pack3' */
  uint8_T command_n[56];               /* '<S165>/Byte Pack3' */
  uint8_T command_o[56];               /* '<S169>/Byte Pack3' */
  uint8_T command_i[56];               /* '<S166>/Byte Pack2' */
  uint8_T command_h[56];               /* '<S171>/Byte Pack2' */
  uint8_T command_hl[56];              /* '<S164>/Byte Pack2' */
  uint8_T command_gh[56];              /* '<S170>/Byte Pack2' */
  uint8_T command_jo[56];              /* '<S167>/Byte Pack2' */
  uint8_T command_c[56];               /* '<S157>/Multiport Switch' */
  uint8_T data_a[20];                  /* '<S159>/Byte Unpack' */
  uint8_T data_e5[20];                 /* '<S192>/Byte Pack' */
  uint8_T data_o[20];                  /* '<S179>/Byte Pack' */
  uint8_T data_bl[20];                 /* '<S180>/Byte Pack' */
  uint8_T data_mh[20];                 /* '<S181>/Byte Pack' */
  uint8_T rpc_n[28];                   /* '<S159>/Byte Pack' */
  uint8_T DomainState_o2;              /* '<Root>/Domain State' */
  uint8_T MasterState_o2;              /* '<Root>/Master State' */
  boolean_T analog;                    /* '<S66>/analog_' */
  boolean_T communication_fault;       /* '<S66>/communication_fault_' */
  boolean_T current;                   /* '<S66>/current_' */
  boolean_T ethercat_m;                /* '<S66>/ethercat_' */
  boolean_T external_panic_e;          /* '<S66>/external_panic_' */
  boolean_T hard_fault;                /* '<S66>/hard_fault_' */
  boolean_T quadrature;                /* '<S66>/quadrature_' */
  boolean_T soft_fault;                /* '<S66>/soft_fault_' */
  boolean_T ssi_i;                     /* '<S66>/ssi_' */
  boolean_T temperature;               /* '<S66>/temperature_' */
  boolean_T voltage;                   /* '<S66>/voltage_' */
  boolean_T analog_i;                  /* '<S79>/analog_' */
  boolean_T communication_fault_o;     /* '<S79>/communication_fault_' */
  boolean_T current_h;                 /* '<S79>/current_' */
  boolean_T ethercat_e;                /* '<S79>/ethercat_' */
  boolean_T external_panic_o;          /* '<S79>/external_panic_' */
  boolean_T hard_fault_e;              /* '<S79>/hard_fault_' */
  boolean_T quadrature_l;              /* '<S79>/quadrature_' */
  boolean_T soft_fault_a;              /* '<S79>/soft_fault_' */
  boolean_T ssi_m;                     /* '<S79>/ssi_' */
  boolean_T temperature_b;             /* '<S79>/temperature_' */
  boolean_T voltage_f;                 /* '<S79>/voltage_' */
  boolean_T Compare;                   /* '<S123>/Compare' */
  boolean_T Compare_a;                 /* '<S125>/Compare' */
  boolean_T Compare_b;                 /* '<S188>/Compare' */
  boolean_T Compare_aw;                /* '<S190>/Compare' */
  boolean_T MasterState_o3;            /* '<Root>/Master State' */
  uint8_T ExtractDesiredBits;          /* '<S77>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_c;        /* '<S90>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_n;        /* '<S67>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly;           /* '<S67>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_c;         /* '<S68>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_j;         /* '<S69>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_b;         /* '<S70>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_jt;        /* '<S71>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_h;         /* '<S72>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_m;         /* '<S73>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_i;         /* '<S74>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_mj;        /* '<S75>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_jz;        /* '<S76>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_d;         /* '<S77>/Modify Scaling Only' */
  uint8_T ExtractDesiredBits_p;        /* '<S80>/Extract Desired Bits' */
  uint8_T ModifyScalingOnly_je;        /* '<S80>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_o;         /* '<S81>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_p;         /* '<S82>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_oc;        /* '<S83>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_l;         /* '<S84>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_e;         /* '<S85>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_f;         /* '<S86>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_cr;        /* '<S87>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_n;         /* '<S88>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_k;         /* '<S89>/Modify Scaling Only' */
  uint8_T ModifyScalingOnly_pi;        /* '<S90>/Modify Scaling Only' */
  uint8_T ExtractDesiredBits_pr;       /* '<S68>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_f;        /* '<S81>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_k;        /* '<S70>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_f2;       /* '<S83>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_l;        /* '<S71>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_j;        /* '<S84>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_d;        /* '<S72>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_lr;       /* '<S85>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_h;        /* '<S73>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_o;        /* '<S86>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_ng;       /* '<S74>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_g;        /* '<S87>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_fo;       /* '<S75>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_kb;       /* '<S88>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_cg;       /* '<S76>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_i;        /* '<S89>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_dw;       /* '<S69>/Extract Desired Bits' */
  uint8_T ExtractDesiredBits_i1;       /* '<S82>/Extract Desired Bits' */
  B_FilteredDerivative_drum_ben_T FilteredDerivative_p;/* '<S13>/Filtered Derivative' */
  B_FilteredDerivative_drum_ben_T FilteredDerivative_c;/* '<S12>/Filtered Derivative' */
  B_selectrpc_drum_bench_T sf_selectrpc_j;/* '<S159>/select rpc' */
  B_MATLABFunction1_drum_bench_T sf_MATLABFunction1_a;/* '<S192>/MATLAB Function1' */
  B_EnabledSubsystem_drum_bench_T EnabledSubsystem_k;/* '<S178>/Enabled Subsystem' */
  B_enabledsubsystem_drum_bench_T enabledsubsystem_l;/* '<S177>/enabled subsystem' */
  B_selectrpc_drum_bench_T sf_selectrpc;/* '<S94>/select rpc' */
  B_MATLABFunction1_drum_bench_T sf_MATLABFunction1;/* '<S127>/MATLAB Function1' */
  B_EnabledSubsystem_drum_bench_T EnabledSubsystem;/* '<S113>/Enabled Subsystem' */
  B_enabledsubsystem_drum_bench_T enabledsubsystem;/* '<S112>/enabled subsystem' */
  B_FilteredDerivative_drum_ben_T FilteredDerivative_n;/* '<S3>/Filtered Derivative' */
  B_FilteredDerivative_drum_ben_T FilteredDerivative;/* '<S2>/Filtered Derivative' */
  B_zeroCrossing_drum_bench_T sf_zeroCrossing;/* '<S16>/zeroCrossing' */
  B_smooth3_drum_bench_T sf_smooth3_d; /* '<S16>/smooth3' */
  B_smooth1_drum_bench_T sf_smooth2_h; /* '<S16>/smooth2' */
  B_smooth1_drum_bench_T sf_smooth1_p; /* '<S16>/smooth1' */
  B_smooth_drum_bench_T sf_smooth;     /* '<S16>/smooth' */
  B_slopeChange_drum_bench_T sf_slopeChange;/* '<S16>/slopeChange' */
  B_mean_drum_bench_T sf_mean2;        /* '<S16>/mean2' */
  B_mean_drum_bench_T sf_mean1;        /* '<S16>/mean1' */
  B_mean_drum_bench_T sf_mean;         /* '<S16>/mean' */
  B_zeroCrossing_drum_bench_T sf_zeroCrossing_f;/* '<S15>/zeroCrossing' */
  B_smooth3_drum_bench_T sf_smooth3;   /* '<S15>/smooth3' */
  B_smooth1_drum_bench_T sf_smooth2;   /* '<S15>/smooth2' */
  B_smooth1_drum_bench_T sf_smooth1;   /* '<S15>/smooth1' */
  B_smooth_drum_bench_T sf_smooth_a;   /* '<S15>/smooth' */
  B_slopeChange_drum_bench_T sf_slopeChange_n;/* '<S15>/slopeChange' */
  B_mean_drum_bench_T sf_mean2_b;      /* '<S15>/mean2' */
  B_mean_drum_bench_T sf_mean1_m;      /* '<S15>/mean1' */
  B_mean_drum_bench_T sf_mean_p;       /* '<S15>/mean' */
} B_drum_bench_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T Delay_DSTATE[2];              /* '<S93>/Delay' */
  real_T GeneratedFilterBlock_states[23];/* '<S223>/Generated Filter Block' */
  real_T GeneratedFilterBlock_states_f[23];/* '<S222>/Generated Filter Block' */
  real_T GeneratedFilterBlock_states_c[23];/* '<S224>/Generated Filter Block' */
  real_T Delay_DSTATE_n[2];            /* '<S158>/Delay' */
  real_T GeneratedFilterBlock_states_ch[23];/* '<S227>/Generated Filter Block' */
  real_T GeneratedFilterBlock_states_k[23];/* '<S226>/Generated Filter Block' */
  real_T GeneratedFilterBlock_states_l[23];/* '<S228>/Generated Filter Block' */
  real_T GeneratedFilterBlock_states_l3[140];/* '<S30>/Generated Filter Block' */
  real_T GeneratedFilterBlock_FILT_STATE[6];/* '<S33>/Generated Filter Block' */
  real_T Buffer3_CircBuf[256];         /* '<S16>/Buffer3' */
  real_T Buffer2_CircBuf[256];         /* '<S16>/Buffer2' */
  real_T Buffer1_CircBuf[256];         /* '<S16>/Buffer1' */
  real_T Buffer_CircBuf[256];          /* '<S16>/Buffer' */
  real_T GeneratedFilterBlock_states_kq[140];/* '<S17>/Generated Filter Block' */
  real_T GeneratedFilterBlock_FILT_STA_d[6];/* '<S20>/Generated Filter Block' */
  real_T Buffer3_CircBuf_k[256];       /* '<S15>/Buffer3' */
  real_T Buffer2_CircBuf_o[256];       /* '<S15>/Buffer2' */
  real_T Buffer1_CircBuf_a[256];       /* '<S15>/Buffer1' */
  real_T Buffer_CircBuf_f[256];        /* '<S15>/Buffer' */
  real_T PrevY;                        /* '<S48>/Rate Limiter' */
  real_T PrevY_a;                      /* '<S49>/Rate Limiter' */
  real_T PrevY_h;                      /* '<S45>/Rate Limiter' */
  real_T PrevY_d;                      /* '<S46>/Rate Limiter' */
  real_T PrevY_dx;                     /* '<S47>/Rate Limiter' */
  real_T PrevY_m;                      /* '<S58>/Rate Limiter' */
  real_T PrevY_b;                      /* '<S59>/Rate Limiter' */
  real_T PrevY_f;                      /* '<S55>/Rate Limiter' */
  real_T PrevY_d5;                     /* '<S56>/Rate Limiter' */
  real_T PrevY_k;                      /* '<S57>/Rate Limiter' */
  real_T buf[128];                     /* '<S16>/MATLAB Function' */
  real_T buf_j[128];                   /* '<S15>/MATLAB Function' */
  void *CSfunction1_PWORK;             /* '<S1>/C++ S-function1' */
  void *CSfunction2_PWORK;             /* '<S1>/C++ S-function2' */
  void *CSfunction3_PWORK[2];          /* '<S1>/C++ S-function3' */
  void *InlinedCSFunction_PWORK[4];    /* '<S1>/Inlined C++ S-Function' */
  struct {
    void *DomainPtr;
  } DomainState_PWORK;                 /* '<Root>/Domain State' */

  struct {
    void *MasterPtr;
  } MasterState_PWORK;                 /* '<Root>/Master State' */

  int32_T GeneratedFilterBlock_circBuf;/* '<S223>/Generated Filter Block' */
  int32_T GeneratedFilterBlock_circBuf_n;/* '<S222>/Generated Filter Block' */
  int32_T GeneratedFilterBlock_circBuf_b;/* '<S224>/Generated Filter Block' */
  int32_T GeneratedFilterBlock_circBuf_o;/* '<S227>/Generated Filter Block' */
  int32_T GeneratedFilterBlock_circBuf_k;/* '<S226>/Generated Filter Block' */
  int32_T GeneratedFilterBlock_circBuf_bj;/* '<S228>/Generated Filter Block' */
  int32_T GeneratedFilterBlock_circBuf_a;/* '<S30>/Generated Filter Block' */
  int32_T Buffer3_inBufPtrIdx;         /* '<S16>/Buffer3' */
  int32_T Buffer3_outBufPtrIdx;        /* '<S16>/Buffer3' */
  int32_T Buffer3_bufferLength;        /* '<S16>/Buffer3' */
  int32_T Buffer2_inBufPtrIdx;         /* '<S16>/Buffer2' */
  int32_T Buffer2_outBufPtrIdx;        /* '<S16>/Buffer2' */
  int32_T Buffer2_bufferLength;        /* '<S16>/Buffer2' */
  int32_T Buffer1_inBufPtrIdx;         /* '<S16>/Buffer1' */
  int32_T Buffer1_outBufPtrIdx;        /* '<S16>/Buffer1' */
  int32_T Buffer1_bufferLength;        /* '<S16>/Buffer1' */
  int32_T Buffer_inBufPtrIdx;          /* '<S16>/Buffer' */
  int32_T Buffer_outBufPtrIdx;         /* '<S16>/Buffer' */
  int32_T Buffer_bufferLength;         /* '<S16>/Buffer' */
  int32_T GeneratedFilterBlock_circBuf_o2;/* '<S17>/Generated Filter Block' */
  int32_T Buffer3_inBufPtrIdx_m;       /* '<S15>/Buffer3' */
  int32_T Buffer3_outBufPtrIdx_a;      /* '<S15>/Buffer3' */
  int32_T Buffer3_bufferLength_b;      /* '<S15>/Buffer3' */
  int32_T Buffer2_inBufPtrIdx_o;       /* '<S15>/Buffer2' */
  int32_T Buffer2_outBufPtrIdx_p;      /* '<S15>/Buffer2' */
  int32_T Buffer2_bufferLength_l;      /* '<S15>/Buffer2' */
  int32_T Buffer1_inBufPtrIdx_b;       /* '<S15>/Buffer1' */
  int32_T Buffer1_outBufPtrIdx_l;      /* '<S15>/Buffer1' */
  int32_T Buffer1_bufferLength_e;      /* '<S15>/Buffer1' */
  int32_T Buffer_inBufPtrIdx_d;        /* '<S15>/Buffer' */
  int32_T Buffer_outBufPtrIdx_f;       /* '<S15>/Buffer' */
  int32_T Buffer_bufferLength_k;       /* '<S15>/Buffer' */
  uint32_T Output_DSTATE;              /* '<S96>/Output' */
  uint32_T Output_DSTATE_h;            /* '<S161>/Output' */
  boolean_T WindowFunction_FLAG;       /* '<S16>/Window Function' */
  boolean_T WindowFunction_FLAG_e;     /* '<S15>/Window Function' */
  DW_FilteredDerivative_drum_be_T FilteredDerivative_p;/* '<S13>/Filtered Derivative' */
  DW_FilteredDerivative_drum_be_T FilteredDerivative_c;/* '<S12>/Filtered Derivative' */
  DW_MATLABFunction1_drum_bench_T sf_MATLABFunction1_a;/* '<S192>/MATLAB Function1' */
  DW_MATLABFunction1_drum_bench_T sf_MATLABFunction1;/* '<S127>/MATLAB Function1' */
  DW_FilteredDerivative_drum_be_T FilteredDerivative_n;/* '<S3>/Filtered Derivative' */
  DW_FilteredDerivative_drum_be_T FilteredDerivative;/* '<S2>/Filtered Derivative' */
  DW_zeroCrossing_drum_bench_T sf_zeroCrossing;/* '<S16>/zeroCrossing' */
  DW_smooth3_drum_bench_T sf_smooth3_d;/* '<S16>/smooth3' */
  DW_smooth1_drum_bench_T sf_smooth2_h;/* '<S16>/smooth2' */
  DW_smooth1_drum_bench_T sf_smooth1_p;/* '<S16>/smooth1' */
  DW_smooth_drum_bench_T sf_smooth;    /* '<S16>/smooth' */
  DW_slopeChange_drum_bench_T sf_slopeChange;/* '<S16>/slopeChange' */
  DW_zeroCrossing_drum_bench_T sf_zeroCrossing_f;/* '<S15>/zeroCrossing' */
  DW_smooth3_drum_bench_T sf_smooth3;  /* '<S15>/smooth3' */
  DW_smooth1_drum_bench_T sf_smooth2;  /* '<S15>/smooth2' */
  DW_smooth1_drum_bench_T sf_smooth1;  /* '<S15>/smooth1' */
  DW_smooth_drum_bench_T sf_smooth_a;  /* '<S15>/smooth' */
  DW_slopeChange_drum_bench_T sf_slopeChange_n;/* '<S15>/slopeChange' */
} DW_drum_bench_T;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState TriggeredSubsystem_Trig_ZCE;/* '<S14>/Triggered Subsystem' */
} PrevZCX_drum_bench_T;

/* Invariant block signals (auto storage) */
typedef struct {
  const real_T modeselect2;            /* '<Root>/mode select2' */
  const uint8_T data[20];              /* '<S107>/Byte Pack' */
  const uint8_T data_p[20];            /* '<S108>/Byte Pack' */
  const uint8_T data_f[20];            /* '<S109>/Byte Pack' */
  const uint8_T data_c[20];            /* '<S110>/Byte Pack' */
  const uint8_T data_h[20];            /* '<S111>/Byte Pack' */
  const uint8_T data_e[20];            /* '<S119>/Byte Pack' */
  const uint8_T data_d[20];            /* '<S112>/Byte Pack' */
  const uint8_T data_cu[20];           /* '<S122>/Byte Pack' */
  const uint8_T data_o[20];            /* '<S121>/Byte Pack' */
  const uint8_T data_cz[20];           /* '<S113>/Byte Pack' */
  const uint8_T data_ef[20];           /* '<S172>/Byte Pack' */
  const uint8_T data_c4[20];           /* '<S173>/Byte Pack' */
  const uint8_T data_g[20];            /* '<S174>/Byte Pack' */
  const uint8_T data_b[20];            /* '<S175>/Byte Pack' */
  const uint8_T data_fw[20];           /* '<S176>/Byte Pack' */
  const uint8_T data_ec[20];           /* '<S184>/Byte Pack' */
  const uint8_T data_og[20];           /* '<S177>/Byte Pack' */
  const uint8_T data_bu[20];           /* '<S187>/Byte Pack' */
  const uint8_T data_ci[20];           /* '<S186>/Byte Pack' */
  const uint8_T data_e0[20];           /* '<S178>/Byte Pack' */
} ConstB_drum_bench_T;

/* Constant parameters (auto storage) */
typedef struct {
  /* Pooled Parameter (Expression: [-0.00194114294287606182 -0.00528555041544641335 -0.0101358680156635409 -0.0145944578239815097 -0.0153324858390983549 -0.00828707693888684586 0.00981514984712612143 0.039713623507287639 0.0784253944922339574 0.119323826009637535 0.153693970934683621 0.173352094665219336 0.173352094665219336 0.153693970934683621 0.119323826009637535 0.0784253944922339574 0.039713623507287639 0.00981514984712612143 -0.00828707693888684586 -0.0153324858390983549 -0.0145944578239815097 -0.0101358680156635409 -0.00528555041544641335 -0.00194114294287606182])
   * Referenced by:
   *   '<S222>/Generated Filter Block'
   *   '<S223>/Generated Filter Block'
   *   '<S224>/Generated Filter Block'
   *   '<S226>/Generated Filter Block'
   *   '<S227>/Generated Filter Block'
   *   '<S228>/Generated Filter Block'
   */
  real_T pooled4[24];

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S1>/C++ S-function1'
   *   '<S1>/C++ S-function2'
   *   '<S1>/C++ S-function3'
   *   '<S1>/Inlined C++ S-Function'
   */
  real_T pooled5[2];

  /* Pooled Parameter (Expression: .001)
   * Referenced by:
   *   '<S1>/C++ S-function1'
   *   '<S1>/C++ S-function2'
   *   '<S1>/C++ S-function3'
   *   '<S1>/Inlined C++ S-Function'
   */
  real_T pooled6;

  /* Pooled Parameter (Expression: [0.000668502224994789039 0.000362091499772520229 0.000317064471929794762 0.000130105867714079614 -0.000230240453059258503 -0.000775363853214135028 -0.00148920109552885729 -0.0023233560056704983 -0.00319797131609298162 -0.00400782840555365116 -0.00463528188264168174 -0.00496668212079509879 -0.00491180854138852357 -0.00442178370103619902 -0.0035034571744654722 -0.00222711311760035325 -0.000724340408961901636 0.000825024583022457834 0.00221544267950916701 0.00324512815615486455 0.00374768622904877655 0.00362264534192081921 0.00285954637593682651 0.0015478878303257222 -0.000129180742918630294 -0.00191553829885152431 -0.0035140688869656979 -0.00463978431412332467 -0.00506017434035816241 -0.00464384636433632075 -0.00339041731511926419 -0.0014411742929599106 0.000930086862716282743 0.00335393844975403858 0.00541744750617245262 0.00673237525317844053 0.00700192416097280765 0.00608172062125102077 0.00401739318978470115 0.00105568065976890555 -0.00237917285363765593 -0.00573984124424709637 -0.00843768406245797367 -0.00993791709879107005 -0.0098568186418434324 -0.00804066226953629136 -0.00461627123709155551 1.40009008710257536e-06 0.00514776018907049139 0.00999372466900650715 0.0136638746220828465 0.0153763386595399676 0.0145825377484405938 0.0110828973483073703 0.00510212712088251811 -0.00269324907841400271 -0.0112435222785368522 -0.0192044076095747446 -0.0251110236571906766 -0.0275696377788395078 -0.0254593731554578415 -0.0181081886975269467 -0.00542428932776899651 0.0120451109768918534 0.0331334689937873919 0.0561584102240170469 0.0790958900177133467 0.0998059079198484606 0.116280007713493269 0.126880871634365622 0.130538372963648536 0.126880871634365622 0.116280007713493269 0.0998059079198484606 0.0790958900177133467 0.0561584102240170469 0.0331334689937873919 0.0120451109768918534 -0.00542428932776899651 -0.0181081886975269467 -0.0254593731554578415 -0.0275696377788395078 -0.0251110236571906766 -0.0192044076095747446 -0.0112435222785368522 -0.00269324907841400271 0.00510212712088251811 0.0110828973483073703 0.0145825377484405938 0.0153763386595399676 0.0136638746220828465 0.00999372466900650715 0.00514776018907049139 1.40009008710257536e-06 -0.00461627123709155551 -0.00804066226953629136 -0.0098568186418434324 -0.00993791709879107005 -0.00843768406245797367 -0.00573984124424709637 -0.00237917285363765593 0.00105568065976890555 0.00401739318978470115 0.00608172062125102077 0.00700192416097280765 0.00673237525317844053 0.00541744750617245262 0.00335393844975403858 0.000930086862716282743 -0.0014411742929599106 -0.00339041731511926419 -0.00464384636433632075 -0.00506017434035816241 -0.00463978431412332467 -0.0035140688869656979 -0.00191553829885152431 -0.000129180742918630294 0.0015478878303257222 0.00285954637593682651 0.00362264534192081921 0.00374768622904877655 0.00324512815615486455 0.00221544267950916701 0.000825024583022457834 -0.000724340408961901636 -0.00222711311760035325 -0.0035034571744654722 -0.00442178370103619902 -0.00491180854138852357 -0.00496668212079509879 -0.00463528188264168174 -0.00400782840555365116 -0.00319797131609298162 -0.0023233560056704983 -0.00148920109552885729 -0.000775363853214135028 -0.000230240453059258503 0.000130105867714079614 0.000317064471929794762 0.000362091499772520229 0.000668502224994789039])
   * Referenced by:
   *   '<S17>/Generated Filter Block'
   *   '<S30>/Generated Filter Block'
   */
  real_T pooled8[141];

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S15>/Window Function'
   *   '<S16>/Window Function'
   */
  real_T pooled15[128];

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S19>/FFT'
   *   '<S32>/FFT'
   */
  real_T pooled16[96];

  /* Pooled Parameter (Expression: zeros(14,1))
   * Referenced by:
   *   '<S100>/filler'
   *   '<S103>/filler'
   *   '<S165>/filler'
   *   '<S168>/filler'
   */
  real_T pooled29[14];

  /* Pooled Parameter (Expression: zeros(13,1))
   * Referenced by:
   *   '<S102>/filler'
   *   '<S104>/filler'
   *   '<S167>/filler'
   *   '<S169>/filler'
   */
  real_T pooled30[13];

  /* Pooled Parameter (Expression: zeros(12,1))
   * Referenced by:
   *   '<S99>/filler'
   *   '<S101>/filler'
   *   '<S105>/filler'
   *   '<S106>/filler'
   *   '<S164>/filler'
   *   '<S166>/filler'
   *   '<S170>/filler'
   *   '<S171>/filler'
   */
  real_T pooled31[12];

  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<S103>/mode'
   *   '<S107>/type_param'
   *   '<S109>/type_param'
   *   '<S110>/type_param'
   *   '<S112>/type_param'
   *   '<S122>/type_param'
   *   '<S168>/mode'
   *   '<S172>/type_param'
   *   '<S174>/type_param'
   *   '<S175>/type_param'
   *   '<S177>/type_param'
   *   '<S187>/type_param'
   *   '<S96>/Output'
   *   '<S127>/Constant1'
   *   '<S161>/Output'
   *   '<S192>/Constant1'
   *   '<S98>/Constant'
   *   '<S163>/Constant'
   */
  uint32_T pooled43;

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S127>/Constant'
   *   '<S192>/Constant'
   */
  uint32_T pooled52[2];

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S114>/type_param1'
   *   '<S115>/type_param1'
   *   '<S116>/type_param1'
   *   '<S179>/type_param1'
   *   '<S180>/type_param1'
   *   '<S181>/type_param1'
   */
  uint32_T pooled53[3];

  /* Pooled Parameter (Expression: uint8(zeros(20,1)))
   * Referenced by:
   *   '<S124>/Out1'
   *   '<S126>/Out1'
   *   '<S189>/Out1'
   *   '<S191>/Out1'
   */
  uint8_T pooled54[20];

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S120>/Constant'
   *   '<S185>/Constant'
   */
  uint8_T pooled55[20];
} ConstP_drum_bench_T;

/* Backward compatible GRT Identifiers */
#define rtB                            drum_bench_B
#define BlockIO                        B_drum_bench_T
#define rtP                            drum_bench_P
#define Parameters                     P_drum_bench_T
#define rtDWork                        drum_bench_DW
#define D_Work                         DW_drum_bench_T
#define tConstBlockIOType              ConstB_drum_bench_T
#define rtC                            drum_bench_ConstB
#define ConstParam                     ConstP_drum_bench_T
#define rtcP                           drum_bench_ConstP
#define rtPrevZCSigState               drum_bench_PrevZCX
#define PrevZCSigStates                PrevZCX_drum_bench_T

/* Parameters (auto storage) */
struct P_drum_bench_T_ {
  struct_Kzi606PJBfEKmP4ngorXDD config;/* Variable: config
                                        * Referenced by:
                                        *   '<Root>/mode select'
                                        *   '<Root>/mode select1'
                                        *   '<S2>/Filtered Derivative'
                                        *   '<S2>/kd'
                                        *   '<S2>/kp'
                                        *   '<S3>/Filtered Derivative'
                                        *   '<S3>/kd'
                                        *   '<S3>/kp'
                                        *   '<S4>/analog_diff_dot'
                                        *   '<S4>/mode'
                                        *   '<S4>/motor_id'
                                        *   '<S4>/motor_torque'
                                        *   '<S4>/motor_vd'
                                        *   '<S4>/quadrature_1_angle_dot'
                                        *   '<S4>/quadrature_2_angle_dot'
                                        *   '<S4>/rpc_command'
                                        *   '<S5>/analog_diff_dot'
                                        *   '<S5>/mode'
                                        *   '<S5>/motor_id'
                                        *   '<S5>/motor_torque'
                                        *   '<S5>/motor_vd'
                                        *   '<S5>/quadrature_1_angle_dot'
                                        *   '<S5>/quadrature_2_angle_dot'
                                        *   '<S5>/rpc_command'
                                        *   '<S12>/Filtered Derivative'
                                        *   '<S12>/bias'
                                        *   '<S12>/ka'
                                        *   '<S12>/kd'
                                        *   '<S13>/Filtered Derivative'
                                        *   '<S13>/bias'
                                        *   '<S13>/ka'
                                        *   '<S13>/kd'
                                        *   '<S14>/Constant'
                                        *   '<S45>/Constant'
                                        *   '<S45>/select input'
                                        *   '<S45>/Gain'
                                        *   '<S45>/Sine Wave'
                                        *   '<S46>/Constant'
                                        *   '<S46>/select input'
                                        *   '<S46>/Gain'
                                        *   '<S46>/Sine Wave'
                                        *   '<S47>/Constant'
                                        *   '<S47>/select input'
                                        *   '<S47>/Gain'
                                        *   '<S47>/Sine Wave'
                                        *   '<S48>/Constant'
                                        *   '<S48>/select input'
                                        *   '<S48>/Gain'
                                        *   '<S48>/Sine Wave'
                                        *   '<S49>/Constant'
                                        *   '<S49>/select input'
                                        *   '<S49>/Gain'
                                        *   '<S49>/Sine Wave'
                                        *   '<S55>/Constant'
                                        *   '<S55>/select input'
                                        *   '<S55>/Gain'
                                        *   '<S55>/Sine Wave'
                                        *   '<S56>/Constant'
                                        *   '<S56>/select input'
                                        *   '<S56>/Gain'
                                        *   '<S56>/Sine Wave'
                                        *   '<S57>/Constant'
                                        *   '<S57>/select input'
                                        *   '<S57>/Gain'
                                        *   '<S57>/Sine Wave'
                                        *   '<S58>/Constant'
                                        *   '<S58>/select input'
                                        *   '<S58>/Gain'
                                        *   '<S58>/Sine Wave'
                                        *   '<S59>/Constant'
                                        *   '<S59>/select input'
                                        *   '<S59>/Gain'
                                        *   '<S59>/Sine Wave'
                                        *   '<S92>/lock_current'
                                        *   '<S93>/eA1'
                                        *   '<S93>/eA2'
                                        *   '<S93>/resistance'
                                        *   '<S157>/lock_current'
                                        *   '<S158>/eA1'
                                        *   '<S158>/eA2'
                                        *   '<S158>/resistance'
                                        *   '<S50>/deltaFreq'
                                        *   '<S51>/deltaFreq'
                                        *   '<S52>/deltaFreq'
                                        *   '<S53>/deltaFreq'
                                        *   '<S54>/deltaFreq'
                                        *   '<S60>/deltaFreq'
                                        *   '<S61>/deltaFreq'
                                        *   '<S62>/deltaFreq'
                                        *   '<S63>/deltaFreq'
                                        *   '<S64>/deltaFreq'
                                        *   '<S95>/marlin_ec'
                                        *   '<S114>/value1'
                                        *   '<S115>/value1'
                                        *   '<S116>/value1'
                                        *   '<S160>/marlin_ec'
                                        *   '<S179>/value1'
                                        *   '<S180>/value1'
                                        *   '<S181>/value1'
                                        *   '<S130>/id'
                                        *   '<S130>/id1'
                                        *   '<S130>/id2'
                                        *   '<S130>/id3'
                                        *   '<S130>/id4'
                                        *   '<S130>/id5'
                                        *   '<S134>/id'
                                        *   '<S134>/id1'
                                        *   '<S135>/id'
                                        *   '<S136>/id'
                                        *   '<S136>/id1'
                                        *   '<S136>/id2'
                                        *   '<S136>/id3'
                                        *   '<S136>/id4'
                                        *   '<S139>/id'
                                        *   '<S139>/id1'
                                        *   '<S139>/id2'
                                        *   '<S139>/id3'
                                        *   '<S139>/id4'
                                        *   '<S139>/id5'
                                        *   '<S139>/id6'
                                        *   '<S139>/id7'
                                        *   '<S140>/id'
                                        *   '<S141>/id'
                                        *   '<S195>/id'
                                        *   '<S195>/id1'
                                        *   '<S195>/id2'
                                        *   '<S195>/id3'
                                        *   '<S195>/id4'
                                        *   '<S195>/id5'
                                        *   '<S199>/id'
                                        *   '<S199>/id1'
                                        *   '<S200>/id'
                                        *   '<S201>/id'
                                        *   '<S201>/id1'
                                        *   '<S201>/id2'
                                        *   '<S201>/id3'
                                        *   '<S201>/id4'
                                        *   '<S204>/id'
                                        *   '<S204>/id1'
                                        *   '<S204>/id2'
                                        *   '<S204>/id3'
                                        *   '<S204>/id4'
                                        *   '<S204>/id5'
                                        *   '<S204>/id6'
                                        *   '<S204>/id7'
                                        *   '<S205>/id'
                                        *   '<S206>/id'
                                        *   '<S143>/id'
                                        *   '<S144>/id'
                                        *   '<S145>/id'
                                        *   '<S145>/id1'
                                        *   '<S145>/id3'
                                        *   '<S145>/id4'
                                        *   '<S145>/id5'
                                        *   '<S145>/id6'
                                        *   '<S146>/id'
                                        *   '<S146>/id1'
                                        *   '<S146>/id3'
                                        *   '<S146>/id4'
                                        *   '<S146>/id5'
                                        *   '<S146>/id6'
                                        *   '<S147>/id'
                                        *   '<S147>/id1'
                                        *   '<S147>/id3'
                                        *   '<S147>/id4'
                                        *   '<S147>/id5'
                                        *   '<S147>/id6'
                                        *   '<S148>/id'
                                        *   '<S148>/id1'
                                        *   '<S148>/id2'
                                        *   '<S148>/id3'
                                        *   '<S149>/id'
                                        *   '<S149>/id1'
                                        *   '<S149>/id2'
                                        *   '<S149>/id3'
                                        *   '<S150>/id'
                                        *   '<S151>/id'
                                        *   '<S152>/id'
                                        *   '<S152>/id1'
                                        *   '<S152>/id2'
                                        *   '<S152>/id3'
                                        *   '<S153>/id'
                                        *   '<S153>/id1'
                                        *   '<S153>/id3'
                                        *   '<S153>/id4'
                                        *   '<S153>/id5'
                                        *   '<S153>/id6'
                                        *   '<S154>/id'
                                        *   '<S154>/id1'
                                        *   '<S154>/id3'
                                        *   '<S154>/id4'
                                        *   '<S154>/id5'
                                        *   '<S154>/id6'
                                        *   '<S155>/id'
                                        *   '<S155>/id1'
                                        *   '<S155>/id3'
                                        *   '<S155>/id4'
                                        *   '<S155>/id5'
                                        *   '<S155>/id6'
                                        *   '<S208>/id'
                                        *   '<S209>/id'
                                        *   '<S210>/id'
                                        *   '<S210>/id1'
                                        *   '<S210>/id3'
                                        *   '<S210>/id4'
                                        *   '<S210>/id5'
                                        *   '<S210>/id6'
                                        *   '<S211>/id'
                                        *   '<S211>/id1'
                                        *   '<S211>/id3'
                                        *   '<S211>/id4'
                                        *   '<S211>/id5'
                                        *   '<S211>/id6'
                                        *   '<S212>/id'
                                        *   '<S212>/id1'
                                        *   '<S212>/id3'
                                        *   '<S212>/id4'
                                        *   '<S212>/id5'
                                        *   '<S212>/id6'
                                        *   '<S213>/id'
                                        *   '<S213>/id1'
                                        *   '<S213>/id2'
                                        *   '<S213>/id3'
                                        *   '<S214>/id'
                                        *   '<S214>/id1'
                                        *   '<S214>/id2'
                                        *   '<S214>/id3'
                                        *   '<S215>/id'
                                        *   '<S216>/id'
                                        *   '<S217>/id'
                                        *   '<S217>/id1'
                                        *   '<S217>/id2'
                                        *   '<S217>/id3'
                                        *   '<S218>/id'
                                        *   '<S218>/id1'
                                        *   '<S218>/id3'
                                        *   '<S218>/id4'
                                        *   '<S218>/id5'
                                        *   '<S218>/id6'
                                        *   '<S219>/id'
                                        *   '<S219>/id1'
                                        *   '<S219>/id3'
                                        *   '<S219>/id4'
                                        *   '<S219>/id5'
                                        *   '<S219>/id6'
                                        *   '<S220>/id'
                                        *   '<S220>/id1'
                                        *   '<S220>/id3'
                                        *   '<S220>/id4'
                                        *   '<S220>/id5'
                                        *   '<S220>/id6'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_drum_bench_T {
  const char_T *path;
  const char_T *modelName;
  struct SimStruct_tag * *childSfunctions;
  const char_T *errorStatus;
  SS_SimMode simMode;
  RTWExtModeInfo *extModeInfo;
  RTWSolverInfo solverInfo;
  RTWSolverInfo *solverInfoPtr;
  void *sfcnInfo;

  /*
   * NonInlinedSFcns:
   * The following substructure contains information regarding
   * non-inlined s-functions used in the model.
   */
  struct {
    RTWSfcnInfo sfcnInfo;
    time_T *taskTimePtrs[6];
    SimStruct childSFunctions[4];
    SimStruct *childSFunctionPtrs[4];
    struct _ssBlkInfo2 blkInfo2[4];
    struct _ssSFcnModelMethods2 methods2[4];
    struct _ssSFcnModelMethods3 methods3[4];
    struct _ssStatesInfo2 statesInfo2[4];
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortOutputs outputPortInfo[1];
      uint_T attribs[1];
      mxArray *params[1];
      struct _ssDWorkRecord dWork[1];
      struct _ssDWorkAuxRecord dWorkAux[1];
    } Sfcn0;

    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortOutputs outputPortInfo[1];
      uint_T attribs[1];
      mxArray *params[1];
      struct _ssDWorkRecord dWork[1];
      struct _ssDWorkAuxRecord dWorkAux[1];
    } Sfcn1;

    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortInputs inputPortInfo[8];
      real_T const *UPtrs0[1];
      real_T const *UPtrs1[1];
      real_T const *UPtrs2[1];
      real_T const *UPtrs3[1];
      real_T const *UPtrs4[1];
      real_T const *UPtrs5[1];
      real_T const *UPtrs6[1];
      real_T const *UPtrs7[1];
      struct _ssPortOutputs outputPortInfo[1];
      uint_T attribs[1];
      mxArray *params[1];
      struct _ssDWorkRecord dWork[1];
      struct _ssDWorkAuxRecord dWorkAux[1];
    } Sfcn2;

    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortInputs inputPortInfo[9];
      real_T const *UPtrs0[1];
      real_T const *UPtrs1[1];
      real_T const *UPtrs2[1];
      real_T const *UPtrs3[1];
      real_T const *UPtrs4[1];
      real_T const *UPtrs5[1];
      real_T const *UPtrs6[1];
      real_T const *UPtrs7[1];
      real_T const *UPtrs8[1];
      struct _ssPortOutputs outputPortInfo[6];
      uint_T attribs[1];
      mxArray *params[1];
      struct _ssDWorkRecord dWork[1];
      struct _ssDWorkAuxRecord dWorkAux[1];
    } Sfcn3;
  } NonInlinedSFcns;

  /*
   * DataMapInfo:
   * The following substructure contains information regarding
   * structures generated in the model's C API.
   */
  struct {
    rtwCAPI_ModelMappingInfo mmi;
  } DataMapInfo;

  /*
   * ModelData:
   * The following substructure contains information regarding
   * the data used in the model.
   */
  struct {
    void *blockIO;
    const void *constBlockIO;
    void *defaultParam;
    ZCSigState *prevZCSigState;
    real_T *contStates;
    real_T *derivs;
    void *zcSignalValues;
    void *inputs;
    void *outputs;
    boolean_T *contStateDisabled;
    boolean_T zCCacheNeedsReset;
    boolean_T derivCacheNeedsReset;
    boolean_T blkStateChange;
    void *dwork;
  } ModelData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
    uint32_T options;
    int_T numContStates;
    int_T numU;
    int_T numY;
    int_T numSampTimes;
    int_T numBlocks;
    int_T numBlockIO;
    int_T numBlockPrms;
    int_T numDwork;
    int_T numSFcnPrms;
    int_T numSFcns;
    int_T numIports;
    int_T numOports;
    int_T numNonSampZCs;
    int_T sysDirFeedThru;
    int_T rtwGenSfcn;
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
    void *xpcData;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T stepSize;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    time_T stepSize1;
    uint32_T clockTick2;
    uint32_T clockTickH2;
    time_T stepSize2;
    uint32_T clockTick3;
    uint32_T clockTickH3;
    time_T stepSize3;
    uint32_T clockTick4;
    uint32_T clockTickH4;
    time_T stepSize4;
    uint32_T clockTick5;
    uint32_T clockTickH5;
    time_T stepSize5;
    struct {
      uint16_T TID[6];
    } TaskCounters;

    time_T tStart;
    time_T tFinal;
    time_T timeOfLastOutput;
    void *timingData;
    real_T *varNextHitTimesList;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *sampleTimes;
    time_T *offsetTimes;
    int_T *sampleTimeTaskIDPtr;
    int_T *sampleHits;
    int_T *perTaskSampleHits;
    time_T *t;
    time_T sampleTimesArray[6];
    time_T offsetTimesArray[6];
    int_T sampleTimeTaskIDArray[6];
    int_T sampleHitArray[6];
    int_T perTaskSampleHitsArray[36];
    time_T tArray[6];
  } Timing;
};

/* Block parameters (auto storage) */
extern P_drum_bench_T drum_bench_P;

/* Block signals (auto storage) */
extern B_drum_bench_T drum_bench_B;

/* Block states (auto storage) */
extern DW_drum_bench_T drum_bench_DW;
extern const ConstB_drum_bench_T drum_bench_ConstB;/* constant block i/o */

/* Constant parameters (auto storage) */
extern const ConstP_drum_bench_T drum_bench_ConstP;

/* External data declarations for dependent source files */

/* Zero-crossing (trigger) state */
extern PrevZCX_drum_bench_T drum_bench_PrevZCX;

/* Function to get C API Model Mapping Static Info */
extern const rtwCAPI_ModelMappingStaticInfo*
  drum_bench_GetCAPIStaticMap(void);

/* Real-time Model object */
extern RT_MODEL_drum_bench_T *const drum_bench_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'drum_bench'
 * '<S1>'   : 'drum_bench/Behavior Control'
 * '<S2>'   : 'drum_bench/impedence_controller'
 * '<S3>'   : 'drum_bench/impedence_controller1'
 * '<S4>'   : 'drum_bench/marlin 1.1 full command generator'
 * '<S5>'   : 'drum_bench/marlin 1.1 full command generator1'
 * '<S6>'   : 'drum_bench/marlin_1.1_full status'
 * '<S7>'   : 'drum_bench/marlin_1.1_full status1'
 * '<S8>'   : 'drum_bench/marlin_1.1_full1'
 * '<S9>'   : 'drum_bench/marlin_1.1_full2'
 * '<S10>'  : 'drum_bench/mason process'
 * '<S11>'  : 'drum_bench/mason process1'
 * '<S12>'  : 'drum_bench/var_impedence_controller'
 * '<S13>'  : 'drum_bench/var_impedence_controller1'
 * '<S14>'  : 'drum_bench/write_yaml_params'
 * '<S15>'  : 'drum_bench/Behavior Control/EMG Filter'
 * '<S16>'  : 'drum_bench/Behavior Control/EMG Filter1'
 * '<S17>'  : 'drum_bench/Behavior Control/EMG Filter/Lowpass Filter'
 * '<S18>'  : 'drum_bench/Behavior Control/EMG Filter/MATLAB Function'
 * '<S19>'  : 'drum_bench/Behavior Control/EMG Filter/Magnitude FFT'
 * '<S20>'  : 'drum_bench/Behavior Control/EMG Filter/Peak-Notch Filter'
 * '<S21>'  : 'drum_bench/Behavior Control/EMG Filter/mean'
 * '<S22>'  : 'drum_bench/Behavior Control/EMG Filter/mean1'
 * '<S23>'  : 'drum_bench/Behavior Control/EMG Filter/mean2'
 * '<S24>'  : 'drum_bench/Behavior Control/EMG Filter/slopeChange'
 * '<S25>'  : 'drum_bench/Behavior Control/EMG Filter/smooth'
 * '<S26>'  : 'drum_bench/Behavior Control/EMG Filter/smooth1'
 * '<S27>'  : 'drum_bench/Behavior Control/EMG Filter/smooth2'
 * '<S28>'  : 'drum_bench/Behavior Control/EMG Filter/smooth3'
 * '<S29>'  : 'drum_bench/Behavior Control/EMG Filter/zeroCrossing'
 * '<S30>'  : 'drum_bench/Behavior Control/EMG Filter1/Lowpass Filter'
 * '<S31>'  : 'drum_bench/Behavior Control/EMG Filter1/MATLAB Function'
 * '<S32>'  : 'drum_bench/Behavior Control/EMG Filter1/Magnitude FFT'
 * '<S33>'  : 'drum_bench/Behavior Control/EMG Filter1/Peak-Notch Filter'
 * '<S34>'  : 'drum_bench/Behavior Control/EMG Filter1/mean'
 * '<S35>'  : 'drum_bench/Behavior Control/EMG Filter1/mean1'
 * '<S36>'  : 'drum_bench/Behavior Control/EMG Filter1/mean2'
 * '<S37>'  : 'drum_bench/Behavior Control/EMG Filter1/slopeChange'
 * '<S38>'  : 'drum_bench/Behavior Control/EMG Filter1/smooth'
 * '<S39>'  : 'drum_bench/Behavior Control/EMG Filter1/smooth1'
 * '<S40>'  : 'drum_bench/Behavior Control/EMG Filter1/smooth2'
 * '<S41>'  : 'drum_bench/Behavior Control/EMG Filter1/smooth3'
 * '<S42>'  : 'drum_bench/Behavior Control/EMG Filter1/zeroCrossing'
 * '<S43>'  : 'drum_bench/impedence_controller/Filtered Derivative'
 * '<S44>'  : 'drum_bench/impedence_controller1/Filtered Derivative'
 * '<S45>'  : 'drum_bench/marlin 1.1 full command generator/analog_diff'
 * '<S46>'  : 'drum_bench/marlin 1.1 full command generator/motor_iq'
 * '<S47>'  : 'drum_bench/marlin 1.1 full command generator/motor_vq'
 * '<S48>'  : 'drum_bench/marlin 1.1 full command generator/quadrature_1_angle'
 * '<S49>'  : 'drum_bench/marlin 1.1 full command generator/quadrature_2_angle'
 * '<S50>'  : 'drum_bench/marlin 1.1 full command generator/analog_diff/Chirp Signal'
 * '<S51>'  : 'drum_bench/marlin 1.1 full command generator/motor_iq/Chirp Signal'
 * '<S52>'  : 'drum_bench/marlin 1.1 full command generator/motor_vq/Chirp Signal'
 * '<S53>'  : 'drum_bench/marlin 1.1 full command generator/quadrature_1_angle/Chirp Signal'
 * '<S54>'  : 'drum_bench/marlin 1.1 full command generator/quadrature_2_angle/Chirp Signal'
 * '<S55>'  : 'drum_bench/marlin 1.1 full command generator1/analog_diff'
 * '<S56>'  : 'drum_bench/marlin 1.1 full command generator1/motor_iq'
 * '<S57>'  : 'drum_bench/marlin 1.1 full command generator1/motor_vq'
 * '<S58>'  : 'drum_bench/marlin 1.1 full command generator1/quadrature_1_angle'
 * '<S59>'  : 'drum_bench/marlin 1.1 full command generator1/quadrature_2_angle'
 * '<S60>'  : 'drum_bench/marlin 1.1 full command generator1/analog_diff/Chirp Signal'
 * '<S61>'  : 'drum_bench/marlin 1.1 full command generator1/motor_iq/Chirp Signal'
 * '<S62>'  : 'drum_bench/marlin 1.1 full command generator1/motor_vq/Chirp Signal'
 * '<S63>'  : 'drum_bench/marlin 1.1 full command generator1/quadrature_1_angle/Chirp Signal'
 * '<S64>'  : 'drum_bench/marlin 1.1 full command generator1/quadrature_2_angle/Chirp Signal'
 * '<S65>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status'
 * '<S66>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults'
 * '<S67>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits'
 * '<S68>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1'
 * '<S69>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10'
 * '<S70>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2'
 * '<S71>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3'
 * '<S72>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4'
 * '<S73>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5'
 * '<S74>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6'
 * '<S75>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7'
 * '<S76>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8'
 * '<S77>'  : 'drum_bench/marlin_1.1_full status/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9'
 * '<S78>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status'
 * '<S79>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults'
 * '<S80>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits'
 * '<S81>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits1'
 * '<S82>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits10'
 * '<S83>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits2'
 * '<S84>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits3'
 * '<S85>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits4'
 * '<S86>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits5'
 * '<S87>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits6'
 * '<S88>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits7'
 * '<S89>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits8'
 * '<S90>'  : 'drum_bench/marlin_1.1_full status1/marlin_1.1_basic status/extract marlin basic faults/_Extract Bits9'
 * '<S91>'  : 'drum_bench/marlin_1.1_full1/marlin_1.1_basic'
 * '<S92>'  : 'drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector'
 * '<S93>'  : 'drum_bench/marlin_1.1_full1/motor_temperature_model'
 * '<S94>'  : 'drum_bench/marlin_1.1_full1/rpc_generator'
 * '<S95>'  : 'drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec'
 * '<S96>'  : 'drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running'
 * '<S97>'  : 'drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running/Increment Real World'
 * '<S98>'  : 'drum_bench/marlin_1.1_full1/marlin_1.1_basic/marlin ec/Counter Free-Running/Wrap To Zero'
 * '<S99>'  : 'drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/analog_diff'
 * '<S100>' : 'drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/brake'
 * '<S101>' : 'drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/current'
 * '<S102>' : 'drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/motor_torque'
 * '<S103>' : 'drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/open'
 * '<S104>' : 'drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/phase lock'
 * '<S105>' : 'drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/quadrature_1'
 * '<S106>' : 'drum_bench/marlin_1.1_full1/marlin_full_control_mode_selector/voltage'
 * '<S107>' : 'drum_bench/marlin_1.1_full1/rpc_generator/10 khz logger read'
 * '<S108>' : 'drum_bench/marlin_1.1_full1/rpc_generator/10 khz logger start'
 * '<S109>' : 'drum_bench/marlin_1.1_full1/rpc_generator/100 khz logger read'
 * '<S110>' : 'drum_bench/marlin_1.1_full1/rpc_generator/100 khz logger read2'
 * '<S111>' : 'drum_bench/marlin_1.1_full1/rpc_generator/100 khz logger start'
 * '<S112>' : 'drum_bench/marlin_1.1_full1/rpc_generator/check safety'
 * '<S113>' : 'drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell'
 * '<S114>' : 'drum_bench/marlin_1.1_full1/rpc_generator/get status debug'
 * '<S115>' : 'drum_bench/marlin_1.1_full1/rpc_generator/get status debug1'
 * '<S116>' : 'drum_bench/marlin_1.1_full1/rpc_generator/get status debug2'
 * '<S117>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1'
 * '<S118>' : 'drum_bench/marlin_1.1_full1/rpc_generator/select rpc'
 * '<S119>' : 'drum_bench/marlin_1.1_full1/rpc_generator/store param'
 * '<S120>' : 'drum_bench/marlin_1.1_full1/rpc_generator/testrpc'
 * '<S121>' : 'drum_bench/marlin_1.1_full1/rpc_generator/zero both encoder angles'
 * '<S122>' : 'drum_bench/marlin_1.1_full1/rpc_generator/zero output torque'
 * '<S123>' : 'drum_bench/marlin_1.1_full1/rpc_generator/check safety/Compare To Constant'
 * '<S124>' : 'drum_bench/marlin_1.1_full1/rpc_generator/check safety/enabled subsystem'
 * '<S125>' : 'drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/Compare To Constant'
 * '<S126>' : 'drum_bench/marlin_1.1_full1/rpc_generator/get raw load cell/Enabled Subsystem'
 * '<S127>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/dsp param iterator'
 * '<S128>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1'
 * '<S129>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/dsp param iterator/MATLAB Function1'
 * '<S130>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp'
 * '<S131>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1'
 * '<S132>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2'
 * '<S133>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff'
 * '<S134>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/bridge'
 * '<S135>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog'
 * '<S136>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_current'
 * '<S137>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle'
 * '<S138>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle'
 * '<S139>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/motor'
 * '<S140>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1'
 * '<S141>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2'
 * '<S142>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi'
 * '<S143>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz'
 * '<S144>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz'
 * '<S145>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_1/sensor'
 * '<S146>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_2/sensor'
 * '<S147>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor'
 * '<S148>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_analog/pid'
 * '<S149>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid'
 * '<S150>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband'
 * '<S151>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate'
 * '<S152>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid'
 * '<S153>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor'
 * '<S154>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor'
 * '<S155>' : 'drum_bench/marlin_1.1_full1/rpc_generator/param1/param_marlin_1.1/ssi/sensor'
 * '<S156>' : 'drum_bench/marlin_1.1_full2/marlin_1.1_basic'
 * '<S157>' : 'drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector'
 * '<S158>' : 'drum_bench/marlin_1.1_full2/motor_temperature_model'
 * '<S159>' : 'drum_bench/marlin_1.1_full2/rpc_generator'
 * '<S160>' : 'drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec'
 * '<S161>' : 'drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running'
 * '<S162>' : 'drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running/Increment Real World'
 * '<S163>' : 'drum_bench/marlin_1.1_full2/marlin_1.1_basic/marlin ec/Counter Free-Running/Wrap To Zero'
 * '<S164>' : 'drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/analog_diff'
 * '<S165>' : 'drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/brake'
 * '<S166>' : 'drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/current'
 * '<S167>' : 'drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/motor_torque'
 * '<S168>' : 'drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/open'
 * '<S169>' : 'drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/phase lock'
 * '<S170>' : 'drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/quadrature_1'
 * '<S171>' : 'drum_bench/marlin_1.1_full2/marlin_full_control_mode_selector/voltage'
 * '<S172>' : 'drum_bench/marlin_1.1_full2/rpc_generator/10 khz logger read'
 * '<S173>' : 'drum_bench/marlin_1.1_full2/rpc_generator/10 khz logger start'
 * '<S174>' : 'drum_bench/marlin_1.1_full2/rpc_generator/100 khz logger read'
 * '<S175>' : 'drum_bench/marlin_1.1_full2/rpc_generator/100 khz logger read2'
 * '<S176>' : 'drum_bench/marlin_1.1_full2/rpc_generator/100 khz logger start'
 * '<S177>' : 'drum_bench/marlin_1.1_full2/rpc_generator/check safety'
 * '<S178>' : 'drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell'
 * '<S179>' : 'drum_bench/marlin_1.1_full2/rpc_generator/get status debug'
 * '<S180>' : 'drum_bench/marlin_1.1_full2/rpc_generator/get status debug1'
 * '<S181>' : 'drum_bench/marlin_1.1_full2/rpc_generator/get status debug2'
 * '<S182>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1'
 * '<S183>' : 'drum_bench/marlin_1.1_full2/rpc_generator/select rpc'
 * '<S184>' : 'drum_bench/marlin_1.1_full2/rpc_generator/store param'
 * '<S185>' : 'drum_bench/marlin_1.1_full2/rpc_generator/testrpc'
 * '<S186>' : 'drum_bench/marlin_1.1_full2/rpc_generator/zero both encoder angles'
 * '<S187>' : 'drum_bench/marlin_1.1_full2/rpc_generator/zero output torque'
 * '<S188>' : 'drum_bench/marlin_1.1_full2/rpc_generator/check safety/Compare To Constant'
 * '<S189>' : 'drum_bench/marlin_1.1_full2/rpc_generator/check safety/enabled subsystem'
 * '<S190>' : 'drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/Compare To Constant'
 * '<S191>' : 'drum_bench/marlin_1.1_full2/rpc_generator/get raw load cell/Enabled Subsystem'
 * '<S192>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/dsp param iterator'
 * '<S193>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1'
 * '<S194>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/dsp param iterator/MATLAB Function1'
 * '<S195>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp'
 * '<S196>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1'
 * '<S197>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2'
 * '<S198>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff'
 * '<S199>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/bridge'
 * '<S200>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog'
 * '<S201>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_current'
 * '<S202>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle'
 * '<S203>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle'
 * '<S204>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/motor'
 * '<S205>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1'
 * '<S206>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2'
 * '<S207>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi'
 * '<S208>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_100kHz'
 * '<S209>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/amp/logger_10kHz'
 * '<S210>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_1/sensor'
 * '<S211>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_2/sensor'
 * '<S212>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/analog_diff/sensor'
 * '<S213>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_analog/pid'
 * '<S214>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid'
 * '<S215>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_deadband'
 * '<S216>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_motor_angle/pid_interpolate'
 * '<S217>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/control_quadrature_2_angle/pid'
 * '<S218>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_1/sensor'
 * '<S219>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/quadrature_2/sensor'
 * '<S220>' : 'drum_bench/marlin_1.1_full2/rpc_generator/param1/param_marlin_1.1/ssi/sensor'
 * '<S221>' : 'drum_bench/mason process/marlin_1.1_basic status'
 * '<S222>' : 'drum_bench/mason process/marlin_1.1_basic status/Lowpass Filter'
 * '<S223>' : 'drum_bench/mason process/marlin_1.1_basic status/Lowpass Filter1'
 * '<S224>' : 'drum_bench/mason process/marlin_1.1_basic status/Lowpass Filter2'
 * '<S225>' : 'drum_bench/mason process1/marlin_1.1_basic status'
 * '<S226>' : 'drum_bench/mason process1/marlin_1.1_basic status/Lowpass Filter'
 * '<S227>' : 'drum_bench/mason process1/marlin_1.1_basic status/Lowpass Filter1'
 * '<S228>' : 'drum_bench/mason process1/marlin_1.1_basic status/Lowpass Filter2'
 * '<S229>' : 'drum_bench/var_impedence_controller/Filtered Derivative'
 * '<S230>' : 'drum_bench/var_impedence_controller1/Filtered Derivative'
 * '<S231>' : 'drum_bench/write_yaml_params/Triggered Subsystem'
 */
#endif                                 /* RTW_HEADER_drum_bench_h_ */
