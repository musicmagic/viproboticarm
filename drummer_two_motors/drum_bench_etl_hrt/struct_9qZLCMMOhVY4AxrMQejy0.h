#ifndef RTW_HEADER_struct_9qZLCMMOhVY4AxrMQejy0_h_
#define RTW_HEADER_struct_9qZLCMMOhVY4AxrMQejy0_h_
#include "rtwtypes.h"

typedef struct {
  real_T current_filter_bandwidth;
  real_T fingerprint_gain;
  real_T ki;
  real_T ki_limit;
  real_T kp;
} struct_9qZLCMMOhVY4AxrMQejy0;

#endif                                 /* RTW_HEADER_struct_9qZLCMMOhVY4AxrMQejy0_h_ */
