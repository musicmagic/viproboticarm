#ifndef RTW_HEADER_struct_fxT5Nq3tGzQ2GEaQv1zUtC_h_
#define RTW_HEADER_struct_fxT5Nq3tGzQ2GEaQv1zUtC_h_
#include "rtwtypes.h"

typedef struct {
  real_T bias;
  real_T gain;
  real_T limit_high;
  real_T limit_low;
  real_T filter_bandwidth;
  real_T dot_filter_bandwidth;
} struct_fxT5Nq3tGzQ2GEaQv1zUtC;

#endif                                 /* RTW_HEADER_struct_fxT5Nq3tGzQ2GEaQv1zUtC_h_ */
