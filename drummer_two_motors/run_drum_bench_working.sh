if [ -z "$1" ]
  then
    sudo ./drum_bench_working -y top_level_yaml/drum_bench.yaml -w project_config/ -r ../../rsc-base/simulink/config/
else
  sudo ./drum_bench_working -y $1 -w project_config/ -r ../../config/
fi
