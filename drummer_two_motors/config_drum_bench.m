
Ts = 0.0003;
project_dir = fileparts(mfilename('fullpath'));

top_level_yaml_dir = [project_dir, '/top_level_yaml'];
top_level_yaml = 'drum_bench.yaml';
workspace_yaml_dir = [project_dir, '/project_config'];

config = config_rsc_project(project_dir, top_level_yaml_dir,...
    top_level_yaml, workspace_yaml_dir);

%cs = config_reference_config;

loaded = load('etherlab_configuration.mat');
cs = loaded.cs;

[path] = fileparts(mfilename('fullpath'));
custom_include = cell(4,1);
custom_include{1} = [path '/../../rsc-base/simulink/library/c'];
custom_include{2} = [matlabroot '/rtw/c/src'];
custom_include{3} = '/usr/lib/x86_64-linux-gnu';  % for libyaml, libpthread
custom_include{4} = '/usr/include';
cs.set_param('CustomInclude', sprintf('%s\n',custom_include{:}));

custom_source = cell(3,1);
custom_source{1} = 'capi_helper.c';
custom_source{2} = 'yaml_helper.c';
custom_source{3} = 'model_params_and_yaml.c';
cs.set_param('CustomSource', sprintf('%s\n',custom_source{:}));

custom_lib = cell(2,1);
custom_lib{1} = 'libyaml.a';
custom_lib{2} = 'libpthread.so';
cs.set_param('CustomLibrary', sprintf('%s\n',custom_lib{:}));
