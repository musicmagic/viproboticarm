/* 
    Example of two different ways to process received OSC messages using oscpack.
    Receives the messages from the SimpleSend.cpp example.
*/

#include <iostream>
#include <cstring>
#include <stdio.h> 
#include <stdlib.h>
#include <string>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdexcept>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define S_FUNCTION_LEVEL 2
#define S_FUNCTION_NAME  networking

//#define IP "127.0.0.1"//"130.207.59.47"
#define IP "10.0.1.2" // Ip for this machine
#define PORT 54327

class udp_client_server_runtime_error : public std::runtime_error
{
public:
    udp_client_server_runtime_error(const char *w) : std::runtime_error(w) {}
};

class udpReceive
{
    
public:
    int                 f_socket;
    int                 f_port;
    std::string         f_addr;
    struct addrinfo *   f_addrinfo;

    udpReceive(const std::string& addr, int port)
    : f_port(port)
    , f_addr(addr)
    {
        char decimal_port[16];
        snprintf(decimal_port, sizeof(decimal_port), "%d", f_port);
        decimal_port[sizeof(decimal_port) / sizeof(decimal_port[0]) - 1] = '\0';
        struct addrinfo hints;
        memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_DGRAM;
        hints.ai_protocol = IPPROTO_UDP;
        int r(getaddrinfo(addr.c_str(), decimal_port, &hints, &f_addrinfo));
        if(r != 0 || f_addrinfo == NULL)
        {
            throw udp_client_server_runtime_error(("invalid address or port for UDP socket: \"" + addr + ":" + decimal_port + "\"").c_str());
        }
        f_socket = socket(f_addrinfo->ai_family, SOCK_DGRAM | SOCK_CLOEXEC, IPPROTO_UDP);
        fcntl(f_socket, F_SETFL,O_NONBLOCK);
        if(f_socket == -1)
        {
            freeaddrinfo(f_addrinfo);
            throw udp_client_server_runtime_error(("could not create UDP socket for: \"" + addr + ":" + decimal_port + "\"").c_str());
        }
        r = bind(f_socket, f_addrinfo->ai_addr, f_addrinfo->ai_addrlen);
        if(r != 0)
        {
            freeaddrinfo(f_addrinfo);
            close(f_socket);
            throw udp_client_server_runtime_error(("could not bind UDP socket with: \"" + addr + ":" + decimal_port + "\"").c_str());
        }
        std::cout<<"bound successfully!!!!!!!!!!!!!!!!!!!!"<<std::endl;
    }
    
    /** \brief Clean up the UDP server.
     *
     * This function frees the address info structures and close the socket.
     */
    ~udpReceive()
    {
        freeaddrinfo(f_addrinfo);
        close(f_socket);
    }
    
    /** \brief The socket used by this UDP server.
     *
     * This function returns the socket identifier. It can be useful if you are
     * doing a select() on many sockets.
     *
     * \return The socket of this UDP server.
     */
    int get_socket() const
    {
        return f_socket;
    }
    
    /** \brief The port used by this UDP server.
     *
     * This function returns the port attached to the UDP server. It is a copy
     * of the port specified in the constructor.
     *
     * \return The port of the UDP server.
     */
    int get_port() const
    {
        return f_port;
    }
    
    /** \brief Return the address of this UDP server.
     *
     * This function returns a verbatim copy of the address as passed to the
     * constructor of the UDP server (i.e. it does not return the canonalized
     * version of the address.)
     *
     * \return The address as passed to the constructor.
     */
    std::string get_addr() const
    {
        return f_addr;
    }
    
    /** \brief Wait on a message.
     *
     * This function waits until a message is received on this UDP server.
     * There are no means to return from this function except by receiving
     * a message. Remember that UDP does not have a connect state so whether
     * another process quits does not change the status of this UDP server
     * and thus it continues to wait forever.
     *
     * Note that you may change the type of socket by making it non-blocking
     * (use the get_socket() to retrieve the socket identifier) in which
     * case this function will not block if no message is available. Instead
     * it returns immediately.
     *
     * \param[in] msg  The buffer where the message is saved.
     * \param[in] max_size  The maximum size the message (i.e. size of the \p msg buffer.)
     *
     * \return The number of bytes read or -1 if an error occurs.
     */
    int recv(char *msg, size_t max_size)
    {
        int temp = ::recv(f_socket, msg, max_size, 0);
        //std::cout << temp <<std::endl;
        
        
        if (temp != -1 ){
            //std::string getData;
            //getData.append(msg,temp);            

            //std::cout << "Received message ............ " << int(msg[temp-8]) << " - " << temp<< " - "<<getData<<std::endl;
            
            
            if(temp >= 2){ //going to be strike, onset, or some other command
                int type = int(msg[temp-5]);
                int command = int(msg[temp-1]);
                if(type==0){
                    std::cout << "Received strike for primary arm ............ " << command <<std::endl;
                    return command;
                }
                if(type==1){
                    std::cout << "Received strike for secondary arm............ " << command <<std::endl;
                    return command+100;
                }
               
                if(type==2){
                    std::cout << "Received onset amplitude ............ " << command <<std::endl;
                    return command+200;
                }
                
                if(type==3){
                    std::cout << "Received piece ............ " << command <<std::endl;
                    return command+300;
                }
                
                if(type==4){
                    std::cout << "Received section ............ " << command <<std::endl;
                    return command+400;
                }
                if(type==5){
                    std::cout << "Received kp for primary arm ............ " << command <<std::endl;
                    return command+500;
                }
                if(type==6){
                    std::cout << "Received kd for primary arm ............ " << command <<std::endl;
                    return command+600;
                }
                if(type==7){                     
                       std::cout << "Recieved kp increment/decrement command which is EMG onset " << command << std::endl; // this is from the onset
                       return command+700;
                }
                if(type==8){
                    std::cout << "Received EMG envelope information " << command << std::endl;
                    return command+800;
                }
                if(type == 9)
                {
                    std::cout << "Recieved amplitude for primary arm ...... " << command <<std::endl;
                    return command+900;
                }
                if(type == 10)
                {
                    std::cout << "Recieved amplitude for secondary arm ...... " << command <<std::endl;
                    return command+1000;
                }
                if(type == 11)
                {
                    std::cout << "Received toggle from pedal board ...... " << command << std::endl;
                    return command+1100;
                }
                if(type == 12)
                {
                    std::cout << "Received expression pedal value from pedal board .... " << command << std::endl;
                    return command + 1200;
                }
            
            }   
        }
        return -1;
    }
    
    /** \brief Wait for data to come in.
     *
     * This function waits for a given amount of time for data to come in. If
     * no data comes in after max_wait_ms, the function returns with -1 and
     * errno set to EAGAIN.
     *
     * The socket is expected to be a blocking socket (the default,) although
     * it is possible to setup the socket as non-blocking if necessary for
     * some other reason.
     *
     * This function blocks for a maximum amount of time as defined by
     * max_wait_ms. It may return sooner with an error or a message.
     *
     * \param[in] msg  The buffer where the message will be saved.
     * \param[in] max_size  The size of the \p msg buffer in bytes.
     * \param[in] max_wait_ms  The maximum number of milliseconds to wait for a message.
     *
     * \return -1 if an error occurs or the function timed out, the number of bytes received otherwise.
     */
    int timed_recv(char *msg, size_t max_size, int max_wait_ms)
    {
        
        fd_set s;
        FD_ZERO(&s);
        FD_SET(f_socket, &s);
        struct timeval timeout;
        timeout.tv_sec = 0;//max_wait_ms / 1000.0;
        timeout.tv_usec = 1000;//(max_wait_ms % 1000) * 1000.0;
        int retval = select(f_socket + 1, &s, &s, &s, &timeout);
        std::cout << "retval =  " << retval <<std::endl;
        if(retval == -1)
        {
            // select() set errno accordingly
            std::cout << "no message  " << msg[0] <<std::endl;
            return -1;
        }
        if(retval > 0)
        {
            // our socket has data
            return ::recv(f_socket, msg, max_size, 0);
        }
        
        // our socket has no data
//         errno = EAGAIN;
        std::cout << "no message  " << msg[0] <<std::endl;
        return -1;
    }
    
    
};









#include "simstruc.h"

#define IS_PARAM_DOUBLE(pVal) (mxIsNumeric(pVal) && !mxIsLogical(pVal) &&\
!mxIsEmpty(pVal) && !mxIsSparse(pVal) && !mxIsComplex(pVal) && mxIsDouble(pVal))

/*====================*
 * S-function methods *
 *====================*/

#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS)  && defined(MATLAB_MEX_FILE)
/*
 * Check to make sure that each parameter is 1-d and positive
 */
static void mdlCheckParameters(SimStruct *S)
{
    
    const mxArray *pVal0 = ssGetSFcnParam(S,0);
    
    if ( !IS_PARAM_DOUBLE(pVal0)) {
        ssSetErrorStatus(S, "Parameter to S-function must be a double scalar");
        return;
    }
}
#endif

/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
    ssSetNumSFcnParams(S, 1);  /* Number of expected parameters */
#if defined(MATLAB_MEX_FILE)
    if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
        mdlCheckParameters(S);
        if (ssGetErrorStatus(S) != NULL) {
            return;
        }
    } else {
        return; /* Parameter mismatch will be reported by Simulink */
    }
#endif
    ssSetSFcnParamTunable(S, 0, 0);
    
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);
    
    if (!ssSetNumInputPorts(S, 0)) return;
    
    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, 1);
    
    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 1); // reserve element in the pointers vector
    ssSetNumModes(S, 0); // to store a C++ object
    ssSetNumNonsampledZCs(S, 0);
    
    ssSetSimStateCompliance(S, USE_CUSTOM_SIM_STATE);
    
    ssSetOptions(S, 0);
}


/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    This function is used to specify the sample time(s) for your
 *    S-function. You must register the same number of sample times as
 *    specified in ssSetNumSampleTimes.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, mxGetScalar(ssGetSFcnParam(S, 0)));
    ssSetOffsetTime(S, 0, 0.0);
    ssSetModelReferenceSampleTimeDefaultInheritance(S);
    
}


#define MDL_START  /* Change to #undef to remove function */
#if defined(MDL_START)
/* Function: mdlStart =======================================================
 * Abstract:
 *    This function is called once at start of model execution. If you
 *    have states that should be initialized once, this is the place
 *    to do it.
 */
static void mdlStart(SimStruct *S)
{
    ssGetPWork(S)[0] = (void *) new udpReceive(IP, PORT); // store new C++ object in the pointer vector
}                                           
#endif /*  MDL_START */


/* Function: mdlOutputs =======================================================
 * Abstract:
 *    In this function, you compute the outputs of your S-function
 *    block.
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    real_T  *y = ssGetOutputPortRealSignal(S,0); 
    int max_size = 1024;
    char* msg = new char[max_size];
    memset(msg, 0, max_size*sizeof(char));
    udpReceive *udpr = (udpReceive *) ssGetPWork(S)[0];
    
    //std::cout << "Received message ............ " << msg[0] <<std::endl;
    int message = udpr->recv(&msg[0], max_size*sizeof(char));
    y[0] = message;
    delete msg;
    //udpr->timed_recv(msg, max_size*sizeof(char),1);
}


/* Function: mdlTerminate =====================================================
 * Abstract:
 *    In this function, you should perform any actions that are necessary
 *    at the termination of a simulation.  For example, if memory was
 *    allocated in mdlStart, this is the place to free it.
 */
static void mdlTerminate(SimStruct *S)
{
    udpReceive *udpr = (udpReceive *) ssGetPWork(S)[0]; // retrieve and destroy C++
    delete udpr; 
}
/*======================================================*
 * See sfuntmpl.doc for the optional S-function methods *
 *======================================================*/

/*=============================*
 * Required S-function trailer *
 *=============================*/

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif



