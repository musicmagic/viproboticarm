package machineMusicianship;

import java.util.ArrayList;
import java.util.List;

import com.cycling74.max.Atom;
import com.cycling74.max.DataTypes;
import com.cycling74.max.MaxObject;

public class NoteDensity extends MaxObject{
	
	private volatile List<Long> in_onsets = new ArrayList<Long>();

	float note_density = 0;
	
	public NoteDensity(Atom[] args)
	{
		declareInlets(new int[]{DataTypes.ALL, DataTypes.ALL});
		declareOutlets(new int[]{DataTypes.ALL});
	}
	
	public void bang()
	{
		int inlet = getInlet();
		if ( inlet == 0 )
		{
			in_onsets.add(System.currentTimeMillis());
		}
		
		if ( inlet == 1 )
		{
			calcND();
			
			if (note_density >= 0 && note_density < 1)
			{
				outlet(0, 1);
			}
			if (note_density >= 1 && note_density < 2)
			{
				outlet(0, 2);
			}
			if (note_density >= 2)
			{
				outlet(0, 3);
			}
			
			in_onsets.clear();
			note_density = 0;
		}
	}
	
	public void inlet(long in)
	{
	
	}
	
	// Calculate note density
	public void calcND()
	{
		note_density = in_onsets.size()*1000/(in_onsets.get(in_onsets.size() - 1) - in_onsets.get(0));
		
	}
}
