outlets = 4;

var num = 30;
var num_impro = 30;
var impro_seq = new Array();
impro_seq.push(1000);
var pit = new Array();
var timeseq = new Array();
var d = new Date();
var pretime = d.getTime();
var curIdx = 1;
var tempo = 0;
var oldtempo = 0;

function isthesame(p){
	if ( p[0] == p[1] && p[1] == p[2] && p[2] == p[3] && p[3] == p[4] && p[4] == p[5] &&p[5] == p[6] && p[6] == p[7])
		return 1;
	return 0;
}

function avg(t){
	var ts = sum(t) - t[0];
	return ts/(t.length-1);
}

function clear(){
	curIdx = 1;
	pit = [];
	timeseq = [];
	impro_seq = [];
	outlet(0, "");
	outlet(1, "");
	outlet(2, 0);
	outlet(3, 1000);
}


function onset(p){
	var dd = new Date();
	var curtime = dd.getTime();
	var timeinter = curtime - pretime;
	var len = pit.length;
	if (len < num){
		pit.push(p);
		timeseq.push(timeinter);
	}
	else{
		pit.shift();
		pit.push(p);
		timeseq.shift();
		timeseq.push(timeinter);
	}
	
	len = pit.length;
	outlet(0, pit);
	outlet(1, timeseq);
	pretime = curtime;
	
	gettempo();
	if (Math.abs(avg(timeseq) - oldtempo) > 10){
		oldtempo = avg(timeseq);
		trick3();
	}
}

function trick1(){
	if (timeseq.length > 0){
		var note = timeseq[curIdx];
		outlet(3, note);
		curIdx = curIdx+1;
		if (curIdx >= timeseq.length)
			curIdx = 0;
	}
	else
		outlet(3, 1000);
}

function trick2(){
	if (timeseq.length > 0){
		var note = timeseq[curIdx]/2;
		outlet(3, note);
		curIdx = curIdx+1;
		if (curIdx >= timeseq.length)
			curIdx = 0;
	}
	else
		outlet(3, 1000);
}

function trick3(){
	tempo = avg(timeseq);
	outlet(2, tempo);
	impro_seq = [];
	improvise3(tempo);
	outlet(3, impro_seq[0]);
}

function improvise3(tempo){
	for (var i=0; i<3; i++){
		var imp = Math.random();
		if (imp>0.7){
			if (impro_seq.length > num_impro)
				impro_seq.shift();
			impro_seq.push(tempo);
		}
		else if (imp>0.3 && imp<=0.7){
			if (impro_seq.length > num_impro)
				impro_seq.shift();
			impro_seq.push(tempo/2);
			if (impro_seq.length > num_impro)
				impro_seq.shift();
			impro_seq.push(tempo/2);
		}
		else{
			if (impro_seq.length > num_impro)
				impro_seq.shift();
			impro_seq.push(tempo/4);
			if (impro_seq.length > num_impro)
				impro_seq.shift();
			impro_seq.push(tempo/4);
			if (impro_seq.length > num_impro)
				impro_seq.shift();
			impro_seq.push(tempo/4);
			if (impro_seq.length > num_impro)
				impro_seq.shift();
			impro_seq.push(tempo/4);
		}
	}
}

function nextStrike(){
	var note = impro_seq[curIdx];
	outlet(3, note);
	curIdx = curIdx+1;
	if (curIdx >= impro_seq.length)
		curIdx = 0;
}

function gettempo(){
	if (timeseq.length > 0 && avg(timeseq) > 10){
		outlet(2, 60000/avg(timeseq));
	}
}


function argmax(x){
	var index = 0;
	var maxv = x[0];
	for (var i=1; i<x.length; i++){
		if (x[i] > maxv){
			index = i;
			maxv = x[i];
		}
	}
	return index;
}

function sum(x){
	var s = 0;
	for (var i=0; i<x.length; i++)
		s += x[i];
	return s;
}

function getindex(x, val){
	for (var i=1; i<x.length; i++){
		if ( x[i] > val){
			var y = i-1;
			return y;
		}
	}
}
